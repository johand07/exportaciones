<?php

namespace App\Http\Controllers;

use App\Models\DetUsuario;
use App\Models\GenTipoEmpresa;
use App\Models\GenActividadEco;
use App\Models\GenSector;
use App\Models\Pais;
use App\Models\Estado;
use App\Models\Municipio;
use App\Models\Parroquia;
use App\Models\CircunsJudicial;
use \App\Models\GenArancelNandina;
use \App\Models\GenArancelMercosur;
use \App\Models\Accionistas;
use \App\Models\Productos;
use \App\Models\GenUnidadMedida;
use App\Models\CasaMatriz;
use App\Models\UltimaExport;
use App\Models\CatExportProd;
use Illuminate\Http\Request;

use Auth;
use Session;
use Laracasth\Flash\Flash;
use Alert;

class DatosJuridicosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    
        /*$titulo="Lista";
        $descripcion="Actualización de Datos de la Empresa";
        return view('DatosJuridicos.botones_juridicos');*/
         $TipoPersona=DetUsuario::where('gen_usuario_id',Auth::user()->id)->first();

        $valor=explode('-',$TipoPersona->rif);
        if( ($valor[0]=='V') || ($valor[0]=='E') || ($valor[0]=='P') ) {

          return view('DatosEmpresa.botones_natural');

        }else{

          $titulo="Lista";
          $descripcion="Actualización de Datos de la Empresa";
          return view('DatosJuridicos.botones_juridicos');

        }
          
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\DetUsuario  $detUsuario
     * @return \Illuminate\Http\Response
     */
    public function show(DetUsuario $detUsuario)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\DetUsuario  $detUsuario
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,DetUsuario $detUsuario, $id)
    {

     
        
        /*****Consulta tabla gen_actividad_eco para mostrar todas las activdades economicas*************/
        $actividadeco=GenActividadEco::all()->pluck('dactividad','id');///firt es para obtener el primero que consiga,get trae toda las filas que coindan con la condicion
        //dd($actividadeco);

        $datosempresa=DetUsuario::where('gen_usuario_id',$id)->first();

         



        $municipios=Municipio::all()->pluck('municipio','id');
        $parroquias=Parroquia::all()->pluck('parroquia','id');

        ##########################################################
         //consulto tabla de pais para mostrar Venezuela
        
        $casamatriz=CasaMatriz::where('gen_usuario_id',Auth::user()->id)->where('bactivo',1)->get();

        $paises=Pais::orderBy('dpais','asc')->pluck('dpais','id');
        //consulto tabla estados
        $estados=Estado::where('id',"<>",null)->orderBy('estado','asc')->pluck('estado','id');

        $sectores=GenSector::all()->pluck('descripcion_sector','id');

        //consulto tabla gen_tipoempresa me traigo todos los tipos de empresa
        $tipos_empresas=GenTipoEmpresa::all()->pluck('d_tipo_empresa','id');

        
         $unidad_medida=GenUnidadMedida::all()->pluck('dunidad','id');

        /************************Consulto la tabla accionista para traerme los que tengo registrado*****************/

         $accionistas=Accionistas::where('gen_usuario_id',Auth::user()->id)->where('bactivo',1)->get();

         /***********************************Consulto la tabla productos para traerme los que tengo registrado********/
         $productos=Productos::where('gen_usuario_id',Auth::user()->id)->where('bactivo',1)->get();

         $id=$id;
        //dd($accionistas);



        /******consulto la tabla gen_sector para traerme todos los sectores*******************/
        ///Consulta para traerme las actividades economicas,datos de la empresa,municipios,parroquias.


        ########Defino las variables que contienen el titulo y descripcion del modulo Datos empresa###############################
        $titulo="Registro del Exportador";
        $descripcion="Actualización de Datos ";


         return view('DatosJuridicos.edit',compact('titulo','descripcion','sectores','tipos_empresas','estados','paises','datosempresa','actividadeco','municipios','parroquias','unidad_medida','accionistas','productos','id','casamatriz'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\DetUsuario  $detUsuario
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DetUsuario $detUsuario,$id)
    {
        //dd($request->all());

    
       
/******************************Evaluamos si los checkbox vienen lleno o no si es Si lo colocamos en 1 y no es 0 ***********/
                   //dd($request->produc);
                if (!empty($request->invers)){

                     $invers=1;
                }else{
                    $invers=0;

                }

                if (!empty($request->export)){

                    $export=1;
                }else{

                    $export=0;
                }

                if (!empty($request->produc)){

                    $produc=1;

                }else{
                    $produc=0;
                }

                if (!empty($request->comerc)) {

                    $comerc=1;
                }else{
                    $comerc=0;
                }

   /************************** Insert de Datos Juridicos ****************************/
        $datos_juridicos=DetUsuario::find($id);
        $datos_juridicos->razon_social=$request->razon_social;
        $datos_juridicos->siglas=$request->siglas;
        $datos_juridicos->rif=$request->rif;
        $datos_juridicos->pagina_web=$request->pagina_web;
        $datos_juridicos->gen_actividad_eco_id=$request->gen_actividad_eco_id;
        $datos_juridicos->gen_tipo_empresa_id=$request->gen_tipo_empresa_id;
        //$datos_juridicos->pais_id=$request->pais_id;
        $datos_juridicos->estado_id=$request->estado_id;
        $datos_juridicos->municipio_id=$request->municipio_id;
        $datos_juridicos->parroquia_id=$request->parroquia_id;
        $datos_juridicos->direccion=$request->direccion;
        $datos_juridicos->telefono_local=$request->telefono_local;
        $datos_juridicos->telefono_movil=$request->telefono_movil;

        $datos_juridicos->invers=$invers;
        $datos_juridicos->export=$export;
        $datos_juridicos->produc=$produc;
        $datos_juridicos->comerc=$comerc;
        $datos_juridicos->save();






        Alert::success('ACTUALIZACIÓN EXISTOSA','Datos Básicos Actualizados')->persistent("Ok");
       return redirect()->action('DatosJuridicosController@index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\DetUsuario  $detUsuario
     * @return \Illuminate\Http\Response
     */
    public function destroy(DetUsuario $detUsuario)
    {
        //
    }
}
