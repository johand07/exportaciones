<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DetDeclaracionProducPais extends Model
{
    //
    protected $table= 'det_declaracion_produc_pais';

    protected $fillable = [

           'det_declaracion_produc_id',
           'pais_id'
                   
    ];
    public function rDeclaracionProduc() {

     return $this->hasOne('App\Models\DetDeclaracionProducPais','id','det_declaracion_produc_id');

    }
    public function rPais() {

     return $this->hasOne('App\Models\Pais','id','pais_id');

    }
    
}
