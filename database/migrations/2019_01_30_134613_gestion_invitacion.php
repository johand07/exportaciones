<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GestionInvitacion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('gestion_invitacion', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('eventos_id')->index()->nullable();
            $table->foreign('eventos_id')->references('id')->on('eventos');
            $table->string('repre_legal', 100)->nullable();
            $table->string('cedula', 15)->nullable();
            $table->string('razon_social', 100)->nullable();
            $table->string('rif', 20)->nullable();
            $table->string('correo', 100);
            $table->boolean('asistencia')->defaul(0)->nullable();
            $table->boolean('invitacion_enviada')->defaul(0)->nullable();
            $table->boolean('bactivo')->default(1)->nullable();
            $table->timestamps();
        });
            
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::dropIfExists('gestion_invitacion');
    }
}
