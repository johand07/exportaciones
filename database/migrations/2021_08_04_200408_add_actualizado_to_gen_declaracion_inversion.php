<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddActualizadoToGenDeclaracionInversion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('gen_declaracion_inversion', function (Blueprint $table) {
            
            $table->boolean('actualizado')->default(0)->after('num_declaracion');
            $table->string('num_declaracion_actualizada')->nullable()->after('actualizado');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('gen_declaracion_inversion', function (Blueprint $table) {
            
            $table->dropColumn('actualizado');
            $table->dropColumn('num_declaracion_actualizada');
        
       });
    }
}
