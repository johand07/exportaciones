<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GenDestinoInversionDjir_03 extends Model
{
    protected $table = 'gen_destino_inversion_djir03';
    protected $fillable = [
        'gen_declaracion_inversion_id',
        'cat_destino_inversion_id',
    ];

    public function genDecInversion()
	{
        return $this->hasOne('App\Models\GenDeclaracionInversion','id','gen_declaracion_inversion_id');
    }

    public function catDestInversion()
	{
        return $this->hasOne('App\Models\CatDestinoInversion','id','cat_destino_inversion_id');
    }

}
