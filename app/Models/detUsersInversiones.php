<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class detUsersInversiones extends Model
{
    protected $table = 'det_users_inversiones';

    protected $fillable = [

                'gen_usuario_id',
                'nombre_user',
                'apellido_user',
                'cedula_user',
                'bactivo',
                       
    ];

    public function usuario() {

     return $this->hasOne('App\Models\GenUsuario','id','gen_usuario_id');

    
    }
}
