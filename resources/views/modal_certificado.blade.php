<div class="modal fade" id="modal" role="dialog" aria-hidden="true">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
            {{$header}}
            {{Form::hidden('tipo_arancel',null,['id'=>'id_aran'])}}
            <br>
            <div class="form-group" align="right">
            {!! Form::label('Buscar')!!}
            {!! Form::text('buscador',null,['id'=>'buscador_aran'])!!}
            </div>
        </div>
        <div class="modal-body" id="arancel_certificado" style="overflow-y:scroll;height:300px">
        {{$body}}
        </div>
        <table class="table table-bordered table-hover">
        <tr><td><b>Para añadir nuevos productos, haga clic en el siguiente botón: </b><a href="{{ route('productos_export.edit', ['id' => 0]) }}" class="btn btn-lg btn-success btn-sm">Nuevos Productos</a></b></tr></table>
        <div class="modal-footer">
          <button type="button" class="btn btn-info" data-dismiss="modal" style="display:none">

         {{$footer}}

          </button>
        </div>
        
      </table>

    </div>
  </div>
