<html>
  <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <title>Planilla Consignacion de Documentos</title>
      <style>
          body{
              text-align:center;
             font-family: Arial;
                   color: #58585A;
          }
          th{
              font-size:10px;
              background-color:#818286;
              color: #fff;
          }
          td{
              font-size:10px;
              border: 1px solid #818286;
          }

          .p{
              border: 0px solid #fff;
          }

          .td2{
              font-size:10px;
              border: 0px solid #818286;
              width: 55%;
              text-align:center;
              font-size:14px;
          }
          .td3{
              
              border: 0px solid #818286;
              width: 15%;
              text-align:center;
              
          }
          .td4{
              
              border: 0px solid #818286;
              
              
          }
          table{
              border: 1px solid #818286;
          }
          .table2{
              border: 0px solid #818286;
          }

         table > tr > td {

            text-align: center;
          }
        .tabla > h4,h6{

          text-align: left;
          margin-bottom: 0;
          font-size:10px;

        }
        .tabla > table > thead > tr > th:nth-child(1){

           width: 30%;
        }
        .tabla > table > .tipo > tr:nth-child(1){

          border-top: 0;
        }

        .tabla2 > table > thead > tr > th:nth-child(0){

           width: 10%;
        }

      </style>
  </head>
      <body>          
        <div class="">
          <table style="width:100%;" class="table2">
            <tbody>
              <tr>
                <td class="td3">
                  
                  <img class="img-responsive" width="165px" height="115px" src='img/MIPPCOEXIN.png'>
                </td>
                <td class="td2"><br><br>
                  <br><br><br><br><h3>ACTA DE CONSIGNACIÓN DE DOCUMENTOS</h3>
                </td>
                <td class="td4">
                  <table  style="width:100%">
                    <thead>
                      <tr>
                        <th> FECHA: </th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td><br></td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
            </tbody>
          </table>
        </div>

        <table  class="table2" style="width: 100%;">
            <tr>
                <td class="td3" align="center">
                SOLICITUD
                  <input type="checkbox" style="display:inline" >
                </td>
                
                <td class="td3" align="center">
                DOCUMENTOS ANEXOS
                  <input type="checkbox" style="display:inline" >
                </td>
            </tr>
        </table>

        <div class="tabla">
          <table  style="width:100%">
            <thead>
              <tr>
                <th align="left"> Tipo de Solicitud </th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>Operación de Exportación. Demostración de la Venta de Divisas (DVD)</td>
              </tr>
            </tbody>
          </table>
        </div>

        <div class="tabla">
          <table  style="width:100%">
            <thead>
              <tr>
                <th align="left"> Nombre o Razón Social </th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>{{$users->razon_social}}</td>
              </tr>
            </tbody>
          </table>
        </div>

        <div class="tabla">
          <table  style="width:100%">
            <thead>
              <tr>
                <th align="left"> RIF / C.I </th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>{{$users->rif}}</td>
              </tr>
            </tbody>
          </table>
        </div>

        <div class="tabla">
          <table  style="width:100%">
            <thead>
              <tr>
                <th align="left" colspan="20"> Documentos a Consignar por Tipo de Solicitud </th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td colspan="3">Chequeo de Usuario</td>
                <td colspan="11"></td>
                <td colspan="3">Chequeo de Operador Cambiario Fecha: / / /</td>
                <td colspan="3">Chequeo por <br>Cencoex / Fecha: / / /</td>
              </tr>
              @foreach ($documentos as $doc)
              <tr>
                <td colspan="3"><input type="checkbox"></td>
                <td colspan="11">{{$doc->nombre_documento}}</td>
                <td colspan="3"><input type="checkbox"></td>
                <td colspan="3"><input type="checkbox"></td>
              </tr>
              @endforeach
              <!--tr>
                <td align="left" colspan="20"> Operación Pactada con Pago Anticipo </td>
              </tr>
              <!--Aqui va el foreach-->
              <!--tr>
                <td colspan="3"><input type="checkbox"></td>
                <td colspan="11"></td>
                <td colspan="3"><input type="checkbox"></td>
                <td colspan="3"><input type="checkbox"></td>
              </tr>
<!--Fin del foreach-->
            </tbody>
          </table>
        </div>
        <div class="tabla">
          <table  style="width:100%">
            <tbody>
            <tr>
                <th align="left" colspan="12"> Observaciones </th>
            </tr>
            <tr>
                <td colspan="12"><br><br></td>
            </tr>
            </tbody>
          </table>
        </div>

        <div class="col-md-12">
          <p align="justify">Por medio de la presente el Usuario o su Representante legal declara que los documentos contenidos en esta carpeta, son ciertos y reflejan con total precisión la situación del solicitante, igualmente autoriza a CENCOEX o a quien éste designe, a realizar las comprobaciones que considere necesarias y notificar por vía del operador cambiario autorizado o del medio que considere conveniente, los actos administrativos que emita de conformidad con la normativa cambiaria. <br><br>
          Se remiten la cantidad de:<b>_______(__________)</b> Folios.</p><br><br><br><br>
        </div>

         <table  class="table2" style="width: 100%;">
            <tr>
                <td class="td3" align="center">
                <b>________________________-____________________</b><br><br>
                Nombre y Firma del Usuario o Representante Legal 
                </td>
                
                <td class="td3" align="center">
                <b>________________________-____________________</b><br><br>
                Firma Autorizada y Sello del Operador Cambiario 
                </td>
            </tr>
        </table>

        <div class="col-md-12">
          <p align="justify"><b><span style="color: #ed1b24">NOTA: </span></b>Verifique que los documentos a consignar se correspondan con su trámite según las instrucciones establecidas en el “Manual de Consignación de Documentos destinados a la solicitudes de operaciones de exportación”.</p>
        </div>

     

  
          
    
        








      </body>
</html>