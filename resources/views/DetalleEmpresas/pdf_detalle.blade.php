<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Lista de Usuarios</title>
    <style>
        body{
            text-align:center;
           font-family: Arial;
                 color: #58585A; 
        }
        th{
            font-size:16px;
            background-color:#818286;
            color: #fff;
        }
        td{
            font-size:14px;
            border: 1px solid #818286;
        }
        table{
            border: 1px solid #818286;
        }
    </style>
</head>
    <body>
<div class="row">
  
  <div class="col-md-12">
    <div class="row">
      <div class="col-md-6">
        <b>Cantidad de Empresas Resgitradas:</b><span class="label label-warning">{{count($empresas)}}</span> 
        
      </div>
      <div class="col-md-6"></div>
    </div>
    <br>
    <table id="listaDetalleEmpresa" class="table table-striped table-bordered" style="width:100%">
            <thead>
                <tr>
                    <th>Rif</th>
                    <th>Nombre empresa </th>
                    <th>Dirección</th>
                    <th>Email</th>
                    <th>Tlf Local</th>
                    <th>Tlf Movil</th>
                    <th>Representante Legal</th>
                    
                </tr>
            </thead>
            <tbody>
            
            	@foreach($empresas as $empre)
                <tr>

                    <td><a title="Consultar detalle" target="_black" href="{{route('DetalleEmpresas.show',$empre->gen_usuario_id)}}" class="btn btn-primary btn-sm"><i class="glyphicon glyphicon-search" ></i> {{$empre->rif}}</a></td>


                    <td>{{$empre->razon_social}}</td>
                    <td>{{$empre->direccion}}</td>
                    <td>{{$empre->correo}}</td>
                    <td>{{$empre->telefono_local}}</td>
                    <td>{{$empre->telefono_movil}}</td>
                    <td>{{$empre->nombre_repre}} {{$empre->apellido_repre}}</td>
                    
                </tr>
              @endforeach
            </tbody>
        </table>
  </div>
  
</div>

</body>
</html>