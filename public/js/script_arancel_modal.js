/**
Requisitos del scrtipt

Jquery
Swal


**/
 var filtro_arancel_modal;
 var filtro_callback_name;
 var filtro_arancel_data_fetched;
 var filtro_arancel_who_calling;
 var arancel_data_selection;
 var colocarCodigoEn=1;
 var colocarDescripcionEn=0;

$(document).ready(function($) {
  $(document).on('click','.field-cod-arancel-modal',function(){

    //console.log(this);
    //console.log(this.value);

    filtro_arancel_modal=null;
    filtro_callback_name=null;
    filtro_arancel_data_fetched=null;
    $('#arancel_certificado').html('');


    filtro_arancel_who_calling=this;

    setFiltroArancelModal();
    setFiltroCallbackName(this);
    setArancelDataSelection(this);
    buscadorArancel();

   // $('#buscador_aran').keyup();


});

$(document).on('click','.field-replicate-arancel-modal',function(){
    replicarClicInitModal(this);
});

$(document).on('click','.filtro_arancel',function(){
 setFiltroArancelModal();
 buscadorArancel();
});

$(document).on('keyup','#buscador_aran',function(){
  buscadorArancel(); 
});



$(document).on('click','tr.data_certificado',function(){

  defaultSetFieldsValues(this);
  $('#modal button').click();
  $('#buscador_aran').val("");

  });

function setFiltroArancelModal(){
  var filtroAran= document.getElementsByClassName('filtro_arancel');

  if(!filtroAran==false){
     for(var i=filtroAran.length -1; i>=0; i--){
         if(filtroAran[i].type === 'radio' && filtroAran[i].checked)
          filtro_arancel_modal=filtroAran[i].value;
     }
  }

  //console.log(filtro_arancel_modal);
}

function  setFiltroCallbackName(elem){
  if(!elem==false){
    var callfunction=elem.getAttribute('data-filtro-arancel-callback');

    if(!callfunction==false){
      filtro_callback_name=callfunction;
    }
  }
}


function buscadorArancel(){
    var base= document.location.origin;
    var urlAjax = '/exportador/ajax/';
    var criterio = '';
    

    if(filtro_arancel_modal==2)
      urlAjax+= 'arancelMercosur/';
    else if(filtro_arancel_modal==1){
      urlAjax+= 'arancelNandina/';
    }else{
      swal("Por Favor!", "Seleccione en el filtro el tipo de arancel (Mercosur)", "warning");
      return;
    }

    

    if(!arancel_data_selection==false){
      urlAjax+=arancel_data_selection;
    }

    criterio=$('#buscador_aran').val();


     $.ajax({

              type:'get',
              url: base + urlAjax,
              data: {'valor': criterio,'tipoAran': filtro_arancel_modal},

              success: function(data){

                filtro_arancel_data_fetched=data;
                $table='<table class="table table-bordered table-hover" id="tabla"><tr><th class="hidden">Nº</th><th>Código Arancelario</th><th>Descripción</th></tr>';

                $('#arancel_certificado').html($table);
                if(data.length==0){

                 $('#tabla tr:last').after('<tr><td colspan="2" style="color:red;text-align:center" >Códigos no existen</td></tr>');

               }else{


                $.each(data,function(index,value){

               $('#tabla tr:last').after('<tr class="data_certificado"><td class="hidden">'+value.id+'</td><td>'+value.codigo+'</td><td>'+value.descripcion+'</td><tr></table');

                });

              }

              }

           });
}


function defaultSetFieldsValues(elem){

  var cod=$(elem).find("td").eq(1).text();
  var ara=$(elem).find("td").eq(2).text();

  if(!filtro_callback_name==false){
      filtro_callback_name(filtro_arancel_data_fetched);
  }else{

    if(!filtro_arancel_who_calling==false){
        
        var trPadre = $(filtro_arancel_who_calling).parent().parent();
    
        if(!trPadre==false){

          trPadre.find("td").eq(colocarDescripcionEn).children("input").val(ara);
          trPadre.find("td").eq(colocarCodigoEn).children("input").val(cod);
        }
    }
  }

}

function  setArancelDataSelection(elem){
  if(!elem==false){
    var dataSelection=elem.getAttribute('data-arancel-data-selection');

    if(!dataSelection==false){
      arancel_data_selection=dataSelection;
    }else{
      arancel_data_selection='all';
    }
  }
}

function replicarClicInitModal(elem){
  if(!elem==false){
    var padre=elem.parentNode.parentNode;
    var elementoCodArancel= padre.getElementsByClassName('field-cod-arancel-modal');

    if(!elementoCodArancel==false){
      if(elementoCodArancel.length==1){
        elementoCodArancel[0].click();
      }
    }
  }
}


});

