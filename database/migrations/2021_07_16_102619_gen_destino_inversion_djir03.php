<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GenDestinoInversionDjir03 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gen_destino_inversion_djir03', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('gen_declaracion_inversion_id');
            $table->foreign('gen_declaracion_inversion_id', 'g_d_i_fk3')->references('id')->on('gen_declaracion_inversion');
            $table->unsignedInteger('cat_destino_inversion_id')->index();
            $table->foreign('cat_destino_inversion_id')->references('id')->on('cat_destino_inversion');
            $table->boolean('bactivo')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gen_destino_inversion_djir03');
    }
}
