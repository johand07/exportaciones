<?php

namespace App\Http\Controllers;
use App\Models\GenUsuario;
use App\Models\DetUsuario;
use App\Models\Pais;
use App\Models\UltimaExport;
use Illuminate\Http\Request;

use Session;
use Auth;
use Laracasts\Flash\Flash;
use Alert;

class UltimaExportacionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('DatosJuridicos.botones_juridicos');

        

    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\DetUsuario  $detUsuario
     * @return \Illuminate\Http\Response
     */
    public function show(UltimaExport $ultimaExport)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\DetUsuario  $detUsuario
     * @return \Illuminate\Http\Response
     */
    public function edit(UltimaExport $ultimaExport,$id)
    {
        $titulo="Registro del Exportador";
        $descripcion="Actualización de Datos";
        @$ultima_exportacion=UltimaExport::where('gen_usuario_id',$id)->first();

        //consulto tabla de pais para mostrar Venezuela
            $paises=Pais::orderBy('dpais','asc')->pluck('dpais','id');

            

       /***************************************************************************/


         return view('DatosJuridicos.edit_ult_export',compact('titulo','descripcion','paises','ultima_exportacion'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\DetUsuario  $detUsuario
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UltimaExport $ultimaExport,$id)
    {
        
         $exportUlt=UltimaExport::find($id);
         $exportUlt->pais_id=$request->pais_id;
         $exportUlt->fecha_ult_export=$request->fecha_ult_export;
         $exportUlt->save();

         
        Alert::success('ACTUALIZACIÓN EXISTOSA','Última Exportación Actualizada')->persistent("ok");
         return redirect()->action('DatosJuridicosController@index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\DetUsuario  $detUsuario
     * @return \Illuminate\Http\Response
     */
    public function destroy(UltimaExport $ultimaExport)
    {
        //
    }
}
