@extends('templates/layoutlte')
@section('content')
{{Form::model($NDebito,['route'=>['NDebito.update', $NDebito->id],'method'=>'PATCH','id'=>'NDebito'])}}
<br><div class="row"> 
<div class="col-md-12">
	<div class="col-md-3"></div>
	<div class="col-md-6">
		<div class="form-group">
			{!! Form::hidden('gen_usuario_id', Auth::user()->id) !!}
			{!! Form::label('Consignatario','Consignatario')!!}
			{!! Form::select('gen_consignatario_id',$consignatario,null,['class'=>'form-control','id'=>'gen_consignatario_id','onkeypress'=>'return soloNumeros(event)','placeholder'=>'Seleccione un Consignatario'])!!}
		</div>
		<div class="form-group">
			{!! Form::label('Numero de Nota de Débito','Numero de Nota de Débito')!!}
			{!! Form::text('num_nota_debito',null,['class'=>'form-control','id'=>'num_nota_debito','onkeypress'=>'return soloNumeros(event)'])!!}
		</div>
			<div class="form-group">
			
		</div>
		<div class="form-group">
	    	{!! Form::label('','Fecha de Emisión') !!} 
	    	<div class="input-group date">
	    		{!! Form::text('fecha_emision',null,['class'=>'form-control','readonly'=>'readonly','id'=>'fecha_emision']) !!}
	    		<span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
	    	</div>	              
	   </div>
	   <div class="form-group">
			{!! Form::label('','Concepto de la Nota de Débito') !!}
			{!! Form::text('concepto_nota_debito',null,['class'=>'form-control','id'=>'concepto_nota_debito']) !!}
		</div>
		<div class="form-group">
			{!! Form::label('Monto de la Nota Débito','Monto de Nota Débito')!!}
			{!! Form::text('monto_nota_debito',null,['class'=>'form-control format','id'=>'monto_nota_debito'])!!}
		</div>
		<div class="form-group">
			{!! Form::label('Numero de Factura Asociada','Número de Factura Asociada') !!}
			{!! Form::select('gen_factura_id',$factura,null,['class'=>'form-control','id'=>'gen_factura_id',
			'placeholder'=>'Seleccione una Factura']) !!}
		</div>
		<!--
		<div class="form-group">
			{!! Form::label('er','Número de la Exportación Realizada (ER)')!!}
			{!! Form::text('er',null,['class'=>'form-control','id'=>'er','maxlength'=>'100','onkeypress'=>'return soloNumerosyLetras(event)'])!!}
		</div> -->
		<div class="form-group">
			{!! Form::label('justificacion','Justificación')!!}
			{!! Form::text('justificacion',null,['class'=>'form-control','id'=>'justificacion','maxlength'=>'100','onkeypress'=>'return soloNumerosyLetras(event)'])!!}
		</div>
	</div>
	<div class="col-md-3"></div>
</div>
</div>
<br><div class="row">
	<div class="col-md-12">
		<div class="col-md-4"></div>
		<div class="col-md-4 text-center">
		<a href="{{ route('NDebito.index') }}" class="btn btn-primary">Cancelar</a>
		<input type="submit" class="btn btn-success" value="Enviar" name="" onclick="enviardebito()">
		</div>
		<div class="col-md-4"></div>
	</div>
</div>
{{Form::close()}}
@stop