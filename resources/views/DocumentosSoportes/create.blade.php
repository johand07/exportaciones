

{{Form::open(['route'=>'DocumentosSoportes.store','method'=>'POST','id'=>'soportes', 'enctype' => 'multipart/form-data'])}}

<div class="content">
	<div class="panel panel-primary">
		<div class="panel-primary" style=" #fff">
			
			<div class="panel-heading"><h4>Carga de Documentos Soporte</h4></div>

			<div class="panel-body">
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-4">
							<tr>
								<td>
									<span><b>1.) Documento de Identidad (Cédula).</b></span><br><br>
                      				<strong style="color: red"> *</strong>
				                      <span class="btn btn-default btn-file" style="margin: 3px;">
				                        Cargar Archivo <span class="glyphicon glyphicon-file"></span>
				                        {!! Form::file('ruta_doc', array('class' => 'file-input','style'=>'margin:10px;','id'=>'ruta_doc','title'=>'formatos permitidos .doc, .docx, .pdf')) !!}
				                      </span>
								</td>
								 <td class="text-center" height="40px"><img id="vista_previa_file_1" src="" alt="" width="300px" style="margin: 3px;"><img id="imgdocumento_ruta_doc" src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px; display: none;"><div id="nombre_ruta_doc"></div>
                 				 </td>
							</tr>
						</div>
						<div class="col-md-4">
							<tr>
								<td>
									<span><b>2.) Documento (RIF).</b></span><br><br>
                      				<strong style="color: red"> *</strong>
				                      <span class="btn btn-default btn-file" style="margin: 3px;">
				                        Cargar Archivo <span class="glyphicon glyphicon-file"></span>
				                        {!! Form::file('ruta_rif', array('class' => 'file-input','style'=>'margin:10px;','id'=>'ruta_rif','title'=>'formatos permitidos .doc, .docx, .pdf')) !!}
				                      </span>
								</td>
								 <td class="text-center" height="40px"><img id="vista_previa_file_2" src="" alt="" width="300px" style="margin: 3px;"><img id="imgdocumento_ruta_rif" src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px; display: none;"><div id="nombre_ruta_rif"></div>
                 				 </td>
							</tr>
						</div>
						<div class="col-md-4">
							<tr>
								<td>
									<span><b>3.) Documento (Registro Mercantil).</b></span><br><br>
                      				<strong style="color: red"> *</strong>
				                      <span class="btn btn-default btn-file" style="margin: 3px;">
				                        Cargar Archivo <span class="glyphicon glyphicon-file"></span>
				                        {!! Form::file('ruta_reg_merc', array('class' => 'file-input','style'=>'margin:10px;','id'=>'ruta_reg_merc','title'=>'formatos permitidos .doc, .docx, .pdf')) !!}
				                      </span>
								</td>
								 <td class="text-center" height="40px"><img id="vista_previa_file_3" src="" alt="" width="300px" style="margin: 3px;"><img id="imgdocumento_ruta_reg_merc" src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px; display: none;"><div id="nombre_ruta_reg_merc"></div>
                 				 </td>
							</tr>
						</div>


					</div>
				</div><br><br><br>

				<div class="row text-center">
					<a href="{{url('exportador/DatosEmpresa')}}" class="btn btn-primary">Cancelar</a>
					<input type="submit" class="btn btn-success" value="Enviar" name="Enviar" onclick="()"> 
				</div>
			</div>


		</div>
	</div>
</div>

{{Form::close()}}

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="{{asset('js/ajax_v2.1.1.js')}}"></script>
<!--Script para Tooltips guia de campos con stylos bootstrap-->
<script src="https://unpkg.com/@popperjs/core@2"></script>
<script src="https://unpkg.com/tippy.js@6"></script>



<script type="text/javascript">


/********************Scrip para carga de archivos**************** */
 function cambiarImagen(event) {
    var id= event.target.getAttribute('name');
    var reader = new FileReader();
    reader.onload = function(){
      var output = document.getElementById('vista_previa_'+id);
      output.src = reader.result;
    };
    //let archivo = $(".file-input").val();
    //if (!archivo) {

      let archivo=$('#'+id).val();
      //console.log(reader);
    //}
    var extensiones = archivo.substring(archivo.lastIndexOf(".")).toLowerCase();
    
    //var imgfile="/exportaciones/public/img/file.png";

//alert(imgfile);
    if(extensiones != ".doc" && extensiones != ".docx" && extensiones != ".pdf"){
      swal("Formato invalido", "Formato de archivo invalido debe ser .doc, .docx o pdf", "warning"); 
        
        
    }else{
      //Pintame la vista previa el Documento azulito cuando se carga un archivo con imgdocumento_
      $('#imgdocumento_'+id).show();
      let file=$('#'+id).val();
     // alert(file);
     //Y pintame ruta y nombre del archivo acargado con nombre_
      $('#nombre_'+id).html('<strong>'+file+'</strong>');
     
      reader.readAsDataURL(event.target.files[0]);

    }

  }
  ///////////////Para validar documentos//////////////////

  function cambiarImagenDoc(event) {

    
    //console.log(event);

    var id= event.target.getAttribute('id');
    //alert(id);
    
  
      archivo=$('#'+id).val();
    //}
    var extensiones = archivo.substring(archivo.lastIndexOf(".")).toLowerCase();
    
    //var imgfile="/exportaciones/public/img/file.png";

    
    if(extensiones != ".doc" && extensiones != ".docx" && extensiones != ".pdf"){
      swal("Formato invalido", "Formato de archivo invalido debe ser .doc, .docx o pdf", "warning"); 
        $('#'+id).val('');
        
    }else{
      $('#imgdocumento_'+id).show();
      let file=$('#'+id).val();
     // alert(file);
      $('#nombre_'+id).html('<strong>'+file+'</strong>');
     
      reader.readAsDataURL(event.target.files[0]);

    }

  }
/////////////////Validacion acondicionada para cuanto la carga de archivo NO puede sercargado un archivo que pee mas de 2.5 megabytes////////////////
    $(document).on('change', '.file-input', function(evt) {
      
      let size=this.files[0].size;
      if (size>8500000) {

         swal("Tamaño de archivo invalido", "El tamaño del archivo No puede ser mayor a 8.5 megabytes","warning"); 
          this.value='';
         //this.files[0].value='';
      }else{
        cambiarImagen(evt);
      }
      
           
    });
///////////LO mismo para carga de archivo lista dinamica//////////
  $(document).on('change', '.file_multiple', function(evt) {
           

          let size=this.files[0].size;
      if (size>8500000) {

         swal("Tamaño de archivo invalido", "El tamaño del archivo No puede ser mayor a 8.5 megabytes","warning"); 
         this.value='';
      }else{
        cambiarImagenDoc(evt);
      }

    });

  </script>




