@extends('templates/layoutlte')
@section('content')
<div class="content">
  @if(!empty($planilla2->estado_observacion))
<div class="alert alert-danger" role="alert">
  <strong>
    <span class="glyphicon glyphicon-circle-arrow-right"></span> Observación Indicada por el Analista: {{$planilla2->descrip_observacion}}
  </strong>
</div>
<div id="space_alert1" class="alert alert-danger" role="alert" style="display: block;">
  <strong>
  <h3>Info: Si se le solicita editar el Código Arancelario o su Denominación comercial; <span style="color:red;text-align:center">Diríjase a su Perfil-> Datos de la empresa -> Actualizar productos, para editar el producto correspondiente: </span></h3>
  <br>
  <table>
    <tr align="center">
        <td >
            <b>1.)</b> <img  src={{ asset('img/actualizar_productos.png') }}>
        </td>
        <td>
            <b>2.)</b> <a href="{{route('productos_export.edit',Auth::user()->id)}}"><img width="80%" src={{ asset('img/actualizar_producto.png') }}></a>
        </td>
        
    </tr>
  </table>
  </strong>
</div>
@endif
<div class="panel-group" style="background-color: #fff;">
      <div class="panel-primary">
        <div class="panel-heading"><h3>(P-2) - 4. Datos Generales del Producto a Exportar</h3></div>
        <div class="panel-body">


<div class="row" style="background-color: #fff">
    <div class="col-md-12">
 
{!! Form::model($planilla2,['route' =>array('DJOPlanilla2.update',$planilla2->id) ,'method'=>'PUT','id'=>'planilla2UpdateForm']) !!}

@if(!empty(old('pais_importado')))
@php
    $auxOldPaisImportado = old('pais_importado');
    session(['_old_input.pais_importado' => null]);
@endphp
@endif
        <div class="row">
            <!--div class="col-md-12">
                <h3>4. Datos generales del producto a exportar</h3>
            </div-->
            <div class="col-md-6">
                <div class="form-group">
                    {!! Form::label('descripcion_comercial','4.1 Denominación Comercial')!!}
                    {!! Form::text('descripcion_comercial',$producto->descrip_comercial,['class'=>'form-control','readonly'=>'readonly'])!!}
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    {!! Form::label('descripcion_arancel','4.2 Descripción Arancelaria')!!}
                    {!! Form::text('descripcion_arancel',$producto->descripcion,['class'=>'form-control','readonly'=>'readonly'])!!}
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    {!! Form::label('codigo_arancel','4.3 Código Arancelario')!!}
                    {!! Form::text('codigo_arancel',$producto->codigo,['class'=>'form-control','readonly'=>'readonly'])!!}
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    {!! Form::label('unidad_medida','4.4 Unidad de medida')!!}
                    {!! Form::text('unidad_medida',$unidadMedida->dunidad,['class'=>'form-control','readonly'=>'readonly'])!!}
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    {!! Form::label('uso_aplicacion','4.5 Uso y aplicación')!!}
                    {!! Form::text('uso_aplicacion',null,['class'=>'form-control tagsInput'])!!}
                </div>
            </div>
        </div>
  
        <div class="row" style="background-color: #fff">
            <div class="col-md-12">
                <p><b>4.6 Materiales que intervienen en la elaboración del producto</b></p>
            </div>
        <div class="col-md-12">

                    <!-- ############################################################### -->
             
              <div class="row">
                  <div class="col-md-12">

                      <table class="table table-bordered" id="fieldse">
                            <tr>
                              <th>Materiales Importados </th>
                              <th>Código Arancelario</th>
                              <th></th>
                              <th>País de Origen</th>
                              <th>Incidencia sobre costo total de Materiales importados</th>
                            </tr>

                            <tr id="row">
                                <td class="hidden">
                                {{ Form::hidden('material_importado_id[]', null) }}
                                </td>
                              <td>                
                                  {!!Form::text('descripcion_arancelaria_importado[]',null,['class'=>'form-control','id'=>'cod_aran_importado', 'placeholder'=>'Selecionar Codigo', 'onkeypress'=>'return soloLetras(event)'])!!}
                              </td>
                              <td>
                                  {!!Form::text('codigo_arancel_importado[]',null,['class'=>'form-control','id'=>'arancel_descrip_importado','onkeypress'=>'return soloNumerosDouble(event)'])!!}
                              </td>
                              <td>
                                    <botton type="button" required="" id="boton_nan_308" class=" btn btn-xs btn-success field-cod-arancel-modal tagsInput" data-toggle="modal" data-target="#modal"><i class="glyphicon glyphicon-plus"></i></botton>
                              </td>
                              <td>
                                 {!! Form::select('pais_importado[]',$pais,isset($auxOldPaisImportado)?$auxOldPaisImportado[0]:null,['class'=>'form-control tagsInput', 'id'=>'pais_origen','placeholder'=>'Seleccione el Pais de Origen']) !!}
                              </td>

                              <td>
                                  {!!Form::number('monto_incidencia_importado[]',null,['class'=>'form-control incidencia-importado tagsInput','step' => '0.01'])!!}
                              </td>

                              <td>
                                  <button  name="add" type="button" id="add" value="add more" class="btn btn-success">Agregar</button>
                              </td> 
                            </tr>

{{-- Si existe old del campo se repite --}}

@if(!empty(old('descripcion_arancelaria_importado')))
@for($tempIter = 1; $tempIter <= count(old('descripcion_arancelaria_importado'))-1 ; $tempIter++ )
            <tr>
                                <td class="hidden">
                                {{ Form::hidden('material_importado_id[]', null) }}
                                </td>
                              <td>
                                  {!!Form::text('descripcion_arancelaria_importado[]',null,['class'=>'form-control ','id'=>'cod_aran_importado', 'placeholder'=>'Selecionar Codigo', 'onkeypress'=>'return soloLetras(event)'])!!}
                              </td>
                              <td>
                                  {!!Form::text('codigo_arancel_importado[]',null,['class'=>'form-control ','id'=>'arancel_descrip_importado','onkeypress'=>'return soloNumerosDouble(event)'])!!}
                              </td>
                              <td>
                                    <botton type="button" required="" id="boton_nan_308" class=" btn btn-xs btn-success field-cod-arancel-modal tagsInput" data-toggle="modal" data-target="#modal"><i class="glyphicon glyphicon-plus"></i></botton>
                              </td>
                              <td>
                                 {!! Form::select('pais_importado[]',$pais,isset($auxOldPaisImportado)?$auxOldPaisImportado[$tempIter]:null,['class'=>'form-control tagsInput', 'id'=>'pais_origen','placeholder'=>'Seleccione el Pais de Origen']) !!}
                              </td>

                              <td>
                                  {!!Form::number('monto_incidencia_importado[]',null,['class'=>'form-control incidencia-importado tagsInput','step' => '0.01'])!!}
                              </td>

                              <td>
                                  <button  name="remove" type="button" id="1'+i+'" value="" class="btn btn-danger btn-remove">x</button>
                              </td> 
            </tr>

      @endfor
@endif
@if(!empty($auxOldPaisImportado))
@php
    session(['_old_input.pais_importado' => $auxOldPaisImportado]);
@endphp
@endif
                            <tr>
                              <td colspan="3" align="right" style="vertical-align:middle">
                                  <b>Total:</b>
                              </td>
                              <td>
                                 <label class="form-control"  id="total_incidencia_importado">0.00</label>
                              </td>
                              <td>
                              </td> 
                            </tr>
                      </table>
         
                  </div>
              </div>
        </div>

</div>
<hr>  
        <!-- ################################################################ -->
<div class="row" style="background-color: #fff">
        <div class="col-md-12">

              <div class="row">
                  <div class="col-md-12">

                      <table class="table table-bordered" id="fieldse2">
                            <tr>
                              <th>Materiales Nacionales</th>
                              <th>Código Arancelario</th>
                              <th></th>
                              <th>Nombre del Productor Nacional</th>
                              <th>Nombre del Proveedor</th>
                              <th>Incidencia sobre costo total De Materiales Nacionales</th>
                              <th>Rif del Productor</th>
                            </tr>

                            <tr id="row">
                            <td class="hidden">
                                {{ Form::hidden('material_nacional_id[]', null) }}
                                </td>
                               <td>                
                                  {!!Form::text('descripcion_arancelaria_nacional[]',null,['class'=>'form-control','id'=>'cod_aran_nacional', 'placeholder'=>'Selecionar Codigo', 'onkeypress'=>'return soloLetras(event)'])!!}
                              </td>
                              <td>
                                  {!!Form::text('codigo_arancel_nacional[]',null,['class'=>'form-control','id'=>'arancel_descrip_nacional','onkeypress'=>'return soloNumerosDouble(event)'])!!}
                              </td>
                              <td>
                                    <botton type="button" required="" id="boton_nan_308" class=" btn btn-xs btn-success field-cod-arancel-modal tagsInput" data-toggle="modal" data-target="#modal"><i class="glyphicon glyphicon-plus"></i></botton>
                              </td>
                              <td>
                                  {!!Form::text('nombre_producto_nacional[]',null,['class'=>'form-control tagsInput','onkeypress'=>'return soloLetras(event)'])!!}
                              </td>
                               <td>
                                  {!!Form::text('nombre_proveedor_nacional[]',null,['class'=>'form-control  tagsInput','onkeypress'=>'return soloLetras(event)'])!!}
                              </td>

                              <td>
                                  {!!Form::number('monto_incidencia_nacional[]',null,['class'=>'form-control incidencia-nacional tagsInput','step' => '0.01'])!!}
                              </td>
                              <td>
                                {!!Form::text('rif_productor[]',null,['class'=>'form-control','id'=>'rif_productor', 'placeholder'=>'Rif del Productor', 'onkeypress'=>'return soloNumerosyLetras(event)'])!!}
                              </td>

                              <td>
                                  <button  name="add" type="button" id="add2" value="add more" class="btn btn-success">Agregar</button>
                              </td> 
                            </tr>

{{-- Si existe old del campo se repite --}}

@if(!empty(old('descripcion_arancelaria_nacional')))
@for($tempIter = count(old('descripcion_arancelaria_nacional'))-1; $tempIter > 0 ; $tempIter-- )
            <tr id="row">
                             <td class="hidden">
                                {{ Form::hidden('material_nacional_id[]', null) }}
                                </td>
                               <td>                
                                  {!!Form::text('descripcion_arancelaria_nacional[]',null,['class'=>'form-control ','id'=>'cod_aran_nacional', 'placeholder'=>'Selecionar Codigo', 'onkeypress'=>'return soloLetras(event)'])!!}
                              </td>
                              <td>
                                  {!!Form::text('codigo_arancel_nacional[]',null,['class'=>'form-control','id'=>'arancel_descrip_nacional','onkeypress'=>'return soloNumerosDouble(event)'])!!}
                              </td>
                              <td>
                                    <botton type="button" required="" id="boton_nan_308" class=" btn btn-xs btn-success field-cod-arancel-modal tagsInput" data-toggle="modal" data-target="#modal"><i class="glyphicon glyphicon-plus"></i></botton>
                              </td>
                              <td>
                                  {!!Form::text('nombre_producto_nacional[]',null,['class'=>'form-control tagsInput','onkeypress'=>'return soloLetras(event)'])!!}
                              </td>
                               <td>
                                  {!!Form::text('nombre_proveedor_nacional[]',null,['class'=>'form-control tagsInput','onkeypress'=>'return soloLetras(event)'])!!}
                              </td>

                              <td>
                                  {!!Form::number('monto_incidencia_nacional[]',null,['class'=>'form-control incidencia-nacional tagsInput','step' => '0.01'])!!}
                              </td>
                               <td>
                                {!!Form::text('rif_productor[]',null,['class'=>'form-control','id'=>'rif_productor', 'placeholder'=>'Rif del Productor', 'onkeypress'=>'return soloNumerosyLetras(event)'])!!}
                              </td>

                              <td>
                                  <button  name="remove" type="button" id="2'+j+'" value="" class="btn btn-danger btn-remove">x</button>
                              </td> 
                            </tr>

                            @endfor
                          @endif

                            <tr>
                              <td colspan="4" align="right" style="vertical-align:middle">
                                  <b>Total:</b>
                              </td>
                              <td>
                                 <label class="form-control"  id="total_incidencia_nacional">0.00</label>
                              </td>
                              <td>
                              </td> 
                            </tr>
                      </table>
         
                  </div>
              </div>
        </div>

           <!--  ###############################################################  -->
        </div>



<div class="row" style="background-color: #fff">

      <div class="col-md-12" align="center">

          <!--input type="submit"  name="submit" id="submit" class="btn btn-info"  value="Corregir" /-->
          <button  type="submit"  name="submit" id="submit"  class="btn btn-primary" onclick="validacionForm()"><b>Corregir</b></button>
      </div>
    
  </div>


{{Form::close()}}

</div>
</div>

        </div>
      </div>
    </div>

  </div><!-- Fin content -->

@component('modal_arancel',['arancel'=>''])

@slot('header') <span>Código Arancelario</span> @endslot

@slot('body')  <div id="arancel_certificado"></div>

@endslot

@slot('footer')@endslot

@endcomponent
  


<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="{{asset('js/script_arancel_modal.js')}}"></script>
<script>
 colocarCodigoEn=2;
 colocarDescripcionEn=1;
</script>
<script type="text/javascript">

 /*-------------------------------------------------*/
  
  /*----------------------------------------------*/

$(document).ready(function (){
 
    

    var i = 0;
    var j=0;

    sumarIncidenciaImportados();
    sumarIncidenciaNacionales()

    $('#add').click(function()
    {
       i++;

      $('#fieldse tr').eq($('#fieldse tr').length - 2).after('<tr id="row1'+i+'" display="block" class="show_div"><td class="hidden">{{ Form::hidden('material_importado_id[]', null) }}</td><td>{!!Form::text('descripcion_arancelaria_importado[]',null,['class'=>'form-control','id'=>'cod_aran_importado', 'placeholder'=>'Selecionar Codigo', 'onkeypress'=>'return soloLetras(event)'])!!}</td><td>{!!Form::text('codigo_arancel_importado[]',null,['class'=>'form-control','id'=>'arancel_descrip_importado','onkeypress'=>'return soloNumerosDouble(event)'])!!}</td><td><botton type="button" required="" id="boton_nan_308" class=" btn btn-xs btn-success field-cod-arancel-modal tagsInput" data-toggle="modal" data-target="#modal"><i class="glyphicon glyphicon-plus"></i></botton></td><td>{!! Form::select('pais_importado[]',$pais,null,['class'=>'form-control tagsInput', 'id'=>'pais_origen','placeholder'=>'Seleccione el Pais de Origen']) !!}</td><td>{!!Form::number('monto_incidencia_importado[]',null,['class'=>'form-control incidencia-importado tagsInput','step' => '0.01'])!!}</td><td><button  name="remove" type="button" id="1'+i+'" value="" class="btn btn-danger btn-remove">x</button></td></tr>');
        sumarIncidenciaImportados();

    });

   $('#add2').click(function()
    {
       j++;

      $('#fieldse2 tr').eq($('#fieldse2 tr').length - 2).after('<tr id="row2'+j+'" display="block" class="show_div"><td class="hidden">{{ Form::hidden('material_nacional_id[]', null) }}</td><td>{!!Form::text('descripcion_arancelaria_nacional[]',null,['class'=>'form-control','id'=>'cod_aran_nacional', 'placeholder'=>'Selecionar Codigo', 'onkeypress'=>'return soloLetras(event)'])!!}</td><td>{!!Form::text('codigo_arancel_nacional[]',null,['class'=>'form-control','id'=>'arancel_descrip_nacional','onkeypress'=>'return soloNumerosDouble(event)'])!!}</td><td><botton type="button" required="" id="boton_nan_308" class=" btn btn-xs btn-success field-cod-arancel-modal tagsInput" data-toggle="modal" data-target="#modal"><i class="glyphicon glyphicon-plus"></i></botton></td><td>{!!Form::text('nombre_producto_nacional[]',null,['class'=>'form-control tagsInput','onkeypress'=>'return soloLetras(event)'])!!}</td><td>{!!Form::text('nombre_proveedor_nacional[]',null,['class'=>'form-control tagsInput','onkeypress'=>'return soloLetras(event)'])!!}</td><td>{!!Form::number('monto_incidencia_nacional[]',null,['class'=>'form-control incidencia-nacional tagsInput','step' => '0.01'])!!}</td><td>{!!Form::text('rif_productor[]',null,['class'=>'form-control','id'=>'rif_productor', 'placeholder'=>'Rif del Productor', 'onkeypress'=>'return soloNumerosyLetras(event)'])!!}</td><td><button  name="remove" type="button" id="2'+j+'" value="" class="btn btn-danger btn-remove">x</button></td></tr>');


    });

      $(document).on('click','.btn-remove',function(){
        
        this.parentNode.parentNode.remove();

      });

      function sumarIncidenciaImportados(){

        var elementos = document.getElementsByName("monto_incidencia_importado[]");
        var suma = 0.0;

        if(!elementos==false){

          for(var i= elementos.length -1 ; i>=0 ; i--){

          
            if(!isNaN(parseFloat(elementos[i].value)))
            suma += parseFloat(elementos[i].value );

          }
        $("#total_incidencia_importado").html(""+parseFloat(suma).toFixed(2));

        }
      }

      function sumarIncidenciaNacionales(){

        var elementos = document.getElementsByName("monto_incidencia_nacional[]");
        var suma = 0.0;

        if(!elementos==false){

          for(var i= elementos.length -1 ; i>=0 ; i--){

          
            if(!isNaN(parseFloat(elementos[i].value)))
            suma += parseFloat(elementos[i].value );

          }
        $("#total_incidencia_nacional").html(""+parseFloat(suma).toFixed(2));
        }
        
      }


      $(document).on('keyup','.incidencia-importado',function(){
        sumarIncidenciaImportados();
      });

       $(document).on('keyup','.incidencia-nacional',function(){
        sumarIncidenciaNacionales();
      });
/*
      $('.incidencia-importado').keypress(function(event) {
        alert('hi');
      });*/


              //Validación de formulario, se puede llamar y pasar la sección que contenga los inputs a validar
              
              var error = '<div class="msg_alert"><span style="color:red" class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span><strong id="cod_aran-error" class="added"> El campo es obligatorio.</strong></div>';
              var valido = '<div class="msg_alert"><span style="color:green" class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span></div>';
              var mens=['Estimado Usuario. Debe completar los campos solicitados',]

              validacionForm = function() {

              $('#planilla2UpdateForm').submit(function(event) {
              
              var campos = $('#planilla2UpdateForm').find('input, select');
              var n = campos.length;
              var err = 0;

              $("div").remove(".msg_alert");
              //bucle que recorre todos los elementos del formulario
              for (var i = 0; i < n; i++) {
                  var cod_input = $('#planilla2UpdateForm').find('input, select').eq(i);
                  if (!cod_input.attr('noreq')) {
                    if (cod_input.val() == '' || cod_input.val() == null)
                    {
                      err = 1;
                      cod_input.css('border', '1px solid red').after(error);
                    }
                    else{
                      if (err == 1) {err = 1;}else{err = 0;}
                      cod_input.css('border', '1px solid green').after(valido);
                    }
                    
                  }
              }

              //Si hay errores se detendrá el submit del formulario y devolverá una alerta
              if(err==1){
                  event.preventDefault();
                  swal("Por Favor!", mens[0], "warning")
                }

                });
              }
      
 });


</script>
@endsection