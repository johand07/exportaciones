<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\PlanillaDjir_01;
use App\Models\PlanillaDjir_02;
use App\Models\PlanillaDjir_03;
use App\Models\Estado;
use App\Models\Municipio;
use App\Models\genDocDecInversion;
use App\Models\GenAccionistasDeclaracionDjir_01;
use Auth;
use Alert;

class ActualizarPlanilla2DecInversionController extends Controller
{
    public function Actualizar(PlanillaDjir_02 $planillaDjir_02, $id, $gen_declaracion_inversion_id)
    {
        //dd($new_gen_declaracion_inversion_id);

        $planillaDjir02 = PlanillaDjir_02::where('gen_declaracion_inversion_id', $id)->first();
        //dd($planillaDjir02->id);



       $aniodeclaracion=['2017'=>'AÑO 2017','2018'=>'AÑO 2018','2019'=>'AÑO 2019','2020'=>'AÑO 2020','2021'=>'AÑO 2021'];




        return view('BandejaInversion.actualizar.editPlanilla2Inversion', compact('gen_declaracion_inversion_id', 'planillaDjir02','aniodeclaracion'));
    }

    public function update(Request $request, PlanillaDjir_02 $planillaDjir_02)
    {
        $gen_declaracion_inversion_id=$request->gen_declaracion_inversion_id;
        
        // realizar insert
        $panilla2 = new PlanillaDjir_02;
        $panilla2->gen_declaracion_inversion_id = $request->gen_declaracion_inversion_id ;
        $panilla2->grupo_emp_internacional = $request->grupo_emp_internacional ;
        $panilla2->emp_relacionadas_internacional = (isset($request->emp_relacionadas_internacional))?$request->emp_relacionadas_internacional:'' ;
        $panilla2->casa_matriz = (isset($request->casa_matriz))?$request->casa_matriz:'';
        $panilla2->emp_repre_casa_matriz = (isset($request->emp_repre_casa_matriz))?$request->emp_repre_casa_matriz:'';
        $panilla2->afiliados_internacional = (isset($request->afiliados_internacional))?$request->afiliados_internacional:'';
        $panilla2->sub_emp_extranjero = (isset($request->sub_emp_extranjero))?$request->sub_emp_extranjero:'';
        $panilla2->grupo_emp_nacional = $request->grupo_emp_nacional;
        $panilla2->emp_relacionados_nacional = (isset($request->emp_relacionados_nacional)) ;
        $panilla2->emp_haldig_coorp = (isset($request->emp_haldig_coorp))?$request->emp_haldig_coorp:'';
        $panilla2->afiliados_nacional = (isset($request->afiliados_nacional))?$request->afiliados_nacional:'';
        $panilla2->sub_locales = (isset($request->sub_locales))?$request->sub_locales:'';
        $panilla2->present_doc_repre_legal = (isset($request->present_doc_repre_legal))?$request->present_doc_repre_legal:'';
        $panilla2->num_apostilla = (isset($request->num_apostilla))?$request->num_apostilla:'';
        $panilla2->fecha_apostilla = (isset($request->fecha_apostilla))?$request->fecha_apostilla:'';
        $panilla2->pais = (isset($request->pais))?$request->pais:'';
        $panilla2->estado = (isset($request->estado))?$request->estado:'';
        $panilla2->autoridad_apostilla = (isset($request->autoridad_apostilla))?$request->autoridad_apostilla:'' ;
        $panilla2->cargo = (isset($request->cargo))?$request->cargo:'' ;
        $panilla2->traductor = (isset($request->traductor))?$request->traductor:'' ;
        $panilla2->datos_adicionales = (isset($request->datos_adicionales))?$request->datos_adicionales:'' ;
        $panilla2->ingresos_anual_ult_ejer = $request->ingresos_anual_ult_ejer ;
        $panilla2->egresos_anual_ult_ejer = $request->egresos_anual_ult_ejer ;
        $panilla2->total_balance_ult_ejer = $request->total_balance_ult_ejer ;
        $panilla2->anio_informacion_financiera = $request->anio_informacion_financiera ;
        $panilla2->moneda_extranjera = $request->moneda_extranjera ;
        $panilla2->utilidades_reinvertidas = $request->utilidades_reinvertidas ;
        $panilla2->credito_casa_matriz = $request->credito_casa_matriz ;
        $panilla2->tierras_terrenos = $request->tierras_terrenos ;
        $panilla2->edificios_construcciones = $request->edificios_construcciones ;
        $panilla2->total_costos_declaracion = $request->total_costos_declaracion ;
        $panilla2->maquinarias_eqp_herra = $request->maquinarias_eqp_herra ;
        $panilla2->eqp_transporte = $request->eqp_transporte ;
        $panilla2->muebles_enceres = $request->muebles_enceres ;
        $panilla2->otros_activos_tangibles = $request->otros_activos_tangibles ;
        $panilla2->software = $request->software ;
        $panilla2->derecho_prop_intelectual = $request->derecho_prop_intelectual ;
        $panilla2->contribuciones_tecno = $request->contribuciones_tecno ;
        $panilla2->otros_activos_intangibles = $request->otros_activos_intangibles ;
        $panilla2->tipo_inversion = $request->tipo_inversion ;
        $panilla2->invert_divisa_cambio = $request->invert_divisa_cambio ;
        $panilla2->bienes_cap_fisico_tangibles = $request->bienes_cap_fisico_tangibles ;
        $panilla2->bienes_inmateriales_intangibles = $request->bienes_inmateriales_intangibles ;
        $panilla2->reinversiones_utilidades = $request->reinversiones_utilidades ;
        $panilla2->especifique_otro = $request->especifique_otro ;
        $panilla2->valor_libros = $request->valor_libros ;
        $panilla2->valor_neto_activos = $request->valor_neto_activos ;
        $panilla2->valor_emp_similar = $request->valor_emp_similar ;
        $panilla2->otro_especifique = $request->otro_especifique ;
        $panilla2->total_modalidad_inv = $request->total_modalidad_inv ;
        $panilla2->gen_status_id = 9;
        $panilla2->estado_observacion = (isset($request->estado_observacion))?$request->estado_observacion:0 ;
        $panilla2->descrip_observacion = (isset($request->descrip_observacion))?$request->descrip_observacion:'' ;
        $panilla2->save();

        $updatePanilla2 = PlanillaDjir_02::find($request->planilla2_id);
        $updatePanilla2->actualizado = 1;
        $updatePanilla2->save();
        
        // 
        $planillaDjir01=PlanillaDjir_01::where('gen_declaracion_inversion_id',$gen_declaracion_inversion_id)->first();
        $planillaDjir02=PlanillaDjir_02::where('gen_declaracion_inversion_id',$gen_declaracion_inversion_id)->first();
        $planillaDjir03=PlanillaDjir_03::where('gen_declaracion_inversion_id',$gen_declaracion_inversion_id)->first();
        Alert::success('Registro Planilla 2 Exitoso!')->persistent("Ok"); 
        return view('BandejaInversion.datos_inversion',compact('gen_declaracion_inversion_id','planillaDjir01','planillaDjir02','planillaDjir03'));
    }
}
