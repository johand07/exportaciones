@if(isset($planillaDjir03))
<div class="row">
    <div class="col-md-12">
        <div class="col-md-6">
        </div>
        <div class="col-md-6 text-right">
            <h2>Estatus: <span class="text-success">{{$planillaDjir03->genStatus->nombre_status}}</span></h2>
        </div>
    </div>
</div>
  <!--div class="panel-group"-->
<div class="panel panel primary">
  <div class="panel-heading">
    <div class="panel-body">
      <div class="row">
        <div class="col-md-6">
          <h4><b style="color:#337AB7">Tipo de Persona:</b></h4>
          <b>{{ $solicitudDeclaracion->CatTipoUsuario->nombre}}</b>
        </div>
        <div class="col-md-3"><!-- Traerme la consulta con el numero de solicitud-->
          <h4><b style="color:#337AB7">Nº Solicitud:</b></h4>
          <b>{{$solicitudDeclaracion->num_declaracion}}</b>
        </div>
        <div class="col-md-3"><!-- Traerme la consulta con la fecha de solicitud-->
          <h4><b style="color:#337AB7">Fecha Solicitud:</b></h4>
          <b>{{$solicitudDeclaracion->created_at}}</b>
        </div>
      </div>
      <br><br>
      <div class="row">
        <div class="panel-primary">
          <div class="panel-heading">
            <h4><b>Perspectivas de realizar inversiones con recursos externos en la República Bolivariana de Venezuela</b></h4>
          </div><br>
          <div class="col-md-12">
           Tiene perspectivas de realizar inversiones con recursos externos en la República Bolivariana de Venezuela: <b>{{$planillaDjir03->inversion_recursos_externos ? $planillaDjir03->inversion_recursos_externos : ''}}</b><br><br>

           

          

          @if(!empty($planillaDjir03->inversion_recursos_externos == 'Si'))

              @if($planillaDjir03->inversion_recursos_externos == 'Si' && $planillaDjir03->proyecto_nuevo == 'Si' )
                Aspectos: <b>Proyecto nuevo (Otra empresa)</b><br>
                @elseif($planillaDjir03->inversion_recursos_externos == 'Si' && $planillaDjir03->proyecto_nuevo == 'No' )
                 Aspectos: <b>Ampliación de su actual empresa</b><br> 
              @endif

              @if($planillaDjir03->proyecto_nuevo == 'Si')
                Nombre del Proyecto: <b>{{$planillaDjir03->nombre_proyecto ? $planillaDjir03->nombre_proyecto : ''}}</b><br>
                Ubicación (Estado): <b>{{$planillaDjir03->ubicacion ? $planillaDjir03->ubicacion : ''}}</b><br>
                Origen de los Recursos (País de Origen): <b>{{$planillaDjir03->pais ? $planillaDjir03->pais->dpais : ''}}</b>
              <br>
              @elseif($planillaDjir03->proyecto_nuevo == 'No')
                Bajo que Modalidad Prevé sus Inversiones en el Futuro: <b>{{$planillaDjir03->ampliacion_actual_emp ? $planillaDjir03->ampliacion_actual_emp : ''}}</b><br>

                @if($planillaDjir03->ampliacion_actual_emp == 'Inversión Extranjera Directa')
                  <ul>
                    @if(!empty($planillaDjir03->ampliacion_accionaria_dir))
                    <li>{{$planillaDjir03->ampliacion_accionaria_dir ? $planillaDjir03->ampliacion_accionaria_dir : ''}}</li>
                    @endif

                    @if(!empty($planillaDjir03->utilidad_reinvertida))
                    <li>{{$planillaDjir03->utilidad_reinvertida ? $planillaDjir03->utilidad_reinvertida : ''}}</li>
                    @endif

                    @if(!empty($planillaDjir03->credito_casa_matriz))
                    <li>{{$planillaDjir03->credito_casa_matriz ? $planillaDjir03->credito_casa_matriz : ''}}</li>
                    @endif

                    @if(!empty($planillaDjir03->creditos_terceros))
                    <li>{{$planillaDjir03->creditos_terceros ? $planillaDjir03->creditos_terceros : ''}}</li>
                    @endif

                    @if(!empty($planillaDjir03->otra_dir))
                    <li>{{$planillaDjir03->otra_dir ? $planillaDjir03->otra_dir : ''}}</li>
                    @endif
                  </ul>  

                @elseif($planillaDjir03->ampliacion_actual_emp == 'Inversión de Cartera')
                  <ul>
                    @if(!empty($planillaDjir03->participacion_accionaria_cart))
                    <li>{{$planillaDjir03->participacion_accionaria_cart ? $planillaDjir03->participacion_accionaria_cart : ''}}</li>
                    @endif

                    @if(!empty($planillaDjir03->bonos_pagare))
                    <li>{{$planillaDjir03->bonos_pagare ? $planillaDjir03->bonos_pagare : ''}}</li>
                    @endif

                    @if(!empty($planillaDjir03->otra_cart))
                    <li>{{$planillaDjir03->otra_cart ? $planillaDjir03->otra_cart : ''}}</li>
                    @endif
                  </ul> 
                @endif
              @endif
          @elseif(!empty($planillaDjir03->inversion_recursos_externos == 'No'))
           {{$planillaDjir03->det_invert_recursos ? $planillaDjir03->det_invert_recursos : ''}}
           @endif
          </div>
        </div>
      </div><br><br>
  <div class="row">
    <div class="panel panel-primary">
      <div class="panel-heading"><h4><b>Periodo previsto para efectuar su inversión</b></h4></div>
      <div class="col-md-12"><br>
          <div class="col-md-6">
            <div class="form-group">
              <label><h5><b>Fecha:</b></h5></label>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <p>{{ date ('d-m-Y',strtotime($planillaDjir03->periodo_inversion ? $planillaDjir03->periodo_inversion : ''))}}</p>
            </div>
          </div>  
          </div>
      </div>
    </div>
     <div class="row">
        <div class="panel panel-primary">
          <div class="panel-heading">
            <h4><b>Sector Productivo o Económico</b></h4>
          </div>
        </div>
            <div class="col-md-12">
            @if(!empty($sectorproducteconomico))
              @foreach($sectorproducteconomico as $sector)
                <ul>
                  <li>{{$sector->sectorProdEco ? $sector->sectorProdEco->nombre_sector : ''}}</li>
                </ul>        
              @endforeach
            @else
            @endif
            </div>
      </div>
      <br>

      <div class="row">
        <div class="panel-primary">
          <div class="panel-heading">
            <h4><b>Destino de la Inversión</b></h4>
          </div><br>
          <div class="col-md-12">
          @if(!empty($destinoInversion))
            @foreach($destinoInversion as $destino)
              <ul>
                <li>{{$destino->catDestInversion ? $destino->catDestInversion->nombre_destino : ''}}</li>
              </ul>        
          @endforeach
          @else
          @endif
        </div>
        </div>
      </div>
      <br>
      <br>

      <br><br>
{{Form::model($solicitudDeclaracion,['route'=>['AnalistDeclaracionInversion.update',$solicitudDeclaracion->id],'method'=>'PATCH','id'=>'formanalistaplanilla3'])}}
      <div class="row">
        <div class="col-md-12">
          <div class="form-group">
          {{Form::hidden('planillaDec',3)}}
            <label>Estatus de Planilla</label><br>
            {!!Form::select('gen_status_id',$estatusplanilla3,null,['placeholder'=>'Seleccione','class'=>'form-control','id'=>'gen_status_id_PDIJ03','onchange'=>'validarObsPDIJ03()'])!!}
          </div>
        </div>
      </div>
  <br>
  <div class="row" id="observacionplanilla3" style="display: none;">
    <b><u>OBSERVACIONES:</u></b>
    <textarea rows="4"  class="form-control text-justify" name="descrip_observacion" id="descrip_observacionPDIJ03" style="font-weight:bold; text-transform: capitalize;" ></textarea>
  </div>
    <div class="row text-center"><br><br>
            <a class="btn btn-primary" href="{{url('AnalistaInversion/AnalistDeclaracionInversion')}}">Cancelar</a>
         <input type="submit" name="" class="btn btn-danger center" value="Guardar Observacion" onclick="return enviarplanilla3 ()"> 
    </div>
{{Form::close()}}
    </div>
  </div>
</div><!--Panel Primary-->
<!--/div--><!--Panel Group-->

@else
   <div class="text-center text-warning"> No se han cargado datos para la panilla Djir03 </div>
@endif


<script>
  

function validarObsPDIJ03 (){
  var estatus=$('#gen_status_id_PDIJ03').val();
    //alert(estatus);
    if(estatus == 11 || estatus== 13 || estatus==15) {
    
      $('#observacionplanilla3').show('show');
      $('#descrip_observacionPDIJ03').prop("required", true);
    }else{
      $('#observacionplanilla3').hide('slow');
      $('#descrip_observacionPDIJ03').prop('required',false);
        $('#descrip_observacionPDIJ03').removeAttr("required");
    
    }
}

</script>