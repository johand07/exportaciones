<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PermisoEspecialBovino extends Model
{
    protected $table = 'permiso_especial_bovino';

    protected $fillable = [

        'gen_usuario_id',
        'num_autorizacion',
        'codigo_arancel',
        'descripcion_arancel',
        'volumen',
        'puerto_salida',
        'puerto_entrada',
        'num_proforma',
        'fembarque_bovino',
        'fvalido',
        'bactivo'

    ];

    protected $date =['created_at','updated_at'];

    public function usuario(){

        return $this->hasOne('App\Models\GenUsuario','id','gen_usuario_id');

    }
}
