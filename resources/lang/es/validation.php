<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => 'El campo :attribute debe ser aceptado.',
    'active_url'           => 'El campo :attribute no es una URL válida.',
    'after'                => 'El campo :attribute debe ser una fecha posterior a :date.',
    'after_or_equal'       => 'El campo :attribute debe ser una fecha posterior o igual a :date.',
    'alpha'                => 'El campo :attribute sólo puede contener letras.',
    'alpha_spaces'        => 'El campo :attribute sólo puede contener letras y números.',
    'alpha_dash'           => 'El campo :attribute sólo puede contener letras, números y guiones (a-z, 0-9, -_).',
    'alpha_num'            => 'El campo :attribute sólo puede contener letras y números.',
    'array'                => 'El campo :attribute debe ser un array.',
    'before'               => 'El campo :attribute debe ser una fecha anterior a :date.',
    'before_or_equal'      => 'El campo :attribute debe ser una fecha anterior o igual a :date.',
    'between'              => [
        'numeric' => 'El campo :attribute debe ser un valor entre :min y :max.',
        'file'    => 'El archivo :attribute debe pesar entre :min y :max kilobytes.',
        'string'  => 'El campo :attribute debe contener entre :min y :max caracteres.',
        'array'   => 'El campo :attribute debe contener entre :min y :max elementos.',
    ],
    'boolean'              => 'El campo :attribute debe ser verdadero o falso.',
    'confirmed'            => 'El campo confirmación de :attribute no coincide.',
    'date'                 => 'El campo :attribute no corresponde con una fecha válida.',
    'date_format'          => 'El campo :attribute no corresponde con el formato de fecha :format.',
    'different'            => 'Los campos :attribute y :other deben ser diferentes.',
    'digits'               => 'El campo :attribute debe ser un número de :digits dígitos.',
    'digits_between'       => 'El campo :attribute debe contener entre :min y :max dígitos.',
    'dimensions'           => 'El campo :attribute tiene dimensiones inválidas.',
    'distinct'             => 'El campo :attribute tiene un valor duplicado.',
    'email'                => 'El campo :attribute debe ser una dirección de correo válida.',
    'exists'               => 'El campo :attribute seleccionado no existe.',
    'file'                 => 'El campo :attribute debe ser un archivo.',
    'filled'               => 'El campo :attribute debe tener algún valor.',
    'image'                => 'El campo :attribute debe ser una imagen.',
    'in'                   => 'El campo :attribute es inválido.',
    'in_array'             => 'El campo :attribute no existe en :other.',
    'integer'              => 'El campo :attribute debe ser un número entero.',
    'ip'                   => 'El campo :attribute debe ser una dirección IP válida.',
    'ipv4'                 => 'El campo :attribute debe ser una dirección IPv4 válida.',
    'ipv6'                 => 'El campo :attribute debe ser una dirección IPv6 válida.',
    'json'                 => 'El campo :attribute debe ser una cadena de texto JSON válida.',
    'max'                  => [
        'numeric' => 'El campo :attribute no debe ser mayor a :max.',
        'file'    => 'El archivo :attribute no debe pesar más de :max kilobytes.',
        'string'  => 'El campo :attribute no debe contener más de :max caracteres.',
        'array'   => 'El campo :attribute no debe contener más de :max.',
    ],
    'mimes'                => 'El campo :attribute debe ser un archivo de tipo :values.',
    'mimetypes'            => 'El campo :attribute debe ser un archivo de tipo :values.',
    'min'                  => [
        'numeric' => 'El campo :attribute debe tener al menos :min.',
        'file'    => 'El archivo :attribute debe pesar al menos :min kilobytes.',
        'string'  => 'El campo :attribute debe contener al menos :min caracteres.',
        'array'   => 'El campo :attribute debe contener al menos :min elementos.',
    ],
    'not_in'               => 'El campo :attribute seleccionado es inválido.',
    'not_regex'            => 'El formato del campo :attribute es inválido.',
	'numeric'              => 'El campo :attribute debe ser un número.',
    'present'              => 'El campo :attribute debe estar presente.',
    'regex'                => 'El formato del campo :attribute es inválido.',
    'required'             => 'El campo :attribute es obligatorio.',
    'required_if'          => 'El campo :attribute es obligatorio cuando el campo :other es :value.',

    'required_unless'      => 'El campo :attribute es requerido a menos que :other se encuentre en :values.',
    'required_with'        => 'El campo :attribute es obligatorio cuando :values está presente.',
    'required_with_all'    => 'El campo :attribute es obligatorio cuando :values está presente.',
    'required_without'     => 'El campo :attribute es obligatorio cuando :values no está presente.',
    'required_without_all' => 'El campo :attribute es obligatorio cuando ninguno de los campos :values está presente.',
    'same'                 => 'Los campos :attribute y :other deben coincidir.',
    'size'                 => [
        'numeric' => 'El campo :attribute debe ser :size.',
        'file'    => 'El archivo :attribute debe pesar :size kilobytes.',
        'string'  => 'El campo :attribute debe contener :size caracteres.',
        'array'   => 'El campo :attribute debe contener :size elementos.',
    ],
    'string'               => 'El campo :attribute debe ser una cadena de caracteres.',
    'timezone'             => 'El campo :attribute debe contener una zona válida.',
    'unique'               => 'El valor del campo :attribute ya está en uso.',
    'uploaded'             => 'El campo :attribute falló al subir.',
    'url'                  => 'El formato del campo :attribute es inválido.',


    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',

        ],

        'email'=> [
        'unique'=>'Este email ya se encuentra registrado.',

         ],


         
         /*Mensajes para los certificados de Origen*/

         'cantidad_segun_factura' => [
            'cantidad_segun_factura' => 'El Campo Cantidad Según Factura es obligatorio.',

        ],


        'numero_factura' => [
            'required_if' => 'EL Campo Número Factura es obligatorio. <br>',

        ],

        'f_fac_certificado' => [
            'required_if' => 'El Campo Fecha Factura es obligatorio.',

        ],

        'razon_social_productor' => [
            'required_if' => 'El Campo Nombre o Razón Social Productor es obligatorio.',

        ],

       
        'direccion_productor' => [
            'required_if' => 'El Campo Direccion Productor es obligatorio.',

        ],

        'rif_productor' => [
            'required_if' => 'El Campo Rif Productor es obligatorio.',

        ],


        'rif_importador' => [
            'required_if' => 'El Campo Rif Importador es obligatorio.',

        ],


        'fecha_emision_exportador' => [
            'required_if' => 'El Campo Fecha Emision Exportador es obligatorio.',

        ],


        'razon_social_importador' => [
            'required_if' => 'El Campo Razon Social Importador es obligatorio.',

        ],


        'direccion_importador' => [
            'required_if' => 'El Campo Dirección Importador es obligatorio.',

        ],


        'pais_ciudad_importador' => [
            'required_if' => 'El Campo Pais/Ciudad Importadores es obligatorio.',

        ],


         'email_importador' => [
            'required_if' => 'El Campo Email Importadores es obligatorio.',

        ],

           'telefono' => [
            'required_if' => 'El Campo Teléfono es obligatorio.',

        ],






         'email_seg'=> [
         'unique'=>'Este email ya se encuentra registrado.'
          ],

         'password_confirm'=> [
         'same'=>'Passwords incorrecto debe coincidir.'
       ],

         'cantidad_producto.*'=>[

          'required'=>'Cantidad es requerido',
          'numeric'=>'Cantidad debe ser numérico'
        ],

        'cantidad.*'=>[

         'required'=>'Cantidad es requerido',
         'numeric'=>'Cantidad debe ser numérico'
       ],

        'codigo_arancel.*'=>[

         'required'=>'Codigo Arancelario es requerido',
         'numeric'=>'Código de Arancel debe ser numérico'
       ],
       'codigo_arancel_c.*'=>[

        'required'=>'Codigo Arancelario es requerido',
        'numeric'=>'Código de Arancel debe ser numérico'
      ],
       'idPaises.*'=>[
            'required'=>'Paises son Requeridos',
            'min_items_coma' => 'Debe indicar como minino :min paises destino'
        ],
       
      'descripcion_comercial_c.*'=>[

        'required'=>'Descripción Comercial es requerido',
      ],

      'descripcion_arancelaria.*'=>[

       'required'=>'Descripción Arancelaria requerido',
     ],
     'descripcion_arancelaria_C.*'=>[

        'required'=>'Descripción Arancelaria requerido',
      ],

     'precio_producto.*'=>[

      'required'=>'Precio es requerido',
      'numeric'=>'Precio debe ser númerico'
        ],

        'unidad_medida_id.*'=>[

        'required'=>'Unidad de Medida es requerido',
        ],

        'plazo_pago'=>[

         'required_if'=>'N° Días plazo es requerido si forma de pago es Crédito.'
        ],

        'especif_instrumento'=>[

        'required_if'=>'Especifique es requerido si Tipo de Instrumento es Carta de Crédito.'
        ],

        'especif_pago'=>[

        'required_with'=>'Especifique es obligatorio.'
        ],

        'numero_factura'=>[

        'unique'=>'Ya existe este número de factura.'
        ],

        'fecha_estimada_pago'=>[

        'required_if'=>'El campo Fecha Estimada de Pago es requerido.'
        ],

        'tipo_plazo'=>[

        'required_if'=>'Forma de Cŕedito es requerido si forma de pago es Crédito.'
        ],

        'captcha'=>[

        'captcha'=>'El captcha ingresado es incorrecto.'
        ],

        'gen_dua_id'=>[

        'required_if'=>'El campo Número de Dua es requerido.'
        ],




       'gen_nota_credito_id' => [
                'required_if' => 'El campo Número de Nota de Crédito no puede estar vacio.',
            ],

        'gen_anticipo_id'  => [
                'required_if' => 'El campo Número de Anticipo no puede estar vacio.',
            ],

        'financiamiento'  => [
                'required_if' => 'El campo Financiamiento Es Obligatorio',
            ],

        'cat_regimen_especial_id'  => [
                'required_if' => 'El campo Régimen Especial no puede estar vacio.',
            ],

        'entidad_financiera'  => [
                'required_if' => 'El campo Entidad Bancaria no puede estar vacio.',
            ],

        'tipo_instrumento_id'  => [
                'required_if' => 'El campo Tipo Instrumento de Crédito no puede estar vacio.',
            ],

        'nro_contrato'   => [
                'required_if' => 'El campo Número de Contrato no puede estar vacio.',
            ],

        'fapro_contrato' => [
                'required_if' => 'El campo Fecha de Aprobación del Contrato no puede estar vacio.',
            ],

        'fvenci_contrato' => [
                'required_if' => 'El campo Fecha de Vencimiento del Contrato no puede estar vacio.',
            ],

        'monto_apro_contrato' => [
                'required_if' => 'El campo Monto Aprobado no puede estar vacio.',
            ],

        'num_cuota_contrato' => [
                'required_if' => 'El campo Número de Cuotas no puede estar vacio.',
            ],

        'monto_amort_contrato' => [
                'required_if' => 'El campo Monto Amortizado no puede estar vacio.',
            ],

        'num_cuota_amortizado' => [
                'required_if' => 'El campo Número de Couta Amortizada no puede estar vacio.',
            ],

        'numero_nota_credito' => [
                'unique' => 'Este Número de Nota de Crédito ya fue registrado.',
            ],

      'consig_tecno' => [
                    'required_if' => 'Debe seleccionar un Consignatario .',
                ],

       'opcion_tecnologia' => [
                    'required_if' => 'Debe seleccionar una opción tecnológica .',
                ],


       'descripcion' => [
                    'required_if' => 'El campo observaciones es requerido .',
                ],

       'produc_tecno' => [
                    'required' => 'El campo Exporta Productos de Servicios y/o Tecnología es requerido .',
                ],

        'bol.*'=>[
                    'required' => 'El campo criterio Bolivia es requerido.',
                ],
        'col.*'=>[
                    'required' => 'El campo criterio Colombia es requerido.',
                ],

        'ecu.*'=>[
                    'required' => 'El campo criterio Ecuador es requerido.',
                ],
        'per.*'=>[
                    'required' => 'El campo criterio Peru es requerido.',
                ],
        'cub.*'=>[
                    'required' => 'El campo criterio Cuba es requerido.',
                ],
        'ald.*'=>[
                    'required' => 'El campo criterio Aladi es requerido.',
                ],
        'arg.*'=>[
                    'required' => 'El campo criterio Argentina es requerido.',
                ],
        'bra.*'=>[
                    'required' => 'El campo criterio Brasil es requerido.',
                ],
        'par.*'=>[
                    'required' => 'El campo criterio Paraguay requerido.',
                ],
        'uru.*'=>[
                    'required' => 'El campo criterio Uruguay es requerido.',
                ],
        'usa.*'=>[
                    'required' => 'El campo criterio Estados Unidos es requerido.',
                ],
        'ue.*'=>[
                    'required' => 'El campo criterio Union Europea es requerido.',
                ],
        'cnd.*'=>[
                    'required' => 'El campo criterio Canada es requerido.',
                ],
        'tp.*'=>[
                    'required' => 'El campo criterio Terceros Paises es requerido.',
                ],
        'rif.*'=>[
            'required' => 'El campo RIF es requerido.',
        ],
        'razon_social.*'=>[
            'required' => 'El campo Razon Social es requerido.',
        ],
        'siglas.*'=>[
            'required' => 'El campo Siglas es requerido.',
        ],

        'cat_banco_admin_id'=>[
            'required' => 'El campo Banco Receptor es requerido.',
        ],
        'gen_operador_cambiario_id'=>[
            'required' => 'El campo Banco Emisor es requerido.',
        ],
        'num_referencia'=>[
            'required' => 'El campo Numero de Referencia es requerido.',
        ],
        'fecha_reporte' =>[
            'required' => 'El campo Fecha de reporte es requerido.',
        ],

        'ruta_doc'=>[
            'required' => 'El campo Doc Soporte es obligatorio.',
        ],

        'gen_cert_reg_seniat_id'   =>[     
            'required' => 'Debes seleccionar al menos un aspecto',
        ],

    ],



    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [

    /****************  Tabla Usuario **********************************/

      'email'=>'Email',
      'email_seg'=>'Email segundario',
      'password'=>'Password',
      'password_confirm'=>'Confirmar Password',
      'respuesta_seg'=>'Respuesta',

      /***************  Tabla gen_factura ******************************/
      'numero_factura'=>'Número de Factura',
      'fecha_factura'=>'Fecha',
      'gen_consignatario_id'=>'Consignatario',
      'gen_divisa_id'=>'Divisa',
      'forma_pago_id'=>'Forma de Pago',
      'tipo_instrumento_id'=>'Tipo de Instrumento',
      'incoterm_id'=>'Incoterm',
      'monto_fob'=>'Monto FOB',
      'monto_flete'=>'Monto Flete',
      'monto_seguro'=>'Monto Seguro',
      'otro_monto'=>'Otro Monto',
      'monto_total'=>'Monto Total',
      'gen_dua_id'=>'Número de Dua',

       /***************  Tabla Consignatario******************************/
      'cat_tipo_cartera_id'=>'Cartera Comprador / Proveedor',
      'rif_empresa'=>'Rif Empresa',
      'nombre_consignatario'=>'Nombre o Razón Social',
      'perso_contac'=>'Persona de Contacto',
      'cargo_contac'=>'Cargo de la Persona de Contacto',
      'pais_id'=>'Pais',
      'cat_tipo_convenio_id'=>'Tipo de Convenio',
      'ciudad_consignatario'=>'Ciudad',
      'direccion'=>'Dirección',
      'telefono_consignatario'=>'Teléfono de Contacto',
      'correo'=>'Correo Electrónico',


       /***************  Tabla Permiso******************************/
      'tipo_permiso_id'=>'Tipo de Permiso',
      'numero_permiso'=>'Número de Permiso',
      'fecha_permiso'=>'Fecha de Permiso',
      'fecha_aprobacion'=>'Fecha Aprobación de Permiso',
      'gen_aduana_salida_id'=>'Aduana de Salida',
      'pais_id'=>'País Destino',

      /***************  Tabla gen_agente_aduanal******************************/
      'nombre_agente'=>'Nombre o Razón Social',
      'rif_agente'=>'Rif',
      'numero_registro'=>'Número de registro',
      'ciudad_agente'=>'Ciudad',
      'telefono_agente'=>'Teléfono',



      /*********** tabla cat_preguntas_seg ********************************/


        'email'=>'Correo Electrónico',
        'preg_anterior'=>'Pregunta anterior',

        //'pregunta_alter'=>'Pregunta Alternativa',
        'respuesta_seg' =>'Respuesta',

      /***************** tabla gen_usuario->Cambio de Contraseña***************************/
      'email'           =>'Correo Eléctronico',
      'password'        =>'Contraseña',
      'password_confirm'=>'Confirmación',
      'codigo_arancel.*'=>'Codigo de Arancel',
      'idPaises.*'=>'Paises',



      /***************** tabla circuns_judicial***************************/


      /***************** tabla det_prod_factura***************************/


      /***************** tabla det_usuario*********************************/
       'cod_empresa'=>'Codigo de la empresa',
       'razon_social'=>'Nombre o Razón Social',
       'siglas'=>'Siglas',
       'rif'=>'Rif',
       'pagina_web'=>'Pagina Web',
       'id_gen_sector'=>'Sector',
       'gen_actividad_eco_id'=>'Actividad Económica',
       'gen_tipo_empresa_id'=>'Tipo de Empresa',
       'export'=>'Tipo de Actividad Economica',
       'pais_id'=>'Pais',
       'estado_id'=>'Estado',
       'municipio_id'=>'Municipio',
       'parroquia_id'=>'Parroquia',
       'direccion'=>'Dirección',
       'telefono_local'=>'Teléfono Local',
       'telefono_movil'=>'Teléfono Celular',
       'circuns_judicial_id'=>'Registro de Circunscripción Judicial',
       'num_registro'=>'Número de Documento ',
       'tomo'=>' Numero de Tomo',
       'folio'=>' Numero de Folio',
       'oficina'=>'Oficina',
       'f_registro_mer'=>'Fecha de Constitucion',
       //'nombre_presid'=>'Nombre del Presidente',
       //'apellido_presid'=>'Apellido del Presidente',
       //'ci_presid'=>'Cédula del Presidente',
       //'cargo_presid'=>'Cargo del Presidente',
       'nombre_repre'=>'Nombre del Representante Legal',
       'apellido_repre'=>'Apellido del Representante Legal',
       'ci_repre'=>'Cédula del Representante Legal',
       'cargo_repre'=>'Cargo',
       'correo_repre'=>'Correo del Representante Legal',
       'accionista'=>'Posée Accionista Si o No',
       'nombre_accionista'=>'Nombre del Accionista',
       'apellido_accionista'=>'Apellido del Accionista',
       'cedula_accionista'=>'Cédula del Accionista',
       'cargo_accionista'=>'Cargo del Accionista',
       'telefono_accionista'=>'Telefono del Accionista',
       'nacionalidad_accionista'=>'Nacionalidad del Accionista',
       'participacion_accionista'=>'Participacion del Accionista',
       'correo_accionista'=>'Correo Accionista',
       'prod_tecnologia'=>'Exporta Productos de Servicios y/o Tecnología',
       'tipo_arancel'=>'Tipo de Arancel',
       'cod_arancel'=>'Tipo de Arancel',
       'descrip_arancel'=>'Descripción Arancelaria',
       'descrip_comercial'=>'Descripción Comercial',
       'gen_unidad_medida_id'=>'Unidad de Medida',
       'cap_opera_anual'=>'Capacidad Operativa Anual',
       'cap_insta_anual'=>'Capacidad Instalada Anual',
       'cap_alamcenamiento'=>'Capacidad Almacenada',
       'cap_exportacion'=>'Capacidad de Exportacion',
       'correo'=>'Correo Electronico',
       'correo_sec'=>'Correo Alternativo',

      /***************** tabla estado***************************/

       /*****************Tabla Gen Solictud***************************/

        'fsolicitud'=>'Fecha de Solicitud',
        'tipo_solicitud_id'=>'Tipo de Solicitud',

      /***************** tabla forma de pago***************************/


      /***************** tabla gen_actividad_eco***************************/


      /***************** tabla gen_aduana_salida***************************/


      /***************** tabla gen_agente_aduanal***************************/



      /***************** tabla gen_arancel_mercosur***************************/


      /***************** tabla gen_arancel_nandina***************************/


      /***************** tabla gen_divisa***************************/


      /***************** tabla gen_dua***************************/
          'numero_dua'=>'Número DUA',
          'numero_referencia'=>'Número Referencia',
          'gen_agente_aduana_id'=>'Agente Aduanal',
          'gen_transporte_id'=>'Modalidad de Transporte',
          'gen_aduana_salida_id'=>'Aduana Salida',
          'lugar_salida'=>'Lugar Salida',
          'aduana_llegada'=>'Aduana llegada',
          'lugar_llegada'=>'Lugar llegada',
          'fregistro_dua_aduana'=>'Fecha registro DUA en aduanas',
          'fembarque'=>'Fecha Embarque',
          'farribo'=>'Fecha Estimada arribo',

      /***************** tabla gen_funciones***************************/


      /***************** tabla gen_juridico_cencoex***************************/


      /***************** tabla gen_juridico_seniat***************************/


      /***************** tabla gen_nota_credito***************************/

           'gen_consignatario_id'=>'Consignatario',
           'numero_nota_credito'=>'Número de Nota de Crédito',
           'femision'=>'Fecha de Emisión',
           'concepto_nota_credito'=>'Concepto de la Nota de Crédito',
           'monto_nota_credito'=>'Monto de Nota Crédito',
           'gen_factura_id'=>'Número de Factura Asociada',
           'justificacion'=>'Justificación de la Nota de Crédito',


      /***************** tabla nota_debito*************************/



            'gen_factura_id'=>'Número de factura Asociada',
            'num_nota_debito'=>'Número nota de debito',
            'fecha_emision'=>'Fecha de Emisión',
            'er'=>'ER',
            'monto_nota_debito'=>'Monto nota de Débito',
            'descrip_nota_debito'=>'Descripción de la Nota de Débito',


 /***************** tabla gen_anticipo***************************/

        'nro_anticipo'=>'Número de Anticipo',
        'fecha_anticipo'=>'Fecha de Anticipo',
        'gen_consignatario_id'=>'Consignatario',
        'gen_divisa_id'=>'Divisa',
        'nro_doc_comercial'=>'Número Documento Comercial',
        'monto_anticipo'=>'Monto Anticipo',
        'monto_total_ant'=>'Monto Total Anticipo',
        'observaciones'=>'Observaciones',

      /***************** tabla gen_roles***************************/


      /***************** tabla gen_roles_funciones***************************/


      /***************** tabla gen_sector***************************/


      /***************** tabla gen_estatus***************************/


      /***************** tabla gen_tipo_empresa***************************/


      /***************** tabla gen_tipo_usuario***************************/

      /***************** tabla gen_transporte***************************/


      /***************** tabla gen_unidad_medida***************************/


      /***************** tabla gen_usuario_roles***************************/


      /***************** tabla incoterm***************************/


      /***************** tabla municipio***************************/


      /***************** tabla pais***************************/


      /***************** tabla parroquia***************************/


      /***************** tabla gen_dvd_solicitud***************************/

        'gen_operador_cambiario_id'=>'Operador Cambiario',
        'fdisponibilidad'=>'Fecha Disponibilidad de las Divisas',
        'gen_factura_id'=>'Numero de Factura',
        'mpercibido'=>'Monto Percibido de la Venta',
        'mvendido'=>'Monto Venta BCV',
        'mretencion'=>'Monto Retenido',
        'descripcion'=>'Observaciones',


      /***************** tabla Factura Solicitud                 ********************************/

      'gen_factura_id'=>'Número de Factura',
      'cat_regimen_export_id'=>'Régimen de Exportación',
      'nota_credito'=>'Nota de Crédito',
      'anticipo'=>'Anticipo',
      'envio_muestra'=>'Envio de Muestra',


      'gen_nota_credito_id'=>'Número nota de Crédito',

      'gen_anticipo_id'=>'Número de Anticipo',

      'financiamiento'=>'Financiamiento',
      'cat_regimen_especial_id'=>'Régimen de Exportación',
      'especifique_regimen'=>'Especifique Régimen',
      'tipo_instrumento_id'=>'Tipo Instrumento de Cŕedito',
      'entidad_financiera'=>'Entidad Bancaria',
      'nro_contrato'=>'Número de Contrato',
      'fapro_contrato'=>'Fecha de Aprobación del Contrato',
      'fvenci_contrato'=>'Fecha de Vencimiento del Contrato',
      'monto_apro_contrato'=>'Monto Aprobado',
      'num_cuota_contrato'=>'Número de Cuotas',
      'monto_amort_contrato'=>'Monto Amortizado',
      'num_cuota_amortizado'=>'Número de Cuota Amortizada',


      /*Palnilla2DJO*/

        'descripcion_comercial'=>'Denominación Comercial',
        'descripcion_arancel'=>'Descripción Arancelaria',
        'codigo_arancel'=>'Código Arancelario',
        'unidad_medida'=>'Unidad de medida',
        'uso_aplicacion'=>'Uso y aplicación',
        'descripcion_arancelaria_importado.*'=>'Materiales Importados ',
        'codigo_arancel_importado.*'=>'Código Arancelario',
        'pais_importado.*'=>'País de Origen',
        'monto_incidencia_importado.*'=>'Incidencia sobre costo total de Materiales importados',
        'descripcion_arancelaria_nacional.*'=>'Materiales Nacionales',
        'codigo_arancel_nacional.*'=>'Código Arancelario',
        'nombre_producto_nacional.*'=>'Nombre del Productor Nacional',
        'nombre_proveedor_nacional.*'=>'Nombre del Proveedor',
        'monto_incidencia_nacional.*'=>'Incidencia sobre costo total De Materiales Nacionales',

      /*Palnilla3DJO*/

         'descripcion_comercial'=>'Denominación Comercial',
         'divisa'=>'Moneda',

      /*gen_sol_inversionista*/

          'razon_social'=>'Nombre o razón social',
          'pais_id'=>'País de Origen',
          'direccion'=>'Dirrección de residencia',
          'telefono_movil'=>'Número telefónico',
          'correo'=>'Correo electrónico',
          'pagina_web'=>'Página Web',   
          'nombre_repre'=>'Nombre del representante legal de la empresa extranjera',   
          'apellido_repre'=>'Apellido del representante legal de la empresa extranjera',   
          'ci_repre'=>'Número de pasaporte ',
          'letravisa' =>'Número de pasaporte ',
          'cat_tipo_visa_id'=>'Tipo de visa',   
          'nombre_accionista'=>'Nombre del accionista',   
          'apellido_accionista'=>'Apellido del accionista',   
          'cedula_accionista'=>'Rif del accionista',    
          'cargo_accionista'=>'Cargo del accionista',
          'telefono_accionista'=>'Telefono del accionista',  
          'nacionalidad_accionista'=>'Nacionalidad del accionista',   
          'participacion_accionista'=>'% Participación',   
          'correo_accionista'=>'Correo del accionista', 


            'file_1'                    => 'Archivo 1',
            'gen_tipo_certificado_id'   => 'Tipo de Certificado',
            'pais_exportador'           => 'Pais Expotador',
            'pais_importador'           => 'Pais Importador',
            'cantidad_segun_factura'    => 'Cantidad segun la Factura',
            'descripcion_comercial'    => 'Cantidad segun la Factura',
            'numero_factura'            => 'Número de factura',
            'f_fac_certificado'         => 'Fecha de Factura',
            'fecha_emision_exportador'  => 'Fecha de Emisión',
            'razon_social_importador'   => 'Razon Social del Importador',
            'direccion_importador'      => 'Dirección del Importador',
            'pais_ciudad_importador'    => 'Pais o Ciudad del Importador',
            'email_importador'          => 'Correo del Impotador',
            'telefono'                  => 'Telefono',


            'file_2'                    => 'Archivo 2',
            'file_3'                    => 'Archivo 3',
            'file_4'                    => 'Archivo 4',
            'file_5'                    => 'Archivo 5',
            'file_6'                    => 'Archivo 6',
            'file_7'                    => 'Archivo 7',
            'file_8'                    => 'Archivo 8',
            'file_9'                    => 'Archivo 9',
            'file_10'                    => 'Archivo 10',
            'file_11'                    => 'Archivo 11',
            'file_12'                    => 'Archivo 12',
            'file_13'                    => 'Archivo 13',
            'file_14'                    => 'Archivo 14',
            'file_15'                    => 'Archivo 15',
            'file_16'                    => 'Archivo 16',
            'file_17'                    => 'Archivo 17',
            'file_18'                    => 'Archivo 18',
            'file_19'                    => 'Archivo 19',
            'file_20'                    => 'Archivo 20',



            //doc banco de venezuela
            //
          // 'ruta_doc_dvd' => 'de los archivos es Obligatorio cuando el Operador Cambiario es Banco de Venezuela',


    ],

];
