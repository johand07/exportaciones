<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDocsSoporteToDetUsuarioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('det_usuario', function (Blueprint $table) {
            
            $table->string('ruta_rif', 200)->nullable()->after('ruta_doc');
            $table->string('ruta_reg_merc', 500)->nullable()->after('ruta_rif');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('det_usuario', function (Blueprint $table) {
            //
            $table->dropColumn('ruta_rif');
            $table->dropColumn('ruta_reg_merc');
            
        });
    }
}
