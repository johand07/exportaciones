@extends('templates/layoutlte_coordinador_djo')

@section('content')

<div class="row">
  
  <div class="col-md-12">
    <div class="row">
      <div class="col-md-10"></div>
      <div class="col-md-2">
        <a class="btn btn-primary" href="{{route('AdministradorDjo.create')}}">
          <span class="glyphicon glyphicon-file" title="Agregar" aria-hidden="true"></span>
           Registro de Usuarios
        </a>
      </div>
    </div>
    <div class="row" style="margin-top: 1.5rem;">
    <table id="listaUsuarios" class="table table-striped table-bordered" style="width:100%">
            <thead>
                <tr>
                  <th>Email</th>
                  <th>Email Secundario</th>
                  <th>Tipo de Usuario</th>
                  <th>Estatus</th>
                  <th>Fecha de Creación</th>
                  <th>Fecha de Actualización</th>
                  <th>Acciones</th>
                </tr>
            </thead>
            <tbody>
              @foreach($users as $user)
                <tr> 
                    <td>{{$user->email}}</td>     
                    <td>{{$user->email_seg}}</td>       
                    <td>{{$user->tipoUsuario->nombre_tipo_usu}}</td>
                    <td>{{$user->estatus->nombre_status}}</td> 
                    <td>{{$user->created_at}}</td> 
                    <td>{{$user->updated_at}}</td> 
                    <td class="col-md-2 text-center">
                    <!--Ruta y metodo para Editar-->
                    <a href="{{route('AdministradorDjo.edit',$user->id)}}" title="Actualizar" class="btn btn-primary btn-sm"><i class="glyphicon glyphicon-edit"></i></a>
                    <!--Boton eliminar-->
                    <input id="token" type="hidden" name="_token" value="{{ csrf_token() }}">
                    </td>
                    </tr>
              @endforeach

            </tbody>
        </table>
        </div>
  </div>
  
</div>

@stop
