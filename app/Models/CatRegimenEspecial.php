<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CatRegimenEspecial extends Model
{
    protected $table = 'cat_regimen_especial';

    protected $fillable =[

     'id',
     'nombre_especial',
     'bactivo'
    ];
}
