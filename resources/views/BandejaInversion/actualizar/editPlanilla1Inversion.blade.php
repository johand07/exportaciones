@extends('templates/layoutlte')
@section('content')



{{Form::model($planillaDjir01,['route'=>['actualizarPlanilla1Inversion.update',$planillaDjir01->id],'method'=>'PATCH','id'=>'formPlanilla1Inversion', 'enctype' => 'multipart/form-data'])}}

@php 

$contador=count($accionistas); 

function namefile($name){
  $explode=explode("/",$name);
  return $explode[4];

}

@endphp
@if(isset($planillaDjir01->descripcion_observacion) && $planillaDjir01->estado_observacion == 1)
<div class="alert alert-dismissible alert-warning">
  <button type="button" class="close" data-dismiss="alert">&times;</button>
  <h4 class="alert-heading">Observación!</h4>
  <p class="mb-0">{{$planillaDjir01->descripcion_observacion}}</p>
</div>
@endif
{{Form::hidden('contador',$contador)}}
{!! Form::hidden('estado_observacion',null,['class'=>'form-control','id'=>'estado_observacion','onkeypress'=>'return soloNumeros(event)'])!!}
{{Form::hidden('declaracion_old',$planillaDjir01->gen_declaracion_inversion_id)}}
{{Form::hidden('gen_declaracion_inversion_id',$new_gen_declaracion_inversion_id)}}

{{Form::hidden('emp_privada_edit',$planillaDjir01->emp_privada,['class'=>'form-control','id'=>'emp_privada_edit','onkeypress'=>'return soloNumerosyLetras(event)'])}}
{{Form::hidden('emp_publica_edit',$planillaDjir01->emp_publica,['class'=>'form-control','id'=>'emp_publica_edit','onkeypress'=>'return soloNumerosyLetras(event)'])}}
{{Form::hidden('emp_mixto_edit',$planillaDjir01->emp_mixto,['class'=>'form-control','id'=>'emp_mixto_edit','onkeypress'=>'return soloNumerosyLetras(event)'])}}
{{Form::hidden('emp_otro_edit',$planillaDjir01->emp_otro,['class'=>'form-control','id'=>'emp_otro_edit','onkeypress'=>'return soloNumerosyLetras(event)'])}}

<div id="section1">
	<ol class="breadcrumb">
	  <li  class="active" id="paso1">Paso 1</li>
	  <li  class="" id="paso1">Paso 2</li>
	  <li  class="" id="paso1">Paso 3</li>
	  <li  class="" id="paso1">Paso 4</li>

	</ol>
	<div class="panel panel-primary">
      <div class="panel-heading"><h4>Carga de Documentos Requisitos del solicitante</h4></div>
      <div class="panel-body">
      	<h5 style="color: red">Los campos marcados con (*) son Obligatorios. (Formatos permitidos .doc, .docx, .pdf) </h5>
		<div class="row">
			<div  class="col-md-12">
				<div class="col-md-4">
					<tr>
						<td  class="text-center" valign="top">
							<span><b>1.) Antecedentes Documentales.</b>
							</span><br><br><br>
							<span class="btn btn-default btn-file" style="margin: 3px;">
		                        Cargar Archivo <span class="glyphicon glyphicon-file"></span>
		                        {!! Form::file('file_1', array('class' => 'file-input','style'=>'margin:10px;','id'=>'file_1','title'=>'formatos permitidos .doc, .docx, .pdf')) !!}
		                    <input type="hidden" value="{{$documentos->id}}" name="file_id">   
	                      	</span>
						</td>
						<td class="text-center" height="40px"><img id="imgdocumento_file_1" src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px; display: none;"><div id="nombre_file_1"></div>
						</td>
						{{--@if(isset($documentos->file_1))
	                      <div id="imgdocumentoEdit_file_1">
	                      <img src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px;">
	                      {{namefile($documentos->file_1)}}
	                      </div>
                    	@endif--}}
					</tr>
				</div>
				<div class="col-md-4">
					<tr>
						<td  class="text-center" valign="top">
							<span><b>2.) Planilla de Requisitos para el Registro o Actualización de Inversiones R-02.</b>
							</span><br><br>
							<span class="btn btn-default btn-file" style="margin: 3px;">
		                        Cargar Archivo <span class="glyphicon glyphicon-file"></span>
		                        {!! Form::file('file_2', array('class' => 'file-input','style'=>'margin:10px;','id'=>'file_2','title'=>'formatos permitidos .doc, .docx, .pdf')) !!}
	                      	</span>
						</td>
						<td class="text-center" height="40px"><img id="imgdocumento_file_2" src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px; display: none;"><div id="nombre_file_2"></div>
						</td>
						{{--@if(isset($documentos->file_2))
	                      <div id="imgdocumentoEdit_file_2">
	                      <img src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px;">
	                      {{namefile($documentos->file_2)}}
	                      </div>
                    	@endif--}}
						
					</tr>
				</div>
				<div class="col-md-4">
					<tr>
						<td  class="text-center" valign="top">
							<span><b>3.) Documento Apostillado que evidencie la cualidad del Representante Legal de la Persona Juridica Solicitante.</b>
							</span><br><br>
							<span class="btn btn-default btn-file" style="margin: 3px;">
		                        Cargar Archivo <span class="glyphicon glyphicon-file"></span>
		                        {!! Form::file('file_3', array('class' => 'file-input','style'=>'margin:10px;','id'=>'file_3','title'=>'formatos permitidos .doc, .docx, .pdf')) !!}
	                      	</span>
						</td>
						<td class="text-center" height="40px"><img id="imgdocumento_file_3" src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px; display: none;"><div id="nombre_file_3"></div>
						</td>
						{{--@if(isset($documentos->file_3))
	                      <div id="imgdocumentoEdit_file_3">
	                      <img src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px;">
	                      {{namefile($documentos->file_3)}}
	                      </div>
                    	@endif--}}
						
					</tr>
				</div>
			</div>
		</div> 
        <br><br>
        <div class="row">
			<div  class="col-md-12">
				<div class="col-md-4">
					<tr>
						<td  class="text-center" valign="top">
							<span><b>4.) Poder de Autenticacion Debidamente Autenticado. En caso de extranjeros, apostillado y traducido en idioma español</b>
							</span><br><br><br>
							<span class="btn btn-default btn-file" style="margin: 3px;">
		                        Cargar Archivo <span class="glyphicon glyphicon-file"></span>
		                        {!! Form::file('file_4', array('class' => 'file-input','style'=>'margin:10px;','id'=>'file_4','title'=>'formatos permitidos .doc, .docx, .pdf')) !!}
	                      	</span>
						</td>
						<td class="text-center" height="40px"><img id="imgdocumento_file_4" src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px; display: none;"><div id="nombre_file_4"></div>
						</td>
						{{--@if(isset($documentos->file_4))
	                      <div id="imgdocumentoEdit_file_4">
	                      <img src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px;">
	                      {{namefile($documentos->file_4)}}
	                      </div>
                    	@endif--}}
					</tr>
				</div>
				<div class="col-md-4">
					<tr>
						<td  class="text-center" valign="top">
							<span><b>5.) Original y una copia de Documentos que generan la modificación o corrección de la inversión. </b>
							</span><br><br>
							<span class="btn btn-default btn-file" style="margin: 3px;">
		                        Cargar Archivo <span class="glyphicon glyphicon-file"></span>
		                        {!! Form::file('file_5', array('class' => 'file-input','style'=>'margin:10px;','id'=>'file_5','title'=>'formatos permitidos .doc, .docx, .pdf')) !!}
	                      	</span>
						</td>
						<td class="text-center" height="40px"><img id="imgdocumento_file_5" src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px; display: none;"><div id="nombre_file_5"></div>
						</td>
						{{--@if(isset($documentos->file_5))
	                      <div id="imgdocumentoEdit_file_5">
	                      <img src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px;">
	                      {{namefile($documentos->file_5)}}
	                      </div>
                    	@endif--}}
						
					</tr>
				</div>
				<div class="col-md-4">
					<tr>
						<td  class="text-center" valign="top">
							<span><b>6.) Comprobante de pago de trámite por un monto equivalente a 2 petros.</b>
							</span><br><br>
							<span class="btn btn-default btn-file" style="margin: 3px;">
		                        Cargar Archivo <span class="glyphicon glyphicon-file"></span>
		                        {!! Form::file('file_6', array('class' => 'file-input','style'=>'margin:10px;','id'=>'file_6','title'=>'formatos permitidos .doc, .docx, .pdf')) !!}
	                      	</span>
						</td>
						<td class="text-center" height="40px"><img id="imgdocumento_file_6" src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px; display: none;"><div id="nombre_file_6"></div>
						</td>
						{{--@if(isset($documentos->file_6))
	                      <div id="imgdocumentoEdit_file_6">
	                      <img src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px;">
	                      {{namefile($documentos->file_6)}}
	                      </div>
                    	@endif--}}
						
					</tr>
				</div>
			</div>
		</div><br>
	 <!--div class="row">
		<div class="panel panel-primary">
	        <div class="panel-heading"><h4>Carga de Documentos Requisitos de la Empresa Receptora</h4></div>
	  </div>
	    	<h5 style="color: red">&nbsp;&nbsp;&nbsp;&nbsp;Los campos marcados con (*) son Obligatorios. (Formatos permitidos .doc, .docx, .pdf) </h5>
		<div class="row">
			<div  class="col-md-12">
				<div class="col-md-6">
					<tr>
						<td  class="text-center" valign="top">
							<span><b>1.) Original y una (1) copia del documento constitutivo de la empresa receptora de inversión, con sus modificaciones.</b>
							</span><br><br>
							<span class="btn btn-default btn-file" style="margin: 3px;">
		                        Cargar Archivo <span class="glyphicon glyphicon-file"></span>
		                        {!! Form::file('file_7', array('class' => 'file-input','style'=>'margin:10px;','id'=>'file_7','title'=>'formatos permitidos .doc, .docx, .pdf')) !!}
	                      	</span>
						</td>
						<td class="text-center" height="40px"><img id="imgdocumento_file_7" src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px; display: none;"><div id="nombre_file_7"></div>
						</td>
						@if(isset($documentos->file_7))
	                      <div id="imgdocumentoEdit_file_7">
	                      <img src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px;">
	                      {{namefile($documentos->file_7)}}
	                      </div>
                    	@endif
					</tr>
				</div>
				<div class="col-md-6">
					<tr>
						<td  class="text-center" valign="top">
							<span><b>2.) Datos y documentos de identificación del Representante legal de la empresa receptora de inversión.(ej.: documento de identidad, pasaporte, visa, etc).</b>
							</span><br><br>
							<span class="btn btn-default btn-file" style="margin: 3px;">
		                        Cargar Archivo <span class="glyphicon glyphicon-file"></span>
		                        {!! Form::file('file_8', array('class' => 'file-input','style'=>'margin:10px;','id'=>'file_8','title'=>'formatos permitidos .doc, .docx, .pdf')) !!}
	                      	</span>
						</td>
						<td class="text-center" height="40px"><img id="imgdocumento_file_8" src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px; display: none;"><div id="nombre_file_8"></div>
						</td>
						@if(isset($documentos->file_8))
	                      <div id="imgdocumentoEdit_file_8">
	                      <img src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px;">
	                      {{namefile($documentos->file_8)}}
	                      </div>
                    	@endif
						
					</tr>
				</div>
			</div>
		</div> 
        <br><br>
        <div class="row">
			<div  class="col-md-12">
				<div class="col-md-6">
					<tr>
						<td  class="text-center" valign="top">
							<span><b>3.) Una (1) copia del Registro de Información Fiscal, del representante legal de la empresa receptora de inversión.</b>
							</span><br><br>
							<span class="btn btn-default btn-file" style="margin: 3px;">
		                        Cargar Archivo <span class="glyphicon glyphicon-file"></span>
		                        {!! Form::file('file_9', array('class' => 'file-input','style'=>'margin:10px;','id'=>'file_9','title'=>'formatos permitidos .doc, .docx, .pdf')) !!}
	                      	</span>
						</td>
						<td class="text-center" height="40px"><img id="imgdocumento_file_9" src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px; display: none;"><div id="nombre_file_9"></div>
						</td>
						@if(isset($documentos->file_9))
	                      <div id="imgdocumentoEdit_file_9">
	                      <img src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px;">
	                      {{namefile($documentos->file_9)}}
	                      </div>
                    	@endif
					</tr>
				</div>
				<div class="col-md-6">
					<tr>
						<td  class="text-center" valign="top">
							<span><b>4.) Cualquier otro solicitado por la autoridad administrativa.</b>
							</span><br><br>
							<span class="btn btn-default btn-file" style="margin: 3px;">
		                        Cargar Archivo <span class="glyphicon glyphicon-file"></span>
		                        {!! Form::file('file_10', array('class' => 'file-input','style'=>'margin:10px;','id'=>'file_10','title'=>'formatos permitidos .doc, .docx, .pdf')) !!}
	                      	</span>
						</td>
						<td class="text-center" height="40px"><img id="imgdocumento_file_10" src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px; display: none;"><div id="nombre_file_10"></div>
						</td>
						@if(isset($documentos->file_10))
	                      <div id="imgdocumentoEdit_file_10">
	                      <img src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px;">
	                      {{namefile($documentos->file_10)}}
	                      </div>
                    	@endif
						
					</tr>
				</div>
			</div>
		</div> 
	</div-->
		
        <br>
        <div class="row">
          <div class="col-md-12">
            <div class="col-md-4"></div>
            <div class="col-md-4 text-center">
              <div class="form-group">

                <a class="btn btn-primary" href="#" id="seguir1">Siguiente</a>
              </div>
            </div>
          </div>
        </div>
      </div>
   	</div>
</div>

<!------------------------------------------------------------------- -->

<div id="section2" style="display: none;">
    <ol class="breadcrumb">
      <li href="#" vid="paso4">Paso 1</li>
      <li class="active"  id="paso3">Paso 2</li>
      <li  class="" id="paso1">Paso 3</li>
	  	<li  class="" id="paso1">Paso 4</li>
    </ol>
    <div class="panel panel-primary">
      <div class="panel-heading"><h4>Información General</h4></div>
      <div class="panel-body">
      <div class="row">
		<div  class="col-md-12">
			<div class="col-md-6">
				<div class="form-group">
                  {!! Form::label('','Nombre o razón social del solicitante')!!}
                  {!! Form::text('razon_social_solicitante',null,['class'=>'form-control','id'=>'razon_social_solicitante','onkeypress'=>'return soloLetras(event)'])!!}
                </div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
                  {!! Form::label('','N° Registro de Información Fiscal (R.I.F)')!!}
                  {!! Form::text('rif_solicitante',null,['class'=>'form-control','id'=>'rif_solicitante','onkeypress'=>'return soloNumerosyLetras(event)'])!!}
                </div>
			</div>
		</div>
	  </div>
	<div class="row">		
		<div class="panel panel-primary">
		    <div class="panel-heading"><h4>Datos de Ingreso de Inversión Extranjera</h4></div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="col-md-6">
					<div class="form-group">
						{!! Form::label('','Fecha de inicio de operaciones') !!}
						<div class="input-group date">
							{!! Form::text('finicio_op',null,['class'=>'form-control','readonly'=>'readonly','id'=>'finicio_op']) !!}
							<span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						{!! Form::label('','Fecha de ingreso de la Inversión') !!}
						<div class="input-group date">
							{!! Form::text('fingreso_inversion',null,['class'=>'form-control','readonly'=>'readonly','id'=>'fingreso_inversion']) !!}
							<span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="col-md-12">
					<center>{!! Form::label('', 'Tipo de Empresa Receptora de IED') !!}</center>
				    <br>
					<div class="col-md-3">
			       		<div class="form-group">
				       {!! Form::radio('gen_tipo_empresa',2,false,['class'=>'radio-inline','id'=>'emp_privada'])!!}
				       {!! Form::label('Empresa Privada ', 'Empresa Privada ',['class'=>'radio-inline']) !!}
			     		</div>
			     	</div>
			     	<div class="col-md-3">
			       		<div class="form-group">
				        {!! Form::radio('gen_tipo_empresa',1,false,['class'=>'radio-inline','id'=>'emp_publica'])!!}
				        {!! Form::label('Empresa Pública', 'Empresa Pública',['class'=>'radio-inline']) !!}
				       
			     		</div>
			     	</div>
			     	<div class="col-md-3">
			       		<div class="form-group">
				        {!! Form::radio('gen_tipo_empresa',4,false,['class'=>'radio-inline','id'=>'emp_mixto'])!!}
				        {!! Form::label('Empresa Capital mixto', 'Empresa Capital mixto',['class'=>'radio-inline']) !!}
			     		</div>
			     	</div>
			     	<div class="col-md-3">
			       		<div class="form-group">
				        {!! Form::radio('gen_tipo_empresa',5,false,['class'=>'radio-inline','id'=>'emp_otro'])!!}
				        {!! Form::label('Otro','Otro',['class'=>'radio-inline']) !!}
			     		</div>
			     	</div>
   				</div>
			</div>
		</div>
		<div class="row" style="display:none" id="tipo_emp_otro">
			<div class="col-md-12">
				<div class="col-md-12">
		      <div class="form-group">
                	{!! Form::label('','(Indicar):')!!}
               	{!! Form::text('especifique_otro',null,['class'=>'form-control','id'=>'especifique_otro','onkeypress'=>''])!!}
		      </div>
			  </div>
   		</div>
		</div><br>

		<div class="row">
			<div class="col-md-12">
				<div class="col-md-3">
					<div class="form-group">
                 	 {!! Form::label('','Dirección de la empresa:')!!}
                 	 {!! Form::text('direccion_emp',null,['class'=>'form-control','id'=>'direccion_emp','onkeypress'=>''])!!}
                	</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
                 	 {!! Form::label('','Teléfono')!!}
                 	 {!! Form::text('tlf_emp',null,['class'=>'form-control','id'=>'tlf_emp','onkeypress'=>'return soloNumeros(event)'])!!}
                	</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
                 	 {!! Form::label('','Página Web')!!}
                 	 {!! Form::text('pagina_web_emp',null,['class'=>'form-control','id'=>'pagina_web_emp','onkeypress'=>''])!!}
                	</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
                 	 {!! Form::label('','Correo electrónico ')!!}
                 	 {!! Form::text('correo_emp',null,['class'=>'form-control','id'=>'correo_emp','onkeypress'=>''])!!}
                	</div>
				</div>
			</div>
		</div><br>

		<div class="row">
			<div class="col-md-12">
				<div class="col-md-3">
					<div class="form-group">
                 	 {!! Form::label('','RR.SS')!!}
                 	 {!! Form::text('rrss',null,['class'=>'form-control','id'=>'rrss','onkeypress'=>'return soloNumerosyLetras(event)'])!!}
                	</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
                 	 {!! Form::label('','Registro Público')!!}
                 	 {!! Form::text('registro_publico',null,['class'=>'form-control','id'=>'registro_publico','onkeypress'=>'return soloNumerosyLetras(event)'])!!}
                	</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
                 	 	{!! Form::label('','Estado')!!}<br>
             			{!!Form::select('estado_emp',$estados,null,
              			['class'=>'form-control','id'=>'id_estado','placeholder'=>'--Seleccione una Opción--'])!!}
               		</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
	                 	{!! Form::label('','Municipio')!!}<br>
	                 	{!!Form::select('municipio_emp',$estados,null,
              			['class'=>'form-control','id'=>'id_municipio','placeholder'=>'--Seleccione una Opción--'])!!}
                	</div>
				</div>
			</div>
		</div><br>
		<div class="row">
			<div class="col-md-12">
				<div class="col-md-3">
					<div class="form-group">
                 	 {!! Form::label('','Número')!!}
                 	 {!! Form::text('numero_r_mercantil',null,['class'=>'form-control','id'=>'numero_r_mercantil','onkeypress'=>'return soloNumeros(event)'])!!}
                	</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
                 	 {!! Form::label('','Folio')!!}
                 	 {!! Form::text('folio',null,['class'=>'form-control','id'=>'folio','onkeypress'=>'return soloNumerosyLetras(event)'])!!}
                	</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
                 	 {!! Form::label('','Tomo')!!}
                 	 {!! Form::text('tomo',null,['class'=>'form-control','id'=>'tomo','onkeypress'=>'return soloNumerosyLetras(event)'])!!}
                	</div>
				</div>
				<div class="col-md-3">
                	<div class="form-group">
						{!! Form::label('','Fecha de Inscripción') !!}
						<div class="input-group date">
							{!! Form::text('finspeccion',null,['class'=>'form-control','readonly'=>'readonly','id'=>'finspeccion']) !!}
							<span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
						</div>
					</div>
				</div>
			</div>
		</div><br>
		<div class="row">
			<div class="col-md-12">
				<center>
    				{!! Form::label('','Número de empleos actuales')!!}
   				</center><br>
				<div class="col-md-6">
					<div class="form-group">
                 	 {!! Form::label('','Directos ')!!}
                 	 {!! Form::text('emple_actual_directo',null,['class'=>'form-control','id'=>'emple_actual_directo','onkeypress'=>'return soloNumeros(event)'])!!}
                	</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
                 	 {!! Form::label('','Indirectos')!!}
                 	 {!! Form::text('emple_actual_indirecto',null,['class'=>'form-control','id'=>'emple_actual_indirecto','onkeypress'=>'return soloNumeros(event)'])!!}
                	</div>
				</div>
			</div>
		</div><br>
		<div class="row">
			<div class="col-md-12">
				<center>
					{!! Form::label('','Empleos  por generar')!!}
				</center><br>	
				<div class="col-md-6">
					<div class="form-group">
	             	 {!! Form::label('','Directos ')!!}
	             	 {!! Form::text('emple_generar_directo',null,['class'=>'form-control','id'=>'emple_generar_directo','onkeypress'=>'return soloNumeros(event)'])!!}
	            	</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
	             	 {!! Form::label('','Indirectos')!!}
	             	 {!! Form::text('emple_generar_indirecto',null,['class'=>'form-control','id'=>'emple_generar_indirecto','onkeypress'=>'return soloNumeros(event)'])!!}
	            	</div>
				</div>
			</div>
		</div><br>
	</div>
         	
            <div class="row">
              <div class="col-md-12">
                <div class="col-md-4"></div>
                <div class="col-md-4 text-center">
                  <div class="form-group">
                    <a class="btn btn-primary" href="#" id="atras1">Atras</a>
                    <a class="btn btn-primary" href="#" id="seguir2">Siguiente</a>
                  </div>
                </div>
              </div>
            </div>
      </div>
    </div>
</div>
<!------------------------------------------------------------------- -->

<div id="section3" style="display: none;">
        <ol class="breadcrumb">
          <li id="paso2">Paso 1</li>
          <li id="paso1">Paso 2</li>
          <li class="active"  id="paso3">Paso 3</li>
	  			<li  class="" id="paso1">Paso 4</li>
        </ol>
        <div class="panel panel-primary">
          <div class="panel-heading"><h4>Capital Social de la Empresa</h4></div>
          <div class="panel-body">
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
			        	{!! Form::label('','Suscrito Bs')!!}
			            {!! Form::text('capital_suscrito',null,['class'=>'form-control','id'=>'capital_suscrito','onkeypress'=>'return soloNumeros(event)'])!!}
			        </div>	
				</div>
				<div class="col-md-6">
					<div class="form-group">
			        	{!! Form::label('','Pagado Bs')!!}
			            {!! Form::text('capital_apagado',null,['class'=>'form-control','id'=>'capital_apagado','onkeypress'=>'return soloNumeros(event)'])!!}
			        </div>	
				</div>
		
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
			        	{!! Form::label('','Número de acciones emitidas')!!}
			            {!! Form::text('num_acciones_emitidas',null,['class'=>'form-control','id'=>'num_acciones_emitidas','onkeypress'=>'return soloNumeros(event)'])!!}
			        </div>	
				</div>
				<div class="col-md-6">
					<div class="form-group">
			        	{!! Form::label('','Valor nominal de cada acción Bs')!!}
			            {!! Form::text('valor_nominal_accion',null,['class'=>'form-control','id'=>'valor_nominal_accion','onkeypress'=>'return soloNumeros(event)'])!!}
			        </div>	
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						{!! Form::label('','Fecha de la última modificación de la composición accionaria y capital social') !!}
						<div class="input-group date">
							{!! Form::text('fmodificacion_accionaria',null,['class'=>'form-control','readonly'=>'readonly','id'=>'fmodificacion_accionaria']) !!}
							<span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
						</div>
					</div>	
				</div>
				<div class="col-md-6">
					<div class="form-group">
						{!! Form::label('','Fecha de Inscripción') !!}
						<div class="input-group date">
							{!! Form::text('finscripcion',null,['class'=>'form-control','readonly'=>'readonly','id'=>'finscripcion']) !!}
							<span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
						</div>
					</div>
				</div>
			</div>
	           
					 <div class="row">
	           <div class="panel panel-primary">
	                <div class="panel-heading"><h4>Accionistas de la Empresa Receptora de Inversión</h4></div>
	            </div>
	         
	         <table class="table table-bordered text-center" id="accionistas">

                   <tr> <td><button  name="add" type="button" id="add-accionistas" value="add more" class="btn btn-success" >Agregar</button></td></tr>

                    <tr>
                      <th>Nombre o Razon Social</th>
                      <th>Identificación (C.I., Pasaporte, ID)</th>
                      <th>Nacionalidad</th>
                      <th>Capital Social suscrito</th>
                      <th>Capital Social Pagado</th>
                      <th>% de participación dentro del capital social </th>
                    </tr>

                    @if (isset($accionistas))
                    	@foreach ($accionistas as $key=>$accionista)
                      	<tr id="row3{{$key+1}}">

                    	<td>{!!Form::text('razon_social_accionista[]',$accionista->razon_social_accionista,['class'=>'form-control','id'=>'razon_social_accionista','onkeypress'=>'','maxlength'=>''])!!}</td>

                       	<td>{!!Form::text('identificacion_accionista[]',$accionista->identificacion_accionista,['class'=>'form-control','id'=>'identificacion_accionista','onkeypress'=>'','maxlength'=>''])!!}</td>

                      	<td>{!!Form::text('nacionalidad_accionista[]',$accionista->nacionalidad_accionista,['class'=>'form-control','id'=>'nacionalidad_accionista','onkeypress'=>'','maxlength'=>''])!!}</td>

                      	<td>{!!Form::text('capital_social_suscrito[]',$accionista->capital_social_suscrito,['class'=>'form-control','id'=>'capital_social_suscrito','onkeypress'=>'','maxlength'=>''])!!}</td>

                       	<td>{!!Form::text('capital_social_pagado[]',$accionista->capital_social_pagado,['class'=>'form-control','id'=>'capital_social_pagado','onkeypress'=>'','maxlength'=>''])!!}</td>

                        <td>{!!Form::text('porcentaje_participacion[]',$accionista->porcentaje_participacion,['class'=>'form-control','id'=>'porcentaje_participacion','onkeypress'=>'','maxlength'=>''])!!}</td>
                        <td> {!!Form::hidden('id[]',$accionista->id)!!}<button  name="remove" type="button" id="{{$key+1}}" value="" class="btn btn-danger btn-remove">x</button></td>
                        </tr> 
                 		 @endforeach
                	@endif
                      
                </table>
              </div>
	            <br>
	            <div class="row">
                  <div class="col-md-12">
                    <div class="col-md-4"></div>
                    <div class="col-md-4 text-center">
                      <div class="form-group">
                        <a class="btn btn-primary" href="#" id="atras2">Atras</a>
                        <a class="btn btn-primary" href="#" id="seguir3">Siguiente</a>
                      </div>
                    </div>
                  </div>
                </div>
          </div>
        </div>
</div>

<div id="section4" style="display: none;">
        <ol class="breadcrumb">
          <li id="paso4">Paso 1</li>
          <li id="paso3">Paso 2</li>
          <li id="paso2">Paso 3</li>
          <li class="active"  id="paso1">Paso 4</li>
        </ol>
        <div class="panel panel-primary">
          <div class="panel-heading"><h4>Datos del Representante Legal o Apoderado</h4></div>
          <div class="panel-body">
          	<div class="row">
          		<div class="col-md-4">
	      			<div class="form-group">
	             	 {!! Form::label('','Nombres')!!}
	             	 {!! Form::text('nombre_repre',null,['class'=>'form-control','id'=>'nombre_repre','onkeypress'=>'return soloLetras(event)'])!!}
	            	</div>
          		</div>
          		<div class="col-md-4">
          			<div class="form-group">
                 	 {!! Form::label('','Apellidos')!!}
                 	 {!! Form::text('apellido_repre',null,['class'=>'form-control','id'=>'apellido_repre','onkeypress'=>'return soloLetras(event)'])!!}
                	</div>
          		</div>
          		<div class="col-md-4">
          			<div class="form-group">
                 	 {!! Form::label('','N° de Documento de Identidad')!!}
                 	 {!! Form::text('numero_doc_identidad',null,['class'=>'form-control','id'=>'numero_doc_identidad','onkeypress'=>'return soloNumerosyLetras(event)'])!!}
                	</div>
          		</div>
          	</div><br>
          	<div class="row">
          		<div class="col-md-4">
	      			<div class="form-group">
	             	 {!! Form::label('','N° de Pasaporte')!!}
	             	 {!! Form::text('numero_pasaporte_repre',null,['class'=>'form-control','id'=>'numero_pasaporte_repre','onkeypress'=>'return soloNumerosyLetras(event)'])!!}
	            	</div>
          		</div>
          		<div class="col-md-4">
          			<div class="form-group">
                 	 {!! Form::label('','Código postal ')!!}
                 	 {!! Form::text('cod_postal_repre',null,['class'=>'form-control','id'=>'cod_postal_repre','onkeypress'=>''])!!}
                	</div>
          		</div>
          		<div class="col-md-4">
          			<div class="form-group">
                 	 {!! Form::label('','N° Registro de Información Fiscal (R.I.F) del Apoderado')!!}
                 	 {!! Form::text('rif_repre',null,['class'=>'form-control','id'=>'rif_repre','onkeypress'=>'return soloNumerosyLetras(event)'])!!}
                	</div>
          		</div>
          	</div>
          	<div class="row">
          		<div class="col-md-3">
	      			<br><div class="form-group">
	             	 {!! Form::label('','Teléfono 1')!!}
	             	 {!! Form::text('telefono_1',null,['class'=>'form-control','id'=>'telefono_1','onkeypress'=>'return soloNumeros(event)'])!!}
	            	</div>
          		</div>
          		<div class="col-md-3">
	      			<br><div class="form-group">
	             	 {!! Form::label('','Teléfono 2')!!}
	             	 {!! Form::text('telefono_2',null,['class'=>'form-control','id'=>'telefono_2','onkeypress'=>'return soloNumeros(event)'])!!}
	            	</div>
          		</div>
          		<div class="col-md-3">
          			<div class="form-group">
                 	 {!! Form::label('','Correo electrónico donde desea recibir las notificaciones')!!}
                 	 {!! Form::text('correo_repre',null,['class'=>'form-control','id'=>'correo_repre','onkeypress'=>''])!!}
                	</div>
          		</div>
          		<div class="col-md-3">
          			<div class="form-group">
                 	 {!! Form::label('','Dirección donde recibirá las notificaciones')!!}
                 	 {!! Form::text('direccion_repre',null,['class'=>'form-control','id'=>'direccion_repre','onkeypress'=>''])!!}
                	</div>
          		</div>
          	</div><br>

            <div class="row">
              <div class="col-md-12">
                <div class="col-md-4"></div>
                <div class="col-md-4 text-center">
                  <div class="form-group">
                    <a class="btn btn-primary" href="#" id="atras3">Atras</a>
                    <a class="btn btn-primary" href="#" id="seguir4" data-toggle="modal" data-target="#myModalInversion">Registrar</a>
                  </div>
                </div>
              </div>
            </div>
       	</div>
    </div>
</div>

<!-------Modal--->
<div id="myModalInversion" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close btn-md" data-dismiss="modal" style="height:5px;">&times;</button>
        <h4 class="modal-title"><b style="color:#337AB7">DECLARACIÓN JURADA DE INVERSIÓN REALIZADA</b></h4>
      </div>
      <div class="modal-body">
      <div class="col-md-12">
        <p class="text-justify">DECLARO QUE LOS DATOS CONTENIDOS EN ESTA DECLARACIÓN HAN SIDO DETERMNADOS EN BASE A LAS DISPOSICIONES LEGALES Y REGLAMENTARIAS CORRESPONDIENTES.
        </p>
        <div class="row">
          <div class="colo-md-1"></div>
          <div class="col-md-10">
             <input type="checkbox" name="condicion" id="condicion" value="si" class="checkbox-inline"><label class="text-justify">LA INFORMACIÓN AQUÍ DECLARADA ES ENTREGADA CON CARÁCTER CONFIDENCIAL.</label>
          </div>
          <div class="colo-md-1"></div>
        </div>
      </div>
      </div>
      <div class="modal-footer">
        <div class="row">
          <div class="col-md-12">
            <div class="col-md-4"></div>
            <div class="col-md-4 text-center">
              <div class="form-group">
                 <button type="button" class="btn btn-default" style="background-color:#ADABC9; color: #FFFFFF;" data-dismiss="modal"><b>Cerrar</b></button>
                <input type="submit" class="btn btn-primary" value="Aceptar" name="Aceptar" onclick="enviarInversionista()"> 
              </div>
            </div>
            <div class="col-md-4"></div>
          </div>
        </div>
      </div>
    </div>

  </div>
</div>




{{Form::close()}}

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="{{asset('js/ajax_v2.1.1.js')}}"></script>
<script type="text/javascript">

/********************Scrip para carga de archivos**************** */
 function cambiarImagen(event) {
    var id= event.target.getAttribute('name');
    var reader = new FileReader();
    reader.onload = function(){
      var output = document.getElementById('vista_previa_'+id);
      output.src = reader.result;
    };
    let archivo = $(".file-input").val();
    if (!archivo) {
      archivo=$('#'+id).val();
    }
    var extensiones = archivo.substring(archivo.lastIndexOf(".")).toLowerCase();
    
    //var imgfile="/exportaciones/public/img/file.png";

//alert(imgfile);
    if(extensiones != ".doc" && extensiones != ".docx" && extensiones != ".pdf"){
      swal("Formato invalido", "Formato de archivo invalido debe ser .doc, .docx o pdf", "warning");
        
    }else{
      $('#imgdocumento_'+id).show();
      $('#imgdocumentoEdit_'+id).hide();
       let file=$('#'+id).val();
     // alert(file);
      $('#nombre_'+id).html('<strong>'+file+'</strong>');
      reader.readAsDataURL(event.target.files[0]);
    }   
  
  }

  /********************************************************************** */


    $(document).on('change', '.file-input', function(evt) {
      
      let size=this.files[0].size;
      if (size>2500000) {

         swal("Tamaño de archivo invalido", "El tamaño del archivo No puede ser mayor a 2.5 megabytes","warning"); 
          this.value='';
         //this.files[0].value='';
      }else{
        cambiarImagen(evt);
      }
      
           
    });


/*************************Lista dinamica para Soportes 2 ********************** */

$(document).ready(function (){
   /* agregar para productos completos en el caso colombia*/

  

/*********************Lista Dinamica para soporte 4 **************************/

  //////////////////Lista Dinamica Accionistas//////////////////
var x = $('input[name="contador"]').val();
var i=parseInt(x);

	$('#add-accionistas').click(function(){
		i++;
	$('#accionistas tr:last').after('<tr id="row3'+i+'"display="block" class="show_div"><td>{!!Form::text("razon_social_accionista[]",null,["class"=>"form-control","id"=>"razon_social_accionista","onkeypress"=>"","maxlength"=>""])!!}</td><td>{!!Form::text("identificacion_accionista[]",null,["class"=>"form-control","id"=>"identificacion_accionista","onkeypress"=>"","maxlength"=>""])!!}</td><td>{!!Form::text("nacionalidad_accionista[]",null,["class"=>"form-control","id"=>"nacionalidad_accionista","onkeypress"=>"","maxlength"=>""])!!}</td><td>{!!Form::text("capital_social_suscrito[]",null,["class"=>"form-control","id"=>"capital_social_suscrito","onkeypress"=>"","maxlength"=>""])!!}</td><td>{!!Form::text("capital_social_pagado[]",null,["class"=>"form-control","id"=>"capital_social_pagado","onkeypress"=>"","maxlength"=>""])!!}</td><td>{!!Form::text("porcentaje_participacion[]",null,["class"=>"form-control","id"=>"porcentaje_participacion","onkeypress"=>"","maxlength"=>""])!!}</td><td>{!!Form::hidden('id[]',0)!!}<button  name="remove" type="button" id="'+i+'" value="" class="btn btn-danger btn-remove">x</button></td></tr>');

		});

	$(document).on('click','.btn-remove',function(){
        var id_boton= $(this).attr("id");
        var id=$(this).prev().val();

    if(id!=0){

         if (confirm('Deseas eliminar este Acionista?')) {

      $.ajax({

       'type':'get',
       'url':'eliminarAccionistaInversion',
       'data':{ 'id':id},
       success: function(data){

        $('#row3'+id_boton).remove();

         }

       });

       }



     }else{

      $('#row3'+id_boton).remove();
     }

     });

/*****************Ocultar y mostrar Empresa Receptora ***********/
	$('#emp_otro').click(function(event) {
		$('#tipo_emp_otro').show();
	});
	$('#emp_privada,#emp_publica,#emp_mixto').click(function(event) {
		$('#tipo_emp_otro').hide();
		$('#tipo_emp_otro').val('');
	});

	/* MARCAR RADIOS TIPO EMPRESA*/
	let valEmpPrivada = $('#emp_privada_edit').val();
	let valEmpPublica = $('#emp_publica_edit').val();
	let valEmpMixto = $('#emp_mixto_edit').val();
	let valEmpOtro = $('#emp_otro_edit').val();
	
	if (valEmpPrivada == 1) {
		// console.log('valEmpPrivada='+valEmpPrivada);
		document.querySelector('emp_privada').checked = true;
	} else if(valEmpPublica == 1) {
		document.querySelector('#emp_publica').checked = true;
	} else if(valEmpMixto == 1) {
		document.querySelector('#emp_mixto').checked = true;
	} else {
		document.querySelector('#emp_otro').checked = true;
		$('#tipo_emp_otro').show();
	}



	
});/*llave del document).ready*/

</script>
@stop