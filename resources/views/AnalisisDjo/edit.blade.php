@extends('templates/layoutlte_analist_djo')

@section('content')

<!-- MENSAJES DE OBSERVACION CORREGIDA -->
	@if($planilla2->estado_observacion==0)
		<div id="space_alert1" class="alert alert-success" role="alert" style="display: block;">
			La observación de la Planilla 2 ha sido Corregida por el Usuario:
		</div>
	@endif
	@if($planilla3->estado_observacion==0)
		<div id="space_alert1" class="alert alert-success" role="alert" style="display: block;">
			La observación de la <b>Planilla 3</b> ha sido Corregida por el Usuario:
		</div>
	@endif
	@if($planilla4->estado_observacion==0)
		<div id="space_alert1" class="alert alert-success" role="alert" style="display: block;">
			La observación de la <b>Planilla 4</b> ha sido Corregida por el Usuario:
		</div>
	@endif
	@if($planilla5->estado_observacion==0)
		<div id="space_alert1" class="alert alert-success" role="alert" style="display: block;">
			La observación de la <b>Planilla 5</b> ha sido Corregida por el Usuario:
		</div>
	@endif
	@if($planilla6->estado_observacion==0)
		<div id="space_alert1" class="alert alert-success" role="alert" style="display: block;">
			La observación de la <b>Planilla 6</b> ha sido Corregida por el Usuario:
		</div>
	@endif
	@if(!empty($doc_adicional) && $doc_adicional->estado_observacion == 0)
		<div id="space_alert1" class="alert alert-success" role="alert" style="display: block;">
			La observación de la <b>Doc Adicionales</b> ha sido Corregida por el Usuario:
		</div>
	@endif
	
	<!-- CONDICIONES SI HAY OBSERVACIONES CUANDO EL COORDINADOR LAS ENVIÓ AL ANALISTA - By WETS -->
	@if(!empty($planilla2) && $planilla2->estado_observacion==1 && $status_id->gen_status_id==36)
	<div id="space_alert1" class="alert alert-danger" role="alert" style="display: block;">
		<strong><span class="glyphicon glyphicon-circle-arrow-right"></span> Observación indicada por el Coordinador en la <b>Planilla 2</b>: {{$planilla2->descrip_observacion}}</strong>
	</div>
	@endif
	@if(!empty($planilla3) && $planilla3->estado_observacion==1 && $status_id->gen_status_id==36)
	<div id="space_alert1" class="alert alert-danger" role="alert" style="display: block;">
		<strong><span class="glyphicon glyphicon-circle-arrow-right"></span> Observación indicada por el Coordinador en la <b>Planilla 3</b>: {{$planilla3->descrip_observacion}}</strong>
	</div>
	@endif
	@if(!empty($planilla4) && $planilla4->estado_observacion==1 && $status_id->gen_status_id==36)
	<div id="space_alert1" class="alert alert-danger" role="alert" style="display: block;">
		<strong><span class="glyphicon glyphicon-circle-arrow-right"></span> Observación indicada por el Coordinador en la <b>Planilla 4</b>: {{$planilla4->descrip_observacion}}</strong>
	</div>
	@endif
	@if(!empty($planilla5) && $planilla5->estado_observacion==1 && $status_id->gen_status_id==36)
	<div id="space_alert1" class="alert alert-danger" role="alert" style="display: block;">
		<strong><span class="glyphicon glyphicon-circle-arrow-right"></span> Observación indicada por el Coordinador en la <b>Planilla 5</b>: {{$planilla5->descrip_observacion}}</strong>
	</div>
	@endif
	@if(!empty($planilla6) && $planilla6->estado_observacion==1 && $status_id->gen_status_id==36)
	<div id="space_alert1" class="alert alert-danger" role="alert" style="display: block;">
		<strong><span class="glyphicon glyphicon-circle-arrow-right"></span> Observación indicada por el Coordinador en la <b>Planilla 6</b>: {{$planilla6->descrip_observacion}}</strong>
	</div>
	@endif
	@if(!empty($doc_adicional) && $doc_adicional->estado_observacion == 1 && $status_id->gen_status_id==36)
	<div id="space_alert1" class="alert alert-danger" role="alert" style="display: block;">
		<strong><span class="glyphicon glyphicon-circle-arrow-right"></span> Observación indicada por el Coordinador en <b>Doc Adicionales</b>: {{$doc_adicional->descrip_observacion}}</strong>
	</div>
	@endif

	@if($status_id->gen_status_id==37)
	<div id="space_alert1" class="alert alert-danger" role="alert" style="display: block;">
		<strong><span class="glyphicon glyphicon-circle-arrow-right"></span> Razón de apelación indicada por exportador: {{$producDeclaracion->observacion_analisis}}</strong>
	</div>
	@endif

<!-- FIN MENSAJES -->

<div class="content" style="background-color: #fff">
	{!! Form::model($producDeclaracion->rDeclaracion, ['method'=>'PATCH','id'=>'validar_declaracion','route'=>['AnalisisDjo.update',$producDeclaracion->id]]) !!}
 
    <div class="panel-group" style="">
      	<div class="panel-primary">
	        <div class="panel-heading">
	        	<div class="row">
		        	<div class="col-md-6"><h3> ANÁLISIS DE DECLARACIÓN JURADA DE ORIGEN</h3></div>
		        	<div class="col-md-2">
		        		<h4>Tipo de Usuario</h4>
		        		{{ ($declaraciondjo->tipoUsuario)?$declaraciondjo->tipoUsuario:'' }}
		        	</div>
		        	<div class="col-md-2">
		        		<h4>Nº Solicitud:</h4>
		        		{{ $producDeclaracion->num_solicitud }}
		        		<input type="hidden" name="gen_declaracion_jo_id" value="{{$producDeclaracion->id}}">
		        		<input type="hidden" name="num_solicitud" value="{{$producDeclaracion->num_solicitud}}">
		        	</div>
		        	<div class="col-md-2">
		        		
		        		<h4>Fecha Solicitud:</h4>
		        		{{ $producDeclaracion->created_at }}
		        	</div>			
	        	</div>
	        </div>
		</div>
		<br>
	
	
		<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#docsoportes">SOPORTES</button>
	 	

        <div class="panel-body">

			<table class="col-md-12" border="2">
				<thead>
					<th class="col-md-2">RIF</th>
					<th class="col-md-2">EMPRESA</th>
					<th class="col-md-4" colspan="8">FECHA EMISIÓN</th>
					<th class="col-md-4" colspan="8">FECHA VENCIMIENTO</th>

				</thead>
				<tbody>
					<tr class="text-center">
						<td>	
							{{ $producDeclaracion->rDeclaracion->rDetUsuario->rif }}
						</td>
						<td> 
							{{ $producDeclaracion->rDeclaracion->rDetUsuario->razon_social }}
							{{-- dd($producDeclaracion) --}}
						</td>
						<td colspan="8" align="center">
							 <div class="input-group date">
								 {!! Form::text('fecha_emision', null, ['class' => 'form-control text-center','placeholder'=>'Ingresa fecha emisión','id'=>'fecha_emision', '']) !!}
								 <span style="background-color: #3c8dbc;color:#fffa" class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
							</div>
						</td>
						<td colspan="8" align="center">
						 	<div class="input-group date"> 
							 {!! Form::text('fecha_vencimiento', null, ['class' => 'form-control text-center','placeholder'=>'Ingresa fecha vencimiento','id'=>'fecha_vencimiento', '']) !!}
							  <span style="background-color: #3c8dbc;color:#fffa" class="input-group-addon "><i class="glyphicon glyphicon-th"></i></span>
							</div>
						</td>
					</tr>
					<tr>
						<th class="text-center">
							Código Arancelario
						</th>
						<th rowspan="2" class="text-center">
							Denominación Comercial del Producto
						</th>
						<th colspan="15" class="text-center">
							Criterios de Origen
						</th>
						
					</tr>
					<tr class="text-center">
						<td style="font-size: 12px;">
							<div class="col-md-12 text-left text-danger"><b>Nacional NCM</b></div>
							<div class="col-md-12 text-right text-success"><b>Naladisa 96</b></div>
						</td>
				
						<td><b>BOL</b></td>
						<td><b>COL</b></td>
						<td><b>ECU</b></td>
						<td><b>PER</b></td>
						<td><b>CUB</b></td>
						<td><b>ALD</b></td>
						<td><b>ARG</b></td>
						<td><b>BRA</b></td>
						<td><b>PAR</b></td>
						<td><b>URU</b></td>
						<td><b>USA</b></td>
						<td><b>UE</b></td>
						<td><b>CND</b></td>
						<td><b>TP</b></td>
						<td><b>TR</b></td>
												
					</tr>
					{{-- dump($producDeclaracion->rCriterioAnalisis) --}}
					@foreach($producDeclaracion->rCriterioAnalisis as $key =>  $productos)

						<tr class="text-center">
							<td style="font-size: 12px;">
								@php 
						 
									$boton_cod_mer='<botton id="boton_mer_'.$productos->det_declaracion_produc_id.'" class="btn btn-xs cargar_arancel btn-danger" data-toggle="modal" data-target="#arancel" data-tipo-arancel="mer" data-indice="'.$productos->det_declaracion_produc_id.'"><i class="glyphicon glyphicon-plus"></i></botton>';
									$boton_cod_mer= @$productos->declaracionProduc->rProducto->codigo ? '' : $boton_cod_mer;
									$boton_cod_nan='<botton type="button" required id="boton_nan_'.$productos->det_declaracion_produc_id.'" class=" btn btn-xs cargar_arancel btn-success" data-toggle="modal" data-target="#arancel" data-tipo-arancel="nan" data-indice="'.$productos->det_declaracion_produc_id.'"><i class="glyphicon glyphicon-plus"></i></button>';						
									$boton_cod_nan= @$productos->rCriterioAnalisis->arancel_nan ? '' : $boton_cod_nan;

								@endphp

								<div class="col-md-12 text-left text-danger" style="padding-left: 6px;">
									<input readonly="true" size="10" class="text-left" style="border:0px;" id="cod_mer_{{$productos->det_declaracion_produc_id}}" type="text" name="cod_mer[{!! $productos->det_declaracion_produc_id !!}]" value="{{ $productos->declaracionProduc ? $productos->declaracionProduc->rProducto->codigo : ''}}" required>
									{!! $boton_cod_mer or '' !!}
								</div>
								<div>---</div>
								<div class="col-md-12 text-right text-success" style="padding-right: 2px;">
									<input  size="10" class="text-right" id="cod_nan_{{$productos->det_declaracion_produc_id}}" type="text" name="cod_nan[{!! $productos->det_declaracion_produc_id !!}]" value="{{ $productos->arancel_nan or '' }}" required>
									{!! $boton_cod_nan or '' !!}
								</div>
							</td>
							<td>
								<button type="button" class="btn btn-primary btn-xs btn-view-planillas" data-toggle="modal" data-target="#modalPlanillas" data-declaracion-product="{{ $productos->det_declaracion_produc_id }}">{{ $key+1 }}- Ver Información</button>
								<br>
								<b class="text-primary">{{ $productos->declaracionProduc->rProducto->descrip_comercial }}</b>
								<br> 
									{{ $productos->declaracionProduc->rProducto->descripcion  }} 
								<br>
								<input class="form-control" type="hidden" readonly="true" name="productoId[]" value="{{$productos->det_declaracion_produc_id}}">
								<input class="form-control" type="text" placeholder="Descripción Adicional" style="font-size: 12px;" name="arancel_descrip_adicional[{{$productos->det_declaracion_produc_id}}]" value="{{$productos->arancel_descrip_adicional}}" >
								
							</td>
							@for($i=1;$i<=15;$i++)
								<td align="center" style="width: 4%;">
									@php 
										$idPaises=[]; 
									 	$idConvenio=[]; 
										//$cat_ue=["w"=>"w","p"=>"p","n"=>"n","*"=>"*"]; 
										//$cat_usa=["p"=>"p","n"=>"n","*"=>"*","%"=>"%","(1)"=>"(1)","(2)"=>"(2)","(3)"=>"(3)"]; 
										//$cat_cnd=["f"=>"f","p"=>"p","n"=>"n","*"=>"*","(1)"=>"(1)","(2)"=>"(2)","(3)"=>"(3)"]; 
									@endphp

									@foreach ($productos->declaracionProduc->rDeclaracionPaises as $key => $value)
										@php $idPaises[]=$value->pais_id; @endphp
										@php $idConvenio[]=$value->rPais->cconvenio; @endphp
									@endforeach

									
									@if($i==1) {{-- Bolivia --}}
										@if (in_array("23", $idPaises)) 
									   		{!!Form::select('bol['.$productos->det_declaracion_produc_id.']',$cat_bolivia,$productos->bol,['style'=>'width:90%;padding:0px;!important;font-size:12px;','placeholder'=>'-','class'=>'form-control','id'=>'id_bol','']) !!}
										@endif
									@endif
									@if($i==2) {{-- colombia --}}
										@if (in_array("37", $idPaises)) 
									   		{!!Form::select('col['.$productos->det_declaracion_produc_id.']',$cat_colombia,$productos->col,['style'=>'width:90%;padding:0px;!important;font-size:12px;','placeholder'=>'-','class'=>'form-control','id'=>'id_col','']) !!}
										@endif
									@endif	
									@if($i==3) {{-- ecuador --}}
										@if (in_array("46", $idPaises)) 
									   		{!!Form::select('ecu['.$productos->det_declaracion_produc_id.']',$cat_aladi,$productos->ecu,['style'=>'width:90%;padding:0px;!important;font-size:12px;','placeholder'=>'-','class'=>'form-control','id'=>'id_ecu','']) !!}
										@endif
									@endif	
									@if($i==4) {{-- peru --}}
										@if (in_array("128", $idPaises)) 
									   		{!!Form::select('per['.$productos->det_declaracion_produc_id.']',$cat_peru,$productos->per,['style'=>'width:90%;padding:0px;!important;font-size:12px;','placeholder'=>'-','class'=>'form-control','id'=>'id_per','']) !!}
										@endif
									@endif	
									@if($i==5) {{-- cuba --}}
										@if (in_array("43", $idPaises)) 
									   		{!!Form::select('cub['.$productos->det_declaracion_produc_id.']',$cat_cuba,$productos->cub,['style'=>'width:90%;padding:0px;!important;font-size:12px;','placeholder'=>'-','class'=>'form-control','id'=>'id_cub','']) !!}
										@endif
									@endif	
									@if($i==6) {{-- ald --}}
										@if (in_array("ald", $idConvenio))
									   		 {!!Form::select('ald['.$productos->det_declaracion_produc_id.']',$cat_aladi,$productos->ald,['style'=>'width:90%;padding:0px;!important;font-size:12px;','placeholder'=>'-','class'=>'form-control','id'=>'id_ald',''])!!}
										@endif
									@endif	
									@if($i==7) {{-- argentina --}}
										@if (in_array("10", $idPaises)) 
									   		{!!Form::select('arg['.$productos->det_declaracion_produc_id.']',$cat_mercosur,$productos->arg,['style'=>'width:90%;padding:0px;!important;font-size:12px;','placeholder'=>'-','class'=>'form-control','id'=>'id_arg','']) !!}
										@endif
									@endif	
									@if($i==8) {{-- brasil --}}
										@if (in_array("25", $idPaises)) 
									   		{!!Form::select('bra['.$productos->det_declaracion_produc_id.']',$cat_mercosur,$productos->bra,['style'=>'width:90%;padding:0px;!important;font-size:12px;','placeholder'=>'-','class'=>'form-control','id'=>'id_bra','']) !!}
										@endif
									@endif	
									@if($i==9) {{-- paraguay --}}
										@if (in_array("127", $idPaises)) 
									   		{!!Form::select('par['.$productos->det_declaracion_produc_id.']',$cat_mercosur,$productos->par,['style'=>'width:90%;padding:0px;!important;font-size:12px;','placeholder'=>'-','class'=>'form-control','id'=>'id_par','']) !!}
										@endif
									@endif	
									@if($i==10) {{-- uruguay --}}
										@if (in_array("164", $idPaises)) 
									   		{!!Form::select('uru['.$productos->det_declaracion_produc_id.']',$cat_mercosur,$productos->uru,['style'=>'width:90%;padding:0px;!important;font-size:12px;','placeholder'=>'-','class'=>'form-control','id'=>'id_uru','']) !!}
										@endif
									@endif	
									@if($i==11) {{-- usa --}}
										@if (in_array("53", $idPaises)) 
											@php 
												$cat_usa->put($productos->usa,$productos->usa); 
											@endphp
										   		{!!Form::select('usa['.$productos->det_declaracion_produc_id.']',$cat_usa,$productos->usa,['style'=>'width:90%;padding:0px;!important;font-size:12px;','placeholder'=>'-','class'=>'form-control','id'=>'id_select_'.$productos->det_declaracion_produc_id.'','']) !!}
													{{-- input agregar porcentaje % --}}
												{!! Form::text('usa', null, ['name'=>'usa['.$productos->det_declaracion_produc_id.']','style'=>'width:90%;padding:0px;font-size:12px;display:none;','disabled','class' => 'form-control text-center','placeholder'=>'%','id'=>'id_input_'.$productos->det_declaracion_produc_id.'']) !!}
										   			{{--btn cancelar porcentaje % --}}
										   			<div  id="id_cancelar_{!!$productos->det_declaracion_produc_id!!}" class="btn btn-xs btn-danger" style="position:absolute;margin-left: 1%;display:none;">X</div>
										   				{{--script validacion --}}
														<script type="text/javascript">
													 		{{-- escuchar select --}}
													 		document.getElementById('id_select_'+{!!$productos->det_declaracion_produc_id!!})
													 		.addEventListener('change', function(element)
													 		{		
													 				if (element.target.value=="%")
													 				{
													 					console.log(element);
													 					{{-- Select hidden, no required y disabled --}}
														 					element.target.style.display = 'none';
														 					element.target.removeAttribute('required');
														 					element.target.setAttribute('disabled','true');
														 					element.target.value='';
													 					{{-- Input  show, active y required --}}
													 						document.getElementById('id_input_'+ {!!$productos->det_declaracion_produc_id!!}).style.display = 'block';
													 						document.getElementById('id_input_'+ {!!$productos->det_declaracion_produc_id!!}).removeAttribute('disabled');
													 						document.getElementById('id_input_'+ {!!$productos->det_declaracion_produc_id!!}).setAttribute('required','true');
													 						
													 					{{-- btn cancelar show --}}
													 						document.getElementById('id_cancelar_'+{!!$productos->det_declaracion_produc_id!!}).style.display = 'block';
													 				}
													 		});

													 		{{-- Escuchar btn Cancelar --}}
													 		document.getElementById('id_cancelar_'+{!!$productos->det_declaracion_produc_id!!})
													 		.addEventListener('click', function(element)
													 		{
													 				{{-- btn hidden --}}	
													 					element.target.style.display = 'none';
													 					
													 				{{-- Select  show, active y required --}}
													 					document.getElementById('id_select_'+{!!$productos->det_declaracion_produc_id!!}).style.display = 'block';
													 					document.getElementById('id_select_'+{!!$productos->det_declaracion_produc_id!!}).removeAttribute('disabled');
													 					document.getElementById('id_select_'+{!!$productos->det_declaracion_produc_id!!}).setAttribute('required','true');
													 															 			
													 				{{-- input hidden, no required, disabled y value default --}}
													 					document.getElementById('id_input_'+{!!$productos->det_declaracion_produc_id!!}).style.display = 'none';
													 					document.getElementById('id_input_'+ {!!$productos->det_declaracion_produc_id!!}).removeAttribute('required');
													 					document.getElementById('id_input_'+ {!!$productos->det_declaracion_produc_id!!}).setAttribute('disabled','true');
													 					document.getElementById('id_input_'+ {!!$productos->det_declaracion_produc_id!!}).value='';																
													 		});
														</script>
										@endif
									@endif	
									@if($i==12) {{-- union_europea --}}
										@if (in_array("ue", $idConvenio))
									   		 {!!Form::select('ue['.$productos->det_declaracion_produc_id.']',$cat_ue,$productos->ue,['style'=>'width:90%;padding:0px;!important;font-size:12px;','placeholder'=>'-','class'=>'form-control','id'=>'id_ue',''])!!}
										@endif
									@endif	
									@if($i==13) {{-- canada --}}
										@if (in_array("32", $idPaises)) 
									   		{!!Form::select('cnd['.$productos->det_declaracion_produc_id.']',$cat_cnd,$productos->cnd,['style'=>'width:90%;padding:0px;!important;font-size:12px;','placeholder'=>'-','class'=>'form-control','id'=>'id_cnd','']) !!}
										@endif
									@endif	
									@if($i==14) {{-- terceros paises --}}
										@if (in_array("tp", $idConvenio))
									   		 {!!Form::select('tp['.$productos->det_declaracion_produc_id.']',$cat_aladi,$productos->tp,['style'=>'width:90%;padding:0px;!important;font-size:12px;','placeholder'=>'-','class'=>'form-control','id'=>'id_tp',''])!!}
										@endif
									@endif
								@if($i==15)  {{-- turquia --}}
									@if (in_array("omc", $idConvenio))
								   		 {!!Form::select('tr['.$productos->det_declaracion_produc_id.']',$cat_turquia,$productos->tr,['style'=>'width:90%;padding:0px;!important;font-size:12px;','placeholder'=>'-','class'=>'form-control','id'=>'id_tp','required'])!!}
									@endif
								@endif																
								</td>
							@endfor
						</tr>
					@endforeach
					@if ($status_id->gen_status_id !== 37)
					<tr>
						<td colspan="19">
							<div class="col-md-12" style="margin-bottom: 10px;">

								<b><u>Observaciones:</u></b>
								<textarea rows="4"  class="form-control text-left" name="observacion_analisis" style="font-weight:bold; text-transform: uppercase;" >{{ $producDeclaracion->observacion_analisis or '' }}</textarea>
							</div>
						</td>
					</tr>
					@else
					<tr>
						<td colspan="19">
							<div class="col-md-12" style="margin-bottom: 10px;">

								<b><u>¿Que deseas realizar?:</u></b>
								<br/>
								<br/>
								<select class="form-control" name="selec_apelacion" style="width:90%;padding:0px;!important;font-size:14px;">
									<option value="0">Aprobar apelación</option>
									<option value="1">Declinar apelación</option>
								  </select>
							</div>
						</td>
					</tr>
						
					@endif
					
				</tbody>
			</table>
        </div>
		
        <hr>

        <div class="panel-footer">
        	<dir class="row">
	        	<!--div class="col-md-4 text-center">
	        		<button  type="submit" class="btn btn-warning btn-lg" > Enviar Observaciones</button>
	        	</div-->
	        	<div class="row text-center">
					<a href="{{ route('AnalisisDjo.index')}}" class="btn btn-primary btn-lg">Cancelar</a>
					<button type="submit" id="submit" class="btn btn-success btn-lg" onclick="validar_declaracion()"> Procesar</button>
					
				</div>
	        	<!--div class="col-md-6 text-center">
	        		<button type="button" class="btn btn-danger btn-lg" > Solicitar Anulación</button>
	        	</div-->
        	</div>
        </div>
      </div>
    </div>
</div>
{{Form::close()}}
<!-- Modal -->
<div class="modal fade" id="modalPlanillas" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" id="closex" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>

                </button>
                 <h4 class="modal-title" id="myModalLabel">Planillas Declaracion Jurada de Origen</h4>
<input type="hidden" name="gen_declaracion_jo_edit_id" value="{{$idDeclaracion}}" placeholder="" id="gen_declaracion_jo_edit_id">
            </div>
            <div class="modal-body">
                <div role="tabpanel">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#p2" class="cargador-planilla" aria-controls="p2" role="tab" data-toggle="tab" id="tab-inicial">Planilla 2</a>
                        </li>
                        <li role="presentation"><a href="#p3" class="cargador-planilla" aria-controls="p3" role="tab" data-toggle="tab">Planilla 3</a>
                        </li>
                        <li role="presentation"><a href="#p4" class="cargador-planilla" aria-controls="p4" role="tab" data-toggle="tab">Planilla 4</a>
                        </li>
                        <li role="presentation"><a href="#p5" class="cargador-planilla" aria-controls="p5" role="tab" data-toggle="tab">Planilla 5</a>
                        </li>
                        <li role="presentation"><a href="#p6" class="cargador-planilla" aria-controls="p6" role="tab" data-toggle="tab">Planilla 6</a>
                        </li>
                        @if(isset($doc_adicional))
	                        <li role="presentation"><a href="#p7" class="cargador-planilla" aria-controls="p7" role="tab" data-toggle="tab">Doc Adicionales</a>
	                        </li>
                        @endif
                    </ul>
                    <div class="row" style="margin-top:10px;">
						<div class="col-md-8"></div>
						<div class="col-md-2"><button type="button" class="btn btn-warning" id="botonObservacion" onclick="mostrarObservacion()">Agregar Observaci&oacute;n</button></div>
						<div class="col-md-2"><button type="button" id="closev" class="btn btn-danger" data-dismiss="modal">Cerrar ventana</button></div>
					</div>
                    <div class="row hidden">
						<div class="col-md-12">
							<div class="form-group text-left">
								<label for="observacion">Observaci&oacute;n</label>
								<textarea name="observacion" id="observacion" class="form-control"></textarea>
							</div>
						</div>
					</div>
					
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="p2">p2</div>
                        <div role="tabpanel" class="tab-pane" id="p3">p3</div>
                        <div role="tabpanel" class="tab-pane" id="p4">p4</div>
                        <div role="tabpanel" class="tab-pane" id="p5">p5</div>
                        <div role="tabpanel" class="tab-pane" id="p6">p6</div>
                        <div role="tabpanel" class="tab-pane" id="p7">p7</div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
				
            </div>
        </div>
    </div>
</div>
<!-- fin Modal-->

<!-- Modal arancel //////////////////////////////////////////////////////////////-->
<div class="modal fade" id="arancel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
            	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                 <h3 class="modal-title" id="myModalLabel"></h3>
            </div>
            <div class="modal-body"></div>
            <div class="modal-footer"></div>
        </div>
    </div>
</div>	
<!-- Fin Modal Arancel //////////////////////////////////////////////////////////-->






<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script type="text/javascript">
var ocultarObservacion=null;
var mostrarObservacion = null;
var cargarObservacion= null;
var guardarObservacion= null;
$(document).ready(function()
{

    /** Escuchando evento click en los elementos que contengan la clase btn-view-planillas 
     *  donde se creara una variable local que contendra el id de det declaracion produc
     *  adicionalmente se induce el evento click en la pestaña inicial de los tabs**/
    $(document).on('click','.btn-view-planillas',function()
    {
        var det_declaracion_id = this.getAttribute('data-declaracion-product');
        localStorage.setItem("det_declaracion_id","");
        
        if(!det_declaracion_id==false)
        {
            localStorage.setItem("det_declaracion_id",det_declaracion_id);
        }

        document.getElementById('tab-inicial').click();
    });

	/** Escuchando evento click en los elementos que contengan la clase cargador-planilla
	 *  donde se obtienen el nombre del ancla (id_href) del enlace y el nombre la planilla a mostrar
	 *  consulta via ajax de la visual de la planilla correspondiente y la carga de los datos recibidos en
	 * el contenedor especifico **/
    $(document).on('click','.cargador-planilla',function()
    {
        var id_href = this.getAttribute('href');
        var nombre = this.getAttribute('aria-controls');
        var det_declaracion_id= localStorage.getItem("det_declaracion_id");
        var gen_declaracion_jo_id=$('#gen_declaracion_jo_edit_id').val();
        
		localStorage.setItem("nombre_tag","");
		ocultarObservacion();

        if(!id_href==false && !nombre==false)
        {
            var base= document.location.origin;
            var url= '/calificaciondjo/';
			localStorage.setItem("nombre_tag",nombre);
            if(nombre=="p2"){
                url+='DJOPlanilla2/show';
            }else if(nombre=="p3"){
                url+='DJOPlanilla3/show';
            }else if(nombre=="p4"){
                url+='DJOPlanilla4/show';
            }else if(nombre=="p5"){
                url+='DJOPlanilla5/show';
            }else if(nombre=="p6"){
                url+='DJOPlanilla6/show';
            }else if(nombre=="p7"){
                url+='DocAdicional/show';
            }else{
                url+='unknow';
            }

            var datos_enviados = {
                'det_declaracion_id' : det_declaracion_id,
                'layout' : false,
                'gen_declaracion_jo_id' : gen_declaracion_jo_id
            }

            var request = $.ajax({
            url: base + url ,
            method: "GET",
            data: datos_enviados,
            dataType: "html"
            });


            request.done(function( data ) {
				$(id_href).html(data);
				cargarObservacion();
            });

            request.fail(function( jqXHR, textStatus ) {
            alert( "Hubo un error: " + textStatus );
            });
        }

    }); /*fin cargador planilla*/

	//////////////////////////////////////////////////////////////////////////////////////
	//  CODIGO ARANCELARIO
    //////////////////////////////////////////////////////////////////////////////////////
    // Escuchando evento click para los botones de carga del arancel
    $(".cargar_arancel").on('click', function()
    { 
    	// Variables
	    var	idBoton = this.getAttribute('id'); //  boton arancel (boton_mer_0 o boton_nan_0)
	    var	indice = this.getAttribute('data-indice'); // numero de la fila	(0)	
	    var tipoArancel = this.getAttribute('data-tipo-arancel'); // tipo de boton arancel (nan o mer)
	    //
	    
	    // Variables de Validacion  
        var tituloArancelModal = (tipoArancel == "mer" ? "Arancel Mercosur" : "Arancel Nandina");
        var urlbase= document.location.origin; // url dominio
		var url= '/calificaciondjo/'; // url view arancel

		// Validamos la url de la busqueda para mercosur o nandina
		if(tipoArancel=='mer'){

			url+="ajax/arancelMercosur/index";

		}else{

			url+="ajax/arancelNandina/index";
		}
        // peticion ajax para traer la data
        var listaArancel = $.ajax({
            url: urlbase + url ,
            method: "GET",
            data: {'tipoArancel':tipoArancel},
            dataType: "html"
            });
        // fin ajax

        // Pintamos el Titulo del Modal
        	$('#arancel .modal-header #myModalLabel').html(tituloArancelModal);
	  	// fin Titulo
	  		
	  		// Si Obtenemos Respuesta
	  		listaArancel.done(function( data ) 
	  		{
					$('#arancel .modal-body').html(data);
					if(tipoArancel=='mer'){
								
						// Inicializando la tabla de codigos arancelarios Nandina				
						$('#listaMercosur').DataTable({
							"serverside":true,
							"ajax":"{{ url('api/arancelMercosur') }}",
							"columns":[
								
								{data:'id'},
								{data:'codigo'},
								{data:'descripcion'},
							]

						});
						//
							// Escuchando el evento click sobre la tabla de codigo arancelario 
							$('#listaMercosur tbody').on( 'click', 'tr', function () {
							    
							    var codigoObtenido=this.cells[1].innerHTML;

							    capturar(indice,tipoArancel,idBoton,codigoObtenido);
		    	
						    	delete indice;
						    	delete tipoArancel;
						    	delete idBoton;	

							} );
							//

					}else{

						// Inicializando la Tabla de codigos Arancelarios Nandina
						$('#listaNandina').DataTable({
							"serverside":true,
							"ajax":"{{ url('api/arancelNandina') }}",
							"asStripeClasses": [ 'codigo_nan_modal strip2'],
							"columns":[
								
								{data:'id'},
								{data:'codigo'},
								{data:'descripcion'},
							]

						});
						//
							// Escuchando el evento click sobre la tabla de codigo arancelario 
							$('#listaNandina tbody').on( 'click', 'tr', function () {
								var codigoObtenido=this.cells[1].innerHTML;
							    //alert( 'Clicked on: '+this.innerHTML );
							    //alert( 'Clicked on: '+this.cells[1].innerHTML);
							    capturar(indice,tipoArancel,idBoton,codigoObtenido);
		    	
						    	delete indice;
						    	delete tipoArancel;
						    	delete idBoton;							    
							} );
							//
					}		
	        });
	  		// Si no obtenemos una respuesta a la peticion de los aranceles
	        listaArancel.fail(function( jqXHR, textStatus ) 
	        {		
	        		data="<p class='text-danger'>Error procesando su peticion</p>"+ textStatus;
	            	 $('#arancel .modal-body').html(data);
	        });
	        //
	        
	     // Funcion para pintar el codigo en la celda correspondiente a la planilla
	    function capturar(indice,tipoArancel,idBoton,codigo="")
	    {
		
	    	// validamos donde colocar el codigo capturado	    	
	    	if(tipoArancel=="mer"){

	    		$('#'+idBoton+ ' i.glyphicon').addClass('glyphicon glyphicon-pencil');
	    		$('#cod_mer_'+indice).val(codigo);
	    		$('#cod_mer_'+indice).attr('readonly', 'true');
	    		
	    	}else{

	    		$('#'+idBoton+ ' i.glyphicon').addClass('glyphicon glyphicon-pencil');
	    		$('#cod_nan_'+indice).val(codigo);
	    		$('#cod_nan_'+indice).attr('readonly', 'true');
	    	}
	    	  // cerramos la ventana modal
	    	  $("#arancel .close").click();
		}

    });
    //////////////////////////////////////////////////////////////////////////////////////
	//  FIN CODIGO ARANCELARIO
    //////////////////////////////////////////////////////////////////////////////////////		

/** Cargar observacion correspondiente a una planilla **/
cargarObservacion=  function(){

	var elemento = $('#observacion');
	var det_declaracion_id= localStorage.getItem("det_declaracion_id");
	var nombre = localStorage.getItem("nombre_tag");
	var gen_declaracion_jo_id=$('#gen_declaracion_jo_edit_id').val();

	if(elemento.length && !nombre==false){

		var base= document.location.origin;
            var url= '/calificaciondjo/';
         
            if(nombre=="p2"){
                url+='DJOPlanilla2/observacion';
            }else if(nombre=="p3"){
                url+='DJOPlanilla3/observacion';
            }else if(nombre=="p4"){
                url+='DJOPlanilla4/observacion';
            }else if(nombre=="p5"){
                url+='DJOPlanilla5/observacion';
            }else if(nombre=="p6"){
                url+='DJOPlanilla6/observacion';
            }else if(nombre=="p7"){
                url+='DocAdicional/observacion';
            }else{
                url+='unknow';
            }

		var datos_enviados = {
					'det_declaracion_id' : det_declaracion_id,
					'gen_declaracion_jo_id': gen_declaracion_jo_id,
				}

				var request = $.ajax({
				url: base + url ,
				method: "GET",
				data: datos_enviados,
				dataType: "json"
				});


				request.done(function( data ) {
					elemento[0].value=data.observacion;
					if(!data.observacion==false){
						mostrarObservacion();
					}
				});

				request.fail(function( jqXHR, textStatus ) {
				alert( "Hubo un error: " + textStatus );
				});
	}

}

/** Guardar observacion correspondiente a la planilla que esta siendo visualizada **/
guardarObservacion= function(){

	var elemento = $('#observacion');
	var det_declaracion_id= localStorage.getItem("det_declaracion_id");
	var nombre = localStorage.getItem("nombre_tag");
	var gen_declaracion_jo_id=$('#gen_declaracion_jo_edit_id').val();

	if(elemento.length && !nombre==false){

		var base= document.location.origin;
            var url= '/calificaciondjo/';
         
            if(nombre=="p2"){
                url+='DJOPlanilla2/observacion';
            }else if(nombre=="p3"){
                url+='DJOPlanilla3/observacion';
            }else if(nombre=="p4"){
                url+='DJOPlanilla4/observacion';
            }else if(nombre=="p5"){
                url+='DJOPlanilla5/observacion';
            }else if(nombre=="p6"){
                url+='DJOPlanilla6/observacion';
            }else if(nombre=="p7"){
                url+='DocAdicional/observacion';
            }else{
                url+='unknow';
            }


		var datos_enviados = {
					"_token": "{{ csrf_token() }}",
					'det_declaracion_id' : det_declaracion_id,
					'observacion': elemento[0].value,
					'gen_declaracion_jo_id': gen_declaracion_jo_id,
				}

				var request = $.ajax({
				url: base + url ,
				method: "POST",
				data: datos_enviados,
				dataType: "json"
				});


				request.done(function( data ) {
					swal("Observación Agregada", "Ha agregado una observación en la planilla "+nombre, "success");
				});

				request.fail(function( jqXHR, textStatus ) {
				alert( "Hubo un error: " + textStatus );
				});
	}

}

/** Mostrar el campo observacion **/
mostrarObservacion = function(){
		var elemento = $('#observacion');
		var boton= $('#botonObservacion');

		if(elemento.length && boton.length){
			
			elemento[0].parentNode.parentNode.parentNode.classList.remove("hidden");

			boton[0].innerHTML="Guardar Observaci&oacute;n";
			boton[0].classList.remove("btn-warning");
			boton[0].classList.add("btn-info");
			boton[0].onclick=guardarObservacion;
		}
	}

/** Ocultar el campo observacion **/
ocultarObservacion = function(){
		var elemento = $('#observacion');
		var boton= $('#botonObservacion');

		if(elemento.length && boton.length){
			
			elemento[0].value="";
			elemento[0].parentNode.parentNode.parentNode.classList.add("hidden");

			boton[0].innerHTML="Agregar Observaci&oacute;n";
			boton[0].classList.remove("btn-info");
			boton[0].classList.add("btn-warning");
			boton[0].onclick=mostrarObservacion;
		}
	}
});



</script>

<script>
	/** script que cambie el texto del botón submit dependiendo de si existen obervaciones o no **/
	$(document).ready(function(){
	  $("#observacion, #observacion_analisis").keyup(function(){
		if($("#observacion").val() !== "" || $("#observacion_analisis").val() !== ""){
		$("#submit").text("Enviar observación");
		}else{
		$("#submit").text("Procesar");
		}
	  });
	  $("#closex, #closev").click(function(){
		if($("#observacion").val() !== "" || $("#observacion_analisis").val() !== ""){
		$("#submit").text("Enviar observación");
		}else{
		$("#submit").text("Procesar");
		}
	  });
	});
</script>

<script>
	//Validación de formulario, se puede llamar y pasar la sección que contenga los inputs a validar
	$(document).ready(function ()
	{
				var error = '<div class="msg_alert"><span style="color:red" class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span><strong id="cod_aran-error" class="added"> El campo es obligatorio.</strong></div>';
				  var valido = '<div class="msg_alert"><span style="color:green" class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span></div>';
				  var mens=['Estimado Usuario. Debe completar los campos solicitados',]
	
				  validacionForm = function() {
	
				  $('#validar_declaracion').submit(function(event) {
				  
				  var campos = $('#validar_declaracion').find('#fecha_emision, #fecha_vencimiento');
				  var n = campos.length;
				  var err = 0;
	
				  $("div").remove(".msg_alert");
				  //bucle que recorre todos los elementos del formulario
				  for (var i = 0; i < n; i++) {
					  var cod_input = $('#validar_declaracion').find('#fecha_emision, #fecha_vencimiento').eq(i);
					  if (!cod_input.attr('noreq')) {
						if (cod_input.val() == '' || cod_input.val() == null)
						{
						  err = 1;
						  cod_input.css('border', '1px solid red').after(error);
						}
						else{
						  if (err == 1) {err = 1;}else{err = 0;}
						  cod_input.css('border', '1px solid green').after(valido);
						}
						
					  }
				  }
	
				  //Si hay errores se detendrá el submit del formulario y devolverá una alerta
				  if(err==1){
					  event.preventDefault();
					  swal("Por Favor!", mens[0], "warning")
					}
	
					});
				  }
				});
	</script>

<!-- Modal Doc Soportes //////////////////////////////////////////////////////////////-->

<div id="docsoportes" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h3 class="modal-title text-primary"><b>Soporte de Usuario</b></h3>
      </div>
      <div class="modal-body">
        <div class="modal-body">
                <div role="tabpanel">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#cedual" class="" aria-controls="cedual" role="tab" data-toggle="tab" id="tab-inicial">Cédula</a>
                        </li>
                        <li role="presentation"><a href="#rif" class="" aria-controls="rif" role="tab" data-toggle="tab">Rif</a>
                        </li>
                        <li role="presentation"><a href="#registromer" class="" aria-controls="registromer" role="tab" data-toggle="tab">Registro Mercantil</a>
                        </li>
                    </ul>
                    <!-- Tab panes -->
                    </div>
                    <div class="tab-content">
                    	<div role="tabpanel" class="tab-pane active text-center" id="cedual">
							@if(file_exists(substr($rutacedula, 1)))
								@if(isset($extencion_file_1) && $extencion_file_1 == 'pdf')
							        <span class="text-success"></span>
							        <br>                                <embed id="fred" src="{{asset($rutacedula)}}" width="800" height="600" type="application/pdf">

							        @elseif(isset($extencion_file_1) && ($extencion_file_1 == 'docx' || $extencion_file_1 == 'doc'))
							          <br><br>
							          <a href="{{asset($rutacedula)}}" class="btn btn-lg btn-primary"><span><i class="glyphicon glyphicon-cloud-download"></i> </span>Descargar documento 1</a>
							          <br><br>
							        @else.
							    @endif
								@else
								<br>
								<div class="alert alert-warning" style="color:#fff"><strong>Atención!</strong> No es posible ubicar este fichero, por favor comunicarse con el administrador de sistemas.</div>
								@endif
			            </div>

                        <div role="tabpanel" class="tab-pane text-center" id="rif">
							@if(file_exists(substr($rutarif, 1)))
								@if(isset($extencion_file_1) && $extencion_file_1 == 'pdf')
							        <span class="text-success"></span>
							        <br>                                <embed id="fred" src="{{asset($rutarif)}}" width="800" height="600" type="application/pdf">

							        @elseif(isset($extencion_file_1) && ($extencion_file_1 == 'docx' || $extencion_file_1 == 'doc'))
							          <br><br>
							          <a href="{{asset($rutarif)}}" class="btn btn-lg btn-primary"><span><i class="glyphicon glyphicon-cloud-download"></i> </span>Descargar documento 1</a>
							          <br><br>
							        @else.
							    @endif
								@else
								<br>
								<div class="alert alert-warning" style="color:#fff"><strong>Atención!</strong> No es posible ubicar este fichero, por favor comunicarse con el administrador de sistemas.</div>
								@endif
                        </div>
                        <div role="tabpanel" class="tab-pane text-center" id="registromer">
							@if(file_exists(substr($rutaregistromer, 1)))
								@if(isset($extencion_file_1) && $extencion_file_1 == 'pdf')
							        <span class="text-success"></span>
							        <br>                                <embed id="fred" src="{{asset($rutaregistromer)}}" width="800" height="600" type="application/pdf">

							        @elseif(isset($extencion_file_1) && ($extencion_file_1 == 'docx' || $extencion_file_1 == 'doc'))
							          <br><br>
							          <a href="{{asset($rutaregistromer)}}" class="btn btn-lg btn-primary"><span><i class="glyphicon glyphicon-cloud-download"></i> </span>Descargar documento 1</a>
							          <br><br>
							        @else.
							   @endif
							   @else
							   <br>
							   <div class="alert alert-warning" style="color:#fff"><strong>Atención!</strong> No es posible ubicar este fichero, por favor comunicarse con el administrador de sistemas.</div>
							   @endif
                        </div>
                    </div>
                </div>
            </div>
      </div>
      <div class="modal-footer">
      </div>
    </div>

  </div>
</div>

<!-- Fin Modal Doc Soportes //////////////////////////////////////////////////////////-->
@stop