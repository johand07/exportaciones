<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CatTiposUsuarios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('cat_tipo_usuario',function(Blueprint $table){
          $table->engine = 'InnoDB';
          $table->increments('id');
          $table->string('nombre');
          $table->string('descripcion');
          $table->boolean('bactivo')->default(1);
          $table->timestamps();



        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::dropIfExists('cat_tipo_usuario');
    }
}
