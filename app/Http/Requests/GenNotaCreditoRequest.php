<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GenNotaCreditoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        
           'gen_consignatario_id'=>'required',
           'numero_nota_credito'=>'required|unique:gen_nota_credito,numero_nota_credito',
           'femision'=>'required',
           'concepto_nota_credito'=>'required',
           'monto_nota_credito'=>'required',
           'gen_factura_id'=>'required',
           'justificacion'=>'required',
        ];
    }
}
