<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CostosDirectos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
          Schema::create('costos_directos', function (Blueprint $table) {
          $table->engine = 'InnoDB';
          $table->increments('id');
          $table->unsignedInteger('planilla_3_id')->index()->nullable();
          $table->foreign('planilla_3_id')->references('id')->on('planilla_3');
          $table->double('mate_importados',100)->nullable();
          $table->double('mate_nacional',100)->nullable();
          $table->double('mano_obra',100)->nullable();
          $table->double('depreciacion',100)->nullable();
          $table->double('otros_costos_dir',100)->nullable();
          $table->double('total',100)->nullable();  
          $table->boolean('bactivo')->default(1);
          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('costos_directos'); 
    }
}
