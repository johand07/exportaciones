
/*  Insert catalogo Bolivia-Vzla */

Insert into cat_bolivia_vzla (CODIGO,DESCRIPCION) values ('3-A','Anexo II,Artículo 3,literal a)');
Insert into cat_bolivia_vzla (CODIGO,DESCRIPCION) values ('3-B','Anexo II,Artículo 3,literal b)');
Insert into cat_bolivia_vzla (CODIGO,DESCRIPCION) values ('3-C','Anexo II,Artículo 3,literal c)');
Insert into cat_bolivia_vzla (CODIGO,DESCRIPCION) values ('3-D','Anexo II,Artículo 3,literal d)');
Insert into cat_bolivia_vzla (CODIGO,DESCRIPCION) values ('3-E','Anexo II,Artículo 3,literal e)');
Insert into cat_bolivia_vzla (CODIGO,DESCRIPCION) values ('3-F','Anexo II,Artículo 3,literal f)');
Insert into cat_bolivia_vzla (CODIGO,DESCRIPCION) values ('10','Anexo II,Artículo 10°');
Insert into cat_bolivia_vzla (CODIGO,DESCRIPCION) values ('N','No califica Origen Venezolano para ese Acuerdo o País');
Insert into cat_bolivia_vzla (CODIGO,DESCRIPCION) values ('*','Información insuficiente para la determinación del Origen');
/*  Insert catalogo Colombia-Vzla */

Insert into cat_colombia_vzla (CODIGO,DESCRIPCION) values ('3-1A','Anexo II,Artículo 3,párrafo 1 literal a)');
Insert into cat_colombia_vzla (CODIGO,DESCRIPCION) values ('3-1B','Anexo II,Artículo 3,párrafo 1 literal b)');
Insert into cat_colombia_vzla (CODIGO,DESCRIPCION) values ('3-1C','Anexo II,Artículo 3,párrafo 1 literal c)');
Insert into cat_colombia_vzla (CODIGO,DESCRIPCION) values ('3-1Di','Anexo II,Artículo 3,párrafo 1 literal d),inciso i');
Insert into cat_colombia_vzla (CODIGO,DESCRIPCION) values ('3-1Dii','Anexo II,Artículo 3,párrafo 1 literal d),inciso ii');
Insert into cat_colombia_vzla (CODIGO,DESCRIPCION) values ('3-1Diii','Anexo II,Artículo 3,párrafo 1 literal d),inciso iii');
Insert into cat_colombia_vzla (CODIGO,DESCRIPCION) values ('7','Anexo II,Articulo 7°');
Insert into cat_colombia_vzla (CODIGO,DESCRIPCION) values ('N','No califica Origen Venezolano para ese Acuerdo o País');
Insert into cat_colombia_vzla (CODIGO,DESCRIPCION) values ('*','Información insuficiente para la determinación del Origen');


/*  Insert catalogo Peru-Vzla */

Insert into cat_peru_vzla (CODIGO,DESCRIPCION) values ('3-1A','Anexo II,Artículo 3 párrafo 1 literales a) al h),(según sea el caso)');
Insert into cat_peru_vzla (CODIGO,DESCRIPCION) values ('3-1B','Anexo II,Artículo 3 párrafo 1 literales a) al h),(según sea el caso)');
Insert into cat_peru_vzla (CODIGO,DESCRIPCION) values ('3-1C','Anexo II,Artículo 3 párrafo 1 literales a) al h),(según sea el caso)');
Insert into cat_peru_vzla (CODIGO,DESCRIPCION) values ('3-1D','Anexo II,Artículo 3 párrafo 1 literales a) al h),(según sea el caso)');
Insert into cat_peru_vzla (CODIGO,DESCRIPCION) values ('3-1E','Anexo II,Artículo 3 párrafo 1 literales a) al h),(según sea el caso)');
Insert into cat_peru_vzla (CODIGO,DESCRIPCION) values ('3-1F','Anexo II,Artículo 3 párrafo 1 literales a) al h),(según sea el caso)');
Insert into cat_peru_vzla (CODIGO,DESCRIPCION) values ('3-1G','Anexo II,Artículo 3 párrafo 1 literales a) al h),(según sea el caso)');
Insert into cat_peru_vzla (CODIGO,DESCRIPCION) values ('3-1H','Anexo II,Artículo 3 párrafo 1 literales a) al h),(según sea el caso)');
Insert into cat_peru_vzla (CODIGO,DESCRIPCION) values ('3-2','Anexo II,Artículo 3 párrafo 2');
Insert into cat_peru_vzla (CODIGO,DESCRIPCION) values ('3-3','Anexo II,Artículo 3 párrafo 3');
Insert into cat_peru_vzla (CODIGO,DESCRIPCION) values ('3-4','Anexo II,Artículo 3 párrafo 4');
Insert into cat_peru_vzla (CODIGO,DESCRIPCION) values ('3-5','Anexo II,Artículo 3 párrafo 5');
Insert into cat_peru_vzla (CODIGO,DESCRIPCION) values ('3-6','Anexo II,Artículo 3 párrafo 6');
Insert into cat_peru_vzla (CODIGO,DESCRIPCION) values ('8','Artículo 8°');
Insert into cat_peru_vzla (CODIGO,DESCRIPCION) values ('N','No califica Origen Venezolano para ese Acuerdo o País');
Insert into cat_peru_vzla (CODIGO,DESCRIPCION) values ('*','Información insuficiente para la determinación del Origen');


/*  Insert catalogo Cuba-Vzla */

Insert into cat_cuba_vzla (CODIGO,DESCRIPCION) values ('3-1A','Anexo I,Artículo 3 párrafo 1, literales a) al h),(según sea el caso)');
Insert into cat_cuba_vzla (CODIGO,DESCRIPCION) values ('3-1B','Anexo I,Artículo 3 párrafo 1, literales a) al h),(según sea el caso)');
Insert into cat_cuba_vzla (CODIGO,DESCRIPCION) values ('3-1C','Anexo I,Artículo 3 párrafo 1, literales a) al h),(según sea el caso)');
Insert into cat_cuba_vzla (CODIGO,DESCRIPCION) values ('3-1D','Anexo I,Artículo 3 párrafo 1, literales a) al h),(según sea el caso)');
Insert into cat_cuba_vzla (CODIGO,DESCRIPCION) values ('3-1E','Anexo I,Artículo 3 párrafo 1, literales a) al h),(según sea el caso)');
Insert into cat_cuba_vzla (CODIGO,DESCRIPCION) values ('3-1F','Anexo I,Artículo 3 párrafo 1, literales a) al h),(según sea el caso)');
Insert into cat_cuba_vzla (CODIGO,DESCRIPCION) values ('3-1G','Anexo I,Artículo 3 párrafo 1, literales a) al h),(según sea el caso)');
Insert into cat_cuba_vzla (CODIGO,DESCRIPCION) values ('3-1H','Anexo I,Artículo 3 párrafo 1, literales a) al h),(según sea el caso)');
Insert into cat_cuba_vzla (CODIGO,DESCRIPCION) values ('3-2','Anexo I,Artículo 3 párrafo 2');
Insert into cat_cuba_vzla (CODIGO,DESCRIPCION) values ('3-4A','Anexo I,Artículo 3 párrafo 4,literal a)');
Insert into cat_cuba_vzla (CODIGO,DESCRIPCION) values ('3-4B','Anexo I,Artículo 3 párrafo 4 literal b)');
Insert into cat_cuba_vzla (CODIGO,DESCRIPCION) values ('3-4C','Anexo I,Artículo 3, párrafo 4, literal c)');
Insert into cat_cuba_vzla (CODIGO,DESCRIPCION) values ('4-1','Anexo I,Artículo 4, párrafo 1');
Insert into cat_cuba_vzla (CODIGO,DESCRIPCION) values ('8','Artículo 8°');
Insert into cat_cuba_vzla (CODIGO,DESCRIPCION) values ('N','No califica Origen Venezolano para ese Acuerdo o País');
Insert into cat_cuba_vzla (CODIGO,DESCRIPCION) values ('*','Información insuficiente para la determinación del Origen');



/*  Insert catalogo aladi-tp */

Insert into cat_ald_tp (CODIGO,DESCRIPCION) values ('A','literal a)');
Insert into cat_ald_tp (CODIGO,DESCRIPCION) values ('B','literal b)');
Insert into cat_ald_tp (CODIGO,DESCRIPCION) values ('C','literal c)');
Insert into cat_ald_tp (CODIGO,DESCRIPCION) values ('D','literal d)');
Insert into cat_ald_tp (CODIGO,DESCRIPCION) values ('E','literal e)');
Insert into cat_ald_tp (CODIGO,DESCRIPCION) values ('2','Artículo 2°');
Insert into cat_ald_tp (CODIGO,DESCRIPCION) values ('N','No califica Origen Venezolano para ese Acuerdo o País');
Insert into cat_ald_tp (CODIGO,DESCRIPCION) values ('*','Información insuficiente para la determinación del Origen');

/*  Insert catalogo can-mercosur */

Insert into cat_can_mercosur (CODIGO,DESCRIPCION) values ('2A','Artículo 2°,literal a)');
Insert into cat_can_mercosur (CODIGO,DESCRIPCION) values ('2C','Artículo 2°,literal c)');
Insert into cat_can_mercosur (CODIGO,DESCRIPCION) values ('4A','Artículo 4°,literal a)');
Insert into cat_can_mercosur (CODIGO,DESCRIPCION) values ('4B','Artículo 4°,literal b)');
Insert into cat_can_mercosur (CODIGO,DESCRIPCION) values ('4C','Artículo 4°,literal c)');
Insert into cat_can_mercosur (CODIGO,DESCRIPCION) values ('5','Artículo 5°');
Insert into cat_can_mercosur (CODIGO,DESCRIPCION) values ('N','No califica Origen Venezolano para ese Acuerdo o País');
Insert into cat_can_mercosur (CODIGO,DESCRIPCION) values ('*','Información insuficiente para la determinación del Origen');

