@extends('templates/layoutlte')
@section('content')


{{Form::model($productos_tecno,['route'=>['productos_tecno.update',$id],'id'=>'formProdTecno','method'=>'PATCH','onSubmit="validarProductosTecno(); return false"'])}}


@php $contador=count($productos_tecno); $x=array(); @endphp
{{Form::hidden('metodo','Editar')}}
{{Form::hidden('contador',$contador)}}

 


 @include('productos_tecno.fields')
 


@push('productos_tecno')


 var i= parseInt($('input[name="contador"]').val());

 $('#add').click(function(){
 i++;
 var row = 'row'+i;



$('#fields_tec tr:last').after('<tr id="row'+i+'" display="block" class="show_div"><td>{!!Form::text('cod[]','0000.00.0',['class'=>'form-control','readonly' => 'readonly'])!!}</td><td>{!!Form::text('descripcion[]',null,['class'=>'form-control'])!!}</td><td>{!!Form::text('cantidad[]',null,['class'=>'form-control format','id'=>'cantidadFob'])!!}</td><td>{!!Form::text('precio[]',null,['class' => 'form-control format text-right','id'=>'precioFob'])!!}</td><td>{!!Form::text('valor_fob[]',null,['class' => 'form-control format text-right','readOnly'=>'readOnly','id'=>'montoFob'])!!}</td><td>{!!Form::select('unidad_medida_id[]',$medidas,null,['class'=>'form-control','placeholder'=>'Seleccione','id'=>'unidad_medida_id'])!!}</td><td>{!!Form::hidden('id[]',0)!!}<button  name="remove" type="button" id="'+i+'" value="" class="btn btn-danger btn-remove">x</button></td></tr>');

var main = document.getElementById(row);

var main = document.getElementById(row);
 main.querySelectorAll('#cantidadFob')[0].setAttribute("onInput", "calcularMonto('"+row+"','cantidadFob','precioFob','x')");

  main.querySelectorAll('#precioFob')[0].setAttribute("onInput", "calcularMonto('"+row+"','cantidadFob','precioFob','x')");


       $(".format").on({
  
      "keypress": function(e) {
        
           key = e.keyCode || e.which;
           tecla = String.fromCharCode(key).toLowerCase();
           letras = "0123456789.";
           especiales = "8-37-39-46";

           tecla_especial = false
           for(var i in especiales){
                if(key == especiales[i]){
                    tecla_especial = true;
                    break;
                }
            }

            if(letras.indexOf(tecla)==-1 && !tecla_especial){
                return false;
            }

          }

        }); 


      });

 
 calcularMonto('row1','cantidadFob','precioFob','x');

@endpush

<script src="{{asset('js/ajax_v2.1.1.js')}}"></script>

<script type="text/javascript">
  
$(document).ready(function(){

  @stack('productos_tecno');

});

</script>


@stop
