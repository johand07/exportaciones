<?php

use Illuminate\Database\Seeder;
use App\Models\CatSectorInversionista;

class cat_sector_inversionista extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sectorInv = [
            [
                'id'         => 1,
                'nombre'       => 'Productos alimenticios y animales vivos',
                'created_at' => '2021-06-15 13:59:08',
                'updated_at' => '2021-06-15 13:59:08',
            ],
            [
                'id'         => 2,
                'nombre'       => 'Bebidas y tabacos',
                'created_at' => '2021-06-15 13:59:08',
                'updated_at' => '2021-06-15 13:59:08',
            ],
            [
                'id'         => 3,
                'nombre'       => 'Materiales crudos no comestibles, excepto los combustibles',
                'created_at' => '2021-06-15 13:59:08',
                'updated_at' => '2021-06-15 13:59:08',
            ],
            [
                'id'         => 4,
                'nombre'       => 'Combustibles y lubricantes minerales y productos conexos',
                'created_at' => '2021-06-15 13:59:08',
                'updated_at' => '2021-06-15 13:59:08',
            ],
            [
                'id'         => 5,
                'nombre'       => 'Aceites, grasas y ceras de origen animal y vegetal',
                'created_at' => '2021-06-15 13:59:08',
                'updated_at' => '2021-06-15 13:59:08',
            ],
            [
                'id'         => 6,
                'nombre'       => 'Productos químicos y productos conexos, n.e.p.',
                'created_at' => '2021-06-15 13:59:08',
                'updated_at' => '2021-06-15 13:59:08',
            ],
            [
                'id'         => 7,
                'nombre'       => 'Artículos manufacturados, clasificados principalmente según el material',
                'created_at' => '2021-06-15 13:59:08',
                'updated_at' => '2021-06-15 13:59:08',
            ],
            [
                'id'         => 8,
                'nombre'       => 'Maquinaria y equipo de transporte',
                'created_at' => '2021-06-15 13:59:08',
                'updated_at' => '2021-06-15 13:59:08',
            ],
            [
                'id'         => 9,
                'nombre'       => 'Artículos manufacturados diversos',
                'created_at' => '2021-06-15 13:59:08',
                'updated_at' => '2021-06-15 13:59:08',
            ],
            [
                'id'         => 10,
                'nombre'       => 'Mercancías y operaciones no clasificadas en otro rubro de la CUCI',
                'created_at' => '2021-06-15 13:59:08',
                'updated_at' => '2021-06-15 13:59:08',
            ],
            
        ];

        CatSectorInversionista::insert($sectorInv);
    }
}
