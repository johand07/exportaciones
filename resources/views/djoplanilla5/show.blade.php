@extends('templates/layoutlte')
@section('content')
<div class="content">
<div class="panel-group" style="background-color: #fff;">
      <div class="panel-primary">
        <div class="panel-heading"> <h3>(P-5) -  7. Proceso productivo gráfico del producto a exportar</h3></div>
        <div class="panel-body">


                  <div class="row" style="background-color: #fff">
                      <div class="col-md-12">

                          <div class="row">
                             
                              <div class="col-md-12">
                                  <table border="1" class="col-md-12">
                                    <thead>
                                      <tr>
                                        <th class="text-center" width="100%" height="40px">Diagrama del proceso productivo</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      <tr>
                                        <td class="text-center" valign="top" >
                                          @if(file_exists(substr($planilla5->diagrama_proceso_productivo, 1)))
                                            @if($extencion == 'pdf' || $extencion == 'docx' || $extencion == 'doc')
                                            <iframe id="fred" style="border:1px solid #666CCC" title="PDF in an i-Frame" src="{{asset($planilla5->diagrama_proceso_productivo)}}" frameborder="1" scrolling="auto" height="780" width="600" ></iframe>

                                            @else
                                              <img src="{{asset($planilla5->diagrama_proceso_productivo)}}" alt="diagrama_proceso_productivo"  width="300px"/>
                                            @endif
                                            @else
                                            <br>
                                            <div class="alert alert-warning" style="color:#fff"><strong>Atención!</strong> No es posible ubicar este fichero, por favor comunicarse con el administrador de sistemas.</div>
                                            @endif
                                              
                                        </td>
                                      </tr>
                                    </tbody>
                                  </table>
                              </div>
                          </div>             



                      </div>
                  </div>

        </div>
      </div>
    </div>

  </div><!-- Fin content -->

<!--script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script-->


@endsection