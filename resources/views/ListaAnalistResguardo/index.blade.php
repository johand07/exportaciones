@extends('templates/layoutlte_analist_resguardo')
@section('content')

<div class="container" style="background-color: #fff">
    <div class="panel-group" style="background-color: #fff;">
      <div class="panel-primary">
        <div class="panel-heading"><h3>SOLICTUDES DE RESGUARDO ADUANERO Y CNA</h3></div>
        <div class="panel-body">
            <div class="row">
              <div class="col-md-12">
                <div class="row">
                  <div class="col-md-8">
                    <h4 class="text-info">Bandeja de Solicitudes:</h4><hr>
                  </div>
                <div class="col-md-2"></div>
                <div class="col-md-2">

                    <a class="btn btn-primary" href="{{route('ListaAnalistResguardo.index')}}">
                      <span class="glyphicon glyphicon-refresh" title="Agregar" aria-hidden="true"></span>
                        Actualizar
                    </a>

                  </div>
                </div>
                <div>
			        <table id="listaResguardo" class="table table-striped table-bordered" style="width:100%">
			            <thead>
			                <tr>
			                    <th>Nro. solicitud</th>
			                    <th>Fecha de Emisión</th>
			                    <th>Estatus</th>
			                    <th>Razon social</th>
			                    <th>Rif</th>
			                    <th>Acciones</th>
			                </tr>
			            </thead>
			            <tbody>
			            @foreach($sol_resguardo as $resguardo)
			                <tr>                   
			                    <td>{{$resguardo->num_sol_resguardo}}</td>
			                    <td>{{$resguardo->created_at}}</td>
			                    <td>{{$resguardo->rEstatusResguardo->nombre_status}}</td>
			                    <td>{{$resguardo->rUsuario->detUsuario->razon_social}}</td>
			                    <td>{{$resguardo->rUsuario->detUsuario->rif}}</td>

					            <td>
		                           <!--Atender Solicitud-->
		                            @if($resguardo->status_resguardo==9 )
		                              <a href="{{ route('ListaAnalistResguardo.edit',$resguardo->id)}}" class="btn btn-sm btn-danger" id="atender"><i class="glyphicon glyphicon-user"></i><b> Atender</b></a>
		                            @endif

		                            @if($resguardo->status_resguardo==10 )
		                              <a href="{{ route('ListaAnalistResguardo.edit',$resguardo->id)}}" class="btn btn-sm btn-warning" id="atender"><i class="glyphicon glyphicon-user"></i><b> Continuar</b></a>
		                            @endif
		                           <!--Fin Atender Solicitud-->

		                           <!--Corregida por el Usuario-->
		                            @if($resguardo->status_resguardo==18 || $resguardo->status_resguardo==20)
		                              <a href="{{ route('ListaAnalistResguardo.edit',$resguardo->id)}}" class="btn btn-sm btn-primary" id="atender"><i class="glyphicon glyphicon-user"></i><b>Reatender</b></a>
		                            @endif


		                            @if($resguardo->status_resguardo==15)@endif
		                     
		                        
		                       
		                            @if($resguardo->status_resguardo==22)
		                            	<a href="{{ route('CertificadoReguardoAduanero.pdf',['cerId'=>$resguardo->id])}}" target="_blank" class="text-center btn btn-sm btn-success list-inline"><i class="glyphicon glyphicon-download"></i> <b>Descargar PDF</b></a>
		                            @endif


		                            @if($resguardo->status_resguardo==24)@endif

		                                



		                      	</td>
			                </tr>
			             @endforeach
			            </tbody>
			        </table>
                </div>
              </div>
            </div><!-- Cierre 1° Row-->
        </div>
      </div>
    </div>
</div><!-- fin content-->

@stop