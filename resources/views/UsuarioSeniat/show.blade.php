@extends('templates/layoutlte_seniat')

@section('content')
<div class="row"> 
    <div class="col-md-12">
        <div id="datos_persona1">
            <div class="panel panel-primary">
            <div class="panel-heading">CERTIFICADO DE ORIGEN</div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-6">
                            <label>Código Arancelario</label><br>
                            <strong>{{ $certificado->codigo_arancelario }}</strong><br><br><br>





@if($certificado->gen_status_id == 29)
                        <label>Archivo</label><br>
                            <a href="{{ asset($file->file) }}" download="Certificado" class="btn btn-primary btn-lg">Certificado de Origen</a>
@elseif($certificado->gen_status_id == 28)
<b>Certificado Desaprobrado</b>
@elseif($certificado->gen_status_id == 27)
<b>En espera de Aprobacion</b>
@endif
                            
                        </div>
                    </div>
                    @if(isset($certificado->observacion_analista))
                    <div class="row"><br><br>
                        <div class="col-md-6">
                            <label>Observación del Analista</label><br>
                            <p>{{ $certificado->observacion_analista }}</p>
                        </div>
                    </div>
                   @endif
                </div>
            </div>
        </div>
    </div>
</div>
@stop