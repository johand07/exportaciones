@extends('templates/layoutlte')
@section('content')
{{Form::open(['route' =>'factura.store' ,'method'=>'POST','id'=>'formFactura'])}}
@include('factura.fields')
{{Form::close()}}
<script src="{{asset('js/ajax_v2.1.1.js')}}"></script>
<script>
$(document).ready(function(){
    // escucha el evento input del campo nombre
    $('#numero_factura').on('input', function(){
        // valida el campo nombre
        var nombre = $('#numero_factura').val();
        if(nombre == ''){
            alert('El campo nombre es obligatorio.');
            return false;
        }
    });
    // escucha el evento submit del formulario miFormulario
    $('#formFactura').submit(function(e){
        // cancela el envío del formulario si la validación del campo nombre falla
        var nombre = $('#numero_factura').val();
        if(nombre == ''){
            alert('El campo nombre es obligatorio.');
            return false;
        }
    });
});
</script>

@stop
