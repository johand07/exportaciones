<?php

namespace App\Http\Controllers;

use App\Models\BandejaAnalisisCertificado;
use App\Models\GenCertificado;
use App\Models\GenStatus;
use App\Models\DetUsuario;
use App\Models\DetProductosCertificado;
use App\Models\DetDeclaracionCertificado;
use App\Models\ImportadorCertificado;
use App\Models\DocumentosCertificado;
use Illuminate\Http\Request;
use Auth;
use Laracasts\Flash\Flash;
use Alert;
use Illuminate\Support\Facades\DB;

class AnalistaCertificadOrigenController extends Controller
{
    /**
     * Display a listing of the resource. rDetProductosCertificado
     * Display a listing of the resource.

     *
     * @return \Illuminate\Http\Response
     */
    public function index()


    {

        $certificado=GenCertificado::where('bactivo',1)->get();
        $titulo= 'Certificado de Origen';
        $descripcion= 'Perfil del Analista';
        return view('AnalistaCertificadoOrigen.index',compact('titulo','descripcion','certificado'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\BandejaAnalisisCertificado  $bandejaAnalisisCertificado
     * @return \Illuminate\Http\Response
     */
    public function show(BandejaAnalisisCertificado $bandejaAnalisisCertificado, $id)
    {
        $titulo= 'Verificación Certificado de Origen';
        $descripcion= 'Perfil del Analista';

        $certificados=GenCertificado::find($id);

        $validaratencioncert=BandejaAnalisisCertificado::where('gen_certificado_id',$certificados->id)->where('gen_usuario_id',Auth::user()->id)->first();

        $tipocertificado=$certificados->gen_tipo_certificado_id;

         /********Consulta de los estatus de observacion del coordinador***/

         $estado_observacion=GenStatus::whereIn('id', [11,12,13])->pluck('nombre_status','id');


         $det_usuario=DetUsuario::where('gen_usuario_id',$certificados->gen_usuario_id)->first();

         $detalleProduct=DetProductosCertificado::where('gen_certificado_id',$certificados->id)->get();

        /************Consulta del detalle de Declaracion certificado para traerme todas***/

          $detdeclaracionCert=DetDeclaracionCertificado::where('gen_certificado_id',$certificados->id)->get();


        /************Consulta para traerme los datos del importador certificado para traerme todas***/

        $importadorCertificado=ImportadorCertificado::where('gen_certificado_id',$certificados->id)->first();

        //COnsulta para los Documentos  
         $docCertificados=DocumentosCertificado::where('gen_certificado_id',$certificados->id)->first();


         if(!empty($docCertificados['file_1'])){
             $exten_file_1=explode(".", $docCertificados['file_1']);
            $extencion_file_1 = $exten_file_1[1];
        } else {
            $extencion_file_1 = '';
        }
        if (!empty($docCertificados['file_2'])) {
            $exten_file_2=explode(".", $docCertificados['file_2']);
            $extencion_file_2 = $exten_file_2[1];
        }else {
            $extencion_file_2 = '';
        }
        if (!empty($docCertificados['file_3'])) {
            $exten_file_3=explode(".", $docCertificados['file_3']);
            $extencion_file_3 = $exten_file_3[1];
        }else {
            $extencion_file_3 = '';
        }
        if (!empty($docCertificados['file_4'])) {
            $exten_file_4=explode(".", $docCertificados['file_4']);
            $extencion_file_4 = $exten_file_4[1];
        }else {
            $extencion_file_4 = '';
        }
        if (!empty($docCertificados['file_5'])) {
            $exten_file_5=explode(".", $docCertificados['file_5']);
            $extencion_file_5 = $exten_file_5[1];
        }else {
            $extencion_file_5 = '';
        }
        if (!empty($docCertificados['file_6'])) {
            $exten_file_6=explode(".", $docCertificados['file_6']);
            $extencion_file_6 = $exten_file_6[1];
        }else {
            $extencion_file_6 = '';
        }
   

        return view('AnalistaCertificadoOrigen.show',compact('titulo','descripcion','estado_observacion','certificados','detalleProduct','detdeclaracionCert','importadorCertificado','det_usuario','validaratencioncert','extencion_file_1','extencion_file_2','extencion_file_3','extencion_file_4','extencion_file_5','extencion_file_6','docCertificados','tipocertificado'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\BandejaAnalisisCertificado  $bandejaAnalisisCertificado
     * @return \Illuminate\Http\Response
     */
    public function edit(BandejaAnalisisCertificado $bandejaAnalisisCertificado, $id)
    {
        $titulo= 'Verificación Certificado de Origen';
        $descripcion= 'Perfil del Analista';
       
        //$id=Auth::user()->id;

         $certificados=GenCertificado::find($id);
        
       //dd($certificados);
        //
        //
        $tipocertificado=$certificados->gen_tipo_certificado_id;
        // dd($tipocertificado);


        $validaratencioncert=BandejaAnalisisCertificado::where('gen_certificado_id',$certificados->id)->where('gen_usuario_id',Auth::user()->id)->first();
         //dd($validaratencioncert);

         if(empty($validaratencioncert)){
            /*Consulto mis solicitudes con el status id 10 atendido*/
            $certificados->gen_status_id=10;
            $certificados->save();
             /***Inserto en la bandeja solinversionista me traigo el id de esa solicitud con el usuario   analista le actualizo el status a 10 que es en Observacion la actualizo a la fecha de hoy****/
            $certificadosOrig= new BandejaAnalisisCertificado;
            $certificadosOrig->gen_certificado_id=$certificados->id;
            $certificadosOrig->gen_usuario_id=Auth::user()->id;
            $certificadosOrig->gen_status_id=10;
            $certificadosOrig->fstatus=date('Y-m-d');
            $certificadosOrig->save();

          }

          /********Consulta de los estatus de observacion del coordinador***/

          $estado_observacion=GenStatus::whereIn('id', [11,12,13])->pluck('nombre_status','id');


          $det_usuario=DetUsuario::where('gen_usuario_id',$certificados->gen_usuario_id)->first();
        
         // dd($det_usuario)
         /* if($tipocertificado == 9) {
             $detalleProduct=DB::table('det_productos_certificado as det')
                ->select('det.num_orden',//
                        'det.cantidad_segun_factura',//
                        'det.descripcion_comercial',//
                        'det.unidad_fisica',//
                        'cri.normas_criterio',//
                        'cri.normas_criterio',//
                        'cri.normas_criterio',//
                    'cri.numero_factura',//
                        'cri.f_fac_certificado'
                        
                        )
                ->join('det_declaracion_certificado as cri', 'det.gen_certificado_id', '=', 'cri.gen_certificado_id')//
                
                ->where('det.bactivo',1)
                ->where('det.gen_certificado_id',$id)
                ->get();
                //dd($detalleProduct);
          } */

          /************Consulta del detalle de productos certificado para traerme todas***/

          $detalleProduct=DetProductosCertificado::where('gen_certificado_id',$certificados->id)->get();
 //dd($detalleProduct);
          /************Consulta del detalle de Declaracion certificado para traerme todas***/

          $detdeclaracionCert=DetDeclaracionCertificado::where('gen_certificado_id',$certificados->id)->get();
    //dd($detdeclaracionCert);

        /************Consulta para traerme los datos del importador certificado para traerme todas***/

          $importadorCertificado=ImportadorCertificado::where('gen_certificado_id',$certificados->id)->first();
         //dd($importadorCertificado);
//COnsulta para los Documentos
         $docCertificados=DocumentosCertificado::where('gen_certificado_id',$certificados->id)->first();

        
         // dd($docCertificados);
        // 
        //$extencion_file_1 = $exten_file_1[1];

         if(!empty($docCertificados['file_1'])){
             $exten_file_1=explode(".", $docCertificados['file_1']);
            $extencion_file_1 = $exten_file_1[1];
        } else {
            $extencion_file_1 = '';
        }
        if (!empty($docCertificados['file_2'])) {
            $exten_file_2=explode(".", $docCertificados['file_2']);
            $extencion_file_2 = $exten_file_2[1];
        }else {
            $extencion_file_2 = '';
        }
        if (!empty($docCertificados['file_3'])) {
            $exten_file_3=explode(".", $docCertificados['file_3']);
            $extencion_file_3 = $exten_file_3[1];
        }else {
            $extencion_file_3 = '';
        }
        if (!empty($docCertificados['file_4'])) {
            $exten_file_4=explode(".", $docCertificados['file_4']);
            $extencion_file_4 = $exten_file_4[1];
        }else {
            $extencion_file_4 = '';
        }
        if (!empty($docCertificados['file_5'])) {
            $exten_file_5=explode(".", $docCertificados['file_5']);
            $extencion_file_5 = $exten_file_5[1];
        }else {
            $extencion_file_5 = '';
        }
        if (!empty($docCertificados['file_6'])) {
            $exten_file_6=explode(".", $docCertificados['file_6']);
            $extencion_file_6 = $exten_file_6[1];
        }else {
            $extencion_file_6 = '';
        }
   

        return view('AnalistaCertificadoOrigen.edit',compact('titulo','descripcion','estado_observacion','certificados','detalleProduct','detdeclaracionCert','importadorCertificado','det_usuario','validaratencioncert','extencion_file_1','extencion_file_2','extencion_file_3','extencion_file_4','extencion_file_5','extencion_file_6','docCertificados','tipocertificado'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\BandejaAnalisisCertificado  $bandejaAnalisisCertificado
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BandejaAnalisisCertificado $bandejaAnalisisCertificado, $id)
    {
        if(!empty($request->observacion_analista ) && $request->gen_status_id==11){
         
                 $certificados=GenCertificado::find($id)->update(['gen_status_id'=>11,'observacion_analista'=>$request->observacion_analista,'updated_at' => date("Y-m-d H:i:s") ]);

                 $bandejaCoordinadorCertificado=BandejaAnalisisCertificado::where('gen_certificado_id',$id)->update(['gen_status_id'=>11,'updated_at' => date("Y-m-d H:i:s") ]);

               
            }elseif(!empty($request->observacion_analista) && $request->gen_status_id==13){
              
              $certificados=GenCertificado::find($id)->update(['gen_status_id'=>13,'observacion_analista'=>$request->observacion_analista,'updated_at' => date("Y-m-d H:i:s") ]);

               $bandejaCoordinadorCertificado=BandejaAnalisisCertificado::where('gen_certificado_id',$id)->update(['gen_status_id'=>13,'updated_at' => date("Y-m-d H:i:s") ]);


           /* }elseif(!empty($request->observacion_analista ) && $request->gen_status_id == 15){
              
              $certificados=GenCertificado::find($id)->update(['gen_status_id'=>15,'observacion_analista'=>$request->observacion_analista,'updated_at' => date("Y-m-d H:i:s") ]);

               $bandejaCoordinadorCertificado=BandejaAnalisisCertificado::where('gen_certificado_id',$id)->update(['gen_status_id'=>15,'updated_at' => date("Y-m-d H:i:s") ]);*/

            }else{
              
              $certificados=GenCertificado::find($id)->update(['gen_status_id'=>12,'updated_at' => date("Y-m-d H:i:s") ]);

              $bandejaCoordinadorCertificado=BandejaAnalisisCertificado::where('gen_certificado_id',$id)->update(['gen_status_id'=>12,'updated_at' => date("Y-m-d H:i:s") ]);

            }

            Alert::warning('Su Solicitud ha sido Atendida ')->persistent("OK");
            return redirect()->action('AnalistaCertificadOrigenController@index'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\BandejaAnalisisCertificado  $bandejaAnalisisCertificado
     * @return \Illuminate\Http\Response
     */
    public function destroy(BandejaAnalisisCertificado $bandejaAnalisisCertificado)
    {
        //
    }
}
