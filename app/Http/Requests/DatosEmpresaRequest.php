<?php
  
namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DatosEmpresaRequest extends FormRequest
{
    /**    
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

       'razon_social'=>'required',    
       'siglas'=>'required',
       'rif'=>'required',
       'id_gen_sector'=>'required',
       'gen_actividad_eco_id'=>'required',
       'gen_tipo_empresa_id'=>'required',
       'export'=>'required',
       'pais_id'=>'required',
       'estado_id'=>'required',
       'municipio_id'=>'required',
       'parroquia_id'=>'required',
       'direccion'=>'required|max:200',
       'telefono_local'=>'required|numeric',
       'telefono_movil'=>'required|numeric',
       'circuns_judicial_id'=>'required',
       'num_registro'=>'required',
       'tomo'=>'required', 
       'folio'=>'required',
       'capital_social'=>'required|numeric',
       'f_registro_mer'=>'required',
       'nombre_presid'=>'required',
       'apellido_presid'=>'required',
       'ci_presid'=>'required',
       'cargo_presid'=>'required',
       'nombre_repre'=>'required',
       'apellido_repre'=>'required',
       'ci_repre'=>'required',
       'cargo_repre'=>'required',
       'accionista'=>'required',
       'nombre_accionista'=>'required',
       'apellido_accionista'=>'required',
       'cedula_accionista'=>'required',    
       'cargo_accionista'=>'required',
       'telefono_accionista'=>'required|numeric',
       'nacionalidad_accionista'=>'required',
       'participacion_accionista'=>'required|numeric',
       'correo_accionista'=>'required',
       'prod_tecnologia'=>'required',
       'tipo_arancel'=>'required',
       'cod_arancel'=>'required',
       'descrip_arancel'=>'required',
       'descrip_comercial'=>'required',
       'gen_unidad_medida_id'=>'required',
       'cap_opera_anual'=>'required|numeric',
       'cap_insta_anual'=>'required',
       'cap_alamcenamiento'=>'required|numeric',
       'correo'=>'required',
       'correo_sec'=>'required',
        
        ];
    }
}
