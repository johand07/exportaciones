<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Permiso extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permiso', function (Blueprint $table) {
           $table->engine = 'InnoDB';
           $table->increments('id');
           $table->unsignedInteger('tipo_permiso_id')->index()->nullable();
           $table->foreign('tipo_permiso_id')->references('id')->on('tipo_permiso');
           $table->unsignedInteger('gen_usuario_id')->index()->nullable();
           $table->foreign('gen_usuario_id')->references('id')->on('gen_usuario');
           $table->unsignedInteger('gen_aduana_salida_id')->index()->nullable();
           $table->foreign('gen_aduana_salida_id')->references('id')->on('gen_aduana_salida');
           $table->unsignedInteger('pais_id')->index()->nullable();
           $table->foreign('pais_id')->references('id')->on('pais');
           $table->string('numero_permiso', 50)->nullable();
           $table->double('monto_aprobado')->nullable();
           $table->date('fecha_permiso')->nullable();
           $table->date('fecha_aprobacion')->nullable();
           $table->boolean('bactivo')->default(1);
           $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::dropIfExists('permiso');
    }
}
