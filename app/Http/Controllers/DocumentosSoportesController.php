<?php

namespace App\Http\Controllers;
use Illuminate\Filesystem\Filesystem;
use App\Models\GenUsuario;
use App\Models\DetUsuario;
use Illuminate\Http\Request;

use Session;
use Auth;
use Alert;
use File;


class DocumentosSoportesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $detusuario=DetUsuario::where('gen_usuario_id',Auth::user()->id)->first();
        $titulo= 'Documentos';
        $descripcion= 'Peril del Exportador';/* nsantos*/




        return view('DocumentosSoportes.documentos',compact('titulo','descripcion','detusuario'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $destino = '/img/registerUser/';
        $destinoPrivado = public_path($destino);
        
        if (!file_exists($destinoPrivado)) {
            $filesystem = new Filesystem();
            $filesystem->makeDirectory($destinoPrivado, $mode = 0777, true, true); 
        }

        $rutaImg = '';
        $rutaImgRif= '';
        $rutaImgRegMerc= '';
        if(!empty($request->file('ruta_doc'))){                     

            $imagen = $request->file('ruta_doc');
            if (!empty($imagen)) {
                
                $nombre_imagen = $imagen->getClientOriginalName();
                
                $imagen->move($destinoPrivado, $nombre_imagen);
                
            }
            
            $rutaImg = $destino.$nombre_imagen;
        }
        if(!empty($request->file('ruta_rif'))){
            
            $ruta_rif = $request->file('ruta_rif');
            if (!empty($ruta_rif)) {
                
                $nombre_ruta_rif = $ruta_rif->getClientOriginalName();
                
                $ruta_rif->move($destinoPrivado, $nombre_ruta_rif);
                
            }
            
            $rutaImgRif = $destino.$nombre_ruta_rif;
        }

        if(!empty($request->file('ruta_reg_merc'))){
            
            $ruta_reg_merc = $request->file('ruta_reg_merc');
            if (!empty($ruta_reg_merc)) {
                
                $nombre_ruta_reg_merc = $ruta_reg_merc->getClientOriginalName();
                
                $ruta_reg_merc->move($destinoPrivado, $nombre_ruta_reg_merc);
                
            }
            
            $rutaImgRegMerc = $destino.$nombre_ruta_reg_merc;
        }


        $DetUser=DetUsuario::where('gen_usuario_id',Auth::user()->id)->first();
        $DetUser->ruta_doc = $rutaImg;
        $DetUser->ruta_rif = $rutaImgRif;
        $DetUser->ruta_reg_merc = $rutaImgRegMerc;
        $DetUser->save();        
                
        Alert::success('Se han guardado los documentos correspondientes!','Registrado Correctamente!')->persistent("Ok");

        return back();

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\GenUsuario  $genUsuario
     * @return \Illuminate\Http\Response
     */
    public function show(GenUsuario $genUsuario)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\GenUsuario  $genUsuario
     * @return \Illuminate\Http\Response
     */
    public function edit(GenUsuario $genUsuario, $id)
    {
          $titulo= 'Documentos';
        $descripcion= 'Peril del Exportador';/* nsantos*/

         $detusuario=DetUsuario::where('gen_usuario_id',Auth::user()->id)->first();

        return view('DocumentosSoportes.edit',compact('titulo','descripcion','detusuario'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\GenUsuario  $genUsuario
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, GenUsuario $genUsuario)
    {
        /*if(!$request->ruta_doc || !$request->ruta_rif || !$request->ruta_reg_merc){
            Alert::warning('Debe cargar los documentos solicitados!','Registro no Actualizado!')->persistent("Ok");

            return back();
        }*/

        $destino = '/img/registerUser/';
        $destinoPrivado = public_path($destino);
        
        if (!file_exists($destinoPrivado)) {
            $filesystem = new Filesystem();
$filesystem->makeDirectory($destinoPrivado, $mode = 0777, true, true); 
        }

        $rutaImg = '';
        $rutaImgRif= '';
        $rutaImgRegMerc= '';
        if(!empty($request->file('ruta_doc'))){                     

            $imagen = $request->file('ruta_doc');
            if (!empty($imagen)) {
                
                $nombre_imagen = $imagen->getClientOriginalName();
                
                $imagen->move($destinoPrivado, $nombre_imagen);
                
            }
            
            $rutaImg = $destino.$nombre_imagen;
        }
        if(!empty($request->file('ruta_rif'))){
            
            $ruta_rif = $request->file('ruta_rif');
            if (!empty($ruta_rif)) {
                
                $nombre_ruta_rif = $ruta_rif->getClientOriginalName();
                
                $ruta_rif->move($destinoPrivado, $nombre_ruta_rif);
                
            }
            
            $rutaImgRif = $destino.$nombre_ruta_rif;
        }

        if(!empty($request->file('ruta_reg_merc'))){
            
            $ruta_reg_merc = $request->file('ruta_reg_merc');
            if (!empty($ruta_reg_merc)) {
                
                $nombre_ruta_reg_merc = $ruta_reg_merc->getClientOriginalName();
                
                $ruta_reg_merc->move($destinoPrivado, $nombre_ruta_reg_merc);
                
            }
            
            $rutaImgRegMerc = $destino.$nombre_ruta_reg_merc;
        }


        $DetUser=DetUsuario::where('gen_usuario_id',Auth::user()->id)->first();
        if(!empty($request->file('ruta_doc'))){
            $DetUser->ruta_doc = $rutaImg;
        }
        if(!empty($request->file('ruta_rif'))){
            $DetUser->ruta_rif = $rutaImgRif;
        }
        if(!empty($request->file('ruta_reg_merc'))){
            $DetUser->ruta_reg_merc = $rutaImgRegMerc;
        }
        $DetUser->save();        
                
        Alert::success('Se han guardado los documentos correspondientes!','Registrado Correctamente!')->persistent("Ok");

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\GenUsuario  $genUsuario
     * @return \Illuminate\Http\Response
     */
    public function destroy(GenUsuario $genUsuario)
    {
        //
    }
}
