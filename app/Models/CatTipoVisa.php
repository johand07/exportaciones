<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CatTipoVisa extends Model

{
	 protected $table = 'cat_tipo_visa';

    protected $fillable = [

           'nombre_visa',
           'bactivo',
    ];

    protected $date =['created_at','updated_at'];
   
}
