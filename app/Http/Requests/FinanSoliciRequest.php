<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator;

class FinanSoliciRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

        
           
            
            
            //'anticipo'=>'required',
            'gen_anticipo_id'=>'required_if:anticipo,1',
            
            
            'cat_regimen_export_id'=>'required',
            
            'financiamiento'=>'required_if:cat_regimen_export_id,1',

            
            'cat_regimen_especial_id'=>'required_if:cat_regimen_export_id,2',
            'especifique_regimen'=>'required_if:cat_regimen_especial_id,3',
            'tipo_instrumento_id'=>'required_if:financiamiento,1',
            'entidad_financiera'=>'required_if:financiamiento,1',
            'nro_contrato'=>'required_if:financiamiento,1',
            'fapro_contrato'=>'required_if:financiamiento,1',
            'fvenci_contrato'=>'required_if:financiamiento,1',
            'monto_apro_contrato'=>'required_if:financiamiento,1',
            'num_cuota_contrato'=>'required_if:financiamiento,1',
            'monto_amort_contrato'=>'required_if:financiamiento,1',
            'num_cuota_amortizado'=>'required_if:desestimiento,2',
            'envio_muestra'=>'required',

         
        ];

                
    }

   /* public function messages()
{
    return [
        'gen_nota_credito_id.required_if' => 'Agrega el nombre del estudiante.',
        
    ];
}*/



}
