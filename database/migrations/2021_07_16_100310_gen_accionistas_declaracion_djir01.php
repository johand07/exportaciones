<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GenAccionistasDeclaracionDjir01 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gen_accionistas_declaracion_djir01', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('gen_declaracion_inversion_id');
            $table->foreign('gen_declaracion_inversion_id', 'gd_inversion_id_fk1')->references('id')->on('gen_declaracion_inversion');
            $table->string('razon_social_accionista', 150)->nullable();
            $table->string('identificacion_accionista', 150)->nullable();
            $table->string('nacionalidad_accionista', 150)->nullable();
            $table->string('capital_social_suscrito', 150)->nullable();
            $table->string('capital_social_pagado', 150)->nullable();
            $table->string('porcentaje_participacion', 150)->nullable();
            $table->boolean('bactivo')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gen_accionistas_declaracion_djir01');
    }
}
