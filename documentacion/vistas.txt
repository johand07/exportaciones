/***********************************DOCUMENTACION DE LAS VISTAS***************************************/

Las vistas en Laravel son la parte pública que el usuario de nuestro sistema va a poder ver, se escriben en HTML junto con un motor de plantillas llamado Blade.

Las vistas se almacenan en la carpeta resource/views dentro de views se debe creear un carpeta con el nombre del modulo a desarrollar, 
o en dado caso si ya exite se deberan de crear las vista que se vallan a utilizar (Nota: Estas Vista deden de tener la extension nombrevista.blade.php).

Entre las vistas mas concurrentes se encuentran:
(Nota: No necesariamente las vistas deben de llamarse como a continuación se escriben): 

1. Vista Index = Se crean los data table.
2. Vista Create = Se crean los formularios y se envian los datos para un controlador en el methodo Store para almacenar esos datos.
3. Vista Edit = Se cargan la misma vista create (Esta vista va a ulitizar el metodo PATCH y se debe cambiar la ruta a nombrecontrolador.update).
4. Vista Show = Muestra el contenido en una ventana de visualizacion.

Para imprimir variables dentro de una vista se utiliza {{ nombre variable }}.

 <div class="container">
 	<div class="content">
 		<div class="title">We are {{ $count }} Nombre</div>
 	</div>
 </div>

Se puede utilizar foreach nomenclatura @foreach (variable as alia)@endforeach

@foreach($bookings as $booking)
    <tr>
        @if($time->availble_times == $booking->booking_time)
            <td>{{$time->availble_times}}: not available</td>
        @else
            <tr><td>{{$time->availble_times}}</td>
        @endif
    </tr>
 @endforeach 

Se pueden recibiar variables desde enviadas desde un controlador.
