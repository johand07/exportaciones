<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PermisoProductos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('permiso_productos', function (Blueprint $table) {
           $table->engine = 'InnoDB';
           $table->increments('id');
           $table->unsignedInteger('permiso_id')->index()->nullable();
           $table->foreign('permiso_id')->references('id')->on('permiso');
           $table->unsignedInteger('gen_unidad_medida_id')->index()->nullable();
           $table->foreign('gen_unidad_medida_id')->references('id')->on('gen_unidad_medida');
           $table->string('codigo', 50)->nullable();
           $table->string('descripcion', 200)->nullable();
           $table->string('descrip_comercial', 200)->nullable();
           $table->string('cantidad', 50)->nullable();
           $table->boolean('bactivo')->default(1);
           $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('permiso_productos');
    }
}
