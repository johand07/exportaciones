@extends('templates/layoutlte')
@section('content')
<div class="content">
	{!! Form::open(['route' => 'CertExportaFacil.store' ,'method'=>'POST']) !!}
	<div class="panel panel-primary"><!--Panel para todos los item azules-->
		<div class="panel-heading"><h4>Código/Régimen</h4></div>
			<div class="panel-body">
				<div class="row">
					<div class="col-md-4">
						<div class="form-group">
		              		{!! Form::label('','Código')!!}
		              		{!! Form::text('codigo',null,['class'=>'form-control','id'=>'codigo','required'=>'true'])!!}
		           		 </div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
		              		{!! Form::label('','Régimen')!!}
		              		{!! Form::text('regimen_one',null,['class'=>'form-control','id'=>'regimen','required'=>'true'])!!}
		           		 </div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							{!! Form::label('','Fecha') !!}
							<div class="input-group date">
								{!! Form::text('fecha_reg',null,['class'=>'form-control','readonly'=>'readonly','id'=>'fecha']) !!}
								<span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
							</div>
		              		
		           		</div>
					</div>
				</div>
			</div><!--1er body-->

		<div class="panel-heading"><h4>Exportador/Remitente</h4></div>
			<div class="panel-body">
				<div class="row">
					<div class="col-md-4">
						<div class="form-group">
							{!!Form::label('','Nombre de Representante/Exportador')!!}
							
							{!!Form::text('nombre_repre_export',null,['class'=>'form-control','id'=>'nombre_repre_export','required'=>'true'])!!}	
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							{!!Form::label('','Teléfono Exportador')!!}
							
							{!!Form::text('telefono_exportador',null,['class'=>'form-control','id'=>'telefono_exportador','required'=>'true','onkeypress'=>'return soloNumeros(event)'])!!}	
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							{!!Form::label('','Email')!!}
							
							{!!Form::email('correo',null,['class'=>'form-control','id'=>'correo','required'=>'true','onkeyup'=>'minuscula(this.value,"#correo")'])!!}
						</div>	
					</div>
				</div><br>
				<div class="row">
					<div class="col-md-4">
						<div class="form-group">
							{!!Form::label('','Dirección')!!}
							
							{!!Form::text('direccion_emisor',null,['class'=>'form-control','id'=>'direccion_emisor','required'=>'true'])!!}	
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							{!!Form::label('','RUD')!!}
							
							{!!Form::text('rud',null,['class'=>'form-control','id'=>'rud','required'=>'true'])!!}	
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							{!!Form::label('','País Origen')!!}
							
							{!! Form::select('pais_origen',$pais,null,['class'=>'form-control', 'id'=>'pais_origen','placeholder'=>'Seleccione el pais']) !!}
						</div>	
					</div>
				</div><br>
				<div class="row">
					<div class="col-md-4">
						<div class="form-group">
							{!!Form::label('','Código Postal')!!}
							
							{!!Form::text('cod_postal',null,['class'=>'form-control','id'=>'cod_postal','required'=>'true'])!!}	
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							{!!Form::label('','Ciudad')!!}
							
							{!!Form::text('ciudad',null,['class'=>'form-control','id'=>'ciudad','required'=>'true'])!!}	
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							{!! Form::label('','Fecha Embarque') !!}
							<div class="input-group date">
								{!! Form::text('fecha_embarque',null,['class'=>'form-control','readonly'=>'readonly','id'=>'fecha_embarque']) !!}
								<span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
							</div>
		              		
		           		</div>
					</div>
				</div><br>
			</div><!-- 2do body -->

		<div class="panel-heading"><h4>Destinatario</h4></div>
			<div class="panel-body">
				<div class="row">
					<div class="col-md-4">
						<div class="form-group">
							{!!Form::label('','Destinatario')!!}
							
							{!!Form::text('destinario',null,['class'=>'form-control','id'=>'destinario','required'=>'true'])!!}	
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							{!!Form::label('','Teléfono Destinatario')!!}
							
							{!!Form::text('telefono_destinario',null,['class'=>'form-control','id'=>'telefono_destinario','required'=>'true','onkeypress'=>'return soloNumeros(event)'])!!}	
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							{!!Form::label('','Dirección')!!}
							
							{!!Form::text('direccion_receptor',null,['class'=>'form-control','id'=>'direccion_receptor','required'=>'true'])!!}
						</div>	
					</div>
				</div><br>
				<div class="row">
					<div class="col-md-4">
						<div class="form-group">
							{!!Form::label('','País Destino')!!}
							
							{!!Form::select('pais_id',$pais,null,['class'=>'form-control', 'id'=>'pais_id_inv','placeholder'=>'Seleccione el pais'])!!}
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							{!!Form::label('','Código Postal')!!}
							
							{!!Form::text('cod_postal_destino',null,['class'=>'form-control','id'=>'cod_postal_destino','required'=>'true'])!!}	
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							{!!Form::label('','Ciudad Destino')!!}
							
							{!!Form::text('ciudad_destino',null,['class'=>'form-control','id'=>'ciudad_destino','required'=>'true'])!!}
						</div>	
					</div>
				</div><br>
			</div><!-- 3er body -->


		<div class="panel-heading"><h4>Determinación de Impuestos</h4></div>
			<div class="panel-body">
				<div class="row">
					<div class="col-md-3">
						<div class="form-group">
							{!! Form::label('','Tipo Mercancia') !!}

				      		{!! Form::select('tipo_meracncia',$tipo_meracncia,null,['class'=>'form-control','id'=>'tipo_meracncia',
			         		 'placeholder'=>'Seleccione el Destino','required'=>'true']) !!}
			        	</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							{!!Form::label('','Total FOB')!!}
							
							{!!Form::text('total_fob',null,['class'=>'form-control','id'=>'total_fob','required'=>'true'])!!}	
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							{!!Form::label('','Total Peso (kg)')!!}
							
							{!!Form::text('peso_estimado',null,['class'=>'form-control','id'=>'peso_estimado','required'=>'true'])!!}	
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							{!!Form::label('','Alto')!!}
							
							{!!Form::text('alto',null,['class'=>'form-control','id'=>'alto','required'=>'true'])!!}	
						</div>
					</div>
				</div><br>
				<div class="row">
					<div class="col-md-4">
						<div class="form-group">
							{!!Form::label('','Ancho')!!}
							
							{!!Form::text('ancho',null,['class'=>'form-control','id'=>'ancho','required'=>'true'])!!}	
						</div>
					</div>
					<div class="col-md-4">
						 <div class="form-group">					
							{!! Form::label('','Tratamiento especial') !!}<br>

							{!! Form::label('', 'Sí',['class'=>'radio-inline']) !!}
							{!! Form::radio('tratamiento_especial', 'Si',false,['class'=>'radio-inline','id'=>'tratamiento_especial']) !!}
								
							{!! Form::label('', 'No',['class'=>'radio-inline']) !!}
							{!! Form::radio('tratamiento_especial', 'No',false,['class'=>'radio-inline','id'=>'tratamiento_especial']) !!}
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							{!!Form::label('','Total Bultos')!!}
							
							{!!Form::text('total_bultos',null,['class'=>'form-control','id'=>'total_bultos','required'=>'true'])!!}	
						</div>
					</div>
				</div><br>
				<div class="row">
					<div class="col-md-3">
						<div class="form-group">
							{!!Form::label('','Tipo de Servicio')!!}
							
							{!!Form::text('tipo_servicio',null,['class'=>'form-control','id'=>'tipo_servicio','required'=>'true'])!!}	
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							{!!Form::label('','Valor Total Flete sin I.V.A')!!}
							
							{!!Form::text('valor_total_flete_sinIva',null,['class'=>'form-control','id'=>'valor_total_flete_sinIva','required'=>'true'])!!}	
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							{!!Form::label('','Valor Total Flete con I.V.A')!!}
							
							{!!Form::text('valor_total_flete_conIva',null,['class'=>'form-control','id'=>'valor_total_flete_conIva','required'=>'true'])!!}
						</div>	
					</div>
					<div class="col-md-3">
						<div class="form-group">
							{!!Form::label('','Total Seguro')!!}
							
							{!!Form::text('total_seguro',null,['class'=>'form-control','id'=>'total_seguro','required'=>'true'])!!}
						</div>	
					</div>
				</div><br>
				
			</div><!-- 4arto body -->
		<div class="panel-heading"><h4>Descripción de la Mercancía</h4></div>
			<div class="panel-body">
					<div class="row"><br><!--Agregar productos-->
					
					<table class="table table-bordered text-left" id="productos">
						<tr> <td><button  name="add" type="button" id="add-productos" value="add more" class="btn btn-success" >Agregar</button></td></tr>

						<tr>
							<th>Serie</th>
							
							<th>Subpartida<br>
								Arancelaria</th>

							<th>Descripción
								<br>de la Mercancía
							</th>

							<th>Valor FOB</th>

							<th>Cantidad</th>
						
							<th>Fecha</th>

							<th>Régimen Aplicable</th>
						
						</tr>
						<tr id="row1"><!--row1 es para la lista dinamica de agregar productos Item Descripción de la Mercancíansantos-->
								<!--Lista dinamica Prodcutos-->
						
							<td>{!!Form::text('serie[]',null,['class'=>'form-control','id'=>'serie','required'=>'true'])!!}
							</td>

							<td>{!!Form::text('sub_partida_arancel[]',null,['class'=>'form-control','id'=>'sub_partida_arancel','required'=>'true'])!!}
							</td>

							<td>{!!Form::text('descripcion[]',null,['class'=>'form-control','id'=>'descripcion','required'=>'true'])!!}
							</td>

							<td>{!!Form::text('valor_fob[]',null,['class'=>'form-control','id'=>'valor_fob','required'=>'true'])!!}
							</td>

							<td>{!!Form::text('cantidad[]',null,['class'=>'form-control','id'=>'cantidad','required'=>'true'])!!}
							</td>
						
							<td>

			            	<div class="form-group">
								<div class="input-group date">
									{!! Form::text('fecha[]',null,['class'=>'form-control','id'=>'fecha'])!!}
									<span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
								</div>
							</div>
							</td>


							<td>{!!Form::text('regimen[]',null,['class'=>'form-control','id'=>'	regimen','required'=>'true'])!!}
							</td>
							
						</tr>
					</table>
				</div>
			
				
			</div>
		<div class="panel-heading"><h4>Documentos Extras</h4></div>
			<div class="panel-body">
				<table class="table">
				  <thead>
				    <tr>
				      <th scope="col">Descripción de Documentos</th>
				      
				      <th scope="col">Número de Documento</th>
				      
				      <th scope="col">Entidad Emisora</th>
				     
				      <th scope="col">Fecha</th>
				      
				    </tr>
				  </thead>
				  <tbody>
				    <tr>
				      <th scope="row">Factura Comercial</th>
				       <input type="hidden" name="cat_descrip_doc_id[]" id="" value="1">

				     		<td>{!!Form::text('numero_documento[]',null,['class'=>'form-control',''=>"modal",'data-target'=>"",'id'=>'numero_documento','required'=>'true'])!!}
							</td><br>
							<td>{!!Form::text('entidad_emisora[]',null,['class'=>'form-control',''=>"modal",'data-target'=>"",'id'=>'entidad_emisora','required'=>'true'])!!}
							</td><br>
							<td><div class="input-group date">
								{!! Form::text('fecha_doc[]',null,['class'=>'form-control','readonly'=>'readonly','id'=>'']) !!}
								<span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
							</div>
							</td>


				      
				    </tr>
				    <tr>
				      <th scope="row">Lista de Embarque</th>
				      <input type="hidden" name="cat_descrip_doc_id[]" id="" value="2">
				     	
							<td>{!!Form::text('numero_documento[]',null,['class'=>'form-control',''=>"modal",'data-target'=>"",'id'=>'numero_documento','required'=>'true'])!!}
							</td>
							<td>{!!Form::text('entidad_emisora[]',null,['class'=>'form-control',''=>"modal",'data-target'=>"",'id'=>'entidad_emisora','required'=>'true'])!!}
							</td>
							<td><div class="input-group date">
								{!! Form::text('fecha_doc[]',null,['class'=>'form-control','readonly'=>'readonly','id'=>'']) !!}
								<span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
							</div>
							</td>
				    </tr>
				    <tr>
				      <th scope="row">Conocimiento de Embarque</th>
				       <input type="hidden" name="cat_descrip_doc_id[]" id="" value="3">
				     		<td>{!!Form::text('numero_documento[]',null,['class'=>'form-control','id'=>'numero_documento',''=>''])!!}
							</td>
							<td>{!!Form::text('entidad_emisora[]',null,['class'=>'form-control',''=>"modal",'data-target'=>"",'id'=>'entidad_emisora',''=>''])!!}
							</td>
							<td><div class="input-group date">
								{!! Form::text('fecha_doc[]',null,['class'=>'form-control','readonly'=>'readonly','id'=>'']) !!}
								<span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
							</div>
							</td>


				    </tr>
				    <tr>
				      <th scope="row">Otros</th>
				      <input type="hidden" name="cat_descrip_doc_id[]" id="" value="4">
				      <td>{!!Form::text('numero_documento[]',null,['class'=>'form-control','id'=>'numero_documento',''=>''])!!}
							</td>
							<td>{!!Form::text('entidad_emisora[]',null,['class'=>'form-control',''=>"modal",'data-target'=>"",'id'=>'entidad_emisora',''=>''])!!}
							</td>
							<td><div class="input-group date">
								{!! Form::text('fecha_doc[]',null,['class'=>'form-control','readonly'=>'readonly','id'=>'']) !!}
								<span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
							</div>
							</td>
				    </tr>
				  </tbody>
				</table>
				
			</div>
	</div><!--Cierre de Panel para todos los item -->

	<div class="row text-center">
		<a href="{{ route('CertExportaFacil.index')}}"  class="btn btn-primary">Cancelar</a>
		<input type="submit" class="btn btn-success" value="Enviar" name="Enviar" onclick="()"> 
   </div>

	{{Form::close()}}
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="{{asset('js/ajax_v2.1.1.js')}}"></script>

<script>
$(document).ready(function (){
	
	var i = 0;
		$('#add-productos').click(function(){
		i++;

	 
	 	$('#productos tr:last').after('<tr id="row1'+i+'"display="block" class="show_div"><td>{!!Form::text("serie[]",null,["class"=>"form-control","id"=>"serie","required"=>"true"])!!}</td><td>{!!Form::text("sub_partida_arancel[]",null,["class"=>"form-control","id"=>"sub_partida_arancel","required"=>"true"])!!}</td><td>{!!Form::text("descripcion[]",null,["class"=>"form-control","id"=>"descripcion","required"=>"true"])!!}</td><td>{!!Form::text("valor_fob[]",null,["class"=>"form-control","id"=>"valor_fob","required"=>"true"])!!}</td><td>{!!Form::text("cantidad[]",null,["class"=>"form-control","id"=>"cantidad","required"=>"true"])!!}</td><td><div class="form-group"><div class="input-group date">{!! Form::date("fecha[]",null,["class"=>"form-control","id"=>"fecha"])!!}<span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span></div></div></td><td>{!!Form::text("regimen[]",null,["class"=>"form-control","id"=>"regimen","required"=>"true"])!!}</td><td><button  name="remove" type="button" id="'+i+'" value="" class="btn btn-danger btn-remove">x</button></td></tr>');


		});

		$(document).on('click','.btn-remove',function(){
			var id_boton= $(this).attr("id");
			$("#row1"+id_boton+"").remove();

		});

 });/*llave del document).ready*/







</script>
@stop	