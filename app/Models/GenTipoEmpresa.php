<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GenTipoEmpresa extends Model
{
    protected $table = 'gen_tipo_empresa';
}
