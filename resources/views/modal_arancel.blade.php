<div class="modal fade modal-arancel-container" id="modal" role="dialog" aria-hidden="true">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
            {{$header}}
            {{Form::hidden('tipo_arancel',null,['id'=>'id_aran'])}}
            <br>
            <br>
            <div class="row">
              <div class="col-md-8">
                  <div class="row"> 
                      <div class="col-md-12">
                      <b>Filtro</b>
                      </div>
                      <div class="col-md-12">
                            {{--{!!Form::radio('tipo_arancel_fil','1',false,['class'=>'radio-inline  filtro_arancel','id'=>'filtro_andina'])!!} 
                            {!! Form::label('filtro_andina','Nandina',['class'=>'radio-inline'])!!}--}}
                            {!!Form::radio('tipo_arancel_fil','2',false,['class'=>'radio-inline filtro_arancel','id'=>'filtro_mercosur'])!!}
                            {!! Form::label('filtro_mercosur','Mercosur (NCM)',['class'=>'radio-inline'])!!}
                      </div>
                   </div>
              </div>
              <div > 
                  <div >
                        <div >
                            {!! Form::label('Buscar')!!}
                        </div>
                        <div >
                            {!! Form::text('buscador',null,['id'=>'buscador_aran','class'=>'buscador-modal-arancel'])!!}
                        </div>
                  </div>
              </div>
            </div>

            
        </div>
        <div class="modal-body" id="arancel_certificado" style="overflow-y:scroll;height:200px">


        {{$body}}

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-info" data-dismiss="modal" style="display:none">

         {{$footer}}

          </button>
        </div>
        
      </div>

    </div>
  </div>
