<!-- Sidebar Menu -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header"><a href="{{url('/admin')}}"><span class="glyphicon glyphicon-home"></span>Menu Principal</a></li>
        <!-- Optionally, you can add icons to the links
        <li class="active"><a href="#"><i class="fa fa-link"></i> <span>Lingeneral</span></a></li>
        <li><a href="#"><i class="fa fa-link"></i> <span>Another Link</span></a></li>-->
        @if(Auth::user()->gen_tipo_usuario_id == 2)
        <li class="treeview">
          <a href="#"><i class="glyphicon glyphicon-tags"></i> <span>Gestion de Usuarios</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
          </a>

          <ul class="treeview-menu">
            <li><a href="{{url('/admin/AdminUsuario')}}">Usuarios Exportadores</a></li>
            <li><a href="{{url('/admin/Administradores')}}">Usuarios Administradores</a></li>
            <li><a href="{{url('/admin/AdministradoresSeniat')}}">Usuarios Seniat</a></li>    
            
          </ul>
        </li>
        @endif
         <li class="treeview">
          <a href="#"><i class="glyphicon glyphicon-tags"></i> <span>Reportes</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
          </a>

          <ul class="treeview-menu">

            
            <li><a href="{{url('/admin/Graficos')}}">Reporte de Usuarios</a></li>
            <li><a href="{{url('/admin/DetalleEmpresas')}}">Detalle de Empresas</a></li>  
            <li><a href="{{url('/admin/DetalleProductos')}}">Detalle de Productos</a></li>
            <li><a href="{{url('/admin/ReporteSolicitudes')}}">Reporte de Solicitudes</a></li>
            <li><a href="{{url('/admin/ReportesEmpresa')}}">Reportes de Empresa</a></li>
            <li><a href="{{url('/admin/ReportesEr')}}">Reportes ER</a></li>
            <li><a href="{{url('/admin/ReportesDVD')}}">Reportes DVD</a></li>
            <li><a href="{{url('/admin/ReportesDvdNotaCred')}}">Reportes DVD Nota Credito</a></li>   

          </ul>
        </li>
         <!--li class="treeview">
          <a href="#"><i class="glyphicon glyphicon-tags"></i> <span>Envio de Correo Masivo</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{url('/admin/NotificacionesEmail')}}">Correos</a></li>    
          </ul>
        </li-->
        <!--<li class="treeview">
          <a href="#"><i class="glyphicon glyphicon-screenshot"></i> <span>Registro de Operación</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="#">Anticipo de Exportación</a></li>
            <li><a href="#">Registro de Exportación</a></li>
            <li><a href="#">Consulta de Exportación</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#"><i class="glyphicon glyphicon-random"></i> <span>Registro de Venta de <br>Divisa</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="#">Registro de DVD</a></li>
            <li><a href="#">Carga en Lote</a></li>
            <li><a href="#">Historial de Lotes</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#"><i class="glyphicon glyphicon-transfer"></i> <span>Asistencia al Usuario</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="#">Preguntas Frecuentes</a></li>
            <li><a href="#">Reportar Duda o Problema <br> de una Solicitud</a></li>
            <li><a href="#">Consulta de Dudas o Problemas</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#"><i class="glyphicon glyphicon-cog"></i> <span>Perfil del Exportador</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="#">Datos de la Empresa</a></li>
            <li><a href="#">Cambio de Contraseña</a></li>
            <li><a href="#">Cambio de Pregunta de Seguridad</a></li>
          </ul>
        </li>
      </ul>-->
      <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
