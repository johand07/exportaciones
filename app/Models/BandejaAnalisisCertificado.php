<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BandejaAnalisisCertificado extends Model
{
    protected $table = 'bandeja_analisis_certificado';



    protected $fillable = [

           'id',
           'gen_certificado_id',
           'gen_usuario_id',
           'gen_status_id',
           'bactivo',
    ];

    protected $date =['created_at','updated_at'];

    public function GenCertificado() {

     return $this->hasOne('App\Models\GenCertificado','id','gen_certificado_id');

    }

    public function GenStatus() {

     return $this->hasOne('App\Models\GenStatus','id','gen_status_id');

    }

     public function GenUsuario() {

     return $this->hasOne('App\Models\GenUsuario','id','gen_usuario_id');

    }
}
