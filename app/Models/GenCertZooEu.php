<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GenCertZooEu extends Model
{
    protected $table = 'gen_cert_zoo_eu';



	protected $fillable = [
        'gen_usuario_id',
        'num_ref_cert',
        'pais_export',
        'cod_iso_pais_export',
        'nombre_import',
        'direccion_import',
        'pais_import',
        'cod_iso_pais_inport',
        'nombre_oper_respons',
        'direccion_oper_respons',
        'pais_oper_respons',
        'cod_iso_pais_oper_respons',
        'pais_origen',
        'cod_iso_pais_origen',
        'pais_destino',
        'cod_iso_pais_destino',
        'region_origen',
        'cod_reg_origen',
        'region_destino',
        'cod_reg_dest',
        'nombre_lug_exp',
        'num_reg_exp',
        'dir_lug_exp',
        'pais_lug_exp',
        'cod_iso_lug_exp',
        'nombre_lug_dest',
        'num_reg_lug_dest',
        'dir_lug_dest',
        'pais_lug_dest',
        'cod_iso_lug_dest',
        'lug_carga',
        'fecha_hora_salida',
        'cat_medio_transport_id',
        'puest_control_front',
        'tipo_doc_acomp',
        'cod_doc_acomp',
        'pais_doc_acomp',
        'cod_iso_doc_acomp',
        'ref_doc_comer',
        'ident_medio_transport',
        'cat_cond_transport_id',
        'num_recipiente',
        'num_precinto',
        'cat_certificado_efectos_de_id',
        'para_merc_interior',
        'num_total_bultos',
        'cant_total',
        'peso_neto_pn',
        'peso_bruto',
        'bactivo',
           
    ];

    protected $date =['created_at','updated_at'];

    public function usuarios() {

     return $this->hasOne('App\Models\GenUsuario','id','gen_usuario_id');

    }

    public function detUsuarios() {

     return $this->hasOne('App\Models\DetUsuario','id','gen_usuario_id');

    }

    public function medioTransporte() {

        return $this->hasOne('App\Models\CatMediotransporte','id','cat_medio_transport_id');
   
    }

    public function condTransporte() {

        return $this->hasOne('App\Models\CatCondtransporte','id','cat_cond_transport_id');
   
    }

    public function catCertEfectosDe() {

        return $this->hasOne('App\Models\CatCertificadoEfectosDe','id','cat_certificado_efectos_de_id');
   
    }

    public function descriptPartidaZoo() {

        return $this->hasOne('App\Models\DescripPartidaZoo','id','descrip_partida__zoo_id');
   
    }
}
