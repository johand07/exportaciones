<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DocumentosCertificado extends Model
{
   protected $table = 'documentos_certicado';
    protected $fillable = [

    	   'id',
           'gen_certificado_id',
           'file_1',
           'file_2',
           'file_3',
           'file_4',
           'file_5',
           'file_6',
           'estado_observacion',
           'descrip_observacion',
           'bactivo',
    ];

    protected $date =['created_at','updated_at'];

    public function GenCertificado() {

     return $this->hasOne('App\Models\GenCertificado','id','gen_certificado_id');

    }
}
