<!-- Pestañas -->
<div class="warpper">
  <input class="radio" id="one" name="group" type="radio">
  <input class="radio" id="two" name="group" type="radio" checked>
  <input class="radio" id="three" name="group" type="radio">
  <div class="tabs">
    
  <a href="{{route('factura.edit',@$idfact)}}" aria-hidden="true"> 
      <label class="tab" >Factura 
      </label>
  </a>
    <label class="tab" id="two-tab" for="two">Productos</label>
  </div>
  <div class="panels">
    <div class="panel" id="two-panel">
    <div class="panel-title" style="color:#1e3165">        
        @if(isset($nfact))
        <span class="glyphicon glyphicon-pencil"></span>
            Factura N°: {{$nfact}}  -  Monto FOB registrado: <b style="color:red">{{$monto}}</b>
        @else
        <span class="glyphicon glyphicon-asterisk"></span>
           Factura Nueva - Monto FOB registrado: <b style="color:red">{{$monto}}</b>
          
        @endif
        </div>
        <br>
      <div class="container">
        <div class="row">
        <div class="col-md-10" align="center">
            <table class="table table-bordered" id="fields">
              <tr>
                <th>Cód Arancelario </th>
                <th>Des Arancelaria</th>
                <th>Des Comercial</th>
                <th>Cant </th>
                <th>Precio </th>
                <th>Monto FOB</th>
                <th>Ud de Medida</th>
                @if(isset($productos))
                <th><button name="add" type="button" id="add" value="add more" class="btn btn-success">+</button></th>
                @else
                <th><button name="add" type="button" id="add" value="add more" class="btn btn-success">+</button></th>
                @endif
                
              </tr>
              @if(!isset($productos))
              <tr id="row0">
                <td>
                  {!!Form::text('codigo_arancel[]',null,['class'=>'form-control','data-toggle'=>"modal",'data-target'=>"#modal",'id'=>'cod_arancel','onkeypress'=>'return
                  soloNumerosDouble(event)','required'])!!}
                </td>
                <td>
                {!! Form::text('descripcion_arancelaria[]', null, ['class' => 'form-control' . ($errors->has('descripcion_arancelaria[]') ? ' is-invalid' : ''), 'id' => 'arancel_descrip', 'onkeypress' => 'return soloLetras(event)', 'rows' => 1, 'required', 'style' => ($errors->has('descripcion_arancelaria[]') ? 'box-shadow: 0 0 0 0.25rem rgba(220, 53, 69, 0.25);' : '')]) !!}</td>
                <td>
                {!!Form::text('descripcion_comercial[]',null,['class'=>'form-control','onkeypress'=>'return soloLetras(event)', 'rows' => 1,'required'])!!}</td>
                <td>{!! Form::text('cantidad_producto[]', null, ['class' => 'form-control format', 'id' =>
                  'cantidad_producto', 'onInput' =>
                  "calcularMonto('row0','cantidad_producto','precio_producto','montoFob')", 'maxlength' =>
                  '10','required']) !!}</td>
                <td>{!!Form::text('precio_producto[]',null,['class'=>'form-control
                  format text-right','id'=>'precio_producto','onInput'=>"calcularMonto('row0','cantidad_producto','precio_producto','montoFob')",
                  'required'])!!}</td>
                <td>
                  {!!Form::text('valor_fob[]',null,['class'=>'form-control text-right','readOnly'=>'readOnly','id'=>'montoFob'])!!}
                </td>
                <td>
                  {!!Form::select('unidad_medida_id[]',$medidas,null,['class'=>'form-control','id'=>'unidad_medida_id','placeholder'=>'Seleccione',
                  'required'])!!}</td>

                <td></td>
              </tr>
              @else
              @foreach($productos as $key =>$valor)
              
              @php $fila=$key+1; @endphp
              <tr id="row{{$key+1}}">
                <td>{!!Form::text('codigo_arancel[]',(isset($valor->codigo_arancel) ? $valor->codigo_arancel :"
                  "),['class'=>'form-control','data-toggle'=>"modal",'data-target'=>"#modal",'id'=>'cod_arancel','onkeypress'=>'return
                  soloNumerosDouble(event)','required'])!!}
                </td>
                <td>
                  {!!Form::text('descripcion_arancelaria[]',$valor->descripcion_arancelaria,['class'=>'form-control','id'=>'arancel_descrip','onkeypress'=>'return
                  soloLetras(event)','rows' => 1,'required'])!!}</td>
                <td>
                  {!!Form::hidden('gen_factura_id',$valor->gen_factura_id)!!}
                  {!!Form::text('descripcion_comercial[]',$valor->descripcion_comercial,['class'=>'form-control','onkeypress'=>'return
                  soloLetras(event)','rows' => 1])!!}</td>
                <td>{!!Form::text('cantidad_producto[]',$valor->cantidad_producto
                  ,['class'=>'form-control
                  format','id'=>'cantidad_producto','onInput'=>"calcularMonto('row$fila','cantidad_producto','precio_producto','x')"])!!}
                </td>
                <td>{!!Form::text('precio_producto[]',$valor->precio_producto,['class'=>'form-control
                  format text-right','id'=>'precio_producto','onInput'=>"calcularMonto('row$fila','cantidad_producto','precio_producto','x')",'required'])!!}
                </td>
                <td>
                  {!!Form::text('monto_total[]',$valor->monto_total,['class'=>'form-control text-right','readOnly'=>'readOnly','id'=>'montoFob','required'])!!}
                </td>
                <td>
                  {!!Form::select('unidad_medida_id[]',$medidas,$valor->unidad_medida_id,['class'=>'form-control','placeholder'=>'Seleccione','id'=>'unidad_medida_id',
                  'required'])!!}</td>
                <td>
                  {!!Form::hidden('id[]',$valor->id)!!}
                  <button name="remove" type="button" id="{{$key+1}}" value="" class="btn btn-danger btn-remove">x</button></td>
              </tr>
              <br>
              @endforeach
              @endif
            </table><br>
           
            <span><b style="color: red;">Nota:</b> Los campos cantidad y precio usan como separador decimal el punto
                (.)
                <b style="color: red;">Ej: 3985.54 </b></span>
              <br><br>
              </div>
            @if(isset($id))
            <div id="monto_data" data-monto={{$monto}}></div>
            <br>
            <div class="col-md-11" align="center">
          
              <input type="submit" name="submit" id="id_boton" class="btn btn-success" value="Enviar" />
              @else

              <div class="col-md-11" align="center">
                <input type="submit" name="submit" id="id_boton" class="btn btn-info" value="Enviar" />
              </div>
              @endif
            </div>
            <br><br>
          </div>
          <div id="monto_data" data-monto={{$monto}}></div>
          @component('modal_dialogo',['arancel'=>$arancel])
          @slot('header') <span>Cód Arancelario</span> @endslot
          @slot('body') @endslot
          @slot('footer') <span>Enviar</span> @endslot
          @endcomponent
          {{Form::close()}}
          @push('productos')
          $(document).on('click','.btn-remove',function(){
          var id_boton= $(this).attr("id");
          var id=$(this).prev().val();
          if(id > 1){
          swal({
          title: "¿Eliminar?",
          text: "¿Está seguro de eliminar este Registro?",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Si!, Borrarlo!",
          cancelButtonText: "No, Cancelar!",
          closeOnConfirm: false,
          closeOnCancel: false,
          showLoaderOnConfirm: true
          },
          function(isConfirm){
          if (isConfirm) {
          $.ajax({
          'type':'get',
          'url':'eliminar',
          'data':{ 'id':id},
          success: function(data){
          $('#row'+id_boton).remove();
          if(data==1){
          swal("Eliminación exitosa","Producto Eliminado ", "success");
          }
          }
          });
          }else{
          swal("Cancelado", "No se ha procesado la eliminación", "warning");
          }
          });
          }else{
          $('#row'+id_boton).remove();
          }
          });
          @endpush