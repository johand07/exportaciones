@extends('templates/layoutlte')

@section('content')

<!--Aqui Form::open-->

{{Form::model($planillaDjir02,['route'=>['savePlanilla2Inversion.update',$planillaDjir02->id],'method'=>'PATCH','id'=>'formPlanilla1Inversion'])}}
@if(isset($planillaDjir02->descrip_observacion) && $planillaDjir02->estado_observacion == 1)
<div class="alert alert-dismissible alert-danger">
  <button type="button" class="close" data-dismiss="alert">&times;</button>
  <h4 class="alert-heading">Observación!</h4>
  <p class="mb-0">{{$planillaDjir02->descrip_observacion}}</p>
</div>
@endif
<!--***************Parte 1 Registro****************************-->
{!! Form::hidden('estado_observacion',null,['class'=>'form-control','id'=>'estado_observacion','onkeypress'=>'return soloNumeros(event)'])!!}
{!! Form::hidden('planilla2_id',$planillaDjir02->id,['class'=>'form-control text-center']) !!}
      <div id="datos_inver1">
        <ol class="breadcrumb">
          <li class="active" style="color:#337AB7">Paso 1</li>
        </ol>
        <div class="panel panel-primary">
          <div class="panel-heading"><h4>Información General</h4></div>
          <div class="panel-body">
             <div class="row"><br>
                   <div class="col-md-1"></div>
                   <div class="col-md-10 text-center">
                      {!! Form::label('','Pertenece a un Grupo Empresarial Internacional?',['class'=>'form-control'])!!}<br>
                      {!! Form::label('','Si',['class'=>'radio-inline'])!!}
                      {!! Form::radio('grupo_emp_internacional','1',false,['class'=>'radio-inline','id'=>'inver1'])!!}

                      {!! Form::hidden('grupo_emp_internacional_edit',$planillaDjir02->grupo_emp_internacional,['class'=>'form-control text-center','onkeypress'=>'return soloLetras(event)','id'=>'grupo_emp_internacional_edit']) !!}

                      {!! Form::label('','No',['class'=>'radio-inline'])!!}
                      {!! Form::radio('grupo_emp_internacional','2',false,['class'=>'radio-inline','id'=>'inver2'])!!}
                    </div>
                  <div class="col-md-1"></div>
                </div><br>
                <div class="row" id="datos_repre" style="display:none;">
                  <div class="col-md-12">
                     <table class="table borde table-responsive table-bordered" border="2">
                      <tr>
                        <td><h4><b>Si respondió si, por favor mencione las empresas relacionadas:</b></h4></td>
                        <td>{!! Form::text('emp_relacionadas_internacional',null,['class'=>'form-control text-center','onkeypress'=>'return soloNumerosyLetras(event)','id'=>'emp_relacionadas_internacional']) !!}<span><b style="color:#E01313">Debe incluir las empresas separadas por guiones</b></span></td>
                      </tr>
                      <tr>
                        <td style="width:40%"><b>Casa Matriz</b></td>
                        <td>{!! Form::text('casa_matriz',null,['class'=>'form-control text-center','onkeypress'=>'return soloLetras(event)','id'=>'casa_matriz']) !!}</td>
                      </tr>
                      <tr>
                        <td style="width:40%"><b>Empresa Representante de la Casa Matriz para <br>América Latina:</b></td>
                        <td>{!! Form::text('emp_repre_casa_matriz',null,['class'=>'form-control text-center','onkeypress'=>'return soloLetras(event)','id'=>'emp_repre_casa_matriz']) !!}</td>
                      </tr>
                      <tr>
                        <td style="width:40%"><b>Afiliadas:</b></td>
                        <td>{!! Form::text('afiliados_internacional',null,['class'=>'form-control text-center','onkeypress'=>'return soloLetras(event)','id'=>'afiliados_internacional']) !!}</td>
                      </tr>
                      <tr>
                        <td style="width:40%"><b>Subsidiarias de su empresa en el Extranjero:</b></td>
                        <td>{!! Form::text('sub_emp_extranjero',null,['class'=>'form-control text-center','onkeypress'=>'return soloLetras(event)','id'=>'sub_emp_extranjero']) !!}</td>
                      </tr>
                    </table>
                  </div>
                </div>
                <br>
                <div class="row"><br>
                   <div class="col-md-1"></div>
                   <div class="col-md-10 text-center">
                      {!! Form::label('','Pertenece a un Grupo Empresarial Nacional?',['class'=>'form-control'])!!}<br>

                      {!! Form::label('','Si',['class'=>'radio-inline'])!!}
                      {!! Form::radio('grupo_emp_nacional',1,false,['class'=>'radio-inline','id'=>'nacional1'])!!}

                       {!! Form::hidden('grupo_emp_nacional_edit',$planillaDjir02->grupo_emp_nacional,['class'=>'form-control text-center','onkeypress'=>'return soloLetras(event)','id'=>'grupo_emp_nacional_edit']) !!}



                      {!! Form::label('','No',['class'=>'radio-inline'])!!}
                      {!! Form::radio('grupo_emp_nacional',2,false,['class'=>'radio-inline','id'=>'nacional2'])!!}
                    </div>
                  <div class="col-md-1"></div>
                </div><br>
                <div class="row" id="datos_emp" style="display:none;">
                  <div class="col-md-12">
                     <table class="table borde table-responsive table-bordered" border="2">
                      <tr>
                        <td style="width:40%"><b>Empresa Holding, Corporación o Consorcio:</b></td>
                        <td>{!! Form::text('emp_haldig_coorp',null,['class'=>'form-control text-center','onkeypress'=>'return soloNumerosyLetras(event)','id'=>'emp_haldig_coorp']) !!}<span><b style="color:#E01313">Debe incluir las empresas o Consorcio separadas por guiones</b></span></td>
                      </tr>
                      <tr>
                        <td style="width:40%"><b>Afiliadas:</b></td>
                        <td>{!! Form::text('afiliados_nacional',null,['class'=>'form-control text-center','onkeypress'=>'return soloLetras(event)','id'=>'afiliados_nacional']) !!}</td>

                      </tr>
                      <tr>
                        <td style="width:40%"><b>Subsidiarias locales:</b></td>
                        <td>{!! Form::text('sub_locales',null,['class'=>'form-control text-center','onkeypress'=>'return soloLetras(event)','id'=>'sub_locales']) !!}</td>
                      </tr>
                    </table>
                  </div>
                </div>
                 <div class="row"><br>
                   <div class="col-md-1"></div>
                   <div class="col-md-10 text-center">
                    <label class="form-control" style="height:5%"><b>Ha presentado ante el Viceministerio de Comercio Exterior y Promoción de Inversiones  los documentos que acreditan la representación legal o poder?</b></label><br>
                      {!! Form::label('','Si',['class'=>'radio-inline'])!!}
                      {!! Form::radio('present_doc_repre_legal','1',false,['class'=>'radio-inline','id'=>'reprelegal1'])!!}

                       {!! Form::hidden('present_doc_repre_legal_edit',$planillaDjir02->present_doc_repre_legal,['class'=>'form-control text-center','onkeypress'=>'return soloLetras(event)','id'=>'present_doc_repre_legal_edit']) !!}


                      {!! Form::label('','No',['class'=>'radio-inline'])!!}
                      {!! Form::radio('present_doc_repre_legal','2',false,['class'=>'radio-inline','id'=>'reprelegal2'])!!}
                    </div><br>
                  <div class="col-md-1"></div>
                </div><br>
                 <div class="row" id="representante" style="display:none;">
                  <div class="col-md-12">
                   <table class="table borde table-responsive table-bordered" border="2">
                      <tr>
                        <td><b>EN CASO AFIRMATIVO, PROPORCIONE LOS SIGUIENTES DATOS:</b></td>
                      </tr>
                      <tr>
                        <td style="width:40%"><b>N° de Apostilla:</b></td>
                        <td>{!! Form::number('num_apostilla',null,['class'=>'form-control text-center','onkeypress'=>'return soloNumeros(event)','id'=>'num_apostilla']) !!}</td>
                      </tr>
                      <tr>
                        <td style="width:40%"><b>Fecha:</b></td>
                        <td><div class="input-group date">
                        {!! Form::text('fecha_apostilla',null,['class'=>'form-control','readonly'=>'readonly','id'=>'fecha_apostilla']) !!}
                        <span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
                      </div></td>
                      </tr>
                      <tr>
                        <td style="width:40%"><b>País:</b></td>
                        <td>{!! Form::text('pais',null,['class'=>'form-control text-center','onkeypress'=>'return soloLetras(event)','id'=>'pais']) !!}</td>
                      </tr>
                       <tr>
                        <td style="width:40%"><b>Estado o Ciudad:</b></td>
                        <td>{!! Form::text('estado',null,['class'=>'form-control text-center','onkeypress'=>'return soloLetras(event)','id'=>'estado']) !!}</td>
                      </tr>
                       <tr>
                        <td style="width:40%"><b>Autoridad competente para
                        <br>apostillar:</b></td>
                        <td>{!! Form::text('autoridad_apostilla',null,['class'=>'form-control text-center','onkeypress'=>'return soloLetras(event)','id'=>'autoridad_apostilla']) !!}</td>
                      </tr>
                      <tr>
                        <td style="width:40%"><b>Cargo:</b></td>
                        <td>{!! Form::text('cargo',null,['class'=>'form-control text-center','onkeypress'=>'return soloLetras(event)','id'=>'cargo']) !!}</td>
                      </tr>
                       <tr>
                        <td style="width:40%"><b>Traductor:</b></td>
                        <td>{!! Form::text('traductor',null,['class'=>'form-control text-center','onkeypress'=>'return soloLetras(event)','id'=>'traductor']) !!}</td>
                      </tr>
                      <tr>
                        <td style="width:40%"><b>Datos Adicionales:</b></td>
                        <td>{!! Form::text('datos_adicionales',null,['class'=>'form-control text-center','onkeypress'=>'return soloLetras(event)','id'=>'datos_adicionales']) !!}</td>
                      </tr>
                    </table>
                  </div>
                </div>
<!------------------------------>
              <div class="row">
                <div class="col-md-12">
                  <br>
                  <table class="table borde table-responsive table-bordered" border="2">
                    <tr>
                      <td style="width:40%"><b>Ingresos anuales promedio del último ejercicio:</b></td>
                      <td>{!! Form::number('ingresos_anual_ult_ejer',null,['class'=>'form-control text-center','onkeypress'=>'return soloNumeros(event)','id'=>'ingresos_anual_ult_ejer']) !!}</td>

                    </tr>
                    <tr>
                      <td style="width:40%"><b>Egresos anuales promedio del último ejercicio:</b></td>
                      <td>{!! Form::number('egresos_anual_ult_ejer',null,['class'=>'form-control text-center','onkeypress'=>'return soloNumeros(event)','id'=>'egresos_anual_ult_ejer']) !!}</td>

                    </tr>
                    <tr>
                      <td style="width:40%"><b>Total balance del último ejercicio:</b></td>
                      <td>{!! Form::number('total_balance_ult_ejer',null,['class'=>'form-control text-center','onkeypress'=>'return soloNumeros(event)','id'=>'total_balance_ult_ejer']) !!}</td>
                    </tr>
                  </table>
                </div>
              </div>    
          <!-----Botones------>
            <div class="row">
              <div class="col-md-12">
                <div class="col-md-4"></div>
                <div class="col-md-4 text-center">
                  <div class="form-group">
                    <a class="btn btn-primary" href="#" id="seguir1">Siguiente</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
<!--**********************************************************************-->
<div id="datos_inver2" style="display: none;">
  <ol class="breadcrumb">
    <li href="#" vid="paso4" style="color:#337AB7">Paso 1</li>
    <li class="active"  id="paso3" style="color:#337AB7">Paso 2</li>
  </ol>
  <div class="panel panel-primary">
  <div class="panel-heading"><h4>Detalle de la Declaración Jurada de la Inversión Realizada</h4></div>
    <div class="panel-body">
      <div class="row">
        <div class="col-md-12">
            <label><h4><b>Año de Declaración</b></h4></label>
          {!!Form::select('anio_informacion_financiera',$aniodeclaracion,null,
              ['class'=>'form-control','id'=>'anio_informacion_financiera','placeholder'=>'--Seleccione una Opción--'])!!}
        </div>
      </div>
      <br><br>
      <div class="row">
        <div class="panel panel-primary">
          <div class="panel-heading"><h4>Inversión Financiera en divisas y/o cualquier otro medio de cambio</h4></div>
          <div class="col-md-12"><br>
            <label><h4><b>Tipo de Moneda</b></h4></label>
             {!!Form::select('moneda_informacion_financiera',$divisas,null,
              ['class'=>'form-control','id'=>'moneda_informacion_financiera','placeholder'=>'--Seleccione una Opción--'])!!}
          </div>
          <div class="col-md-12">
            <br>
              <table class="table borde table-responsive table-bordered" border="2">
              <tr>
                <td style="width:40%"><b>Moneda extranjera:</b></td>
                <td>{!! Form::number('moneda_extranjera',null,['class'=>'form-control text-center','onkeypress'=>'return soloNumeros(event)','id'=>'moneda_extranjera']) !!}</td>
                
              </tr>
              <tr>
                <td style="width:40%"><b>Utilidades reinvertidas:</b></td>
                <td>{!! Form::number('utilidades_reinvertidas',null,['class'=>'form-control text-center','onkeypress'=>'return soloNumeros(event)','id'=>'utilidades_reinvertidas']) !!}</td>
                
              </tr>
              <tr>
                <td style="width:40%"><b>Crédito con casa matriz y/o filial extranjera:</b></td>
                <td>{!! Form::number('credito_casa_matriz',null,['class'=>'form-control text-center','onkeypress'=>'return soloNumeros(event)','id'=>'credito_casa_matriz']) !!}</td>
              </tr>
            </table>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="panel panel-primary">
          <div class="panel-heading"><h4>Bienes de capital fisico o tangibles (Maquinarias, equipos,  herramientas o cualquier otro tipo de activo tangible). </h4></div>
           <div class="col-md-12"><br>
            <label><h4><b>Tipo de Moneda</b></h4></label>
              {!!Form::select('moneda_bienes_tangibles',$divisas,null,
              ['class'=>'form-control','id'=>'moneda_bienes_tangibles','placeholder'=>'--Seleccione una Opción--'])!!}
          </div>
          <div class="col-md-12">
             <br>
            <table class="table borde table-responsive table-bordered" border="2">
              <tr>
                <td style="width:40%"><b>Tierras y terrenos:</b></td>
                <td>{!! Form::number('tierras_terrenos',null,['class'=>'form-control text-center','onkeypress'=>'return soloNumeros(event)','id'=>'tierras_terrenos']) !!}</td>

              </tr>
              <tr>
                <td style="width:40%"><b>Edificios y otras construcciones:</b></td>
                <td>{!! Form::number('edificios_construcciones',null,['class'=>'form-control text-center','onkeypress'=>'return soloNumeros(event)','id'=>'edificios_construcciones']) !!}</td>

              </tr>
              <tr>
                <td style="width:40%"><b>Maquinarias, equipos y herramientas:</b></td>
                <td>{!! Form::number('maquinarias_eqp_herra',null,['class'=>'form-control text-center','onkeypress'=>'return soloNumeros(event)','id'=>'maquinarias_eqp_herra']) !!}</td>

              </tr>
              <tr>
                <td style="width:40%"><b>Equipos de transporte:</b></td>
                <td>{!! Form::number('eqp_transporte',null,['class'=>'form-control text-center','onkeypress'=>'return soloNumeros(event)','id'=>'eqp_transporte']) !!}</td>

              </tr>
              <tr>
                <td style="width:40%"><b>Otros activos fijos tangibles:</b></td>
                <td>{!! Form::number('otros_activos_tangibles',null,['class'=>'form-control text-center','onkeypress'=>'return soloNumeros(event)','id'=>'otros_activos_tangibles']) !!}</td>

              </tr>
              <tr>
                <td style="width:40%"><b>Muebles, enseres y equipos de oficina:</b></td>
                <td>{!! Form::number('muebles_enceres',null,['class'=>'form-control text-center','onkeypress'=>'return soloNumeros(event)','id'=>'muebles_enceres']) !!}</td>

              </tr>
            </table>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="panel panel-primary">
          <div class="panel-heading"><h4>Bienes inmateriales o intangible (Software, patentes, derechos de marca u otros activos intangibles).</h4></div>
           <div class="col-md-12"><br>
            <label><h4><b>Tipo de Moneda</b></h4></label>
               {!!Form::select('moneda_bienes_intangibles',$divisas,null,
              ['class'=>'form-control','id'=>'moneda_bienes_intangibles','placeholder'=>'--Seleccione una Opción--'])!!}
          </div>
          <div class="col-md-12">
             <br>
              <table class="table borde table-responsive table-bordered" border="2">
              <tr>
                <td style="width:40%"><b>Software:</b></td>
                <td>{!! Form::number('software',null,['class'=>'form-control text-center','onkeypress'=>'return soloNumeros(event)','id'=>'software']) !!}</td>
              </tr>
               <tr>
                <td style="width:40%"><b>Derechos de propiedad intelectual:</b></td>
                <td>{!! Form::number('derecho_prop_intelectual',null,['class'=>'form-control text-center','onkeypress'=>'return soloNumeros(event)','id'=>'derecho_prop_intelectual']) !!}</td>
              </tr>
              <tr>
                <td style="width:40%"><b>Contribuciones tecnológicas intangibles:</b></td>
                <td>{!! Form::number('contribuciones_tecno',null,['class'=>'form-control text-center','onkeypress'=>'return soloNumeros(event)','id'=>'contribuciones_tecno']) !!}</td>
                
              </tr>
              <tr>
                <td style="width:40%"><b>Otros activos fijos intangibles:</b></td>
                <td>{!! Form::number('otros_activos_intangibles',null,['class'=>'form-control text-center','onkeypress'=>'return soloNumeros(event)','id'=>'otros_activos_intangibles']) !!}</td>
              </tr>
               <tr>
                <td style="width:40%" class="text-center"><b>TOTAL</b></td>
                <td>{!! Form::number('total_costos_declaracion',null,['class'=>'form-control text-center','onkeypress'=>'return soloNumeros(event)','id'=>'total_costos_declaracion', 'readonly'=>'true']) !!}</td>
              </tr>
            </table>
          </div>
        </div>
      </div>
        <br>
        <br>
      <!---Botones-->
      <div class="row">
        <div class="col-md-12">
          <div class="col-md-4"></div>
          <div class="col-md-4 text-center">
            <div class="form-group">
              <a class="btn btn-primary" href="#" id="atras1">Atras</a>
              <a class="btn btn-primary" href="#" id="seguir2">Siguiente</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!--*************************************-->
<div id="datos_inver3" style="display: none;">
  <ol class="breadcrumb"> 
    <li href="#" vid="paso4" style="color:#337AB7">Paso 1</li>
    <li href="#" vid="paso4" style="color:#337AB7">Paso 2</li>
    <li class="active"  id="paso3" style="color:#337AB7">Paso 3</li>
  </ol>
  <div class="panel panel-primary">
  <div class="panel-heading"><h4>Tipo de Inversión</h4></div>
    <div class="panel-body">
      <div class="row">
        <table class="table table-responsive table-bordered">
          <thead>
            <tr>
             <td style="width:50%"><b>Inversión Extranjera Directa</b> se entiende la inversión productiva efectuada a través de los aportes realizados por los inversionistas extranjeros conformados por recursos tangibles o financieros, destinados 
              a formar parte del patrimonio de los sujetos receptores de inversión extranjera en el territorio nacional, con la finalidad de generar valor agregado al proceso productivo en el que se inserta. Estos aportes deben representar una participación igual o superior al 10% del capital societario)</td>
              <td style="width:50%">{!! Form::radio('tipo_inversion',1,false,['class'=>'radio-inline','id'=>'tipo_inversion'])!!}
              </td>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td style="width:50%"><b>Inversión de Cartera</b>(Se refiere a la adquisición de acciones o participaciones societarias en todo tipo de empresas que representen un nivel de participación en el patrimonio societario inferior al diez por ciento (10%)</td>
              <td style="width:50%">{!! Form::radio('tipo_inversion',2,false,['class'=>'radio-inline','id'=>'tipo_inversion'])!!}</td>
            </tr>
          </tbody>
        </table>
      </div>
      <div class="row">
        <div class="panel panel-primary">
          <div class="panel-heading"><h4>RESUMEN <br>Estimación de la Inversión Realizada (DJIR -02)</h4></div>
          <div class="col-md-12">
            <br>
            <table class="table borde table-responsive table-bordered" border="2">
              <tr>
                <td style="width:40%" class="text-center"><b>Modalidad de la Inversión</b></td>
                <td style="width:40%" class="text-center"><b>Monto</b></td>
              </tr>
              <tr>
                <td style="width:40%"><b>Inversión Financiera en divisas y/o cualquier otro medio de cambio</b></td>
                <td style="width:50%">{!! Form::number('invert_divisa_cambio',null,['class'=>'form-control text-center','readonly'=>'readonly','onkeypress'=>'return soloNumeros(event)','id'=>'invert_divisa_cambio']) !!}</td>
              </tr>
              <tr>
                <td style="width:40%"><b>Bienes de capital fisico o tangibles (Maquinarias, equipos, herramientas o cualquier otro tipo de activo tangible).</b></td>
                <td style="width:50%">{!! Form::number('bienes_cap_fisico_tangibles',null,['class'=>'form-control text-center','readonly'=>'readonly','onkeypress'=>'return soloNumeros(event)','id'=>'bienes_cap_fisico_tangibles']) !!}</td>
              </tr>
              <tr>
                <td style="width:40%"><b>Bienes inmateriales o intangible (Software, patentes, derechos de marca u otros activos intangibles.</b></td>
                <td style="width:50%">{!! Form::number('bienes_inmateriales_intangibles',null,['class'=>'form-control text-center','readonly'=>'readonly','onkeypress'=>'return soloNumeros(event)','id'=>'bienes_inmateriales_intangibles']) !!}</td>
              </tr>
              <tr>
                <td style="width:40%"><b>Reinversiones de utilidades ( SOLO PARA PROCESO DE ACTUALIZACIÓN)</b></td>
                <td style="width:50%">{!! Form::number('reinversiones_utilidades',null,['class'=>'form-control text-center','onkeypress'=>'return soloNumeros(event)','id'=>'reinversiones_utilidades']) !!}</td>
              </tr>
              <tr>
                <td style="width:40%"><b>Otra (especifique)</b></td>
                <td>{!! Form::number('especifique_otro',null,['class'=>'form-control text-center','onkeypress'=>'return soloNumeros(event)','id'=>'especifique_otro']) !!}</td>
              </tr>
              <tr>
                <td style="width:40%" class="text-center"><b>TOTAL</b></td>
                <td>{!! Form::number('total_modalidad_inv',null,['class'=>'form-control text-center','readonly'=>'readonly','onkeypress'=>'return soloNumeros(event)','id'=>'total_modalidad_inv', 'readonly'=>'true']) !!}</td>
              </tr>
            </table>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <table class="table borde table-responsive table-bordered" border="2">
                  <tr>
                    <td><b>Cuál fue la base de estimación?</b></td>
                    <td></td>
                  </tr>
                  <tr>
                    <td><b>Valor en libros (valor del patrimonio)</b></td>
                    <td>{!! Form::radio('base_estimacion','Valor en libros (valor del patrimonio)',false,['class'=>'radio-inline','id'=>'base_estimacion'])!!}</td>
                  </tr>
                  <tr>
                    <td><b>Valor neto de activos</b></td>
                    <td>{!! Form::radio('base_estimacion','Valor neto de activos',false,['class'=>'radio-inline','id'=>'base_estimacion'])!!}</td>
                  </tr>
                  <tr>
                    <td><b>Valor de una empresa similar</b></td>
                    <td>{!! Form::radio('base_estimacion','Valor de una empresa similar',false,['class'=>'radio-inline','id'=>'base_estimacion'])!!}</td>
                  </tr>
                   <tr>
                    <td><b>Otros (especificar)</b></td>
                    <td>{!! Form::text('otro_especifique',null, ['class'=>'form-control','id'=>'otro_especifique','onkeypress'=>'return soloNumerosyLetras(event)']) !!}</td>
                  </tr>
            </table>
        </div>
      </div>
      <br><br>
      <!---Botones-->
      <div class="row">
        <div class="col-md-12">
          <div class="col-md-4"></div>
          <div class="col-md-4 text-center">
            <div class="form-group">
            <a href="#" class="btn btn-primary" id="atras2">Atras</a>
            <a class="btn btn-primary" href="#" id="guardar_personal" onclick="enviarplanilla2()">Registrar</a>
            </div>
          </div>
          <div class="col-md-4"></div>
        </div>
      </div>
    </div>
  </div>
</div>


<!---Modal-->
<div id="planilla2" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close btn-md" data-dismiss="modal" style="height:5px;">&times;</button>
        <h4 class="modal-title"><b style="color:#337AB7">DECLARACIÓN JURADA DE INVERSIÓN REALIZADA</b></h4>
      </div>
      <div class="modal-body">
      <div class="col-md-12">
        <p class="text-justify">DECLARO QUE LOS DATOS CONTENIDOS EN ESTA DECLARACIÓN HAN SIDO DETERMNADOS EN BASE A LAS DISPOSICIONES LEGALES Y REGLAMENTARIAS CORRESPONDIENTES.
        </p>
        <div class="row">
          <div class="colo-md-1"></div>
          <div class="col-md-10">
             <input type="checkbox" name="condicion" id="condicion" value="si" class="checkbox-inline"><label class="text-justify">LA INFORMACIÓN AQUÍ DECLARADA ES ENTREGADA CON CARÁCTER CONFIDENCIAL.</label>
          </div>
          <div class="colo-md-1"></div>
        </div>
      </div>
      </div>
      <div class="modal-footer">
        <div class="row">
          <div class="col-md-12">
            <div class="col-md-4"></div>
            <div class="col-md-4 text-center">
              <div class="form-group">
                 <button type="button" class="btn btn-default" style="background-color:#ADABC9; color: #FFFFFF;" data-dismiss="modal"><b>Cerrar</b></button>
                <input type="submit" class="btn btn-primary" value="Aceptar" name="Aceptar" onclick="enviarInversionista()"> 
              </div>
            </div>
            <div class="col-md-4"></div>
          </div>
        </div>
      </div>
    </div>

  </div>
</div>
<!-------Modal--->
{{Form::close()}}
@stop

<script src="{{asset('js/ajax_v2.1.1.js')}}"></script>
<script type="text/javascript">
 $(document).ready(function() { 
  /***Validar los campos si y no que estan ocultos**/


 let GrupoInternacional=$('#grupo_emp_internacional_edit').val();

 if(GrupoInternacional == 1){
   $('#datos_repre').show(1000).animate({width: "show"}, 1000,"linear");
 } else{

  $('#datos_repre').hide(1000).animate({height: "hide"}, 1000,"linear");
 }




 let GrupoNacionaL=$('#grupo_emp_nacional_edit').val();

   if(GrupoNacionaL == 1){
     $('#datos_emp').show(1000).animate({width: "show"}, 1000,"linear");
   } else{

    $('#datos_emp').hide(1000).animate({height: "hide"}, 1000,"linear");
   }



 let Representante=$('#present_doc_repre_legal_edit').val();

   if(Representante == 1){
     $('#representante').show(1000).animate({width: "show"}, 1000,"linear");
   }else{
     $('#representante').hide(1000).animate({width: "hide"}, 1000,"linear");

   }

});


 function sumarBalanceEjercicio(){  
    var suma =0;
    var ingresos_anual_ult_ejer=parseFloat($('#ingresos_anual_ult_ejer').val());
    var egresos_anual_ult_ejer=parseFloat($('#egresos_anual_ult_ejer').val());

    if(!isNaN(ingresos_anual_ult_ejer)){
    suma +=ingresos_anual_ult_ejer;

    }
    if(!isNaN(egresos_anual_ult_ejer)){
    suma -=egresos_anual_ult_ejer;

    }


    $("#total_balance_ult_ejer").val(suma);  
  }

  $(document).on('keyup','#ingresos_anual_ult_ejer',function(){
          sumarBalanceEjercicio();
        });
  $(document).on('keyup','#egresos_anual_ult_ejer',function(){
              sumarBalanceEjercicio();
            });

/*********Function de detalle de declaracion de inversion*******/

 function sumarDetalleInversion(){  
    var suma =0;
    var inverDivisa=0;
    var tangibles=0;
    var intangibles=0;
    var moneda_extranjera=parseFloat($('#moneda_extranjera').val());
    var utilidades_reinvertidas=parseFloat($('#utilidades_reinvertidas').val());
    var credito_casa_matriz=parseFloat($('#credito_casa_matriz').val());
    var tierras_terrenos=parseFloat($('#tierras_terrenos').val());
    var edificios_construcciones=parseFloat($('#edificios_construcciones').val());
    var maquinarias_eqp_herra=parseFloat($('#maquinarias_eqp_herra').val());
    var eqp_transporte=parseFloat($('#eqp_transporte').val());
    var otros_activos_tangibles=parseFloat($('#otros_activos_tangibles').val());
    var muebles_enceres=parseFloat($('#muebles_enceres').val());
    var software=parseFloat($('#software').val());
    var derecho_prop_intelectual=parseFloat($('#derecho_prop_intelectual').val());
    var contribuciones_tecno=parseFloat($('#contribuciones_tecno').val());
    var otros_activos_intangibles=parseFloat($('#otros_activos_intangibles').val());


    if(!isNaN(moneda_extranjera)){
    suma +=moneda_extranjera;
    inverDivisa+=moneda_extranjera;

    }
    if(!isNaN(utilidades_reinvertidas)){
    suma +=utilidades_reinvertidas;
    inverDivisa+=utilidades_reinvertidas;

    }

    if(!isNaN(credito_casa_matriz)){
    suma +=credito_casa_matriz;
     inverDivisa+=credito_casa_matriz;

    }

    $('#invert_divisa_cambio').val(inverDivisa);

    if(!isNaN(tierras_terrenos)){
    suma +=tierras_terrenos;
    tangibles+=tierras_terrenos;
    }

    if(!isNaN(edificios_construcciones)){
    suma +=edificios_construcciones;
    tangibles+=edificios_construcciones;

    }

    if(!isNaN(maquinarias_eqp_herra)){
    suma +=maquinarias_eqp_herra;
    tangibles+=maquinarias_eqp_herra;

    }

    if(!isNaN(eqp_transporte)){
    suma +=eqp_transporte;
    tangibles+=eqp_transporte;

    }

    if(!isNaN(otros_activos_tangibles)){
    suma +=otros_activos_tangibles;
    tangibles+=otros_activos_tangibles;

    }

    if(!isNaN(muebles_enceres)){
    suma +=muebles_enceres;
    tangibles+=muebles_enceres;

    }

    $('#bienes_cap_fisico_tangibles').val(tangibles);

    if(!isNaN(software)){
    suma +=software;
    intangibles+=software;

    }

    if(!isNaN(derecho_prop_intelectual)){
    suma +=derecho_prop_intelectual;
    intangibles+=derecho_prop_intelectual;

    }

    if(!isNaN(contribuciones_tecno)){
    suma +=contribuciones_tecno;
    intangibles+=contribuciones_tecno;

    }

    if(!isNaN(otros_activos_intangibles)){
    suma +=otros_activos_intangibles;
    intangibles+=otros_activos_intangibles;
    }

    $('#bienes_inmateriales_intangibles').val(intangibles);
    
    var totales=inverDivisa+tangibles+intangibles;
    $('#total_modalidad_inv').val(totales);


    $("#total_costos_declaracion").val(suma);  
  }

  $(document).on('keyup','#moneda_extranjera',function(){
          sumarDetalleInversion();
        });
  $(document).on('keyup','#utilidades_reinvertidas',function(){
              sumarDetalleInversion();
        });
  $(document).on('keyup','#credito_casa_matriz',function(){
          sumarDetalleInversion();
        });
  $(document).on('keyup','#tierras_terrenos',function(){
          sumarDetalleInversion();
        });
  $(document).on('keyup','#edificios_construcciones',function(){
          sumarDetalleInversion();
        });
  $(document).on('keyup','#maquinarias_eqp_herra',function(){
          sumarDetalleInversion();
        });
  $(document).on('keyup','#eqp_transporte',function(){
          sumarDetalleInversion();
        });
  $(document).on('keyup','#otros_activos_tangibles',function(){
          sumarDetalleInversion();
        });
  $(document).on('keyup','#muebles_enceres',function(){
          sumarDetalleInversion();
        });
  $(document).on('keyup','#software',function(){
          sumarDetalleInversion();
        });
  $(document).on('keyup','#derecho_prop_intelectual',function(){
          sumarDetalleInversion();
        });
  $(document).on('keyup','#contribuciones_tecno',function(){
          sumarDetalleInversion();
        });
  $(document).on('keyup','#otros_activos_intangibles',function(){
          sumarDetalleInversion();
        });



/**********Function estimacion de la inversion**************/


  
  function sumarEstimacionInversion(){  
    var suma =0;
    var invert_divisa_cambio=parseFloat($('#invert_divisa_cambio').val());
    var bienes_cap_fisico_tangibles=parseFloat($('#bienes_cap_fisico_tangibles').val());
    var bienes_inmateriales_intangibles=parseFloat($('#bienes_inmateriales_intangibles').val());
    /* var reinversiones_utilidades=parseFloat($('#reinversiones_utilidades').val());
    var especifique_otro=parseFloat($('#especifique_otro').val()); */
    

    if(!isNaN(invert_divisa_cambio)){
    suma +=invert_divisa_cambio;

    }
    if(!isNaN(bienes_cap_fisico_tangibles)){
    suma +=bienes_cap_fisico_tangibles;

    }

    if(!isNaN(bienes_inmateriales_intangibles)){
    suma +=bienes_inmateriales_intangibles;

    }

    /* if(!isNaN(reinversiones_utilidades)){
    suma +=reinversiones_utilidades;

    }

    if(!isNaN(especifique_otro)){
    suma +=especifique_otro;

    } */

      $("#total_modalidad_inv").val(suma);  
  }

  $(document).on('keyup','#invert_divisa_cambio',function(){
          sumarEstimacionInversion();
        });
  $(document).on('keyup','#bienes_cap_fisico_tangibles',function(){
              sumarEstimacionInversion();
        });
  $(document).on('keyup','#bienes_inmateriales_intangibles',function(){
          sumarEstimacionInversion();
        });
  /* $(document).on('keyup','#reinversiones_utilidades',function(){
          sumarEstimacionInversion();
        });
  $(document).on('keyup','#especifique_otro',function(){
          sumarEstimacionInversion();
        }); */


/***************************************************************/
  $(document).on('keyup','#reinversiones_utilidades',function(){
      var totales=$('#total_modalidad_inv').val();
      let totalStorage=sessionStorage.setItem('total_modalidad_inv',totales);
      console.log('totales1-'+totales);
      if(!isNaN($('#reinversiones_utilidades').val())){
        totales=parseFloat(sessionStorage.getItem('total_modalidad_inv'))+parseFloat($('#reinversiones_utilidades').val());
        $('#total_modalidad_inv').val(totales);
      }else{
        $('#total_modalidad_inv').val(sessionStorage.getItem('total_modalidad_inv'));
      }
    });

    $(document).on('keyup','#especifique_otro',function(){
      var totales=$('#total_modalidad_inv').val();
      console.log('totales2-'+totales);
      if(!isNaN($('#especifique_otro').val())){
        totales=parseFloat(sessionStorage.getItem('total_modalidad_inv'))+parseFloat($('#especifique_otro').val());
        $('#total_modalidad_inv').val(totales);
      }else{
        $('#total_modalidad_inv').val(sessionStorage.getItem('total_modalidad_inv'));
      }
    });

    
</script>
 
