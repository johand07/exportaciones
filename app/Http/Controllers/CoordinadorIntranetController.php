<?php

namespace App\Http\Controllers;

use App\Models\GenSolicitud;
use App\Models\GenDVDSolicitud;
use App\Models\TipoSolicitud;
use App\Models\TipoDocBanco;
use App\Models\GenUsuarioOca;
use App\Models\GenUsuario;
use App\Models\BandejaAsignacionesIntranet;
use App\Models\BandejaCoordinadorIntranet;
use App\Models\GenStatus;
use Illuminate\Support\Facades\DB;
use Session;
use Auth;
use Laracasts\Flash\Flash;
use Alert;
use Illuminate\Http\Request;

class CoordinadorIntranetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $solicitudesSuspendidos = DB::table('bandeja_asignaciones_intranet as ba')
            ->join('gen_solicitud as gs','ba.gen_solicitud_id','=','gs.id')
            
            ->join('tipo_solicitud as ts','gs.tipo_solicitud_id','=','ts.id')
            ->join('det_usuario as du','gs.gen_usuario_id','=','du.gen_usuario_id')
            ->join('gen_operador_cambiario as  op','gs.coperador_cambiario','=','op.id')
            ->join('gen_usuario as  gu','ba.analizado_por','=','gu.id')
            ->join('gen_usuario as  guas','ba.asignado_por','=','guas.id')
            ->select(
                    'du.rif',
                    'du.razon_social',
                    'gs.id as gen_solicitud_id',
                    'ts.solicitud',
                    'op.nombre_oca',
                    'gu.email as analizado_por',
                    'guas.email as asignado_por',
                    'ba.gen_status_id',
                    'gs.observacion_analista_intra',
                    'gs.bactivo'
                   )
            ->whereIn('gs.gen_status_id',[31])
            ->where('gs.bactivo',1)
            ->get();
        
        $solicitudesCierreConformes = DB::table('bandeja_asignaciones_intranet as ba')
            ->join('gen_solicitud as gs','ba.gen_solicitud_id','=','gs.id')
            
            ->join('tipo_solicitud as ts','gs.tipo_solicitud_id','=','ts.id')
            ->join('det_usuario as du','gs.gen_usuario_id','=','du.gen_usuario_id')
            ->join('gen_operador_cambiario as  op','gs.coperador_cambiario','=','op.id')
            ->join('gen_usuario as  gu','ba.analizado_por','=','gu.id')
            ->join('gen_usuario as  guas','ba.asignado_por','=','guas.id')
            ->select(
                    'du.rif',
                    'du.razon_social',
                    'gs.id as gen_solicitud_id',
                    'ts.solicitud',
                    'op.nombre_oca',
                    'gu.email as analizado_por',
                    'guas.email as asignado_por',
                    'ba.gen_status_id',
                    'gs.observacion_analista_intra',
                    'gs.bactivo'
                   )
            ->whereIn('gs.gen_status_id',[32])
            ->where('gs.bactivo',1)
            ->get();

        $solicitudesCierreParciales = DB::table('bandeja_asignaciones_intranet as ba')
            ->join('gen_solicitud as gs','ba.gen_solicitud_id','=','gs.id')
            
            ->join('tipo_solicitud as ts','gs.tipo_solicitud_id','=','ts.id')
            ->join('det_usuario as du','gs.gen_usuario_id','=','du.gen_usuario_id')
            ->join('gen_operador_cambiario as  op','gs.coperador_cambiario','=','op.id')
            ->join('gen_usuario as  gu','ba.analizado_por','=','gu.id')
            ->join('gen_usuario as  guas','ba.asignado_por','=','guas.id')
            ->select(
                    'du.rif',
                    'du.razon_social',
                    'gs.id as gen_solicitud_id',
                    'ts.solicitud',
                    'op.nombre_oca',
                    'gu.email as analizado_por',
                    'guas.email as asignado_por',
                    'ba.gen_status_id',
                    'gs.observacion_analista_intra',
                    'gs.bactivo'
                   )
            ->whereIn('gs.gen_status_id',[33])
            ->where('gs.bactivo',1)
            ->get();

            $solicitudesEviadasArchivos = DB::table('bandeja_asignaciones_intranet as ba')
            ->join('gen_solicitud as gs','ba.gen_solicitud_id','=','gs.id')
            
            ->join('tipo_solicitud as ts','gs.tipo_solicitud_id','=','ts.id')
            ->join('det_usuario as du','gs.gen_usuario_id','=','du.gen_usuario_id')
            ->join('gen_operador_cambiario as  op','gs.coperador_cambiario','=','op.id')
            ->join('gen_usuario as  gu','ba.analizado_por','=','gu.id')
            ->join('gen_usuario as  guas','ba.finalizado_por','=','guas.id')
            ->select(
                    'du.rif',
                    'du.razon_social',
                    'gs.id as gen_solicitud_id',
                    'ts.solicitud',
                    'op.nombre_oca',
                    'gu.email as analizado_por',
                    'guas.email as finalizado_por',
                    'ba.gen_status_id',
                    'gs.observacion_analista_intra',
                    'gs.bactivo'
                   )
            ->whereIn('gs.gen_status_id',[35])
            ->where('gs.bactivo',1)
            ->get();

        
        return view('CoordinadorIntranet.index', compact('solicitudesSuspendidos', 'solicitudesCierreConformes', 'solicitudesCierreParciales', 'solicitudesEviadasArchivos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\GenSolicitud  $genSolicitud
     * @return \Illuminate\Http\Response
     */
    public function show(GenSolicitud $genSolicitud)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\GenSolicitud  $genSolicitud
     * @return \Illuminate\Http\Response
     */
    public function edit(GenSolicitud $genSolicitud)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\GenSolicitud  $genSolicitud
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, GenSolicitud $genSolicitud)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\GenSolicitud  $genSolicitud
     * @return \Illuminate\Http\Response
     */
    public function destroy(GenSolicitud $genSolicitud)
    {
        //
    }
}
