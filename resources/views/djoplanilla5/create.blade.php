@extends('templates/layoutlte')
@section('content')
<div class="content">
<div class="panel-group" style="background-color: #fff;">
      <div class="panel-primary">
        <div class="panel-heading"> <h3>(P-5) -  7. Proceso Productivo Gráfico del Producto a Exportar</h3></div>
        <div class="panel-body">


                  <div class="row" style="background-color: #fff">
                      <div class="col-md-12">
                      <h5 style="color: red">Formatos permitidos .gif, .png, .jpg, .jpeg</h5>
                   
                  {!! Form::open(['route' =>'DJOPlanilla5.store' ,'method'=>'POST', 'id'=>'planilla5Form', 'enctype' => 'multipart/form-data']) !!}

                          <div class="row">
                      
                              <div class="col-md-12">
                                  <table border="0" class="col-md-12">
                                    <thead>
                                      <tr>
                                        <th class="text-center" width="50%" height="40px"><h3><b>Sección de Carga</b></h3></th>
                                        <th class="text-center"><h3><b>Vista Previa</b></h3></th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      <tr>
                                        <td class="text-center" valign="top" > 
                                          <span  id="class_file" class="btn btn-default btn-file" style="margin: 3px;">
                                              Cargar Gráfico&nbsp;  <span><i class="glyphicon glyphicon-floppy-open"></i></span>
                                              {!! Form::file('proceso_grafico_imagen', array('class' => 'file-input','style'=>'margin: 10px;','id'=>'file')) !!}
                                          </span>
                                        </td>
                                        <td class="text-center" height="40px"><img id="vista_previa_proceso_grafico" src="" alt="" width="300px" style="margin: 3px;">
                                          <img id="imgdocumento" src="{{ asset('img/file.png') }}" alt="" width="300px" style="margin: 3px; display: none;">
                                        
                                          <div id="nombre"></div> <div id="alert"></div>
                                        </td>
                                      </tr>
                                    </tbody>
                                  </table>
                              </div>
                          </div>
<hr>
                          <div class="row" style="background-color: #fff">

                                <div class="col-md-12" align="center">

                                     <input type="submit"  name="submit" id="submit" class="btn btn-primary"  value="Guardar" onclick="validacionForm()">
                                    <a class="btn btn-danger btn-md" href="{{ url('exportador/DeclaracionJO') }}">Cancelar</a>


                                </div>
                              
                            </div>


                  {{Form::close()}}

                      </div>
                  </div>

        </div>
      </div>
    </div>

  </div><!-- Fin content -->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script>
   function cambiarImagen(event) {
    var reader = new FileReader();
    reader.onload = function(){
      var output = document.getElementById('vista_previa_proceso_grafico');
      output.src = reader.result;
    };
    var archivo = $(".file-input").val();
    var extensiones = archivo.substring(archivo.lastIndexOf(".")).toLowerCase();
    if(extensiones != ".jpeg" && extensiones != ".png" && extensiones != ".jpg" && extensiones != ".gif"){
       // $('#imgdocumento').show();
       //alert("Es diferente");
       $('#vista_previa_proceso_grafico').hide();
       $('#imgdocumento').css('width', '100px');   
       $('#imgdocumento').show();
       $('#alert').html('<p style="color:red;">Formato no permitido para la carga</p>');
       $('#class_file').removeClass('btn btn-default btn-file').addClass('btn btn-danger btn-file')
       $('#submit').attr('disabled', 'disabled');    
        
    }else{
     
      reader.readAsDataURL(event.target.files[0]);
      $('#imgdocumento').hide();
      $('#vista_previa_proceso_grafico').show();
      $('#class_file').removeClass('btn btn-default btn-file').addClass('btn-success btn btn-file')
      $('#class_file').removeClass('btn btn-danger btn-file').addClass('btn-success btn btn-file');
      $('#alert').html('');
      $('#submit').removeAttr("disabled");   
    }
  }
    $(document).on('change', '.file-input', function(evt) {
        cambiarImagen(evt);
    });



</script>


<script>
  //Validación de formulario, se puede llamar y pasar la sección que contenga los inputs a validar
  $(document).ready(function ()
  {
              var error = '<div class="msg_alert"><span style="color:red" class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span><strong id="cod_aran-error" class="added"> El campo es obligatorio.</strong></div>';
                var valido = '<div class="msg_alert"><span style="color:green" class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span></div>';
                var mens=['Estimado Usuario. Debe completar los campos solicitados',]
  
                validacionForm = function() {
  
                $('#planilla5Form').submit(function(event) {
                
                var campos = $('#planilla5Form').find('input:file');
                var n = campos.length;
                var err = 0;
  
                $("div").remove(".msg_alert");
                //bucle que recorre todos los elementos del formulario
                for (var i = 0; i < n; i++) {
                    var cod_input = $('#planilla5Form').find('input:file').eq(i);
                    if (!cod_input.attr('noreq')) {
                      if (cod_input.val() == '' || cod_input.val() == null)
                      {
                        err = 1;
                        cod_input.css('border', '1px solid red').after(error);
                      }
                      else{
                        if (err == 1) {err = 1;}else{err = 0;}
                        cod_input.css('border', '1px solid green').after(valido);
                      }
                      
                    }
                }
  
                //Si hay errores se detendrá el submit del formulario y devolverá una alerta
                if(err==1){
                    event.preventDefault();
                    swal("Por Favor!", mens[0], "warning")
                  }
  
                  });
                }
              });
  </script>


@endsection