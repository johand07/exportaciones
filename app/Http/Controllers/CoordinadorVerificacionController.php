<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use App\Models\GenDeclaracionJO;
use App\Models\DetUsuario;
use Illuminate\Http\Request;
use App\Models\DetDeclaracionProduc;
use App\Models\GenArancelNandina;
use App\Models\GenArancelMercosur;
use App\Models\Productos;
use App\Models\CatAldTp;
use App\Models\CatBoliviaVzla;
use App\Models\CatCanMercosur;
use App\Models\CatCubaVzla;
use App\Models\CatColombiaVzla;
use App\Models\CatPeruVzla;
use App\Models\CatCndVzla;
use App\Models\CatUeVzla;
use App\Models\CatUsaVzla;
use App\Models\AnalisisCalificacion;
use App\Models\CriterioAnalisis;
use App\Models\GenBandejaEntradaDjo;
use App\Models\DocAdicionalDjo;
use App\Models\CatTurquiaVzla;
use Illuminate\Support\Facades\Crypt;
//use App\Http\Requests\AnalisisDJO;
use Auth;
use Laracasts\Flash\Flash;
use Alert;


class CoordinadorVerificacionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //dd(1);
     
        $model= new GenDeclaracionJO;

        if(!empty($request->start_date) && !empty($request->end_date)){
            $declaraciones= $model
            ->with('rDeclaracionProduc.rProducto.rArancelNan')
            ->with('rDeclaracionProduc.rProducto.rArancelMer')
            ->with('rDeclaracionProduc.rDeclaracionPaises.rPais')
            ->with('rDetUsuario')
            ->with('rGenStatus')
            ->with('rBandejaEntradaDjo')
            ->where('bactivo',1)
            ->whereBetween('fstatus', [$request->start_date, $request->end_date])
            ->orderBy('id','desc')
            ->get();
        }else{
            $declaraciones= $model
            ->with('rDeclaracionProduc.rProducto.rArancelNan')
            ->with('rDeclaracionProduc.rProducto.rArancelMer')
            ->with('rDeclaracionProduc.rDeclaracionPaises.rPais')
            ->with('rDetUsuario')
            ->with('rGenStatus')
            ->with('rBandejaEntradaDjo')
            ->where('bactivo',1)
            ->where('gen_status_id',9)
            ->orwhere('gen_status_id',10)
            ->orwhere('gen_status_id',11)
            ->orwhere('gen_status_id',12)
            ->orwhere('gen_status_id',16)
            ->orwhere('gen_status_id',36)
            ->orwhere('gen_status_id',37)
            ->orwhere('gen_status_id',38)
            ->orderBy('id','desc')
            ->get();
        }

            //dd($declaraciones);
                $titulo= 'Verificación de Declaración Jurada de Origen';
                $descripcion= 'Perfil del Coordinador';

        return view('CoordinadorDjo.index',compact('declaraciones', 'descripcion', 'titulo'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
       
       $existe_observaciones=$this->detectar_observaciones($request->num_solicitud);
       if($existe_observaciones==1){

          $estatus_gen_declaracion_jo=11;
            $mensajeModal="La Solicitud sera enviada al usuario Exportador para que sea Corregída";
            $tituloModal="Solicitud en Observación";
            
        }else{

          $estatus_gen_declaracion_jo=12;
          $mensajeModal="Esta Declaracion se ha Validado Exitosamnte!";
          $tituloModal="¡Solicitud Verificada!";
         
         
          $this->validate($request, [

            'fecha_emision'=>'required',
            'fecha_vencimiento'=>'required',
            'gen_declaracion_jo_id'=>'required',
            'num_solicitud'=>'required',
            'bol.*'=>'required',
            'col.*'=>'required',
            'ecu.*'=>'required',
            'per.*'=>'required',
            'cub.*'=>'required',
            'ald.*'=>'required',
            'arg.*'=>'required',
            'bra.*'=>'required',
            'par.*'=>'required',
            'uru.*'=>'required',
            'usa.*'=>'required',
            'ue.*'=>'required',
            'cnd.*'=>'required',
            'tp.*'=>'required',

            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\GenDeclaracionJO  $genDeclaracionJO
     * @return \Illuminate\Http\Response
     */
    public function show(GenDeclaracionJO $genDeclaracionJO,$djo)
    {
//dd(1);
         $titulo= 'Verificación de Declaración Jurada de Origen';
         $descripcion= 'Perfil del Coordinador';



        //dd(1);
         $idDeclaracion=decrypt($djo)?:00; 
         $declaraciondjo=GenDeclaracionJO::find($idDeclaracion);
         $det_usuario=DetUsuario::where('gen_usuario_id',$declaraciondjo->gen_usuario_id)->first();
         $rutaregistromer=$det_usuario->ruta_reg_merc;
         $rutacedula=$det_usuario->ruta_doc;
         //dd($rutacedula);
         $rutarif=$det_usuario->ruta_rif;
         // consulta de la declaracion jurada de origen y todos sus datos asociados
        $modelDeclaracion= new  AnalisisCalificacion;
        $producDeclaracion= $modelDeclaracion
                        ->where('gen_declaracion_jo_id',$idDeclaracion)                
                        ->with('rCriterioAnalisis.declaracionProduc.rProducto.rArancelNan')
                        ->with('rCriterioAnalisis.declaracionProduc.rProducto.rArancelMer') 
                        ->with('rCriterioAnalisis.declaracionProduc.rDeclaracionPaises')                      
                        ->first();

        $doc_adicional= DocAdicionalDjo::where('gen_declaracion_jo_id',$idDeclaracion)->first(); 


        $gen_Declaracion=GenDeclaracionJO::where('id',$idDeclaracion)->first();
        //dd($gen_Declaracion);
        // fin

      // validamos si la solicitud existe
      if (empty($producDeclaracion->id)) {

           Alert::warning('No es Posible Validar la Solicitud. Intente Nuevamente.','Acción Denegada!')->persistent("OK");
           return redirect()->back()->withInput();
      }


      // validamos si la solicitud fue analizada si no enviame para el create para analizarla
      /*if (empty($producDeclaracion->rAnalisisCalificacion)) {
       
          Session::put('gen_declaracion_jo_id',$idDeclaracion);

          return redirect()->action('AnalisisCalificacionController@create');
      }*/

      // Consulta de los catalogos referente a los acuerdos internacionales
          $cat_bolivia=CatBoliviaVzla::all()->pluck('codigo','codigo');
          $cat_mercosur=CatCanMercosur::all()->pluck('codigo','codigo');
          $cat_colombia=CatColombiaVzla::all()->pluck('codigo','codigo');
          $cat_cuba=CatCubaVzla::all()->pluck('codigo','codigo');
          $cat_aladi=CatAldTp::all()->pluck('codigo','codigo');
          $cat_peru=CatPeruVzla::all()->pluck('codigo','codigo');

          $cat_cnd=CatCndVzla::all()->pluck('codigo','codigo');
          $cat_ue=CatUeVzla::all()->pluck('codigo','codigo');
          $cat_usa=CatUsaVzla::all()->pluck('codigo','codigo');
          $cat_turquia=CatTurquiaVzla::all()->pluck('codigo','codigo');


          /*** Documentos de usuario****/

        
        $doc_Usuario= DetUsuario::where('gen_usuario_id',$gen_Declaracion->gen_usuario_id)->first();

       //dd($doc_Usuario);


         if(!empty($doc_Usuario['ruta_doc'])){
             $exten_file_1=explode(".", $doc_Usuario['ruta_doc']);
            $extencion_file_1 = $exten_file_1[1];
        } else {
            $extencion_file_1 = '';
        }
        if (!empty($doc_Usuario['ruta_rif'])) {
            $exten_file_2=explode(".", $doc_Usuario['ruta_rif']);
            $extencion_file_2 = $exten_file_2[1];
        }else {
            $extencion_file_2 = '';
        }
        if (!empty($doc_Usuario['ruta_reg_merc'])) {
            $exten_file_3=explode(".", $doc_Usuario['ruta_reg_merc']);
            $extencion_file_3 = $exten_file_3[1];
        }else {
            $extencion_file_3 = '';
        }


        return view('CoordinadorDjo.show',compact('gen_Declaracion','cat_turquia', 'descripcion', 'titulo','doc_Usuario','extencion_file_1','extencion_file_2','extencion_file_3','gen_Declaracion','idDeclaracion','producDeclaracion','cat_bolivia','cat_mercosur','cat_colombia','cat_cuba','cat_aladi','cat_peru','cat_cnd','cat_ue','cat_usa','doc_adicional','rutacedula','rutarif','rutaregistromer' ));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\GenDeclaracionJO  $genDeclaracionJO
     * @return \Illuminate\Http\Response
     */
    public function edit(GenDeclaracionJO $genDeclaracionJO, $djo)
    {
        $idDeclaracion=decrypt($djo)?:00;
      //dd($idDeclaracion);
        $declaraciondjo=GenDeclaracionJO::find($idDeclaracion);
        //dd($declaraciondjo);

        $det_usuario=DetUsuario::where('gen_usuario_id',$declaraciondjo->gen_usuario_id)->first();
        // dd($det_usuario->usuario->tipoUsuario);

        $rutacedula=$det_usuario->ruta_doc;
        //dd($rutacedula);
        $rutarif=$det_usuario->ruta_rif;
        //dd($rutarif);
        $rutaregistromer=$det_usuario->ruta_reg_merc;
        // dd($rutaregistromer);

        if(!empty($rutacedula)){
            $exten_file_1=explode(".", $rutacedula);
           $extencion_file_1 = $exten_file_1[1];
       } else {
           $extencion_file_1 = '';
       }
   //dd($extencion_file_1);

       if (!empty($rutarif)) {
           $exten_file_2=explode(".", $rutarif);
           $extencion_file_2 = $exten_file_2[1];
       }else {
           $extencion_file_2 = '';
       }
       if (!empty($rutaregistromer)) {
           $exten_file_3=explode(".", $rutaregistromer);
           $extencion_file_3 = $exten_file_3[1];
       }else {
           $extencion_file_3 = '';
       }


     // consulta de la declaracion jurada de origen y todos sus datos asociados
     $modelDeclaracion= new  AnalisisCalificacion;
     $producDeclaracion= $modelDeclaracion
                       ->where('gen_declaracion_jo_id',$idDeclaracion)                
                       ->with('rCriterioAnalisis.declaracionProduc.rProducto.rArancelNan')
                       ->with('rCriterioAnalisis.declaracionProduc.rProducto.rArancelMer') 
                       ->with('rCriterioAnalisis.declaracionProduc.rDeclaracionPaises')                      
                       ->first();
       //dd($producDeclaracion);

       

      /* foreach ($producDeclaracion->rCriterioAnalisis as $key => $productos) {
           dd($productos->arancel_mer);
       }*/

       $doc_adicional= DocAdicionalDjo::where('gen_declaracion_jo_id',$idDeclaracion)->first(); 
     // fin

     // validamos si la solicitud existe
     if (empty($producDeclaracion->id)) {

          Alert::warning('No es Posible Validar la Solicitud. Intente Nuevamente.','Acción Denegada!')->persistent("OK");
          return redirect()->back()->withInput();
     }

     // Consulta de los catalogos referente a los acuerdos internacionales
         $cat_bolivia=CatBoliviaVzla::all()->pluck('codigo','codigo');
         $cat_mercosur=CatCanMercosur::all()->pluck('codigo','codigo');
         $cat_colombia=CatColombiaVzla::all()->pluck('codigo','codigo');
         $cat_cuba=CatCubaVzla::all()->pluck('codigo','codigo');
         $cat_aladi=CatAldTp::all()->pluck('codigo','codigo');
         $cat_peru=CatPeruVzla::all()->pluck('codigo','codigo');

         $cat_cnd=CatCndVzla::all()->pluck('codigo','codigo');
         $cat_ue=CatUeVzla::all()->pluck('codigo','codigo');
         $cat_usa=CatUsaVzla::all()->pluck('codigo','codigo');
         $cat_turquia=CatTurquiaVzla::all()->pluck('codigo','codigo');

         
     // fin

     $titulo= 'DECLARACIÓN JURADA DE ORIGEN';
     $descripcion= 'Coordinador';

     return view('CoordinadorDjo.edit',compact('declaraciondjo','titulo','descripcion','producDeclaracion','cat_bolivia','cat_mercosur','cat_colombia','cat_cuba','cat_aladi','cat_peru','cat_cnd','cat_ue','cat_usa','cat_turquia','idDeclaracion','doc_adicional','extencion_file_1','extencion_file_2','extencion_file_3','det_usuario','rutacedula','rutarif','rutaregistromer'));








    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\GenDeclaracionJO  $genDeclaracionJO
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, GenDeclaracionJO $genDeclaracionJO)
    {

        // dd($request->all());
        //dd($request->num_solicitud);
        //echo "update";
        //dd($request);
        
        // dd($request->all());
       
       $existe_observaciones=$this->detectar_observaciones($request->num_solicitud);
       if($existe_observaciones==1){

          $estatus_gen_declaracion_jo=36;
            $mensajeModal="La Solicitud será enviada al Analista para que sea corregida";
            $tituloModal="Solicitud en Observación por el Coordinador";
            
        }else{
            //dd(1);
          $estatus_gen_declaracion_jo=12;
          $mensajeModal="Esta Declaracion se ha Validado Exitosamnte!";
          $tituloModal="¡Solicitud Verificada!";
         
         
          $this->validate($request, [

            'fecha_emision'=>'required',
            'fecha_vencimiento'=>'required',
            'gen_declaracion_jo_id'=>'required',
            'num_solicitud'=>'required',
            'bol.*'=>'required',
            'col.*'=>'required',
            'ecu.*'=>'required',
            'per.*'=>'required',
            'cub.*'=>'required',
            'ald.*'=>'required',
            'arg.*'=>'required',
            'bra.*'=>'required',
            'par.*'=>'required',
            'uru.*'=>'required',
            'usa.*'=>'required',
            'ue.*'=>'required',
            'cnd.*'=>'required',
            'tp.*'=>'required',

            ]);
        }

        DB::beginTransaction();
  
          $criterios=[ 
                      "ald",
                      "arg",
                      "ue",
                      "bra",
                      "uru",
                      "cnd",
                      "tp",
                      "bol",
                      "col",
                      "ecu",
                      "per",
                      "cub",
                      "par",
                      "usa"  
                    ];
  
          // Validacion para la de solicitud
          if (empty($request->num_solicitud)) 
          {
  
              Alert::warning('No es Posible Procesar la Solicitud. Intente Nuevamente.','Acción Denegada!')->persistent("OK");
              return redirect()->back()->withInput();
  
          }
  
          // Validacion para los Productos
          if (empty($request->productoId)) 
          {
  
              Alert::warning('No es Posible Procesar la Solicitud. Intente Nuevamente.','Acción Denegada!')->persistent("OK");
              return redirect()->back()->withInput();
  
          }
  
          // Validar si existe alguna observación
              $existe_observaciones=$this->detectar_observaciones($request->num_solicitud);
  
              if($existe_observaciones==1){
  
                $estatus_gen_declaracion_jo=36;
                  $mensajeModal="La Solicitud será enviada al Analista para que sea corregida";
                  $tituloModal="Solicitud en Observación por el Coordinador";
                  
              }else{
  
                $estatus_gen_declaracion_jo=16;
                $mensajeModal="Esta Declaracion se ha Validado Exitosamnte!";
                $tituloModal="¡Solicitud Verificada!";
              }
  
          // Modelo Analisis  (Insertamos)
          
              $modelAnalisis=AnalisisCalificacion::where('num_solicitud',$request->num_solicitud)->first();
              $modelAnalisis->observacion_analisis=$request->observacion_analisis;
              $modelAnalisis->gen_status_id=$estatus_gen_declaracion_jo; 
              
              if ($modelAnalisis->save()) 
              {
                      // Recorremos los Productos
                      foreach ($request->productoId as $key => $idProducto) 
                      {
                       
                            $modelCriterio=CriterioAnalisis::where('analisis_calificacion_id',$modelAnalisis->id)
                                                            ->where('det_declaracion_produc_id',$idProducto)
                                                            ->first();
  
                            $modelCriterio->arancel_nan=$request->cod_nan[$idProducto];
                            $modelCriterio->arancel_mer=$request->cod_mer[$idProducto];
                            $modelCriterio->arancel_descrip_adicional=$request->arancel_descrip_adicional[$idProducto];
  
                            // recorremos los 14 criterios y asignamos los valores del modelo donde correspondan 
                              foreach ($criterios as $key2 => $idCriterio) 
                              {
  
                                  if (isset($request->$idCriterio[$idProducto]))
                                  {
                
                                    $modelCriterio->$idCriterio=$request->$idCriterio[$idProducto];
                                  
                                  }
  
                              }
  
                              if ($modelCriterio->save())
                              {
                                
                              }else{
  
                                  DB::rollback();
  
                                  Alert::warning('No es Posible Validar el Criterio','Acción Denegada!')->persistent("OK");
                                  return redirect()->back()->withInput();
                                 
                              }
     
                      }
  
  
                      // Actualizar la fecha de Emision y vencimiento de la declaracion
                      $modelDeclaracion=GenDeclaracionJO::find($modelAnalisis->gen_declaracion_jo_id);
                      $modelDeclaracion->fecha_emision=$request->fecha_emision;
                      $modelDeclaracion->fecha_vencimiento=$request->fecha_vencimiento;
                      $modelDeclaracion->observacion_corregida=0;
                      $modelDeclaracion->save();
                  
                    // Actualizar los estados de la solicitud en bandeja de entrada y en declaracion_jo
                       $id_declaracion=$modelAnalisis->gen_declaracion_jo_id;
                          $this->actualizar_estatus_bandeja($id_declaracion,$estatus_gen_declaracion_jo);
                    // fin validar estatus
                    
                  DB::commit();
                  Alert::success($mensajeModal, $tituloModal)->persistent("Ok");
                  return redirect()->action('CoordinadorVerificacionController@index');
  
              }else{
                  
  
                  DB::rollback();
                  Alert::warning('No es Posible Validar la Solicitud. Intente Nuevamente.','Acción Denegada!')->persistent("OK");
                  return redirect()->back()->withInput();
              }
      }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\GenDeclaracionJO  $genDeclaracionJO
     * @return \Illuminate\Http\Response
     */
    public function destroy(GenDeclaracionJO $genDeclaracionJO)
    {
        //
    }

    public function detectar_observaciones($num_solicitud)
    {

        $model=new GenDeclaracionJO;
        $declaracion=$model->where('num_solicitud',$num_solicitud)
                            
                            ->with('rDeclaracionProduc')
                            ->with('rDeclaracionProduc.rPlanilla2')
                            ->with('rDeclaracionProduc.rPlanilla3')
                            ->with('rDeclaracionProduc.rPlanilla4')
                            ->with('rDeclaracionProduc.rPlanilla5')
                            ->with('rDeclaracionProduc.rPlanilla6')
                            ->get()
                            ->last();

            // verificamos si tiene una observacion
            $tieneObservacion=[];

                $id_declaracion=$declaracion->id;

                foreach ($declaracion->rDeclaracionProduc as $key => $productos) 
                {

                    $id_declaracion_product=$productos->id;

                    if ($productos->rPlanilla2->estado_observacion==1) 
                    {
                        // idproducto + campo_planilla
                        $tieneObservacion[$id_declaracion_product]['estado_p2']='estado_p2';

                    }if ($productos->rPlanilla3->estado_observacion==1) 
                    {
                         $tieneObservacion[$id_declaracion_product]['estado_p3']='estado_p3';

                    }if ($productos->rPlanilla4->estado_observacion==1) 
                    {
                         $tieneObservacion[$id_declaracion_product]['estado_p4']='estado_p4';

                    }if ($productos->rPlanilla5->estado_observacion==1) 
                    {
                         $tieneObservacion[$id_declaracion_product]['estado_p5']='estado_p5';

                    }if ($productos->rPlanilla6->estado_observacion==1) 
                    {
                         $tieneObservacion[$id_declaracion_product]['estado_p6']='estado_p6';
                    }

                }

            //$resultado = array_merge($array1, $array2);
            // dd($declaracion->id);
            $detDocAdicionalDeclaracion= DocAdicionalDjo::where('gen_declaracion_jo_id',$declaracion->id)->first();
            

            if (!empty($tieneObservacion)) 
            {
 
              // Enviamos para Actualizar los estados de las planillas en det_declaracion_produc 
                  $estatusPlanillas=$this->actualizar_estatus_planillas($tieneObservacion,$id_declaracion);

              // Si se actualizaron los estados de las planillas devuelve 0 si no 1
                  if($estatusPlanillas==0){

                      $repuestaTieneObservacion=1;
                  
                  }else{

                      // no actualizaron los estados de las planillas mostrar error
                      Alert::warning('No es Posible Validar la Solicitud.','Error:D-O_01!')->persistent("OK");
                      return redirect()->back()->withInput();
                  }
                

                return  $repuestaTieneObservacion; // Tiene almenos una observacion
               
            }elseif (!empty($detDocAdicionalDeclaracion->descrip_observacion) && $detDocAdicionalDeclaracion->estado_observacion == 1) {
              $repuestaTieneObservacion=1;
              return $repuestaTieneObservacion;
            }
            else{

                return $repuestaTieneObservacion=0; 
                 
            }
            
        
    } 


    public function actualizar_estatus_bandeja($id_declaracion,$estatus)
    {

        //dd($id_declaracion);

            $model= new GenBandejaEntradaDjo;
            $estatus_bandeja=$model->where('gen_declaracion_jo_id',$id_declaracion)->get()->last();
           // dd(Auth::user()->id);
          // Si no esta la solicitud en bandeja asignala al usuario analista logueado y cambia el estado a atendida                                      
            if (empty($estatus_bandeja->id)) 
            {

               $atender_solicitud=$model;
               $atender_solicitud->gen_declaracion_jo_id=$id_declaracion;
               $atender_solicitud->gen_usuario_id=Auth::user()->id;
               $atender_solicitud->estado=$estatus;
  
                if ($atender_solicitud->save()) 
                {
                    // Actualiza tambien el estado de la solicitud en la tabla declaracion jo
                      $declaracionAtendida=$this->actualizar_estatus_declaracion($id_declaracion,$estatus);
                   
                    return $declaracionAtendida; 
                }                
            }

          // Si existe la solicitud en la bandeja actualiza los estados
            if (!empty($estatus_bandeja->id)) 
            {
                // Verifica que sea del analista logueado 
                // Verifica que se encuentre en estatus de atendida
                // Verifica que estado actual sea con observacion o aprobada 
                if ($estatus_bandeja->gen_usuario_id==Auth::user()->id || Auth::user()->gen_tipo_usuario_id==8 && $estatus_bandeja->estado>=10 && $estatus>=11) {
                   // dd($estatus);
                   $estatus_bandeja->estado=$estatus;
                     
                     if($estatus_bandeja->save())
                     {
                         // Actualiza tambien el estado de la solicitud en la tabla declaracion jo
                          $declaracionActualizada=$this->actualizar_estatus_declaracion($id_declaracion,$estatus);
                       
                        return $declaracionActualizada; 
                     }
                }
            }

      return 0;
    }


    public function actualizar_estatus_declaracion($id_declaracion,$estatus)
    {
        $model = new GenDeclaracionJO;
        $declaracion=$model->where('id',$id_declaracion)->get()->last();
        $declaracion->gen_status_id=$estatus;


        if ($declaracion->save()) {

              $estadoDeclaracionActualizada=1; 

          return $estadoDeclaracionActualizada;
        }
        
    }

    public function actualizar_estatus_planillas($id_declaracion_product=[],$id_declaracion)
    {
          DB::beginTransaction();
          foreach ($id_declaracion_product as $id_producto => $planilla) 
          {

            foreach ($planilla as $key => $estado_planilla) {

                  $model = new DetDeclaracionProduc;
                  $declaracion=$model->where('id',$id_producto)->get()->last();
                  $declaracion->$estado_planilla=0;
                  
                  if ($declaracion->save()) 
                  {
                      
                  }else{
                          $error=1;
                          Alert::warning('No es posible procesar la solicitud.','Error:A-E-P01!')->persistent("danger");
                          DB::rollback();
                  }
              }  
          }

          $error=@$error ?: 0;
          if ($error==0) {
            DB::commit();
          }

        return $error;

    }




}



