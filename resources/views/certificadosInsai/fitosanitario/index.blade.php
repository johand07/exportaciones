@extends('templates/layoutlte')
@section('content')
<div class="content" style="background-color: #fff">
	<div class="panel-group" style="background-color: #fff;">
		<div class="panel-primary">
			<div class="panel-heading"><h3>Datos Generales de Certificados Fitosanitario</h3> </div>
			<div class="panel-body">
				<div class="row">
					<div class="col-md-12">
						<div class="row">
							<div class="col-md-7 col-sm-7 col-xs-7">
								<h4 class="text-info"> Mis Certificados:</h4>
							</div>
							<div class="col-md-3 col-sm-3 col-xs-3">
			                    <a class="btn btn-success form-control" href="{{route('certificadosInsai.index')}}">
			                      <span class="glyphicon glyphicon-refresh" title="Agregar" aria-hidden="true"></span>
			                      Actualizar
			                    </a>
			                </div>   
						</div> <hr>
						<table id="listaInsai" class="table table-striped table-bordered" style="width:100%">
							<thead>
								<tr>
									<th>Número</th>
									<th>Fecha Certificado</th>
									<th>Lugar Origen</th>
									<th>Nombre Destinatario</th>
									<th>Rubro</th>
									<th>Cantidad Declarada</th>
									<th>Medio Transporte</th>
									<th>Acciones</th>
								</tr>
							</thead>
							<tbody id="tbodyInsai">
								@if(isset($datosApi))
								@foreach($datosApi as  $dato)
								<tr>
									<td>{{$dato['nro']}}</td>
									<td>{{$dato['fecha_certificado']}}</td>
									<td>{{$dato['lugar_origen']}}</td>
									<td>{{$dato['nombre_destinatario']}}</td>
									<td>{{$dato['rubro']}}</td>
									<td>{{$dato['cantidad_declarada']}}</td>
									<td>{{$dato['medio_transporte']}}</td>
									<td>
										<a href="{{$dato['ruta']}}" class="btn btn-success btn-sm glyphicon glyphicon-file">Cerificado</a>
									</td>
								</tr>
								@endforeach
								@endif
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div><!-- fin content-->

@stop
<script src="{{asset('adminlte/bower_components/jquery/dist/jquery.min.js')}}"></script>
<script type="text/javascript">
/*	$(document).ready(function() {
// function testInsai(){
		$.ajax({  

		url: "/exportador/getDataInsai",
		type: 'GET',

		})
		.done(function(data) {
			console.log('data');
			console.log(data.success);
			// if (data) {
			//	$('#loading_insai').hide();
			//	$('#table_data_insai').show();
			// }

			if(data.success) {
			$('#loading_insai').hide();
			$('#table_data_insai').show();
			var valor = '';
			data.result.forEach(item => {
			valor += "<tr>"+
			"<td>" + item.nro + "</td>"+
			"<td>" + item.fecha_certificado + "</td>"+
			"<td>" + item.lugar_origen + "</td>"+ 
			"<td>" + item.nombre_destinatario + "</td>"+
			"<td>" + item.rubro + "</td>"+
			"<td>" + item.cantidad_declarada + "</td>"+
			"<td>" + item.medio_transporte + "</td>"+
			"<td><a href='"+item.ruta+"' class='btn btn-success btn-sm glyphicon glyphicon-file' target='_blank'> Cerificado</a></td>"+
			"<tr>";
			})
			$("#tbodyInsai").html(valor);
			}
		})
		.fail(function(jqXHR, textStatus, thrownError) {
		errorAjax(jqXHR,textStatus)

		})
// }

}); */
</script>