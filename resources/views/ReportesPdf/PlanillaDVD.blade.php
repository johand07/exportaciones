<html>
  <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <title>DVD</title>
      <style>
          body{
              text-align:center;
             font-family: Arial;
                   color: #58585A;
          }
          th{
              font-size:10px;
              background-color:#818286;
              color: #fff;
          }
          td{
              font-size:10px;
              border: 1px solid #818286;
          }

          .p{
              border: 0px solid #fff;
          }

          .td2{
              font-size:10px;
              border: 0px solid #818286;
              width: 55%;
              text-align:center;
              font-size:14px;
          }
          .td3{
              
              border: 0px solid #818286;
              width: 15%;
              text-align:center;
              
          }
          .td4{
              
              border: 0px solid #818286;
              
              
          }
          table{
              border: 1px solid #818286;
          }
          .table2{
              border: 0px solid #818286;
          }

         table > tr > td {

            text-align: center;
          }
        .tabla > h4,h6{

          text-align: left;
          margin-bottom: 0;
          font-size:10px;

        }
        .tabla > table > thead > tr > th:nth-child(1){

           width: 30%;
        }
        .tabla > table > .tipo > tr:nth-child(1){

          border-top: 0;
        }

        .tabla2 > table > thead > tr > th:nth-child(0){

           width: 10%;
        }

      </style>
  </head>
      <body>
          
        <div class="">
          <table style="width:100%;" class="table2">
            <tbody>
              <tr>
                <td class="td3">
                  
                  <img class="img-responsive" width="165px" height="115px" src='img/MIPPCOEXIN.png'>
                </td>
                <td class="td2">
                  <h3>Demostración de Ventas de Divisas al BCV<br>Provenientes de las Exportaciones</h3>
                </td>
                <td class="td4">
                  <table  style="width:100%">
                    <thead>
                      <tr>
                        <th>1) N° EXPORTACIÓN</th>
                        
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>{{$gen_solicitud->gen_solicitud_id}}</td>
                        
                      </tr>
                    </tbody>
                  </table>
                  <table  style="width:100%">
                    <thead>
                      <tr>
                        <th>2) FECHA/HORA</th>
                        
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>{{$gen_solicitud->created_at}}</td>
                        
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
              
            </tbody>
          </table>
          
          
        </div>

        <div class="tabla">
         <div align="left">
           <span style="font-size:12px"><b>Datos del Exportador</b></span>
           <span style="margin-left:180px;">Código de Seguridad</span>
           <p style="font-size:10px;" align="center">{{$gen_solicitud->cod_seguridad}}</p>
         </div>
            <table  style="width:100%">
              <thead>
                <tr>
                  <th>3) NÚMERO DE RIF</th>
                  <th>4) NOMBRE O RAZÓN SOCIAL</th>
                  <th>5) SIGLAS REGISTRADAS </th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>{{$users->rif}}</td>
                  <td>{{$users->razon_social}}</td>
                  <td>{{$users->siglas}}</td>
                </tr>
              </tbody>
            </table>
        </div><br>

        <table  class="table2" style="width: 100%;">
            <tr>
              <td class="td4">
                <h4>Venta de Divisas: </h4>
              </td>
              
                <td class="td4">
                TOTAL
                  <input type="checkbox" style="display:inline" @if($gen_solicitud->mpendiente == 0) checked @endif>
                </td>
                
                <td class="td4">
                PARCIAL
                  <input type="checkbox" style="display:inline" @if($gen_solicitud->mpendiente != 0) checked @endif>
                </td>

                
              
            </tr>
        </table>

        <div class="tabla">
          <h4>DESCRIPCIÓN DE LA EXPORTACIÓN </h4>
          
          <table  style="width:100%">
            <thead>
              <tr>
                <th>6) N° FACTURA</th>
                @if(isset($num_nota_credito))<th>7) N° NOTA CRÉDITO</th>@endif
                @if(isset($num_nota_credito))<th>8) FECHA EMISIÓN DE LA FACTURA</th>@else<th>7) FECHA EMISIÓN DE LA FACTURA</th>@endif

                @if(isset($num_nota_credito))<th>9) DIVISA </th>@else<th>8) DIVISA </th>@endif
                @if(isset($num_nota_credito))<th>10) MONTO FOB</th>@else<th>9) MONTO FOB</th>@endif
                @if(isset($num_nota_credito))<th>11) MONTO TOTAL FACTURA </th>@else<th>10) MONTO TOTAL FACTURA </th>@endif
              </tr>
            </thead>

            <tbody>
              <tr>
                <td>{{$gen_solicitud->factura->numero_factura}}</td>

                @if(isset($num_nota_credito))
                <td>
                  @foreach($num_nota_credito as $nota)
                    {{$nota->numero_nota_credito}},
                  @endforeach
                </td>
                @endif
                <td>
                 
                    {{$gen_solicitud->factura->fecha_factura}}
             
                </td>
                <td>
                  
                    {{$gen_solicitud->factura->divisa->ddivisa_abr}}
                </td>
                <td>
                  {{$gen_solicitud->factura->monto_fob}}
                    
                </td>
                <td>
                  {{$gen_solicitud->factura->monto_total}}
                    
                </td>
              </tr>
            </tbody>
          </table>
        </div>

        <div class="tabla">
          <h4>DATOS DE LA VENTA DE DIVISAS </h4>
          

          <table  style="width:100%">
            <thead>
              <tr class="tabla">
                <th>11) FECHA DISP. DIVISAS</th>
                <th>12) MONTO PERCIBIDO DE LA VENTA</th>
                <th>13) MONTO VENDIDO </th>
                <th>14) MONTO RETENIDO</th>
                <th>15) MONTO POR VENDER </th>
              </tr>
            </thead>

            <tbody>
              <tr>
                <td>{{$gen_solicitud->fdisponibilidad}}</td>
                <td>{{$gen_solicitud->mpercibido}}</td>
                <td>{{$gen_solicitud->mvendido}}</td>
                <td>{{$gen_solicitud->mretencion}}</td>
                <td>{{$gen_solicitud->mpendiente}}</td>
              </tr>
            </tbody>
          </table>
        </div>

        <div class="tabla">
          <h4>OBSERVACIONES </h4>
        </div>
        <table  style="width:100%">
          <thead>
            <tr>
              <th>DECLARACIÓN JURADA DEL EXPORTADOR</th>
            </tr>
          </thead>
          <tbody>
            <tr>
            <td align="justify">
            <p><br>De Conformidad con la normativa cambiaria, el exportador o representante legal declara conocer las condiciones y términos establecidos en el basamento legal aplicable para esta solicitud, así como las consecuencias que de ellas se deslinden; por tanto manifiesta que tanto la información como la documentación suministrada, son ciertas, fidedignas e irrefutables, ya que reflejan con total precisión, los elementos de comprobación necesarios para su validez y eficacia; en este sentido autorizo al Centro Nacional de Comercio Exterior (CENCOEX), o a quien este designe , a encauzar todos los medios de confirmación que considere necesarios, sin más limitaciones que las establecidas en la Constitución y las Leyes de la República, sin perjucios de las acciones civiles, penales, administrativas que como resultado de dicha comprobación tuvieren lugar.</p>
           
            <p><br>De Igual modo declaro que, la recepción de toda notificación, mensaje de datos o incidencias relacionadas con esta solicitud, será efectiva desde el momento que sea emitida por el Sistema de Exportadores del CENCOEX, de conformidad a lo estipulado en el articulo 11 del Decreto con Rango, Valor y Fuerza de Ley N° 1204, de fecha de 10 de febrero de 2001, que regula la materia de Mensaje de Datos y Firmas Electrónicas, publicado en la Gaceta Oficial de la República Bolivariana de Venezuela bajo el número 37.148, de fecha 28 de febrero de 2001.</p><br>
              <table class="p" style="width:100%">
                <tr>
                  <td style="width:50%"><br><br>__________________________________________________<br>FIRMA DE EXPORTADOR O REPRESENTANTE LEGAL</td>
                  <td style="width:50%"><br><br>_____________________/____/____/____/<br>LUGAR Y FECHA</td>
                </tr>
              </table>
              
            </td>
          </tr>
          </tbody>
        </table>

        <table style="width:100%">
          <thead>
            <tr>
              <th>DECLARACIÓN JURADA DEL OPERADOR CAMBIARIO</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td style="width:100%" align="justify">
                <br>
                  <p>El Operador cambiario declara que ha examinado todos los documentos para dar cumplimiento, comprobar con los términos y condiciones previstos en la normativa cambiaria para efecto de cualquiera medida de control.</p><br><br>

                <table class="p" style="width:100%">
                  <tr>
                    <td style="width:50%"><br><br>__________________________________________________<br>FIRMA DEL OPERADOR O REPRESENTANTE LEGAL</td>
                    <td style="width:50%"><br><br>_____________________/____/____/____/<br>LUGAR Y FECHA</td>
                  </tr>
                </table>
              </td>
              
                    
              
            </tr>
          </tbody>
        </table>

        <table  style="width:100%">
          <thead>
            <tr>
              <th>SOLO PARA EL USO DE CENCOEX</th>
            </tr>
          </thead>
        </table>
        <table  style="width:100%">
          <thead>
            <tr>
              <td style="width:50%">APELLIDOS Y NOMBRES DEL FUNCIONARIO RESPONSABLE</td>
              <td style="width:50%">OBSERVACIONES</td>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>
                <p  style="width:25%">_______________________________________</p>
              </td>
              <td>
                
              </td>
              

              
            </tr>
          </tbody>
        








      </body>
</html>