<?php

namespace App\Http\Controllers;

use App\Models\GenUsuario;
use App\Models\CatPreguntaSeg;
use App\Models\GenTipoUsuario;
use App\Models\DetUsuario;
use App\Models\Accionistas;
use App\Models\Productos;
use App\Models\GenOperadorCamb;
use App\Models\GenStatus;
use App\Models\GenUsuarioOca;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Alert;

use Auth;


class AdministradoresController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
     
      $titulo= 'Usuarios Administradores';
      $descripcion= 'Gestión de Usuarios';
      $users=GenUsuario::where('bactivo',1)->whereIn('gen_tipo_usuario_id',[2,3,7,8, 18, 19])->get();
      
     
      return view('Administradores.index',compact('users','titulo','descripcion'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\GenUsuario  $genUsuario
     * @return \Illuminate\Http\Response
     */
    public function show(GenUsuario $genUsuario)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\GenUsuario  $genUsuario
     * @return \Illuminate\Http\Response
     */
    public function edit(GenUsuario $genUsuario,$id)
    {
        
        $titulo= 'Editar Usuarios Administradores';
        $descripcion= 'Editar Usuario';
        $usu=GenUsuario::find($id);
        if ($usu->gen_tipo_usuario_id == 3) {
          $usu_edit = DB::table('gen_usuario')
                  //->join('gen_tipo_usuario','gen_usuario.gen_tipo_usuario_id','=','gen_tipo_usuario.id')
                  //->join('gen_status','gen_usuario.gen_status_id','=','gen_status.id')
                  //->join('cat_preguntas_seg','gen_usuario.cat_preguntas_seg_id','=','cat_preguntas_seg.id')
                  ->join('gen_usuario_oca', 'gen_usuario.id','=','gen_usuario_oca.gen_usuario_id')
                  //->join('gen_operador_cambiario', 'gen_operador_cambiario.id','=','gen_usuario_oca.gen_operador_cambiario_id')
                  ->select(
                          'gen_usuario.id',
                          'gen_usuario.email',
                          'gen_usuario.email_seg',
                          'gen_usuario.gen_tipo_usuario_id',
                          //'gen_usuario_oca.gen_usuario_id',
                          'gen_usuario_oca.gen_operador_cambiario_id',
                          //'gen_operador_cambiario.nombre_oca',
                          'gen_usuario.gen_status_id',
                          'gen_usuario.cat_preguntas_seg_id',
                          'gen_usuario.respuesta_seg'
                          )
                  ->where('gen_usuario.id','=',$id)
                  ->first();
        } else {
          $usu_edit=$usu;
        }
        
        
        //dd($usu_edit);
        $tipousu=GenTipoUsuario::whereIn('id',[2,3,7])->pluck('nombre_tipo_usu','id');
        $statususu=GenStatus::whereIn('id', [1, 2])->pluck('nombre_status','id');
        $preguntas=CatPreguntaSeg::all()->pluck('descripcion_preg','id');

         //preguntamos por los usuarios banco de produccion para que les muestre sus oca de prueba sino para los usuarios normales muestra los oca reales
       if (Auth::user()->id == 4 || Auth::user()->id == 1) {
         $oca=GenOperadorCamb::whereNotIn('id',[31])->pluck('nombre_oca','id');
       } else {
         $oca=GenOperadorCamb::whereNotIn('id',[31,32,33])->pluck('nombre_oca','id');
       }
        
        return view('Administradores.edit', compact('usu_edit','preguntas','titulo','descripcion','tipousu','statususu','oca'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\GenUsuario  $genUsuario
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, GenUsuario $genUsuario,$id)

    {
        if (isset($request->cat_preguntas_seg_id)) {
        if (isset($request->pregunta_alter)) {

           $pregunta=$request['pregunta_alter'];
        }else{

           $pregunta='N/A';
        }
      }
      
      //dd($pregunta);
      $usu= GenUsuario::find($id);
      $usu->email=$request->email;
      $usu->email_seg=$request->email_seg;
      $usu->gen_tipo_usuario_id=$request->gen_tipo_usuario_id;
      $usu->gen_status_id=$request->gen_status_id;
      $usu->password=bcrypt($request->password);
      $usu->cat_preguntas_seg_id=$request->cat_preguntas_seg_id;
      $usu->pregunta_alter=$pregunta;
      $usu->respuesta_seg=$request->respuesta_seg;
      $usu->updated_at=date("Y-m-d H:i:s");
      $usu->save();
      
      if ($usu->gen_tipo_usuario_id == 3) {
        $usuoca=GenUsuarioOca::where('gen_usuario_id',$id)->update(['gen_operador_cambiario_id' => $request->gen_operador_cambiario_id ,'updated_at' => date("Y-m-d H:i:s")]);
      }


      Alert::success('Registro Actualizado!', 'Administrador Actualizado.')->persistent("Ok");
      return redirect()->action('AdministradoresController@index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\GenUsuario  $genUsuario
     * @return \Illuminate\Http\Response
     */
    public function destroy(GenUsuario $genUsuario)
    {
        //
    }
}
