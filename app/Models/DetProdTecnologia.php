<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DetProdTecnologia extends Model
{
    protected $table = 'det_prod_tecnologia';

    protected $fillable=[

    'id',
    'codigo_arancel',
    'gen_factura_id',
    'descripcion',
    'cantidad',
    'unidad_medida_id',
    'precio',
    'valor_fob',

  ];

  public function factura(){

  return $this->hasOne('App\Models\GenFactura','id','gen_factura_id');
 }

 public function unidadesMedidas(){

  return $this->hasOne('App\Models\GenUnidadMedida','id','unidad_medida_id');
 }

}
