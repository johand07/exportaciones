<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NdRecepcionOca extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nd_recepcion_oca', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('gen_dvd_nd_id')->index()->nullable();
            $table->foreign('gen_dvd_nd_id')->references('id')->on('gen_dvd_nd');
            $table->unsignedInteger('gen_usuario_id')->index()->nullable();
            $table->foreign('gen_usuario_id')->references('id')->on('gen_usuario');
            $table->boolean('bactivo')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nd_recepcion_oca');
    }
}
