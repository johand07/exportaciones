@php
use Illuminate\Support\Facades\Crypt;
@endphp
@extends('templates/layoutlte_coordinador_djo')

@section('content')
<div class="content" style="background-color: #fff">
    <div class="panel-group" style="background-color: #fff;">
      <div class="panel-primary">
        <div class="panel-heading"><h3>Solicitudes de Declaración Jurada de Origen</h3> </div>
        <div class="panel-body">

            

            <div class="row">
              {{Form::open(['route' =>'coordinadorDjo.index' ,'method'=>'GET'])}}
              <div class="col-md-12">
                <div class="row">
                  <div class="col-md-4">

                           <h4 class="text-info">Bandeja de Entrada:</h4><hr>
                          
                  </div>
                  <div class="col-md-2">
                    <div class="input-group date">
                      {!! Form::text('start_date', null, ['class' => 'form-control text-center','placeholder'=>'Fecha inicio','id'=>'start_date', 'readonly'=>'readonly']) !!}
                      <span style="background-color: #3c8dbc;color:#fffa" class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
                    </div>
                  </div>
                  <div class="col-md-2">
                   <div class="input-group date">
                    {!! Form::text('end_date', null, ['class' => 'form-control text-center','placeholder'=>'Fecha fin','id'=>'end_date', 'readonly'=>'readonly']) !!}
                    <span style="background-color: #3c8dbc;color:#fffa" class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
                  </div>
                </div>
                  <div class="col-md-2">
                    <div>
                      <button id="filtrodate" type="submit" class="btn btn-primary">Buscar</button>
                      <a class="btn btn-warning" href="{{route('coordinadorDjo.index')}}">Resetear</a>
                    </div>
                  </div>
                  {{Form::close()}}
                  <div class="col-md-2">
                    <a class="btn btn-primary" href="{{route('coordinadorDjo.index')}}">
                      <span class="glyphicon glyphicon-refresh" title="Agregar" aria-hidden="true"></span>
                        Actualizar
                    </a>
                  </div>
                </div>
                <table id="listaUsuarios" class="table table-striped table-bordered" style="width:100%">
                        <thead style="font-size: 11px;">
                            <tr class="text-center">
                            	  <th class="col-md-1 text-center" >Nº</th>
                                <th class="col-md-2 text-center">Nº Solicitud</th>                     
                                <!--th>Razon Social</th-->
                                <th>Rif</th>
                                <th class="col-md-2 text-center">Nº Productos</th>
                                <th class="col-md-2 text-center">Nº Paises </th>                     
                                <th class="col-md-2 text-center">Fecha de Solicitud</th>
                                <th class="col-md-2 text-center">Fecha de Atendido</th>
                                <th class="col-md-2 text-center">Fecha de Salida</th>  
                                <th class="col-md-1 text-center">Estatus</th>
                                <th class="col-md-1 text-center">Analista</th>                                  
                                <th class="col-md-1 text-center">Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                          @foreach($declaraciones as $key =>  $declaracion)
                            @if($declaracion->bactivo ==1)
                            <tr style="font-size: 11px;" >           
                            	<td  class="col-md-1 text-center" id="aranx_{{ $key }}"><b>{{ $key + 1 }}</b></td>          
                                <td class="col-md-2 text-center"  ><b class="text-primary">{{ $declaracion->num_solicitud }}</b></td>
                                <!--td >{{-- $declaracion->razon_social --}}</td--> 
                                <td>{{$declaracion->rDetUsuario->rif}}</td>
                                <td  class="col-md-2 text-center">{{ $declaracion->num_productos }}</td> 
                                <td class="col-md-2 text-center">{{ $declaracion->num_paises }}</td>
                                <td class="col-md-3 text-center"><b>{{ date('d-m-Y', strtotime($declaracion->fstatus)) }}</b></td>
                                <td>
                                 
                                  {{ date('d-m-Y',strtotime(@$declaracion->rBandejaEntradaDjo->created_at)) }}

                                </td>
                                <td>
                                 
                                  {{ date('d-m-Y',strtotime($declaracion->updated_at)) }}

                                </td>
                                <td class="col-md-2 text-center" style="font-size: 11px;">
                                  <b>
                                    @if($declaracion->gen_status_id==12)
                                        Verificado <i class="glyphicon glyphicon-ok text-success"></i>
                                    @elseif($declaracion->gen_status_id==11)
                                         @if(@$declaracion->observacion_corregida==1)
                                            ¡Corregida por el usuario!
                                         @else
                                            ¡Tiene Observaciones!
                                         @endif  
                                    @elseif ($declaracion->gen_status_id==10)
                                      <i class="glyphicon glyphicon-user"></i> Atendido 
                                    @elseif ($declaracion->gen_status_id==9)
                                      <i class="glyphicon glyphicon-time"></i> No Atendida
                                    @else
                                      {{ $declaracion->rGenStatus->nombre_status }}
                                    @endif
                                  </b>
                                </td>
                                <td >
                                  {{ @$declaracion->rBandejaEntradaDjo ? @$declaracion->rBandejaEntradaDjo->rGenUsuario->email : 'No Atendida'}}
                                </td>
                                <td class="col-md-2 text-center">
                                  <input id="token" type="hidden" name="_token" value="{{ csrf_token() }}">
                                                                    
                                  @if($declaracion->gen_status_id==8)
                                  
                                      <a class="text-center btn btn-sm btn-danger list-inline"><i class="glyphicon glyphicon-remove "></i> {{ $declaracion->nombre_status }}</a>

                                  @elseif($declaracion->gen_status_id==9)
 <!-- PENDIENTE POR TERMINAR-->
  <!-- VER SOLICITUD EN OBSERVACION -->                                  
                    
                                      <!--a href="{{-- route('AnalisisDjo.create',['djo'=>encrypt($declaracion->id)]) --}}" class="text-center btn btn-sm btn-danger list-inline"><i class="glyphicon glyphicon-share"></i> <b>Atender</b></a-->


                                  @elseif($declaracion->gen_status_id==10)
                                       
                                      <!-- Validando con la bandeja -->
                                      @if(@$declaracion->rBandejaEntradaDjo->gen_usuario_id==Auth::user()->id && @$declaracion->rBandejaEntradaDjo->estado==10)
                                            
                                            <!--a href="{{ route('AnalisisDjo.create',['djo'=>encrypt($declaracion->id)]) }}" class="text-center btn btn-sm btn-primary list-inline"><i class="glyphicon glyphicon-user"></i> <b>Por Atender</b></a-->
                                  
                                      @endif

                                  @elseif($declaracion->gen_status_id==36)

                                    <a href="{{ route('coordinadorDjo.edit',['djo'=>encrypt($declaracion->id)]) }}" class="text-center btn btn-sm btn-warning list-inline"><i class="glyphicon glyphicon-pencil"></i><b> En Observación</b> </a>

                                  @elseif($declaracion->gen_status_id==16 || $declaracion->gen_status_id==38)
                                      
                                        <a style="font-size: 11px;" href="{{ url('api/analisisDeclaracion',['nro_solicitud'=>encrypt($declaracion->num_solicitud)]) }}" class="text-center btn btn-sm btn-success list-inline" target="_blank"> <b>Ver Planilla</b></a>


                                  @elseif($declaracion->gen_status_id==12)<!-- estatus 12 aprobado por el analista -->      
                                        <a href="{{ route('coordinadorDjo.edit',['djo'=>encrypt($declaracion->id)]) }}" class="text-center btn btn-sm btn-danger list-inline"><i class="glyphicon glyphicon-pencil"></i> <b>Evaluar</b></a-->


                                  @endif

                                  @if($declaracion->rAnalisisCalificacion)
                                   <a href="{{ route('coordinadorDjo.show',['djo'=>encrypt($declaracion->id)]) }}" class="text-center btn btn-sm btn-info list-inline"><i class="glyphicon glyphicon-eye-open"></i><b> Ver </b></a> 
                                  @endif
                                </td><!--Cambiar la ruta-->
                                @endif
                              </tr>              
            				      @endforeach
                        </tbody>
                    </table>
              </div>
              
            </div>
        </div>
      </div>
    </div>
</div><!-- fin content-->

@stop