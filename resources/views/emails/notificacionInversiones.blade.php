<!DOCTYPE html>
<html lang="">
	<head>
		<meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	  
		<style type="text/css">
			#contact{

        background-repeat: no-repeat;
        background: #3a6186; /* fallback for old browsers */
        background: -webkit-linear-gradient(to left, #ECF2F4 , #D6E6F3); /* Chrome 10-25, Safari 5.1-6 */
        background: linear-gradient(to left, #ECF2F4 , #D6E6F3); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */

        }

		.col-md-12 {
		width: 100%;
		}
		.col-md-6 {
		    width: 50%;
		}
		.text-center {
		    text-align: center;
		}
		.container {
		    width: 1170px;
		}
		.row {
		    margin-right: -15px;
		    margin-left: -15px;
		}
		.col-md-4 {
		width: 25%;
		}
		.btn-primary {
		    color: #FFFFFF;
		    background-color: #337ab7;
		    border-color: #2e6da4;
		}
		.btn {
		    display: inline-block;
		    padding: 6px 12px;
		    margin-bottom: 0;
		    font-size: 14px;
		    font-weight: 400;
		    line-height: 1.42857143;
		    text-align: center;
		    white-space: nowrap;
		    vertical-align: middle;
		    -ms-touch-action: manipulation;
		    touch-action: manipulation;
		    cursor: pointer;
		    -webkit-user-select: none;
		    -moz-user-select: none;
		    -ms-user-select: none;
		    user-select: none;
		    background-image: none;
		    border: 1px solid transparent;
		    border-radius: 4px;
		}
		.btn-primary:hover {
		    color: #FFFFFF;
		    background-color: #286090;
		    border-color: #204d74;
		}

		.btn-block {
		    display: block;
		    width: 100%;
		}
		.text-justify {
		    text-align: justify;
		}
		.text-right {
		    text-align: right;
		}
		.pull-right {
		    float: right !important;
		}
		.btn-group-lg > .btn, .btn-lg {
		    padding: 10px 16px;
		    font-size: 18px;
		    line-height: 1.3333333;
		    border-radius: 6px;
		}

		.p-3{
			padding:1rem !important;

		}
		</style>
	</head>
	<body>
		<div class="container">
			<div class="row">
					
				</div>
			</div>
			<br>
<div class="row">
	<div class="col-md-12 p-3">
		<img src="{{asset('img/cintillo-vuce.jpeg')}}"><br>
		<br>
		<b>Sres.</b> {{$inversiones->DetUsuario->razon_social}}<br>
		<b>Rif: </b>{{$inversiones->DetUsuario->rif}}
	</div>
</div>
			<br>
			<div class="row">
                <div class="col-md-12 p-3">
                    <div class="col-md-12 text-justify">
                        Su solicitud <b>N° {{$inversiones->num_sol_inversionista }}</b> de Registro de Potencial Inversionista en el sistema VUCE, paso a estatus <b>{{ $inversiones->estatus->nombre_status}}</b><br>

                        @if($inversiones->gen_status_id == 11)
                            <br><b>Observación:</b><br>
                            {{ $inversiones->observacion_inversionista }}
                        @elseif($inversiones->gen_status_id == 13)
                            <br><b>Observación:</b><br>
                            {{ $inversiones->observacion_anulacion }}
                        @elseif($inversiones->gen_status_id == 15)
                            <br><b>Observación:</b><br>
                            {{ $inversiones->observacion_doc_incomp }}
                        @elseif($inversiones->gen_status_id == 19)
                            <br><b>Observación:</b><br>
                            {{ $inversiones->observacion_inversionista }}
                        @elseif($inversiones->gen_status_id == 26)
                            <br><b>Observación:</b><br>
                            {{ $inversiones->observacion_inversionista }}
                        @else
                        @endif
                    </div>
                </div>
            </div>
			<br>
			<div class="row text-center">
				<div class="col-md-12">
					<div class="col-md-4"></div>
					<div class="col-md-4 ">
						<!--<center><a href="localhost:8000/loguin"  target="_blank" class="btn btn-primary btn-lg btn-block text-center" style="color:#FFFFFF;"> VOLVER AL SISTEMA </a></center>-->
						<br>
					</div>
					<div class="col-md-4"></div>
				</div>

			</div>
		    </div>
		</div>
	</body>
</html>
