<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddGenUsuarioIdToGenDeclaracionInversion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('gen_declaracion_inversion', function (Blueprint $table) {
            $table->unsignedInteger('gen_usuario_id')->index()->nullable()->after('id');
            $table->foreign('gen_usuario_id')->references('id')->on('gen_usuario');
            
         
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('gen_declaracion_inversion', function (Blueprint $table) {
            $table->dropColumn('gen_usuario_id');
           
        });
    }
}
