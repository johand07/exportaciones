<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Planilla6 extends Model
{
    protected $table = 'planilla_6';
    protected $fillable = [

           'det_declaracion_produc_id',
           'imagen1',
           'imagen2',
           'imagen3',
           'descrip_observacion',
           'bactivo',
          
    ];
   
   		public function declaracionProduc() {

   		return $this->hasOne('App\Models\DetDeclaracionProduc','id','det_declaracion_produc_id');

    }

}
