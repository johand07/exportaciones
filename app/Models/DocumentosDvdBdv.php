<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DocumentosDvdBdv extends Model
{
    protected $table = 'documentos_dvd_bdv';

    	protected $fillable = [

           
           'gen_dvd_solicitud_id',
           'cat_documentos_id',
           'ruta_doc_dvd',
           'bactivo',
    ];

    protected $date =['created_at','updated_at'];

 

    public function dvdsolicitud() {

     return $this->hasOne('App\Models\GenDVDSolicitud','id','gen_dvd_solicitud_id');

    }

    public function documentos() {

     return $this->hasOne('App\Models\CatDocumentos','id','cat_documentos_id');

    }
    

}
