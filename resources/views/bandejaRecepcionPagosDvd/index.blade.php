@extends('templates/layoutlte_recepcion_pago')
@section('content')
@php
function explodeImgRefencia($img)
{
	$explo_cap=explode('.', $img);
	$extension= $explo_cap[1];
	return $extension;
}
@endphp
<div class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="col-md-8"></div>
			<div class="col-md-4 text-right">
				<button class="btn btn-primary" id="ActualizarBandejaPago"> <span class="glyphicon glyphicon-refresh"></span> Actualizar</button>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<form id="formHiring" method="POST">
			        <div class="message" style="display: none"></div>
			            <input id="_token" type="hidden" name="_token" value="<?php echo csrf_token(); ?>">            
			            <section class="content" id="gestion_pagos" >
			                <div class="row">
			                    
			                    <div class="col-md-12">

			                        <ul class="nav nav-tabs">
			                            <li class="active" >
			                                <a href="#pagosrecibidos" data-toggle="tab">Nuevos Pagos Recibidos
			                                    @if (isset($pagosrecibidos) && count($pagosrecibidos) > 0)
			                                        <span class="label label-warning">
			                                        <i class="fa fa-bell-o"></i>
			                                        
			                                            {{count($pagosrecibidos)}}
			                                    @endif    
			                                    </span>
			                                    
			                                </a>
			                            </li> 
			                            <li>
			                                <a href="#pagosvalidados" data-toggle="tab">Pagos Validados
			                                    @if (isset($pagosvalidados) && count($pagosvalidados) > 0)
			                                        <span class="label label-success">
			                                        <i class="fa fa-bell-o"></i>
			                                        
			                                            {{count($pagosvalidados)}}
			                                    @endif    
			                                    </span>
			                                    
			                                </a>    
			                            </li>
			                            <li>
			                                <a href="#pagosnovalidados" data-toggle="tab">Pagos No Validos
			                                    @if (isset($pagosnovalidados) && count($pagosnovalidados) > 0)
			                                        <span class="label label-danger">
			                                        <i class="fa fa-bell-o"></i>
			                                        
			                                            {{count($pagosnovalidados)}}
			                                    @endif    
			                                    </span>
			                                    
			                                </a>    
			                            </li>
			                        </ul>
			                        <div class="nav-tabs-custom">
			                            <div class="tab-content"> 
			                                <div class="tab-pane active" id="pagosrecibidos">
			                                    <section class="content-header">
			                                      <h3>
			                                        Nuevos Pagos Recibidos
			                                        <small>DashBoard</small>
			                                      </h3>
			                                    </section>
			                                    <div style="overflow-y: auto;">
				                                    <table id="example1" style="font-size: 14px;" class="table table-striped table-bordered"  width="100%">
				                                        <thead>
				                                            <tr>
				                                                <th style="text-align: center; vertical-align: top" >N° de Solicitud</th>
				                                                <th style="text-align: center; vertical-align: top" >Monto</th>
				                                                <th style="text-align: center; vertical-align: top" >Fecha Transf</th>
				                                                <th style="text-align: center; vertical-align: top" > Banco Acreditado </th>
				                                                <th style="text-align: center; vertical-align: top" >Banco Emisor</th>
				                                                <th style="text-align: center; vertical-align: top" >Nro Referencia</th>
				                                                <th style="text-align: center; vertical-align: top" >Img Capture</th>
				                                                <th style="text-align: center; vertical-align: top" >Usuario</th>
				                                                 <th style="text-align: center; vertical-align: top">Estatus</th>
				                                                <th style="text-align: center; vertical-align: top" >Acción</th>
				                                            </tr>
				                                            

				                                        </thead>
				                                        <tbody>
				                                                @if(isset($pagosrecibidos))
				                                                    @foreach($pagosrecibidos as $pagosrecibido)
				                                                        <tr id="rowhn{{$pagosrecibido->id}}">
				                                                            
				                                                            <td>

				                                                                {{$pagosrecibido->gen_dvd_solicitud_id}}
				                                                            </td>
				                                                            <td>
				                                                                {{number_format($pagosrecibido->TipoPago->monto_bolivares, 2, ',', '.')}} BS / <br> ${{number_format($pagosrecibido->TipoPago->monto_divisa, 2, ',', '.')}}
				                                                            </td>
				                                                            <td>
				                                                            	@php
				                                                                    $originalDate = $pagosrecibido->fecha_reporte;
				                                                                    echo $newDate = date("d/m/Y", strtotime($originalDate));
				                                                                @endphp
				                                                            </td>
				                                                            <td nowrap>
				                                                                {{$pagosrecibido->CatBancoAdmin->nombre_banco}} / <br> {{$pagosrecibido->CatBancoAdmin->num_cuenta}}
				                                                            </td>
				                                                            <td nowrap>
				                                                                {{$pagosrecibido->GenOperadorCamb->nombre_oca}}
				                                                            </td>
				                                                            <td>
				                                                            	<strong class="text-success">{{$pagosrecibido->num_referencia}}</strong>
				                                                            </td>
				                                                            <td>
					                                                            @if(isset($pagosrecibido->img_capture))
					                                                            	@if(explodeImgRefencia($pagosrecibido->img_capture) == 'pdf' || explodeImgRefencia($pagosrecibido->img_capture) == 'docx' || explodeImgRefencia($pagosrecibido->img_capture) == 'doc')
					                                                            		<a href="{{asset($pagosrecibido->img_capture)}}" target="_blanck">
					                                                            		<img  src="{{ asset('img/file.png') }}" alt="" width="50px" style="margin: 3px;"></a>
					                                                            	@else
					                                                            		<a href="{{asset($pagosrecibido->img_capture)}}" target="_blanck"><img src="{{asset($pagosrecibido->img_capture)}}" alt="Imagen 2" width="56px" height="76px" style="border-radius: 5px;"></a>
					                                                                @endif
					                                                            @endif
				                                                            </td>
				                                                            <td>
				                                                            	{{$pagosrecibido->GenDVDSolicitud->solicitud->usuario->detUsuario->razon_social}} / <br>
				                                                            	{{$pagosrecibido->GenDVDSolicitud->solicitud->usuario->email}}
				                                                                
				                                                            </td>
				                                                            <td nowrap align="center" style="width:200px;">
				                                                                
				                                                                <select id="tbl_pago{{$pagosrecibido->id}}" class="form-control">
				                                                                    @if(count($fases) > 0)
				                                                                        <option selected>--Selecione un Estatus--</option>
				                                                                    @endif
				                                                                    @foreach($fases as $fase)
				                                                                        <option value="{{$fase->id}}">{{$fase->nombre_fase}}</option>

				                                                                    @endforeach
				                                                                </select>
				                                                            </td>
				                                                            
				                                                            <td><a onclick="asignEstatus({{$pagosrecibido->id}}, {{$pagosrecibido->gen_dvd_solicitud_id}})" class="btn btn-success btn-block">Asignar</a></td>

				                                                        </tr>

				                                                    @endforeach 
				                      
				                                                @endif     
				                                        </tbody>
				                                    </table>
			                                    </div>
			                                </div> 
			                                <div class="tab-pane" id="pagosvalidados">
			                                    <section class="content-header">
			                                      <h3>
			                                        Pagos Validados
			                                        <small>DashBoard</small>
			                                      </h3>
			                                    </section>
												<div style="overflow-y: auto;">
				                                    <table id="example2" style="font-size: 14px;" class="table table-striped table-bordered"  width="100%">
				                                        <thead>
				                                            <tr>
				                                                <th style="text-align: center; vertical-align: top" >N° de Solicitud</th>
				                                                <th style="text-align: center; vertical-align: top" >Monto</th>
				                                                <th style="text-align: center; vertical-align: top" >Fecha Transf</th>
				                                                <th style="text-align: center; vertical-align: top" > Banco Acreditado </th>
				                                                <th style="text-align: center; vertical-align: top" >Banco Emisor</th>
				                                                <th style="text-align: center; vertical-align: top" >Nro Referencia</th>
				                                                <th style="text-align: center; vertical-align: top" >Img Capture</th>
				                                                <th style="text-align: center; vertical-align: top" >Usuario</th>
				                                                 <th style="text-align: center; vertical-align: top">Estatus</th>
				                                                
				                                            </tr>

				                                        </thead>
				                                        <tbody>
				                                                @if(isset($pagosvalidados))
				                                                    @foreach($pagosvalidados as $pagosvalidado)
				                                                        <tr id="rowhn{{$pagosvalidado->id}}">
				                                                            
				                                                            <td>

				                                                                {{$pagosvalidado->gen_dvd_solicitud_id}}
				                                                            </td>
				                                                            <td>
				                                                                {{number_format($pagosvalidado->TipoPago->monto_bolivares, 2, ',', '.')}} / {{number_format($pagosvalidado->TipoPago->monto_divisa, 2, ',', '.')}}
				                                                            </td>
				                                                            <td>
				                                                            	@php
				                                                                    $originalDate = $pagosvalidado->fecha_reporte;
				                                                                    echo $newDate = date("d/m/Y", strtotime($originalDate));
				                                                                @endphp
				                                                            </td>
				                                                            <td nowrap>
				                                                                {{$pagosvalidado->CatBancoAdmin->nombre_banco}} / {{$pagosvalidado->CatBancoAdmin->num_cuenta}}
				                                                            </td>
				                                                            <td nowrap>
				                                                                {{$pagosvalidado->GenOperadorCamb->nombre_oca}}
				                                                            </td>
				                                                            <td>
				                                                            	<strong class="text-success">{{$pagosvalidado->num_referencia}}</strong>
				                                                            </td>
				                                                            <td>
					                                                            @if(isset($pagosvalidado->img_capture))
					                                                                @if(explodeImgRefencia($pagosvalidado->img_capture) == 'pdf' || explodeImgRefencia($pagosvalidado->img_capture) == 'docx' || explodeImgRefencia($pagosvalidado->img_capture) == 'doc')
					                                                            		<a href="{{asset($pagosvalidado->img_capture)}}" target="_blanck">
					                                                            		<img  src="{{ asset('img/file.png') }}" alt="" width="50px" style="margin: 3px;"></a>
					                                                            	@else
					                                                            		<a href="{{asset($pagosvalidado->img_capture)}}" target="_blanck"><img src="{{asset($pagosvalidado->img_capture)}}" alt="Imagen 2" width="56px" height="76px" style="border-radius: 5px;"></a>
					                                                                @endif
					                                                            @endif
				                                                            </td>
				                                                            <td>
				                                                            	{{$pagosvalidado->GenDVDSolicitud->solicitud->usuario->detUsuario->razon_social}} /
				                                                            	{{$pagosvalidado->GenDVDSolicitud->solicitud->usuario->email}}
				                                                                
				                                                            </td>
				                                                            <td  align="center">
				                                                                
				                                                                {{$pagosvalidado->GenStatus->nombre_status}}
				                                                            </td>
				                                                           
				                                                            
				                                                            

				                                                        </tr>
				                                                    @endforeach 
				                      
				                                                @endif     
				                                        </tbody>
				                                    </table> 
												</div>
			                                </div> 
			                                <div class="tab-pane" id="pagosnovalidados">
			                                    <section class="content-header">
			                                      <h3>
			                                        Pagos No Validos
			                                        <small>DashBoard</small>
			                                      </h3>
			                                    </section>
			                                    <div style="overflow-y: auto;">
				                                    <table id="example3" style="font-size: 14px;" class="table table-striped table-bordered"  width="100%">
				                                        <thead>
				                                            <tr>
				                                                
				                                                <th style="text-align: center; vertical-align: top" >N° de Solicitud</th>
				                                                <th style="text-align: center; vertical-align: top" >Monto</th>
				                                                <th style="text-align: center; vertical-align: top" >Fecha Transf</th>
				                                                <th style="text-align: center; vertical-align: top" > Banco Acreditado </th>
				                                                <th style="text-align: center; vertical-align: top" >Banco Emisor</th>
				                                                <th style="text-align: center; vertical-align: top" >Nro Referencia</th>
				                                                <th style="text-align: center; vertical-align: top" >Img Capture</th>
				                                                <th style="text-align: center; vertical-align: top" >Usuario</th>
				                                                 <th style="text-align: center; vertical-align: top">Estatus</th>
				                                                
				                                            </tr>
				                                    	</thead>
				                                    
				                                        <tbody>
				                                                @if(isset($pagosnovalidados))
				                                                    @foreach($pagosnovalidados as $pagosnovalidado)
				                                                        
				                                                            <td>

				                                                                {{$pagosnovalidado->TipoPago->nombre_tipo_pago}}</td>
				                                                            <td>

				                                                                {{$pagosnovalidado->gen_dvd_solicitud_id}}
				                                                            </td>
				                                                            <td>
				                                                                {{number_format($pagosnovalidado->TipoPago->monto_bolivares, 2, ',', '.')}} / {{number_format($pagosnovalidado->TipoPago->monto_divisa, 2, ',', '.')}}
				                                                            </td>
				                                                            <td>
				                                                            	@php
				                                                                    $originalDate = $pagosnovalidado->fecha_reporte;
				                                                                    echo $newDate = date("d/m/Y", strtotime($originalDate));
				                                                                @endphp
				                                                            </td>
				                                                            <td nowrap>
				                                                                {{$pagosnovalidado->CatBancoAdmin->nombre_banco}} / {{$pagosnovalidado->CatBancoAdmin->num_cuenta}}
				                                                            </td>
				                                                            <td nowrap>
				                                                                {{$pagosnovalidado->GenOperadorCamb->nombre_oca}}
				                                                            </td>
				                                                            <td>
				                                                            	<strong class="text-success">{{$pagosnovalidado->num_referencia}}</strong>
				                                                            </td>
				                                                            <td>
					                                                            @if(isset($pagosnovalidado->img_capture))
					                                                                @if(explodeImgRefencia($pagosnovalidado->img_capture) == 'pdf' || explodeImgRefencia($pagosnovalidado->img_capture) == 'docx' || explodeImgRefencia($pagosnovalidado->img_capture) == 'doc')
					                                                            		<a href="{{asset($pagosnovalidado->img_capture)}}" target="_blanck">
					                                                            		<img  src="{{ asset('img/file.png') }}" alt="" width="50px" style="margin: 3px;"></a>
					                                                            	@else
					                                                            		<a href="{{asset($pagosnovalidado->img_capture)}}" target="_blanck"><img src="{{asset($pagosnovalidado->img_capture)}}" alt="Imagen 2" width="56px" height="76px" style="border-radius: 5px;"></a>
					                                                                @endif
					                                                            @endif
				                                                            </td>
				                                                            <td>
				                                                            	{{$pagosnovalidado->GenDVDSolicitud->solicitud->usuario->detUsuario->razon_social}} /
				                                                            	{{$pagosnovalidado->GenDVDSolicitud->solicitud->usuario->email}}
				                                                                
				                                                            </td>
				                                                            <td  align="center">
				                                                                
				                                                                {{$pagosnovalidado->GenStatus->nombre_status}}
				                                                            </td>
				                                                        </tr>
				                                                    @endforeach 
				                      
				                                                @endif     
				                                        </tbody>
				                                    </table> 
			                                    </div>  
			                                </div>                    
			                            </div>

			                        </div>
			                    </div>
			                </div>
			            </section>
			        </div>
			</form>
		</div>
	</div>
</div>


@stop
@section('scripts')
	<script type="text/javascript">
	    $(document).ready(function() {
	        $('#example1,#example2,#example3').DataTable({
	          "paging": true,
	          "lengthMenu": [ 10, 25, 50, 75, 100 ],
	          "lengthChange": true,
	          "searching": true,
	          "ordering": true,
	          "info": true,
	          "autoWidth": false,
	          "order": [[ 4, "desc" ]]
	        });

	        
    	});

    	$('#ActualizarBandejaPago').click(function() {
            // Recargo la página
            location.reload();
    	});

	    function asignEstatus(pago_id, solicitud_id) {
	        if ($('#tbl_pago'+pago_id).val() == "--Selecione un Estatus--") {
	            //alert('You have not yet selected a Booster. Please choose one.');
	            //swal(message[1], message[31],'warning');
	            swal("Por favor!", "Debe seleccionar un Estatus!", "warning")
	            /*alertify.alert("<h3>Debe seleccionar un Estatus</h3>", function(){
	            alertify.success('OK');
	        });*/
	            //alertify.success('<h3>Debe seleccionar un Estatus</h3>');
	            $('#tbl_pago'+pago_id).focus();    
	        }else{
	            //var token = $("input[name='_token']").val();
	            $.ajax({
	                url: "validarPagosDvd",
	                type: "POST",
	                
	                data: {
	                        _token: $('#_token').val(),
	                        reporte_pago_id: pago_id,
	                        gen_status_id: $('#tbl_pago'+pago_id).val(),
	                        gen_dvd_solicitud_id: solicitud_id

	                    },
	            })
	            .done(function(data) {
	                if (data == 1) {
	                    swal("Pago Validado de forma satisfactoria!", "Pago de DVD Recibido.", "success");
	                    $("#gestion_pagos").load(" #gestion_pagos");
	                    //location.reload();
	                }
	                if (data == 2) {
	                    swal("Pago invalido!", "El pago no fue validado en la cuenta referenciada.", "error");
	                    $("#gestion_pagos").load(" #gestion_pagos");
	                    //location.reload();
	                }
	                
	                
	                
	            })
	            .fail(function(jqXHR, textStatus, thrownError) {
	                errorAjax(jqXHR,textStatus)

	            })
	        
	            
	        }
	    }
        
    </script>
@endsection