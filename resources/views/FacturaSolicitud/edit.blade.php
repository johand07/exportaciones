@extends('templates/layoutlte')
@section('content')
{{Form::model($facturas,['route'=>['FinanciamientoSolicitud.edit',$id],'method'=>'GET','id'=>'addfacturaSolic','onSubmit'=>"return validar_facturas(this);"]) }}
<!--{{Form::hidden('metodo','Crear')}}!-->
@php $contador=count($facturas);  @endphp<!--Cree una etiqiueta php para con un contador!-->
{{Form::hidden('contador',$contador)}}<!--Campo oculto!-->
  <!-- Pestañas -->
  <div class="warpper">
    <input class="radio" id="one" name="group" type="radio" checked>
    <input class="radio" id="two" name="group" type="radio">
    <input class="radio" id="three" name="group" type="radio">

	  <div class="tabs">
	  <label class="tab" id="one-tab" for="one">Facturas asociadas</label>
      @if($id)
      <a href="{{route('FinanciamientoSolicitud.edit',$id)}}" aria-hidden="true"> 
      <label class="tab" >Editar Financiamiento 	</label>
      </a>
      @else
	  <a href="#" aria-hidden="true"> 
      <label class="tab" >Editar Financiamiento</label>
      </a>
      @endif
    </div>
	
    <div class="panels">
      <div class="panel" id="one-panel">
<!--/////////////////////////////////////// -->
<div class="row">
	<div class="col-md-12">
		<div class="col-md-2"></div>
		<div class="col-md-6">
			<div class="form-group">
				<table class="table table-bordered" id="fieldsfac" style="border: 0px solid ">
					<tr>
					<td colspan="4">
					{!! Form::label('tsolicitud','Tipo de Solictud')!!}
                    {!! Form::select('tipo_solicitud_id',$tiposolic,null,['class'=>'form-control','id'=>'tipo_solicitud_id']) !!}
					</td>
			     	</tr>
					<tr>
					<td colspan="2">{!! Form::label('asociadas','Facturas Asociadas')!!}</td>
				
					<td><button  name="add" type="button" id="add" value="add more" class="btn btn-success" onclick="">Asociar Nueva Factura</button></td></tr>
                   @if ($status!=0) 
                     @foreach($solicitudes as $key=>$value)
					<tr id="row{{$key+1}}">
						<td>{!!Form::label('','Número de Factura')!!}{!!Form::select('gen_factura_id[]',$facturas,$value->id,['class'=>'form-control','id'=>"gen_factura_id_".($key+1),'placeholder'=>'-- Seleccione una opción --','onchange'=>"numeroNotaCredi(this.value,($key+1))"]) !!}</td>
						<td>{!!Form::label('','Número nota Crédito')!!}{!! Form::text('numero_nota_credito[]',null,['class'=>'form-control nota','readonly'=>'readonly','id'=>'numero_nota_credito'.($key+1)]) !!}</td>
						<td><br>
						 {!!Form::hidden('x',$value->id)!!}
						 <button  name="remove" type="button" id="{{$key+1}}" value="" class="btn btn-danger btn-remove">x</button>
						 {!!Form::hidden('gen_nota_credito_id[]',null,['class'=>'form-control','readonly'=>'readonly','id'=>'gen_nota_credito_id'.($key+1)])!!}
					  </td>
					
					</tr> 
					@endforeach
				   @else 
                     <tr id="">
						<td>{!!Form::label('','Número de Factura')!!}{!!Form::select('gen_factura_id[]',$facturas,null,['placeholder'=>'--Seleccione Una Factura--','class'=>'form-control','id'=>'gen_factura_id0','onchange'=>"numeroNotaCredi(this.value,0)"]) !!}</td>
						<td>{!!Form::label('','Número nota Crédito')!!}{!! Form::text('numero_nota_credito[]',null,['class'=>'form-control nota','readonly'=>'readonly','id'=>'numero_nota_credito0']) !!}</td>
						
						<td><br>
						<button  name="remove" type="button" id="" value="" class="btn btn-danger btn-remove">x</button>{!!Form::hidden('gen_nota_credito_id[]',null,['class'=>'form-control','readonly'=>'readonly','id'=>'gen_nota_credito_id0'])!!}</td>
					    
					</tr> 
				    @endif 
				</table>
				</div>
			</div>
		</div>
	
<!--/////////////////////////////////// -->

<!--/////////////////////////////////// -->
<div class="row">
			<div class="col-md-12">
				<div class="col-md-4"></div>
				<div class="col-md-4 text-left">
					<a href="{{ url('exportador/SolicitudER/'.$id.'/edit') }}" class="btn btn-primary">Regresar</a>
					{!! Form::hidden('id',$id) !!}
					<button href="#" class="btn btn-success"  value="Continuar"  name="" >Continuar</button>
				</div>
				<div class="col-md-4"></div>
			</div>
	</div>
</div>	
		{{Session::put('status_id',$status)}}
		{{Form::close()}}
<!--script inicio!-->
<script src="{{asset('js/ajax_v2.1.1.js')}}"></script>
<script type="text/javascript">


var item_dua=null;
var duas=[];
var facturas=[];


	 @foreach($duas as $key=> $data)
	    @if($data!=null){
      duas[{{$key}}]={{$data}};
		}
		@endif
	 @endforeach


onloadFacturas();

$(document).ready(function (){
var x = $('input[name="contador"]').val();
//var i=parseInt(x);
var i=0;
var status={{$status}};
var total={{count($facturas)}};
var total_facturas={{count($solicitudes)}};
var solicitudes={{$solicitudes}};
var table="";
var id_sol={{$id}};

//////////////////////////////////////////////////////////////
   input_select= $('[id^=gen_factura_id] option:selected');
        for(i=0;i<input_select.length;i++)
         {
            
          $(input_select).trigger('change');
                                  
      }
////////////////////////////////////////////////////////////
   (status==1) ?  table=total_facturas:  table=1;     
      $('#add').click(function(){
       i++;
       $.ajax({
               'type':'get',
               'url':'eliminar',
               'data':{id:0,id_sol:id_sol},
               success: function(data){
                total_facturas=data;  

              }
                
       });

   if(table < total || table===0 ) {
    $('#fieldsfac tr:last').after('<tr id="row'+i+'" display="block" class="show_div"><td><select class="form-control grupos_facturas" id="gen_factura_id'+i+'" onchange="numeroNotaCredi(this.value,'+i+')" name="gen_factura_id[]"><option value="">--Seleccione Una Factura--</option>@foreach ($facturas as $key => $element)<option value="{{$key}}">{{$element}}</option>@endforeach</select></td><td><input name="numero_nota_credito[]" type="text" class="form-control nota" id="numero_nota_credito'+i+'" readonly="readonly"></td><td><button  name="remove" type="button" id="'+i+'" value="" class="btn btn-danger btn-remove">x</button><input type="hidden" name="gen_nota_credito_id[]" class="form-control" id="gen_nota_credito_id'+i+'" readonly="readonly"></td></tr>');
		 table++;
	}else{
  
	swal("Denegada", "Ya alcanzó el máximo de facturas registradas", "warning");
	}
onloadFacturas();
});
 $(document).on('click','.btn-remove',function(){
        var id_boton= $(this).attr("id");
        var estatus=$(this).prev().val();
        var id=$(this).parents("tr").find("select")[0].value;
        var id_nota_credito=$(this).next().val();
        if(id>0){
                 swal({
                 /*Cargo el alert para confirmar o declinar*/
                     title: "¿Eliminar?",
                     text: "¿Está seguro de eliminar este Registro?",
                     type: "warning",
                     showCancelButton: true,
                     confirmButtonColor: "#DD6B55",
                     confirmButtonText: "Si!, Borrarlo!",
                     cancelButtonText: "No, Cancelar!",
                     closeOnConfirm: false,
                     closeOnCancel: false,
                     showLoaderOnConfirm: true
                     },
    function(isConfirm){
         if (isConfirm) {
              $.ajax({
               'type':'get',
               'url':'eliminar',
               'data':{ 'id':id,'id_sol':id_sol,'id_nota':id_nota_credito},
               success: function(data){
                $('#row'+id_boton).remove();
                if(data==1){
                   swal("Eliminación exitosa","Producto Eliminado ", "success");
                   if(table!=total_facturas){ 
                         table++;
                    }else{

                    	table--;
                    }
                  }
                 }
             });
         }else{
              swal("Cancelado", "No se ha procesado la eliminación", "warning");
              
            }

        });
        }else{
            $('#row'+id_boton).remove();
                    table--;     
         }

    });
});

  function onloadFacturas(){
 	   var select=document.getElementsByTagName('select');
 		 for (var i = 0; i < select.length; i++) {
			 select[i].removeEventListener('change',function(){});
 		 	 select[i].addEventListener('change',function(){
 				   validar_facturas(this);
 			});
 		 }
  }

function validar_facturas(element){
	var ret = true;
	var aux=false;
	var auxIndex;
	var empty=0;
	if(!element==false){
	var selects = element.parentNode.parentNode.parentNode.parentNode.parentNode.getElementsByTagName('select');
     if(!selects==false){
					facturas=[];
					 item_dua=null;
					for(var i = 0; i<=selects.length-1; i++){

						if(selects[i].value==""){

							    empty++;
									ret = false;
						}

						if(empty>=1){
							swal("Denegada", "Debe seleccionar una opción para continuar",'warning');

						}


						facturas.forEach(function(value,index,arr){
							if(value===selects[i].value){
							aux=true;
							swal("Denegada", "Las Facturas deben ser diferentes",'warning');
							selects[i].selectedIndex=0;
							}
						});

						if(!aux && selects[i].value!="")
						facturas.push(selects[i].value);
						else {
							ret = false;
						}


							//console.log(facturas);

                      if(selects[i].value!=""){
							if(item_dua==null ){
								item_dua = duas[selects[i].value];
								//console.log(item_dua);
							}else{

								if(duas[selects[i].value]!=item_dua){
									swal("Denegada", "Las Facturas deben tener la misma dua",'warning');
                                  
									auxIndex = facturas.indexOf(selects[i].value);
									if(auxIndex>-1){
										facturas.splice(auxIndex,1);
									}
									
                                        var index=selects[i].selectedIndex;
                                     
									    selects[i].selectedIndex=0;

											ret = false;
								}

							}
							}
					}


				}


		}

		return ret;
}

function numeroNotaCredi(valor,num){//alert(num);

	/*var select=document.getElementsByTagName('select');
    for (var i = 0; i < select.length; i++) {
	    var Val=$("#gen_factura_id"+i).val();        			
	    
	           			
   	}	*/					
							 //num_nota[i].value(numero);
	

		$.ajax({   
	            url: '/exportador/factura/'+valor,
	            type: 'GET',
	            

	            data: {
	                    '_token'  : $('#token').val(),
	                    
	                    
	                    
	                },
	        })

	        .done(function(data) {
	        	
	        	console.log('data',data);
	        	/*if (data.length == 0 ) {
	        		$('#numero_nota_credito'+num).val(0);
	        	}*/
	           // alert(data);
	           let notas_credito = '';
	           let notas_id = '';
	            //if (isset(data)) {
	            if (data.length !== 0) {
	            	$.each(data, function(index, val) {
		            	//var numero     = data.numero_nota_credito;
		            	//var nota_credito_id     = data.id;
		            	//alert(nota_credito_id);
		            	// console.log('val.numero_nota_credito', val.numero_nota_credito);
		            	 notas_credito += val.numero_nota_credito+', ';
		            	 //val.numero_nota_credito.concat(',', val.numero_nota_credito);
		            	 notas_id     += val.id+',';
		            			//alert(num);
		            			
		            	//console.log('val.numero_nota_credito', notas_credito);		
		            				    //$('#numero_nota_credito'+num).val(numero);
		            					//$('#gen_nota_credito_id'+num).val(nota_credito_id);
		            			
		            });
		            
		            console.log('val.numero_nota_credito', notas_credito);
		            $('#numero_nota_credito'+num).val(notas_credito);	
		            $('#gen_nota_credito_id'+num).val(notas_id.substr(0, notas_id.length - 1));	
	            } else {
	            	$('#numero_nota_credito'+num).val(0);
	            }
	            		
		            		//	alert(a);
		            			//console.log(a);
					 		
		            	//
	            	//}
	            /*}else{
	            	$('#valor').val(0);
	            }*/
	            



	           
	    })

	    .fail(function(jqXHR, textStatus, thrownError) {
	        errorAjax(jqXHR,textStatus);
	    })

   
}





</script><!--Fin del script-->
@stop
