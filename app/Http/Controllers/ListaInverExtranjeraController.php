<?php

namespace App\Http\Controllers;

use App\Models\GenDeclaracionInversion;
use App\Models\PlanillaDjir_01;
use App\Models\PlanillaDjir_02;
use App\Models\GenDivisa;
use App\Models\PlanillaDjir_03;
use App\Models\Estado;
use App\Models\Municipio;
use App\Models\GenAccionistasDeclaracionDjir_01;
use App\Models\GenStatus;
use App\Models\GenSectorProductivoDjir_03;
use App\Models\GenDestinoInversionDjir_03;
use App\Models\BandejAnalistaDecInversion;
use App\Models\genDocDecInversion;
use Illuminate\Http\Request;
use Session;
use Auth;
use Alert;


class ListaInverExtranjeraController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $declaracionesInversion = GenDeclaracionInversion::where('bactivo', 1)->get();

        //dd($declaracionesInversion);

        return view('AnalistInverExtranjera.index', compact('declaracionesInversion'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\GenSolicitud  $genSolicitud
     * @return \Illuminate\Http\Response
     */
    public function show(GenDeclaracionInversion $genSolicitud)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\GenSolicitud  $genSolicitud
     * @return \Illuminate\Http\Response
     */
    public function edit(GenDeclaracionInversion $genSolicitud, $id)
    {

      
        $titulo= 'Declaracion de Inversionista Extranjero';
        $descripcion= 'Perfil del Analista';

        $solicitudDeclaracion=GenDeclaracionInversion::find($id);

        $validardeclaracion=BandejAnalistaDecInversion::where('gen_declaracion_inversion_id',$solicitudDeclaracion->id)->where('gen_usuario_id',Auth::user()->id)->first();
        //dd($validardeclaracion);

        if(empty($validardeclaracion)){

          $solicitudDeclaracion=GenDeclaracionInversion::find($id);
          $solicitudDeclaracion->gen_status_id=10;
          $solicitudDeclaracion->fstatus=date('Y-m-d');
          $solicitudDeclaracion->save();


          /*****Inserto en la bandeja del analista y me traigo el id de esa solicitud*****/

          $solicitudDec= new BandejAnalistaDecInversion;
          $solicitudDec->gen_declaracion_inversion_id=$solicitudDeclaracion->id;
          $solicitudDec->gen_usuario_id=Auth::user()->id;
          $solicitudDec->gen_status_id=10;
          $solicitudDec->fstatus=date('Y-m-d');
          $solicitudDec->save();

        }

       /////////////////////////////////////////////////
        $planillaDjir01=PlanillaDjir_01::where('gen_declaracion_inversion_id',$id)->first();
        $estados=Estado::where('id',"<>",null)->orderBy('estado','asc')->pluck('estado','id');

        //dd($planillaDjir01);

        $municipios=Municipio::all()->pluck('municipio','id');
        //dd($municipios);

        $accionistas=GenAccionistasDeclaracionDjir_01::where('gen_declaracion_inversion_id',$id)->where('bactivo',1)->get();
        
        $estado_observacion_planilla1=GenStatus::whereIn('id', [11,12,13,15])->pluck('nombre_status','id');
        

        $docDecInversion=genDocDecInversion::where('gen_declaracion_inversion_id',$id)->first();

        //Doc Adicionales
         if(!empty($docDecInversion['file_1'])){
            $exten_file_1=explode(".", $docDecInversion['file_1']);
            for($i=0; $i <= count($exten_file_1)-1; $i++){
                if($exten_file_1[$i] == 'pdf' || $exten_file_1[$i] == 'doc' || $exten_file_1[$i] =='docx'){
                    //return $exten_file_1[$i];
                     $extencion_adi_file_1 = $exten_file_1[$i];
                }
            }
        } else {
            $extencion_adi_file_1 = '';
        }
        if (!empty($docDecInversion['file_2'])) {
            $exten_file_2=explode(".", $docDecInversion['file_2']);
            for($i=0; $i <= count($exten_file_2)-1; $i++){
                if($exten_file_2[$i] == 'pdf' || $exten_file_2[$i] == 'doc' || $exten_file_2[$i] =='docx'){
                    //return $exten_file_1[$i];
                     $extencion_adi_file_2 = $exten_file_2[$i];
                }
            }
        }else {
            $extencion_adi_file_2 = '';
        }
        if (!empty($docDecInversion['file_3'])) {
            $exten_file_3=explode(".", $docDecInversion['file_3']);
            for($i=0; $i <= count($exten_file_3)-1; $i++){
                if($exten_file_3[$i] == 'pdf' || $exten_file_3[$i] == 'doc' || $exten_file_3[$i] =='docx'){
                    //return $exten_file_1[$i];
                     $extencion_adi_file_3 = $exten_file_3[$i];
                }
            }
        }else {
            $extencion_adi_file_3 = '';
        }
        if (!empty($docDecInversion['file_4'])) {
            $exten_file_4=explode(".", $docDecInversion['file_4']);
            for($i=0; $i <= count($exten_file_4)-1; $i++){
                if($exten_file_4[$i] == 'pdf' || $exten_file_4[$i] == 'doc' || $exten_file_4[$i] =='docx'){
                    //return $exten_file_1[$i];
                     $extencion_adi_file_4 = $exten_file_4[$i];
                }
            }
        }else {
            $extencion_adi_file_4 = '';
        }
        if (!empty($docDecInversion['file_5'])) {
            $exten_file_5=explode(".", $docDecInversion['file_5']);
            for($i=0; $i <= count($exten_file_5)-1; $i++){
                if($exten_file_5[$i] == 'pdf' || $exten_file_5[$i] == 'doc' || $exten_file_5[$i] =='docx'){
                    //return $exten_file_1[$i];
                     $extencion_adi_file_5 = $exten_file_5[$i];
                }
            }
        }else {
            $extencion_adi_file_5 = '';
        }
        if (!empty($docDecInversion['file_6'])) {
            $exten_file_6=explode(".", $docDecInversion['file_6']);
            for($i=0; $i <= count($exten_file_6)-1; $i++){
                if($exten_file_6[$i] == 'pdf' || $exten_file_6[$i] == 'doc' || $exten_file_6[$i] =='docx'){
                    //return $exten_file_1[$i];
                     $extencion_adi_file_6 = $exten_file_6[$i];
                }
            }
        }else {
            $extencion_adi_file_6 = '';
        }
        if (!empty($docDecInversion['file_7'])) {
            $exten_file_7=explode(".", $docDecInversion['file_7']);
            for($i=0; $i <= count($exten_file_7)-1; $i++){
                if($exten_file_7[$i] == 'pdf' || $exten_file_7[$i] == 'doc' || $exten_file_7[$i] =='docx'){
                    //return $exten_file_1[$i];
                     $extencion_adi_file_7 = $exten_file_7[$i];
                }
            }
        }else {
            $extencion_adi_file_7 = '';
        }
        if (!empty($docDecInversion['file_8'])) {
            $exten_file_8=explode(".", $docDecInversion['file_8']);
            for($i=0; $i <= count($exten_file_8)-1; $i++){
                if($exten_file_8[$i] == 'pdf' || $exten_file_8[$i] == 'doc' || $exten_file_8[$i] =='docx'){
                    //return $exten_file_1[$i];
                     $extencion_adi_file_8 = $exten_file_8[$i];
                }
            }
        }else {
            $extencion_adi_file_8 = '';
        }
        if (!empty($docDecInversion['file_9'])) {
            $exten_file_9=explode(".", $docDecInversion['file_9']);
            for($i=0; $i <= count($exten_file_9)-1; $i++){
                if($exten_file_9[$i] == 'pdf' || $exten_file_9[$i] == 'doc' || $exten_file_9[$i] =='docx'){
                    //return $exten_file_1[$i];
                     $extencion_adi_file_9 = $exten_file_9[$i];
                }
            }
        }else {
            $extencion_adi_file_9 = '';
        }
        if (!empty($docDecInversion['file_10'])) {
            $exten_file_10=explode(".", $docDecInversion['file_10']);
            for($i=0; $i <= count($exten_file_10)-1; $i++){
                if($exten_file_10[$i] == 'pdf' || $exten_file_10[$i] == 'doc' || $exten_file_10[$i] =='docx'){
                    //return $exten_file_1[$i];
                     $extencion_adi_file_10 = $exten_file_10[$i];
                }
            }
        }else {
            $extencion_adi_file_10 = '';
        }
        if (!empty($docDecInversion['file_11'])) {
            $exten_file_11=explode(".", $docDecInversion['file_11']);
            for($i=0; $i <= count($exten_file_11)-1; $i++){
                if($exten_file_11[$i] == 'pdf' || $exten_file_11[$i] == 'doc' || $exten_file_11[$i] =='docx'){
                    //return $exten_file_1[$i];
                     $extencion_adi_file_11 = $exten_file_11[$i];
                }
            }
        }else {
            $extencion_adi_file_11 = '';
        }



        /////////////////////////////////////////////////

        $planillaDjir02=PlanillaDjir_02::where('gen_declaracion_inversion_id',$id)->first();
        //dd($planillaDjir02);
    
         $divisas=GenDivisa::orderBy('ddivisa_abr','asc')->pluck('ddivisa_abr','id');
         //dd($divisas);

        $estado_observacion_planilla02=GenStatus::whereIn('id', [11,12,13])->pluck('nombre_status','id');
           //dd($estado_observacion);

        $validardeclaracion=BandejAnalistaDecInversion::where('gen_declaracion_inversion_id',$solicitudDeclaracion->id)->where('gen_usuario_id',Auth::user()->id)->first();
        //dd($validardeclaracion);
        /*******************************************************/

/***********************************ANALISTA PLANILLA3*************************************************/


        $planillaDjir03=PlanillaDjir_03::where('gen_declaracion_inversion_id',$id)->first();
        //dd($planillaDjir03);
        $sectorproducteconomico=GenSectorProductivoDjir_03::where('gen_declaracion_inversion_id',$id)->get();

        $destinoInversion=GenDestinoInversionDjir_03::where('gen_declaracion_inversion_id',$id)->get();

        $estatusplanilla3=GenStatus::whereIn('id', [11,12,13])->pluck('nombre_status','id');
        if ($planillaDjir01->gen_status_id == 16 && $planillaDjir02->gen_status_id == 16 && $planillaDjir03->gen_status_id == 16) {
          $estado_observacion_planilla1=GenStatus::whereIn('id', [13])->pluck('nombre_status','id');
          $estado_observacion_planilla02=GenStatus::whereIn('id', [13])->pluck('nombre_status','id');
          $estatusplanilla3=GenStatus::whereIn('id', [13])->pluck('nombre_status','id');
        }
        return view('AnalistInverExtranjera.edit', compact('planillaDjir01','planillaDjir02','planillaDjir03','estados','municipios','accionistas','solicitudDeclaracion','planillaDjir02','estado_observacion_planilla02','sectorproducteconomico','destinoInversion','estatusplanilla3','estado_observacion_planilla1','docDecInversion','extencion_adi_file_1','extencion_adi_file_2','extencion_adi_file_3','extencion_adi_file_4','extencion_adi_file_5','extencion_adi_file_6','extencion_adi_file_7','extencion_adi_file_8','extencion_adi_file_9','extencion_adi_file_10','extencion_adi_file_11','divisas'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\GenSolicitud  $genSolicitud
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, GenDeclaracionInversion $genSolicitud ,$id)
    {
        // dd($request->all());
        /*Observacionesdeplanilla3*/

        switch ($request->planillaDec) {
            case 1:
                    if(!empty($request->descrip_observacion ) && $request->gen_status_id==11){
                 
                        $declaracionInversion=PlanillaDjir_01::where('gen_declaracion_inversion_id',$id)->update(['gen_status_id'=>11,'estado_observacion'=>1,'descripcion_observacion'=>$request->descrip_observacion,'updated_at' => date("Y-m-d H:i:s") ]);

                        $bandejadeclaracioninversion=BandejAnalistaDecInversion::where('gen_declaracion_inversion_id',$id)->update(['gen_status_id'=>11,'updated_at' => date("Y-m-d H:i:s") ]);

                        $solicitudDeclaracion=GenDeclaracionInversion::where('id',$id)->update(['gen_status_id'=>11,'updated_at' => date("Y-m-d H:i:s"), 'fstatus'=>date('Y-m-d') ]);
                         

                       
                    }elseif(empty($request->descrip_observacion ) && $request->gen_status_id == 12){
                      
                        $declaracionInversion=PlanillaDjir_01::where('gen_declaracion_inversion_id',$id)->update(['gen_status_id'=>12,'estado_observacion'=>0,'updated_at' => date("Y-m-d H:i:s") ]);

                        $bandejadeclaracioninversion=BandejAnalistaDecInversion::where('gen_declaracion_inversion_id',$id)->update(['gen_status_id'=>12,'updated_at' => date("Y-m-d H:i:s") ]);
                        $solicitudDeclaracion=GenDeclaracionInversion::where('id',$id)->update(['gen_status_id'=>12,'updated_at' => date("Y-m-d H:i:s"), 'fstatus'=>date('Y-m-d') ]);

                    }elseif(!empty($request->descrip_observacion ) && $request->gen_status_id == 13){
                      // dd($request->descrip_observacion);
                        $declaracionInversion=PlanillaDjir_01::where('gen_declaracion_inversion_id',$id)->update(['gen_status_id'=>13,'estado_observacion'=>1,'descripcion_observacion'=>$request->descrip_observacion,'updated_at' => date("Y-m-d H:i:s") ]);

                        $bandejadeclaracioninversion=BandejAnalistaDecInversion::where('gen_declaracion_inversion_id',$id)->update(['gen_status_id'=>13,'updated_at' => date("Y-m-d H:i:s") ]);

                        $solicitudDeclaracion=GenDeclaracionInversion::where('id',$id)->update(['gen_status_id'=>13,'updated_at' => date("Y-m-d H:i:s"), 'fstatus'=>date('Y-m-d') ]);

                    }elseif(empty($request->descrip_observacion ) && $request->gen_status_id == 15){
                      
                        $declaracionInversion=PlanillaDjir_01::where('gen_declaracion_inversion_id',$id)->update(['gen_status_id'=>15,'estado_observacion'=>1,'descripcion_observacion'=>$request->descrip_observacion,'updated_at' => date("Y-m-d H:i:s") ]);

                        $bandejadeclaracioninversion=BandejAnalistaDecInversion::where('gen_declaracion_inversion_id',$id)->update(['gen_status_id'=>15,'updated_at' => date("Y-m-d H:i:s") ]);
                        $solicitudDeclaracion=GenDeclaracionInversion::where('id',$id)->update(['gen_status_id'=>15,'updated_at' => date("Y-m-d H:i:s"), 'fstatus'=>date('Y-m-d') ]);

                    }
                    Alert::warning('Su Planilla Djir01 procesda ')->persistent("OK");
                    return redirect()->route('AnalistDeclaracionInversion.edit', ['id'=>$id]);
                    // return redirect()->action('ListaInverExtranjeraController@index');
                break;
            case 2:
                    
                    if(!empty($request->descrip_observacion ) && $request->gen_status_id==11){
                 
                        $declaracionInversion=PlanillaDjir_02::where('gen_declaracion_inversion_id',$id)->update(['gen_status_id'=>11,'estado_observacion'=>1,'descrip_observacion'=>$request->descrip_observacion,'updated_at' => date("Y-m-d H:i:s") ]);

                        $bandejadeclaracioninversion=BandejAnalistaDecInversion::where('gen_declaracion_inversion_id',$id)->update(['gen_status_id'=>11,'updated_at' => date("Y-m-d H:i:s") ]);

                        $solicitudDeclaracion=GenDeclaracionInversion::where('id',$id)->update(['gen_status_id'=>11,'updated_at' => date("Y-m-d H:i:s"), 'fstatus'=>date('Y-m-d') ]);
                         

                       
                    }elseif (!empty($request->descrip_observacion ) && $request->gen_status_id==13) {

                        $declaracionInversion=PlanillaDjir_02::where('gen_declaracion_inversion_id',$id)->update(['gen_status_id'=>13,'estado_observacion'=>1,'descrip_observacion'=>$request->descrip_observacion,'updated_at' => date("Y-m-d H:i:s") ]);

                        $bandejadeclaracioninversion=BandejAnalistaDecInversion::where('gen_declaracion_inversion_id',$id)->update(['gen_status_id'=>13,'updated_at' => date("Y-m-d H:i:s") ]);

                        $solicitudDeclaracion=GenDeclaracionInversion::where('id',$id)->update(['gen_status_id'=>13,'updated_at' => date("Y-m-d H:i:s"), 'fstatus'=>date('Y-m-d') ]);

                    }elseif(empty($request->descrip_observacion ) && $request->gen_status_id == 12){
                      
                        $declaracionInversion=PlanillaDjir_02::where('gen_declaracion_inversion_id',$id)->update(['gen_status_id'=>12,'estado_observacion'=>0,'updated_at' => date("Y-m-d H:i:s") ]);

                        $bandejadeclaracioninversion=BandejAnalistaDecInversion::where('gen_declaracion_inversion_id',$id)->update(['gen_status_id'=>12,'updated_at' => date("Y-m-d H:i:s") ]);
                        $solicitudDeclaracion=GenDeclaracionInversion::where('id',$id)->update(['gen_status_id'=>12,'updated_at' => date("Y-m-d H:i:s"), 'fstatus'=>date('Y-m-d') ]);

                    }
                    Alert::warning('Su Planilla Djir02 procesda')->persistent("OK");
                    return redirect()->route('AnalistDeclaracionInversion.edit', ['id'=>$id]);
                    
                    //return redirect()->action('ListaInverExtranjeraController@index');
                break;
            case 3:
                    if(!empty($request->descrip_observacion ) && $request->gen_status_id==11){
                 
                        $declaracionInversion=PlanillaDjir_03::where('gen_declaracion_inversion_id',$id)->update(['gen_status_id'=>11,'estado_observacion'=>1,'descrip_observacion'=>$request->descrip_observacion,'updated_at' => date("Y-m-d H:i:s") ]);

                        $bandejadeclaracioninversion=BandejAnalistaDecInversion::where('gen_declaracion_inversion_id',$id)->update(['gen_status_id'=>11,'updated_at' => date("Y-m-d H:i:s") ]);

                        $solicitudDeclaracion=GenDeclaracionInversion::where('id',$id)->update(['gen_status_id'=>11,'updated_at' => date("Y-m-d H:i:s"), 'fstatus'=>date('Y-m-d') ]);
                         

                       
                    }elseif (!empty($request->descrip_observacion ) && $request->gen_status_id==13) {

                        $declaracionInversion=PlanillaDjir_03::where('gen_declaracion_inversion_id',$id)->update(['gen_status_id'=>13,'estado_observacion'=>1,'descrip_observacion'=>$request->descrip_observacion,'updated_at' => date("Y-m-d H:i:s") ]);

                        $bandejadeclaracioninversion=BandejAnalistaDecInversion::where('gen_declaracion_inversion_id',$id)->update(['gen_status_id'=>13,'updated_at' => date("Y-m-d H:i:s") ]);

                        $solicitudDeclaracion=GenDeclaracionInversion::where('id',$id)->update(['gen_status_id'=>13,'updated_at' => date("Y-m-d H:i:s"), 'fstatus'=>date('Y-m-d') ]);

                    }elseif(empty($request->descrip_observacion ) && $request->gen_status_id == 12){
                      
                        $declaracionInversion=PlanillaDjir_03::where('gen_declaracion_inversion_id',$id)->update(['gen_status_id'=>12,'estado_observacion'=>0,'updated_at' => date("Y-m-d H:i:s") ]);

                        $bandejadeclaracioninversion=BandejAnalistaDecInversion::where('gen_declaracion_inversion_id',$id)->update(['gen_status_id'=>12,'updated_at' => date("Y-m-d H:i:s") ]);
                        $solicitudDeclaracion=GenDeclaracionInversion::where('id',$id)->update(['gen_status_id'=>12,'updated_at' => date("Y-m-d H:i:s"), 'fstatus'=>date('Y-m-d') ]);

                    }


                    Alert::warning('Su Planilla Djir03 procesda')->persistent("OK");
                    return redirect()->action('ListaInverExtranjeraController@index');
                break;
            
            default:
                    Alert::warning('estatus para planilla no válido')->persistent("OK");
                    return redirect()->action('ListaInverExtranjeraController@index');
                break;
        }
         

/*Fin obsrvaciones de planilla3*/
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\GenSolicitud  $genSolicitud
     * @return \Illuminate\Http\Response
     */
    public function destroy(GenDeclaracionInversion $genSolicitud)
    {
        //
    }
}
