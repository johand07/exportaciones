@extends('templates/layoutlte')

@section('content')
{!!Form::open(['route'=>'Dudas.store','method'=>'POST','id'=>'dudas']) !!}
              
<div class="panels">
    <div class="row inicial"><br>
        <div class="col-md-12">
            <div class="col-md-2"></div>
            <div class="col-md-4">
            <div class="form-group">
                {!! Form::label('','Nombre o Razón Social') !!}
                {!! Form::text('razon_social',$datosempresa->razon_social,['class'=>'form-control','id'=>'razon_social','onkeypress'=>'return soloLetras(event)','maxlength'=>'40']) !!}
            </div> 
            <div class="form-group">
                {!! Form::label('', 'Correo Electrónico') !!}
                {!! Form::email('correo', $datosempresa->correo, ['class' => 'form-control','id'=>'correo']) !!}
            </div> 
            </div><!--1era columna de 4-->
            <div class="col-md-4">
            <div class="form-group">
            {!! Form::label('','Teléfono') !!}
            {!! Form::text('telefono_movil',$datosempresa->telefono_movil,['class'=>'form-control','id'=>'telefono_movil','onkeypress'=>'return soloNumeros(event)','maxlength'=>'12']) !!}
            </div> 
            <div class="form-group">
                <label>Motivo de Contacto</label>
                <select class="form-control" name="tipo" id="tipo" required="">
                    <option value="">---Seleccione----</option>
                    <option value="Atención al Exportador">Atención al Exportador</option>
                    <option value="Soporte al Usuario">Soporte al Usuario</option>
                </select>  
            </div> 
            </div><!-- 2da columna de 4-->
            <div class="col-md-2"></div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 text-center">
            <div class="col-md-2"></div>
            <div class="col-md-8">
            <div class="form-group">
                {!! Form::label('','Escriba su mensaje') !!}
                {!! Form::textarea('mensaje',null,['class'=>'form-control','id'=>'mensaje','onkeypress'=>'return soloNumerosyLetras(event)','maxlength'=>'500', 'required'=>'true']) !!}
            </div>  
            </div>
            <div class="col-md-2"></div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
          <div class="col-md-4"></div>
              <div class="col-md-4 text-center">
                <div class="form-group">
                  <a href="{{url('/exportador/Dudas')}}" class="btn btn-primary">Cancelar</a>
                  <input class="btn btn-success" type="submit" name="" value="Enviar" onclick="enviardua()">
              </div>
          </div>
          <div class="col-md-4"></div>
        </div>
    </div>
</div>

{!! Form::hidden('gen_usuario_id', $datosempresa->gen_usuario_id)!!}

{{Form::close()}}


<script>
    $('#mensaje_registrado, #respuesta_registrada').prop('disabled', true);
</script>
@stop
