<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Planilla2 extends Model
{
    protected $table = 'planilla_2';
    protected $fillable = [

           'det_declaracion_produc_id',
           'uso_aplicacion',
           'total_mate_import',
           'total_mate_nac',
           'descrip_observacion',
           'bactivo',
          
    ];

     
    public function declaracionProduc(){

     return $this->hasOne('App\Models\DetDeclaracionProduc','id','det_declaracion_produc_id');

    }

    public function materialesImportados() {

        return $this->hasMany('App\Models\MaterialesImportados','planilla_2_mi_id','id');
   
    }

    public function materialesNacionales() {

        return $this->hasMany('App\Models\MaterialesNacionales','planilla_2_id','id');
   
    }


}
