<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BandejaAsignacionesIntranet extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bandeja_asignaciones_intranet', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('gen_solicitud_id')->nullable();
            $table->foreign('gen_solicitud_id')->references('id')->on('gen_solicitud');
            $table->unsignedInteger('gen_usuario_id')->nullable();
            $table->foreign('gen_usuario_id')->references('id')->on('gen_usuario');
            $table->unsignedInteger('gen_status_id')->nullable();
            $table->foreign('gen_status_id')->references('id')->on('gen_status');
            $table->date('fstatus',150)->nullable();
            $table->unsignedInteger('asignado_por')->nullable();
            $table->foreign('asignado_por')->references('id')->on('gen_usuario');
            $table->unsignedInteger('analizado_por')->nullable();
            $table->foreign('analizado_por')->references('id')->on('gen_usuario');
            $table->unsignedInteger('finalizado_por')->nullable();
            $table->foreign('finalizado_por')->references('id')->on('gen_usuario');
            $table->boolean('bactivo')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bandeja_asignaciones_intranet');
    }
}
