<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\GenArancelMercosur;
use App\Models\CatCertRegSeniat;
use App\Models\GenCertRegSeniat;
use App\Models\DocCertRegSeniat;
use App\Models\AspectoCumplidoCertRegSeniat;
use App\Models\BandejaAnalistaCertRegSeniat;
use Auth;
use File;
use Alert;

class AnalistaSeniatController extends Controller
{

    public function index()
    {
        return view('AnalistaSeniat.index');
    }


    public function lista(Request $request)
    {
        if(isset($request->status)){
            
            if($request->status == 12){
                $certificados = GenCertRegSeniat::with('GenStatus')->where('bactivo', 1)
                                                ->where('gen_status_id', $request->status)
                                                ->orWhere('gen_status_id', 29)
                                                ->get();
            }else{
                $certificados = GenCertRegSeniat::with('GenStatus')->where('bactivo', 1)
                                                ->where('gen_status_id', $request->status)
                                                ->get();
            }
        }else{
            $certificados = GenCertRegSeniat::with('GenStatus')->where('bactivo', 1)->get();
        }
       
       // dd($certificados);
        return view('AnalistaSeniat.lista',compact('certificados'));
    }

    public function show($id)
    {
    	$certificado = GenCertRegSeniat::find($id);

        $file = DocCertRegSeniat::where('gen_cert_reg_seniat_id', $id)->first();

        $catCert = CatCertRegSeniat::where('bactivo', 1)->get();

        //dd($catCert);

        return view('AnalistaSeniat.show', compact('certificado', 'file', 'catCert'));
    }


    public function mostar($id)
    {
       // dd(123789);
        $certificado = GenCertRegSeniat::find($id);

         $file = DocCertRegSeniat::where('gen_cert_reg_seniat_id', $id)->first();
         $catCert = CatCertRegSeniat::where('bactivo', 1)->get();
        return view('AnalistaSeniat.mostrar', compact('certificado', 'file','catCert'));
     }





    public function store(Request $request)
    {
        

        $certificado = GenCertRegSeniat::find($request->id);

        $mensaje1   = '';
        $mensaje2   = '';

        if($request->desaprobar){

            $this->validate($request, [
                'observacion_analista'          => 'required'
                
            ]);

            $certificado->observacion_analista  =  $request->observacion_analista;
            $certificado->gen_status_id         =   28;
            $certificado->save();

            $mensaje1 = 'El certificado se ha Desaprobado exitosamente';
            $mensaje2 = 'Desaprobado con exito';

        }elseif($request->aprobar){

            $this->validate($request, [
                "gen_cert_reg_seniat_id"    => "required|array|min:1",
                "gen_cert_reg_seniat_id.*"  => "required|distinct|min:1",
            ]);

            if(isset($request->gen_cert_reg_seniat_id)){

                $contador = count($request->gen_cert_reg_seniat_id);

                for($i=0; $i <= $contador-1; $i++){

                    $aspectoCumplido = new AspectoCumplidoCertRegSeniat;
                    $aspectoCumplido->gen_cert_reg_seniat_id    =   $request->id;
                    $aspectoCumplido->cat_cert_reg_seniat_id    =   $request->gen_cert_reg_seniat_id[$i];
                    $aspectoCumplido->save();

                }

                if($contador == 13){
                    $certificado->gen_status_id         =   12;
                    $certificado->save();   

                    $mensaje1 = 'El certificado ha sido Aprobado de manera exitosa';
                    $mensaje2 = 'Aprobado con exito';

                }else{
                    $certificado->gen_status_id         =   28;
                    $certificado->save();  

                    $mensaje1 = 'El certificado no cumple con los aspectos requeridos';
                    $mensaje2 = 'Desaprobado';
                }
            }else{

                $certificado->gen_status_id         =   28;
                $certificado->save();  

                $mensaje1 = 'El certificado no cumple con los aspectos requeridos';
                $mensaje2 = 'Desaprobado';
            }
            
        }

        BandejaAnalistaCertRegSeniat::create([
            'gen_cert_reg_seniat_id'    =>      $certificado->id,
            'gen_usuario_id'            =>      Auth::user()->id,
            'gen_status_id'             =>      $certificado->gen_status_id,
            'fstatus'                   =>      date('Y-m-d'),
        ]);

        Alert::success($mensaje1.'!',$mensaje2.'!')->persistent("Ok");

        return redirect()->route('analista');
    	
    }

}
