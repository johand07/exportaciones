<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Facades\Auth;

class Exportadores
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if (Auth::user()->gen_tipo_usuario_id != 1)  {
         // dd('No eres Exportador');
           return redirect('admin/denegadaExportadores');
        }

        return $next($request);
    }
}
