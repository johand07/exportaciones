-- phpMyAdmin SQL Dump
-- version 4.2.12deb2+deb8u2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 18, 2019 at 12:54 PM
-- Server version: 10.0.32-MariaDB-0+deb8u1
-- PHP Version: 7.1.26-1+0~20190113101856.12+jessie~1.gbp7077bb

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `exportaciones`
--

-- --------------------------------------------------------

--
-- Table structure for table `gen_accionista_inversionista`
--

CREATE TABLE IF NOT EXISTS `gen_accionista_inversionista` (
`id` int(10) unsigned NOT NULL,
  `gen_sol_inversionista_id` int(10) unsigned DEFAULT NULL,
  `accionistas_id` int(10) unsigned DEFAULT NULL,
  `bactivo` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `gen_accionista_inversionista`
--

--
-- Indexes for dumped tables
--

--
-- Indexes for table `gen_accionista_inversionista`
--
ALTER TABLE `gen_accionista_inversionista`
 ADD PRIMARY KEY (`id`), ADD KEY `gen_accionista_inversionista_gen_sol_inversionista_id_index` (`gen_sol_inversionista_id`), ADD KEY `gen_accionista_inversionista_accionistas_id_index` (`accionistas_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `gen_accionista_inversionista`
--
ALTER TABLE `gen_accionista_inversionista`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `gen_accionista_inversionista`
--
ALTER TABLE `gen_accionista_inversionista`
ADD CONSTRAINT `gen_accionista_inversionista_accionistas_id_foreign` FOREIGN KEY (`accionistas_id`) REFERENCES `accionistas` (`id`),
ADD CONSTRAINT `gen_accionista_inversionista_gen_sol_inversionista_id_foreign` FOREIGN KEY (`gen_sol_inversionista_id`) REFERENCES `gen_sol_inversionista` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
