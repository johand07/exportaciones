<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BandejaSolInversionista extends Model
{
    protected $table = 'bandeja_sol_inversionista';

       protected $fillable = [
       
       'gen_sol_inversionista_id',
       'gen_usuario_id',
       'gen_status_id',
       'fstatus',
       'bactivo',
     ];

     
   /* Se agrego la relacion de belongsTo uno a muchos con la tabla gen_sol_inversionista*/
     public function rinversionista() {

     return $this->belongsTo('App\Models\GenSolInversionista','gen_sol_inversionista_id','id');

    }

     public function rinversionistauser() {

     return $this->hasOne('App\Models\GenUsuario','id','gen_usuario_id');

    }
    

    public function inverstatus() {

     return $this->hasOne('App\Models\GenStatus','id','gen_status_id');

    }

    
    public function rDetUsuario() {

     return $this->hasOne('App\Models\DetUsuario','gen_usuario_id','gen_usuario_id');

    
    }


    public function rcoordinversion() {

     
      return $this->belongsTo('App\Models\CoordInversionista', 'id', 'bandeja_sol_inversionista_id');

    }
}


