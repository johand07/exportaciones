<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NotificacionesInversiones extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($inversiones)
    {
        $this->inversiones = $inversiones;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {   
        $dia=date("d.m.Y");
        $hora=date("H:i:s");
        $inversiones = $this->inversiones;
        
        return $this->view('emails.notificacionInversiones', compact('inversiones'))->subject("VENTANILLA UNICA DE COMERCIO EXTERIOR (VUCE) - Enviado $dia - $hora");
    }
}
