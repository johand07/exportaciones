@extends('templates/layoutlte')
@section('content')

{{ Form::model($cafecvc,array('route' => array('corporacionCafe.update',$cafecvc->id),'method' => 'PUT','enctype' => 'multipart/form-data')) }}  

@if(isset($cafecvc->observacion) && $cafecvc->edo_observacion == 1)
	<div class="alert alert-dismissible alert-danger">
	  <button type="button" class="close" data-dismiss="alert">&times;</button>
	  <h4 class="alert-heading">Observación!</h4>
	  <p class="mb-0">{{$cafecvc->observacion}}</p>
	</div>
@endif

<div class="content">
	<div class="panel panel-primary">
		<div class="panel-primary" style=" #fff">
			 <div class="panel-heading"><h4>CORPORACIÓN VENEZOLANA DEL CAFÉ</h4></div>
		</div>
		<div class="panel-body"><!--todas las filas row van dentro del panel body-->
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-4">
						<div class="form-group">					
							{!! Form::label('','Exportador (Nombre)') !!}
							{!! Form::text('razon_social',$det_usuario->razon_social,['class'=>'form-control','id'=>'razon_social','readonly'=>'true']) !!}
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">					
							{!! Form::label('','Dirección para Notificaciones') !!}
							
							{!! Form::text('direccion_notif',null,['class'=>'form-control','id'=>'direccion_notif','required'=>'true','required'=>'true']) !!}
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">		
							{!! Form::label('','Clave del pais') !!}
							
							{!! Form::number('clave_pais',null,['class'=>'form-control','id'=>'clave_pais','required'=>'true']) !!}		
							  
						</div>
					</div>
				</div>
			</div><br><br><!--Fin 1er row-->
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-4">
						<div class="form-group">					
							{!! Form::label('','Puerto de Embarque') !!}
							{!! Form::select('gen_aduana_salida_id',$aduanasal,null,['class'=>'form-control','placeholder'=>'Seleccione la Aduana de Salida','id'=>'gen_aduana_salida_id','required'=>'true']) !!}
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">					
							{!! Form::label('','Número de Serie') !!}<br>

							{!! Form::number('num_serie',null,['class'=>'form-control','id'=>'num_serie','required'=>'true']) !!}	
						</div>
						
					</div>
					<div class="col-md-4">
						<div class="form-group">					
							{!! Form::label('','País Productor') !!}<br>

							{!! Form::text('pais_productor','VENEZUELA',['class'=>'form-control','id'=>'pais_productor','readonly'=>'true']) !!}	
						</div>
					</div>
				</div>
			</div><br><br><!--fin 2do row-->
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-4">
						<div class="form-group">					
							{!! Form::label('','País de Destino') !!}
							{!! Form::select('pais_destino',$pais,null,['class'=>'form-control', 'id'=>'pais_destino','placeholder'=>'Seleccione el pais','required'=>'true']) !!}
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							{!! Form::label('','Fecha de Exportación') !!}
							<div class="input-group date">		
								{!! Form::text('fexportacion',null,['class'=>'form-control','readonly'=>'readonly','id'=>'fexportacion'])!!}<span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
							</div>
						</div>
						
					</div>
					<div class="col-md-4">
						<div class="form-group">					
							{!! Form::label('','País de Trasbordo') !!}
							{!! Form::select('pais_transbordo',$pais,null,['class'=>'form-control', 'id'=>'pais_transbordo','placeholder'=>'Seleccione el pais','required'=>'true']) !!}
						</div>
					</div>
				</div>
			</div><br><br><!--Fin 3er row fila -->
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-4">
						<div class="form-group">					
							{!! Form::label('','Nombre del Medio de Transporte') !!}
							
							{!! Form::text('nombre_transporte',null,['class'=>'form-control','id'=>'nombre_transporte','required'=>'true']) !!}
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">					
							{!! Form::label('','Marca de Identificación') !!}
							
							{!! Form::text('marca_ident',null,['class'=>'form-control','id'=>'marca_ident','required'=>'true']) !!}
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">					
							{!! Form::label('','Cargados') !!}<br>

							{!! Form::label('', 'Sacos',['class'=>'radio-inline']) !!}
							{!! Form::radio('cat_cargados_id', '1',false,['class'=>'radio-inline','id'=>'cat_cargados_id']) !!}
								
							{!! Form::label('', 'Granel',['class'=>'radio-inline']) !!}
							{!! Form::radio('cat_cargados_id', '2',false,['class'=>'radio-inline','id'=>'cat_cargados_id']) !!}
								
							{!! Form::label('', 'Contenedor',['class'=>'radio-inline']) !!}
							{!! Form::radio('cat_cargados_id', '3',false,['class'=>'radio-inline','id'=>'cat_cargados_id']) !!}
							   
						</div>
					</div>
				</div>
			</div><br><br><!--Fin 4arto row-->


			<div class="row">
				<div class="col-md-12">
					<div class="col-md-6">
						<div class="form-group">					
							{!! Form::label('','Peso Neto de la Partida') !!}<br>

							{!! Form::number('peso_neto',null,['class'=>'form-control','id'=>'peso_neto','required'=>'true']) !!}	
						    
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">					
							{!! Form::label('','Unidad de Peso') !!}<br>

							{!! Form::select('gen_unidad_medida_id',$UnidadMedida,null,['class'=>'form-control','id'=>'	gen_unidad_medida_id',
								'placeholder'=>'Seleccione Unidad de peso','onchange'=>'valorSelect()', 'required'=>'true']) !!}
							  
						</div>
					</div>
					
				</div>
			</div><br><br><!--Fin 4arto row-->
			
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-12">
						<div class="form-group">					
							{!! Form::label('','Descripción del café (Formato, tipo, si procede)') !!}<br>
							
							{!! Form::label('', 'Arábica verde',['class'=>'radio-inline']) !!}
							{!! Form::radio('cat_tipo_cafe_id', '1',false,['class'=>'radio-inline','id'=>'cat_tipo_cafe_id']) !!}
							
							{!! Form::label('', 'Robusta verde',['class'=>'radio-inline']) !!}
							{!! Form::radio('cat_tipo_cafe_id', '2',false,['class'=>'radio-inline','id'=>'cat_tipo_cafe_id']) !!}
							
							{!! Form::label('', 'Tostado',['class'=>'radio-inline']) !!}
							{!! Form::radio('cat_tipo_cafe_id', '3',false,['class'=>'radio-inline','id'=>'cat_tipo_cafe_id']) !!}
							
							{!! Form::label('', 'Soluble',['class'=>'radio-inline']) !!}
							{!! Form::radio('cat_tipo_cafe_id', '4',false,['class'=>'radio-inline','id'=>'cat_tipo_cafe_id']) !!}

							{!! Form::label('', 'Liquido',['class'=>'radio-inline']) !!}
							{!! Form::radio('cat_tipo_cafe_id', '5',false,['class'=>'radio-inline','id'=>'cat_tipo_cafe_id']) !!}
							
							{!! Form::label('', 'Otros',['class'=>'radio-inline']) !!}
							{!! Form::radio('cat_tipo_cafe_id', '6',false,['class'=>'radio-inline','id'=>'cat_tipo_cafe_id']) !!}  
						</div>
					</div>
				</div>
			</div><br><br>

			<div class="row">
				<div class="col-md-12">
					<div class="col-md-12">
						<div class="form-group">					
							{!! Form::label('','Método de elaboración') !!}<br>
							
							{!! Form::label('', 'Descafeinado',['class'=>'radio-inline']) !!}
							{!! Form::radio('cat_metodo_elab_id', '1',false,['class'=>'radio-inline','id'=>'cat_metodo_elab_id']) !!}
							
							{!! Form::label('', 'Orgánico',['class'=>'radio-inline']) !!}
							{!! Form::radio('cat_metodo_elab_id', '2',false,['class'=>'radio-inline','id'=>'cat_metodo_elab_id']) !!}
							
							{!! Form::label('', 'Certificado',['class'=>'radio-inline']) !!}
							{!! Form::radio('cat_metodo_elab_id', '3',false,['class'=>'radio-inline','id'=>'cat_metodo_elab_id']) !!}
							
							{!! Form::label('', 'Nº Certificado',['class'=>'radio-inline']) !!}
							{!! Form::radio('cat_metodo_elab_id', '4',false,['class'=>'radio-inline','id'=>'cat_metodo_elab_id']) !!}

							{!! Form::label('', 'Café Verde',['class'=>'radio-inline']) !!}
							{!! Form::radio('cat_metodo_elab_id', '5',false,['class'=>'radio-inline','id'=>'cat_metodo_elab_id']) !!}
							
				
							
							{!! Form::label('', 'Vía Seca',['class'=>'radio-inline']) !!}
							{!! Form::radio('cat_metodo_elab_id', '6',false,['class'=>'radio-inline','id'=>'cat_metodo_elab_id']) !!}
							
							{!! Form::label('', 'Vía Humeda',['class'=>'radio-inline']) !!}
							{!! Form::radio('cat_metodo_elab_id', '7',false,['class'=>'radio-inline','id'=>'cat_metodo_elab_id']) !!}
							
							{!! Form::label('', 'Café Soluble',['class'=>'radio-inline']) !!}
							{!! Form::radio('cat_metodo_elab_id', '8',false,['class'=>'radio-inline','id'=>'cat_metodo_elab_id']) !!}<br><br>
							
							{!! Form::label('', 'Centrifugado',['class'=>'radio-inline']) !!}
							{!! Form::radio('cat_metodo_elab_id', '9',false,['class'=>'radio-inline','id'=>'cat_metodo_elab_id']) !!}

							{!! Form::label('', 'Liofilizado',['class'=>'radio-inline']) !!}
							{!! Form::radio('cat_metodo_elab_id', '10',false,['class'=>'radio-inline','id'=>'cat_metodo_elab_id']) !!}
						
						</div>
					</div>
				</div>
			</div>
			<div class="alert alert-info">
  				<strong>POR EL PRESENTE SE CERTIFICA QUE EL CAFÉ ARRIBA DESCRITO FUE PRODUCIDO/BENEFICIADO EN EL PAÍS QUE SE INDICA EN LA CASILLA (PAIS PRODUCTOR) Y EXPORTADO EN LA FECHA QUE SEGUIDAMENTE SE HACE CONSTAR ESTE CERTIFICDO SE EXPIDE EXCLUSIVAMENTE PARA FINES ESTADISTICOS Y PROCEDENCIA DE ORIGEN</strong> 
			</div>

			<div class="row">
				<div class="col-md-12">
					<div class="col-md-6">
						<div class="form-group">

							{!! Form::label('','Normas óptimas de calidad del café verde (ICC Resolución Número 420)') !!}<br>	

							{!! Form::label('', '"S" Plena observancia de las normas óptimas sobre defecto y humedad',['class'=>'radio-inline']) !!}
							{!! Form::radio('norma_calidad_cafe', 'S',false,['class'=>'radio-inline','id'=>'norma_calidad_cafe']) !!}<br><br>

							{!! Form::label('', '"XM" el café no responde a las normas óptimas sobre humedad',['class'=>'radio-inline']) !!}
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{!! Form::radio('norma_calidad_cafe', 'XM',false,['class'=>'radio-inline','id'=>'norma_calidad_cafe']) !!}
						</div>
					</div><br>
					<div class="col-md-6">
						<div class="form-group">					

							{!! Form::label('', '"XD" El café no responde a las normas óptimas sobre defectos ',['class'=>'radio-inline']) !!}
							{!! Form::radio('norma_calidad_cafe', 'XD',false,['class'=>'radio-inline','id'=>'norma_calidad_cafe']) !!}<br><br>

							{!! Form::label('', '"XDM" el café no responde a ninguna de las normas óptimas',['class'=>'radio-inline']) !!}
							{!! Form::label('', '(ni la referente a defectos ni la referente a humedad)',['class'=>'radio-inline']) !!}
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{!! Form::radio('norma_calidad_cafe', 'XDM',false,['class'=>'radio-inline','id'=>'norma_calidad_cafe']) !!}
						</div>
					</div>
				</div>
			</div><br><br>
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-4">
						<div class="form-group">					
							{!! Form::label('','Código Arancelario') !!}
							
							{!! Form::text('codigo_arancelario',null,['class'=>'form-control','id'=>'codigo_arancelario','required'=>'true']) !!}
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">					
							{!! Form::label('','Características especiales (especifique)') !!}
							
							{!! Form::text('caract_especiales',null,['class'=>'form-control','id'=>'caract_especiales','required'=>'true']) !!}
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">					
							{!! Form::label('','Código del Sistema Armonizado (SA)') !!}
							
							{!! Form::text('codio_sist_sa',null,['class'=>'form-control','id'=>'codio_sist_sa','required'=>'true']) !!}
						</div>
					</div>
				</div>
			</div><br><br>
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-4">
						<div class="form-group">					
							{!! Form::label('','Valor (FOB) del embarque') !!}<br>
							
							{!! Form::number('valor_fob',null,['class'=>'form-control','id'=>'valor_fob','required'=>'true']) !!}	
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">	

						{!! Form::label('','Divisas') !!}

						{!! Form::select('gen_divisa_id',$divisas,null,['class'=>'form-control','id'=>'gen_divisa_id','placeholder'=>'Seleccione divisa (Moneda)','onchange'=>'valorSelect()','required'=>'true','required'=>'true']) !!}

							
						</div>
					</div>
					<div class="col-md-4">
						{!! Form::label('','Información adicional') !!}
							
						{!! Form::text('info_adicional',null,['class'=>'form-control','id'=>'info_adicional','required'=>'true']) !!}
						
					</div>
				</div>
			</div><br><br><br><br><!--Fin 4arto row-->


			


			<div class="row text-center">
         	 	<a href="{{ url('/exportador/corporacionCafe')}}" class="btn btn-primary">Cancelar</a>
          		
          		<input type="submit" class="btn btn-success" value="Enviar" name="Enviar" onclick="()"> 

        	</div><br>



		</div>
	</div>
</div>






@stop	