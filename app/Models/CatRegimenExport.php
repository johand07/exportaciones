<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CatRegimenExport extends Model
{
    protected $table='cat_regimen_export';

    protected $fillable =[

     'id',
     'nombre_regimen',
     'disponibles',
     'bactivo'
    ];

   /*public function (){
    
     
   }*/

}

