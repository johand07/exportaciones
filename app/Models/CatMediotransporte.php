<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CatMediotransporte extends Model
{
    protected $table = 'cat_medio_transport';



	protected $fillable = [

        'nombre',
        'bactivo',
    ];

    protected $date =['created_at','updated_at'];
}
