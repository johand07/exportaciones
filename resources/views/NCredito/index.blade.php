@extends('templates/layoutlte')
@section('content')

   <div class="row">
  <div class="col-md-12">
    <div class="row">
      <div class="col-md-10"></div>
      <div class="col-md-2">
        <a class="btn btn-primary" href="{{url('/exportador/NCredito/create')}}">
          <span class="glyphicon glyphicon-file" title="Agregar" aria-hidden="true"></span>
           Registro Nota Crédito
        </a>
      </div>
    </div>
    <table id="listacredito" class="table table-striped table-bordered" style="width:100%">
            <thead>
                <tr>
                   
                    <th>Consignatario</th>
                    <th>Nro. Nota de Crédito</th>
                    <th>Fecha de Emisión</th>
                    <th>Concepto Nota de Crédito</th>
                    <th>Monto Nota de Crédito</th>
                    <th>Nro. Factura Asociada</th>
                    <th>Justificación</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody>
              @foreach($ncredito as  $credit)
                <tr>                   
                    <td>{{$credit->consignatario->nombre_consignatario}}</td>
                    <td>{{$credit->numero_nota_credito}}</td>
                    <td>{{$credit->femision}}</td>
                    <td>{{$credit->concepto_nota_credito}}</td>
                    <td>{{$credit->monto_nota_credito}}</td>
                    <td>{{$credit->factura->numero_factura}}</td>
                    <td>{{$credit->justificacion}}</td>
                    <td>
                       <a href="{{route('NCredito.edit',$credit->id)}}" class="glyphicon glyphicon-edit btn btn-primary btn-sm" title="Modificar" aria-hidden="true"></a>
                    </td>
                </tr>
              @endforeach
            </tbody>
        </table>
  </div>
</div>
@stop
