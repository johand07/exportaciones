@php
use Illuminate\Support\Facades\Crypt;
@endphp
@extends('templates/layoutlte_analist_djo')

@section('content')

<div class="content" style="background-color: #fff">
    <div class="panel-group" style="background-color: #fff;">
      <div class="panel-primary">
        <div class="panel-heading"><h3>Solicitudes de Declaración Jurada de Origen</h3> </div>
        <div class="panel-body">
            <div class="row">
              {{Form::open(['route' =>'AnalisisDjo.index' ,'method'=>'GET'])}}
              <div class="col-md-12">
                <div class="row">
                  <div class="col-md-4">

                    <h4 class="text-info">Bandeja de Entrada:</h4><hr>
                          
                  </div>
                  <div class="col-md-2">
                    <div class="input-group date">
                      {!! Form::text('start_date', null, ['class' => 'form-control text-center','placeholder'=>'Fecha inicio','id'=>'start_date', 'readonly'=>'readonly']) !!}
                      <span style="background-color: #3c8dbc;color:#fffa" class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
                    </div>
                  </div>
                  <div class="col-md-2">
                   <div class="input-group date">
                    {!! Form::text('end_date', null, ['class' => 'form-control text-center','placeholder'=>'Fecha fin','id'=>'end_date', 'readonly'=>'readonly']) !!}
                    <span style="background-color: #3c8dbc;color:#fffa" class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
                  </div>
                </div>
                  <div class="col-md-2">
                    <div>
                      <button id="filtrodate" type="submit" class="btn btn-primary">Buscar</button>
                      <a class="btn btn-warning" href="{{route('AnalisisDjo.index')}}">Resetear</a>
                    </div>
                  </div>
                  <div class="col-md-2">
                    <a class="btn btn-primary" href="{{route('AnalisisDjo.index')}}">
                      <span class="glyphicon glyphicon-refresh" title="Agregar" aria-hidden="true"></span>
                        Actualizar
                    </a>
                  </div>
                  {{Form::close()}}

                </div>


                <table id="listaUsuarios" class="table table-striped table-bordered" style="width:100%">
                        <thead>
                            <tr class="text-center">
                            	  <th class="col-md-1 text-center" >Nº</th>
                                <th class="col-md-2 text-center">Nº Solicitud</th>  
                              
                                <th class="col-md-2 text-center">Tipo</th>                     
                                <!--th>Razon Social</th-->
                                <th>Rif</th>
                                <!--th class="col-md-2 text-center">Nº Productos</th-->
                                <!--th class="col-md-2 text-center">Nº Paises Destinos</th-->                     
                                <th class="col-md-3 text-center">Fecha de Solicitud</th> 
                                <th class="col-md-1 text-center">Estatus</th><br>

                                <th class="col-md-2 text-center">Observación Analista</th>

                                <th class="col-md-2 text-center">Acciones</th>
                               
                                
                            </tr>
                        </thead>
                        <tbody>
                          @foreach($declaraciones as $key =>  $declaracion)
                            @if($declaracion->bactivo == 1)
                            <tr >           
                            	<td  class="col-md-1 text-center" id="aranx_{{ $key }}"><b>{{ $key + 1 }}</b></td>          
                                <td class="col-md-2 text-center"  ><b class="text-primary">{{ $declaracion->num_solicitud }}</b></td>


                                <td class="col-md-2 text-center"  ><b class="text-dark">{{ ($declaracion->tipoUsuario)?$declaracion->tipoUsuario:'' }}</b></td>
                                <!--td >{{-- $declaracion->razon_social --}}</td--> 
                                <td>{{$declaracion->rDetUsuario->rif}}</td>
                                <!--td  class="col-md-2 text-center">{{ $declaracion->num_productos }}</td--> 
                                <!--td class="col-md-2 text-center">{{ $declaracion->num_paises }}</td-->
                                <td class="col-md-3 text-center"><b>{{ $declaracion->fstatus }}</b></td>
                                <td class="col-md-2 text-center">
                                  <b>
                                    @if($declaracion->gen_status_id==12)
                                      <i class="glyphicon glyphicon-ok text-success"></i> Verificado
                                    @elseif($declaracion->gen_status_id==11)
                                         @if(@$declaracion->observacion_corregida==1)
                                            ¡Corregida por el usuario!
                                         @else
                                            ¡Tiene Observaciones!
                                         @endif  
                                    @elseif ($declaracion->gen_status_id==10)
                                      <i class="glyphicon glyphicon-user"></i> Atendido 
                                    @else
                                      {{ $declaracion->rGenStatus->nombre_status }}
                                    @endif
                                  </b>
                                </td>

                                 <!--Validando para pintar la observacion en la vista--> 
                                <td class="col-md-2 text-center">
                                    <!-- Este es el boto VER que cuando le das Click se activa la Modal; por tanto a este boton se le esta agregando una modal -->
                                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" onClick="verObservacion({{$declaracion->id}})">
                                     Ver
                                    </button>
                                </td>


                                <td class="col-md-2 text-center">
                                  <input id="token" type="hidden" name="_token" value="{{ csrf_token() }}">
                                                                    
                                  @if($declaracion->gen_status_id==8)
                                  
                                      <a class="text-center btn btn-sm btn-danger list-inline"><i class="glyphicon glyphicon-remove "></i> {{ $declaracion->nombre_status }}</a>

                                  @elseif($declaracion->gen_status_id==9)
                                  
                    
                                           <a href="{{ route('AnalisisDjo.create',['djo'=>encrypt($declaracion->id)]) }}" class="text-center btn btn-sm btn-danger list-inline"><i class="glyphicon glyphicon-share"></i> <b>Atender</b></a>


                                  @elseif($declaracion->gen_status_id==10)
                                       
                                      <!-- Validando con la bandeja -->
                                      @if(@$declaracion->rBandejaEntradaDjo->gen_usuario_id==Auth::user()->id && @$declaracion->rBandejaEntradaDjo->estado==10)
                                            
                                            <a href="{{ route('AnalisisDjo.create',['djo'=>encrypt($declaracion->id)]) }}" class="text-center btn btn-sm btn-primary list-inline"><i class="glyphicon glyphicon-user"></i> <b>Por Atender</b></a>
                                  
                                      @endif

                                  @elseif($declaracion->gen_status_id==11)

                                      <!-- Validando con la bandeja -->
                                      @if(@$declaracion->rBandejaEntradaDjo->gen_usuario_id==Auth::user()->id && @$declaracion->rBandejaEntradaDjo->estado==11)
                                          <!-- Validando si esta corregida==1 -->
                                          @if(@$declaracion->observacion_corregida==1)
                                              <a href="{{ route('AnalisisDjo.edit',['djo'=>encrypt($declaracion->id)]) }}" class="text-center btn btn-sm btn-info list-inline"><i class="glyphicon glyphicon-pencil"></i> <b>En Observación</b></a> 

                                          @else
                                              <a href="{{ route('AnalisisDjo.edit',['djo'=>encrypt($declaracion->id)]) }}" class="text-center btn btn-sm btn-warning list-inline"><i class="glyphicon glyphicon-pencil"></i><b> En Observación</b> </a>
                                          @endif  
                                      @endif

                                  @elseif($declaracion->gen_status_id==36)

                                   <a href="{{ route('AnalisisDjo.edit',['djo'=>encrypt($declaracion->id)]) }}" class="text-center btn btn-sm btn-warning list-inline"><i class="glyphicon glyphicon-eye-open"></i><b> Revisar</b> </a>



                                  @elseif($declaracion->gen_status_id==12)
                                      @if(@$declaracion->rBandejaEntradaDjo->gen_usuario_id==Auth::user()->id)
                                        <a class="text-center btn btn-sm btn-success list-inline"><i class="glyphicon glyphicon-ok"></i> <b>Verificado</b></a> 
                                        <a href="{{ route('AnalisisDjo.show',['djo'=>encrypt($declaracion->id)]) }}" class="text-center btn btn-sm btn-info list-inline"><i class="glyphicon glyphicon-eye-open"></i><b> Ver </b></a>   
                                      @endif



                                  @elseif($declaracion->gen_status_id==16 || $declaracion->gen_status_id==38)
                                  <a href="{{ route('AnalisisDjo.show',['djo'=>encrypt($declaracion->id)]) }}" class="text-center btn btn-sm btn-info list-inline"><i class="glyphicon glyphicon-eye-open"></i><b> Ver </b></a>
                                  
                                  @elseif($declaracion->gen_status_id==37)

                                  <a href="{{ route('AnalisisDjo.edit',['djo'=>encrypt($declaracion->id)]) }}" class="text-center btn btn-sm btn-danger list-inline"><i class="glyphicon glyphicon-eye-open"></i><b> Revisar</b> </a>
                                  @endif
                                </td>
                                @endif
                              </tr>   
            				      @endforeach
                        </tbody>
                    </table>
              </div>
              
            </div>
        </div>
      </div>
    </div>
</div><!-- fin content-->

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document" style="width:90%">
    <div class="modal-content">
      <div class="modal-header">
       <h5 class="modal-title" id="exampleModalLabel"><h4><b style="color:#337AB7">Observaciones (Analista)</b></h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id='observacionesDjo'>
        <table class="table table-striped table-bordered" style="width:100%">
          <tr>
            <th>código</th>
            <th>descripcion</th>
            <th>planilla 2</th>
            <th>planilla 3</th>
            <th>planilla 4</th>
            <th>planilla 5</th>
            <th>planilla 6</th>
          </tr>
          <tbody id="obsplan">
            
          </tbody>
        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <!--<button type="button" class="btn btn-primary">Save changes</button>-->
      </div>
    </div>
  </div>
</div>

@stop
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="{{asset('js/ajax_v2.1.1.js')}}"></script>
<!--Script para Tooltips guia de campos con stylos bootstrap-->
<script src="https://unpkg.com/@popperjs/core@2"></script>
<script src="https://unpkg.com/tippy.js@6"></script>
<script type="text/javascript">
  
  
//Cuando le damos click al boton VER observacion y sale la modal por medio de un onClick="verObservacion"

//Creando funcion JavaScrip
  function verObservacion (djoId) {
    //Con peticion AJAX
    $.ajax({   
            
          //ruta mas valor que pasa por parametros 
          url: 'AnalisisDjo/observacion/'+djoId, 
            type: 'GET',
     
        })

        .done(function(data) {

          console.log('data', data);

            if (data.length !== 0) {
              //Se Limpia la tabla con  $('#id_donde_empujo').html('');
              $('#obsplan').html('');
              //Otra forma de iterar $.each(data, function(index, val) {
                data.forEach((val, index) => {
                      
                   $('#obsplan').append('<tr><td>'+val.codigo+'</td><td>'+val.descripcion+'</td><td>'+val.obsp2+'</td><td>'+val.obsp3+'</td><td>'+val.obsp4+'</td><td>'+val.obsp5+'</td><td>'+val.obsp6+'</td></tr>') 
                          
                });
                
                
            } else {
             
              }

        })/*cierre done*/
     
        .fail(function(jqXHR, textStatus, thrownError) {
            errorAjax(jqXHR,textStatus)

        })
  }

</script>

