@if(Auth::user()->rol=='Guest')
        <h1>Logeado {{ Auth::user()->name }}--{{ Auth::user()->rol }}</h1>
@endif
@yield('content')
@include('flash::message')
    <!-- /.content -->
<!--Incluyendo cartas con los modulos más usuados en el sistema-->
  <section id="team" class="pb-4">
    <div class="content">
        <h3 class="section-title h3"></h3>
        <div class="row">
            <!-- Team member -->
            <!--div class="col-xs-12 col-sm-6 col-md-3">
                <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                    <div class="mainflip">
                        <div class="frontside">
                            <div class="card">
                                <div class="card-body text-center">
                                     <br><br><br>
                                    <p><img class=" img-fluid" src="/img/certificado.png" style="width: 80px; height: 65px;"></p>
                                    <h4 class="card-title">Certificado de Origen</h4>                             
                                    <a href="#" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                                </div>
                            </div>
                        </div>
                         <div class="backside">
                            <div class="card">
                                <div class="card-body text-center mt-4">
                                    <br><br><br>
                                   <h4 class="card-title">Certificado de Origen</h4>
                                    <p class="card-text justify">El certificado de origen permite determinar y demostrar que un producto o mercancia a exportar proviene de un territorio en especifico, debido a que se acredita de donde proceden ciertos productos</p>
                                    <a href="#" class="btn btn-primary" style="color:#FFFFFF;">CERTIFICADO</a> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div-->
            {{--<div class="row">
                <div class="col-md-12 text-right"><a href="{{action('PdfController@CertificadoRegistroVuce')}}" class="btn btn-danger">Descargar Certificado Registro VUCE</a>
                </div>
            </div><br><br> --}}
            <!-- ./Team member -->
             <div class="col-xs-12 col-sm-4 col-md-3">
                <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                    <div class="mainflip">
                        <div class="frontside">
                            <div class="card">
                                <div class="card-body text-center">
                                     <br><br><br>
                                    <p><img class=" img-fluid" src="/img/datosexportacion.png" style="width: 80px; height: 68px;"></p>
                                    <h4 class="card-title">Declaración de Exportación <br>Realizada</h4>                             
                                    <a href="#" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="backside">
                            <div class="card">
                                <div class="card-body text-center mt-4">
                                    <br><br><br>
                                    <h4 class="card-title">Registro de Operación</h4>
                                    <p class="card-text"></p>
                                    <a href="{{url('exportador/SolicitudER')}}" class="btn btn-primary" style="color:#FFFFFF;">REGISTRO DE <br>EXPORTACIÓN</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
             <!-- Team member -->
            <!-- Team member -->
             <div class="col-xs-12 col-sm-4 col-md-3">
                <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                    <div class="mainflip">
                        <div class="frontside">
                            <div class="card">
                                <div class="card-body text-center">
                                     <br><br><br>
                                    <p><img class=" img-fluid" src="/img/ventadivisa_2.png" style="width: 80px; height: 65px;"></p>
                                    <h4 class="card-title">Declaración de Venta de Divisa</h4>                             
                                    <a href="#" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="backside">
                            <div class="card">
                                <div class="card-body text-center mt-4">
                                    <br><br><br>
                                    <h4 class="card-title">Registro de Venta de Divisa</h4>
                                    <p class="card-text"></p>
                                    <a href="{{url('exportador/DvdSolicitud')}}" class="btn btn-primary" style="color:#FFFFFF;">EXPORTACIÓN <br> REALIZADA</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ./Team member -->
            <!-- ./Team member -->
            <!-- Team member -->
             <div class="col-xs-12 col-md-4 col-md-3">
                <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                    <div class="mainflip">
                        <div class="frontside">
                            <div class="card">
                                <div class="card-body text-center">
                                     <br><br><br>
                                    <p><img class=" img-fluid" src="/img/inversiones.png" style="width: 80px; height: 65px;"></p>
                                    <h4 class="card-title">Solicitud de Potencial Inversionista</h4>
                                    <!--<p class="card-text">Gestión de Médicos</p>-->
                                    <a href="#" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="backside">
                            <div class="card">
                                <div class="card-body text-center mt-4">
                                    <br><br><br>
                                    <h4 class="card-title">Inversiones Extranjeras</h4>
                                    <p class="card-text"></p>
                                    <a href="{{url('exportador/BandejaInversionista')}}" class="btn btn-primary" style="color:#FFFFFF;">Solicitud de <br>Potencial Inversionista</a>
                                </div>
                                 <div class="card-body text-center mt-4">
                                    <br>
                                    <p class="card-title">Declaración Jurada de Inversión</p>
                                    <a href="{{url('exportador/BandejaInversion')}}" class="btn btn-primary" style="color:#FFFFFF;">Declaración Jurada de <br>Inversión Realizada</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ./Team member -->
             <!-- Team member -->
             <div class="col-xs-12 col-md-4 col-md-3">
                <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                    <div class="mainflip">
                        <div class="frontside">
                            <div class="card">
                                <div class="card-body text-center">
                                     <br><br><br>
                                    <p><img class=" img-fluid" src="/img/declaracion.png" style="width: 85px; height: 70px;"></p>
                                    <h4 class="card-title">Declaración Jurada <br>de Origen</h4>
                                    <!--<p class="card-text">Gestión de Médicos</p>-->
                                    <a href="#" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="backside">
                            <div class="card">
                                <div class="card-body text-center mt-4">
                                    <br><br><br>
                                    <h4 class="card-title">Declaración jurada</h4>
                                    <p class="card-text"></p>
                                    <a href="{{url('exportador/ListaDeclaracionJO')}}" class="btn btn-primary" style="color:#FFFFFF;">DECLARACIÓN</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
           
        </div>
    </div>
</section>
<!--Incluyendo cartas con los modulos más usuados en el sistema-->
