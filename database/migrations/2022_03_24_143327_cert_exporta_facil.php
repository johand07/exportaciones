<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CertExportaFacil extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cert_exporta_facil', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('gen_usuario_id')->index()->nullable();
            $table->foreign('gen_usuario_id')->references('id')->on('gen_usuario');
             $table->string('tipo_meracncia', 255)->nullable();
            $table->string('meracncia', 255)->nullable();
            $table->string('codigo_arancelario', 255)->nullable();
            $table->unsignedInteger('pais_id')->index()->nullable();
            $table->foreign('pais_id')->references('id')->on('pais');
            $table->double('peso_estimado')->nullable();
            $table->integer('alto')->nullable();
            $table->integer('ancho')->nullable();
            $table->integer('largo')->nullable();
            $table->string('correo', 255)->nullable();
            $table->double('cantidad')->nullable();
            $table->string('tratamiento_especial', 255)->nullable();
            $table->text('direccion_emisor')->nullable();
            $table->text('direccion_receptor')->nullable();
            $table->boolean('bactivo')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cert_exporta_facil');
    }
}
