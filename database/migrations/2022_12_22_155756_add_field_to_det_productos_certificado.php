<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldToDetProductosCertificado extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
          Schema::table('det_productos_certificado', function (Blueprint $table) {
            $table->text('descripcion_comercial')->nullable()->after('denominacion_mercancia');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('det_productos_certificado', function (Blueprint $table) {
            //
            $table->dropColumn('descripcion_comercial');
        });
    }
}