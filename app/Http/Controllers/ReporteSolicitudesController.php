<?php

namespace App\Http\Controllers;
use App\Http\Requests\ReportesSolicitudes;
use App\Models\GenSolicitud;
use App\Models\GenDVDSolicitud;
use Illuminate\Http\Request;



class ReporteSolicitudesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $titulo = 'Reporte de Solicitudes';
        $descripcion = 'Solicitudes Er';
        $fecha=date("Y-m-d");
        $fecha_desde=$fecha.' 00:00:00';
        //dd($fecha_desde);
        $fecha_hasta=$fecha.' 23:59:59';
        $desde=$fecha;
        $hasta=$fecha;
        $titulo = 'Reporte de Solicitudes';
        $descripcion = 'Solicitudes Er';

       $sol_bienes=count(GenSolicitud::where('tipo_solicitud_id',1)->whereBetween('created_at',[$fecha_desde,$fecha_hasta])->get());

             /**************************Consulta para de solicitudes de Tecnologia me traigo las que tengo resgitrada*/

            $sol_tecno=count(GenSolicitud::where('tipo_solicitud_id',2)->whereBetween('created_at',[$fecha_desde,$fecha_hasta])->get());
                //dd($sol_tecno);

        



    /********************* Consulta de Solicitudes de DVD***********************************/
        

        $array_bienes=[];// Creo un array lo voy a llenar con los id de mis solicitudes//
        $array_tecno =[];// creo un array lo voy a llenar con los id de mis solicitudes//
        
        //Creo la consulta para traerme las solicitudes con un rango de fecha
        $id_sol_tecno=GenSolicitud::where('tipo_solicitud_id',2)->whereBetween('created_at',[$fecha_desde,$fecha_hasta])->get();

        ///Itero mi array///
        foreach ($id_sol_tecno as $key => $value) {
          $array_tecno[]=$value->id;
          
        }

         $id_sol_bienes=GenSolicitud::where('tipo_solicitud_id',1)->whereBetween('created_at',[$fecha_desde,$fecha_hasta])->get();
        
        //Itero mi array que voy a llenar con los id de mis solicitudes//
        foreach ($id_sol_bienes as $key => $value) {
          $array_bienes[]=$value->id;
          
        }
            //dd($array_elem);
            //dd($id_sol_bienes->id);

          $dvd_bienes=count(GenDVDSolicitud::whereBetween('created_at',[$fecha_desde,$fecha_hasta])->WhereIn('gen_solicitud_id',$array_bienes)->get());
            //dd($dvd_bienes);

          $dvd_tecno=count(GenDVDSolicitud::whereBetween('created_at',[$fecha_desde,$fecha_hasta])->WhereIn('gen_solicitud_id',$array_tecno)->get());

         
        return view ('ReporteSolicitudes.index', compact('titulo','descripcion','sol_bienes','sol_tecno','dvd_bienes','dvd_tecno','desde','hasta'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ReportesSolicitudes $request)
    {
        //dd($request->all());
        $fecha=date("Y-m-d H");
        $fecha_desde=$request->desde.' 00:00:00';
        //dd($fecha_desde);
        $fecha_hasta=$request->hasta.' 23:59:59';
  $desde=$request->desde;
  $hasta=$request->hasta;
        $titulo = 'Reporte de Solicitudes';
        $descripcion = 'Solicitudes Er';

   /**************************Consulta para de solicitudes de bienes me traigo las que tengo resgitrada*/

            $sol_bienes=count(GenSolicitud::where('tipo_solicitud_id',1)->whereBetween('created_at',[$fecha_desde,$fecha_hasta])->get());

             /**************************Consulta para de solicitudes de Tecnologia me traigo las que tengo resgitrada*/

            $sol_tecno=count(GenSolicitud::where('tipo_solicitud_id',2)->whereBetween('created_at',[$fecha_desde,$fecha_hasta])->get());
                //dd($sol_tecno);

        



    /********************* Consulta de Solicitudes de DVD***********************************/
        

        $array_bienes=[];// Creo un array lo voy a llenar con los id de mis solicitudes//
        $array_tecno =[];// creo un array lo voy a llenar con los id de mis solicitudes//
        
        //Creo la consulta para traerme las solicitudes con un rango de fecha
        $id_sol_tecno=GenSolicitud::where('tipo_solicitud_id',2)->whereBetween('created_at',[$fecha_desde,$fecha_hasta])->get();

        ///Itero mi array///
        foreach ($id_sol_tecno as $key => $value) {
          $array_tecno[]=$value->id;
          
        }

         $id_sol_bienes=GenSolicitud::where('tipo_solicitud_id',1)->whereBetween('created_at',[$fecha_desde,$fecha_hasta])->get();
        
        //Itero mi array que voy a llenar con los id de mis solicitudes//
        foreach ($id_sol_bienes as $key => $value) {
          $array_bienes[]=$value->id;
          
        }
            //dd($array_elem);
            //dd($id_sol_bienes->id);

          $dvd_bienes=count(GenDVDSolicitud::whereBetween('created_at',[$fecha_desde,$fecha_hasta])->WhereIn('gen_solicitud_id',$array_bienes)->get());
            //dd($dvd_bienes);

          $dvd_tecno=count(GenDVDSolicitud::whereBetween('created_at',[$fecha_desde,$fecha_hasta])->WhereIn('gen_solicitud_id',$array_tecno)->get());

        //dd($dvd_tecno);

/****************return a mi vista index donde le paso los siguientes parametros********************/

        return view('ReporteSolicitudes.index',compact('sol_bienes','sol_tecno','dvd_tecno','dvd_bienes','titulo','descripcion','desde','hasta'));

    }




    /**
     * Display the specified resource.
     *
     * @param  \App\Models\GenSolicitud  $genSolicitud
     * @return \Illuminate\Http\Response
     */
    public function show(GenSolicitud $genSolicitud)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\GenSolicitud  $genSolicitud
     * @return \Illuminate\Http\Response
     */
    public function edit(GenSolicitud $genSolicitud)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\GenSolicitud  $genSolicitud
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, GenSolicitud $genSolicitud)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\GenSolicitud  $genSolicitud
     * @return \Illuminate\Http\Response
     */
    public function destroy(GenSolicitud $genSolicitud)
    {
        //
    }
}
