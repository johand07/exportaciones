@extends('templates/layoutlte')
@section('content')
{{Form::model($permis,['route'=>['Permisos.update', $permis->id],'method'=>'PATCH','id'=>'formpermiso']) }}
<br><div class="row"> 
<div class="col-md-12">
  <div class="col-md-6">
    <div class="form-group">
      {!! Form::hidden('gen_usuario_id', Auth::user()->id) !!}
      {!! Form::label('Permiso','Tipo de Permiso') !!}
      {!! Form::select('tipo_permiso_id',$tpermiso,null,['class'=>'form-control','placeholder'=>'Seleccione el tipo de Permiso','id'=>'tipo_permiso_id']) !!}
    </div>
    <div class="form-group">
      {!! Form::label('numpermiso','Número de Permiso')!!}
      {!! Form::text('numero_permiso',null,['class'=>'form-control','id'=>'numero_permiso','maxlength'=>'50'])!!}
    </div>
      <div class="form-group">
        {!! Form::label('','Fecha de Permiso') !!} 
        <div class="input-group date">
          {!! Form::text('fecha_permiso',null,['class'=>'form-control','readonly'=>'readonly','id'=>'fecha_permiso']) !!}
          <span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
        </div>                
      </div>
      <div class="form-group">
      {!! Form::label('Monto Aprobado','Monto Aprobado')!!}
      {!! Form::number('monto_aprobado',null,['class'=>'form-control','id'=>'monto_aprobado','maxlength'=>'50','onkeypress'=>'return soloNumeros(event)'])!!}
    </div>
  </div>
  <div class="col-md-6">  
        <div class="form-group">
        {!! Form::label('','Fecha Aprobación de Permiso') !!} 
        <div class="input-group date">
          {!! Form::text('fecha_aprobacion',null,['class'=>'form-control','readonly'=>'readonly','id'=>'fecha_aprobacion']) !!}
          <span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
        </div>                
      </div>   
      <div class="form-group">
            {!! Form::label('','Aduana de Salida') !!} 
        {!! Form::select('gen_aduana_salida_id',$aduanasal,null,['class'=>'form-control','placeholder'=>'Seleccione la Aduana de Salida','id'=>'gen_aduana_salida_id']) !!}
      </div>
      <div class="form-group">
            {!! Form::label('','País Destino') !!} 
        {!! Form::select('pais_id',$pais,null,['class'=>'form-control','placeholder'=>'Seleccione el país destino','id'=>'pais_id']) !!}
      </div>
  </div>
</div>
</div>
<br><div class="row">
  <div class="col-md-12">
    <div class="col-md-4"></div>
    <div class="col-md-4 text-center">
    <a href="{{ route('Permisos.index') }}" class="btn btn-primary">Cancelar</a>
    <input type="submit" class="btn btn-success" value="Enviar" name="" onclick="enviarpermiso()">
    </div>
    <div class="col-md-4"></div>
  </div>
</div>
{{Form::close()}}

@stop
