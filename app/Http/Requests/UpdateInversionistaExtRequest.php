<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateInversionistaExtRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'cat_tipo_usuario_id'             =>      'required',
            'razon_social'                    =>      'required',
            'pais_id'                         =>      'required',
            'gen_divisa_id'                         =>      'required',
            'n_doc_identidad'                 =>      'required_if:cat_tipo_usuario_id,1',
            'ci_repre'                        =>      'required_if:cat_tipo_usuario_id,1',
            'direccion'                       =>      'required',
            'telefono_movil'                  =>      'required',
            'correo'                          =>      'required',
            'tipo_inversion'                  =>      'required',
            'monto_inversion'                 =>      'required_if:cat_tipo_usuario_id,1',
            //'actividad_eco_emp'               =>      'required_if:cat_tipo_usuario_id,1',
            'descrip_proyec_inver'            =>      'required_if:cat_tipo_usuario_id,1',
            'correo_emp_recep'                =>      'required_if:cat_tipo_usuario_id,1',
            'nombre_repre'                    =>      'required_if:cat_tipo_usuario_id,1',
            'apellido_repre'                  =>      'required_if:cat_tipo_usuario_id,1',
            'file_1'                          =>      'mimes:pdf,doc,docx,zip|max:8500',
            'file_2'                          =>      'mimes:pdf,doc,docx,zip|max:8500',
            /*'file_3'                          =>      'mimes:pdf,doc,docx,zip|max:8500',*/
            'file_4'                          =>      'mimes:pdf,doc,docx,zip|max:8500',
            'file_5'                          =>      'mimes:pdf,doc,docx,zip|max:8500',
            'file_6'                          =>      'mimes:pdf,doc,docx,zip|max:8500',
            'file_7'                          =>      'mimes:pdf,doc,docx,zip|max:8500',
            /*'file_8'                          =>      'mimes:pdf,doc,docx,zip|max:8500',
            'file_9'                          =>      'mimes:pdf,doc,docx,zip|max:8500',*/
            'file_10'                         =>      'mimes:pdf,doc,docx,zip|max:8500',
  
  
            
            
            //'cat_tipo_visa_id'                =>      'required_if:letravisa,P,J,G',   
            //'nombre_accionista'               =>      'required_if:cat_tipo_usuario_id,2',   
            //'apellido_accionista'             =>      'required_if:cat_tipo_usuario_id,2',   
            //'cedula_accionista'               =>      'required_if:cat_tipo_usuario_id,2',      
            //'nacionalidad_accionista'         =>      'required_if:cat_tipo_usuario_id,2',   
           // 'participacion_accionista'        =>      'required_if:cat_tipo_usuario_id,2',   
           // 'correo_accionista'               =>      'required_if:cat_tipo_usuario_id,2', 
           
            
          ];
    }
}
