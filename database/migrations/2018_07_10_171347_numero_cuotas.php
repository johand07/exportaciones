<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NumeroCuotas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

      Schema::create('numero_cuotas',function(Blueprint $table){

        $table->engine = 'InnoDB';
        $table->increments('id');
        $table->unsignedInteger('gen_factura_id')->index()->nullable();
        $table->foreign('gen_factura_id')->references('id')->on('gen_factura');
        $table->integer('num_cuotas')->nullable();
        $table->integer('monto')->nullable();
        $table->date('fecha_pago')->nullable();
        $table->timestamps();



        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
          Schema::dropIfExists('numero_cuotas');
    }
}
