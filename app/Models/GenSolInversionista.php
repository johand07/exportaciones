<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GenSolInversionista extends Model
{
    protected $table = 'gen_sol_inversionista';

    protected $fillable = [

    	     'num_sol_inversionista',
           'gen_usuario_id',
           'gen_status_id',
           'pais_id',
           'gen_divisa_id',
           'cat_tipo_visa_id',
           'cat_tipo_usuario_id',
           'cat_sector_inversionista_id',
           'fstatus',
           'estado_observacion',
           'observacion_inversionista',
           'observacion_anulacion',
           'observacion_doc_incomp',
           'n_doc_identidad',
           'tipo_inversion',
           'monto_inversion',
           'descrip_proyec_inver',
           'actividad_eco_emp',
           'correo_emp_recep',
           'pag_web_emp_recep',
           'nombre_repre_legal',
           'apellido_repre_legal',
           'especif_otro_sector',
           'especif_otro_actividad',
           'fecha_expiracion',
           'bactivo',
    ];

    protected $date =['created_at','updated_at'];

  public function usuario() {

    	return $this->hasOne('App\Models\GenUsuario','id','gen_usuario_id');

	}

  public function CatSectorInversionista() {

      return $this->hasOne('App\Models\CatSectorInversionista','id','cat_sector_inversionista_id');

  }

   public function CatActividadEcoInversionista() {

      return $this->hasOne('App\Models\CatActividadEcoInversionista','id','actividad_eco_emp');

  }

	public function estatus() {

    	return $this->hasOne('App\Models\GenStatus','id','gen_status_id');

	}

  public function divisas() {

    return $this->hasOne('App\Models\GenDivisa','id','gen_divisa_id');

}

    public function CatTipoUsuario() {

      return $this->hasOne('App\Models\CatTipoUsuario','id','cat_tipo_usuario_id');

  }

	public function pais() {

    	return $this->hasOne('App\Models\Pais','id','pais_id');

	}

	public function tipovisa() {

    	return $this->hasOne('App\Models\CatTipoVisa','id','cat_tipo_visa_id');

	}

  public function DetUsuario() {

     return $this->hasOne('App\Models\DetUsuario','gen_usuario_id','gen_usuario_id');

    
  }

  public function rbandejainversionista() {

     return $this->hasMany('App\Models\BandejaSolInversionista','gen_sol_inversionista_id','id');

    
  }


}

