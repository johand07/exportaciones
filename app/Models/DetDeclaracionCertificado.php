<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DetDeclaracionCertificado extends Model
{
    protected $table = 'det_declaracion_certificado';
    protected $fillable = [

           'id',
           'gen_certificado_id',
           'num_orden_declaracion',
           'normas_criterio',
           'numero_factura',
           'nombre_norma_origen',
           'f_fac_certificado',
           'bactivo',
    ];

    protected $date =['created_at','updated_at'];

    public function GenCertificado() {

     return $this->hasOne('App\Models\GenCertificado','id','gen_certificado_id');

    }

    public function DetProductosCertificado() {

        return $this->hasOne('App\Models\DetProductosCertificado','gen_certificado_id','gen_certificado_id');
   
    }
}
