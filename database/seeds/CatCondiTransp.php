<?php

use Illuminate\Database\Seeder;
use App\Models\CatCondtransporte;

class CatCondiTransp extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $condiTransp = [
            [
                'id'         => 1,
                'nombre'       => 'Ambiente',
                'created_at' => '2021-12-22 13:59:08',
                'updated_at' => '2021-12-22 13:59:08',
            ],
            [
                'id'         => 2,
                'nombre'       => 'De refrigeración',
                'created_at' => '2021-12-22 13:59:08',
                'updated_at' => '2021-12-22 13:59:08',
            ],
            [
                'id'         => 3,
                'nombre'       => 'De Congelación',
                'created_at' => '2021-12-22 13:59:08',
                'updated_at' => '2021-12-22 13:59:08',
            ],
        ];

        CatCondtransporte::insert($condiTransp);
    }
}
