@extends('templates/layoutlte')
@section('content')

<div class="content">
<div class="panel-group" style="background-color: #fff;">
      <div class="panel-primary">
        <div class="panel-heading"><h3>(P-4) - 6. Proceso productivo literal del producto a exportar</h3></div>
        <div class="panel-body">


<div class="row" style="background-color: #fff">
    <div class="col-md-12">
 
{!! Form::open(['route' =>'DJOPlanilla4.store' ,'method'=>'POST', 'id'=>'planilla4Form']) !!}

        <div class="row">
            <div class="col-md-12"><h4><b>Producto:</b><h4></div>
            <div class="col-md-12">
                <h4>{{$producto->descrip_comercial}} (COD.ARANCELARIO: {{$producto->codigo}})</h4>
            </div>
            <div class="col-md-12" >
                <div class="form-group" style="border:1px solid; border-radius:5px;">
                    {!! Form::textarea('proceso',null,['class'=>'form-control editor-texto','placeholder'=>'Escribe aqui el proceso productivo de tu producto a Exportar..'])!!}
                </div>
            </div>
        </div>

        <div class="row" style="background-color: #fff">

              <div class="col-md-12" align="center">

                  <input type="submit"  name="submit" id="submit" class="btn btn-primary" value="Guardar" onclick="validacionForm()">
                  <a class="btn btn-danger btn-md" href="{{ url('exportador/DeclaracionJO') }}">Cancelar</a>

              </div>
            
          </div>


{{Form::close()}}

    </div>
</div>

        </div>
      </div>
    </div>

  </div><!-- Fin content -->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
    <script>
    CKEDITOR.config.browserContextMenuOnCtrl = false;
        CKEDITOR.replace( 'proceso',{

              language: 'es',
              uiColor: '#FFC3DF28',
              toolbar: [
                         { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-'] },
                        { name: 'insert', items: [ 'HorizontalRule','PageBreak'] },
                        { name: 'tools', items: [ 'Maximize', 'ShowBlocks' ] },
                        { name: 'editing', groups: [ 'find', 'selection', 'spellchecker' ], items: [ 'Find', 'Replace', '-', 'SelectAll', '-', 'Scayt' ] },
                       

                ],
              

        });
        

    </script>


<script>
//Validación de formulario, se puede llamar y pasar la sección que contenga los inputs a validar
$(document).ready(function ()
{
            var error = '<div class="msg_alert"><span style="color:red" class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span><strong id="cod_aran-error" class="added"> El campo es obligatorio.</strong></div>';
              var valido = '<div class="msg_alert"><span style="color:green" class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span></div>';
              var mens=['Estimado Usuario. Debe completar los campos solicitados',]

              validacionForm = function() {

              $('#planilla4Form').submit(function(event) {
              
              var campos = $('#planilla4Form').find('textarea');
              var n = campos.length;
              var err = 0;

              $("div").remove(".msg_alert");
              //bucle que recorre todos los elementos del formulario
              for (var i = 0; i < n; i++) {
                  var cod_input = $('#planilla4Form').find('textarea').eq(i);
                  if (!cod_input.attr('noreq')) {
                    if (cod_input.val() == '' || cod_input.val() == null)
                    {
                      err = 1;
                      cod_input.css('border', '1px solid red').after(error);
                    }
                    else{
                      if (err == 1) {err = 1;}else{err = 0;}
                      cod_input.css('border', '1px solid green').after(valido);
                    }
                    
                  }
              }

              //Si hay errores se detendrá el submit del formulario y devolverá una alerta
              if(err==1){
                  event.preventDefault();
                  swal("Por Favor!", mens[0], "warning")
                }

                });
              }
            });
</script>


@endsection