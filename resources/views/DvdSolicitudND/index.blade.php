@extends('templates/layoutlte')
@section('content')

<div id="myTabContent" class="tab-content">
    <table id="notasdedebito" class="table table-striped table-bordered" style="width:100%">
      <thead>
          <tr>
              <th>Consignatario</th>
              <th>Nro. Nota de Débito</th>
              <th>Fecha de Emisión</th>
              <th>Monto Nota Debito</th>
              <th>Nro. Factura Asociada</th>
              <th>Justificacion</th>
              <th>Acciones</th>
          </tr>
      </thead>
      <tbody>
        @foreach($NDebito as  $debito)
          <tr>
              <td>{{$debito->consignatario->nombre_consignatario}}</td>
              <td>{{$debito->num_nota_debito}}</td>
              <td>{{$debito->fecha_emision}}</td>
              <td>{{$debito->monto_nota_debito}}</td>
              <td>{{$debito->facturand->numero_factura}}</td>
              <td>{{$debito->justificacion}}</td>
              <td>
                 <a href="{{route('ListaVentaND.edit',$debito->id)}}" class="glyphicon glyphicon-pencil btn btn-success btn-sm" title="Generar Nueva Venta" aria-hidden="true"></a>
              </td>
          </tr>  
         @endforeach
      </tbody>
    </table>
</div>
@stop