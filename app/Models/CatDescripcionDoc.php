<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CatDescripcionDoc extends Model
{
    protected $table = 'cat_descrip_doc';
    protected $fillable = [

        'nombre',
        'bactivo',
    ];
}
