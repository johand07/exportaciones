<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Accionistas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accionistas', function (Blueprint $table) {
           $table->engine = 'InnoDB';
           $table->increments('id');
           $table->unsignedInteger('gen_usuario_id')->index()->nullable();
           $table->foreign('gen_usuario_id')->references('id')->on('gen_usuario');
           $table->string('nombre_accionista', 50)->nullable();
           $table->string('apellido_accionista', 50)->nullable();
           $table->string('cedula_accionista', 20)->nullable();
           $table->string('cargo_accionista', 80)->nullable();
           $table->string('telefono_accionista', 15)->nullable();
           $table->string('nacionalidad_accionista', 50)->nullable();
           $table->string('participacion_accionista', 10)->nullable();
           $table->string('correo_accionista', 80)->nullable();
           $table->boolean('bactivo')->default(1);
           $table->timestamps();
        });  
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accionistas');
    }
}
