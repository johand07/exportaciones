<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GenFacturaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

       $id=null;
       if($this->factura)
       {
         $id=$this->factura->id;
       }

        return [

         'numero_factura'=>'required',
         'fecha_factura'=>'required',
         'gen_divisa_id'=>'required',
         'forma_pago_id'=>'required',
         'fecha_estimada_pago'=>'required_if:forma_pago_id,1,3|required_if:tipo_plazo,Total',
         'tipo_instrumento_id'=>'required',
         'incoterm_id'=>'required',
         'monto_fob'=>'required|numeric',
         //'monto_flete'=>'required|numeric',
       //  'monto_seguro'=>'required|numeric',
         //'otro_monto'=>'required|numeric',
         'monto_total'=>'required',
         'gen_dua_id'=>'required_if:tipo_solicitud_id,1',
         'plazo_pago'=>'required_if:forma_pago_id,2',
         'especif_instrumento'=>'required_if:tipo_instrumento_id,3',
         'especif_pago'=>'required_with:otro_monto',
         'tipo_plazo'=>'required_if:forma_pago_id,2',
         'tipo_solicitud_id'=>'required'



          ];
    }
}
