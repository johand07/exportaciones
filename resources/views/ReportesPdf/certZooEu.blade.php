<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Certificado_Sanitario_Europeo-21</title>
    <style type="text/css">
        .centrar{
            text-align: center;
        }

        .titulo{
            
            font-weight: bold;
        }

        .nro-correlativo{
            color: red;
            font-size: 16px;
        }

        .derecha{
            text-align: right;

        }

        .text-normal{
            font-size: 14px;
        }

        .text-medio{
            font-size: 12px;
        }
        .text-11{
            font-size: 11px;
        }
        .text-small{
            font-size: 10px;
        }

        .text-9{
            font-size: 9px;
        }

        .text-8{
            font-size: 8px;
        }

        .espacio-superior{
            margin-top: 10px;
        }

        .espacio-superior-small{
            margin-top: 5px;
        }

        .ancho-medio{
            width: 50%;
        }

        .ancho-full{
            width: 100%;
        }

        .ancho-medio-alto{
            width: 75%;
        }

        .ancho-90{
            width: 90%;
        }

        .table{
            border-collapse: collapse;
        }
        
        .borde{
            border: 1px solid #000;
        }

        .sin-borde{
            border: none;
        }

        .top{
            vertical-align: top;
        }

        .under-dotted{    
            border-bottom: 1px dotted #000;
            text-decoration: none;
        }

        .content-1{
            width: 5%;
        }

        .content-9{
            width: 20%;
        }

        .sangria-1{
            margin-left: 28px;
        }

        .sangria-2{
            margin-left: 36px;
        }

        .sangria-3{
            margin-left: 54px;
        }

        .sangria-sub{
            margin-left: 14px;
            
        }

        
        .page-break { page-break-before: always; }

        .table-center{
            margin: 0 auto; 
        }

        .justify{
            text-align: justify;
        }

        td.justify {
            padding-left: 8px;
            padding-right: 8px; 
        }
        .lineal{
            
        }
        .lineal>div{
            max-width:91%;

        }
        .lineal>*{
           display: inline-block;
           vertical-align:top;
        }

        .sangria-lista{
            list-style-type: none;
            margin: 0;
            padding: 0;
            margin-bottom: 4px;
        }

        .sangria-lista>li{
            margin-top: 5px;
            padding-left: 10px;
            padding-right: 10px;
        }
        .table2{
            border: 0px solid #E2E6E9;
        }

        {--texto-vertical-2 {
            float: left; 
             position: relative; 
             -moz-transform: rotate (270deg);/* FF3.5 + */
             -o-transform: rotate (270deg);/* Opera 10.5 */
             -webkit-transform: rotate (270deg);/* Saf3.1 +, Chrome */
             filter: progid: DXImageTransform.Microsoft.BasicImage (rotación = 3);/* IE6, IE7 */
             -ms-filter: progid: DXImageTransform.Microsoft.BasicImage (rotación = 3);/* IE8 */
       }--}

        .izq{
          margin: 2px;
          text-align: left;
        }

        .der{
          margin: auto;
          text-align: right;
        }
    </style>
</head>
<body>          
     <!--div class="centrar espacio-superior">
        <table class="ancho-full">
            <tr>
                <td class="top center" style="width: 25%; font-size:40%;">
                      <img class="img-responsive" style="width: 565px;" src="img/cintillo.png">
                </td>
            </tr>
        </table>
    </div-->
<!--- Primera parte Datos exportador-->
    <div class="centrar espacio-superior">
        <table style="width:100%" class="table" border=1>
            <tr>
                <td class="text-small table borde" colspan="2">
                     <b>País:</b>{{$certZooEu->pais_origen ? $certZooEu->pais_origen : ''}}
                </td>
                <td class="table borde">
                     <b class="text-small der">Certificado zoosanitario-oficial para la UE</b>
                </td>
            </tr>
            <tr>
               <td  class="top text-8" style="width: 4%;">
                <img class="img-responsive centrar" style="width: 20px;" src="img/Descrip_partida.png">
                </td>
                <!-- Segunda columna-->
                <td>
                    <table style="width:100%" border=1>
                        <tr>
                            <td colspan="2" class="text-small espacio-superior"><b>I.1Expedidor/Exportador</b></td>
                        </tr>   
                        <tr>
                            <td colspan="2" class="text-small"><b>Nombre:</b> IMPORTADORA SABANA MAR, C.A. SPES-1505</td>
                        </tr>   
                        <tr>
                             <td colspan="2" class="text-small"><b>Dirección:</b> AV. TAMANACO CON CALLE LA TINAJA LOCAL GALPON NRO 23 I.4 Autoridad local competente
                             URB EL LLANITO CARACAS (PETARE) MIRANDA ZONA POSTAL 1070. <br><br><br></td>
                        </tr>
                        <tr>
                            <td class="text-small"><b>País:</b>{{$certZooEu->pais_export ? $certZooEu->pais_export : '' }}</td>
                            <td class="text-small"><b>Código ISO del país:</b>{{$certZooEu->cod_iso_pais_export ? $certZooEu->cod_iso_pais_export : ''}}</td>
                        </tr>
                        <tr>
                            <td class="text-small" colspan="2"><b>I.5 Destinatario/Importador</b></td>
                        </tr> 
                        <tr>
                            <td class="text-small" colspan="2"><b>Nombre:</b>{{$certZooEu->nombre_import ? $certZooEu->nombre_import : ''}}</td>
                        </tr>  
                        <tr>
                            <td class="text-small" colspan="2"><b>Dirección:</b>{{$certZooEu->direccion_import ? $certZooEu->direccion_import : ''}}</td>
                        </tr>
                        <tr>
                            <td class="text-small"><b>País:</b>{{$certZooEu->pais_import ? $certZooEu->pais_import : ''}}</td>
                            <td class="text-small"><b>Código ISO del país:</b>{{$certZooEu->cod_iso_pais_inport ? $certZooEu->cod_iso_pais_inport :''}}</td>
                        </tr>
                        <tr>
                            <td class="text-small"><b>I.7 País de origen</b> {{$certZooEu->pais_origen ? $certZooEu->pais_origen : ''}}</td>
                            <td class="text-small"><b>Código ISO del país</b> {{$certZooEu->cod_iso_pais_origen ? $certZooEu->cod_iso_pais_origen : ''}}</td>
                        </tr>
                        <tr>
                            <td class="text-small"><b>I.8 Región de origen</b> {{$certZooEu->region_origen ? $certZooEu->region_origen : ''}}</td>
                            <td class="text-small"><b>Código</b> {{$certZooEu->cod_reg_origen ? $certZooEu->cod_reg_origen : ''}}</td>
                        </tr>
                        <tr>
                            <td class="text-small" colspan="2"><b>I.11 Lugar de expedición</b></td>
                        </tr>
                        <tr>
                            <td class="text-small"><b>Nombre:</b> {{$certZooEu->nombre_lug_exp ? $certZooEu->nombre_lug_exp : ''}}</td>
                            <td class="text-small"><b>Número de registro/
                            Autorización:</b> {{$certZooEu->num_reg_exp ? $certZooEu->num_reg_exp : ''}}</td>
                        </tr>
                        <tr>
                            <td class="text-small" colspan="2"><b>Dirección:</b> {{$certZooEu->dir_lug_exp ? $certZooEu->dir_lug_exp : ''}}</td>
                        </tr>
                        <tr>
                            <td class="text-small"><b>País</b> {{$certZooEu->pais_lug_exp ? $certZooEu->pais_lug_exp : ''}}</td>
                            <td class="text-small"><b>Código ISO del país</b> {{$certZooEu->cod_iso_lug_exp ? $certZooEu->cod_iso_lug_exp : ''}}</td>
                        </tr>
                        <tr>
                            <td class="text-small" colspan="2"><b>I.13 Lugar de carga</b></td>
                        </tr>
                        <tr>
                            <td class="text-small" colspan="2">{{$certZooEu->lug_carga ? $certZooEu->lug_carga : ''}}</td>
                        </tr>
                        <tr>
                            <td class="text-small" colspan="2"><b>I.15 Medios de transporte</b></td>
                        </tr>
                        <tr>
                            <td class="text-small" colspan="2">
                             @foreach($medioTransporte as $data)
                                    <input type="checkbox" style="display:inline" @if($data->id==$certZooEu->cat_medio_transport_id) checked @endif />&nbsp;&nbsp;&nbsp;{{$data->nombre}}
                            @endforeach
                            </td>
                        </tr>
                        <tr>
                            <td class="text-small" colspan="2"><b>Identificación:</b></td>
                        </tr>
                        <tr>
                           <td class="text-small" colspan="2">{{$certZooEu->ident_medio_transport ? $certZooEu->ident_medio_transport : ''}} <br></td> 
                        </tr>
                    </table>
                </td>
                 <!-- Tercera columna-->
                <td>
                    <table  style="width:100%" border=1>
                        <tr>
                            <td class="text-small"><b>I.2. Numero de referencia del certificado</b>  {{$certZooEu->num_ref_cert ? $certZooEu->num_ref_cert : ''}}</td>
                            <td class="text-small der"><b>I.2a Referencia SGICO</b></td>
                        </tr>
                        <tr>
                            <td class="text-small"><b>I.3 Autoridad central competente</b></td>
                            <td class="centrar"><img src="data:image/png;base64,{!! base64_encode(QrCode::format('png')->size(230)->generate(URL::to('/api/CertificadoZooSanitarioEu/'.$certZooEu->id))) !!}" width="140px" height="125px"></td>
                        </tr>
                        <tr>
                            <td class="text-small" colspan="2">
                               <b>I.4 Autoridad local competente <br>
                                INSTITUTO SOCIALISTA DE LA PESCA Y LA <br>ACUICULTURA</b> 
                            </td><br>
                        </tr>
                        <tr>
                            <td class="text-small" colspan="2"><b>I.6 Operador responsable de la partida</b></td>
                        </tr>
                        <tr>
                            <td class="text-small" colspan="2"><b>Nombre:</b> {{$certZooEu->nombre_oper_respons ? $certZooEu->nombre_oper_respons : ''}}</td>
                        </tr>  
                        <tr>
                            <td class="text-small" colspan="2"><b>Dirección:</b> {{$certZooEu->direccion_oper_respons ? $certZooEu->direccion_oper_respons : ''}}</td>
                        </tr>
                        <tr>
                            <td class="text-small"><b>País:</b> {{$certZooEu->pais_oper_respons ? $certZooEu->pais_oper_respons : ''}}</td>
                            <td class="text-small"><b>Código ISO del país:</b> {{$certZooEu->cod_iso_pais_oper_respons ? $certZooEu->cod_iso_pais_oper_respons : ''}}</td>
                        </tr>
                         <tr>
                            <td class="text-small"><b>I.9 País de destino</b> {{$certZooEu->pais_destino ? $certZooEu->pais_destino : ''}}</td>
                            <td class="text-small"><b>Código ISO del país</b> {{$certZooEu->cod_iso_pais_destino ? $certZooEu->cod_iso_pais_destino :''}}</td>
                        </tr>
                        <tr>
                            <td class="text-small"><b>I.10 Región de destino</b> {{$certZooEu->region_destino ? $certZooEu->region_destino : ''}}</td>
                            <td class="text-small"><b>Código</b> {{$certZooEu->cod_reg_dest ? $certZooEu->cod_reg_dest : ''}}</td>
                        </tr>
                        <tr>
                            <td class="text-small" colspan="2"><b>I.12 Lugar de destino</b></td>
                        </tr>
                        <tr>
                            <td class="text-small"><b>Nombre:</b> {{$certZooEu->nombre_lug_dest ? $certZooEu->nombre_lug_dest : ''}}</td>
                            <td class="text-small"><b>Número de registro/autorización:</b> {{$certZooEu->num_reg_lug_dest ? $certZooEu->num_reg_lug_dest : ''}}</td>
                        </tr>
                        <tr>
                            <td colspan="2" class="text-small"><b>Dirección:</b> {{$certZooEu->dir_lug_dest ? $certZooEu->dir_lug_dest : ''}}</td>
                        </tr>
                        <tr>
                            <td class="text-small"><b>País</b> {{$certZooEu->pais_lug_dest ? $certZooEu->pais_lug_dest : ''}}</td>
                            <td class="text-small"><b>Código ISO del país</b> {{$certZooEu->cod_iso_lug_dest ? $certZooEu->cod_iso_lug_dest : ''}}</td>
                        </tr>
                        <tr>
                            <td class="text-small" colspan="2"><b>I.14 Fecha y hora de salida</b> {{$certZooEu->fecha_hora_salida ? $certZooEu->fecha_hora_salida : ''}}</td>
                        </tr>
                        <tr>
                            <td class="text-small" colspan="2"><b>I.16 Puesto de control fronterizo de entrada</b> {{$certZooEu->puest_control_front ? $certZooEu->puest_control_front : ''}}</td>
                        </tr>
                         <tr>
                            <td class="text-small" colspan="2"><b>I.17 Documentos de acompañamiento</b></td>
                        </tr>
                        <tr>
                            <td class="text-small"><b>Tipo:</b> {{$certZooEu->tipo_doc_acomp ? $certZooEu->tipo_doc_acomp : ''}}</td>
                            <td class="text-small"><b>Código:</b> {{$certZooEu->cod_doc_acomp ? $certZooEu->cod_doc_acomp : ''}}</td>
                        </tr>
                        <tr>
                            <td class="text-small"><b>País:</b> {{$certZooEu->pais_doc_acomp ? $certZooEu->pais_doc_acomp : ''}}</td>
                            <td class="text-small">Código ISO del país:  {{$certZooEu->cod_iso_doc_acomp ? $certZooEu->cod_iso_doc_acomp : ''}}</td>
                        </tr>
                        <tr>
                            <td class="text-small" colspan="2">Referencia del documento comercial: {{$certZooEu->ref_doc_comer ? $certZooEu->ref_doc_comer : ''}}</td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <!---Condiciones de transporte-->
        <table style="width:100%" border=2>
            <tr>
                <td class="text-small" colspan="100"><b>I.18 Condiciones de transporte</b></td>    
            </tr>
            <tr>
                <td class="text-small" colspan="100">
                @foreach($CondiTransporte as $data)
                    <input type="checkbox" style="display:inline" @if($data->id==$certZooEu->cat_cond_transport_id) checked @endif />&nbsp;&nbsp;&nbsp;{{$data->nombre}}
                @endforeach
                </td>
            </tr>
            <tr>
                <td class="text-small" colspan="100"><b>I.19 Número de recipiente/Número de precinto</b></td>
            </tr>
            <tr>
                <td class="text-small" colspan="50">Número del recipiente: {{$certZooEu->num_recipiente ? $certZooEu->num_recipiente : ''}}</td>
                <td class="text-small" colspan="50">Número del precinto: {{$certZooEu->num_precinto ? $certZooEu->num_precinto : ''}}</td>
            </tr>
            <tr>
                <td class="text-small" colspan="100"><b>I.20 Certificados como o a efectos de:</b></td>
            </tr>
            <tr>
                <td class="text-small" colspan="100">
                @foreach($catCertEfectosDe as $data)
                    <input type="checkbox" style="display:inline" @if($data->id==$certZooEu->cat_certificado_efectos_de_id) checked @endif />&nbsp;&nbsp;&nbsp;{{$data->nombre}}
                @endforeach
                </td>
            </tr>
             <tr>
                <td class="text-small" colspan="50"><b>I.21</b><br></td>
                <td class="text-small" colspan="50"><b>I.22 <input type="checkbox" name="mercado"> Para el mercado interior:</b>  {{$certZooEu->para_merc_interior ? $certZooEu->para_merc_interior : ''}}</td>
            </tr>
            <tr>
                <td class="text-small" colspan="50"></td>
                <td class="text-small" colspan="50"><b>I.23</b></td>
            </tr> 
            <tr>
                <td class="text-small" colspan="35"><b>I.24 Número total de bultos</b></td>
                <td class="text-small" colspan="35"><b>I.25 Cantidad total</b></td>
                <td class="text-small" colspan="30"><b>I.26 Peso neto / Peso bruto total (kg)</b></td>
            </tr>
            <tr>
                <td class="text-small centrar" colspan="35"> {{$certZooEu->num_total_bultos ? $certZooEu->num_total_bultos : ''}}</td>
                <td class="text-small centrar" colspan="35"> {{$certZooEu->cant_total ? $certZooEu->cant_total : ''}}</td>
                <td class="text-small centrar" colspan="30"> {{$certZooEu->peso_neto_pn ? $certZooEu->peso_neto_pn : ''}} / {{$certZooEu->peso_bruto ? $certZooEu->peso_bruto : ''}}</td>
            </tr>
        </table>
        <!---Descripcion -->
        <table  style="width:100%" border=1>
            <tr>
                <td class="text-small" colspan="100"><b>I.27 Descripción de la partida</b></td>
                <table style="width:100%" border=1>
                    @foreach($descriptPartidaZoo as  $descPart)
                    <tr>
                        <th class="text-small">Código NC </th>
                        <th class="text-small">Especie</th>
                        <th class="text-small">Almacén frigorifico</th>
                        <th class="text-small">Marca de identificación</th>
                        <th class="text-small">Tipos de embalaje</th>
                        <th class="text-small">Naturaleza de la mercancía fábrica / <br>planta procesadora</th>
                    </tr>
                    <tr>
                        <td class="text-small centrar">{{$descPart->codigo_nc ? $descPart->codigo_nc : ''}}</td>
                        <td class="text-small centrar">{{$descPart->especie ? $descPart->especie :''}}</td>
                        <td class="text-small centrar">{{$descPart->almacen_frigo ? $descPart->almacen_frigo :'' }}</td>
                        <td class="text-small centrar">{{$descPart->marc_ident ? $descPart->marc_ident : '' }}</td>
                        <td class="text-small centrar">{{$descPart->tipos_embalaje ? $descPart->tipos_embalaje : '' }}</td>
                        <td class="text-small centrar">{{$descPart->nat_merc ? $descPart->nat_merc : '' }}</td>
                    </tr>
                    <tr>
                        <th class="text-small">Consumidor final</th>
                        <th class="text-small">Tipo de tratamiento</th>
                        <th class="text-small">Fecha de recogida / producción</th>
                        <th class="text-small">Peso neto</th>
                        <th class="text-small">Número de bultos</th>
                        <th class="text-small">Número de lote</th>  
                    </tr>
                    <tr>
                        <td class="text-small centrar">{{$descPart->consumidor_final ? $descPart->consumidor_final : ''}}</td>
                        <td class="text-small centrar">{{$descPart->tipo_tratamiento ? $descPart->tipo_tratamiento : '' }}</td>
                        <td class="text-small centrar">{{$descPart->fecha_recogida ? $descPart->fecha_recogida : ''}}</td>
                        <td class="text-small centrar">{{$descPart->peso_neto ? $descPart->peso_neto : ''}}</td>
                        <td class="text-small centrar">{{$descPart->num_bultos ? $descPart->num_bultos : '' }}</td>
                        <td class="text-small centrar">{{$descPart->num_lotes ? $descPart->num_lotes : ''}}</td> 
                    </tr>
                    @endforeach
                </table>
            </tr>
        </table>
        <!--Segunda Pagina-->
  <!-- Validacion de los productos -->
    @if($cantDescripPartida == 1)
        <div style="page-break-after:always;"></div>
    @endif

<!--Fin de la validacion-->
<table style="width:100%"  border=1>
    <tr>
        <td class="text-medio" colspan="100">
            <b>PAIS VENEZUELA</b>
            </td>
            <td class="text-medio der" colspan="200">
                <b>Productos de la pesca</b>
            </td>
        </tr>
        <tr>
            <td class="text-medio" colspan="100">II. Información sanitaria</td>
            <td class="text-medio" colspan="100">II.a. Referencia del certificado:</td>
            <td class="text-medio" colspan="100">II.b. Referencia SGICO</td>
        </tr>
        <tr>
            <td style="width: 4%;" class="top">
                II.1.
            </td>
            <td class="text-medio" colspan="300">
                (1) Declaración sanitaria 
                <p class="text-medio">El abajo firmante declara que conoce los requisitos pertinentes del Reglamento (CE) n.° 178/2002 del Parlamento Europeo y del Consejo1, del
                    Reglamento ( CE) n.° 852/2004 del Parlamento Europeo y del Consejo2, del Reglamento (CE) n.° 853/2004 del Parlamento Europeo y del Consejo
                    y Reglamento (UE) 2017/625 del Parlamento Europeo y del Consejo y certifica que los productos de la pesca descritos en la parte I se han
                    producidos de conformidad con dichos requisitos, y en particular que:
                    a) se han obtenido en las regiones o los países Venezuela, que, en la fecha de expedición del presente certificado, están autorizados para la
                    introducción en la Unión de productos de la pesca y figuran en la lista de la Comisión de conformidad con el artículo 127, apartado 2, del
                    Reglamento (UE) 2017/625;
                </p>
                <p class="text-medio">b) proceden de establecimientos que aplican los requisitos generales de higiene, que cuentan con un programa basado en los principios de
                    análisis de peligros y puntos de control crítico (APPCC) de conformidad con el artículo 5 del Reglamento (CE) n.° 852/2004, que son sometidos
                    periódicamente a auditoría por las autoridades competentes y que figuran en la lista de establecimientos autorizados de la UE;
                </p>
                <p class="text-medio">c) han sido capturados y manipulados a bordo de buques, desembarcados, manipulados y, en su caso, preparados, transformados, congelados y
                    descongelados higiénicamente conforme a los requisitos establecidos la sección VIII, capítulos I a IV, del anexo III del Reglamento (CE) n.°
                853/2004;</p>
                <p class="text-medio">d) no han sido almacenados en bodegas, cisternas o recipientes utilizados para fines distintos de la producción o el almacenamiento de productos
                de la pesca;</p>
                <p class="text-medio">e) cumplen las normas sanitarias establecidas en la sección VIII, capítulo V, del anexo III del Reglamento (CE) n.° 853/2004 y los criterios
                    establecidos en el Reglamento (CE) n.° 2073/2005 de la Comisión3.,
                    f) han sido embalados, almacenados y transportados conforme a lo dispuesto en la sección VIII, capítulo VI a VIII, del anexo III Reglamento (CE) n.
                ° 853/2004;</p>
                <p class="text-medio">g) han sido marcados conforme a lo dispuesto en la sección I del anexo II del Reglamento (CE) N.° 853/2004;
                    h) si tienen origen en la acuicultura, cumplen las garantías relativas a los animales vivos y sus productos que ofrecen los planes de residuos
                    presentados de conformidad con el artículo 29 de la Directiva 96/23/CE del Consejo4, y los animales y productos en cuestión figuran en la lista
                de la Decisión 2011/163/UE de la Comisión5 con respecto al país de origen correspondientes;</p>
                <p class="text-medio">i) se han producido en condiciones que garantizan el respeto de los contenidos máximos de contaminantes establecidos en el Reglamento (CE)
                n.0 1881/2006 de la comisión7.</p>
                <p class="text-medio">j) han superado satisfactoriamente los controles oficiales establecidos en los artículos 67 a 71 del Reglamento de Ejecución (UE) 2019/627 de la
                Comisición7.</p>
                <p class="text-medio"><s>(2)[II.2. Declaración zoosanitaria relativa a peces vivos y   crustáceos vivos de (3) especies de la lista destinados al consumo humano y a productos
                    de origen animal procedentes de esos animales acuáticos destinados a una transformación ulterior en la Unión antes del consumo humano,
                    excepto peces vivos y crustáceos vivos y sus productos desembarcados de buques pesqueros
                    II.2.1. De acuerdo con la información oficial, los (4) [ animales acuáticos indicados en la casilla I.27 de la parte I] (4)[productos de origen animal
                    procedentes de animales acuáticos distintos de los animales acuáticos vivos indicados en la casilla I.27 de la parte I se han obtenido de animales
                    que] cumplen los siguientes requisitos zoosanitarios:
                    II.2.1.1. Proceden de (4)[un establecimiento] (4)[un hábitat] que no está sujeto a medidas de restricción nacionales por motivos zoosanitarios o
                    debido a una mortalidad anormal por causa indeterminada, en particular por las enfermedades pertinentes de la lista mencionadas en el anexo I
                    del Reglamento Delegado (UE) 2020/692 de la Comisión8 y las enfermedades emergentes.
                    II.2.1.2. Los (4)[animales acuáticos no están destinados a la matanza] (4)[productos de origen animal procedentes de animales acuáticos que no
                    son animales acuáticos vivos se han obtenido de animales que no estaban destinados a la matanza] en el marco de un programa nacional de
                    erradicación de enfermedades, en particular las enfermedades pertinentes de la lista mencionadas en el anexo I del Reglamento Delegado (UE)
                    2020/692 y las enfermedades emergentes.
                    (4)[II.2.2. Los (4) [animales de acuicultura indicados en la casilla I.27 de la parte I] (4) [productos de origen animal procedentes de animales de
                    acuicultura distintos de los animales de acuicultura vivos indicados en la casilla I.27 de la parte I se han obtenido de animales que] cumplen los
                    siguientes requisitos:
                    II.2.2.1. Proceden de un establecimiento de acuicultura que está (4) )[registrado] (4) )[autorizado] por la autoridad competente del tercer país o
                    territorio de origen y sometido a su control, y que cuenta con un sistema para llevar y conservar durante al menos tres años registrados
                actualizados con información sobre:</s></p>
            </td>
        </tr>
    </table>
    <table class="" style="width:100%" border=1>
        <tr>
            <td class="text-medio" colspan="30">Forma GTVC 19-1 / L214 / 11</td>
            <td class="text-medio" colspan="40">2 DE 6</td>
            <td class="text-medio" colspan="30"></td>
        </tr>
        <tr>
            <td class="text-medio" colspan="100">http://registro-empresa.vuce.gob.ve/exportador/certZooEu/{{$id}}</td>
        </tr>
    </table>
    <!--Tercera pagina-->
<div style="page-break-after:always;"></div>
    <table style="width:100%"  border=1>
        <tr>
            <td class="text-medio" colspan="100">
                <b>PAIS VENEZUELA</b>
            </td>
            <td class="text-medio der" colspan="200">
                <b>Productos de la pesca</b>
            </td>
        </tr>
        <tr>
            <td class="text-medio" colspan="100">II. Información sanitaria</td>
            <td class="text-medio" colspan="100">II.a. Referencia del certificado:</td>
            <td class="text-medio" colspan="100">II.b. Referencia SGICO</td>
        </tr>
        <tr>
            <td class="text-medio" colspan="300">

                <p class="text-medio"><s>i) las especies, las categorías y el número de animales de acuicultura del establecimiento;
                    ii) la entrada de animales acuáticos en el establecimiento y la salida de animales de acuicultura de él;
                    iii) la mortalidad en el establecimiento.
                    II.2.2.2. Proceden de un establecimiento de acuicultura que, con una frecuencia proporcional al riesgo que presenta, recibe visitas zoosanitarias
                    periódicas de un veterinario con el fin de detectar, e informar al respecto, signos indicativos de la presencia de enfermedades, en particular las
                    enfermedades pertinentes de la lista mencionadas en el anexo I del Reglamento Delegado (UE) 2020/692 y las enfermedades emergentes.]
                    II.2.3. Requisitos zoosanitarios generales
                    Los (4)[ animales acuáticos indicados en la casilla I.27 de la parte I] (4) [productos de origen animal procedentes de animales acuáticos distintos de los
                    animales acuáticos vivos indicados en la casilla I.27 de la parte I se han obtenido de animales que] cumplen los siguientes requisitos zoosanitarios:
                    (4)(6) [II.2.3.1. Están sujetos a los requisitos de la parte II.2.4 y proceden de (4)[un país] (4)[un territorio], (4)[una zona] (4)[un compartimiento] con el (5)
                    código:_ _-_que, en la fecha de expedición del presente certificado, figura en una lista de terceros países y territorios adoptada por la Comisión de
                    conformidad con el artículo 230, apartado 1, del Reglamento (UE) 2016/429 para la introducción en la Unión de (3) [animales acuáticos] (3) [productos
                    de origen animal procedentes de animales acuáticos que no sean animales acuáticos vivos].]
                    (4)(6)[II.2.3.2. Son animales acuáticos que han sido sometidos por un veterinario oficial a una inspección clínica en las setenta y dos horas previas al
                    momento de la carga. Durante la inspección, los animales no presentaban signos de enfermedad transmisible y, según los registros pertinentes del
                    establecimiento, no había indicios de problemas de salud.]
                    II.2.3.3. Son animales acuáticos que se expiden directamente a la Unión desde su establecimiento de origen.
                    II.2.3.4. No han estado en contacto con animales acuáticos de situación sanitaria inferior.
                    II. Información Sanitaria
                    o bien (4)(6)[II.2.4. Requisitos sanitarios específicos
                    II.2.4.1 Requisitos aplicables a (3) las especies de la lista en relación con la necrosis hematopoyética epizoótica, la infección por el virus del síndrome de
                    Taura y la infección por virus de la cabeza amarilla
                    Los (4) [animales acuáticos indicados en la casilla I.27 de la parte I] (4)[productos de origen animal procedentes de animales acuáticos vivos indicados en
                    la casilla I.27 de la parte se han obtenido de animales que] proceden de (4)[un país] (4)[un territorio] (4)[una zona] (4)[un compartimiento] que se ha
                    declarado libre de (4)[necrosis hematopoyética epizoótica] (4)[infección por el virus del síndrome de Taura] (4)[infección por el virus de cabeza amarilla]
                    conforme a condiciones al menos tan estrictas como las establecidas en el artículo 66 o en el artículo 73, apartado1,y apartado 2, letra a), del
                    Reglamento Delegado (UE) 2020/689 de la Comision9, y, en el caso de los animales acuáticos, ninguna de (3) las especies de la lista en relación con las
                    enfermedades pertinentes:
                    i. se introduce desde otro país, territorio, zona o compartimiento que no se haya declarado libre de las mismas enfermedades;
                    ii. es vacunada contra (4)[esa] (4)[esas] enfermedad(4)[es].]
                    (4)(7)[II.2.4.2. Requisitos aplicables a (3)las especies de la lista en relación con la septicemia hemorrágica viral, la necrosis hematopoyética infecciosa, la
                    infección por el virus de la anemia infecciosa del salmón con supresión en la región altamente polimórfica o la infección por el virus del síndrome de las
                    manchas blancas
                    Los (4)[animales acuáticos indicados en la casilla I.27 de la parte I] (4)[productos de origen animal procedentes de animales acuáticos vivos indicados en
                    la casilla I.27 de la parte se han obtenido de animales que] proceden de (4)[un país] (4)[un territorio] (4)[una zona] (4)[un compartimiento] que se ha
                    declarado libre de (4)[septicemia hemorrágica viral] (4)[necrosis hematopoyética infecciosa] (4)infección por el virus de la anemia infecciosa del salmón
                    con supresión en la región altamente polimórfica] (4) [infección por el virus del síndrome de las manchas blancas] de conformidad con el capítulo 4 de la
                    parte II del Reglamento Delegado (UE) 2020/689, y, en el caso de los animales acuáticos, ninguna de (3) las especies de la lista en la relación con las
                    enfermedades pertinentes:
                    i. se introduce desde otro país, territorio, zona o compartimiento que no se haya declarado libre de las mismas enfermedades;
                    ii. es vacunada contra (4 )[esa] (4)[esas] enfermedad(4)[es].]
                    (4) (8) [II. 2.4.3 Requisitos aplicables a las (9) especies sensibles a la infección por viremia primaveral de la carpa, la renibacteriosis, la infección por el
                    virus de la necrosis pancreática infecciosa, la infección por Gyrodactylus salaris y la infección por el alfavirus de los salmónidos , así como (3) a las
                    especies sensibles al herpesvirus koi.
                    Los (4) [animales acuáticos indicados en la casilla I.27 de la parte I] (4) [productos de origen animal procedentes de animales acuáticos distintos de los
                    animales acuáticos vivos indicados en la casilla I.27 de la parte I se han obtenido de animales que] proceden de (4) [un país] (4) [un territorio] (4) [una
                    zona] (4) [un compartimento] que satisface las garantías sanitarias respecto a (4) [la viremia primaveral de la carpa], (4) [la renibacteriosis), (4) el virus de
                    la necrosis pancreática infecciosa], (4) la infección por Gyrodactyus salaris], (4) [el alfavirus de los salmónidos], (4) [el herpesvirus koi] que son
                    necesarias para cumplir las medidas nacionales de aplicación en el Estado miembro destino, conforme a los actos de ejecución adoptados por la
                Comisión con arreglo al artículo 226, apartado 3, del Reglamento (UE) 2016/429.]]</s></p>
            </td>
        </tr>
    </table>
    <table class="" style="width:100%" border=1>
        <tr>
            <td class="text-medio" colspan="30">Forma GTVC 19-1 / L214 / 11</td>
            <td class="text-medio" colspan="40">3 DE 6</td>
            <td class="text-medio" colspan="30"></td>
        </tr>
        <tr>
            <td class="text-medio" colspan="100">http://registro-empresa.vuce.gob.ve/exportador/certZooEu/{{$id}}</td>
        </tr>
    </table>
<div style="page-break-after:always;"></div>
<!--Cuarta pagina-->
<table style="width:100%"  border=1>
    <tr>
        <td class="text-medio" colspan="100">
            <b>PAIS VENEZUELA</b>
        </td>
        <td class="text-medio der" colspan="200">
            <b>Productos de la pesca</b>
        </td>
    </tr>
    <tr>
        <td class="text-medio" colspan="100">II. Información sanitaria</td>
        <td class="text-medio" colspan="100">II.a. Referencia del certificado:</td>
        <td class="text-medio" colspan="100">II.b. Referencia SGICO</td>
    </tr>
    <tr>
        <td class="text-medio" colspan="300">

            <p class="text-medio"><s>(4) (6) [II.2.4 Requisitos sanitarios específicos
                Los (4) [animales acuáticos indicados en la casilla I.27 de la parte I] (4) [productos de origen animal procedentes de animales acuáticos distintos de los
                animales acuáticos vivos indicados en la casilla I.27 de la parte I se han obtenido de animales que] están destinados a un establecimiento de alimentos
                acuáticos para el control de enfermedades situado en la Unión y autorizado de conformidad con el artículo 11 del Reglamento Delegado (UE) 2020/691
                de la Comisión 10, en el que serán transformados para el consumo humano.]
                II.2.5. A mi leal saber y entender, y según declara el operador, los (4) [animales acuáticos indicados en la casilla I.27 de la parte I]
                (4) [productos de origen animal procedentes de animales acuáticos distintos de los animales acuáticos vivos indicados en la casilla I.27 de la parte I se
                han obtenido de animales que] proceden de (4) [un establecimiento] (4) [un hábitat] donde:
                I. No se han dado casos de mortalidad anormal por causa indeterminada; y
                II. No han estado en contacto con animales acuáticos de (3) especies de la lista que no cumplieran los requisitos indicados en el punto II.2.I.
                II.2.6. Requisitos de transporte
                Se han tomado medidas para transportar a los animales acuáticos indicados en la casilla I.27 de la parte I de conformidad con los requisitos de los
                artículos 167 y 168 del Reglamento Delegado (UE) 2020/692, y concretamente:
                II.2.6.I. cuando los animales se transportan en agua, en la que se transportan no se cambia en un tercer país, territorio, zona o compartimento que no
                figure en la lista para la introducción en la Unión de la especie y categoría de animales acuáticos de que se trate;
                II.2.6.2. los animales no se transportan en condiciones que comprometan su situación sanitaria en particular:
                i) Cuando los animales se transportan en agua, esta no altera su situación sanitaria;
                ii) Los medios de transporte y los recipientes están construidos de forma que no se compromete la situación sanitaria de los animales acuáticos durante
                el transporte
                iii) El (4)[recipiente] (4)[buque vivero] (4)[no se ha utilizado previamente] (4)[se ha limpiado y desinfectado conforme a un protocolo y con productos
                autorizados por la autoridad competente del (4)[tercer país](4)[territorio]de origen, antes de la carga para la expedición a la unión];
                II.2.6.3. desde el momento de la carga en el establecimiento de origen hasta el momento de llegada a la unión, los animales de la partida no son
                transportados en la misma agua ni el mismo (4)[recipiente] (4)[buque vivero] que animales acuáticos de situación sanitaria inferior o no destinados a
                entrar en la unión
                II.2.6.4. cuando es necesario cambiar el agua en (4)[un país] (4)[un territorio] (4)[una zona] (4)[un comportamiento] que figura en la lista para la entrada
                en la unión de la especie y categoría de animales acuáticos de que se trate, el cambio solo se efectúa, (4)[en el caso del transporte por tierra, en puntos
                de cambio de agua aprobados por la autoridad competente del (4)[tercer país] (4)[territorio] donde se cambie el agua] (4)[en el caso del transporte en
                buque vivero, a una distancia de por lo menos 10 km de cualquier establecimiento de acuicultura que se encuentre en la ruta desde el lugar de origen
                hasta el lugar de destino];
                II.2.7. Requisitos de etiquetado
                II.2.7.1. se ha tomado medidas para identificar y etiquetar los (4)[medios de transporte] (4)[recipiente] de conformidad con el artículo 169 del
                reglamento delegado (UE) 2020/692 y, en particular, para identificar la partida mediante (4)[una etiqueta legible y visible en el exterior del recipiente]
                (4)[una entrada en el manifiesto del buque, en caso de transporte en buque vivero,] que la vincule claramente con el presente certificado zoosanitario-
                oficial.
                (4)[II.2.7.2. En el caso de los animales acuáticos, la etiqueta legible y visible a la que se refiere el punto II.2.7.1 contiene, como mínimo, la información
                siguiente
                a) el número de recipiente de la partida;
                b) el nombre de las especies presentes en cada recipientes;
                c) el número de animales por recipientes de cada una de las especies presentes;
                d) el siguiente enunciado: (4)[“”peces vivos destinados al consumo humano en la unión europea””] (4)[“”crustáceo vivos destinados al consumo humano
                en la Unión Europea].]
                (4)[II.2.7.3. En el caso de los productos de origen animal procedentes de animales acuáticos que no sean animales acuáticos vivos, la etiqueta legible y
                visible a la que se refiere el punto II.2.7.1 contiene uno de los siguientes enunciados:
                a) “”pescado destinado a una transformación ulterior en la Unión Europea antes del consumo humano””;
            b) “”crustáceos destinados a una transformación ulterior en la unión europea antes del consumo humano””.]</s></p>
        </td>
    </tr>
</table>
<table style="width:100%" border=1>
    <tr>
        <td class="text-medio" colspan="30">Forma GTVC 19-1 /L214/11</td>
        <td class="text-medio" colspan="40">4 DE 6</td>
        <td class="text-medio" colspan="30"></td>
    </tr>
    <tr>
        <td class="text-medio" colspan="100">http://registro-empresa.vuce.gob.ve/exportador/certZooEu/{{$id}}</td>
    </tr>
</table>
<div style="page-break-after:always;"></div>

<!--Quinta pagina-->
<table style="width:100%"  border=1>
    <tr>
        <td class="text-medio" colspan="100">
            <b>PAIS VENEZUELA</b>
        </td>
        <td class="text-medio der" colspan="200">
            <b>Productos de la pesca</b>
        </td>
    </tr>
    <tr>
        <td class="text-medio" colspan="100">II. Información sanitaria</td>
        <td class="text-medio" colspan="100">II.a. Referencia del certificado:</td>
        <td class="text-medio" colspan="100">II.b. Referencia SGICO</td>
    </tr>
    <tr>
        <td class="text-medio" colspan="300">

            <p class="text-14"><s>II.2.8. Validez del certificado zoosanitario-oficial
                El presente certificado zoosanitario-oficial es válido durante diez días a partir de la fecha de expedición. En caso de transporte de animales acuáticos
            por vía navegable o mar, este periodo de diez días podría ampliarse tanto como dure el viaje por vía navegable o mar.]</s>
        </p>
        <p class="text-14">
            Notas <br>
            De conformidad con el acuerdo sobre la retirada del reino unido de Gran Bretaña e Irlanda del Norte de la Unión Europea y de la Comunidad Europea
            de la Energía Atómica, y en particular con el artículo 5. Apartado 4, del protocolo sobre Irlanda / Irlanda del Norte, en relación con el anexo 2 de dicho
            protocolo, las referencias hechas a la Unión Europea en el presente certificado incluyendo al Reino Unido con respecto a Irlanda del Norte.
            Los « animales acuáticos» Son los animales que se definen en el artículo 4, punto 3, del reglamento (UE) 2016/429 del parlamento Europeo y del
            consejo. Los « animales de acuicultura» son animales acuáticos sometidos a métodos de acuicultura según se define en el artículo 4. Punto 7, del
            reglamento (UE) 2016/429.
            Todos los animales acuáticos y los productos de origen animal procedentes de animales acuáticos que no sean animales acuáticos vivos, a los que se
            aplica la parte II.2.4. Del presente certificado, deben proceder de un país, territorio, zona o compartimento que figuren en una lista de terceros países y
            territorios adoptados por la comisión de conformidad con el artículo 230, apartado 1, del reglamento (UE) 2016/429.
            La parte II.2.4 del certificado no se aplica a los siguientes crustáceos y peces, que, por consiguiente, puede proceder de un país, un territorio o una parte
            de estos que figuren en la lista de la comisión de conformidad con el artículos 127, apartado 2, del Reglamento (UE) 2016/429.  
        </p>
        <p class="text-14">
            a) Crustáceos que estén envasados y etiquetados para el consumo humano de conformidad con los requisitos específicos del Reglamento (CE) n.°
            853/2004 que les sean aplicables, y que ya no puedan sobrevivir como animales vivos si se devuelve al medio acuático;
            b) Crustáceos que estén destinados al consumos humano sin transformación ulterior, siempre que estén envasado para su venta al por menor de
            conformidad con los requisitos del reglamento (CE) n.° 853/2004 aplicables a los envases de que se trate;
            c) Crustáceos que estén envasados y etiquetados para el consumo humano de conformidad con los requisitos específicos del reglamento (CE) n.°
            853/2004 que les sean aplicables y que estén destinados a una transformación ulterior sin almacenamiento temporal en el lugar de transformación;
            d) Peces sacrificados y eviscerados antes de la expedición. 
        </p>
        <p>
            El presente certificado se aplica a los productos de origen animal y los animales acuáticos vivos destinados al consumo humano o directo, así como a los
            animales acuáticos vivos destinados a los siguientes establecimientos de acuicultura: i) un establecimiento de alimentos acuáticos para el control de
            enfermedades según se define en el artículo 4, punto 52, del Reglamento (UE) 2016/429; o ii) un centro de expedición según se define en el artículo 2,
            punto 3, del Reglamento DELEGADO (UE) 2016/691, donde posteriormente se transforman o preparan para el consumo humano.
            El presente certificado zoosanitario-oficial deberá cumplimentarse de conformidad con las notas para la cumplimentación de los certificados
            establecidos en el capítulo 4 del anexo I del Reglamento de ejecución (UE) 2020/2235.  
        </p>
        <p>
            Parte I: <br>
            • Casilla I.20: Márquese «Industria conservera» para los pescados enteros inicialmente congelados en salmuera a -9 °C o a una temperatura superior a -
            18 °C, y destinados a ser preparados en conserva de acuerdo con los requisitos de las sección VIII, capítulo I, parte II, punto 7, del anexo III del
            Reglamento (CE) n.° 853/2004. Márquese «Productos destinados al consumo humano» o «Transformación ulterior» en los demás casos. <br>
            • Casilla I.27: Insértese el código apropiado del Sistema Armonizado (SA) utilizando las partidas como: 0301, 0302, 0303, 0304, 0305, 0306, 0307,
            0308, 0511, 1504,1516, 1518, 1603, 1604,1605, o 2106. <br>
            • Casilla I.27: Descripción de la partida:
            «Naturaleza de la mercancía»: especifique si procede de la acuicultura o del medio natural.
            «Tipo de tratamiento»: especifique si se trata de animales vivos, refrigerados, congelados o transformados.
            «Fábrica»: incluye buques factoría, buques congeladores, buques frigoríficos, almacenes frigoríficos y plantas de transformación.   
        </p>

    </td>
</tr>
</table>
<table class="" style="width:100%" border=1>
    <tr>
        <td class="text-medio" colspan="30">Forma GTVC 19-1/L214/11</td>
        <td class="text-medio" colspan="40">5 DE 6</td>
        <td class="text-medio" colspan="30"></td>
    </tr>
    <tr>
        <td class="text-medio" colspan="100">http://registro-empresa.vuce.gob.ve/exportador/certZooEu/{{$id}}</td>
    </tr>
</table>
<div style="page-break-after:always;"></div>
<!--Sexta pagina-->
<table style="width:100%"  border=1>
    <tr>
        <td class="text-medio" colspan="100">
            <b>PAIS VENEZUELA</b>
        </td>
        <td class="text-medio der" colspan="200">
            <b>Productos de la pesca</b>
        </td>
    </tr>
    <tr>
        <td class="text-medio" colspan="100">II. Información sanitaria</td>
        <td class="text-medio" colspan="100">II.a. Referencia del certificado:</td>
        <td class="text-medio" colspan="100">II.b. Referencia SGICO</td>
    </tr>
    <tr>
        <td class="text-medio" colspan="300">

            <p class="text-medio"><b>Parte II:</b><br>(1) La parte II.1 del presente certificado no es aplicable a los países con requisitos especiales de certificación sanitaria establecidos en acuerdos de
            equivalencia u otros actos legislativos de la UE.</p>
            <p>(2)La parte II.2 no se aplica, y además suprimirse, cuando la partida e componga de: a) especies destinadas de las enumeradas en el anexo del
                Reglamento de Ejecución (UE) 2018/1882 de la Comision11; b) animales acuáticos silvestres y producto de origen animal procedentes de esos animales
                acuáticos, que se desembarquen de buques pesqueros para el consumo humano; o c) producto del origen animal que procedan de animales que no sean
            animales acuáticos vivos y que entren en la unión listos para el consumo humano directo</p>
            <p>(3) Especies enumeradas en las columnas 3 y 4 del cuadro del anexo del Reglamento de Ejecución (UE) 2018/1882. Las especies enumeradas en la
            columna 4 solo se consideraran vectores en las condicione expuestas en el artículo 171 del Reglamento Delegado (UE) 2020/692.</p>
            <p>(4) Tácheselo que no proceda / Suprímase si no es aplicable.
                (5) Código de tercer país /territorio /zona /compartimiento tal como figura en una lista de terceros países y territorio adoptada por la Comisión de
                conformidad con el artículo 230, apartado1, del Reglamento (UE) 2016/429.
                (6) Las partes II.2.3.1, II.2.3.2 y II.2.4 del presente certificado no se aplican, y deberían suprimirse, si la partida solo contiene los crustáceos o peces
                siguientes:
                a) Crustáceos que estén envasados y etiquetados para el consumo humano de conformidad con los requisitos específicos del Reglamento (CE) n.
                °853/2004 que les sean aplicables, y que ya no puedan sobrevivir como animales vivos si se devuelven al medio acuático;
                b) Crustáceos que estén destinados al consumo humano sin transformación ulterior , siempre que estén envasados para su venta al por menor de
                conformidad con los requisitos del Reglamento (CE) n.°853/2004 aplicables a los envases de que se trate;
                c) Crustáceos que estén envasados y etiquetados para el consumo humano de conformidad con los requisitos específicos del Reglamento (CE) n.°
                853/2004 que les sean aplicables y que estén destinados a una transformación ulterior sin almacenamiento temporal en un lugar de transformación;
            d) Peces sacrificados y eviscerados antes de la expedición.</p>
            <p>(7) Aplicable cuando el Estado miembro de la Unión, o bien tenga el estatus de libre de una enfermedad de la categoría C según se define el artículo 1,
                punto3, del Reglamento de Ejecución (UE) 2018/1882, o bien este sometido a un programa de erradicación voluntaria establecido de conformidad con
            el artículo 31, apartado 2, del Reglamento (UE) 2016-7429; de lo contrario, suprímase.</p>
            <p>(8)Aplicable cuando el Estado miembro de destino en la Unión ha adoptado medidas nacionales con respecto a una enfermedad especifica que han sido
            aprobadas por la Comisión con arreglo al artículo 226 del Reglamento (UE) 2018/429: de lo contrario, suprímase</p>
            <p>(9) Especies enumeradas en la columna 2 del cuadro del anexo XXIX del Reglamento Delegado (UE) 2020/692, sobre las enfermedades para las que los
            Estados miembros aplican medidas nacionales de conformidad con el artículo 226 del Reglamento (UE) 2016/429.</p>
            <p>(10) Debe ir firmado por : <br>
                - un veterinario oficial, cuando no se suprima la parte II.2 «Declaración zoosanitaria» «» <br>
            - un agente certificador o un veterinario oficial, cuando se suprima la parte II.2 «Declaración zoosanitaria» del certificado.</p>
        </td>
    </tr>
    <tr>
        <td class="text-medio" colspan="300"><b>[Veterinario oficial] (4)(10) / [Agente certificador] (4)(10)</b></td>
    </tr>
    <tr>
        <td class="text-medio" colspan="100">Nombre (en mayúscula)</td>
        <td class="text-medio" colspan="50"></td>
        <td class="text-medio" colspan="100">Cualificación y cargo:</td>
        <td class="text-medio" colspan="50"></td>
    </tr>
    <tr>
        <td class="text-medio" colspan="100">Fecha:</td>
        <td class="text-medio" colspan="50"></td>
        <td class="text-medio" colspan="100">Firma:</td>
        <td class="text-medio" colspan="50"></td>
    </tr>
    <tr>
        <td class="text-medio" colspan="100">Sello:</td>
        <td class="text-medio" colspan="50"></td>
        <td class="text-medio" colspan="100"></td>
        <td class="text-medio" colspan="50"></td>
    </tr>
</table>
<table class="" style="width:100%" border=1>
    <tr>
        <td class="text-medio" colspan="30">Forma GTVC 19-1 /L214/11</td>
        <td class="text-medio" colspan="40">6 DE 6</td>
        <td class="text-medio" colspan="30"></td>
    </tr>
    <tr>
        <td class="text-medio" colspan="100">http://registro-empresa.vuce.gob.ve/exportador/certZooEu/{{$id}}</td>
    </tr>
</table>
    </div>
</body>
</html>