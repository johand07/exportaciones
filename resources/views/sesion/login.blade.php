@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2"><br><br>
            <div class="panel panel-primary">
                <div class="panel-heading"><span><i class="glyphicon glyphicon-user"></i></span> <bold>Login</bold></div>

                <div class="panel-body">

                        {{ Form::open(['action'=>'SesionController@storeLogin','method'=>'POST'])}}
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">Correo Electrónico</label>

                            <div class="col-md-6">
                                <div class="input-group">
                                    {!!Form::email('email',null,['class'=>'form-control','id'=>'email','placeholder'=>'ejemplo@ejemplo.com','required'=>'required','autofocus'=>'true'])!!}
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
                                </div>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <div class="input-group">

                                  
                                  {!!Form::password('password',['class'=>'form-control','id'=>'password','placeholder'=>'Contraseña','required'=>'required'])!!}

                                    <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                                </div>
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('captcha') ? ' has-error' : '' }}">
                            <label for="captcha" class="col-md-4 control-label">Captcha</label>

                            <div class="col-md-6">
                                <div class="captcha">
                                    <span>{!! captcha_img('flat')!!}</span>
                                    <button type="button" class="btn btn-primary btn-refresh">Cambiar</button>
                                </div>
                                <br>
                                <div class="input-group">
                                    <input id="captcha" type="text" class="form-control" name="captcha" placeholder="Ingrese Caracateres de la Imagen" required>
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
                                </div>

                               <!--<div class="g-recaptcha" data-sitekey="6LfdaFcUAAAAAIk5MtzzcVAQiUoYL7pEC7JByecA"></div>-->

                                @if ($errors->has('captcha'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('captcha') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <div class="checkbox">
                                   <label>{!!Form::checkbox('remember',null)!!}Recuérdame</label><br>
                                </div>
                                <div class="text-center">
                                    <a href=" {{ url('/reestablecer-password') }} ">¿Olvidaste tu contraseña?</a>
                                </div><br>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary btn-lg btn-block">
                                    Enviar
                                </button>

                                <!--<a class="btn btn-link" href="{{ url('/passwordReset') }}">
                                    Olvidaste tu contraseña?
                                </a>-->
                            </div>
                        </div>
                   {{Form::close()}}
                </div>
            </div>
        </div>
    </div>
</div>
@include('templates/footer')

@endsection
