<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Planilla5 extends Model
{
    protected $table = 'planilla_5';
    protected $fillable = [

           'det_declaracion_produc_id',
           'proceso_produccion',
           'descrip_observacion',
           'bactivo',
          
    ];
   
   		public function declaracionProduc() {

   		return $this->hasOne('App\Models\DetDeclaracionProduc','id','det_declaracion_produc_id');

    }
}
