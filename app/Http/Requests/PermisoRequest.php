<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PermisoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'tipo_permiso_id'=>'required',
            'numero_permiso'=>'required',
            'fecha_permiso'=>'required',
            'fecha_aprobacion'=>'required',
            'gen_aduana_salida_id'=>'required',
            'pais_id'=>'required',

        ];
    }
}
