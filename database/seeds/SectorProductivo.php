<?php

use Illuminate\Database\Seeder;
use App\Models\CatSectorProductivoEco;

class SectorProductivo extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sectorProductivo = [
            [
                'id'         => 1,
                'nombre_sector'       => 'Agricultura, Ganadería, Pesca y Productos Forestales',
                'created_at' => '2021-07-20 20:59:08',
                'updated_at' => '2021-07-20 20:59:08',
            ],
            [
                'id'         => 2,
                'nombre_sector'       => 'Comercio al Mayor y Detal',
                'created_at' => '2021-07-20 20:59:08',
                'updated_at' => '2021-07-20 20:59:08',
            ],
            [
                'id'         => 3,
                'nombre_sector'       => 'Construcción',
                'created_at' => '2021-07-20 20:59:08',
                'updated_at' => '2021-07-20 20:59:08',
            ],
            [
                'id'         => 4,
                'nombre_sector'       => 'Energía Eléctrica',
                'created_at' => '2021-07-20 20:59:08',
                'updated_at' => '2021-07-20 20:59:08',
            ],
            [
                'id'         => 5,
                'nombre_sector'       => 'Industrias Manufactureras',
                'created_at' => '2021-07-20 20:59:08',
                'updated_at' => '2021-07-20 20:59:08',
            ],
            [
                'id'         => 6,
                'nombre_sector'       => 'Hidrocarburos',
                'created_at' => '2021-07-20 20:59:08',
                'updated_at' => '2021-07-20 20:59:08',
            ],
            [
                'id'         => 7,
                'nombre_sector'       => 'Minería',
                'created_at' => '2021-07-20 20:59:08',
                'updated_at' => '2021-07-20 20:59:08',
            ],
            [
                'id'         => 8,
                'nombre_sector'       => 'Transporte Almacenamiento y Comunicaciones',
                'created_at' => '2021-07-20 20:59:08',
                'updated_at' => '2021-07-20 20:59:08',
            ],
            [
                'id'         => 9,
                'nombre_sector'       => 'Manejo de Desechos',
                'created_at' => '2021-07-20 20:59:08',
                'updated_at' => '2021-07-20 20:59:08',
            ],
            [
                'id'         => 10,
                'nombre_sector'       => 'Servicios de Salud y Asistencia Social',
                'created_at' => '2021-07-20 20:59:08',
                'updated_at' => '2021-07-20 20:59:08',
            ],
            [
                'id'         => 11,
                'nombre_sector'       => 'Servicios Turísticos',
                'created_at' => '2021-07-20 20:59:08',
                'updated_at' => '2021-07-20 20:59:08',
            ],
            [
                'id'         => 12,
                'nombre_sector'       => 'Servicios Financieros y de Seguros',
                'created_at' => '2021-07-20 20:59:08',
                'updated_at' => '2021-07-20 20:59:08',
            ],
            [
                'id'         => 13,
                'nombre_sector'       => 'Servicios Educativos',
                'created_at' => '2021-07-20 20:59:08',
                'updated_at' => '2021-07-20 20:59:08',
            ],
            [
                'id'         => 14,
                'nombre_sector'       => 'Servicios de Esparcimiento Cultural y Deportivo',
                'created_at' => '2021-07-20 20:59:08',
                'updated_at' => '2021-07-20 20:59:08',
            ],
            [
                'id'         => 15,
                'nombre_sector'       => 'Servicios Inmobiliarios y Alquiler de Bienes Inmuebles',
                'created_at' => '2021-07-20 20:59:08',
                'updated_at' => '2021-07-20 20:59:08',
            ],
            [
                'id'         => 16,
                'nombre_sector'       => 'Servicios Profesionales, Científicos',
                'created_at' => '2021-07-20 20:59:08',
                'updated_at' => '2021-07-20 20:59:08',
            ],
            
            
        ];

        CatSectorProductivoEco::insert($sectorProductivo);
    }
}
