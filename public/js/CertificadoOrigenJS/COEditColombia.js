function cambiarImagen(event) {
    var id= event.target.getAttribute('name');
    var reader = new FileReader();
    reader.onload = function(){
      var output = document.getElementById('vista_previa_'+id);
     output.src = reader.result;
    };
    let archivo = $(".file-input").val();
    if (!archivo) {
      archivo=$('#'+id).val();
    }
    var extensiones = archivo.substring(archivo.lastIndexOf(".")).toLowerCase();
    
    //var imgfile="/exportaciones/public/img/file.png";

//alert(imgfile);
    if(extensiones != ".doc" && extensiones != ".docx" && extensiones != ".pdf" && extensiones != ".ppt" && extensiones != ".pptx"){
      swal("Formato invalido", "Formato de archivo invalido debe ser .doc, .docx, .ppt, .pptx o pdf", "warning");
        
        
    }else{
      $('#imgdocumento_'+id).show();
      $('#imgdocumentoEdit_'+id).hide();
       let file=$('#'+id).val();
     // alert(file);
     //Y pintame ruta y nombre del archivo acargado con nombre_
      $('#nombre_'+id).html('<h4>'+file+'</h4>');

      reader.readAsDataURL(event.target.files[0]);
    }

  }

   $(document).on('change', '.file-input', function(evt) {
           cambiarImagen(evt);
    });



    /*************************************************************************/
   function cambiarImagenDoc(event) {

    
    //console.log(event);

    var id= event.target.getAttribute('id');
    //alert(id);
    
  
      archivo=$('#'+id).val();
    //}
    var extensiones = archivo.substring(archivo.lastIndexOf(".")).toLowerCase();
    
    //var imgfile="/exportaciones/public/img/file.png";

    
    if(extensiones != ".doc" && extensiones != ".docx" && extensiones != ".pdf" && extensiones != ".ppt" && extensiones != ".pptx"){
      swal("Formato invalido", "Formato de archivo invalido debe ser .doc, .docx, .ppt, .pptx o pdf", "warning"); 
        $('#'+id).val('');
        
    }else{
      $('#imgdocumento_'+id).show();
      let file=$('#'+id).val();
     // alert(file);
      $('#nombre_'+id).html('<strong>'+file+'</strong>');
     
      reader.readAsDataURL(event.target.files[0]);

    }

  }

    $(document).on('change', '.file-input', function(evt) {
      
      let size=this.files[0].size;
      if (size>8500000) {

         swal("Tamaño de archivo invalido", "El tamaño del archivo No puede ser mayor a 8.5 megabytes","warning"); 
          this.value='';
         //this.files[0].value='';
      }else{
        cambiarImagen(evt);
      }
      
           
    });
/////////////////Par lista dinamica////////////////
  $(document).on('change', '.file_multiple', function(evt) {
           

          let size=this.files[0].size;
      if (size>2500000) {

         swal("Tamaño de archivo invalido", "El tamaño del archivo No puede ser mayor a 8.5 megabytes","warning"); 
         this.value='';
      }else{
        cambiarImagenDoc(evt);
      }

    });



/********************************************************************** */

$(document).ready(function (){

	valorSelect(1);	
	var x = $('input[name="contador"]').val();
	var j=parseInt(x);
	
	$('#add-productos').click(function(){
		j++;

	 		$('#productos tr:last').after('<tr id="row1'+j+'"display="block" class="show_div"><td>{!!Form::text("num_orden[]",null,["class"=>"form-control","id"=>"num_orden", "noreq"=>"noreq"])!!}</td><td>{!!Form::text("codigo_arancel[]",null,["class"=>"form-control","id"=>"codigo_arancel", "noreq"=>"noreq"])!!}</td><td>{!!Form::text("denominacion_mercancia[]",null,["class"=>"form-control","id"=>"denominacion_mercancia", "noreq"=>"noreq"])!!}</td><td>{!!Form::text("unidad_fisica[]",null,["class"=>"form-control","id"=>"unidad_fisica", "noreq"=>"noreq"])!!}</td><td>{!!Form::text("valor_fob[]",null,["class"=>"form-control","id"=>"valor_fob","onkeypress"=>"return soloNumeros(event)", "noreq"=>"noreq"])!!}</td><td>{!!Form::text("cantidad_segun_factura[]",null,["class"=>"form-control","id"=>"cantidad_segun_factura","onkeypress"=>"return soloNumeros(event)", "noreq"=>"noreq"])!!}</td><td>{!!Form::hidden("id_productos[]",0)!!}<button  name="remove" type="button" id="'+j+'" value="" class="btn btn-danger btn-remove-productos">x</button></td></tr>');


	});

	$(document).on('click','.btn-remove',function(){
        var id_boton= $(this).attr("id");
        var id=$(this).prev().val();
		
		if(id!=0){

			if (confirm('Deseas eliminar este Registro?')) {

				$.ajax({

				'type':'get',
				'url': 'eliminarRegistroProd',
				'data':{ 'id':id},
				success: function(data){

					$('#row'+id_boton).remove();

					}

				});

			}



		}else{

		$('#row'+id_boton).remove();
		}

	});

	/*Aqui va lista dinamica de Criterios*/

	var x = $('input[name="contador_criterio"]').val();
	var i=parseInt(x);
	$('#add-criterios').click(function(){
	i++;

	 	$('#criterios tr:last').after('<tr id="row'+i+'"display="block" class="show_div"><td>{!!Form::text("num_orden_declaracion[]",null,["class"=>"form-control","id"=>"num_orden_declaracion", "noreq"=>"noreq"])!!}</td><td>{!!Form::text("normas_criterio[]",null,["class"=>"form-control","id"=>"normas_criterio", "noreq"=>"noreq"])!!}</td><td>{!!Form::hidden("id_criterios[]",0)!!}<button name="remove"type="button" id="'+i+'" value=""class="btn btn-danger btn-remove-criterio">x</button></td></tr>');

	});

    $(document).on('click','.btn-remove-criterio',function(){
		var id_boton= $(this).attr("id");
        var id=$(this).prev().val();
		
		if(id!=0){

			if (confirm('Deseas eliminar este Registro?')) {

				$.ajax({

				'type':'get',
				'url': 'eliminarRegistroCriterio',
				'data':{ 'id':id},
				success: function(data){

					$('#row'+id_boton).remove();

					}

				});

			}



		}else{

		$('#row'+id_boton).remove();
		}

	});

	
    //Validación de formulario, se puede llamar y pasar la sección que contenga los inputs a validar
		
	var error = '<div class="msg_alert"><span style="color:red" class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span><strong id="cod_aran-error" class="added"> El campo es obligatorio.</strong></div>';
	var valido = '<div class="msg_alert"><span style="color:green" class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span></div>';
	var mens=['Estimado Usuario. Debe completar los campos solicitados', 'Estimado Usuario. Debe realizar los cambios solicitados en la observación']
	var err_edit = 1;

	validacionForm = function() {

	$('#FormProd').submit(function(event) {
	
	var campos = $('#FormProd').find('input:text, select, textarea');
	var n = campos.length;
	var err = 0;

	$("div").remove(".msg_alert");
	//bucle que recorre todos los elementos del formulario
	for (var i = 0; i < n; i++) {
			var cod_input = $('#FormProd').find('input:text, select, textarea').eq(i);
			if (!cod_input.attr('noreq')) {
				if (cod_input.val() == '' || cod_input.val() == null)
				{
					err = 1;
					cod_input.css('border', '1px solid red').after(error);
				}
				else{
					if (err == 1) {err = 1;}else{err = 0;}
					cod_input.css('border', '1px solid green').after(valido);
				}
				
			}
	}

	//Si hay errores se detendrá el submit del formulario y devolverá una alerta
	if(err==1){
			event.preventDefault();
			swal("Por Favor!", mens[0], "warning")
		}

  	if(err_edit==1){
			event.preventDefault();
			swal("Por Favor!", mens[1], "warning")
		}

		});
}

//Función para detectar si existe algún cambio dentro del formulario
$("input:text, select, input:file, textarea").change(function(){
err_edit = 0;
});



});/*llave del document).ready*/

function valorSelect(id) { //jquery o javaScrip

/**let gen_tipo_certificado=$('gen_tipo_certificado_id').val();  con jquery**/ 

	let gen_tipo_certificado = document.getElementById('gen_tipo_certificado_id').value;



//alert(gen_tipo_certificado);

if (gen_tipo_certificado==1) {//Caso Colombia

	document.getElementById('pais_exp_imp').style.display = 'block';//mostrar
	document.getElementById('productos_planilla').style.display = 'block';
	document.getElementById('declaracion_origen').style.display = 'block';
	document.getElementById('criterios_normas').style.display = 'block';
	document.getElementById('razon_social_exp').style.display = 'block';
	document.getElementById('identificacion_fiscal').style.display = 'block';
	document.getElementById('direccion_exp').style.display = 'block';
	document.getElementById('paisciudad_email_telf_exp').style.display = 'block';
	document.getElementById('fecha_exp').style.display = 'block';
	document.getElementById('nombre_direcc_impo').style.display = 'block';
	document.getElementById('pais_email_telef_impor').style.display = 'block';
	document.getElementById('transporte_puerto_impor').style.display = 'block';
	document.getElementById('observaciones').style.display = 'block';

	document.getElementById('datos_productor').style.display = 'none';//ocultar
	document.getElementById('rif_imp').style.display = 'none';
	
	
	
	

}else if (gen_tipo_certificado==2) {//Caso Bolivia
	document.getElementById('pais_exp_imp').style.display = 'block';//mostrar
	 	document.getElementById('declaracion_origen').style.display = 'block';
	 	document.getElementById('criterios_normas').style.display = 'block';
	 	document.getElementById('razon_social_exp').style.display = 'block';
	 	document.getElementById('direccion_exp').style.display = 'block';
	 	document.getElementById('fecha_exp').style.display = 'block';
	 	document.getElementById('nombre_direcc_impo').style.display = 'block';
	 	document.getElementById('transporte_puerto_impor').style.display = 'block';
	 	
	 	document.getElementById('observaciones').style.display = 'block';
	 	
	 
	 	document.getElementById('identificacion_fiscal').style.display = 'none';
	 	document.getElementById('paisciudad_email_telf_exp').style.display = 'none';
	 	document.getElementById('pais_email_telef_impor').style.display = 'none';
	 	document.getElementById('datos_productor').style.display = 'none';
	 	document.getElementById('productos_planilla').style.display = 'none';
	 	document.getElementById('rif_imp').style.display = 'none';


	

}else{//Caso Chile
	document.getElementById('pais_exp_imp').style.display = 'block';//mostrar
	document.getElementById('datos_productor').style.display = 'block';
	document.getElementById('razon_social_exp').style.display = 'block';
	document.getElementById('direccion_exp').style.display = 'block';
	document.getElementById('identificacion_fiscal').style.display = 'block';
	document.getElementById('nombre_direcc_impo').style.display = 'block';
	document.getElementById('rif_imp').style.display = 'block';
	document.getElementById('declaracion_origen').style.display = 'block';
	document.getElementById('productos_planilla_2_3').style.display = 'block';
	document.getElementById('criterios_normas').style.display = 'block';
	document.getElementById('observaciones').style.display = 'block';

	document.getElementById('paisciudad_email_telf_exp').style.display = 'none';//ocultar
	document.getElementById('fecha_exp').style.display = 'none';
	document.getElementById('productos_planilla').style.display = 'none';
	document.getElementById('pais_email_telef_impor').style.display = 'none';



}


}