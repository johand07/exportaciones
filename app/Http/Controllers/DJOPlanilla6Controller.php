<?php

namespace App\Http\Controllers;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Http\Request;
use App\Models\DetDeclaracionProduc;
use App\Models\Planilla6;
use App\Models\Productos;
use Auth;
use Alert;
use Session;
use DB;
use View;
use Response;

class DJOPlanilla6Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return $this->create($request);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $productos_id= $request['producto_id'];
        $gen_declaracion_jo_id = Session::get('gen_declaracion_jo_id');

        // validamos que exista la declaracion 
        if(empty($gen_declaracion_jo_id)){

          Alert::warning('La solicitud que intenta realizar requiere que este asociada a una Declaracion Jurada de Origen. Seleccione o creela.','Acción Denegada1!')->persistent("OK");

          return redirect()->action('ListaDeclaracionJOController@index'); 
        }

         $det_declaracion_product=DetDeclaracionProduc::with('rProducto')->with('rPlanilla6')->where('gen_declaracion_jo_id',$gen_declaracion_jo_id)->where('id',$productos_id)->get()->last();

        // validamos que existe el producto asociado a la declaracion
        if(empty($det_declaracion_product->id)){

          Alert::warning('La solicitud que intenta realizar requiere que el producto este asociado a la Declaracion Jurada de Origen. Intente nuevamente.','Acción Denegada!')->persistent("OK");

          return redirect()->action('DeclaracionJOController@index');

        }else{
        
            Session::put('det_declaracion_product_id',$det_declaracion_product->id);
        
        }

        // Verificamos si existe datos en la planilla asociada al producto 
        if(!empty($det_declaracion_product->rPlanilla6->id)){

           return redirect()->action('DJOPlanilla6Controller@edit', ['id' => $det_declaracion_product->rPlanilla6->id]);
        }

        
        $titulo= 'Declaración Jurada de Origen';
        $descripcion= 'Planilla 6';
        $producto = $det_declaracion_product->rProducto;

        return view('djoplanilla6.create',compact('producto','titulo','descripcion'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {



        /*******************   Validar reglas de las imagenes   ********************/
         $this->validate($request, [

                'foto_1' => 'required|image|mimes:jpeg,png,jpg,gif|max:9500',
                //'foto_2' => 'required|image|mimes:jpeg,png,jpg,gif|max:250',
                //'foto_3' => 'required|image|mimes:jpeg,png,jpg,gif|max:250',

            ]);
         //dd($request->file('foto_2'));
         /*****************   Validar que se tenga la session de la declaracion asociada al producto  ****************/

            $det_declaracion_product_id = Session::get('det_declaracion_product_id');
            $gen_declaracion_jo_id = Session::get('gen_declaracion_jo_id');
            
            if(empty($det_declaracion_product_id)){

                Alert::warning('No es posible realizar su solicitud. Intente nuevamente.','Acción Denegada!')->persistent("OK");

                return redirect()->back()->withInput();
            }


         /*************************** Validar que no hayan registros previas en planilla6 asociados al producto declarado *****/

            $planilla6 = Planilla6::with('declaracionProduc')->where('det_declaracion_produc_id',$det_declaracion_product_id)->get()->last();

            if(!empty($planilla6)){
                 Alert::error('Usted ya registro datos para la planilla 6 asociada al producto " '.$planilla6->declaracionProduc->rProducto->descrip_comercial.' ". Intente nuevamente.','Acción Denegada!')->persistent("OK");

                return redirect()->action('DeclaracionJOController@index');
            }


        /*************************** Guardar las imagenes  ******************************/
    
        $destino = '/img/declaracionjurada/'.Auth::user()->detUsuario->cod_empresa;
        $destinoPrivado = public_path($destino);
        
        if (!file_exists($destinoPrivado)) {
            $filesystem = new Filesystem();
            $filesystem->makeDirectory($destinoPrivado, $mode = 0777, true, true); 
        }


        for( $i=1; $i <=3 ; $i++ ){

            $imagen = $request->file('foto_'.$i);
            if (!empty($imagen)) {
               $nombre_imagen = str_replace('/tmp','',$imagen->getPathName()).time().'.'.$imagen->getClientOriginalExtension();
                $imagen->move($destinoPrivado, $nombre_imagen);
                $auxStr='foto_'.$i;
                $$auxStr = $nombre_imagen;
            }
            

        }


            $planilla6 = new Planilla6;
            $planilla6->det_declaracion_produc_id = $det_declaracion_product_id;
            $planilla6->imagen1 = $destino.'/'.$foto_1;
            if (!empty($foto_2)) {
                $planilla6->imagen2 = $destino.'/'.$foto_2;
            }
            if (!empty($foto_3)) {
                $planilla6->imagen3 = $destino.'/'.$foto_3;
            }
            
            
            $planilla6->bactivo=1;

                DB::beginTransaction();

            if($planilla6->save())
            {
                /*****  Cambiando el estado de la planilla 6 en det declaracion product ******/
                $detDeclaracionProducModel = $planilla6->declaracionProduc;
                $detDeclaracionProducModel->estado_p6=1;
                $detDeclaracionProducModel->save();

                DB::commit();

                Alert::success('Se han guardado los datos correspondientes a la Planilla 6!','Registrado Correctamente!')->persistent("Ok");

                return redirect()->action('DeclaracionJOController@index');
            }else{

                DB::rollback();

                Alert::warning('No se logro guardar la información. Intente nuevamente.','Acción Denegada!')->persistent("OK");

                return redirect()->back()->withInput();
            }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $layout= $request->layout==="false"? false: true;

        if(!empty($request->det_declaracion_id))
        {
            
            $det_declaracion= DetDeclaracionProduc::with('rPlanilla6')->find($request->det_declaracion_id);
        
            if(empty($det_declaracion)){
                Alert::error('La planilla que intenta visualizar no esta registrada en el sistema. D1','Acción Denegada!')->persistent("OK");
                return $layout? redirect()->action('DeclaracionJOController@index') : '<div class="alert alert-danger">La planilla que intenta visualizar no esta registrada en el sistema. D1</div>';
            }

            $planilla6 = $det_declaracion->rPlanilla6;


        }else{
            $id= $request->id ;
            $planilla6 = Planilla4::with('declaracionProduc')->find($id);
        }

        /***** Validar que haya un registro de la planilla ****/
        if(empty($planilla6)){

             Alert::error('La planilla que intenta visualizar no esta registrada en el sistema.','Acción Denegada!')->persistent("OK");

            return $layout? redirect()->action('DeclaracionJOController@index') : '<div class="alert alert-danger">La planilla que intenta visualizar no esta registrada en el sistema.</div>' ;
        }

        /***** Validar que la planilla sea de un usuario en especifico ****/
        if($planilla6->declaracionProduc->declaracionJO->gen_usuario_id!=Auth::user()->id && Auth::user()->gen_tipo_usuario_id!=5 && Auth::user()->gen_tipo_usuario_id!=6 && Auth::user()->gen_tipo_usuario_id!=1 && Auth::user()->gen_tipo_usuario_id!=8){

            Alert::error('No tiene permisos para visualizar esta planilla.','Acción Denegada!')->persistent("OK");

           return $layout? redirect()->action('DeclaracionJOController@index') :'<div class="alert alert-danger">No tiene permisos para visualizar esta planilla.</div>' ;
       }

       $producto = $planilla6->declaracionProduc->rProducto;

        if($layout)
            return view('djoplanilla6.show',compact('layout','planilla6','producto'));
        else
            return View::make('djoplanilla6.show',compact('layout','planilla6','producto'))->renderSections()['content'];
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // get the planilla4
        $planilla6 = Planilla6::find($id);

        $producto = $planilla6->declaracionProduc->rProducto;
        
        // show the edit form and pass the planilla4
        return View::make('djoplanilla6.edit',compact('planilla6','producto'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    { 
       /*******************   Validar reglas de las imagenes   ********************/
       $this->validate($request, [

        //'foto_1' => 'required|image|mimes:jpeg,png,jpg,gif|max:250',
        //'foto_2' => 'required|image|mimes:jpeg,png,jpg,gif|max:250',
        //'foto_3' => 'required|image|mimes:jpeg,png,jpg,gif|max:250',

    ]);




/*************************** Guardar las imagenes  ******************************/

$destino = '/img/declaracionjurada/'.Auth::user()->detUsuario->cod_empresa;
$destinoPrivado = public_path($destino);

if (!file_exists($destinoPrivado)) {
    $filesystem = new Filesystem();
$filesystem->makeDirectory($destinoPrivado, $mode = 0777, true, true); 
}

for( $i=1; $i <=3 ; $i++ ){

    $imagen = $request->file('foto_'.$i);
    if (!empty($imagen)) {
        $nombre_imagen = str_replace('/tmp','',$imagen->getPathName()).time().'.'.$imagen->getClientOriginalExtension();
        $imagen->move($destinoPrivado, $nombre_imagen);
        $auxStr='foto_'.$i;
        $$auxStr = $nombre_imagen;
    }

}



    $planilla6 = Planilla6::find($id);

    
    if(!empty($foto_1)){
        $auxFoto1= $planilla6->imagen1;
        $planilla6->imagen1 = $destino.'/'.$foto_1;
    }
    if(!empty($foto_2)){
        $auxFoto2= $planilla6->imagen2;
        $planilla6->imagen2 = $destino.'/'.$foto_2;
    }
    if(!empty($foto_3)){
        $auxFoto3= $planilla6->imagen3;
        $planilla6->imagen3 = $destino.'/'.$foto_3;
    }

    $planilla6->bactivo=1;
    $planilla6->estado_observacion=0;


    if($planilla6->touch()){
        if(!empty($auxFoto1)){
            if(is_file('.'.$auxFoto1))
            unlink('.'.$auxFoto1); //elimino el fichero
        }
        if(!empty($auxFoto2)){
            if(is_file('.'.$auxFoto2))
            unlink('.'.$auxFoto2); //elimino el fichero
        }
        if(!empty($auxFoto3)){
            if(is_file('.'.$auxFoto3))
            unlink('.'.$auxFoto3); //elimino el fichero
        }

    }else{
        if (!empty($foto_1)) {
            if(is_file('.'.$destino.'/'.$foto_1))
            unlink('.'.$destino.'/'.$foto_1);
        }
        
        if (!empty($foto_2)) {
            if(is_file('.'.$destino.'/'.$foto_2))
            unlink('.'.$destino.'/'.$foto_2);
        }
        
        if (!empty($foto_3)) {
            if(is_file('.'.$destino.'/'.$foto_3))
            unlink('.'.$destino.'/'.$foto_3);
        }
        

        Alert::warning('No es posible realizar su solicitud. Intente nuevamente.','Acción Denegada!')->persistent("OK");
        return redirect()->back()->withInput();
    }
    /*****  Cambiando el estado de la planilla 6 en det declaracion product ******/
    $detDeclaracionProducModel = $planilla6->declaracionProduc;
    $detDeclaracionProducModel->estado_p6=1;
    $detDeclaracionProducModel->touch();
// redirect
Alert::success('Se han actualizado los datos correspondientes a la Planilla 6!','Actualizado Correctamente!')->persistent("Ok");

return redirect()->action('DeclaracionJOController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
    }

          /** Para consultar las observaciones mediante ajax **/
    /**
     * Se recibe como parametro el $id de la planilla
     * 
     */
    public function obtenerObservacion(Request $request,$id=null){

        if(!empty($request->det_declaracion_id)){
            $detDeclaracion= DetDeclaracionProduc::with('rPlanilla6')->find($request->det_declaracion_id);
            $planilla6= $detDeclaracion->rPlanilla6;
        }else{
        $planilla6 = Planilla6::find($id);
        }
        return Response::json(array('observacion'=>$planilla6->descrip_observacion));
        
    }

      /** Para guardar las observaciones mediante ajax **/
    /**
     * Se recibe como parametro el $id de la planilla
     * 
     */
    public function storeObservacion(Request $request,$id=null){

        
        if(!empty($request->det_declaracion_id)){
            $detDeclaracion= DetDeclaracionProduc::with('rPlanilla6')->find($request->det_declaracion_id);
            $planilla6= $detDeclaracion->rPlanilla6;
        }else{
            $planilla6 = Planilla6::find($id);
        }
        if(!empty($request->observacion)){
            $planilla6->descrip_observacion=$request->observacion;
            $planilla6->estado_observacion=1;
        }else{
            $planilla6->descrip_observacion=null;
            $planilla6->estado_observacion=0;
        }

        if($planilla6->touch())
            return Response::json(array('respuesta'=>'Solicitud procesada con exito','estado'=>1));
        else
            return Response::json(array('respuesta'=>'La solicitud no pudo ser procesada','estado'=>0));
        
    }
}
