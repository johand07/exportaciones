<?php

namespace App\Http\Controllers;

use App\Models\GenUsuario;
use App\Models\CatPreguntaSeg;
use App\Models\GenTipoUsuario;
use App\Models\DetUsuario;
use App\Models\Accionistas;
use App\Models\Productos;
use App\Models\GenOperadorCamb;
use App\Models\GenStatus;
use App\Models\GenUsuarioOca;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Laracasts\Flash\Flash;
use App\Http\Requests\GenUsuarioRequest;
use App\Http\Requests\CamPreSegRequest;

use Alert;
use Auth;
//use Validator;


class GenUsuarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function index()
    {
      $titulo= 'Usuarios Exportadores';
      $descripcion= 'Gestión de Usuarios';
      
      $users=DetUsuario::where('bactivo',1)->get();

      
 
      return view('AdminUsuario.index',compact('users','titulo','descripcion'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $titulo= 'Creación de Usuarios';
        $descripcion= 'Usuarios del sistema';
        
        $preguntas=CatPreguntaSeg::all()->pluck('descripcion_preg','id');
        $tipousu=GenTipoUsuario::whereIn('id',[2,3,5,6,7,8,9,10,18,19])->pluck('nombre_tipo_usu','id');
        $statususu=GenStatus::whereIn('id', [1, 2])->pluck('nombre_status','id');
         //preguntamos por los usuarios banco de produccion para que les muestre sus oca de prueba sino para los usuarios normales muestra los oca reales
       if (Auth::user()->id == 4 || Auth::user()->id == 1) {
         $oca=GenOperadorCamb::whereNotIn('id',[31])->pluck('nombre_oca','id');
       } else {
         $oca=GenOperadorCamb::whereNotIn('id',[31,32,33])->pluck('nombre_oca','id');
       }
       // dd($oca);
        //dd($statususu);
        return view('AdminUsuario.create',compact('preguntas','tipousu','statususu', 'titulo', 'descripcion','oca'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(GenUsuarioRequest $request)
    {

      if (isset($request->cat_preguntas_seg_id)) {
        if (isset($request->pregunta_alter)) {

           $pregunta=$request['pregunta_alter'];
        }else{

           $pregunta='N/A';
        }
      }
      $usu= new GenUsuario;
      $usu->email=$request->email;
      $usu->email_seg=$request->email_seg;
      $usu->gen_tipo_usuario_id=$request->gen_tipo_usuario_id;
      $usu->gen_status_id=$request->gen_status_id;
      $usu->password=bcrypt($request->password);
      $usu->cat_preguntas_seg_id=$request->cat_preguntas_seg_id;
      $usu->pregunta_alter=$pregunta;
      $usu->respuesta_seg=$request->respuesta_seg;
      $usu->remember_token=str_random(10);
      $usu->save();
     // dd($request->gen_operador_cambiario_id);
      if (isset($request->gen_operador_cambiario_id)) {
         $ult_usuario=GenUsuario::where('gen_status_id',1)->where('gen_tipo_usuario_id',3)->orderby('created_at','DESC')->take(1)->first();
         
//dd($ult_usuario);
        $usubanco = new GenUsuarioOca;
        $usubanco->gen_usuario_id=$ult_usuario->id;
        $usubanco->gen_operador_cambiario_id=$request->gen_operador_cambiario_id;
        $usubanco->save();
      }

      Alert::success('Registro Exitoso!', 'Usuario agregado.')->persistent("Ok");
      return redirect()->action('AdministradoresController@index');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\GenUsuario  $genUsuario
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {
        $titulo= 'Detalles Usuarios';
        $descripcion= 'Visualizar detalles del Usuario';

        
        $detusers=DetUsuario::where('gen_usuario_id', $id)->first();

        #$detusers1=DetUsuario::all()->pluck('descripcion_preg','id');
      /*if (!empty($detusers)) {
        $users=GenUsuario::where('id', $detusers->gen_usuario_id)->first();

          if (empty($users->gen_tipo_usuario_id == 2)) {

              Alert::success('El Usuario no esta registrado como Exportador!', 'Es un Administrador.')->persistent("Ok");

            return redirect()->action('GenUsuarioController@index');
            
          }
      } */
      
        

          $accionistas=Accionistas::where('gen_usuario_id', $id)->get();
          $productos=Productos::where('gen_usuario_id', $id)->get();

        return view('AdminUsuario.show',compact('detusers','accionistas','productos','titulo','descripcion'));   

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\GenUsuario  $genUsuario
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,$id)
    {   
      
        $titulo= 'Editar Usuarios Exportadores';
        $descripcion= 'Editar Usuario';
        $usu=GenUsuario::find($id);
        if ($usu->gen_tipo_usuario_id == 3) {
          $usu_edit = DB::table('gen_usuario')
                  //->join('gen_tipo_usuario','gen_usuario.gen_tipo_usuario_id','=','gen_tipo_usuario.id')
                  //->join('gen_status','gen_usuario.gen_status_id','=','gen_status.id')
                  //->join('cat_preguntas_seg','gen_usuario.cat_preguntas_seg_id','=','cat_preguntas_seg.id')
                  ->join('gen_usuario_oca', 'gen_usuario.id','=','gen_usuario_oca.gen_usuario_id')
                  //->join('gen_operador_cambiario', 'gen_operador_cambiario.id','=','gen_usuario_oca.gen_operador_cambiario_id')
                  ->select(
                          'gen_usuario.id',
                          'gen_usuario.email',
                          'gen_usuario.email_seg',
                          'gen_usuario.gen_tipo_usuario_id',
                          //'gen_usuario_oca.gen_usuario_id',
                          'gen_usuario_oca.gen_operador_cambiario_id',
                          //'gen_operador_cambiario.nombre_oca',
                          'gen_usuario.gen_status_id',
                          'gen_usuario.cat_preguntas_seg_id',
                          'gen_usuario.respuesta_seg'
                          )
                  ->where('gen_usuario.id','=',$id)
                  ->first();
        } else {
          $usu_edit=$usu;
        }
        
        
        //dd($usu_edit);
        $tipousu=GenTipoUsuario::whereIn('id',[1])->pluck('nombre_tipo_usu','id');
       
        $statususu=GenStatus::whereIn('id', [1, 2])->pluck('nombre_status','id');
        $preguntas=CatPreguntaSeg::all()->pluck('descripcion_preg','id');
        //preguntamos por los usuarios banco de produccion para que les muestre sus oca de prueba sino para los usuarios normales muestra los oca reales
       if (Auth::user()->id == 4 || Auth::user()->id == 1) {
         $oca=GenOperadorCamb::whereNotIn('id',[31])->pluck('nombre_oca','id');
       } else {
         $oca=GenOperadorCamb::whereNotIn('id',[31,32,33])->pluck('nombre_oca','id');
       }
        return view('AdminUsuario.edit', compact('usu_edit','preguntas','titulo','descripcion','tipousu','statususu','oca'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\GenUsuario  $genUsuario
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, GenUsuario $GenUsuario, $id)
    {
 
        if (isset($request->cat_preguntas_seg_id)) {
        if (isset($request->pregunta_alter)) {

           $pregunta=$request['pregunta_alter'];
        }else{

           $pregunta='N/A';
        }
      }
      
      //dd($pregunta);
      $usu= GenUsuario::find($id);
      $usu->email=$request->email;
      $usu->email_seg=$request->email_seg;
      $usu->gen_tipo_usuario_id=$request->gen_tipo_usuario_id;
      $usu->gen_status_id=$request->gen_status_id;
      $usu->password=bcrypt($request->password);
      $usu->cat_preguntas_seg_id=$request->cat_preguntas_seg_id;
      $usu->pregunta_alter=$pregunta;
      $usu->respuesta_seg=$request->respuesta_seg;
      $usu->updated_at=date("Y-m-d H:i:s");
      $usu->save();
      
      /*if ($usu->gen_tipo_usuario_id == 3) {
        $usuoca=GenUsuarioOca::where('gen_usuario_id',$id)->update(['gen_operador_cambiario_id' => $request->gen_operador_cambiario_id ,'updated_at' => date("Y-m-d H:i:s")]);
      }*/

      Alert::success('Registro Actualizado!', 'Usuario Actualizado.')->persistent("Ok");
      return redirect()->action('GenUsuarioController@index');
    
  }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\GenUsuario  $genUsuario
     * @return \Illuminate\Http\Response
     */
    public function destroy(GenUsuario $genUsuario, $id)

    {
      $usuario_destroy=GenUsuario::find($id)->update(['bactivo'=>0,'updated_at' => date("Y-m-d H:i:s")]);
        if ($usuario_destroy) {
          return 1;
        } else {
         return 2;
        }
             
    }
}
