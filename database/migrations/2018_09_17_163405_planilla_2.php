<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Planilla2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('planilla_2', function (Blueprint $table) {
          $table->engine = 'InnoDB';
          $table->increments('id');
          $table->unsignedInteger('det_declaracion_produc_id')->index()->nullable();
          $table->foreign('det_declaracion_produc_id')->references('id')->on('det_declaracion_produc');
          $table->string('uso_aplicacion',100)->nullable();
          $table->double('total_mate_import',100)->nullable();
          $table->double('total_mate_nac',100)->nullable();
          $table->boolean('bactivo')->default(1);
          $table->timestamps();
      });
     
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::dropIfExists('planilla_2'); 
    }
}
