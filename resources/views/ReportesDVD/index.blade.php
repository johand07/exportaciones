@extends('templates/layoutlte_admin')

@section('content')
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title></title>
</head>
<body>
	<!-- estilo para toda la pagina -->
	<style type="text/css">
	@media print{@page { width: 50%;
		height: 50%;
		margin: 1% 0% 1% 0%;
		filter: progid:DXImageTransform.Microsoft.BasicImage(Rotation=3);
		size: landscape;

		}}
	</style>
	<!--estilo para toda la pagina-->
	<div id="printf">
		<div class="row">
			<div class="col-md-12">
				{!! Form::open(['route'=>'ReportesDVD.store','method'=>'POST','id'=>'detalleemp']) !!}
				<br>
				<div class="col-md-6">
					<div class="input-daterange input-group" id="datepicker">
	      			<span class="input-group-addon"><b>Desde:</b> </span>
	     			 {!!Form::text('desde',(!is_null($desde))?$desde:old('desde'),['class'=>'form-control','id'=>'desde'])!!}
	      			<span class="input-group-addon"><b>Hasta:</b> </span>
	      			{!!Form::text('hasta',(!is_null($hasta))?$hasta:old('hasta'),['class'=>'form-control','id'=>'hasta'])!!}
        			</div>
				</div>
				<div class="col-md-3">
				 <a class="btn btn-primary" href="{{url('admin/AdminUsuario')}}">Cancelar</a>
        		 <input type="submit" name="Consultar" class="btn btn-success center" value="Consultar" onclick="">
				</div>
				 
				<div class="col-md-3">
					<!--button type="button" onClick="javascript:printfun({{count($sol_dvd)}})" class="btn btn-primary"><span class="glyphicon glyphicon-print" aria-hidden="true"></span>Imprimir</button-->

      				<a href="#" id="btnExport" class="btn btn-primary" onclick="exportarexel()"><span class="glyphicon glyphicon-file" aria-hidden="true" ></span>Exportar Excel</a>
				</div>
			</div>
			{{Form::close()}}
		</div>
		<br><br><br>
		<!--Boton de busqueda-->
		<div class="row">
			<div class="col-md-8">
			</div>
			<div class="col-md-4">
				<div class="input-group">
					<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
					<input id="system_search" name="busqueda" type="text" class="form-control" placeholder="Buscar">
				</div>
			</div>
		</div>
	</div>
	<div class="row" style="display:none;" id="cintillo"><br>
		<table style="width: 100%" >
			<tr>
				<td></td>
				<td align="center"><img class="img-responsive" style="width: 800px;" src={{ asset('img/cintillo_reporte.png') }}></td>
			</tr>
			<tr>
				<td></td>
				<td align="center">Desde: @if (!empty($desde)) {{$desde}}@endif     Hasta: @if (!empty ($hasta)) {{$hasta}}@endif</td>

			</tr>
		</table>
	</div>
<!---->
	<div class="row" style="display:none;" id="cintillo"><br>
		<table style="width: 100%" >
			<tr>
				<td></td>
				<td align="center"><img class="img-responsive" style="width: 800px;" src={{ asset('img/cintillo_reporte.png') }}></td>
			</tr>
			<tr>
				<td></td>
				<td align="center">Desde: @if (!empty($desde)) {{$desde}}@endif     Hasta: @if (!empty ($hasta)) {{$hasta}}@endif</td>

			</tr>
		</table>
	</div>
<!---->
<div class="row">
  <div class="col-md-12">
    <div class="row">
      <div class="col-md-6">
        <b>Número de Solicitudes:&nbsp;</b><span class="label label-success">&nbsp;&nbsp;&nbsp;<b style="font-size:14px;">{{count($sol_dvd)}} &nbsp;&nbsp;</b></span> 
      </div>
      <div class="col-md-6"></div>
    </div>
    <br>
    <table id="listaDetalleEmpresa" class="table-list-search tbody table table-bordered table-hover" style="width:100%">
            <thead>
                <tr>
                    <th>Num. Solicitud</th>
                    <th>Fecha de Solicitud</th>
                    <th>Rif</th>
                    <th>Razón Social</th>
                    <th>Fecha de Estatus</th>
                    <th>Fecha de Embarque</th>
                    <th>Divisa</th> 
                </tr>
            </thead>
            <tbody>
            
            	@foreach($sol_dvd as $key=>$empre)
                <tr>
                    <td>{{$empre->Num_Solicitud ? $empre->Num_Solicitud :''}}</td>
                    <td>{{$empre->Fecha_DVD ? $empre->Fecha_DVD :''}}</td>
                    <td>{{$empre->rif ? $empre->rif :''}}</td>
                    <td>{{$empre->razon_social ? $empre->razon_social :''}}</td>
                    <td>{{$empre->Fecha_Estatus ? $empre->Fecha_Estatus :''}}</td>
                    <td>{{$empre->Fecha_Embarque ? $empre->Fecha_Embarque:''}}</td>
                    <td>{{$empre->Divisa ? $empre->Divisa :''}}</td>            
                </tr>
              @endforeach
            </tbody>
        </table>
  </div>
</div>
@stop

<script src="{{asset('js/ajax_v2.1.1.js')}}"></script>
<script type="text/javascript">

  function printfun(cantidad){
    IE = window.navigator.appName.toLowerCase().indexOf("micro") != -1; //se determina el tipo de  navegador
    (IE)?sColl = "all.":sColl = "getElementById('";
    (IE)?sStyle = ".style":sStyle = "').style";
    eval("document." + sColl + "printf" + sStyle + ".display = 'none';"); //se ocultan los botones
    eval("document." + sColl + "ver" + sStyle + ".display = 'none';"); //se ocultan los botones
    for (var i =0; i <=cantidad; i++) {
      $("#det_"+i).hide();
      //eval("document." + sColl + "det_"+i + sStyle + ".display = 'none';"); //se ocultan los botones

    }

    eval("document." + sColl + "cintillo" + sStyle + ".display = 'block';");
    $('.main-footer').hide();


    window.print();
    //print();
    /*if (i == cantidad) {
       print(); //se llama el dialogo de impresiÃ³n
    }*/
    //$('#ver').hide();//Casa 
    
    eval("document." + sColl + "printf" + sStyle + ".display = '';"); // se muestran los botones nuevamente
    eval("document." + sColl + "ver" + sStyle + ".display = '';"); //se ocultan los botones
    for (var i =0; i <=cantidad; i++) {
      $("#det_"+i).show();
      //eval("document." + sColl + "det_"+i + sStyle + ".display = 'none';"); //se ocultan los botones

    }

    eval("document." + sColl + "cintillo" + sStyle + ".display = 'none';");
    $('.main-footer').show();

  }
    /*$("#btnExport").click(function(e) {

        window.open('data:application/vnd.ms-excel,' + encodeURIComponent($('#listaDetalleEmpresa').html()));

        e.preventDefault();

    });*/


    /*--------------ajax reporte exel---------------------------------------*/
    /***Valido si viene vacio los campos desde y hasta****/
function exportarexel(){

	if($('#desde').val() =='' && $('#hasta').val() == ''){
		
	 swal("Por Favor","Debe filtrar un rango de fecha para exportar el excel"); 
	}else{

	 $.ajax({    //AgenteAduanal.destroy O exportador/AgenteAduanal/{AgenteAduanal}
            url: '/admin/ExcelReportesDVD',
            type: 'GET',

            data: {
                    
                    'desde': $('#desde').val(),
                    'hasta': $('#hasta').val()

                },
        })
        .done(function(data) {
            //alert(data.file);
              var a = document.createElement("a");
              a.href = data.file;
              a.download = data.name+'.xls';
              document.body.appendChild(a);
              a.click();
              a.remove();
              console.log(data);
       



        })
        .fail(function(jqXHR, textStatus, thrownError) {
            errorAjax(jqXHR,textStatus)

        })

	}

}
</script>