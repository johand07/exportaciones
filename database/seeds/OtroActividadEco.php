<?php

use Illuminate\Database\Seeder;
use App\Models\CatActividadEcoInversionista;

class OtroActividadEco extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $sectorInv = [
            [
               
                'cat_sector_inversionista_id'   => 1,
                'nombre'       => 'Otro',
                'created_at' => '2021-08-24 13:59:08',
                'updated_at' => '2021-08-24 13:59:08',
            ],
            [
               
                'cat_sector_inversionista_id'   => 2,
                'nombre'       => 'Otro',
                'created_at' => '2021-06-15 13:59:08',
                'updated_at' => '2021-06-15 13:59:08',
            ],
            [
                
                'cat_sector_inversionista_id'   => 3,
                'nombre'       => 'Otro',
                'created_at' => '2021-06-15 13:59:08',
                'updated_at' => '2021-06-15 13:59:08',
            ],
            [
               
                'cat_sector_inversionista_id'   => 4,
                'nombre'       => 'Otro',
                'created_at' => '2021-06-15 13:59:08',
                'updated_at' => '2021-06-15 13:59:08',
            ],
            [
                
                'cat_sector_inversionista_id'   => 5,
                'nombre'       => 'Otro',
                'created_at' => '2021-06-15 13:59:08',
                'updated_at' => '2021-06-15 13:59:08',
            ],
            [
               
                'cat_sector_inversionista_id'   => 6,
                'nombre'       => 'Otro',
                'created_at' => '2021-06-15 13:59:08',
                'updated_at' => '2021-06-15 13:59:08',
            ],
            [
               
                'cat_sector_inversionista_id'   => 7,
                'nombre'       => 'Otro',
                'created_at' => '2021-06-15 13:59:08',
                'updated_at' => '2021-06-15 13:59:08',
            ],
            [
               
                'cat_sector_inversionista_id'   => 8,
                'nombre'       => 'Otro',
                'created_at' => '2021-06-15 13:59:08',
                'updated_at' => '2021-06-15 13:59:08',
            ],
            [
                
                'cat_sector_inversionista_id'   => 9,
                'nombre'       => 'Otro',
                'created_at' => '2021-06-15 13:59:08',
                'updated_at' => '2021-06-15 13:59:08',
            ],
            [
                
                'cat_sector_inversionista_id'   => 10,
                'nombre'       => 'Otro',
                'created_at' => '2021-06-15 13:59:08',
                'updated_at' => '2021-06-15 13:59:08',
            ],

            [
                
                'cat_sector_inversionista_id'   => 11,
                'nombre'       => 'Otro',
                'created_at' => '2021-06-15 13:59:08',
                'updated_at' => '2021-06-15 13:59:08',
            ],
            
            

            
        ];

        CatActividadEcoInversionista::insert($sectorInv);
    }
}
