<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddGenAduanaSalidaIdToDetUserResguardo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('det_users_resguardo', function (Blueprint $table) {
            
            $table->unsignedInteger('gen_aduana_salida_id')->index()->after('unidad')->nullable();
            $table->foreign('gen_aduana_salida_id')->references('id')->on('gen_aduana_salida');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('det_users_resguardo', function (Blueprint $table) {
            
            $table->dropColumn('gen_aduana_salida_id');
           
        
       });
    }
}
