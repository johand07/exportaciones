@extends('templates/layoutlte')

@section('content')

<!--Aqui Form::open-->
{!! Form::open(['route' => 'CertificadoOrigen.FormCerticado' ,'method'=>'POST']) !!}


<div class="content">
	<div class="panel-group">
		<div class="panel-primary" style="background-color: #fff">
			<div class="panel-heading"><h3>Seleccione el tipo de certificado</h3>
	
			</div>
			<div class="panel-body">
				<div class="form-group">
					{!! Form::label('Certificado','Certificado') !!}

					{!! Form::select('gen_tipo_certificado_id',$tipo_certificado,null,['class'=>'form-control','id'=>'gen_tipo_certificado_id',
					'placeholder'=>'Seleccione un certificado de origen','onchange'=>'valorSelect()', 'required'=>'true']) !!}
				</div>

				<div class="row text-center">
					<a href="{{ url('/exportador/CertificadoOrigen')}}" class="btn btn-primary">Cancelar</a>
					<input type="submit" class="btn btn-success" value="Siguiente" name="Siguiente"> 
				</div>
			</div>
		</div>
	</div>
</div>

{{Form::close()}}
@stop

		