<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ProductosInversion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('productos_inversion', function (Blueprint $table) {
          $table->engine = 'InnoDB';
          $table->increments('id');
          $table->unsignedInteger('gen_sol_inversion_id')->index()->nullable();
          $table->foreign('gen_sol_inversion_id')->references('id')->on('gen_sol_inversion');
          $table->unsignedInteger('productos_id')->index()->nullable();
          $table->foreign('productos_id')->references('id')->on('productos');
          $table->string('capacidad_utilizada','100')->nullable();
          $table->string('unidad_tiempo','100')->nullable();
          $table->string('porcentaje_util','100')->nullable();
          $table->string('capacidad_ociosa','100')->nullable();
          $table->boolean('bactivo')->default(1);
          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::dropIfExists('productos_inversion');
    }
}
