@extends('templates/layoutlte_admin_resgu_cna')
@section('content')
  <div class="content" style="background-color: #fff">
    <div class="panel-group" style="background-color: #fff;">
      <div class="panel-primary">
        <div class="panel-heading"><h3>Gestion de Usuarios</h3> </div>
        <div class="panel-body">
            <div class="row">
              
              <div class="col-md-12">
                <div class="row">
                  <div class="col-md-6">

                           <h4 class="text-info">Bandeja de Gestión de usuarios analistas y administradores:</h4><hr>
                          
                  </div>
                  <div class="col-md-3">
                   
                  </div>
                  <div class="col-md-3 text-left">
                    <a class="btn btn-primary" href="{{url('/AdministradorResguardoCna/userManagerResgCna/create')}}">
                      <span class="glyphicon glyphicon-file" title="Agregar" aria-hidden="true"></span>
                       Registrar nuevo Usuario
                    </a>
                  </div>
                </div>
                <table id="listaUsersInversiones" class="table table-striped table-bordered" style="width:100%">
                  <thead>
                      <tr>

                          <th>#</th>
                          <th>Nombre</th>
                          <th>Apellido</th>
                          <th>Cédula</th>
                          <th>email</th>
                          <th>Tipo de Usuario</th>
                          <th>Estatus</th>
                          <th>Fecha Creación</th>
                          <th>Acciones</th>
                      </tr>
                  </thead>
                  <tbody>

                    @if(isset($users))
                      @foreach($users as $key =>  $user)
                        @if(isset($user->rDetUsersResguardo) && ($detUser->gen_aduana_salida_id == $user->rDetUsersResguardo->gen_aduana_salida_id))
                        <tr>
                          <td>
                            {{$key+1}}
                          </td>
                          <td>
                            {{$user->rDetUsersResguardo ? $user->rDetUsersResguardo->nombre_funcionario : ''}}
                          </td>
                          <td>
                            {{$user->rDetUsersResguardo ? $user->rDetUsersResguardo->apellido_funcionario : ''}}
                          </td>
                          <td>
                            {{$user->rDetUsersResguardo ? $user->rDetUsersResguardo->cedula_funcionario : ''}}
                          </td>
                          <td>
                            {{$user->email ? $user->email : ''}}
                          </td>
                          <td>
                            {{$user->tipoUsuario ? $user->tipoUsuario->nombre_tipo_usu : ''}}
                          </td>
                          <td>
                            {{$user->estatus ? $user->estatus->nombre_status : ''}}
                          </td>
                          <td>
                            {{$user->created_at ? $user->created_at : ''}}
                          </td>
                          
                          <td>
                           <a href="{{ route('userManagerResgCna.edit',$user->id)}}" class="btn btn-sm btn-warning" id="atender"><i class="glyphicon glyphicon-edit"></i><b> Editar</b></a>
                          </td>
                        </tr>
                        @endif
                      @endforeach
                    @endif
                  
                  </tbody>    
                </table>
              </div>
            </div><!-- Cierre 1° Row-->
        </div>
      </div>
    </div>
</div><!-- fin content-->
@stop