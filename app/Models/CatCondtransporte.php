<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CatCondtransporte extends Model
{
    protected $table = 'cat_cond_transport';



	protected $fillable = [

        'nombre',
        'bactivo',
    ];

    protected $date =['created_at','updated_at'];
}
