/****alter_table_observaciones***/

ALTER TABLE `gen_certificado` CHANGE `observaciones` `observaciones` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL;
ALTER TABLE `gen_certificado` CHANGE `observacion_analista` `observacion_analista` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL;
ALTER TABLE `gen_certificado` CHANGE `observacion_coordinador` `observacion_coordinador` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL;
