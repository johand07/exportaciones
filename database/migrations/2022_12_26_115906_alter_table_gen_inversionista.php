<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableGenInversionista extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('gen_sol_inversionista', function (Blueprint $table) {
            
            $table->unsignedInteger('gen_divisa_id')->index()->nullable()->after('cat_tipo_usuario_id');
            $table->foreign('gen_divisa_id')->references('id')->on('gen_divisa');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('gen_sol_inversionista', function (Blueprint $table) {
            $table->dropColumn('gen_divisa_id');
            
        });
    }
}
