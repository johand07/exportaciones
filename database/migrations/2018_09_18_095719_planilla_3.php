<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Planilla3 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('planilla_3', function (Blueprint $table) {
          $table->engine = 'InnoDB';
          $table->increments('id');
          $table->unsignedInteger('det_declaracion_produc_id')->index()->nullable();
          $table->foreign('det_declaracion_produc_id')->references('id')->on('det_declaracion_produc');
          $table->unsignedInteger('gen_divisa_id')->index()->nullable();
          $table->foreign('gen_divisa_id')->references('id')->on('gen_divisa');
          $table->string('utilidad',100)->nullable();
          $table->double('precio_puerta_fabri')->nullable();
          $table->double('flete_interno')->nullable();
          $table->double('precio_fob_export')->nullable();
          $table->boolean('bactivo')->default(1);
          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('planilla_3'); 
    }
}
