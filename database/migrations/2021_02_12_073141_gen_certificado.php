<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GenCertificado extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gen_certificado', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('gen_usuario_id')->default(1)->index()->nullable();
            $table->foreign('gen_usuario_id')->references('id')->on('gen_usuario');
            $table->unsignedInteger('gen_status_id')->default(1)->index()->nullable();
            $table->foreign('gen_status_id')->references('id')->on('gen_status');
            $table->unsignedInteger('gen_tipo_certificado_id')->default(1)->index()->nullable();
            $table->foreign('gen_tipo_certificado_id')->references('id')->on('gen_tipo_certificado');
            $table->string('num_certificado', 150)->unique()->nullable();
            $table->string('pais_exportador', 150)->nullable();
            $table->string('pais_cuidad_exportador', 150)->nullable();
            $table->string('pais_importador', 150)->nullable();
            $table->date('fecha_emision_exportador')->nullable();
            $table->string('observaciones', 250)->nullable();
            $table->date('fecha_certicado')->nullable(); 
            $table->string('ciudad_certificado', 200)->nullable();
            $table->string('observacion_analista', 100)->nullable();
            $table->string('observacion_coordinador', 100)->nullable();
            $table->boolean('bactivo')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gen_certificado');
    }
}
