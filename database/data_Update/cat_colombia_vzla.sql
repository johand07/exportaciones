/*
-- Query: SELECT * FROM exportaciones.cat_colombia_vzla
LIMIT 0, 500

-- Date: 2019-08-23 01:56
*/
INSERT INTO `cat_colombia_vzla` (`id`,`codigo`,`descripcion`,`bactivo`,`created_at`,`updated_at`) VALUES (1,'3-1a','Anexo II,Artículo 3,párrafo 1 literal a)',1,NULL,NULL);
INSERT INTO `cat_colombia_vzla` (`id`,`codigo`,`descripcion`,`bactivo`,`created_at`,`updated_at`) VALUES (2,'3-1b','Anexo II,Artículo 3,párrafo 1 literal b)',1,NULL,NULL);
INSERT INTO `cat_colombia_vzla` (`id`,`codigo`,`descripcion`,`bactivo`,`created_at`,`updated_at`) VALUES (3,'3-1c','Anexo II,Artículo 3,párrafo 1 literal c)',1,NULL,NULL);
INSERT INTO `cat_colombia_vzla` (`id`,`codigo`,`descripcion`,`bactivo`,`created_at`,`updated_at`) VALUES (4,'3-1di','Anexo II,Artículo 3,párrafo 1 literal d),inciso i',1,NULL,NULL);
INSERT INTO `cat_colombia_vzla` (`id`,`codigo`,`descripcion`,`bactivo`,`created_at`,`updated_at`) VALUES (5,'3-1dii','Anexo II,Artículo 3,párrafo 1 literal d),inciso ii',1,NULL,NULL);
INSERT INTO `cat_colombia_vzla` (`id`,`codigo`,`descripcion`,`bactivo`,`created_at`,`updated_at`) VALUES (6,'3-1diii','Anexo II,Artículo 3,párrafo 1 literal d),inciso iii',1,NULL,NULL);
INSERT INTO `cat_colombia_vzla` (`id`,`codigo`,`descripcion`,`bactivo`,`created_at`,`updated_at`) VALUES (7,'7','Anexo II,Articulo 7°',1,NULL,NULL);
INSERT INTO `cat_colombia_vzla` (`id`,`codigo`,`descripcion`,`bactivo`,`created_at`,`updated_at`) VALUES (8,'n','No califica Origen Venezolano para ese Acuerdo o País',1,NULL,NULL);
INSERT INTO `cat_colombia_vzla` (`id`,`codigo`,`descripcion`,`bactivo`,`created_at`,`updated_at`) VALUES (9,'*','Información insuficiente para la determinación del Origen',1,NULL,NULL);
INSERT INTO `cat_colombia_vzla` (`id`,`codigo`,`descripcion`,`bactivo`,`created_at`,`updated_at`) VALUES (10,'(1)','no',1,NULL,NULL);
INSERT INTO `cat_colombia_vzla` (`id`,`codigo`,`descripcion`,`bactivo`,`created_at`,`updated_at`) VALUES (11,'(2)','no',1,NULL,NULL);
INSERT INTO `cat_colombia_vzla` (`id`,`codigo`,`descripcion`,`bactivo`,`created_at`,`updated_at`) VALUES (12,'(3)','no',1,NULL,NULL);
