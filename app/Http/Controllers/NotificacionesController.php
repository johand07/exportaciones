<?php

namespace App\Http\Controllers;

use App\Models\DetUsuario;
use App\Models\GestionInvitacion;
use App\Models\Eventos;
use Illuminate\Http\Request;
use \App\Mail\notificaciones as notificacionesEmail;
use \Illuminate\Support\Facades\Mail;
use Alert;
use Auth;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use URL;

class NotificacionesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $titulo='Envio de Correo Masivo';
        $descripcion='Ventanilla Unica de Comercio Exterior';

        $eventos = Eventos::where('bactivo',1)->pluck('nom_evento','id');
        return view('NotificacionesEmail.create',compact('titulo','descripcion','eventos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\DetUsuario  $detUsuario
     * @return \Illuminate\Http\Response
     */
    
    public function NotificacionesEmail(Request $request)
    {
        //$universo=DetUsuario::where('bactivo',1)->whereIn('id',[1,2,8,9,21])->get();
        //dd($universo);
        //datos del evento
        $evento = Eventos::find($request->eventos_id);
        $universo=GestionInvitacion::where('eventos_id',$request->eventos_id)->get();
        //dd($universo);
        $contarEnviado = 0;
        $contarFallido = 0;
        foreach ($universo as $key => $invitado) {
            if (empty($invitado->invitacion_enviada)) {
            
            try{
          
           
               $myRequest = new \Illuminate\Http\Request();
               $myRequest->setMethod('GET');
               $myRequest->merge(['id_invitacion' => encrypt($invitado->id)]);
               $myRequest->merge(['arte' => 1]);
               $myRequest->merge(['returnbinario' => 1]);
               $guardoimg= $this->generarinvitacion( $myRequest);



                $result  = Mail::to($invitado->correo)->send(new notificacionesEmail(
                $evento,
                $invitado,
                $guardoimg));

                $invitado->invitacion_enviada=1;
                $invitado->touch();

                $contarEnviado++;

            }catch(\Exception $e){
                $contarFallido++;
            }

            }
           
        }
        
        Alert::success('Notificación Enviada con Éxito, se envió un email de invitación a '.$contarEnviado.' Exportador!'.(($contarFallido>0)?"\n\n$contarFallido invitacion no enviada":''))->persistent("Ok");
        
        return redirect('/admin');
            
    }

    public function show(DetUsuario $detUsuario)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\DetUsuario  $detUsuario
     * @return \Illuminate\Http\Response
     */
    public function edit(DetUsuario $detUsuario)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\DetUsuario  $detUsuario
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DetUsuario $detUsuario)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\DetUsuario  $detUsuario
     * @return \Illuminate\Http\Response
     */
    public function destroy(DetUsuario $detUsuario)
    {
        //
    }


    public function generarQR(Request $request)
    {
        $id_invitacion = $request->id_invitacion;
/*
Ver si la empresa esta registrada
otras validaciones
 */
        header("Content-Type: image/png");
        echo QrCode::format('png')->size(300)->margin(0)->generate(\URL::to('/api/validarInvitacion/'.$id_invitacion));
        exit;
    }

    public function validarQR(Request $request){
        $id_invitacion = decrypt($request->id_invitacion);
        $validardatos = GestionInvitacion::find($id_invitacion);
        if(empty($validardatos->asistencia)){
            //dd(Auth::user());
            if (!empty(Auth::user()) && Auth::user()->gen_tipo_usuario_id == 2) {
                 $validardatos->asistencia=1;
                 $validardatos->touch();
            }
              /*Validar que se marque la asistencia el dia del evento
                o?
              */
             
        }
      
        return view('NotificacionesEmail.validarQR',compact('id_invitacion','validardatos'));
    }

    public function generarinvitacion(Request $request)
    {
        /*Falta recibir los datos que vienen del request */
   
            $id_invitacion = decrypt($request->id_invitacion);
            $arte = ($request->arte);
            $returnbinario= (!empty($request->returnbinario) && $request->returnbinario ==1)?1:0;
           // (condicion logica)?'condicion verdadera':'condicion false';
            $invitacion = GestionInvitacion::find($id_invitacion);


            switch ($arte) {
                case '1':
            
                if(!$returnbinario)
                    header("Content-type: image/jpeg");
 

    $imageBase = base_path().'/public/img/invitacion.jpg';

    $imageQR = imagecreatefromstring( QrCode::format('png')->size(130)->margin(0)->generate(\URL::to('/api/validarInvitacion/'.encrypt($id_invitacion))));

    $im =  @imagecreatefromjpeg($imageBase);

                 /* Ver si falló */
    if($im)
    {

        /*Fuente */

      $font_path = base_path().'/public/css/fonts/Raleway/Raleway-Regular.ttf';

      /*Colores RGB*/
       $negro = imagecolorallocate($im, 0, 0, 0);
        $azul = imagecolorallocate($im, 0, 0, 255);
         $rojo = imagecolorallocate($im, 200, 0, 0);

                /*Nombre y Apellido */
               
                imagettftext($im, 25, 0, 38, 524, $negro, $font_path, $invitacion->repre_legal);
                /*Cedula*/
                imagettftext($im, 25, 0, 38, 580, $negro, $font_path, $invitacion->cedula);
                /*Institucion*/
                imagettftext($im, 25, 0, 38, 640, $negro, $font_path, $invitacion->razon_social);
                 /*CodifoEmpresa*/
                imagettftext($im, 13, 0, 400, 685, $negro, $font_path, 123456);

                /*Pegar QR*/

                imagecopy($im, $imageQR, 412, 506, 0, 0, 130, 130);

       if($returnbinario)
         ob_start(); //inicializa la captura de informacion en el contenido de la pagina


                imagejpeg($im); // imprime img

        if($returnbinario){
             $final_image = ob_get_contents(); // obtine la informacion que capturo ob_start

             ob_end_clean(); //limpiar y terminal el canal de captura de la informacion
             return $final_image;
        }else{
                imagedestroy($im); // destruir imagen
            }
    }
                break;
                
                default:
                    # code...
                    break;
            }
    }

   
}
