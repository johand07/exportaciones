<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>PLANILLA DJIR 01</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style type="text/css">
        .centrar{
            text-align: center;
        }

        .titulo{
            
            font-weight: bold;
        }

        .nro-correlativo{
            color: red;
            font-size: 16px;
        }

        .derecha{
            text-align: left;

        }

        .text-normal{
            font-size: 14px;
        }

        .text-medio{
            font-size: 12px;
        }
        .text-11{
            font-size: 11px;
        }
        .text-small{
            font-size: 10px;
        }

        .text-9{
            font-size: 9px;
        }

        .text-8{
            font-size: 8px;
        }

        .espacio-superior{
            margin-top: 10px;
        }

        .espacio-superior-small{
            margin-top: 5px;
        }

        .ancho-medio{
            width: 50%;
        }

        .ancho-full{
            width: 100%;
        }

        .ancho-medio-alto{
            width: 75%;
        }

        .ancho-90{
            width: 90%;
        }

        .table{
            border-collapse: collapse;
        }
        
        .borde{
            border: 1px solid #fff;
        }

        .sin-borde{
            border: none;
        }

        .top{
            vertical-align: top;
        }

        .under-dotted{    
            border-bottom: 1px dotted #000;
            text-decoration: none;
        }

        .content-1{
            width: 5%;
        }

        .content-9{
            width: 20%;
        }

        .sangria-1{
            margin-left: 28px;
        }

        .sangria-2{
            margin-left: 36px;
        }

        .sangria-3{
            margin-left: 54px;
        }

        .sangria-sub{
            margin-left: 14px;
            
        }

        
        .page-break { page-break-before: always; }

        .table-center{
            margin: 0 auto; 
        }

        .justify{
            text-align: justify;
        }

        td.justify {
            padding-left: 8px;
            padding-right: 8px; 
        }
        .lineal{
            
        }
        .lineal>div{
            max-width:91%;

        }
        .lineal>*{
           display: inline-block;
           vertical-align:top;
        }

        .sangria-lista{
            list-style-type: none;
            margin: 0;
            padding: 0;
            margin-bottom: 4px;
        }

        .sangria-lista>li{
            margin-top: 5px;
            padding-left: 10px;
            padding-right: 10px;
        }
    </style>
</head>
<body>
    <div class="centrar espacio-superior">
        <table style="width:100%;" class="table2">
          <tbody>
            <tr>
              <td class="td3">
                <img class="img-responsive" style="width:200px; height: 40px;" src='img/MIPPCOEXIN.png'>
              </td>
           <td class="nro-correlativo" style="width: 30%; font-size:80%; text-align: left;">
            <td class="nro-correlativo" style="width: 30%; font-size:80%; text-align: left;">
               <td>
                  <img class="img-responsive borde" style="width: 300px; height: 40px; text-align: right;" src='img/cintillo.png'>
              </td>
            </tr>
          </tbody>
        </table>
    </div><br>

    <div class="centrar espacio-superior">
        <table class="ancho-full">
            <tr>
                <td class="center" style="width: 70%; font-size:107%; text-align: center;">
                    PLANILLA (DJIR 01)
                </td>
            </tr>
            <tr>
                <td class="center" style="width: 70%; font-size:14px; text-align: center;">
                     <b>PLANILLA DE DECLARACIÓN JURADA DE INVERSIÓN REALIZADA (01)
                        INFORMACIÓN GENERAL</b>
                </td>
            </tr>
        </table>
    </div>

    <div class="centrar espacio-superior">
        <table class="ancho-full">
            <tr>
            	<td class="nro-correlativo" style="width: 30%; font-size:80%; text-align: left;">
                     <b>Tipo de Persona:</b><b style="color: black">{{ $solicitudDeclaracion->CatTipoUsuario->nombre}}</b>
                </td>
                
                <td class="nro-correlativo" style="width: 30%; font-size:80%; text-align: right;"></td>
                <td class="nro-correlativo" style="width: 30%; font-size:80%; text-align: right;">
                     <b>Fecha Solicitud:</b><b style="color: black">{{$solicitudDeclaracion->created_at}}
                </td>

            </tr>
            <tr>
            	<td class="nro-correlativo" style="width: 30%; font-size:80%; text-align: right;"></td>
                <td class="nro-correlativo" style="width: 30%; font-size:80%; text-align: right;"></td>
                <td class="nro-correlativo" style="width: 30%; font-size:80%; text-align: right;">
                     <b>Nº Solicitud:</b><b style="color: black">{{$solicitudDeclaracion->num_declaracion}}</b>
                </td>

            </tr>
        </table>
    </div>
    <div class="centrar espacio-superior">
        <table class="ancho-full" border="1">
              <tr>
                <td class="top" style="background-color:#818286; color:#fff;text-align: left; font-size: 13px" colspan="100"><b>Información General</b></td>
            </tr>
            <tr>
                <td class="top" style="text-align: left; font-size: 10px" colspan="50"><b>Nombre o razón social del solicitante</b><br>
                    <ul>
                     {{$planillaDjir01->razon_social_solicitante ? $planillaDjir01->razon_social_solicitante:''}}
                    </ul>        
                </td>

                <td class="top" style="text-align: left; font-size: 10px" colspan="50"><b>N° Registro de Información Fiscal (R.I.F)</b><br>
                    <ul>
                      {{$planillaDjir01->rif_solicitante ? $planillaDjir01->rif_solicitante:''}}
                    </ul>        
                </td>
            </tr>
        </table>
    </div>
    <div class="centrar espacio-superior">
        <table class="ancho-full" border="1">
            <tr>
                <td class="top" style="background-color:#818286; color:#fff;text-align: left; font-size: 13px" colspan="100"><b>Datos de Ingreso de Inversión Extranjera</b></td>
            </tr>
            <tr>
                <td class="top" style="text-align: left; font-size: 10px" colspan="50"><b>Fecha de inicio de operaciones</b><br>
                    <ul>
                     {{$planillaDjir01->finicio_op ? $planillaDjir01->finicio_op:''}}
                    </ul>        
                </td>

                <td class="top" style="text-align: left; font-size: 10px" colspan="50"><b>Fecha de ingreso de la Inversión</b><br>
                    <ul>
                      {{$planillaDjir01->fingreso_inversion ? $planillaDjir01->fingreso_inversion:''}}
                    </ul>        
                </td>
            </tr>
        </table>
        <table class="ancho-full" border="1">
            <tr>
                <td class="top" style="text-align: left; font-size: 13px" colspan="100"><b>Tipo de Empresa Receptora de IED</b></td>
            </tr>
        </table>
        <table class="ancho-full" border="1">
            <tr>
                <td colspan="2">
                @if(isset($planillaDjir01->emp_privada) && $planillaDjir01->emp_privada == 1)
                        <div>Empresa Privada</div>
                    @elseif(isset($planillaDjir01->emp_publica) && $planillaDjir01->emp_publica == 1 )
                        <div>Empresa Pública</div>

                    @elseif(isset($planillaDjir01->emp_mixto) && $planillaDjir01->emp_mixto == 1 )
                        <div>Empresa Capital mixto</div>
                    @else
                        <div>Otro: {{$planillaDjir01->especifique_otro ? $planillaDjir01->especifique_otro:''}}</div>
                @endif
                </td>
            </tr>
        </table>
        <table class="ancho-full" border="1">
            <tr>
                <td class="top" style="text-align: left; font-size: 10px" colspan="50"><b>Dirección de la empresa</b><br>
                  
                      {{$planillaDjir01->direccion_emp ? $planillaDjir01->direccion_emp:''}}
                           
                <br><br></td>

                <td class="top" style="text-align: left; font-size: 10px" colspan="50"><b>Teléfono</b><br>
                       {{$planillaDjir01->tlf_emp ? $planillaDjir01->tlf_emp:''}}
                  
                <br><br></td>
                <td class="top" style="text-align: left; font-size: 10px" colspan="50"><b>Página Web</b><br>
                   
                    {{$planillaDjir01->pagina_web_emp? $planillaDjir01->pagina_web_emp:''}}
                         
                <br><br></td>

                <td class="top" style="text-align: left; font-size: 10px" colspan="50"><b>Correo electrónico</b><br>
                   
                      {{$planillaDjir01->correo_emp ? $planillaDjir01->correo_emp:''}}
                       
                <br><br></td>
            </tr>

      
   
            <tr>
                <td class="top" style="text-align: left; font-size: 10px" colspan="50"><b>RR.SS</b><br>
                   
                      {{$planillaDjir01->rrss ? $planillaDjir01->rrss:''}}
                           
                <br><br></td>

                <td class="top" style="text-align: left; font-size: 10px" colspan="50"><b>Registro Público</b><br>
                    
                       {{$planillaDjir01->registro_publico ? $planillaDjir01->registro_publico:''}}
                           
                <br><br></td>
                <td class="top" style="text-align: left; font-size: 10px" colspan="50"><b>Estado</b><br>
                 
                    {{$planillaDjir01->estado ? $planillaDjir01->estado->estado: ''}} 
                          
                <br><br></td>

                <td class="top" style="text-align: left; font-size: 10px" colspan="50"><b>Municipio</b><br>
                    
                     {{$planillaDjir01->municipio ? $planillaDjir01->municipio->municipio : ''}}
                <br><br></td>
            </tr>
      
        
            <tr>
                <td class="top" style="text-align: left; font-size: 10px" colspan="50"><b>Número</b><br>
                   
                      {{$planillaDjir01->numero_r_mercantil ? $planillaDjir01->numero_r_mercantil:''}}
                          
                <br><br></td>

                <td class="top" style="text-align: left; font-size: 10px" colspan="50"><b>Folio</b><br>
                   
                       {{$planillaDjir01->folio ? $planillaDjir01->folio:''}}
                           
                <br><br></td>
                <td class="top" style="text-align: left; font-size: 10px" colspan="50"><b>Tomo</b><br>
                   
                    {{$planillaDjir01->tomo ? $planillaDjir01->tomo:''}}
                         
                <br><br></td>

                <td class="top" style="text-align: left; font-size: 10px" colspan="50"><b>Fecha de Inscripción</b><br>
                   
                      {{$planillaDjir01->finspeccion ? $planillaDjir01->finspeccion:''}}
                        
                <br><br></td>
            </tr>
        </table>
        <table class="ancho-full" border="1">
            <tr>
                <td class="top" style="text-align: left; font-size: 13px" colspan="100"><b>Número de empleos actuales</b></td>
            </tr>
            <tr>
                <td class="top" style="text-align: left; font-size: 10px" colspan="50"><b>Directos</b><br>
                   
                      {{$planillaDjir01->emple_actual_directo ? $planillaDjir01->emple_actual_directo:''}}
                         
                </td>

                <td class="top" style="text-align: left; font-size: 10px" colspan="50"><b>Indirectos</b><br>
                  
                      {{$planillaDjir01->emple_actual_indirecto ? $planillaDjir01->emple_actual_indirecto :''}}
                         
                </td>
            </tr>
      
            <tr>
                <td class="top" style="text-align: left; font-size: 13px" colspan="100"><b>Empleos por generar</b></td>
            </tr>
            <tr>
                <td class="top" style="text-align: left; font-size: 10px" colspan="50"><b>Directos</b><br>
                    
                      {{$planillaDjir01->emple_generar_directo ? $planillaDjir01->emple_generar_directo :''}}
                         
                </td>

                <td class="top" style="text-align: left; font-size: 10px" colspan="50"><b>Indirectos</b><br>
                   
                     {{$planillaDjir01->emple_generar_indirecto ? $planillaDjir01->emple_generar_indirecto:''}}
                          
                </td>
            </tr>
        </table>
    </div>
    <div class="centrar espacio-superior">
       <table class="ancho-full" border="1">
            <tr>
                <td class="top" style="background-color:#818286; color:#fff;text-align: left; font-size: 13px" colspan="100"><b>Capital Social de la Empresa</b></td>
            </tr>
            <tr>
                <td class="top" style="text-align: left; font-size: 10px" colspan="50"><b>Suscrito Bs</b><br>
                    
                      {{$planillaDjir01->capital_suscrito ? $planillaDjir01->capital_suscrito:''}}
                         
                <br><br></td>

                <td class="top" style="text-align: left; font-size: 10px" colspan="50"><b>N° Pagado Bs</b><br>
                    
                      {{$planillaDjir01->capital_apagado ? $planillaDjir01->capital_apagado:''}}
                        
                 <br><br></td>
            </tr>
            <tr>
                <td class="top" style="text-align: left; font-size: 10px" colspan="50"><b>Número de acciones emitidas</b><br>
                    
                      {{$planillaDjir01->num_acciones_emitidas ? $planillaDjir01->num_acciones_emitidas:''}}
                          
                 <br><br></td>

                <td class="top" style="text-align: left; font-size: 10px" colspan="50"><b>Valor nominal de cada acción Bs</b><br>
                    
                    {{$planillaDjir01->valor_nominal_accion ? $planillaDjir01->valor_nominal_accion:''}}
                          
                 <br><br></td>
            </tr>
             <tr>
                <td class="top" style="text-align: left; font-size: 10px" colspan="50"><b>Fecha composición accionaria y capital social</b><br>
                    
                      {{$planillaDjir01->fmodificacion_accionaria ? $planillaDjir01->fmodificacion_accionaria:''}}
                           
                 <br><br></td>

                <td class="top" style="text-align: left; font-size: 10px" colspan="50"><b>Fecha de Inscripción</b><br>
                   
                     {{$planillaDjir01->finscripcion ? $planillaDjir01->finscripcion:''}}
                         
                 <br><br></td>
            </tr>
        </table>
    </div>

    <div class="centrar espacio-superior">
        <table class="ancho-full" border="1">
              <tr>
                <td class="top" style="background-color:#818286; color:#fff;text-align: left; font-size: 13px" colspan="100"><b>Accionistas de la Empresa Receptora de Inversión</b></td>
            </tr>
        </table>
        <table class="ancho-full" border="1">

             <thead>
                 <tr>
                    <td class="top" style="text-align: left; font-size: 10px" colspan="50"><b>Nombre o Razon Social</b><br>
                            
                    </td>
                    <td class="top" style="text-align: left; font-size: 10px" colspan="50"><b>Identificación (C.I., Pasaporte, ID)</b><br>
                                 
                    </td>
                    <td class="top" style="text-align: left; font-size: 10px" colspan="50"><b>Nacionalidad</b><br>
                               
                    </td>
                    <td class="top" style="text-align: left; font-size: 10px" colspan="50"><b>Capital Social suscrito</b><br>
                             
                    </td>
                    <td class="top" style="text-align: left; font-size: 10px" colspan="50"><b>Capital Social Pagado</b><br>
                               
                    </td>
                    <td class="top" style="text-align: left; font-size: 10px" colspan="50"><b>% de participación dentro del capital social </b><br>
                    </td>
                </tr>
            </thead>
             <tbody>
                @if(isset($accionistas))
                    @foreach ($accionistas as $key => $accionistasEmp)
                    <tr class="text-center">
                         
                        <td class="top" style="text-align: left; font-size: 10px" colspan="50">{{$accionistasEmp->razon_social_accionista ? $accionistasEmp->razon_social_accionista:''}}</td>

                        <td class="top" style="text-align: left; font-size: 10px" colspan="50">{{$accionistasEmp->identificacion_accionista ? $accionistasEmp->identificacion_accionista:''}}</td>

                        <td class="top" style="text-align: left; font-size: 10px" colspan="50">{{$accionistasEmp->nacionalidad_accionista ? $accionistasEmp->nacionalidad_accionista:''}}</td>

                         <td class="top" style="text-align: left; font-size: 10px" colspan="50">{{$accionistasEmp->capital_social_suscrito ? $accionistasEmp->capital_social_suscrito:''}}</td>

                         <td class="top" style="text-align: left; font-size: 10px" colspan="50">{{$accionistasEmp->capital_social_pagado ? $accionistasEmp->capital_social_pagado:''}}</td>

                         <td class="top" style="text-align: left; font-size: 10px" colspan="50">{{$accionistasEmp->porcentaje_participacion ? $accionistasEmp->porcentaje_participacion:''}}</td>
   
                    </tr> 
                    @endforeach
                @endif
            </tbody>
        </table> 
    </div><br>
   
   
     <div class="centrar espacio-superior">
       <table class="ancho-full" border="1">
            <tr>
                <td class="top" style="background-color:#818286; color:#fff;text-align: left; font-size: 13px" colspan="100"><b>Datos del Representante Legal o Apoderado</b></td>
            </tr>
        </table>
        <table class="ancho-full" border="1">
            <tr>
                <td class="top" style="text-align: left; font-size: 10px" colspan="50"><b>Nombres</b><br>
                    
                     {{$planillaDjir01->nombre_repre ? $planillaDjir01->nombre_repre:''}}
                         
                <br><br></td>

                <td class="top" style="text-align: left; font-size: 10px" colspan="50"><b>Apellidos</b><br>
                    
                       {{$planillaDjir01->apellido_repre ? $planillaDjir01->apellido_repre:''}}
                        
                 <br><br></td>
                <td class="top" style="text-align: left; font-size: 10px" colspan="50"><b>N° de Documento de Identidad</b><br>
                    
                      {{$planillaDjir01->numero_doc_identidad ? $planillaDjir01->numero_doc_identidad:''}}
                        
                 <br><br></td>
            </tr>
            <tr>
                <td class="top" style="text-align: left; font-size: 10px" colspan="50"><b>N° de Pasaporte</b><br>
                    
                      {{$planillaDjir01->numero_pasaporte_repre ? $planillaDjir01->numero_pasaporte_repre:''}}
                          
                 <br><br></td>

                <td class="top" style="text-align: left; font-size: 10px" colspan="50"><b>Código postal</b><br>
                    
                    {{$planillaDjir01->cod_postal_repre ? $planillaDjir01->cod_postal_repre:''}}
                          
                 <br><br></td>
                <td class="top" style="text-align: left; font-size: 10px" colspan="50"><b>N° Registro de Información Fiscal (R.I.F) del Apoderado</b><br>
                    
                    {{$planillaDjir01->rif_repre ? $planillaDjir01->rif_repre:''}}
                          
                 <br><br></td>
            </tr>
        </table>
        <table class="ancho-full" border="1">
            <tr>
                <td class="top" style="text-align: left; font-size: 10px" colspan="50"><b>Teléfono 1</b><br>
                    
                      {{$planillaDjir01->telefono_1 ? $planillaDjir01->telefono_1:''}}
                           
                 <br><br></td>

                <td class="top" style="text-align: left; font-size: 10px" colspan="50"><b>Teléfono 2</b><br>
                   
                     {{$planillaDjir01->telefono_2 ? $planillaDjir01->telefono_2:''}}
                         
                 <br><br></td>
                  <td class="top" style="text-align: left; font-size: 10px" colspan="50"><b>Correo electrónico</b><br>
                   
                     {{$planillaDjir01->correo_repre ? $planillaDjir01->correo_repre:''}}}
                         
                 <br><br></td>
                  <td class="top" style="text-align: left; font-size: 10px" colspan="50"><b>Dirección donde recibirá las notificaciones</b><br>
                   
                     {{$planillaDjir01->direccion_repre ? $planillaDjir01->direccion_repre:''}}
                         
                 <br><br></td>
            </tr>
        </table>
    </div>

<br><br>
	 <div class="centrar espacio-superior">
        <table class="ancho-full">
            <tr>
                <td class="top" style="text-align: center; font-size: 13px; text-transform: uppercase" colspan="100">
                Representante legal n° de rif: <b>{{ $solicitudDeclaracion->rPlanillaDjir01 ? $solicitudDeclaracion->rPlanillaDjir01->numero_doc_identidad:''}}</b><br>
                Firma del contribuyente o Representante Legal <br><br><br>



                _______________________________________________________
            </td>
            </tr>
        </table>
    </div>

</body>
</html>