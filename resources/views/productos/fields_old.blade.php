

<div class="row">

<h3 align="center">{{$descripcion}}</h3>

</div>

 <div>
     <span style="font-size:17px">Monto FOB registrado: <b>{{$monto}}</b></span>
     <p class="alert alert-warning"><span style="font-size:17px">Aviso: Las sumatorias de los valores FOB no debe ser mayor al monto FOB registrado. Si no se cumple con esta condición no podrá registrar los productos.</span></p>
  </div>

<div class="container">

  <table class="table table-bordered" id="fields">

     @if(isset($productos))  

     <td><button  name="add" type="button" id="add" value="add more" class="btn btn-success" >Agregar</button></td></tr>

   @endif

    <tr>
    <th>Código Arancelario </th>
    <th>Descripción Arancelaria</th>
    <th>Descripción Comercial</th>
    <th>Cantidad </th>
    <th>Precio </th>
    <th>Monto FOB</th>
    <th>Unidad de Medida</th>
    

  </tr>

 @if(!isset($productos))

	  <tr id="row0">

	  <td>{!!Form::text('codigo_arancel[]',null,['class'=>'form-control','data-toggle'=>"modal",'data-target'=>"#modal",'id'=>'cod_arancel','onkeypress'=>'return soloNumerosDouble(event)'])!!}
	  </td>
	  <td>{!!Form::text('descripcion_arancelaria[]',null,['class'=>'form-control','id'=>'arancel_descrip','onkeypress'=>'return soloLetras(event)'])!!}</td>
	  <td>{!!Form::text('descripcion_comercial[]',null,['class'=>'form-control','onkeypress'=>'return soloLetras(event)'])!!}</td>
	  <td>{!!Form::text('cantidad_producto[]',null,['class'=>'form-control format','id'=>'cantidad_producto','onInput'=>"calcularMonto('row0','cantidad_producto','precio_producto','montoFob')"])!!}</td>
	  <td>{!!Form::text('precio_producto[]',null,['class'=>'form-control format','id'=>'precio_producto','onInput'=>"calcularMonto('row0','cantidad_producto','precio_producto','montoFob')"])!!}</td>

	  <td>{!!Form::text('valor_fob[]',null,['class'=>'form-control','readOnly'=>'readOnly','id'=>'montoFob'])!!}</td>
	  <td>{!!Form::select('unidad_medida_id[]',$medidas,null,['class'=>'form-control','id'=>'unidad_medida_id','placeholder'=>'Seleccione'])!!}</td>

	  <td><button  name="add" type="button" id="add" value="add more" class="btn btn-success">Agregar</button></td>
</tr>

  @else

    @foreach($productos as $key =>$valor)

       @php $fila=$key+1; @endphp


		  <tr id="row{{$key+1}}">

           
    
		  <td>{!!Form::text('codigo_arancel[]',(isset($valor->codigo_arancel) ? $valor->codigo_arancel :" "),['class'=>'form-control','data-toggle'=>"modal",'data-target'=>"#modal",'id'=>'cod_arancel','onkeypress'=>'return soloNumerosDouble(event)'])!!}
		  </td>

		 <td>{!!Form::text('descripcion_arancelaria[]',$valor->descripcion_arancelaria,['class'=>'form-control','id'=>'arancel_descrip','onkeypress'=>'return soloLetras(event)'])!!}</td>

		  <td>
		  {!!Form::hidden('gen_factura_id',$valor->gen_factura_id)!!}
		  {!!Form::text('descripcion_comercial[]',$valor->descripcion_comercial,['class'=>'form-control','onkeypress'=>'return soloLetras(event)'])!!}</td>
		  <td>{!!Form::text('cantidad_producto[]',$valor->cantidad_producto
,['class'=>'form-control format','id'=>'cantidad_producto','onInput'=>"calcularMonto('row$fila','cantidad_producto','precio_producto','x')"])!!}</td>

		  <td>{!!Form::text('precio_producto[]',$valor->precio_producto,['class'=>'form-control format','id'=>'precio_producto','onInput'=>"calcularMonto('row$fila','cantidad_producto','precio_producto','x')"])!!}</td>

		  <td>{!!Form::text('monto_total[]',$valor->monto_total,['class'=>'form-control','readOnly'=>'readOnly','id'=>'montoFob'])!!}</td>

		  <td>{!!Form::select('unidad_medida_id[]',$medidas,$valor->unidad_medida_id,['class'=>'form-control','placeholder'=>'Seleccione','id'=>'unidad_medida_id'])!!}</td>

		 

		 <td>
		   {!!Form::hidden('id[]',$valor->id)!!}
		   <button  name="remove" type="button" id="{{$key+1}}" value="" class="btn btn-danger btn-remove">x</button></td>

		</tr><br>

   @endforeach

@endif


  </table>



  @if(isset($id))


  <div id="monto_data" data-monto={{$monto}}></div>

	<br><div class="col-md-12" align="center">

	 <a href="../../../exportador/factura/{{$id}}/edit"><input type="button" name="submit" class="btn btn-success" value="Regresar"/></a>

	<input type="submit" name="submit" id="id_boton" class="btn btn-success" value="Enviar"/>

	 @else

    <div class="col-md-12" align="center">
	 <input type="submit" name="submit" id="id_boton" class="btn btn-info"  value="Enviar"/>
	</div>


 @endif

 </div>

<br><br>

<div class="container">
<span><b>Nota:</b> Los campos cantidad y precio usan como separador decimal el punto (.) <br>
<b>Ej: 3985.54 </b></span>
</div>



<div id="monto_data" data-monto={{$monto}}></div>




@component('modal_dialogo',['arancel'=>$arancel])

@slot('header') <span>Código Arancelario</span> @endslot

@slot('body')  @endslot

@slot('footer') <span>Enviar</span> @endslot

@endcomponent 

{{Form::close()}}


@push('productos')

 $(document).on('click','.btn-remove',function(){

               var id_boton= $(this).attr("id");
               var id=$(this).prev().val();

           
            
               if(id > 1){

                 swal({
              
                     title: "¿Eliminar?",
                     text: "¿Está seguro de eliminar este Registro?",
                     type: "warning",
                     showCancelButton: true,
                     confirmButtonColor: "#DD6B55",
                     confirmButtonText: "Si!, Borrarlo!",
                     cancelButtonText: "No, Cancelar!",
                     closeOnConfirm: false,
                     closeOnCancel: false,
                     showLoaderOnConfirm: true
                     },

    function(isConfirm){
         if (isConfirm) {

              $.ajax({

               'type':'get',
               'url':'eliminar',
               'data':{ 'id':id},
               success: function(data){

                $('#row'+id_boton).remove();
                if(data==1){

                   swal("Eliminación exitosa","Producto Eliminado ", "success");

                  }

                 }

             });


         }else{

              swal("Cancelado", "No se ha procesado la eliminación", "warning");

            }

        });


        }else{

            $('#row'+id_boton).remove();
         }

   });

  
  


 @endpush



