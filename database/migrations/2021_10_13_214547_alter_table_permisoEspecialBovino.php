<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTablePermisoEspecialBovino extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
          Schema::table('permiso_especial_bovino', function (Blueprint $table) {
            //
            $table->string('num_autorizacion',255)->nullable()->after('gen_usuario_id'); 
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
          Schema::table('permiso_especial_bovino', function (Blueprint $table) {
            //
            $table->dropColumn('num_autorizacion');
        });
    }
}
