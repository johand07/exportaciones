<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GenDocResguardo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gen_doc_resguardo', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('gen_sol_resguardo_aduanero_id')->index()->nullable();
            $table->foreign('gen_sol_resguardo_aduanero_id')->references('id')->on('gen_sol_resguardo_aduanero');
            $table->string('file_1', 255)->nullable();
            $table->string('file_2', 255)->nullable();
            $table->string('file_3', 255)->nullable();
            $table->string('file_4', 255)->nullable();
            $table->string('file_5', 255)->nullable();
            $table->string('file_6', 255)->nullable();
            $table->string('file_7', 255)->nullable();
            $table->string('file_8', 255)->nullable();
            $table->string('file_9', 255)->nullable();
            $table->string('file_10', 255)->nullable();
            $table->string('file_11', 255)->nullable();
            $table->string('file_12', 255)->nullable();
            $table->string('file_13', 255)->nullable();
            $table->string('file_14', 255)->nullable();
            $table->string('file_15', 255)->nullable();
            $table->string('file_16', 255)->nullable();
            $table->string('file_17', 255)->nullable();
            $table->string('file_18', 255)->nullable();
            $table->string('file_19', 255)->nullable();
            $table->string('file_20', 255)->nullable();
            $table->string('file_21', 255)->nullable();
            $table->string('file_22', 255)->nullable();
            $table->string('file_23', 255)->nullable();
            $table->string('file_24', 255)->nullable();
            $table->string('file_25', 255)->nullable();
            $table->string('file_26', 255)->nullable();
            $table->string('file_27', 255)->nullable();
            $table->boolean('bactivo')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gen_doc_resguardo');
    }
}
