@extends('templates/layoutlte')
@section('content')

<div class="panels">
    <div class="row">
      <div class="col-md-1">
      <a href="{{url('exportador/Dudas/create')}}"class="btn btn-primary">
          <span class="glyphicon glyphicon-file" title="Agregar" aria-hidden="true"></span>
          Nueva Solicitud Dudas
          </a>
          
      </div>
      <div class="col-md-1">
      </div>
      <div class="col-md-3"></div>
    </div>
    <hr>

        <table id="listaAgenteAduanal" class="table table-striped table-bordered" style="width:100%">
            <thead>
                <tr>
                    <th class="col-md-1 text-center">Nº</th>
                    <th class="col-md-2 text-center">Tipo</th>
                    <th class="col-md-3 text-center">Email Solicitante</th>
                    <th class="col-md-3 text-center">Fecha de Solicitud</th>
                    <th class="col-md-1 text-center">Estatus</th>
                    <th class="col-md-2 text-center">Analista</th>
                    <th class="col-md-2 text-center">Acciones</th>
                </tr>
            </thead>
            <tbody>
                
                @foreach($solicitudes as $key=> $solicitud)

                <tr>

                    <td>{{$solicitud['id']}}</td>
                    <td>{{$solicitud['motivo_de_contacto']}}</td>
                    {{-- <td>@if(!is_null($solicitud->gen_status_id)){{$solicitud->status->nombre_status}}@endif</td> --}}
                    <td>{{$solicitud['email']}}</td>
                    <td>{{$solicitud['created_at']}}</td>
                    <td>{{$solicitud['nombre_status']}}</td>
                    <td>@if(!is_null($solicitud['gen_usuario_atencion'])){{$solicitud['gen_usuario_atencion']}}@endif</td>
                    <td class="col-md-2 text-center">
                        @if($solicitud['status'] == 10)
                            <a href="{{route('Dudas.edit',$solicitud['id'])}}" title="Ver" class="btn btn-primary btn-md">Ver</a>
                        @else
                            <a href="{{route('Dudas.edit',$solicitud['id'])}}" title="Ver" class="btn btn-primary btn-sm"><i class="glyphicon glyphicon-edit "></i></a>
                        @endif
                        @if($solicitud['status'] == 9)
                            <a href="#" class="btn btn-danger btn-sm" title="Eliminar" onclick="return elimDudas('/exportador/Dudas/delete/{{$solicitud['id']}}', {{$solicitud['id']}})">Eliminar <i class="glyphicon glyphicon-trash"></i></a>
                        @endif
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    </div>
</div>

<script>
    //funcion para eliminar Duda************************************
function elimDudas(route, id){
    console.log(id);  
    console.log(route);
  
  swal({
      title: "¿Eliminar?",
      text: "¿Está seguro de eliminar este Registro?",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "¡Si!, Borrarlo!",
      cancelButtonText: "¡No, Cancelar!",
      closeOnConfirm: false,
      closeOnCancel: false,
      showLoaderOnConfirm: true
    },
    function(isConfirm){
      if (isConfirm) {

        $.ajax({
          url: route,
          type: 'POST',
          dataType: "JSON",
          data: {
                "id": id,
                _token: "{{ csrf_token() }}"
          },
          
          success : function(response){
            console.log(response);
            swal("¡Hecho!", "¡Eliminado exitosamente!", "success");
            $("#listaAgenteAduanal").load("#listaAgenteAduanal");
            window.location.href = "/exportador/Dudas";
          },
          error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr);
            console.log(ajaxOptions);
            console.log(thrownError);
            swal("¡Error eliminando!", "Por favor, intente de nuevo", "error");
          }
        });
      } else {
        swal("Cancelado", "No se ha procesado la eliminación", "warning");
      }
    }
  );
  
}
</script>

@stop
