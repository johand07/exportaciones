<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CatPreguntaSeg extends Model
{
    protected $table = 'cat_preguntas_seg';

    /* protected $fillable = [

           
    ];*/

    public function usuario()
    {
        return $this->belongsTo('App\Models\GenUsuario');
    }
    
}
