<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DocAdicionalDjo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('doc_adicional_djo', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('gen_declaracion_jo_id')->index()->nullable();
            $table->foreign('gen_declaracion_jo_id')->references('id')->on('gen_declaracion_jo');
            $table->string('file_1',300)->nullable();
            $table->string('file_2',300)->nullable();
            $table->string('file_3',300)->nullable();
            $table->string('file_4',300)->nullable();
            $table->string('file_5',300)->nullable();
            $table->string('file_6',300)->nullable();
            $table->integer('estado_observacion')->nullable();
            $table->string('descrip_observacion',255)->nullable();
            $table->boolean('bactivo')->default(1);
          $table->timestamps();
        });
          
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('doc_adicional_djo');
    }
}
