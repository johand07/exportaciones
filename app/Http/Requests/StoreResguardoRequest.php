<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreResguardoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'file_1'                        =>'required',
            'file_2'                        =>'required',
            'file_3'                        =>'required',
            'file_4'                        =>'required',
            'file_5'                        =>'required',
            'file_6'                        =>'required',
            'file_7'                        =>'required',
            'file_8'                        =>'required',
            'file_9'                        =>'required',
            'file_10'                       =>'required',
            'file_11'                       =>'required',
            'file_12'                       =>'required',
            'file_13'                       =>'required',
            'file_14'                       =>'required',
            'file_15'                       =>'required',
            'file_16'                       =>'required',
            'file_17'                       =>'required',
            'file_18'                       =>'required',
            'file_19'                       =>'required',
            'file_20'                       =>'required',
            //'tlf_1'                         =>'required',
            //'tlf_2'                         =>'required',
            //'tlf_3'                         =>'required',
            //'ubicacion_georeferencial'      =>'required',
        ];
    }
}
