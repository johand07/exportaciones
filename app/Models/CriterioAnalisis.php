<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CriterioAnalisis extends Model
{
    protected $table = 'criterio_analisis';
    protected $fillable = [

           'analisis_calificacion_id',
           'det_declaracion_produc_id',
           'arancel_mer',
           'arancel_nan',
           'bol',
           'col',
           'ecu',
           'per',
           'cub',
           'ald',
           'arg',
           'bra',
           'par',
           'uru',
           'usa',
           'ue',  
           'cnd',
           'tp',
           'bactivo',
          
    ];

    	public function AnalisisCalifi() {

   		return $this->hasOne('App\Models\AnalisisCalificacion','id','analisis_calificacion_id');

    }
   
   		public function declaracionProduc() {

   		return $this->hasOne('App\Models\DetDeclaracionProduc','id','det_declaracion_produc_id');

    }

}
