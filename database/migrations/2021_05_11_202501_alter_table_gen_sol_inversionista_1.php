<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableGenSolInversionista1 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('gen_sol_inversionista', function (Blueprint $table) {
            //
            $table->unsignedInteger('cat_tipo_usuario_id')->index()->nullable()->after('cat_tipo_visa_id');
            $table->foreign('cat_tipo_usuario_id')->references('id')->on('cat_tipo_usuario');
            $table->string('n_doc_identidad')->nullable()->after('observacion_doc_incomp'); 
            $table->string('tipo_inversion')->nullable()->after('n_doc_identidad');
            $table->double('monto_inversion')->nullable()->after('tipo_inversion');
            $table->string('descrip_proyec_inver')->nullable()->after('monto_inversion');
            $table->string('actividad_eco_emp')->nullable()->after('descrip_proyec_inver');
            $table->string('correo_emp_recep')->nullable()->after('actividad_eco_emp');
            $table->string('pag_web_emp_recep')->nullable()->after('correo_emp_recep');
            $table->string('nombre_repre_legal')->nullable()->after('pag_web_emp_recep');
            $table->string('apellido_repre_legal')->nullable()->after('nombre_repre_legal');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::table('gen_sol_inversionista', function (Blueprint $table) {
            //
            $table->dropColumn('cat_tipo_usuario_id');
            $table->dropColumn('n_doc_identidad');
            $table->dropColumn('tipo_inversion');
            $table->dropColumn('monto_inversion');
            $table->dropColumn('descrip_proyec_inver');
            $table->dropColumn('actividad_eco_emp');
            $table->dropColumn('correo_emp_recep');
            $table->dropColumn('pag_web_emp_recep');
            $table->dropColumn('nombre_repre_legal');
            $table->dropColumn('apellido_repre_legal');
        });
    }
}
