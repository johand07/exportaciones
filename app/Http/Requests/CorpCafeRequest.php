<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CorpCafeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'direccion_notif' => 'required',
            'clave_pais' => 'required',
            'gen_aduana_salida_id' => 'required',
            'num_serie' => 'required',
            'pais_destino' => 'required',
            'pais_productor' => 'required',
            'pais_transbordo' => 'required',
            'fexportacion' => 'required',
            'nombre_transporte' => 'required',
            'marca_ident' => 'required',
            'cat_cargados_id' => 'required',
            'peso_neto' => 'required',
            'gen_unidad_medida_id' => 'required',
            'cat_tipo_cafe_id' => 'required',
            'cat_metodo_elab_id' => 'required',
            'norma_calidad_cafe' => 'required',
            'caract_especiales' => 'required',
            'codio_sist_sa' => 'required',
            'valor_fob' => 'required',
            'gen_divisa_id' => 'required',
            'codigo_arancelario' => 'required',
            'info_adicional' => 'required',
        ];
    }
}
