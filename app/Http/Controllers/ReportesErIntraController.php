<?php

namespace App\Http\Controllers;

use App\Models\GenSolicitud;
use App\Models\GenUsuario;
use App\Models\DetUsuario;
use App\Models\GenDVDSolicitud;
use Illuminate\Http\Request;
use Session;
use Auth;
use Alert;
use PDF;
use Illuminate\Routing\Route;
use Illuminate\Http\Response;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\DB;

class ReportesErIntraController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $titulo='Reportes Exportación Realizada (ER)';
        $descripcion='Detalle de solicitud ER';
        $fecha=date("Y-m-d");
        $desde=$request->desde;
        $hasta=$request->hasta;
        $mes=date('m');
        $anio=date('Y');
        // dd($anio);
        // $desde =  $anio."/01/01";
        // $hasta = date('Y/m/d');
        /*$timestamp = strtotime($originalDate); 
        $newDate = date("d-m-Y", $timestamp );
        //dd($newDate);
        $numero = 1002002.365;
        $format=number_format($numero, 2, ",", ".");*/
        //dd($format);
       
        $sol_er_intra=DB::table('det_usuario as det')
        ->select('sol.id as Num_Solicitud',
                'sol.fsolicitud as Fecha_Solicitud',
                'gte.d_tipo_empresa as Tipo_Empresa',
                'det.rif',
                'det.razon_social',
            /**/'est.estado',
                'dua.numero_dua as DUA',
                'reg.nombre_regimen as Regimen',
                'st.nombre_status as Estatus',
                'tsol.solicitud as tipo_Exportacion',
                'tpc.nombre_convenio as Convenio',
                'sol.fstatus as Fecha_Estatus',
                'dua.fembarque as Fecha_Embarque',
            /**/'adu.daduana as Puerto_Embarque',
            /**/'dua.lugar_salida as Salida',
                'dua.lugar_llegada as Puerto_Llegada',
                'fa.fecha_Factura as Fecha_Factura',
                'fa.numero_factura as Num_Factura',
                'gdiv.ddivisa_abr as Divisa',
                'fr.descrip_pago as Forma_Pago',
                'fa.plazo_pago as Plazo_Pago',
                'prod.codigo_arancel',
            /**/'prod.descripcion_arancelaria',
                'prod.descripcion_comercial as productos',
                'prod.cantidad_producto',
                'prod.precio_producto as Precio_Unitario ',
                'prod.monto_total as Monto_Total',
                'prod.unidad_medida_id as Codigo_Unidad_Medida',
                'med.dunidad as Unidad_Medida',
                'gst.descripcion_sector as Sector_Rubro',
                'dua.aduana_llegada as Pais_Destino',
                'fa.monto_fob  as Monto_Fob_Factura',
                'sol.monto_solicitud as Monto_Fob_Solicitud',
                'fa.monto_flete as Monto_Flete',
                'fa.monto_seguro as Monto_Seguro',
                'fa.otro_monto as Otro_Monto'
                )
        ->join('gen_solicitud as sol', 'det.gen_usuario_id', '=', 'sol.gen_usuario_id')
        ->join('factura_solicitud as fac', 'sol.id', '=', 'fac.gen_solicitud_id')
        ->join('gen_factura as fa', 'fac.gen_factura_id', '=', 'fa.id')
        ->join('det_prod_factura as prod', 'prod.gen_factura_id', '=', 'fa.id')
        ->join('gen_dua as dua', 'fa.gen_dua_id', '=', 'dua.id')
        ->join('gen_consignatario as cons', 'dua.gen_consignatario_id', '=', 'cons.id')
        ->join('gen_status as st', 'sol.gen_status_id', '=', 'st.id')
        ->join('tipo_solicitud as tsol', 'sol.tipo_solicitud_id', '=', 'tsol.id')
        ->join('forma_pago as fr', 'fa.forma_pago_id', '=', 'fr.id')
        ->join('gen_aduana_salida as adu', 'dua.gen_aduana_salida_id', '=', 'adu.id')
        ->join('estado as est', 'det.estado_id', '=', 'est.id')
        ->join('gen_unidad_medida as med', 'prod.unidad_medida_id', '=', 'med.id')
        ->join('financiamiento_solicitud as fin', 'sol.id', '=', 'fin.gen_solicitud_id')
        ->join('gen_tipo_empresa as gte', 'det.gen_tipo_empresa_id', '=', 'gte.id')
        ->join('financiamiento_solicitud as fisol', 'sol.id', '=', 'fisol.gen_solicitud_id')
        ->join('cat_regimen_export as reg', 'fin.cat_regimen_export_id', '=', 'reg.id')
        ->join('gen_divisa as gdiv', 'fa.gen_divisa_id', '=', 'gdiv.id')
        ->join('gen_actividad_eco as act', 'act.id', '=', 'det.gen_actividad_eco_id')
        ->join('gen_sector as gst', 'act.gen_sector_id', '=', 'gst.id')
        ->join('cat_tipo_convenio as tpc', 'cons.cat_tipo_convenio_id', '=', 'tpc.id')
        ->whereNotIn('det.gen_usuario_id',[1,2,3,4,5,11,12,2430,2268])

        // ->whereMonth('sol.created_at',$mes)
        ->whereYear('sol.fsolicitud',$anio)
        ->where('sol.bactivo',1)
        ->orderBy('sol.fsolicitud', 'desc')
        //->whereBetween('sol.fsolicitud',[$desde,$hasta])
        ->distinct()
        ->get();

        
        //dd($sol_er_intra);

    return view('ReportesErIntra.index',compact('titulo','descripcion','desde','hasta','sol_er_intra'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         //dd($request->all());
        $fecha=date("Y-m-d H");
        $fecha_desde=$request->desde.' 00:00:00';
        $fecha_hasta=$request->hasta.' 23:59:59';
        $desde=$request->desde;
        $hasta=$request->hasta;
        
        $titulo = 'Reporte de Exportacion Realizada (ER) ';
        $descripcion = 'Solicitudes Er';
        $sol_er_intra=DB::table('det_usuario as det')
        ->select('sol.id as Num_Solicitud',
                'sol.fsolicitud as Fecha_Solicitud',
                'gte.d_tipo_empresa as Tipo_Empresa',
                'det.rif',
                'det.razon_social',
            /**/'est.estado',
                'dua.numero_dua as DUA',
                'reg.nombre_regimen as Regimen',
                'st.nombre_status as Estatus',
                'tsol.solicitud as tipo_Exportacion',
                'tpc.nombre_convenio as Convenio',
                'sol.fstatus as Fecha_Estatus',
                'dua.fembarque as Fecha_Embarque',
            /**/'adu.daduana as Puerto_Embarque',
            /**/'dua.lugar_salida as Salida',
                'dua.lugar_llegada as Puerto_Llegada',
                'fa.fecha_Factura as Fecha_Factura',
                'fa.numero_factura as Num_Factura',
                'gdiv.ddivisa_abr as Divisa',
                'fr.descrip_pago as Forma_Pago',
                'fa.plazo_pago as Plazo_Pago',
                'prod.codigo_arancel',
            /**/'prod.descripcion_arancelaria',
                'prod.descripcion_comercial as productos',
                'prod.cantidad_producto',
                'prod.precio_producto as Precio_Unitario ',
                'prod.monto_total as Monto_Total',
                'prod.unidad_medida_id as Codigo_Unidad_Medida',
                'med.dunidad as Unidad_Medida',
                'gst.descripcion_sector as Sector_Rubro',
                'dua.aduana_llegada as Pais_Destino',
                'fa.monto_fob  as Monto_Fob_Factura',
                'sol.monto_solicitud as Monto_Fob_Solicitud',
                'fa.monto_flete as Monto_Flete',
                'fa.monto_seguro as Monto_Seguro',
                'fa.otro_monto as Otro_Monto'
                )
        ->join('gen_solicitud as sol', 'det.gen_usuario_id', '=', 'sol.gen_usuario_id')
        ->join('factura_solicitud as fac', 'sol.id', '=', 'fac.gen_solicitud_id')
        ->join('gen_factura as fa', 'fac.gen_factura_id', '=', 'fa.id')
        ->join('det_prod_factura as prod', 'prod.gen_factura_id', '=', 'fa.id')
        ->join('gen_dua as dua', 'fa.gen_dua_id', '=', 'dua.id')
        ->join('gen_consignatario as cons', 'dua.gen_consignatario_id', '=', 'cons.id')
        ->join('gen_status as st', 'sol.gen_status_id', '=', 'st.id')
        ->join('tipo_solicitud as tsol', 'sol.tipo_solicitud_id', '=', 'tsol.id')
        ->join('forma_pago as fr', 'fa.forma_pago_id', '=', 'fr.id')
        ->join('gen_aduana_salida as adu', 'dua.gen_aduana_salida_id', '=', 'adu.id')
        ->join('estado as est', 'det.estado_id', '=', 'est.id')
        ->join('gen_unidad_medida as med', 'prod.unidad_medida_id', '=', 'med.id')
        ->join('financiamiento_solicitud as fin', 'sol.id', '=', 'fin.gen_solicitud_id')
        ->join('gen_tipo_empresa as gte', 'det.gen_tipo_empresa_id', '=', 'gte.id')
        ->join('financiamiento_solicitud as fisol', 'sol.id', '=', 'fisol.gen_solicitud_id')
        ->join('cat_regimen_export as reg', 'fin.cat_regimen_export_id', '=', 'reg.id')
        ->join('gen_divisa as gdiv', 'fa.gen_divisa_id', '=', 'gdiv.id')
        ->join('gen_actividad_eco as act', 'act.id', '=', 'det.gen_actividad_eco_id')
        ->join('gen_sector as gst', 'act.gen_sector_id', '=', 'gst.id')
        ->join('cat_tipo_convenio as tpc', 'cons.cat_tipo_convenio_id', '=', 'tpc.id')
        ->whereNotIn('det.gen_usuario_id',[1,2,3,4,5,11,12,2430,2268])
        ->where('sol.bactivo',1)
        ->whereBetween('sol.fsolicitud',[$desde,$hasta])
        ->orderBy('sol.fsolicitud', 'desc')
        ->distinct()
        ->get();

        //dd($sol_er);


    return view('ReportesErIntra.index',compact('titulo','descripcion','desde','hasta','sol_er_intra'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\GenSolicitud  $genSolicitud
     * @return \Illuminate\Http\Response
     */
    public function show(GenSolicitud $genSolicitud)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\GenSolicitud  $genSolicitud
     * @return \Illuminate\Http\Response
     */
    public function edit(GenSolicitud $genSolicitud)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\GenSolicitud  $genSolicitud
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, GenSolicitud $genSolicitud)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\GenSolicitud  $genSolicitud
     * @return \Illuminate\Http\Response
     */
    public function destroy(GenSolicitud $genSolicitud)
    {
        //
    }


     public function ExcelReportesEr(Request $request, DetUsuario $detUsuario)
    {
        $desde=$request->desde;
        $hasta=$request->hasta;
        //$mes=date('m');

        //dd($request->all());
         $sol_er_intra=DB::table('det_usuario as det')
        ->select('sol.id as Num_Solicitud',
                'sol.fsolicitud as Fecha_Solicitud',
                'gte.d_tipo_empresa as Tipo_Empresa',
                'det.rif',
                'det.razon_social',
            /**/'est.estado as Estados',
                'dua.numero_dua as DUA',
                'reg.nombre_regimen as Regimen',
                'st.nombre_status as Estatus',
                'tsol.solicitud as tipo_Exportacion',
                'tpc.nombre_convenio as Convenio',
                'sol.fstatus as Fecha_Estatus',
                'dua.fembarque as Fecha_Embarque',
            /**/'adu.daduana as Puerto_Embarque',
            /**/'dua.lugar_salida as Salida',
                'dua.lugar_llegada as Puerto_Llegada',
                'fa.fecha_Factura as Fecha_Factura',
                'fa.numero_factura as Num_Factura',
                'gdiv.ddivisa_abr as Divisa',
                'fr.descrip_pago as Forma_Pago',
                'fa.plazo_pago as Plazo_Pago',
                'prod.codigo_arancel as Codigo_Arancelario',
            /**/'prod.descripcion_arancelaria as Descripcion_arancelaria',
                'prod.descripcion_comercial as productos',
                'prod.cantidad_producto as Cantidad_Productos',
                'prod.precio_producto as Precio_Unitario',
                'prod.monto_total as Monto_Total',
                'prod.unidad_medida_id as Codigo_Unidad_Medida',
                'med.dunidad as Unidad_Medida',
                'gst.descripcion_sector as Sector_Rubro',
                'dua.aduana_llegada as Pais_Destino',
                'fa.monto_fob  as Monto_Fob_Factura',
                'sol.monto_solicitud as Monto_Fob_Solicitud',
                'fa.monto_flete as Monto_Flete',
                'fa.monto_seguro as Monto_Seguro',
                'fa.otro_monto as Otro_Monto'
                )
        ->join('gen_solicitud as sol', 'det.gen_usuario_id', '=', 'sol.gen_usuario_id')
        ->join('factura_solicitud as fac', 'sol.id', '=', 'fac.gen_solicitud_id')
        ->join('gen_factura as fa', 'fac.gen_factura_id', '=', 'fa.id')
        ->join('det_prod_factura as prod', 'prod.gen_factura_id', '=', 'fa.id')
        ->join('gen_dua as dua', 'fa.gen_dua_id', '=', 'dua.id')
        ->join('gen_consignatario as cons', 'dua.gen_consignatario_id', '=', 'cons.id')
        ->join('gen_status as st', 'sol.gen_status_id', '=', 'st.id')
        ->join('tipo_solicitud as tsol', 'sol.tipo_solicitud_id', '=', 'tsol.id')
        ->join('forma_pago as fr', 'fa.forma_pago_id', '=', 'fr.id')
        ->join('gen_aduana_salida as adu', 'dua.gen_aduana_salida_id', '=', 'adu.id')
        ->join('estado as est', 'det.estado_id', '=', 'est.id')
        ->join('gen_unidad_medida as med', 'prod.unidad_medida_id', '=', 'med.id')
        ->join('financiamiento_solicitud as fin', 'sol.id', '=', 'fin.gen_solicitud_id')
        ->join('gen_tipo_empresa as gte', 'det.gen_tipo_empresa_id', '=', 'gte.id')
        ->join('financiamiento_solicitud as fisol', 'sol.id', '=', 'fisol.gen_solicitud_id')
        ->join('cat_regimen_export as reg', 'fin.cat_regimen_export_id', '=', 'reg.id')
        ->join('gen_divisa as gdiv', 'fa.gen_divisa_id', '=', 'gdiv.id')
        ->join('gen_actividad_eco as act', 'act.id', '=', 'det.gen_actividad_eco_id')
        ->join('gen_sector as gst', 'act.gen_sector_id', '=', 'gst.id')
        ->join('cat_tipo_convenio as tpc', 'cons.cat_tipo_convenio_id', '=', 'tpc.id')
        ->whereNotIn('det.gen_usuario_id',[1,2,3,4,5,11,12,2430,2268])
        ->where('sol.bactivo',1)
        //->whereMonth('sol.created_at',$mes)
        ->whereBetween('sol.fsolicitud',[$desde,$hasta])
        ->orderBy('sol.fsolicitud', 'desc')
        ->distinct()
        ->get();

        //dd($sol_er_intra);

        $originalDate = "2022-06-02";
        $timestamp = strtotime($originalDate); 
        $newDate = date("d-m-Y", $timestamp );
        //dd($newDate);
        $numero = 1002002.365;
        $format=number_format($numero, 2, ",", ".");
        //dd($format);
        
        /**Estructura de excel***/
        $name="Reportes de Solicitudes ER";
        //dd($sol_er_intra);
        $myFile   = Excel::create('Reportes de Solicitudes ER',function($excel)use($sol_er_intra){ // Nombre princial del archivo
            $excel->sheet('Solicitud ER',function($sheet)use ($sol_er_intra){ // se crea la hoja con el nombre de datos
               // dd($sol_er_intra);
                //header
                $sheet->setStyle(array(
                'font' => array(
                    'name' =>  'itálico',
                    'size' =>  12,
                    'bold' =>  true
                )
                ));

                $sheet->mergeCells('A1:H1'); // union de celdas desde A a F
                $sheet->row(1,['Reportes de Solicitudes ER']);
                $sheet->row(2,[]);
                $sheet->row(3,['Numero Solicitud','Fecha de Solicitud','Tipo de Empresa','Rif','Razon Social','Estado','Dua','Regimen','Estatus','Tipo de Exportacion','Convenio','Fecha de Estatus','Fecha de Embarque','Puerto Embarque/Salida','Puerto Llegada','Fecha Factura','Número Factura','Divisa','Forma de Pago','Plazo de Pago','Codigo Arancelario','Descripcion Arancelaria','Productos','Cantidad de Productos','Precio Unitario','Monto Total','Codigo Unidad Medida','Unidad de Medida','Sector de Rubro','Pais Destino','Monto Fob Factura','Monto Fob Solicitud','Monto Flete','Monto Seguro','Otro Monto']);

                //$sheet->fromArray($sol_er_intra, null, 'A4', null, false);
                // consulta


                
               //dd($sol_er_intra);
                //recorrido
                foreach ($sol_er_intra as $emp) {

                $forma_sol=strtotime($emp->Fecha_Solicitud); 
                $fechasol = date("d-m-Y", $forma_sol);

                $forma_status=strtotime($emp->Fecha_Estatus); 
                $fechastatus = date("d-m-Y", $forma_status );

                $forma_embarque=strtotime($emp->Fecha_Embarque); 
                $fechaembarque = date("d-m-Y", $forma_embarque );

                $forma_factura=strtotime($emp->Fecha_Factura); 
                $fechafactura = date("d-m-Y", $forma_factura );




                   $row=[]; // un arreglo vacio que se va a llenar con las posicion 
                   $row[0]=$emp->Num_Solicitud; 
                   $row[1]=$fechasol; 
                   $row[2]=$emp->Tipo_Empresa; 
                   $row[3]=$emp->rif; 
                   $row[4]=$emp->razon_social; 
                   $row[5]=$emp->Estados; 
                   $row[6]=$emp->DUA; 
                   $row[7]=$emp->Regimen; 
                   $row[8]=$emp->Estatus; 
                   $row[9]=$emp->tipo_Exportacion;//si exporta el excel 
                   $row[10]=$emp->Convenio; 
                   $row[11]=$fechastatus; 
                   $row[12]=$fechaembarque; 
                   $row[13]=$emp->Puerto_Embarque.'/'.$emp->Salida;  
                   $row[14]=$emp->Puerto_Llegada; 
                   $row[15]=$fechafactura; 
                   $row[16]=$emp->Num_Factura; 
                   $row[17]=$emp->Divisa; 
                   $row[18]=$emp->Forma_Pago; 
                   $row[19]=$emp->Plazo_Pago; 
                   $row[20]=$emp->Codigo_Arancelario; 
                   $row[21]=$emp->Descripcion_arancelaria; 
                   $row[22]=$emp->productos; 
                   $row[23]=$emp->Cantidad_Productos; 
                   $row[24]=$emp->Precio_Unitario; 
                   $row[25]=number_format($emp->Monto_Total, 2, ",", ".");//Formatear los montos con puntos y decimales
                   $row[26]=$emp->Codigo_Unidad_Medida; 
                   $row[27]=$emp->Unidad_Medida; 
                   $row[28]=$emp->Sector_Rubro; 
                   $row[29]=$emp->Pais_Destino; 
                   $row[30]=number_format($emp->Monto_Fob_Factura, 2, ",", "."); 
                   $row[31]=number_format($emp->Monto_Fob_Solicitud, 2, ",", "."); 
                   $row[32]=number_format($emp->Monto_Flete, 2, ",", ".");
                   $row[33]=number_format($emp->Monto_Seguro, 2, ",", ".");
                   $row[34]=number_format($emp->Otro_Monto, 2, ",", ".");
                   
                   $sheet->appendRow($row); // agrega una fila vacia al final del reporte

                }

            });
           // dd($empresas);
           
        });
        $myFile   = $myFile->string('xls'); 
        $response = array(
            'name' => $name, 
            'file' => "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64," . base64_encode($myFile), 
        );

        return response()->json($response);
        //->export('xlsx');
    }

}
