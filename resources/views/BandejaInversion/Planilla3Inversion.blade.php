@extends('templates/layoutlte')

@section('content')
<script src="{{asset('plugins/fastselect/build.min.js')}}"></script>
<link rel="stylesheet" href="{{asset('plugins/fastselect/fastselect.min.css')}}">
<script src="{{asset('plugins/fastselect/fastselect.standalone.js')}}"></script>

<!--Aqui Form::open-->
{!! Form::open(['route' => 'savePlanilla3Inversion.store' ,'method'=>'POST','id'=>'formplanilla3']) !!}

{{Form::hidden('gen_declaracion_inversion_id',$gen_declaracion_inversion_id)}}
<div class="panels">
	<div id="section1">
		<ol class="breadcrumb">
			<li  class="active" id="paso1" style="color:#1E3165;">Paso 1</li>
			<li  id="paso2">Paso 2</li>
		</ol>
		<div class="panel panel-primary">
			<div class="panel-heading"><h3>Tiene perspectivas de realizar inversiones con recursos externos en la República Bolivariana de Venezuela</h3></div>
				<div class="panel-body">
					<div class="row">
						<div class="col-md-12">
							<div class="col-md-2"><br>
								<div class="form-group">
									<div class="form-check form-check-inline" id="rvalorsi">
										{!! Form::radio('inversion_recursos_externos','Si',false,['class'=>'form-check-input ire_si','id'=>'inversion_recursos_externos'])!!}
										{!! Form::label('','Si',['class'=>'form-check-label'])!!}
									</div>
								</div>
							</div>
							<div class="col-md-2"><br>
								<div class="form-group">
									<div class="form-check form-check-inline" id="rvalorno">
										{!! Form::radio('inversion_recursos_externos','No',false,['class'=>'form-check-input ire_no','id'=>'inversion_recursos_externos'])!!}
										{!! Form::label('','No',['class'=>'form-check-label'])!!}
									</div>
								</div>
							</div>
							<div class="col-md-8"><br>
								<div class="form-group" id="detailinver">
									{!! Form::label('','Detalle:')!!}
									{!! Form::text('det_invert_recursos',null,['class'=>'form-control','id'=>'det_invert_recursos','onkeypress'=>'return soloLetras(event)'])!!}
								</div>
							</div>
						</div>
					</div>

					<div class="row" style="display: none;" id="emp">
						<div class="col-md-12">
							<h4 style="color:red;"><b>Indicar los siguientes aspectos:</b></h4>
							<div class="form-group">
								<div class="form-check form-check-inline">
									{!! Form::radio('proyecto_nuevo','Si',false,['class'=>'form-check-input','id'=>'proyecnew'])!!}
									{!! Form::label('','Proyecto nuevo (Otra empresa)',['class'=>'form-check-label'])!!}
								</div>
							</div>
							<div class="form-group">
								<div class="form-check form-check-inline">
									{!! Form::radio('proyecto_nuevo','No',false,['class'=>'form-check-input','id'=>'ampliacion'])!!}
									{!! Form::label('','Ampliación de su actual empresa',['class'=>'form-check-label'])!!}
								</div>
							</div>
						</div>
					</div><br><br>

					<div class="row" style="display: none;" id="emp1">
						<div class="col-md-12">
							<div class="col-md-4">
								<div class="form-group">
									{!! Form::label('','Nombre del Proyecto:')!!}
									{!! Form::text('nombre_proyecto',null,['class'=>'form-control','id'=>'nompre_proyecto','onkeypress'=>'return soloLetras(event)'])!!}
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									{!! Form::label('','Ubicación (Estado):')!!}
									{!! Form::text('ubicacion',null,['class'=>'form-control','id'=>'ubicacion','onkeypress'=>'return soloNumerosyLetras(event)'])!!}
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									{!! Form::label('','Origen de los Recursos (País de Origen):')!!}

									{!!Form::select('pais_id',$estado,null,['class'=>'form-control', 'id'=>'pais_id','placeholder'=>'Seleccione el País de Origen']) !!}
								</div>
							</div>		
						</div>
					</div>

					<div class="row" style="display: none;" id="emp2">
						<div class="col-md-12">
							<h4 style="color:red;"><b>Bajo que Modalidad Prevé sus Inversiones en el Futuro:</b></h4>
							<div class="form-group">
								<div class="form-check form-check-inline" id="ried">
									{!! Form::radio('ampliacion_actual_emp','Inversión Extranjera Directa',false,['class'=>'form-check-input','id'=>'ied'])!!}
									{!! Form::label('','Inversión Extranjera Directa (IED)',['class'=>'form-check-label'])!!}
								</div>
							</div>
							<div class="form-group">
								<div class="form-check form-check-inline" id="ric">
									{!! Form::radio('ampliacion_actual_emp','Inversión de Cartera',false,['class'=>'form-check-input','id'=>'ic'])!!}
									{!! Form::label('','Inversión de Cartera',['class'=>'form-check-label'])!!}
								</div>
							</div>
						</div>
					</div>

					<div class="row" style="display: none;" id="directa">
						<div class="col-md-12">
							<div class="col-md-6">
								<div class="form-group">
									<div class="form-check form-check-inline">
										{!! Form::checkbox('ampliacion_accionaria_dir','Participacion Accionaria',false,['class'=>'form-check-input','id'=>'particiarancel'])!!}
										{!! Form::label('','Participación Accionaria',['class'=>'form-check-label'])!!}
									</div>
								</div>
								<div class="form-group">
									<div class="form-check form-check-inline">
										{!! Form::checkbox('utilidad_reinvertida','Utilidades Reinvertidas',false,['class'=>'form-check-input','id'=>'utilidades'])!!}
										{!! Form::label('','Utilidades Reinvertidas',['class'=>'form-check-label'])!!}
									</div>
								</div>
								<div class="form-group">
									<div class="form-check form-check-inline">
										{!! Form::checkbox('credito_casa_matriz','Creditos con Casa Matriz o Afiliadas',false,['class'=>'form-check-input','id'=>'csamatriz'])!!}
										{!! Form::label('','Créditos con Casa Matriz o Afiliadas',['class'=>'form-check-label'])!!}
									</div>
								</div>
								<div class="form-group">
									<div class="form-check form-check-inline">
										{!! Form::checkbox('creditos_terceros','Creditos con Terceros',false,['class'=>'form-check-input','id'=>'terceros'])!!}
										{!! Form::label('','Créditos con Terceros',['class'=>'form-check-label'])!!}
									</div>
								</div>
								<div class="form-group">
									<div class="form-check form-check-inline">
										{!! Form::checkbox('otra_dir','Otras',false,['class'=>'form-check-input','id'=>'otras'])!!}
										{!! Form::label('','Otras',['class'=>'form-check-label'])!!} 
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="row" style="display: none;" id="cartera">
						<div class="col-md-12">
							<div class="col-md-6">
								<div class="form-group">
									<div class="form-check form-check-inline">
										{!! Form::checkbox('participacion_accionaria_cart','Participacion Accionaria',false,['class'=>'form-check-input','id'=>'partiarancel'])!!}
										{!! Form::label('','Participación Accionaria',['class'=>'form-check-label'])!!}
									</div>
								</div>
								<div class="form-group">
									<div class="form-check form-check-inline">
										{!! Form::checkbox('bonos_pagare','Bonos y Pagares',false,['class'=>'form-check-input','id'=>'bonospagares'])!!}
										{!! Form::label('','Bonos y Pagares',['class'=>'form-check-label'])!!}
									</div>
								</div>
								<div class="form-group">
									<div class="form-check form-check-inline">
										{!! Form::checkbox('otra_cart','Otras',false,['class'=>'form-check-input','id'=>'otro'])!!}
										{!! Form::label('','Otras',['class'=>'form-check-label'])!!}
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="panel-primary" style="#fff">
						<div class="panel-heading"><h3>Periodo previsto para efectuar su inversión</h3></div>
							<div class="panel-body"><br>
								<div class="row">
									<div class="col-md-12">
										<div class="col-md-4">
											<h5><b>Fecha:</b></h5>
											<div class="input-group date">
											{!! Form::text('periodo_inversion',null,['class'=>'form-control','readonly'=>'readonly','id'=>'periodo_inversion']) !!}
											<span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
										</div>
									</div>
									<div class="col-md-4"></div>
									<div class="col-md-4"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
					
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-4"></div>
						<div class="col-md-4 text-center">
						<div class="form-group">
							<a class="btn btn-primary" href="#" id="seguir1" onclick="enviarPlanillaInversionista('#section1')">Siguiente</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!------------------------------------------------------------------------------------------------------------->

	<div id="section2" style="display: none;">
		<ol class="breadcrumb">
			<li  class="active" id="paso1" style="color:#1E3165;">Paso 1</li>
			<li  class="active" id="paso2" style="color:#1E3165;">Paso 2</li>
		</ol>
		<div class="panel panel-primary">
			<div class="panel-primary" style="#fff">
				<div class="panel-heading"><h3>Sector Productivo o Económico</h3></div>
					<div class="panel-body"><br>
						<div class="row">
							<div class="col-md-12">
								<div class="col-md-6"><b>Sector Productivo o Económico</b><br>
									<div class="form-group">	
										<input
										type="text"
										multiple
										class="tagsInput"
										value=""
										placeholder="Seleccionar Sector Productivo o Económico"
										data-initial-value=''
										data-user-option-allowed="false"
										data-url="{{url('exportador/ajax/sectoreconomico')}}"
										data-load-once="true"
										name="cat_sector_productivo_eco_id[]"
										id="cat_sector_productivo_eco_id">
									</div>
								</div>
							</div>
						</div><br>
					</div>
				</div>
			
				<div class="panel-primary" style="#fff">
				<div class="panel-heading"><h3>Destino de la Inversión</h3></div>
				<div class="panel-body"><br>
					<div class="row">
						<div class="col-md-12">
							<div class="col-md-6"><b>Destino de la Inversión</b><br>
								<div class="form-group">	
									<input
									type="text"
									multiple
									class="tagsInput"
									value=""
									placeholder="Seleccionar Destino de la Inversión"
									data-initial-value=''
									data-user-option-allowed="false"
									data-url="{{url('exportador/ajax/destinoinversion')}}"
									data-load-once="true"
									name="cat_destino_inversion_id[]"
									id="cat_destino_inversion_id">		
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<br><br>
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-4"></div>
					<div class="col-md-4 text-center">
					<div class="form-group">
						<a class="btn btn-primary" href="#" id="atras1">Atras</a>
						<a class="btn btn-primary" href="#" id="guardar_personal">Registrar</a>
					</div>
					</div>
				</div>
			</div>
			</div>		
		</div>
	</div>
</div>
<!------------------------------------------------------------------------------------------------------------->



<!-------Modal--->
<div id="planilla3" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close btn-md" data-dismiss="modal" style="height:5px;">&times;</button>
        <h4 class="modal-title"><b style="color:#337AB7">DECLARACIÓN JURADA DE INVERSIÓN REALIZADA</b></h4>
      </div>
      <div class="modal-body">
      <div class="col-md-12">
        <p class="text-justify">DECLARO QUE LOS DATOS CONTENIDOS EN ESTA DECLARACIÓN HAN SIDO DETERMNADOS EN BASE A LAS DISPOSICIONES LEGALES Y REGLAMENTARIAS CORRESPONDIENTES.
        </p>
        <div class="row">
          <div class="colo-md-1"></div>
          <div class="col-md-10">
             <input type="checkbox" name="condicion" id="condicion" value="si" class="checkbox-inline"><label class="text-justify">LA INFORMACIÓN AQUÍ DECLARADA ES ENTREGADA CON CARÁCTER CONFIDENCIAL.</label>
          </div>
          <div class="colo-md-1"></div>
        </div>
      </div>
      </div>
      <div class="modal-footer">
        <div class="row">
          <div class="col-md-12">
            <div class="col-md-4"></div>
            <div class="col-md-4 text-center">
              <div class="form-group">
                 <button type="button" class="btn btn-default" style="background-color:#ADABC9; color: #FFFFFF;" data-dismiss="modal"><b>Cerrar</b></button>
                <!-- <input type="submit" class="btn btn-primary" value="Aceptar" name="Aceptar">  -->
				<input type="button" class="btn btn-primary" value="Aceptar" name="Aceptar" onclick="registrarPlanilla()"> 
              </div>
            </div>
            <div class="col-md-4"></div>
          </div>
        </div>
      </div>
    </div>

  </div>
</div>

{{Form::close()}}


<script type="text/javascript">
	$('.tagsInput').fastselect();

	$('#otro').click(function(event) {
		$('#destinver').show();      
	});

	$('#empresas,#negocios,#inmuebles,#entidades,#lucro,#instrumentos,#actos,#fondo').click(function(event) {
	$('input[type="text"]').val('');
		$('#destinver').hide();
			
	});

	$('#rvalorsi').click(function(event) {
		if ($('#proyecnew,#ampliacion').is(':checked')) {
		$('#proyecnew,#ampliacion').removeAttr('checked');
		}
		$('#emp').show();			
	});

	$('#rvalorno').click(function(event) {
	if ($('#proyecnew,#ampliacion').is(':checked')) {
		$('#proyecnew,#ampliacion').removeAttr('checked');
		}
	if ($('#particiarancel,#utilidades,#csamatriz,#terceros,#otras,#partiarancel,#bonospagares,#otro').is(':checked')) {
		$('#particiarancel,#utilidades,#csamatriz,#terceros,#otras,#partiarancel,#bonospagares,#otro').prop("checked", false);
		}
		$('input[type="text"]').val(''); 
		$('#emp1').hide();
		$('#emp').hide();
		$('#emp2').hide();
		$('#directa').hide();
		$('#cartera').hide();
			
	});


	$('#proyecnew').click(function(event) {
		if ($('#ied,#ic').is(':checked')) {
		$('#ied,#ic').removeAttr('checked');
		}
	$('#emp2').hide();
	$('#directa').hide();
	$('#cartera').hide();
	$('#emp1').show();
			
	});

	$('#ampliacion').click(function(event) { //remover el check de directa y de cartera
		$('input[type="text"]').val('');  
		$('#emp1').hide();
		$('#emp2').show();
		if ($('#ied,#ic').is(':checked')) {
		$('#ied,#ic').removeAttr('checked');
		}
			
	});

	$('#ied').click(function(event) {
		$('#directa').show();
		$('#cartera').hide();
	});

	$('#ic').click(function(event) {
		$('#cartera').show();
		$('#directa').hide();      
	});

	$('#checkspe').click(function(event) {
		$('#spe').show();
	});


	$('#ic').click(function(event) {
		if ($('#particiarancel,#utilidades,#csamatriz,#terceros,#otras').is(':checked')) {
			$('#particiarancel,#utilidades,#csamatriz,#terceros,#otras').prop("checked", false);
		}
	});


	$('#ied').click(function(event) {
		if ($('#partiarancel,#bonospagares,#otro').is(':checked')) {
		$('#partiarancel,#bonospagares,#otro').prop("checked", false);
		}

	});

	var noseguir = 0;
	function enviarPlanillaInversionista(section) {
		let input_basicos = $('#formplanilla3 '+section).find('input, select, textarea').not(':button,:hidden, :file, .fstQueryInput, .fstQueryInputExpanded');
		//console.log(input_basicos);
		let campos = [];
		let nombre_campos = [];
		let extraer = ['total_costos_declaracion', 'invert_divisa_cambio','fstControls'];
		let excepcion = [];
		let agregar = ['periodo_inversion', 'cat_sector_productivo_eco_id', 'inversion_recursos_externos', 'cat_destino_inversion_id', 'ied'];
		let check_boxes = ['particiarancel', 'utilidades', 'csamatriz', 'terceros', 'otras', 'bonospagares','otro' ];
		
			for(let i=0; i < input_basicos.length; i++){
				if(input_basicos[i] != 'undefined' || input_basicos[i] != undefined){
					
					if(!extraer.includes(input_basicos[i].id)){
						campos.push(input_basicos[i].id);
					}
				}
			}	

			$("div").remove(".msg_alert");

			let err_duda = 0;
			let error_dudas = '<div class="msg_alert"><span style="color:red" class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span><strong id="cod_aran-error" class="added"> El campo es obligatorio.</strong></div>';
			let valido_dudas = '<div class="msg_alert"><span style="color:green" class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span></div>';
			
			let n = campos.length;
			let input_duda = '';

			for (var i = 0; i < n; i++) {
				input_duda = $('#'+campos[i]);
				let valor_campo = '';

				switch(campos[i]){
					case 'inversion_recursos_externos':
						if($('#inversion_recursos_externos.ire_si').is(":checked")  ){
							if($('#ied').is(":checked")  ){
								valor_campo = 1;
							}
							if($('#ic').is(":checked")  ){
								valor_campo = 1;
							}
						}

						if($('#inversion_recursos_externos.ire_no').is(":checked")){
							valor_campo = 1;
						}
						
					break;

					case 'proyecnew':
					case 'ampliacion':
					case 'particiarancel':
					case 'utilidades':
					case 'csamatriz':
					case 'terceros':
					case 'otras':
					case 'bonospagares':
					case 'otro':
						if($('#proyecnew').is(":checked")  ){
							valor_campo = 1;
						}

						if($('#ampliacion').is(":checked")){
							if($('#ied').is(":checked") || $('#ic').is(":checked") ){
								if($('#particiarancel, #utilidades, #csamatriz, #terceros, #otras, #bonospagares,#otro').is(":checked")){
									valor_campo = 1;
								}
								//valor_campo = 1;
							}
							console.log(valor_campo);
						}
					break;

					case 'cat_sector_productivo_eco_id':
						if($('#cat_sector_productivo_eco_id').parent().find('.fstChoiceItem')[0]){
							valor_campo = 1;							
						}
					break;

					case 'cat_destino_inversion_id':
						if($('#cat_destino_inversion_id').parent().find('.fstChoiceItem')[0]){
							valor_campo = 1;
						}
					break;

					default:
						valor_campo = input_duda.val();
					break;
				}

				if (valor_campo == '')
				{
					err_duda = 1;
					noseguir = 1;
					switch(campos[i]){
						case 'inversion_recursos_externos':
							if($('#inversion_recursos_externos.ire_si').nextAll('.msg_alert').length == 0){
								$('#inversion_recursos_externos.ire_si').css('border', '1px solid red').after(error_dudas);
							}

							if($('#inversion_recursos_externos.ire_no').nextAll('.msg_alert').length == 0){
								$('#inversion_recursos_externos.ire_no').css('border', '1px solid red').after(error_dudas);
							}
						break;

						case 'proyecnew':
						case 'ampliacion':
							if($('#proyecnew').nextAll('.msg_alert').length == 0){
								$('#proyecnew').css('border', '1px solid red').after(error_dudas);
							}
							if($('#ampliacion').nextAll('.msg_alert').length == 0){
								$('#ampliacion').css('border', '1px solid red').after(error_dudas);
							}
						break;

						default:
							input_duda.css('border', '1px solid red').after(error_dudas);
						break;
					}

					if(err_duda==1){
						event.preventDefault();
						swal("¡Por Favor!", 'Estimado usuario. Debe completar los campos solicitados', "warning");
					}
				} else {
					if (err_duda == 1) {
						err_duda = 1;
						noseguir = 1;
					}else{
						err_duda = 0;
						noseguir = 0;
					}
					//input_duda.css('border', '1px solid green').after(valido_dudas);
					switch(campos[i]){
						case 'inversion_recursos_externos':
							if($('#inversion_recursos_externos.ire_si').nextAll('.msg_alert').length == 0){
								$('#inversion_recursos_externos.ire_si').css('border', '1px solid green').after(valido_dudas);
							}

							if($('#inversion_recursos_externos.ire_no').nextAll('.msg_alert').length == 0){
								$('#inversion_recursos_externos.ire_no').css('border', '1px solid green').after(valido_dudas);
							}
						break;

						default:
							input_duda.css('border', '1px solid green').after(valido_dudas);
						break;
					}
				}
			}

			if(err_duda == 0 && section == "#section2"){
				enviarplanilla3();
			}
	}

	$('#seguir1').on('click', function(){
		enviarPlanillaInversionista('#section1')
	});

	$('#guardar_personal').on('click', function(){
		enviarPlanillaInversionista('#section2')
	});

	function registrarPlanilla(){
		$('#formplanilla3').submit();
	}

	function replaceStr(str, find) {
    for (var i = 0; i < find.length; i++) {
        str = str.replace(new RegExp(find[i], 'gi'), '');
    }
    return str;
}

</script>
@stop

    