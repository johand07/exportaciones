<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CapitalSocialAccionista extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('capital_social_accionista', function (Blueprint $table) {
          $table->engine = 'InnoDB';
          $table->increments('id');
          $table->unsignedInteger('gen_sol_inversion_id')->index()->nullable();
          $table->foreign('gen_sol_inversion_id')->references('id')->on('gen_sol_inversion');
          $table->unsignedInteger('accionistas_id')->index()->nullable();
          $table->foreign('accionistas_id')->references('id')->on('accionistas');
          $table->string('capital_social','100')->nullable();
          $table->boolean('bactivo')->default(1);
          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('capital_social_accionista');
    }
}
