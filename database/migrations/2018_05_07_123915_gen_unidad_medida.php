->nullable()<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GenUnidadMedida extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gen_unidad_medida', function (Blueprint $table) {
           $table->engine = 'InnoDB';
           $table->increments('id');
           $table->string('dunidad', 100)->nullable();
           $table->boolean('bactivo')->default(1);
           $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::dropIfExists('gen_unidad_medida');
    }
}
