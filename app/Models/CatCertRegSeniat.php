<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CatCertRegSeniat extends Model
{
    protected $table = 'cat_cert_reg_seniat';
    protected $fillable = [

           'id',
           'pregunta',
           'bactivo',
    ];

    protected $date =['created_at','updated_at'];
}
