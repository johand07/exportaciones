@extends('templates/layoutlte_coordinador_intranet')

@section('content')
<div id="printf">
  <div class="row">
    <div class="col-md-10">
      {!! Form::open(['route'=>'ReporteSolicitudesIntranet.store','method'=>'POST','id'=>'reportesolicitudintra']) !!}
      <div class="col-md-6">
					<div class="input-daterange input-group" id="datepicker">
	      			<span class="input-group-addon"><b>Desde:</b> </span>
	     			 {!!Form::text('desde',(!is_null($desde))?$desde:old('desde'),['class'=>'form-control','id'=>'desde'])!!}
	      			<span class="input-group-addon"><b>Hasta:</b> </span>
	      			{!!Form::text('hasta',(!is_null($hasta))?$hasta:old('hasta'),['class'=>'form-control','id'=>'hasta'])!!}
        			</div>
				</div> 
      <div class="col-md-3">
        <a class="btn btn-primary" href="{{url('IntranetCoordinador/ReporteSolicitudesIntranet')}}">Cancelar</a>
        <input type="submit" name="Consultar" class="btn btn-success center" value="Consultar" onclick="">
      </div>
        <div class="col-md-3">
            <a href="#" id="btnExport" class="btn btn-primary" onclick="exportarexel()"><span class="glyphicon glyphicon-file" aria-hidden="true" ></span>Exportar Excel</a>
        </div>
      {{Form::close()}}  
    </div>
    <br><br><br>
    <div class="col-md-1"></div>
  </div>
  </div>
 <div class="row" style="display:none;" id="reportesol"><br>
  <table style="width: 100%" align="text-rigth">
    <tr>
      <td></td>
      <td><img class="img-responsive" style="width: 800px;" src={{ asset('img/cintillo_reporte.png') }}></td>
    </tr>
    <tr>
      <td></td>
      <td align="center">Desde: @if (!empty($desde)) {{$desde}}@endif     Hasta: @if (!empty ($hasta)) {{$hasta}}@endif</td>

    </tr>
  </table>
</div>

@if(isset($solicitudesSuspendidos) || isset($solicitudesCierreConformes) || isset($solicitudesCierreParciales) || isset($solicitudesEviadasArchivos))
<div class="row">
    <div class="col-md-12">
        <div role="tabpanel">
            <!-- Nav tabs -->
             <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#p2" class="cargador-planilla" aria-controls="p2" role="tab" data-toggle="tab" id="tab-inicial"><strong>Suspendido- Documento Faltante</strong>  <div class="label label-danger text-center">total <b>{{count($solicitudesSuspendidos)}}</b></div></a>
                </li>
                <li role="presentation"><a href="#p3" class="cargador-planilla" aria-controls="p3" role="tab" data-toggle="tab"><strong>Cierre Conforme de Exportacion</strong> <div class="label label-success text-center">total <b>{{count($solicitudesCierreConformes)}}</b></div></a>
                </li>
                <li role="presentation"><a href="#p4" class="cargador-planilla" aria-controls="p4" role="tab" data-toggle="tab"><strong>Cierre parcial de Exportacion</strong> <div class="label label-warning text-center">total <b>{{count($solicitudesCierreParciales)}}</b></div></a>
                </li>
                <li role="presentation"><a href="#p5" class="cargador-planilla" aria-controls="p5" role="tab" data-toggle="tab"><strong>Enviada a Archivo</strong> <div class="label label-primary text-center">total <b>{{count($solicitudesEviadasArchivos)}}</b></div></a>
                </li>
                
            </ul>
            <!-- Tab panes -->
        </div>
        <div class="tab-content">
            
            <div role="tabpanel" class="tab-pane active" id="p2">
                <br><br>
                <!-- Tabla solicitudesSuspendidos status 31 -->
                <table id="ReporsolicitudesSuspendidos" class="table" style="width:100%">
                    <thead>
                        
                        <tr class="text-center">
                            <th class="col-md-2 text-center">Rif</th>  
                            <th class="col-md-2 text-center">Razon Social</th>  
                            <th class="col-md-2 text-center">Nº Solicitud</th> 
                            <th class="col-md-2 text-center">Tipo</th>        
                            <th class="col-md-3 texstatus-center">OCA</th>
                            <th class="col-md-2 text-center">Observacion</th>
                            <th class="col-md-2 text-center">Analizada Por</th>
                        
                            
                        </tr>
                    </thead>
                    <tbody>
                        @if(isset($solicitudesSuspendidos))
                            @foreach($solicitudesSuspendidos as $key =>  $solicitud)
                                @if($solicitud->bactivo == 1)
                                    <tr>           
                                        <td class="text-center"  > {{ $solicitud->rif }}</td>          
                                        <td class="text-center"  >{{ $solicitud->razon_social }}</td>
                                        <td class="text-center"  ><b class="text-primary">{{ $solicitud->gen_solicitud_id }}</b></td>
                                        <td class="text-center"  >{{ $solicitud->solicitud }}</td>
                                        <td class="text-center"  >{{ $solicitud->nombre_oca }}</td>
                                        <td class="text-center"  >{{ $solicitud->observacion_analista_intra ? $solicitud->observacion_analista_intra : '' }}</td>
                                        <td class="text-center"  >{{ $solicitud->analizado_por ? $solicitud->analizado_por : '' }}</td>

                                    </tr>   
                                @endif
                            @endforeach
                        @else
                            <p> No se encontraron Solicitudes bajo este estatus</p>
                        @endif
                    </tbody>
                </table>
            </div>
            <div role="tabpanel" class="tab-pane" id="p3">
                <br><br>
                <!-- Tabla solicitudesSuspendidos status 32 -->
                <table id="ReporsolicitudesCierreConformes" class="table" style="width:100%">
                    <thead>
                        
                        <tr class="text-center">
                            <th class="col-md-2 text-center">Rif</th>  
                            <th class="col-md-2 text-center">Razon Social</th>  
                            <th class="col-md-2 text-center">Nº Solicitud</th> 
                            <th class="col-md-2 text-center">Tipo</th>        
                            <th class="col-md-3 texstatus-center">OCA</th>
                            <th class="col-md-2 text-center">Observacion</th>
                            <th class="col-md-2 text-center">Analizada Por</th>
                        
                            
                        </tr>
                    </thead>
                    <tbody>
                        @if(isset($solicitudesCierreConformes))
                            @foreach($solicitudesCierreConformes as $key =>  $solicitud)
                                @if($solicitud->bactivo == 1)
                                    <tr>           
                                        <td class="text-center"  > {{ $solicitud->rif }}</td>          
                                        <td class="text-center"  >{{ $solicitud->razon_social }}</td>
                                        <td class="text-center"  ><b class="text-primary">{{ $solicitud->gen_solicitud_id }}</b></td>
                                        <td class="text-center"  >{{ $solicitud->solicitud }}</td>
                                        <td class="text-center"  >{{ $solicitud->nombre_oca }}</td>
                                        <td class="text-center"  >{{ $solicitud->observacion_analista_intra ? $solicitud->observacion_analista_intra : '' }}</td>
                                        <td class="text-center"  >{{ $solicitud->analizado_por ? $solicitud->analizado_por : '' }}</td>
                                        
                                    
                                    </tr>   
                                @endif
                            @endforeach
                        @else
                            <p> No se encontraron Solicitudes bajo este estatus</p>
                        @endif
                    </tbody>
                </table>
            </div>
            <div role="tabpanel" class="tab-pane" id="p4">
                <br><br>
                <!-- Tabla solicitudesCierreparciales status 33 -->
                <table id="ReporsolicitudesCierreParciales" class="table" style="width:100%">
                    <thead>
                        
                        <tr class="text-center">
                            <th class="col-md-2 text-center">Rif</th>  
                            <th class="col-md-2 text-center">Razon Social</th>  
                            <th class="col-md-2 text-center">Nº Solicitud</th> 
                            <th class="col-md-2 text-center">Tipo</th>        
                            <th class="col-md-3 texstatus-center">OCA</th>
                            <th class="col-md-2 text-center">Observacion</th>
                            <th class="col-md-2 text-center">Analizada Por</th>
                        
                            
                        </tr>
                    </thead>
                    <tbody>
                        @if(isset($solicitudesCierreParciales))
                            @foreach($solicitudesCierreParciales as $key =>  $solicitud)
                                @if($solicitud->bactivo == 1)
                                    <tr>           
                                        <td class="text-center"  > {{ $solicitud->rif }}</td>          
                                        <td class="text-center"  >{{ $solicitud->razon_social }}</td>
                                        <td class="text-center"  ><b class="text-primary">{{ $solicitud->gen_solicitud_id }}</b></td>
                                        <td class="text-center"  >{{ $solicitud->solicitud }}</td>
                                        <td class="text-center"  >{{ $solicitud->nombre_oca }}</td>
                                        <td class="text-center"  >{{ $solicitud->observacion_analista_intra ? $solicitud->observacion_analista_intra : '' }}</td>
                                        <td class="text-center"  >{{ $solicitud->analizado_por ? $solicitud->analizado_por : '' }}</td>
                                       
                                    
                                    </tr>   
                                @endif
                            @endforeach
                        @else
                            <p> No se encontraron Solicitudes bajo este estatus</p>
                        @endif
                    </tbody>
                </table>
            </div>
            <div role="tabpanel" class="tab-pane" id="p5">
                <br><br>
                <!-- Tabla solicitudesCierreparciales status 35 -->
                <table id="ReporsolicitudesEviadasArchivos" class="table" style="width:100%">
                    <thead>
                        
                        <tr class="text-center">
                            <th class="col-md-2 text-center">Rif</th>  
                            <th class="col-md-2 text-center">Razon Social</th>  
                            <th class="col-md-2 text-center">Nº Solicitud</th> 
                            <th class="col-md-2 text-center">Tipo</th>        
                            <th class="col-md-3 texstatus-center">OCA</th>
                            <th class="col-md-2 text-center">Observacion</th>
                            <th class="col-md-2 text-center">Analizada Por</th>
                            <th class="col-md-2 text-center">Finalizada Por</th>
                        
                            
                        </tr>
                    </thead>
                    <tbody>
                        @if(isset($solicitudesEviadasArchivos))
                            @foreach($solicitudesEviadasArchivos as $key =>  $solicitud)
                                @if($solicitud->bactivo == 1)
                                    <tr>           
                                        <td class="text-center"  > {{ $solicitud->rif }}</td>          
                                        <td class="text-center"  >{{ $solicitud->razon_social }}</td>
                                        <td class="text-center"  ><b class="text-primary">{{ $solicitud->gen_solicitud_id }}</b></td>
                                        <td class="text-center"  >{{ $solicitud->solicitud }}</td>
                                        <td class="text-center"  >{{ $solicitud->nombre_oca }}</td>
                                        <td class="text-center"  >{{ $solicitud->observacion_analista_intra ? $solicitud->observacion_analista_intra : '' }}</td>
                                        <td class="text-center"  >{{ $solicitud->analizado_por ? $solicitud->analizado_por : '' }}</td>
                                        <td class="text-center"  >{{ $solicitud->finalizado_por ? $solicitud->finalizado_por : '' }}</td>
                                       
                                    
                                    </tr>   
                                @endif
                            @endforeach
                        @else
                            <p> No se encontraron Solicitudes bajo este estatus</p>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
        </div>
    </div>
</div>
@endif

@stop

<script src="{{asset('js/ajax_v2.1.1.js')}}"></script>
<script type="text/javascript">
/*-------------Reporte exel---------------------------------------*/

function exportarexel(){

if($('#desde').val() == '' && $('#hasta').val() == ''){

 swal("Por Favor","Debe filtrar un rango de fecha para exportar el excel"); 
}else{

$.ajax({    
        url: '/IntranetCoordinador/ExcelReportesRecepcionSolicitudes',
        type: 'GET',

        data: {
                
                'desde': $('#desde').val(),
                'hasta': $('#hasta').val()

            },
    })
    .done(function(data) {
        //alert(data.file);
          var a = document.createElement("a");
          a.href = data.file;
          a.download = data.name+'.xls';
          document.body.appendChild(a);
          a.click();
          a.remove();
          console.log(data);
   



    })
    .fail(function(jqXHR, textStatus, thrownError) {
        errorAjax(jqXHR,textStatus)

    })	
}

}
</script>