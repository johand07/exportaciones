<?php

namespace App\Http\Controllers;

use App\Models\GenSolicitud;
use App\Models\GenDVDSolicitud;
use App\Models\TipoSolicitud;
use App\Models\TipoDocBanco;
use App\Models\GenUsuarioOca;
use App\Models\GenUsuario;
use App\Models\BandejaAsignacionesIntranet;
use App\Models\GenStatus;
use App\Models\BandejaAnalistaIntranet;
use App\Models\FacturaSolicitud;
use App\Models\GenNotaCredito;
use App\Models\GenDvdNd;
use App\Models\NotaDebito;
use Illuminate\Support\Facades\DB;
use Session;
use Auth;
use Laracasts\Flash\Flash;
use Illuminate\Support\Facades\Crypt;
use Alert;
use Illuminate\Http\Request;

class AnalistaIntranetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $solicitudes = DB::table('bandeja_asignaciones_intranet as ba')
            ->join('gen_solicitud as gs','ba.gen_solicitud_id','=','gs.id')
            
            ->join('tipo_solicitud as ts','gs.tipo_solicitud_id','=','ts.id')
            ->join('det_usuario as du','gs.gen_usuario_id','=','du.gen_usuario_id')
            ->join('gen_operador_cambiario as  op','gs.coperador_cambiario','=','op.id')
            ->join('gen_usuario as  gu','ba.gen_usuario_id','=','gu.id')
            ->join('gen_usuario as  guas','ba.asignado_por','=','guas.id')
            ->select(
                    'du.rif',
                    'du.razon_social',
                    'gs.id as gen_solicitud_id',
                    'ts.solicitud',
                    'op.nombre_oca',
                    'gu.email as asignada_a',
                    'guas.email as asignado_por',
                    'ba.gen_status_id',
                    'gs.bactivo'
                   )
            ->whereIn('gs.gen_status_id',[6,30])
            ->where('ba.gen_usuario_id',Auth::user()->id)
            ->where('gs.bactivo',1)
            ->get();
        return view('AnalistaIntranet.index', compact('solicitudes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $solicitudId = $request->gen_solicitud_id ? $request->gen_solicitud_id : '';

        // Validando si viene por get el id de la solicitud
        if (!empty($solicitudId)) {

            $solicitudId = decrypt($solicitudId) ? decrypt($solicitudId) : 'false';
            Session::put('gen_solicitud_id',$solicitudId);

        // o Validando si esta en la sesion
        } else {

            $solicitudId = Session::get('gen_solicitud_id');
        }

        // Validamos que  exista el id de la solicitud
        if(empty($solicitudId) or  $solicitudId=='false') {
              
              Alert::warning('No puede ser procesada la Solicitud.','Acción Denegada!')->persistent("OK");

              return redirect()->action('AnalistaIntranetController@index'); 

        }
        $validarAnalisis = BandejaAnalistaIntranet::where('gen_solicitud_id', $solicitudId)->where('gen_usuario_id', Auth::user()->id)->first();
        if (empty($validarAnalisis)) {
            /* insertamos la atencion al usuario analista de las solicitud seleccionada */
            $bandeja = new BandejaAnalistaIntranet;
            $bandeja->gen_solicitud_id = $solicitudId;
            $bandeja->gen_usuario_id = Auth::user()->id;
            $bandeja->gen_status_id = 30;
            $bandeja->fstatus= date('Y-m-d');
            $bandeja->save();
        }
        
        /* actualizamos la bandeja de asignaciones */
        $actualizarBandejaAsignaciones=BandejaAsignacionesIntranet::where('gen_solicitud_id', $solicitudId)->where('gen_usuario_id', Auth::user()->id)->update(['gen_status_id' => 30,'fstatus'=>date('Y-m-d'),'updated_at' => date("Y-m-d H:i:s") ]);
        /* Consulta la solicitudes para pasar al nuevo estatus una vez atendido */
        $solicitud=GenSolicitud::find($solicitudId);
        $solicitud->gen_status_id = 30;
        $solicitud->fstatus=date('Y-m-d');
        $solicitud->updated_at=date("Y-m-d H:i:s");
        $solicitud->save();
        /* Actaulizamos dvds */
        $actualizar_dvds=GenDVDSolicitud::where('gen_solicitud_id', $solicitudId)->update(['gen_status_id' => 30, 'fstatus'=>date('Y-m-d'),'updated_at' => date("Y-m-d H:i:s") ]);
        
        /*consultamos detalles para pintar vista create con detalles para el analisis */          
        $solicitud=GenSolicitud::where('id', $solicitudId)->where('bactivo',1)->first();

        $listaventa=GenDVDSolicitud::where('gen_solicitud_id',$solicitudId)->get();

        $facturasSolicitud=FacturaSolicitud::where('gen_solicitud_id',$solicitudId)->get();

        //Se itera a facturasSolicitud y lleno un arreglo con !empty Aqui si se usa !empty ya que este se usa es para evaluar un arreglo; y luego solo con los id de las facturas gen_factura_id. (Y ya con este arreglo Si esta lleno se puede pasar a las otras 2 consultas a gen nota credito y a nota debito)
        $facturasId = [];

        if (!empty($facturasSolicitud)) {
            foreach ($facturasSolicitud as $key => $value) {
                array_push($facturasId, $value->gen_factura_id);
            }
        }
        // dd($facturasId);
        $ncredito = GenNotaCredito::whereIn('gen_factura_id',$facturasId)->where('bactivo',1)->get();
        $NDebito=NotaDebito::whereIn('gen_factura_id',$facturasId)->where('bactivo',1)->get();
        $listaventaNd=GenDvdNd::whereIn('gen_factura_id',$facturasId)->where('tipo_solicitud_id',4)->get();

        $status=GenStatus::whereIn('id',[31,32,33])->pluck('nombre_status','id');
        return view('AnalistaIntranet.create', compact('solicitud', 'listaventa', 'status', 'solicitudId','ncredito','NDebito','listaventaNd'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $solicitudId = $request->gen_solicitud_id;
        /* actualizamos la bandeja del analista con el status asignado por su evaluacion */
        $actualizarBandejaAnalista=BandejaAnalistaIntranet::where('gen_solicitud_id', $solicitudId)->where('gen_usuario_id', Auth::user()->id)->update(['gen_status_id' => $request->gen_status_id,'fstatus'=>date('Y-m-d'),'updated_at' => date("Y-m-d H:i:s") ]);
        /* actualizamos la bandeja de asignaciones */
        $actualizarBandejaAsignaciones=BandejaAsignacionesIntranet::where('gen_solicitud_id', $solicitudId)->where('gen_usuario_id', Auth::user()->id)->update(['gen_status_id' => $request->gen_status_id, 'analizado_por'=> Auth::user()->id ,'fstatus'=>date('Y-m-d'),'updated_at' => date("Y-m-d H:i:s") ]);
        /* Consulta la solicitud para pasar al nuevo estatus una vez evaluado */
        $solicitud=GenSolicitud::find($solicitudId);
        $solicitud->gen_status_id = $request->gen_status_id;
        $solicitud->observacion_analista_intra = !empty($request->observacion_analista_intra) && ($request->gen_status_id == 31 || $request->gen_status_id == '31' ) ? $request->observacion_analista_intra : null;
        $solicitud->fstatus=date('Y-m-d');
        $solicitud->updated_at=date("Y-m-d H:i:s");
        $solicitud->save();
        /* Actaulizamos dvds */
        $actualizar_dvds=GenDVDSolicitud::where('gen_solicitud_id', $solicitudId)->update(['gen_status_id' => $request->gen_status_id, 'fstatus'=>date('Y-m-d'),'updated_at' => date("Y-m-d H:i:s") ]);
        Alert::success('Se ha Evaluado la solicitud!','Evaluado Correctamente!')->persistent("Ok");

        return redirect()->action('AnalistaIntranetController@index');
    }
    
    
    public function solEvaluadasAnalista(Request $request)
    {
        $solicitudesSuspendidos = DB::table('bandeja_asignaciones_intranet as ba')
            ->join('gen_solicitud as gs','ba.gen_solicitud_id','=','gs.id')
            
            ->join('tipo_solicitud as ts','gs.tipo_solicitud_id','=','ts.id')
            ->join('det_usuario as du','gs.gen_usuario_id','=','du.gen_usuario_id')
            ->join('gen_operador_cambiario as  op','gs.coperador_cambiario','=','op.id')
            ->join('gen_usuario as  gu','ba.gen_usuario_id','=','gu.id')
            ->join('gen_usuario as  guas','ba.asignado_por','=','guas.id')
            ->select(
                    'du.rif',
                    'du.razon_social',
                    'gs.id as gen_solicitud_id',
                    'ts.solicitud',
                    'op.nombre_oca',
                    'gu.email as asignada_a',
                    'guas.email as asignado_por',
                    'ba.gen_status_id',
                    'gs.observacion_analista_intra',
                    'gs.bactivo'
                   )
            ->whereIn('gs.gen_status_id',[31])
            ->where('ba.analizado_por',Auth::user()->id)
            ->where('gs.bactivo',1)
            ->get();
        
        $solicitudesCierreConformes = DB::table('bandeja_asignaciones_intranet as ba')
            ->join('gen_solicitud as gs','ba.gen_solicitud_id','=','gs.id')
            
            ->join('tipo_solicitud as ts','gs.tipo_solicitud_id','=','ts.id')
            ->join('det_usuario as du','gs.gen_usuario_id','=','du.gen_usuario_id')
            ->join('gen_operador_cambiario as  op','gs.coperador_cambiario','=','op.id')
            ->join('gen_usuario as  gu','ba.gen_usuario_id','=','gu.id')
            ->join('gen_usuario as  guas','ba.asignado_por','=','guas.id')
            ->select(
                    'du.rif',
                    'du.razon_social',
                    'gs.id as gen_solicitud_id',
                    'ts.solicitud',
                    'op.nombre_oca',
                    'gu.email as asignada_a',
                    'guas.email as asignado_por',
                    'ba.gen_status_id',
                    'gs.observacion_analista_intra',
                    'gs.bactivo'
                   )
            ->whereIn('gs.gen_status_id',[32])
            ->where('ba.analizado_por',Auth::user()->id)
            ->where('gs.bactivo',1)
            ->get();

        $solicitudesCierreParciales = DB::table('bandeja_asignaciones_intranet as ba')
            ->join('gen_solicitud as gs','ba.gen_solicitud_id','=','gs.id')
            
            ->join('tipo_solicitud as ts','gs.tipo_solicitud_id','=','ts.id')
            ->join('det_usuario as du','gs.gen_usuario_id','=','du.gen_usuario_id')
            ->join('gen_operador_cambiario as  op','gs.coperador_cambiario','=','op.id')
            ->join('gen_usuario as  gu','ba.gen_usuario_id','=','gu.id')
            ->join('gen_usuario as  guas','ba.asignado_por','=','guas.id')
            ->select(
                    'du.rif',
                    'du.razon_social',
                    'gs.id as gen_solicitud_id',
                    'ts.solicitud',
                    'op.nombre_oca',
                    'gu.email as asignada_a',
                    'guas.email as asignado_por',
                    'ba.gen_status_id',
                    'gs.observacion_analista_intra',
                    'gs.bactivo'
                   )
            ->whereIn('gs.gen_status_id',[33])
            ->where('ba.analizado_por',Auth::user()->id)
            ->where('gs.bactivo',1)
            ->get();

        return view('AnalistaIntranet.solicitudesEvaluadas', compact('solicitudesSuspendidos', 'solicitudesCierreConformes', 'solicitudesCierreParciales'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\GenSolicitud  $genSolicitud
     * @return \Illuminate\Http\Response
     */
    public function show(GenSolicitud $genSolicitud , $id)
    {
        $solicitudId = $id ? $id : '';

        // Validando si viene por get el id de la solicitud
        if (!empty($solicitudId)) {

            $solicitudId = decrypt($solicitudId) ? decrypt($solicitudId) : 'false';
            Session::put('gen_solicitud_id',$solicitudId);

        // o Validando si esta en la sesion
        } else {

            $solicitudId = Session::get('gen_solicitud_id');
        }

        // Validamos que  exista el id de la solicitud
        if(empty($solicitudId) or  $solicitudId=='false') {
              
              Alert::warning('No puede ser procesada la Solicitud.','Acción Denegada!')->persistent("OK");

              return redirect()->action('AnalistaIntranetController@index'); 

        }
        /*consultamos detalles para pintar vista show con detalles para el analisis */          
        $solicitud=GenSolicitud::where('id', $solicitudId)->where('bactivo',1)->first();
        $listaventa=GenDVDSolicitud::where('gen_solicitud_id',$solicitudId)->get();
        $facturasSolicitud=FacturaSolicitud::where('gen_solicitud_id',$solicitudId)->get();

        $facturasId = [];

        if (!empty($facturasSolicitud)) {
            foreach ($facturasSolicitud as $key => $value) {
                array_push($facturasId, $value->gen_factura_id);
            }
        }
        // dd($facturasId);
        $ncredito = GenNotaCredito::whereIn('gen_factura_id',$facturasId)->where('bactivo',1)->get();
        $NDebito=NotaDebito::whereIn('gen_factura_id',$facturasId)->where('bactivo',1)->get();
        $listaventaNd=GenDvdNd::whereIn('gen_factura_id',$facturasId)->where('tipo_solicitud_id',4)->get();
        return view('AnalistaIntranet.show', compact('solicitud', 'listaventa', 'solicitudId','ncredito','NDebito','listaventaNd'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\GenSolicitud  $genSolicitud
     * @return \Illuminate\Http\Response
     */
    public function edit(GenSolicitud $genSolicitud)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\GenSolicitud  $genSolicitud
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, GenSolicitud $genSolicitud)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\GenSolicitud  $genSolicitud
     * @return \Illuminate\Http\Response
     */
    public function destroy(GenSolicitud $genSolicitud)
    {
        //
    }
}
