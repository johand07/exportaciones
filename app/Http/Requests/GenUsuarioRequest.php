<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GenUsuarioRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

           
            'email'=>'required|unique:gen_usuario|email|max:255',
            'email_seg'=>'email|unique:gen_usuario,email_seg',
            'gen_tipo_usuario_id'=>'required',
            'gen_status_id'=>'required',
            'password'=>'required|min:6',
            'password_confirm'=>'required|min:6|same:password',
            'respuesta_seg'=>'required|min:3',         

        ];
    }
}
