<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GenRolesFunciones extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gen_roles_funciones', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('gen_roles_id')->default(1)->index()->nullable();
            $table->foreign('gen_roles_id')->references('id')->on('gen_roles');
            $table->unsignedInteger('gen_funciones_id')->default(1)->index()->nullable();
            $table->foreign('gen_funciones_id')->references('id')->on('gen_funciones');
            $table->string('rol_orden', 80)->nullable();
            $table->boolean('bactivo')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gen_roles_funciones');
    }
}
