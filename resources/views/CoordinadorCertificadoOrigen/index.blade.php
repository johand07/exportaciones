@extends('templates/layoutlte_coordinador_djo')
@section('content')

<div class="content" style="background-color: #fff">
	<div class="panel-group" style="background-color: #fff">
		<div class="panel-primary">
			<div class="panel-heading"><h3>Solicitudes de Certificados de Origen</h3></div>
			<div class="panel-body">
      
			<div class="row">
				<div class="col-md-12">
					<div class="row">
						<div class="col-md-8">
							<h4 class="text-info"> Bandeja de Entrada:</h4><hr>
						</div>
						<div class="col-md-2">
							
						</div>
						<div class="col-md-2">
							<a class=" btn btn-primary form-control" href="{{route('CoordinadorCertificadoOrigen.index')}}"><span class="glyphicon glyphicon-refresh" title="Agregar" aria-hidden="true"></span>
							Actualizar
							</a>
						</div>
					</div><!--Segunda row-->
					<table id="listaCertificados" class="table table-striped table-bordered" style="width:100%">
						<thead style="font-size: 12px;">
							<tr class="text-center">
								<th class="col-md-1 text-center">Nº</th>
								<th class="col-md-2 text-center">Nº Certificado</th>
								<th class="text-center">Nombre de Certificado</th>
								<th class="text-center">Nº Rif</th>
								<th class="text-center">Fecha de Emisión</th>
								<th class="text-center">Fecha de Estatus</th>
								<th class="text-center">Analista</th>
								<th class="text-center">Estatus</th>
								<th class="text-center">Acciones</th>
							</tr>
						</thead>
						<tbody>
						@foreach($coordinadorCertificado as $key => $coordinador)
							<tr style="font-size:12px;">
								<td class="col-md-1 text-center" id="aranx_{{ $key }}"><b>{{ $key + 1 }}</b></td>
								<td class="col-md-2 text-center"><b class="text-primary">{{ $coordinador->num_certificado }}</b></td>

								<td class="col-md-5 text-center"><b class="text-primary">{{ $coordinador->GenTipoCertificado->nombre_certificado }}</td>
								<td class="text-center">{{$coordinador->GenUsuario->DetUsuario->rif}}</td>
								<td>{{ date ('d-m-Y',strtotime($coordinador->created_at)) }}</td>
								<td class=" text-center">{{ date ('d-m-Y',strtotime($coordinador->created_at))}}</td>
								<td>@if(isset($coordinador->rBandejaAnalista->GenUsuario->email))
									{{$coordinador->rBandejaAnalista->GenUsuario->email}}
								@else
									<i class="glyphicon glyphicon-user"></i>
									!Sin atender!
								@endif</td>
								<td class=" text-center" style="font-size:12px;">
									<b>
                                    	@if($coordinador->gen_status_id==10)
                                          <i class="glyphicon glyphicon-user"></i> ¡Atendida  por el Analista!
                                        @elseif($coordinador->gen_status_id==11)
                                          <i class="glyphicon glyphicon-user"></i> ¡En Observación!
                                        @elseif($coordinador->gen_status_id==12)
                                          <i class="glyphicon glyphicon-ok"></i> ¡Verificado!
                                        @elseif($coordinador->gen_status_id==18)
                                          <i class="glyphicon glyphicon-ok"></i> Corregida por el usuario!
                                        @elseif($coordinador->gen_status_id==13)
                                          <i class="glyphicon glyphicon-remove"></i> ¡Solicitud para Anulación!
                                        @elseif($coordinador->gen_status_id==16)
                                          <i class="glyphicon glyphicon-ok"></i> ¡Aprobada por el Coordinador!
                                        @elseif($coordinador->gen_status_id==9)
                                          <i class="glyphicon glyphicon-user"></i> ¡Por atender!
                                        @else
                                            <i class="glyphicon glyphicon-user"></i> Por Atender 
                                    	@endif
                                  </b>	
								</td>
								<td class="col-md-2 text-center" id="status">
									<input id="token" type="hidden" name="_token" value="{{ csrf_token() }}">
										@if($coordinador->gen_status_id==10)
                                      	@elseif($coordinador->gen_status_id==11)
										  <a href="{{ route('CoordinadorCertificadoOrigen.edit',$coordinador->id)}}" class="btn btn-sm btn-danger list-inline" id="atender"><i class="glyphicon glyphicon-share"></i></a>
                                      	@elseif($coordinador->gen_status_id==18)
                                      	@elseif($coordinador->gen_status_id==12)
										  <button id="atender" class="btn btn-sm btn-danger list-inline" onclick="confirmView({{$coordinador->id}})"><i class="glyphicon glyphicon-share"></i><b> Atender</b> </button>
                                       	@elseif($coordinador->gen_status_id==13)
                                          <a href="{{ route('CoordinadorCertificadoOrigen.edit',$coordinador->id)}}" class="btn btn-sm btn-warning list-inline" id="atender"><i class="glyphicon glyphicon-share"></i><b>Anular</b></a>
										@elseif($coordinador->gen_status_id==14)
										  <a id="atender" href="{{ route('CoordinadorCertificadoOrigen.edit',$coordinador->id)}}" class="btn btn-sm btn-danger list-inline"><i class="glyphicon glyphicon-share"></i><b> Atender</b> </a>
                                        @elseif($coordinador->gen_status_id==16)
                                          <a href="{{ route('CertificadoOrigen.pdf',['cerId'=>encrypt($coordinador->id)])}}" target="_blank" class="text-center btn btn-sm btn-success list-inline"><i class="glyphicon glyphicon-download"></i> <b>Descargar</b></a>
                                           <a href="{{ route('excelColombia.excel',['cerId'=>encrypt($coordinador->id)])}}" class="text-center btn btn-sm btn-default list-inline"><i class="glyphicon glyphicon-download"></i> <b>Excel</b></a> 
                                        @elseif($coordinador->gen_status_id==9)
                                          
                                      	@else
                                          <a href="{{ route('CertificadoOrigen.pdf',['cerId'=>encrypt($coordinador->id)])}}" target="_blank" class="text-center btn btn-sm btn-success list-inline"><i class="glyphicon glyphicon-download"></i> <b>Descargar</b></a>

                                      	@endif


                                 </td>
							</tr>
						  @endforeach
						</tbody>
					</table>
				</div>
			</div>
			</div>
		</div>
	</div>
</div>
@stop

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="{{asset('js/ajax_v2.1.1.js')}}"></script>
<script type="text/javascript">

 /*--------------ajax reporte exel---------------------------------------*/
/*
	$(document).on('click','.btn-default',function(){
		var id_boton= $(this).attr("cerId");
	    var id=$(this).prev().val();
	    console.log('certId', id );
	    $.ajax({    //AgenteAduanal.destroy O exportador/AgenteAduanal/{AgenteAduanal}
	            url: 'excelColombia/excel',
	            type: 'GET',

	            data: {
	                    
	                    'cerId': id,
	                    

	                },
	        })
	        .done(function(data) {
	            //alert(data.file);
	              var a = document.createElement("a");
	              a.href = data.file;
	              a.download = data.name;
	              document.body.appendChild(a);
	              a.click();
	              a.remove();
	              console.log(data);
	       



	        })
	        .fail(function(jqXHR, textStatus, thrownError) {
	            errorAjax(jqXHR,textStatus)

	        })
    });
*/
</script>

<script>
	$(document).ready(function() {
	
	  confirmView = function(idcert) {
	
		$(document).on('click', '.SwalBtn1', function() {
			//Some code 1
			window.location.href = "CoordinadorCertificadoOrigen/"+idcert+"";
		});
		$(document).on('click', '.SwalBtn2', function() {
			//Some code 2 
			window.location.href = "CoordinadorCertificadoOrigen/"+idcert+"/edit"
		});
		
		swal({
		  title: "¿Qué deseas realizar con esta solicitud?",
		  html: true,
		  text: '<button type="button" role="button" class="SwalBtn1 text-center btn btn-sm btn-primary list-inline" style="margin-right:5px">' + 'Visualizar' + '</button> ' +
				'<button type="button" role="button" class="SwalBtn2 text-center btn btn-sm btn-success list-inline">' + 'Atender' + '</button>',
		  type: "warning",
		  showConfirmButton: false,
		  showCancelButton: false
		});
	  }
	
	});
	
</script>
