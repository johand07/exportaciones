<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DocExtra extends Model
{
    protected $table = 'doc_extras';
    protected $fillable = [

        'cert_exporta_facil_id',
        'cat_descrip_doc_id',
        'numero_documento',
        'entidad_emisora',
        'fecha',
        'bactivo',
    ];

    protected $date =['created_at','updated_at'];

    public function CertExportaFacil() {

        return $this->hasOne('App\Models\CertExportaFacil','id','cert_exporta_facil_id');

    }

    public function CatDescripcionDoc() {

        return $this->hasOne('App\Models\CatDescripcionDoc','id','cat_descrip_doc_id');

    }
}
