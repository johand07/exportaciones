@extends('templates/layoutlte')
@section('content')
	<div class="container-fluid">
		<div class="row">
			<h1>Reporte Pago</h1>
		</div>
		<div class="row">
			pintar las cuentas.
		</div>
		
		<div class="row">
			{{Form::open(['route' =>'Pagos.store' ,'method'=>'POST','id'=>'formProductos', 'enctype' => 'multipart/form-data'])}}
				<div class="col-md-6">
					<div class="form-group">
						{{Form::hidden('tipo_pago',$tipo_pago,['readonly'=>'true'])}}
						{{Form::hidden('solicitud_id',$solicitud_id,['readonly'=>'true'])}}
						{!! Form::label('banco_receptor','Banco receptor') !!}
						{!! Form::select('cat_banco_admin_id',$cat_banco_admin_id,null,['class'=>'form-control','id'=>'cat_banco_admin_id',
						'placeholder'=>'Seleccione cuenta a la que transfirió']) !!}
					</div>

					<div class="form-group">
						<label for="numero_referencia">Ingrese el número de referencia</label>
						<input type="text" name="numero_referencia" id="numero_referencia" class="form-control">
					</div>

					<div class="form-group">
		            	{!! Form::label('','Fecha Registro DUA en Aduanas') !!}
		            	<div class="input-group date">
		            		{!! Form::text('fecha_reporte',null,['class'=>'form-control','readonly'=>'readonly','id'=>'fecha_reporte']) !!}
		            		<span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
		            	</div>
	            	</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						{!! Form::label('banco_emisor','Banco emisor') !!}
						{!! Form::select('gen_operador_cambiario_id',$gen_operador_cambiario_id,null,['class'=>'form-control','id'=>'gen_operador_cambiario_id',
						'placeholder'=>'Seleccione banco a la que realizó el pago']) !!}
					</div>

					<div class="form-group">
						<span class="btn btn-default btn-file" style="margin: 3px;">
	                          Cargar Capture&nbsp;  <span><i class="glyphicon glyphicon-floppy-open"></i></span>
	                          {!! Form::file('img_capture', array('class' => 'file-input','style'=>'margin: 10px;')) !!}
	                    </span>
	                    <div>
	                    	<img id="vista_previa_proceso_grafico" src="" alt="" width="300px" style="margin: 3px;">
                            <img id="imgdocumento" src="{{ asset('img/file.png') }}" alt="" width="300px" style="margin: 3px; display: none;">
	                    </div>
					</div>
				</div>
				<div class="col-md-12">
					<button type="submit" class="btn btn-primary">Guardar</button>
				</div>
			{{Form::close()}}
				
		</div>

	</div>


	<script src="{{asset('js/jquery.min.js')}}"></script>

	<script>
	function cambiarImagen(event) {

	    var reader = new FileReader();
	    reader.onload = function(){
	      var output = document.getElementById('vista_previa_proceso_grafico');
	      output.src = reader.result;
	    };
	    var archivo = $(".file-input").val();
	    var extensiones = archivo.substring(archivo.lastIndexOf(".")).toLowerCase();
	    //alert(archivo);
	    //var imgfile="/exportaciones/public/img/file.png";

	//alert(imgfile);
	    if(extensiones != ".jpeg" && extensiones != ".png" && extensiones != ".jpg" && extensiones != ".gif"){
	        $('#imgdocumento').show();
	        
	    }else{
	      $('#imgdocumento').hide();
	      reader.readAsDataURL(event.target.files[0]);
	    }

	    
	  
	 }

    $(document).on('change', '.file-input', function(evt) {
           cambiarImagen(evt);
    });
	</script>
@stop