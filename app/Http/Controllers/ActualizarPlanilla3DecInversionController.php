<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\PlanillaDjir_01;
use App\Models\PlanillaDjir_02;
use App\Models\PlanillaDjir_03;
use App\Models\GenSectorProductivoDjir_03;
use App\Models\GenDestinoInversionDjir_03;
use App\Models\GenDeclaracionInversion;
use App\Models\Pais;
use Auth;
use Alert;

class ActualizarPlanilla3DecInversionController extends Controller
{
    public function Actualizar($id, $gen_declaracion_inversion_id)
    {
        $planillaDjir03 = PlanillaDjir_03::where('gen_declaracion_inversion_id', $id)->first();



        $gen_destino_inversion_djir03=GenDestinoInversionDjir_03::where('gen_declaracion_inversion_id',$id)->get();

        $gen_sector_productivo_djir03=GenSectorProductivoDjir_03::where('gen_declaracion_inversion_id',$id)->get(); 
        $estado=Pais::all()->pluck('dpais','id');

        return view('BandejaInversion.actualizar.editPlanilla3Inversion', compact('gen_declaracion_inversion_id','planillaDjir03','estado','gen_destino_inversion_djir03','gen_sector_productivo_djir03'));
    }

    public function update(Request $request, PlanillaDjir_03 $planillaDjir_03)
    {//dd($request->all());
            $planilla3 = new PlanillaDjir_03;
            $planilla3->gen_declaracion_inversion_id = $request->gen_declaracion_inversion_id ;
            $planilla3->inversion_recursos_externos = $request->inversion_recursos_externos ;
            $planilla3->det_invert_recursos = $request->det_invert_recursos;
            $planilla3->proyecto_nuevo = (isset($request->proyecto_nuevo))?$request->proyecto_nuevo:'';
            $planilla3->nombre_proyecto = ($request->proyecto_nuevo=='Si')?$request->nombre_proyecto:'';
            $planilla3->ubicacion = ($request->proyecto_nuevo=='Si')?$request->ubicacion:'';
            $planilla3->pais_id = ($request->inversion_recursos_externos == 'Si' && $request->proyecto_nuevo=='Si')?$request->pais_id:168;
            $planilla3->ampliacion_actual_emp = (isset($request->ampliacion_actual_emp))?$request->ampliacion_actual_emp:'';
            $planilla3->ampliacion_accionaria_dir = (isset($request->ampliacion_accionaria_dir))?$request->ampliacion_accionaria_dir:'';
            $planilla3->utilidad_reinvertida = (isset($request->ampliacion_actual_emp))?$request->utilidad_reinvertida:'';
            $planilla3->credito_casa_matriz = (isset($request->ampliacion_actual_emp))?$request->credito_casa_matriz:'';
            $planilla3->creditos_terceros = (isset($request->ampliacion_actual_emp))?$request->creditos_terceros:'';
            $planilla3->otra_dir = (isset($request->otra_dir))?$request->otra_dir:'';
            $planilla3->especifique_directa = (isset($request->especifique_directa))?$request->especifique_directa:'';
            $planilla3->participacion_accionaria_cart = (isset($request->participacion_accionaria_cart))?$request->participacion_accionaria_cart:'';
            $planilla3->bonos_pagare = (isset($request->bonos_pagare))?$request->bonos_pagare:'';
            $planilla3->otra_cart = (isset($request->otra_cart))?$request->otra_cart:'';
            $planilla3->periodo_inversion = (isset($request->periodo_inversion))?$request->periodo_inversion:'';
            $planilla3->gen_status_id = 9;
            $planilla3->estado_observacion = (isset($request->estado_observacion))?$request->estado_observacion:0;
            $planilla3->descrip_observacion = (isset($request->descrip_observacion))?$request->descrip_observacion:'';
            $planilla3->save();

            if(isset($request->cat_sector_productivo_eco_id[0])){
                foreach ($request->cat_sector_productivo_eco_id as $idTablaProducto => $idSectorProd) {
            
                    $idSectProd=explode(',',$idSectorProd);
        
                    foreach ($idSectProd as $key => $idSector) {//dd($idSector);
                        if(isset($idSector) && $idSector != ''){
                        $sectorProd = new GenSectorProductivoDjir_03;
                        $sectorProd->gen_declaracion_inversion_id = $request->gen_declaracion_inversion_id;
                        $sectorProd->cat_sector_productivo_eco_id = $idSector;
                        $sectorProd->save();
                        }   
                    }
                }
            }
            
            if(isset($request->cat_sector_productivo_eco_id[0])){
                foreach ($request->cat_destino_inversion_id as $idTablaProducto => $idDestinInv) {
            
                    $idDestInv=explode(',',$idDestinInv);
        
                    foreach ($idSectProd as $key => $idSector) {
                        if(isset($idSector) && $idSector != ''){
                            $sectorProd = new GenDestinoInversionDjir_03;
                            $sectorProd->gen_declaracion_inversion_id = $request->gen_declaracion_inversion_id;
                            $sectorProd->cat_destino_inversion_id = $idSector;
                            $sectorProd->save();
                        }
                    }
                }  
            }
            $genDeclaracion = GenDeclaracionInversion::find($request->planilla3);
            $genDeclaracion->gen_status_id = 17;
            $genDeclaracion->save();

            $updatePlanilla1 = PlanillaDjir_01::where('gen_declaracion_inversion_id', $request->planilla3)->first();
            $updatePlanilla1->gen_status_id = 17;
            $updatePlanilla1->save();

            $updatePlanilla2 = PlanillaDjir_03::where('gen_declaracion_inversion_id', $request->planilla3)->first();
            $updatePlanilla2->gen_status_id = 17;
            $updatePlanilla2->save();
            
            $updatePlanilla3 = PlanillaDjir_03::where('gen_declaracion_inversion_id', $request->planilla3)->first();
            $updatePlanilla3->actualizado = 1;
            $updatePlanilla3->gen_status_id = 17;
            $updatePlanilla3->save();
        // 
        
        $gen_declaracion_inversion_id=$request->gen_declaracion_inversion_id;

        // 
       // dd($gen_declaracion_inversion_id);

        $planillaDjir01=PlanillaDjir_01::where('gen_declaracion_inversion_id',$gen_declaracion_inversion_id)->first();
        $planillaDjir02=PlanillaDjir_02::where('gen_declaracion_inversion_id',$gen_declaracion_inversion_id)->first();
        $planillaDjir03=PlanillaDjir_03::where('gen_declaracion_inversion_id',$gen_declaracion_inversion_id)->first();
        Alert::success('Se ha registrado en la planilla 3!','Registrado Correctamente!')->persistent("Ok");
       return redirect()->action('BandejaInversionController@index');
    }
}
