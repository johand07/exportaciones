<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GenTrasferTecnoInversion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Gen_trasfer_tecno_inversion', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('nro_sol_tranferencia', 120)->nullable();
            $table->unsignedInteger('gen_status_id')->index()->nullable();
            $table->foreign('gen_status_id')->references('id')->on('gen_status');
            $table->unsignedInteger('gen_sol_inversionista_id')->index()->nullable();
            $table->foreign('gen_sol_inversionista_id')->references('id')->on('gen_sol_inversionista');
            $table->string('fstatus', 100)->nullable(); 
            $table->string('fcelebracion_contrato', 100)->nullable();
            $table->string('finicio_contrato', 100)->nullable();
            $table->string('fvencimiento_contrato', 100)->nullable();
            $table->unsignedInteger('cat_tipo_contrato_id')->index()->nullable();
            $table->foreign('Cat_tipo_contrato_id')->references('id')->on('Cat_tipo_contrato');
            $table->string('objeto_contrato', 100)->nullable();
            $table->string('monto_contrato', 100)->nullable();
            $table->unsignedInteger('gen_divisa_id')->index()->nullable();
            $table->foreign('gen_divisa_id')->references('id')->on('gen_divisa');
            $table->string('status_observacion', 100)->nullable();
            $table->string('observacion_transferencia', 100)->nullable();
            $table->boolean('bactivo')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Gen_trasfer_tecno_inversion');
    }
}
