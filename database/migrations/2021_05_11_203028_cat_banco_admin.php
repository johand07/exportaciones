<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CatBancoAdmin extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('cat_banco_admin', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('nombre_banco',100)->nullable();
            $table->string('tipo_cuenta',100)->nullable();
            $table->string('num_cuenta',30)->nullable();
            $table->string('ci_rif',20)->nullable();
            $table->string('nombre_ente',100)->nullable();
            $table->string('telf_ente',15)->nullable();
            $table->string('img_banco',300)->nullable();
            $table->string('moneda',100)->nullable();
            $table->boolean('bactivo')->default(1);
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cat_banco_admin');
    }
}
