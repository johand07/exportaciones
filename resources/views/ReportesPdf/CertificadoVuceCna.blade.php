<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>CERTIFICADO VUCE-CNA</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style type="text/css">
        .centrar{
            text-align: center;
        }

        .titulo{
            
            font-weight: bold;
        }

        .nro-correlativo{
            color: red;
            font-size: 16px;
        }

        .derecha{
            text-align: left;

        }

        .text-normal{
            font-size: 14px;
        }

        .text-medio{
            font-size: 12px;
        }
        .text-11{
            font-size: 11px;
        }
        .text-small{
            font-size: 10px;
        }

        .text-9{
            font-size: 9px;
        }

        .text-8{
            font-size: 8px;
        }

        .espacio-superior{
            margin-top: 10px;
        }

        .espacio-superior-small{
            margin-top: 5px;
        }

        .ancho-medio{
            width: 50%;
        }

        .ancho-full{
            width: 100%;
        }

        .ancho-medio-alto{
            width: 75%;
        }

        .ancho-90{
            width: 90%;
        }

        .table{
            border-collapse: collapse;
        }
        
        .borde{
            border: 1px solid #000;
        }

        .sin-borde{
            border: none;
        }

        .top{
            vertical-align: top;
        }

        .under-dotted{    
            border-bottom: 1px dotted #000;
            text-decoration: none;
        }

        .content-1{
            width: 5%;
        }

        .content-9{
            width: 20%;
        }

        .sangria-1{
            margin-left: 28px;
        }

        .sangria-2{
            margin-left: 36px;
        }

        .sangria-3{
            margin-left: 54px;
        }

        .sangria-sub{
            margin-left: 14px;
            
        }

        
        .page-break { page-break-before: always; }

        .table-center{
            margin: 0 auto; 
        }

        .justify{
            text-align: justify;
        }

        td.justify {
            padding-left: 8px;
            padding-right: 8px; 
        }
        .lineal{
            
        }
        .lineal>div{
            max-width:91%;

        }
        .lineal>*{
           display: inline-block;
           vertical-align:top;
        }

        .sangria-lista{
            list-style-type: none;
            margin: 0;
            padding: 0;
            margin-bottom: 4px;
        }

        .sangria-lista>li{
            margin-top: 5px;
            padding-left: 10px;
            padding-right: 10px;
        }
    </style>
</head>
<body>
    <div class="centrar espacio-superior">
        <table class="ancho-full">
            <tr>
                <td class="top center" style="width: 70%; font-size:107%;">
                      <img class="img-responsive" style="width: 565px;" src="img/cintillo.png">
                </td>
            </tr>
        </table>
    </div><br>
    <div class="centrar espacio-superior">
        <table class="ancho-full">
            <tr>
                <td class="nro-correlativo" style="width: 70%; font-size:107%; text-align: right;">
                     <b>CI-CNA/N°</b><b style="color: black">{{ $solicitud_cna->num_sol_resguardo}}</b>
                </td>
            </tr>
        </table>
    </div><br>
    <div class="centrar espacio-superior">
        <table class="ancho-full">
            <tr>
                <td class="center" style="width: 70%; font-size:107%; text-align: center;">
                     CERTIFICADO DE INSPECCION DEL CNA
                                DEL EXPORTADOR
                </td>
            </tr>
            <tr>
                <td class="center" style="width: 70%; font-size:30px; text-align: center;">
                     <b>CI-CNA.</b>
                </td>
            </tr>
        </table>
    </div>

<div class="centrar espacio-superior">
    <table class="ancho-full">
        <tr>
            <td class="top" style="text-align: left; font-size: 20px" colspan="50"><b>Sres.</b><br><br>{{ $users->razon_social}}</td>
            <td class="top" style="text-align: center; font-size: 20px" colspan="50"><b>RIF</b><br><br>{{ $users->rif}}<br><br></td>
        </tr>
    </table>

</div>

<div>
    <table>
        <tr>
            <td style="text-align: justify; font-size: 23px;" colspan="80">Despacho de Viceministro de Comercio Exterior y Promoción de Inversiones, emite el presente certificado, haciendo constar que la empresa {{ $users->razon_social}} cumplió con los requisitos correspondientes ante
            EL CNA DEL SENIAT de exportadores de la Ventanilla Única de Comercio
            Exterior.</td>
        </tr>
    </table>
</div>

<br><br><br><br>

<div>
    <table>
        <tr>
            <td style="text-align: center; font-size: 20px;" colspan="80"><b>{{ $datosfirmante->nombre_firmante}} {{ $datosfirmante->apellido_firmante}}</b><br><b>{{ $datosfirmante->cargo_firmante}}</b><br>{{ $datosfirmante->gaceta_firmante}}</td>
        </tr>
    </table>
</div>
<br><br>
<div class="centrar espacio-superior">
    <table class="ancho-full">
        <tr class="text-11">
            <td class="top" style="text-align: left" colspan="50"><b><img class="img-responsive" style="width: 250;" src="img/logovuce.png"></b><br><br></td>
            <td class="top" style="text-align: center" colspan="50"><b><img src="data:image/png;base64,{!! base64_encode(QrCode::format('png')->size(350)->generate(URL::to('/api/VerificacionCertificadoCna/'.$solicitud_cna->id))) !!}" width="160px" height="145px"></b></td>
        </tr>
    </table>
</div>
</body>
</html>
