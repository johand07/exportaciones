<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DocumentosDvd extends Model
{
    protected $table = 'documentos_nd';

    	protected $fillable = [

           'gen_dvd_nd_id',
           'cat_documentos_id',
           'gen_status_id',
           'fstatus',
           'firma_seguridad',
           'bactivo',
    ];

    protected $date =['created_at','updated_at'];


    public function dvdnd() {

     return $this->hasOne('App\Models\GenDvdNd','id','gen_dvd_nd_id');

    }

    public function documentos() {

     return $this->hasOne('App\Models\CatDocumentos','id','cat_documentos_id');

    }

    public function estatus() {

     return $this->hasOne('App\Models\GenStatus','id','gen_status_id');

    }
}
