<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TipoDocBanco extends Model
{
    protected $table = 'tipo_doc_banco';
}
