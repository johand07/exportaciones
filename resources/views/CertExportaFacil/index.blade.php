    @extends('templates/layoutlte')

    @section('content')

    <div class="content" style="background-color: #fff">
      <div class="panel-group" style="background-color: #fff;">
        <div class="panel-primary">
          <div class="panel-heading"><h3>Certificado Exporta Facil</h3> </div>
          <div class="panel-body">
            <div class="row">
              <div class="col-md-12">
                <div class="row">
                  <div class="col-md-7 col-sm-7 col-xs-7">
                    <h4 class="text-info"> Mis Certificados:</h4>
                  </div>                 
                  <div class="col-md-3 col-sm-3 col-xs-3">
                    <a class="btn btn-success form-control" href="{{route('CertExportaFacil.create')}}">
                      <span class="glyphicon glyphicon-file" title="Agregar" aria-hidden="true"></span>
                      Crear Nueva Solicitud
                    </a>
                  </div>
                  <div class="col-md-2 col-sm-2 col-xs-2">

                    <a class="btn btn-primary form-control" href="{{route('CertExportaFacil.index')}}">
                      <span class="glyphicon glyphicon-refresh" title="Agregar" aria-hidden="true"></span>
                      Actualizar
                    </a>
                  </div>

                </div> <hr>
                <table id="listaCertificados" class="table table-striped table-bordered" style="width:100%">
                  <thead>
                    <tr>
                      <th class="text-center">Número Solicitud</th>
                      <th class="text-center">Destino</th>
                      <th class="text-center">Fecha de Emisión</th>
                      <th class="text-center">Tipo de Mercancia</th>
                      <th class="text-center">Codigo</th>
                      <th class="text-center">Tratamiento Especial</th>
                      <th class="text-center">Acciones</th>
                    </tr>
                  </thead>
                  <tbody>
                  @if(isset($solicitudes))
                    @foreach($solicitudes as $key => $sol)
                      <tr class="text-center">
                        <td>{{ $sol->num_sol_cert }}</td>
                        <td>{{ $sol->pais->dpais }}</td>
                        <td>{{ $sol->created_at }}</td>
                        <td>{{ $sol->tipo_meracncia }}</td>
                        <td>{{ $sol->codigo}}</td>
                        <td>{{ $sol->tratamiento_especial}}</td>
                        <td>
                          <a href="{{action('PdfController@CertificadoExportaFacil',array('id'=>$sol->id))}}" class="glyphicon glyphicon-file btn btn-default btn-sm" target="_blank"  aria-hidden="true" title="Certificado exporta fácil"></a>
                        </td>

                      </tr>
                    @endforeach
                  @endif
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div><!-- fin content-->
    @stop
