<?php

namespace App\Http\Controllers;

use App\Models\GenUsuario;
use App\Models\Accionistas;
use App\Models\Productos;
use App\Models\DetUsuario;
use Illuminate\Http\Request;
use Session;
use Auth;
use Laracasts\Flash\Flash;
use Alert;
use PDF;
use Illuminate\Routing\Route;
use Illuminate\Http\Response;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\DB;

class ReportEmpresaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $titulo="Empresas Registradas";
        $descripcion="Detalle de Empresas Registradas"; 
        //dd($request->desde);
        $fecha=date("Y-m-d");
        $desde=$request->desde;
        $hasta=$request->hasta;
        $mes=date('m');
        $anio=date('Y');

 $empresa_reg=DB::table('det_usuario as det')
        ->select('det.rif',//
                'det.razon_social',//
                'det.nombre_repre as Nombre_Representante_Legal',//
                'det.apellido_repre as Apellido_Representante_Legal',//
                'gs.descripcion_sector',//
            /**/'ae.dactividad as Actividad Economica',//
                'est.estado',//
                'mun.municipio as Municipio',//
                'pa.parroquia as Parroquia',//
                'det.direccion',//
                'det.telefono_local',//
                'det.telefono_movil',//
                'det.correo',
                'ctu.descripcion',
                'det.created_at as Fecha de Registro'
                )
    ->join('gen_usuario as gu', 'det.gen_usuario_id', '=', 'gu.id')//
        ->join('estado as est', 'det.estado_id', '=', 'est.id')//
        ->join('municipio as mun', 'det.municipio_id', '=', 'mun.id')//
        ->join('parroquia as pa', 'det.parroquia_id', '=', 'pa.id')//
        ->join('gen_actividad_eco as ae', 'det.gen_actividad_eco_id', '=', 'ae.id')//
        ->join('gen_sector as gs', 'ae.gen_sector_id', '=', 'gs.id')//
        ->join('cat_tipo_usuario as ctu', 'gu.cat_tipo_usuario_id', '=', 'ctu.id')//
        ->whereNotIn('det.gen_usuario_id',[1,2,3,4,5,11,12,2430,2268])
        ->whereBetween('det.created_at',[$desde,$hasta])
        ->where('det.bactivo',1)
        ->orderBy('det.created_at', 'desc')
        ->get();

        return view('ReportesEmpresa.index',compact('titulo','descripcion','desde','hasta','empresa_reg'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $titulo="Empresas Registradas";
        $descripcion="Detalle de Empresas Registradas"; 
        //dd($request->desde);
        $fecha=date("Y-m-d");
        $desde=$request->desde;
        $hasta=$request->hasta;
        $mes=date('m');
        $anio=date('Y');

        $empresa_reg=DB::table('det_usuario as det')
        ->select('det.rif',//
                'det.razon_social',//
                'det.nombre_repre as Nombre_Representante_Legal',//
                'det.apellido_repre as Apellido_Representante_Legal',//
                'gs.descripcion_sector',//
            /**/'ae.dactividad as Actividad Economica',//
                'est.estado',//
                'mun.municipio as Municipio',//
                'pa.parroquia as Parroquia',//
                'det.direccion',//
                'det.telefono_local',//
                'det.telefono_movil',//
                'det.correo',
                'ctu.descripcion',
                'det.created_at as Fecha de Registro'
                )
    ->join('gen_usuario as gu', 'det.gen_usuario_id', '=', 'gu.id')//
        ->join('estado as est', 'det.estado_id', '=', 'est.id')//
        ->join('municipio as mun', 'det.municipio_id', '=', 'mun.id')//
        ->join('parroquia as pa', 'det.parroquia_id', '=', 'pa.id')//
        ->join('gen_actividad_eco as ae', 'det.gen_actividad_eco_id', '=', 'ae.id')//
        ->join('gen_sector as gs', 'ae.gen_sector_id', '=', 'gs.id')//
        ->join('cat_tipo_usuario as ctu', 'gu.cat_tipo_usuario_id', '=', 'ctu.id')//
        ->whereNotIn('det.gen_usuario_id',[1,2,3,4,5,11,12,2430,2268])
        //->whereMonth('det.created_at',$mes)
        ->whereBetween('det.created_at',[$desde,$hasta])
        ->where('det.bactivo',1)
        ->orderBy('det.created_at', 'desc')
        ->get();

        //dd($empresa_reg);

        return view('ReportesEmpresa.index',compact('titulo','descripcion','desde','hasta','empresa_reg'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\GenUsuario  $genUsuario
     * @return \Illuminate\Http\Response
     */
    public function show(GenUsuario $genUsuario)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\GenUsuario  $genUsuario
     * @return \Illuminate\Http\Response
     */
    public function edit(GenUsuario $genUsuario)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\GenUsuario  $genUsuario
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, GenUsuario $genUsuario)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\GenUsuario  $genUsuario
     * @return \Illuminate\Http\Response
     */
    public function destroy(GenUsuario $genUsuario)
    {
        
    }

    public function ExcelEmpresasReportes(Request $request, DetUsuario $detUsuario)
    {
        /************Consulta de fecha*******/
        $desde=$request->desde;
        $hasta=$request->hasta;

        //dd($request->all());

        $empresa_reg=DB::table('det_usuario as det')
        ->select('det.rif',//
                'det.razon_social',//
                'det.nombre_repre as Nombre_Representante_Legal',//
                'det.apellido_repre as Apellido_Representante_Legal',//
                'gs.descripcion_sector',//
            /**/'ae.dactividad as Actividad_Economica',//
                'est.estado',//
                'mun.municipio as Municipio',//
                'pa.parroquia as Parroquia',//
                'det.direccion',//
                'det.telefono_local',//
                'det.telefono_movil',//
                'det.correo',
                'ctu.descripcion',
                'det.created_at as Fecha_de_Registro'
                )
    ->join('gen_usuario as gu', 'det.gen_usuario_id', '=', 'gu.id')//
        ->join('estado as est', 'det.estado_id', '=', 'est.id')//
        ->join('municipio as mun', 'det.municipio_id', '=', 'mun.id')//
        ->join('parroquia as pa', 'det.parroquia_id', '=', 'pa.id')//
        ->join('gen_actividad_eco as ae', 'det.gen_actividad_eco_id', '=', 'ae.id')//
        ->join('gen_sector as gs', 'ae.gen_sector_id', '=', 'gs.id')//
        ->join('cat_tipo_usuario as ctu', 'gu.cat_tipo_usuario_id', '=', 'ctu.id')//
        ->whereNotIn('det.gen_usuario_id',[1,2,3,4,5,11,12,2430,2268])
        //->whereMonth('det.created_at',$mes)
        ->whereBetween('det.created_at',[$desde,$hasta])
        ->where('det.bactivo',1)
        ->orderBy('det.created_at', 'desc')
        ->get();

        //dd($empresa_reg);

        $originalDate = "2022-06-02";
        $timestamp = strtotime($originalDate); 
        $newDate = date("d-m-Y", $timestamp );
        //dd($newDate);
        $numero = 1002002.365;
        $format=number_format($numero, 2, ",", ".");

         /**Estructura de excel***/
        $name="Reportes de Empresas Registradas";
        //dd($sol_er);
        $myFile   = Excel::create('Reportes de Empresas Registradas',function($excel)use($empresa_reg){ // Nombre princial del archivo
            $excel->sheet('Empresas Registradas',function($sheet)use ($empresa_reg){ // se crea la hoja con el nombre de datos
               // dd($sol_er);
                //header
                $sheet->setStyle(array(
                'font' => array(
                    'name' =>  'itálico',
                    'size' =>  12,
                    'bold' =>  true
                )
                ));

                $sheet->mergeCells('A1:H1'); // union de celdas desde A a F
                $sheet->row(1,['Empresas Registradas']);
                $sheet->row(2,[]);
                $sheet->row(3,['Rif','Razon Social','Nombre Representante Legal','Apellido Representante Legal','Descripción Sector','Actividad Economica','Estado','Municipio','Parroquia','Dirección','Telefono Local','Telefono Movíl','Correo','Descripción','Fecha de Registro']);
                //dd($empresa_reg);
                //recorrido
                foreach ($empresa_reg as $empresa) {

                   $row=[]; // un arreglo vacio que se va a llenar con las posicion 
                   $row[0]=$empresa->rif; 
                   $row[1]=$empresa->razon_social; 
                   $row[2]=$empresa->Nombre_Representante_Legal; 
                   $row[3]=$empresa->Apellido_Representante_Legal; 
                   $row[4]=$empresa->descripcion_sector; 
                   $row[5]=$empresa->Actividad_Economica; 
                   $row[6]=$empresa->estado;
                   $row[7]=$empresa->Municipio; 
                   $row[8]=$empresa->Parroquia;  
                   $row[9]=$empresa->direccion;  
                   $row[10]=$empresa->telefono_local; 
                   $row[11]=$empresa->telefono_movil; 
                   $row[12]=$empresa->correo; 
                   $row[13]=$empresa->descripcion; 
                   $row[14]=$empresa->Fecha_de_Registro; 
                   $sheet->appendRow($row); // agrega una fila vacia al final del reporte

                }

            });
           // dd($empresa_reg);
           
        });
        $myFile   = $myFile->string('xls');
        $response = array(
            'name' => $name, 
            'file' => "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64," . base64_encode($myFile), 
        );

        return response()->json($response);
        //->export('xlsx');
    
    }

}
