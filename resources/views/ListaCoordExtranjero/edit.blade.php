@extends('templates/layoutlte_coord_inversionista')
@php
function extnamefile($sopAdicionalesInv2){
  $explode=explode(".",$sopAdicionalesInv2);
  for($i=0; $i <= count($explode)-1; $i++){
    if($explode[$i] == 'pdf' || $explode[$i] == 'doc' || $explode[$i] =='docx'){
         return $explode[$i];
    }
  }

}
  function extnamefile1($sopAdicionalesInv4){
  $explode=explode(".",$sopAdicionalesInv4);
  for($i=0; $i <= count($explode)-1; $i++){
    if($explode[$i] == 'pdf' || $explode[$i] == 'doc' || $explode[$i] =='docx'){
         return $explode[$i];
    }
  }

}
@endphp
@section('content')

<style type="text/css">
        .centrar{
            text-align: center;
        }

        .titulo{
            
            font-weight: bold;
        }

        .nro-correlativo{
            color: red;
            font-size: 16px;
        }

        .derecha{
            text-align: left;

        }

        .text-normal{
            font-size: 14px;
        }

        .text-medio{
            font-size: 12px;
        }
        .text-11{
            font-size: 11px;
        }
        .text-small{
            font-size: 10px;
        }

        .text-9{
            font-size: 9px;
        }

        .text-8{
            font-size: 8px;
        }

        .espacio-superior{
            margin-top: 10px;
        }

        .espacio-superior-small{
            margin-top: 5px;
        }

        .ancho-medio{
            width: 50%;
        }

        .ancho-full{
            width: 100%;
        }

        .ancho-medio-alto{
            width: 75%;
        }

        .ancho-90{
            width: 90%;
        }

        .table{
            border-collapse: collapse;
        }
        
        .borde{
            border: 1px solid #000;
        }

        .sin-borde{
            border: none;
        }

        .top{
            vertical-align: top;
        }

        .under-dotted{    
            border-bottom: 1px dotted #000;
            text-decoration: none;
        }

        .content-1{
            width: 5%;
        }

        .content-9{
            width: 20%;
        }

        .sangria-1{
            margin-left: 28px;
        }

        .sangria-2{
            margin-left: 36px;
        }

        .sangria-3{
            margin-left: 54px;
        }

        .sangria-sub{
            margin-left: 14px;
            
        }

        
        .page-break { page-break-before: always; }

        .table-center{
            margin: 0 auto; 
        }

        .justify{
            text-align: justify;
        }

        td.justify {
            padding-left: 8px;
            padding-right: 8px; 
        }
        .lineal{
            
        }
        .lineal>div{
            max-width:91%;

        }
        .lineal>*{
           display: inline-block;
           vertical-align:top;
        }

        .sangria-lista{
            list-style-type: none;
            margin: 0;
            padding: 0;
            margin-bottom: 4px;
        }

        .sangria-lista>li{
            margin-top: 5px;
            padding-left: 10px;
            padding-right: 10px;
        }
    </style>
<div class="content"  style="background-color: #fff">
	{{Form::model($solicitud,['route'=>['ListaCoordExtranjero.update',$solicitud->id],'method'=>'PATCH','id'=>'formcoordinversion'])}}
	<div class="row">
		<div class="col-md-12">
			<div class="col-md-6">
			</div>
			<div class="col-md-6 text-right">
				<h3>Estatus: <span class="text-success">{{$solicitud->estatus ? $solicitud->estatus->nombre_status : ''}}</span></h3>
			</div>
		</div>
	</div>
	<div class="panel-group">
		<div class="panel-primary">
			<div class="panel-heading">
				<div class="row">
					<div class="col-md-8"><h3>(RPI-01)-ANÁLISIS DE POTENCIAL INVERSIONISTA EXTRANJERO</h3></div>
					<div class="col-md-2"><!-- Traerme la consulta con el numero de solicitud-->
					
						<h4><b>Nº Solicitud:</b></h4>
                          {{ $solicitud->num_sol_inversionista ? $solicitud->num_sol_inversionista : ''}}
					</div>
					
					<div class="col-md-2"><!-- Traerme la consulta con la fecha de solicitud-->
						<h4><b>Fecha Solicitud:</b></h4>
						  {{ $solicitud->created_at ?  $solicitud->created_at : ''}}
					</div>
				</div>
			</div>
			<br><br>
			<div class="panel-body">
            	<div class="row">
                	<div class="col-md-12">
                	<div class="col-md-4"><button type="button" class="btn btn-warning btn-lg" data-toggle="modal" data-target="#myModal">RECAUDOS CONSIGNADOS</button></div>
                	<div class="col-md-4"><button type="button" class="btn btn-info btn-lg text-right" data-toggle="modal" data-target="#soportedoc">SOPORTE DE RECAUDOS</button></div>
                	@if(isset($docAdicionales))
                		<div class="col-md-4"><button type="button" class="btn btn-danger btn-lg text-right" data-toggle="modal" data-target="#docadicionales">DOCUMENTOS ADICIONALES</button></div>
                	@endif
                	<br><br><br><br>

						<div id="myModal" class="modal fade" role="dialog">
						  	<div class="modal-dialog modal-lg">
						    @if($solicitud->cat_tipo_usuario_id == 1)
							    <div class="modal-content">
								    <div class="modal-header">
								         <button type="button" class="close" data-dismiss="modal">&times;</button>
								        <h3 class="modal-title text-primary"><b>REQUISITOS PARA EL REGISTRO DE POTENCIALES INVERSIONISTAS</b></h3>
								        <div id="mostrar_info">
								        	<a href="#"><span class="glyphicon glyphicon-info-sign text-success"> Ver info.</span> </a>
								        </div>
								        <div id="ocultar_info" style="display: none;">
								        	<a href="#"><span class="glyphicon glyphicon-resize-small text-warning"> Ocultar info.</span></a>
								        </div>
								         
								        <div class="col-md-12" id="det_info" style="display: none;">
								        	@if($solicitud->cat_tipo_usuario_id == 1)
										    	<div class="col-md-12">
											    	<div class="text-muted text-justify">
											    		<p>Naturales</p>
														@foreach($documentosNat as $key=> $documento)
												            <ul>
												              <li >{{$key+1}}) {{$documento->nombre_documento}}</li>
												            </ul>
												        @endforeach
											        </div>
										    	</div>
									    	@else
										    	<div class="col-md-12">
										    		<div class="text-muted text-justify">
											    		<p>Juridicos</p>
														@foreach($documentosJuri as $key=> $documento)
												            <ul>
												              <li >{{$key+1}}) {{$documento->nombre_documento}}</li>
												            </ul>
												        @endforeach
											        </div>
										    	</div>
									    	@endif
									    </div>
								    </div>
								    <div class="modal-body">
								        <div class="modal-body">
								            <div role="tabpanel">
								                <ul class="nav nav-tabs" role="tablist">
								                    <li role="presentation" class="active"><a href="#p2" class="cargador-planilla" aria-controls="p2" role="tab" data-toggle="tab" id="tab-inicial"><b>Documento 1</b></a>
								                    </li>
								                    <li role="presentation"><a href="#p3" class="cargador-planilla" aria-controls="p3" role="tab" data-toggle="tab"><b>Documentos 2</b></a>
								                    </li>
								                    <li role="presentation"><a href="#p4" class="cargador-planilla" aria-controls="p4" role="tab" data-toggle="tab"><b>Documentos 3</b></a>
								                    </li>
								                </ul>
								            </div>
						                    <div class="tab-content">
						                        <div role="tabpanel" class="tab-pane active text-center" id="p2">
						                        @if(isset($extencion_file_1) && $extencion_file_1 == 'pdf')
						                            <span class="text-success"></span><br>
						                                 <embed src="{{asset($docCertificados['file_1'])}}" style="width: 100%; height: 100%;"></embed>
						                        @elseif(isset($extencion_file_1) && ($extencion_file_1 == 'docx' || $extencion_file_1 == 'doc'))
						                                <br><br>
						                                <a href="#" class="btn btn-lg btn-primary"><span><i class="glyphicon glyphicon-cloud-download"></i> </span>Descargar documento 1</a>
						                                <br><br>
						                                @else
                                  						.
                                  				@endif
						                        </div>
												<div role="tabpanel" class="tab-pane text-center" id="p3">
								                     @if(isset($extencion_file_2) && $extencion_file_2 == 'pdf')
								                        <span class="text-success"></span>
								                        <br>
								                         <embed src="{{asset($docCertificados['file_2'])}}" style="width: 100%; height: 100%;"></embed>
								                        @elseif(isset($extencion_file_2) && ($extencion_file_2 == 'docx' || $extencion_file_2 == 'doc'))
								                          <br><br>
								                          <a href="{{asset($docCertificados['file_2'])}}" class="btn btn-lg btn-primary"><span><i class="glyphicon glyphicon-cloud-download"></i> </span>Descargar documento 2</a>
								                          <br><br>
								                        @else
								                          .
								                    @endif
								                </div>
												<div role="tabpanel" class="tab-pane text-center" id="p4">
						                             @if(isset($extencion_file_3) && $extencion_file_3 == 'pdf')
						                                <span class="text-success"></span>
						                                <br>
						                               <embed src="{{asset($docCertificados['file_3'])}}" style="width: 100%; height: 100%;"></embed>
						                                @elseif(isset($extencion_file_3) && ($extencion_file_3 == 'docx' || $extencion_file_3 == 'doc'))
						                                  <br><br>
						                                  <a href="{{asset($docCertificados['file_3'])}}" class="btn btn-lg btn-primary"><span><i class="glyphicon glyphicon-cloud-download"></i> </span>Descargar documento 3</a>
						                                  <br><br>
						                                @else
						                                  .
						                            @endif
						                        </div>
						                    </div>
								        </div>
								    </div>
							    </div>
							    <div class="modal-footer"></div>
							</div>
						    @elseif($solicitud->cat_tipo_usuario_id == 2)
							    <div class="modal-content">
							      	<div class="modal-header">
							        	<button type="button" class="close" data-dismiss="modal">&times;</button>
							        	<h3 class="modal-title text-primary"><b>REQUISITOS PARA EL REGISTRO DE POTENCIALES INVERSIONISTAS</b></h3>
							        	<div id="mostrar_info">
								        	<a href="#"><span class="glyphicon glyphicon-info-sign text-success"> Ver info.</span> </a>
								        </div>
								        <div id="ocultar_info" style="display: none;">
								        	<a href="#"><span class="glyphicon glyphicon-resize-small text-warning"> Ocultar info.</span></a>
								        </div>
								         
								        <div class="col-md-12" id="det_info" style="display: none;">
									    	@if($solicitud->cat_tipo_usuario_id == 1)
										    	<div class="col-md-12">
											    	<div class="text-muted text-justify">
											    		<p>Naturales</p>
														@foreach($documentosNat as $key=> $documento)
												            <ul>
												              <li >{{$key+1}}) {{$documento->nombre_documento}}</li>
												            </ul>
												        @endforeach
											        </div>
										    	</div>
									    	@else
										    	<div class="col-md-12">
										    		<div class="text-muted text-justify">
											    		<p>Juridicos</p>
														@foreach($documentosJuri as $key=> $documento)
												            <ul>
												              <li >{{$key+1}}) {{$documento->nombre_documento}}</li>
												            </ul>
												        @endforeach
											        </div>
										    	</div>
									    	@endif
									    </div>
							     	</div>
							      	<div class="modal-body">
							        	<div class="modal-body">
							            	<div role="tabpanel">
							                	<ul class="nav nav-tabs" role="tablist">
							                    <li role="presentation" class="active"><a href="#p2" class="cargador-planilla" aria-controls="p2" role="tab" data-toggle="tab" id="tab-inicial">Documentos 1</a>
							                    </li>
							                    <li role="presentation"><a href="#p3" class="cargador-planilla" aria-controls="p3" role="tab" data-toggle="tab">Documentos 2</a>
							                    </li>
							                    <li role="presentation"><a href="#p4" class="cargador-planilla" aria-controls="p4" role="tab" data-toggle="tab">Documentos 3</a>
							                    </li>
							                    <li role="presentation"><a href="#p5" class="cargador-planilla" aria-controls="p5" role="tab" data-toggle="tab">Documentos 4</a>
							                    </li>
							                    <li role="presentation"><a href="#p6" class="cargador-planilla" aria-controls="p6" role="tab" data-toggle="tab">Documentos 5</a>
							                	</ul>
							            	</div>
							                <div class="tab-content">
												<div role="tabpanel" class="tab-pane active text-center" id="p2">
						                            @if(isset($extencion_file_1) && $extencion_file_1 == 'pdf')
						                                <span class="text-success"></span>
						                                <br>
						                                <embed src="{{asset($docCertificados['file_1'])}}" style="width: 100%; height: 100%;"></embed>
						                                @elseif(isset($extencion_file_1) && ($extencion_file_1 == 'docx' || $extencion_file_1 == 'doc'))
						                                  <br><br>
						                                  <a href="{{asset($docCertificados['file_1'])}}" class="btn btn-lg btn-primary"><span><i class="glyphicon glyphicon-cloud-download"></i> </span>Descargar documento 1</a>
						                                  <br><br>
						                                @else
						                                  .
						                            @endif
						                        </div>
												<div role="tabpanel" class="tab-pane text-center" id="p3">
						                             @if(isset($extencion_file_2) && $extencion_file_2 == 'pdf')
						                                <span class="text-success"></span>
						                                <br>
						                                 <embed src="{{asset($docCertificados['file_2'])}}" style="width: 100%; height: 100%;"></embed>
						                                @elseif(isset($extencion_file_2) && ($extencion_file_2 == 'docx' || $extencion_file_2 == 'doc'))
						                                  <br><br>
						                                  <a href="{{asset($docCertificados['file_2'])}}" class="btn btn-lg btn-primary"><span><i class="glyphicon glyphicon-cloud-download"></i> </span>Descargar documento 2</a>
						                                  <br><br>
						                                @else
						                                  .
						                            @endif
						                        </div>
												<div role="tabpanel" class="tab-pane text-center" id="p4">
						                             @if(isset($extencion_file_3) && $extencion_file_3 == 'pdf')
						                                <span class="text-success"></span>
						                                <br>
						                                 <embed src="{{asset($docCertificados['file_3'])}}" style="width: 100%; height: 100%;"></embed>
						                                @elseif(isset($extencion_file_3) && ($extencion_file_3 == 'docx' || $extencion_file_3 == 'doc'))
						                                  <br><br>
						                                  <a href="{{asset($docCertificados['file_3'])}}" class="btn btn-lg btn-primary"><span><i class="glyphicon glyphicon-cloud-download"></i> </span>Descargar documento 3</a>
						                                  <br><br>
						                                @else
						                                  .
						                            @endif
						                        </div>
												<div role="tabpanel" class="tab-pane text-center" id="p5">
						                             @if(isset($extencion_file_4) && $extencion_file_4 == 'pdf')
						                                <span class="text-success"></span>
						                                <br>
						                                 <embed src="{{asset($docCertificados['file_4'])}}" style="width: 100%; height: 100%;"></embed>
						                                @elseif(isset($extencion_file_4) && ($extencion_file_4 == 'docx' || $extencion_file_4 == 'doc'))
						                                  <br><br>
						                                  <a href="{{asset($docCertificados['file_4'])}}" class="btn btn-lg btn-primary"><span><i class="glyphicon glyphicon-cloud-download"></i> </span>Descargar documento 4</a>
						                                  <br><br>
						                                @else
						                                  .
						                            @endif
						                        </div>
						                        <div role="tabpanel" class="tab-pane text-center" id="p6">
						                             @if(isset($extencion_file_5) && $extencion_file_5 == 'pdf')
						                                <span class="text-success"></span>
						                                <br>
						                                <embed src="{{asset($docCertificados['file_5'])}}" style="width: 100%; height: 100%;"></embed>
						                                @elseif(isset($extencion_file_5) && ($extencion_file_5 == 'docx' || $extencion_file_5 == 'doc'))
						                                  <br><br>
						                                  <a href="{{asset($docCertificados['file_5'])}}" class="btn btn-lg btn-primary"><span><i class="glyphicon glyphicon-cloud-download"></i> </span>Descargar documento 5</a>
						                                  <br><br>
						                                @else
						                                  .
						                            @endif
						                        </div>
							                    </div>
							            </div>
							        </div>
							    </div>
							    <div class="modal-footer"></div>
						</div>
						    @endif
  					</div>
				</div>
            </div>
        </div>
    </div>
</div><!--1° cierre de panel primary-->

		<!--1° cierre de panel primary-->

		<div id="soportedoc" class="modal fade" role="dialog">
						  	<div class="modal-dialog modal-lg">
							    <div class="modal-content">
								    <div class="modal-header">
								        <button type="button" class="close" data-dismiss="modal">&times;</button>
								        <h3 class="modal-title text-primary"><b>SOPORTE DE RECAUDOS DE DOCUMENTO CONSIGNADOS</b></h3>
								        <div id="mostrar_info_soport">
								        	<a href="#"><span class="glyphicon glyphicon-info-sign text-success"> Ver info.</span> </a>
								        </div>
								        <div id="ocultar_info_soport" style="display: none;">
								        	<a href="#"><span class="glyphicon glyphicon-resize-small text-warning"> Ocultar info.</span></a>
								        </div>
								        <div class="col-md-12" id="det_info_soport" style="display: none;">
								        	<div class="text-muted text-justify">
								        		<br>
								        		@if($solicitud->cat_tipo_usuario_id == 1)
								        		<div class="col-md-12">
								        			<div class="text-muted text-justify">
								        				@foreach($documentosSport as $key=> $documento)
								        				<ul>
								        					<li >{{$key+1}}) {{$documento->nombre_documento}}</li>
								        				</ul>
								        				@endforeach
								        			</div>
								        		</div>
								        		@else
								        		<div class="col-md-12">
								        			<div class="text-muted text-justify">
								        				<ul>
								        					<li >Últimas actas de Asambleas en el que se reflejen la composición accionaria de la empresa.</li>
								        				</ul>
								        			</div>
								        		</div>
								        		@endif
								        	</div>
								        </div>
								    </div>
								    <div class="modal-body">
								        <div class="modal-body">
								            <div role="tabpanel">
								            	@if($solicitud->cat_tipo_usuario_id == 1)
								                <ul class="nav nav-tabs" role="tablist">
								                    <!--li role="presentation" class="active"><a href="#so1" class="cargador-planilla" aria-controls="so1" role="tab" data-toggle="tab" id="tab-inicial">Documentos 1</a>
								                    </li-->
								                    <li role="presentation"><a href="#so2" class="cargador-planilla" aria-controls="so2" role="tab" data-toggle="tab">Documentos 1</a>
								                    </li>
								                    <!--li role="presentation"><a href="#so3" class="cargador-planilla" aria-controls="so3" role="tab" data-toggle="tab">Documentos 3</a>
								                    </li-->
								                    <li role="presentation"><a href="#so4" class="cargador-planilla" aria-controls="so4" role="tab" data-toggle="tab">Documentos 2</a>
								                    </li>
								                    <!--li role="presentation"><a href="#so5" class="cargador-planilla" aria-controls="so5" role="tab" data-toggle="tab">Documentos 5</a>
								                    </li-->
								                </ul>
								               @else
								                <ul class="nav nav-tabs" role="tablist">
							                    <!--li role="presentation" class="active"><a href="#so1" class="cargador-planilla" aria-controls="so1" role="tab" data-toggle="tab" id="tab-inicial">Documentos 1</a>
							                    </li-->
							                    <li role="presentation"><a href="#so2" class="cargador-planilla" aria-controls="so2" role="tab" data-toggle="tab">Documentos 1</a>
							                    </li>
							                    <!--li role="presentation"><a href="#so3" class="cargador-planilla" aria-controls="so3" role="tab" data-toggle="tab">Documentos 3</a>
							                    </li-->
							                    <!--li role="presentation"><a href="#so4" class="cargador-planilla" aria-controls="so4" role="tab" data-toggle="tab">Documentos 4</a>
							                    </li-->
							                    <!--li role="presentation"><a href="#so5" class="cargador-planilla" aria-controls="so5" role="tab" data-toggle="tab">Documentos 5</a>
							                    </li-->
							                	</ul>
							                @endif
								            </div>
						                    <div class="tab-content">
											{{--<div role="tabpanel" class="tab-pane active text-center" id="so1">
					                           @if($solicitud->cat_tipo_usuario_id == 2)
					                            @if(isset($extencion_file_6) && $extencion_file_6 == 'pdf')
					                                <span class="text-success"></span>
					                                <br>
					                                 <embed src="{{asset($docCertificados['file_6'])}}" style="width: 100%; height: 100%;"></embed>
					                                @elseif(isset($extencion_file_6) && ($extencion_file_6 == 'docx' || $extencion_file_6 == 'doc'))
					                                  <br><br>
					                                  <a href="{{asset($docCertificados['file_6'])}}" class="btn btn-lg btn-primary"><span><i class="glyphicon glyphicon-cloud-download"></i> </span>Descargar documento 1</a>
					                                  <br><br>
					                                @else
					                                  .
					                            @endif
					                            @else
						                            @if(isset($extencion_file_4) && $extencion_file_4 == 'pdf')
						                                <span class="text-success"></span>
						                                <br>
						                                 <embed src="{{asset($docCertificados['file_4'])}}" style="width: 100%; height: 100%;"></embed>
						                                @elseif(isset($extencion_file_4) && ($extencion_file_4 == 'docx' || $extencion_file_4 == 'doc'))
						                                  <br><br>
						                                  <a href="{{asset($docCertificados['file_4'])}}" class="btn btn-lg btn-primary"><span><i class="glyphicon glyphicon-cloud-download"></i> </span>Descargar documento 1</a>
						                                  <br><br>
						                                @else
						                                  .
						                            @endif
					                           @endif
					                        </div>--}}
					                        <div role="tabpanel" class="tab-pane text-center" id="so2">
					                        @foreach($sopAdicionalesInv2 as $documentos) 
					                            @if($solicitud->cat_tipo_usuario_id == 2)
						                            @if(extnamefile($documentos->file) == 'pdf')
						                                <span class="text-success"></span>
						                                <br>
						                                <embed src="{{asset($documentos->file)}}" style="width: 100%; height: 100%;"></embed>
						                            @elseif(extnamefile($documentos->file) == 'docx' || extnamefile($documentos->file) == 'doc')
						                                  <br><br>
						                                  <a href="{{asset($documentos->file)}}" class="btn btn-lg btn-primary"><span><i class="glyphicon glyphicon-cloud-download"></i> </span>Descargar documento 1</a>
						                                  <br><br>
						                            @else
						                                  .
						                            @endif
					                            @else
						                            @if(extnamefile($documentos->file) == 'pdf')
						                                <span class="text-success"></span>
						                                <br>
						                                <embed src="{{asset($documentos->file)}}" style="width: 100%; height: 100%;"></embed>
						                            @elseif(extnamefile($documentos->file) == 'docx' || extnamefile($documentos->file) == 'doc')
						                                  <br><br>
						                                  <a href="{{asset($documentos->file)}}" class="btn btn-lg btn-primary"><span><i class="glyphicon glyphicon-cloud-download"></i> </span>Descargar documento 1</a>
						                                  <br><br>
						                            @else
						                                  .
						                            @endif
					                            @endif
					                        @endforeach
					                        </div>
					                        {{--<div role="tabpanel" class="tab-pane text-center" id="so3">
					                        	@if($solicitud->cat_tipo_usuario_id == 2)
					                             @if(isset($extencion_file_8) && $extencion_file_8 == 'pdf')
					                                <span class="text-success"></span>
					                                <br>
					                                 <embed src="{{asset($docCertificados['file_8'])}}" style="width: 100%; height: 100%;"></embed>
					                                @elseif(isset($extencion_file_8) && ($extencion_file_8 == 'docx' || $extencion_file_8 == 'doc'))
					                                  <br><br>
					                                  <a href="{{asset($docCertificados['file_8'])}}" class="btn btn-lg btn-primary"><span><i class="glyphicon glyphicon-cloud-download"></i> </span>Descargar documento 3</a>
					                                  <br><br>
					                                @else
					                                  .
					                            @endif
					                            @else
					                            @if(isset($extencion_file_6) && $extencion_file_6 == 'pdf')
					                                <span class="text-success"></span>
					                                <br>
					                                <embed src="{{asset($docCertificados['file_6'])}}" style="width: 100%; height: 100%;"></embed>
					                                @elseif(isset($extencion_file_6) && ($extencion_file_6 == 'docx' || $extencion_file_6 == 'doc'))
					                                  <br><br>
					                                  <a href="{{asset($docCertificados['file_6'])}}" class="btn btn-lg btn-primary"><span><i class="glyphicon glyphicon-cloud-download"></i> </span>Descargar documento 3</a>
					                                  <br><br>
					                                @else
					                                  .
					                            @endif
					                           @endif
					                        </div>--}}
					                        <div role="tabpanel" class="tab-pane text-center" id="so4">
					                        @foreach($sopAdicionalesInv4 as $documentos) 
					                            @if($solicitud->cat_tipo_usuario_id == 2)
						                            @if(extnamefile1($documentos->file) == 'pdf')
						                                <span class="text-success"></span>
						                                <br>
						                                <embed src="{{asset($documentos->file)}}" style="width: 100%; height: 100%;"></embed>
						                            @elseif(extnamefile1($documentos->file) == 'docx' || extnamefile1($documentos->file) == 'doc')
						                                  <br><br>
						                                  <a href="{{asset($documentos->file)}}" class="btn btn-lg btn-primary"><span><i class="glyphicon glyphicon-cloud-download"></i> </span>Descargar documento 1</a>
						                                  <br><br>
						                            @else
						                                  .
						                            @endif
					                            @else
						                            @if(extnamefile1($documentos->file) == 'pdf')
						                                <span class="text-success"></span>
						                                <br>
						                                <embed src="{{asset($documentos->file)}}" style="width: 100%; height: 100%;"></embed>
						                               @elseif(extnamefile1($documentos->file) == 'docx' || extnamefile1($documentos->file) == 'doc')
						                                  <br><br>
						                                  <a href="{{asset($documentos->file)}}" class="btn btn-lg btn-primary"><span><i class="glyphicon glyphicon-cloud-download"></i> </span>Descargar documento 1</a>
						                                  <br><br>
						                                @else
						                                  .
						                            @endif
					                            @endif
					                        @endforeach
					                        </div>
					                        {{--<div role="tabpanel" class="tab-pane text-center" id="so5">
					                        	@if($solicitud->cat_tipo_usuario_id == 2)
					                             @if(isset($extencion_file_10) && $extencion_file_10 == 'pdf')
					                                <span class="text-success"></span>
					                                <br>
					                                 <embed src="{{asset($docCertificados['file_10'])}}" style="width: 100%; height: 100%;"></embed>
					                                @elseif(isset($extencion_file_10) && ($extencion_file_10 == 'docx' || $extencion_file_10 == 'doc'))
					                                  <br><br>
					                                  <a href="{{asset($docCertificados['file_10'])}}" class="btn btn-lg btn-primary"><span><i class="glyphicon glyphicon-cloud-download"></i> </span>Descargar documento 5</a>
					                                  <br><br>
					                                @else
					                                  .
					                            @endif
					                            @else
					                            @if(isset($extencion_file_8) && $extencion_file_8 == 'pdf')
					                                <span class="text-success"></span>
					                                <br>
					                                 <embed src="{{asset($docCertificados['file_8'])}}" style="width: 100%; height: 100%;"></embed>
					                                @elseif(isset($extencion_file_8) && ($extencion_file_8 == 'docx' || $extencion_file_8 == 'doc'))
					                                  <br><br>
					                                  <a href="{{asset($docCertificados['file_8'])}}" class="btn btn-lg btn-primary"><span><i class="glyphicon glyphicon-cloud-download"></i> </span>Descargar documento 5</a>
					                                  <br><br>
					                                @else
					                                  .
					                            @endif
					                           @endif
					                        </div>--}}
						                    </div>
								        </div>
								    </div>
							    </div>
							    <div class="modal-footer"></div>
							</div>
						</div>
						<!-- Modal para documentos Adicionales-->
	<div id="docadicionales" class="modal fade" role="dialog">
	  	<div class="modal-dialog modal-lg">
		    <div class="modal-content">
			    <div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal">&times;</button>
			        <h3 class="modal-title text-primary"><b>DOCUMENTOS ADICIONALES</b></h3>
			    </div>
			    <div class="modal-body">
			        <div class="modal-body">
			            <div role="tabpanel">
			                <ul class="nav nav-tabs" role="tablist">
			                    <li role="presentation" class="active"><a href="#doc1" class="cargador-planilla" aria-controls="doc1" role="tab" data-toggle="tab" id="tab-inicial">Documento 1</a>
			                    </li>
			                    <li role="presentation"><a href="#doc2" class="cargador-planilla" aria-controls="doc2" role="tab" data-toggle="tab">Documento 2</a>
			                    </li>
			                    <li role="presentation"><a href="#doc3" class="cargador-planilla" aria-controls="doc3" role="tab" data-toggle="tab">Documento 3</a>
			                    </li>
			                    <li role="presentation"><a href="#doc4" class="cargador-planilla" aria-controls="doc4" role="tab" data-toggle="tab">Documento 4</a>
			                    </li>
			                    <li role="presentation"><a href="#doc5" class="cargador-planilla" aria-controls="doc5" role="tab" data-toggle="tab">Documento 5</a>
			                    </li>
			                    <li role="presentation"><a href="#doc6" class="cargador-planilla" aria-controls="doc6" role="tab" data-toggle="tab">Documento 6</a>
			                    </li>
			                </ul>
			            </div>
	                    <div class="tab-content">
						<div role="tabpanel" class="tab-pane active text-center" id="doc1">
						@if(isset($docAdicionales))
				            @if(isset($extencion_adi_file_1) && $extencion_adi_file_1 == 'pdf')
                                <span class="text-success"></span>
                                <br>
                                 <embed src="{{asset($docAdicionales['file_1'])}}" style="width: 100%; height: 100%;"></embed>
                                @elseif(isset($extencion_adi_file_1) && ($extencion_adi_file_1 == 'docx' || $extencion_adi_file_1 == 'doc'))
                                  <br><br>
                                  <a href="{{asset($docAdicionales['file_1'])}}" class="btn btn-lg btn-primary"><span><i class="glyphicon glyphicon-cloud-download"></i> </span>Descargar documento 1</a>
                                  <br><br>
                                @else
                                  .
                            @endif
                         @endif
                        </div>
                        <div role="tabpanel" class="tab-pane text-center" id="doc2">
                        @if(isset($docAdicionales))
                            @if(isset($extencion_adi_file_2) && $extencion_adi_file_2 == 'pdf')
                                <span class="text-success"></span>
                                <br>
                                 <embed src="{{asset($docAdicionales['file_2'])}}" style="width: 100%; height: 100%;"></embed>
                                @elseif(isset($extencion_adi_file_2) && ($extencion_adi_file_2 == 'docx' || $extencion_adi_file_2 == 'doc'))
                                  <br><br>
                                  <a href="{{asset($docAdicionales['file_2'])}}" class="btn btn-lg btn-primary"><span><i class="glyphicon glyphicon-cloud-download"></i> </span>Descargar documento 1</a>
                                  <br><br>
                                @else
                                  .
                            @endif
                        @endif
                        </div>
                        <div role="tabpanel" class="tab-pane text-center" id="doc3">
                        @if(isset($docAdicionales))
                           @if(isset($extencion_adi_file_3) && $extencion_adi_file_3 == 'pdf')
                                <span class="text-success"></span>
                                <br>
                                 <embed src="{{asset($docAdicionales['file_3'])}}" style="width: 100%; height: 100%;"></embed>
                                @elseif(isset($extencion_adi_file_3) && ($extencion_adi_file_3 == 'docx' || $extencion_adi_file_3 == 'doc'))
                                  <br><br>
                                  <a href="{{asset($docAdicionales['file_3'])}}" class="btn btn-lg btn-primary"><span><i class="glyphicon glyphicon-cloud-download"></i> </span>Descargar documento 1</a>
                                  <br><br>
                                @else
                                  .
                            @endif
                        @endif
                        </div>
                        <div role="tabpanel" class="tab-pane text-center" id="doc4">
                        @if(isset($docAdicionales))
                            @if(isset($extencion_adi_file_4) && $extencion_adi_file_4 == 'pdf')
                                <span class="text-success"></span>
                                <br>
                                 <embed src="{{asset($docAdicionales['file_4'])}}" style="width: 100%; height: 100%;"></embed>
                                @elseif(isset($extencion_adi_file_4) && ($extencion_adi_file_4 == 'docx' || $extencion_adi_file_4 == 'doc'))
                                  <br><br>
                                  <a href="{{asset($docAdicionales['file_4'])}}" class="btn btn-lg btn-primary"><span><i class="glyphicon glyphicon-cloud-download"></i> </span>Descargar documento 1</a>
                                  <br><br>
                                @else
                                  .
                            @endif
                        @endif
                        </div>
                        <div role="tabpanel" class="tab-pane text-center" id="doc5">
                        @if(isset($docAdicionales))
                            @if(isset($extencion_adi_file_5) && $extencion_adi_file_5 == 'pdf')
                                <span class="text-success"></span>
                                <br>
                                <embed src="{{asset($docAdicionales['file_5'])}}" style="width: 100%; height: 100%;"></embed>
                                @elseif(isset($extencion_adi_file_5) && ($extencion_adi_file_5 == 'docx' || $extencion_adi_file_5 == 'doc'))
                                  <br><br>
                                  <a href="{{asset($docAdicionales['file_5'])}}" class="btn btn-lg btn-primary"><span><i class="glyphicon glyphicon-cloud-download"></i> </span>Descargar documento 1</a>
                                  <br><br>
                                @else
                                  .
                            @endif
                        @endif
                        </div>
                        <div role="tabpanel" class="tab-pane text-center" id="doc6">
                        @if(isset($docAdicionales))
                            @if(isset($extencion_adi_file_6) && $extencion_adi_file_6 == 'pdf')
                                <span class="text-success"></span>
                                <br>
                                 <embed src="{{asset($docAdicionales['file_6'])}}" style="width: 100%; height: 100%;"></embed>
                                @elseif(isset($extencion_adi_file_6) && ($extencion_adi_file_6 == 'docx' || $extencion_adi_file_6 == 'doc'))
                                  <br><br>
                                  <a href="{{asset($docAdicionales['file_6'])}}" class="btn btn-lg btn-primary"><span><i class="glyphicon glyphicon-cloud-download"></i> </span>Descargar documento 1</a>
                                  <br><br>
                                @else
                                  .
                            @endif
                        @endif
                        </div>
	                    </div>
			        </div>
			    </div>
		    </div>
		    <div class="modal-footer"></div>
		</div>
	</div>

<!--Fin de Documentos Adicinales-->
      <div class="panel-body">
        <div class="row">
        	<div class="col-md-12">	
        		<div class="col-md-3">
        			<h5 style="color: #000;"><b>1. TIPO DE PERSONA:</b></h5>
        		</div>
        		<div class="col-md-3">
        			<h5 style="color: #000;"></h5>
        			  {{ $solicitud->CatTipoUsuario ? $solicitud->CatTipoUsuario->nombre : ''}}
        		</div>
        		<div class="col-md-3">
        			<h5 style="color: #000;"><b>2. PAIS DE ORIGEN:</b></h5>
        		</div>
        		<div class="col-md-3">
        			<h5 style="color: #000;"></h5>
        			   {{ $pais->dpais ? $pais->dpais : ''}}
        		</div>
        	</div>	
        </div>
        @if($solicitud->cat_tipo_usuario_id == 1)
        <div class="row">
        	<div class="col-md-12">	
        		<div class="col-md-3">
                 <h5 style="color: #000;"><b>3. NOMBRE O RAZÓN SOCIAL:</b></h5>
        		</div>
        		<div class="col-md-5">
        			<h5 style="color: #000;"></h5>
                      {{ $det_usuario->razon_social ? $det_usuario->razon_social : ''}}
        		</div>
        	</div>	
        </div>
        <div class="row">
        	<div class="col-md-12">	
        		<div class="col-md-3">
        			<h5 style="color: #000;"><b>4. N° DE DOCUMENTO DE IDENTIDAD:</b></h5>
        		</div>
        		<div class="col-md-3">
        			<h5 style="color: #000;"></h5>
        			  {{ $solicitud->n_doc_identidad ?  $solicitud->n_doc_identidad : ''}}
        		</div>
        		<div class="col-md-3">
        			<h5 style="color: #000;"><b>5. N° DE PASAPORTE:</b></h5>
        		</div>
        		<div class="col-md-3">
        			<h5 style="color: #000;"></h5>
        			  {{ $det_usuario->ci_repre ? $det_usuario->ci_repre : ''}}
        		</div>
        	</div>	
        </div>
        <div class="row">
        	<div class="col-md-12">	
        		<div class="col-md-3">
        			<h5 style="color: #000;"><b>6. DOMICILIO EN EL EXTERIOR:</b></h5>
        		</div>
        		<div class="col-md-9">
        			<h5 style="color: #000;"></h5>
        			  {{ $det_usuario->direccion ? $det_usuario->direccion : ''}}
        		</div>	
        	</div>
		</div>
		<div class="row">
        	<div class="col-md-12">	
        		<div class="col-md-3">
        			<h5 style="color: #000;"><b>7. NÚMERO TELEFÓNICO:</b></h5>
        		</div>
        		<div class="col-md-3">
        			<h5 style="color: #000;"></h5>
        			 <b>PERSONAL : </b>{{ $det_usuario->telefono_movil ?  $det_usuario->telefono_movil : ''}} <br>
        			 <b>OFICINA:</b> {{ $det_usuario->telefono_local ? $det_usuario->telefono_local : ''}}
        		</div>
        		<div class="col-md-3">
        			<h5 style="color: #000;"><b>8. CORREO ELECTRÓNICO:</b></h5>
        			<h5 style="color: #000;"><b>9. PAGINA WEB:</b></h5>
        		</div>
        		<div class="col-md-3">
        			{{ $det_usuario->usuario ? $det_usuario->usuario->email : ''}}<br><br>
        			{{ $det_usuario->pagina_web ? $det_usuario->pagina_web : ''}}
        		</div>	
        	</div>
		</div><br>
		<div class="row">
        	<div class="col-md-12">	
        		<div class="col-md-3">
                 <h5 style="color: #000;"><b>10. TIPO DE INVERSIÓN:</b></h5>
        		</div>
        		<div class="col-md-5">
        			<h5 style="color: #000;"></h5>
                       {{ $solicitud->tipo_inversion ? $solicitud->tipo_inversion : ''}}
        		</div>
        	</div>	
        </div>
        <div class="row">
        	<div class="col-md-12">	
        		<div class="col-md-3">
                 <h5 style="color: #000;"><b>11. MONTO DE LA INVERSIÓN:</b></h5>
        		</div>
        		<div class="col-md-3">
        			<h5 style="color: #000;"></h5>
					<b>{{ $solicitud->divisas ? $solicitud->divisas->ddivisa_abr : ''}}</b> {{ $solicitud->monto_inversion ? $solicitud->monto_inversion : ''}}
        		</div>
        		<div class="col-md-3">
        			<h5 style="color:#000;"><b>ESCALA DE INVERSIÓN:</b></h5>
        		</div>
        		<div class="col-md-3">
        			<h5 style="color:#000;"></h5>
        			@if($solicitud->monto_inversion >= 10000 && $solicitud->monto_inversion < 40000)
                        Baja
                      @elseif($solicitud->monto_inversion >= 40000 && $solicitud->monto_inversion < 80000)
                        Media
                      @elseif($solicitud->monto_inversion >= 80000)
                        Alta
                      </td>
                    @endif
        		</div>
        	</div>	
        </div>
        <div class="row">
        	<div class="col-md-12">	
        		<div class="col-md-3">
                 <h5 style="color: #000;"><b>12. BREVE DESCRIPCIÓN DEL PROYECTO DE INVERSIÓN:</b></h5>
        		</div>
        		<div class="col-md-5">
        			<h5 style="color: #000;"></h5>
                     {{ $solicitud->descrip_proyec_inver ? $solicitud->descrip_proyec_inver : ''}}
        		</div>
        	</div>	
        </div>
        <div class="row">
        	<div class="col-md-12">	
        		<div class="col-md-3">
                 <h5 style="color: #000;"><b>13. ACTIVIDAD ECONÓMICA DE LA EMPRESA:</b></h5>
        		</div>
        		<div class="col-md-5">
        		
        			<h5 style="color: #000;"></h5>
                     {{ $solicitud->CatSectorInversionista ? $solicitud->CatSectorInversionista->nombre : ''}} :
                     @if($solicitud->cat_sector_inversionista_id==11)
                     	{{$solicitud->especif_otro_sector ? $solicitud->especif_otro_sector : ''}}
                     @endif

                     <b style="color: black"> - </b>
                     {{ $solicitud->CatActividadEcoInversionista ? $solicitud->CatActividadEcoInversionista->nombre : ''}} :
                      @if($solicitud->actividad_eco_emp==66 || $solicitud->actividad_eco_emp==67 || $solicitud->actividad_eco_emp==68 || $solicitud->actividad_eco_emp==69 || $solicitud->actividad_eco_emp==70 || $solicitud->actividad_eco_emp==71 || $solicitud->actividad_eco_emp==72 || $solicitud->actividad_eco_emp==73 || $solicitud->actividad_eco_emp==74 || $solicitud->actividad_eco_emp==75 || $solicitud->actividad_eco_emp==76)
                     	{{$solicitud->especif_otro_actividad ? $solicitud->especif_otro_actividad : ''}}
                     @endif
        		</div>
        	</div>	
        </div>
        <div class="row">
        	<div class="col-md-12">	
        		<div class="col-md-3">
        			<h5 style="color: #000;"><b>14. CORREO ELECTRONICO:</b></h5>
        		</div>
        		<div class="col-md-3">
        			<h5 style="color: #000;"></h5>
        			  {{ $solicitud->correo_emp_recep ? $solicitud->correo_emp_recep : ''}}
        		</div>
        		<div class="col-md-3">
        			<h5 style="color: #000;"><b>15. PAGINA WEB:</b></h5>
        		</div>
        		<div class="col-md-3">
        			<h5 style="color: #000;"></h5>
        			 {{ $solicitud->pag_web_emp_recep ? $solicitud->pag_web_emp_recep : ''}}
        		</div>
        	</div>	
        </div><br><br>
        <div class="row">
            <center><div class="col-md-12"> 
             <h5 style="color: #000;"><b>REPRESENTANTE LEGAL O APODERADO</b></h5>
            </div></center>
        </div>
        <div class="row">
        	<div class="col-md-12">	
        		<div class="col-md-3">
        			<h5 style="color: #000;"><b>NOMBRE:</b></h5>
        		</div>
        		<div class="col-md-3">
        			<h5 style="color: #000;"></h5>
        			  {{ $solicitud->nombre_repre_legal ? $solicitud->nombre_repre_legal : ''}}
        		</div>
        		<div class="col-md-3">
        			<h5 style="color: #000;"><b>APELLIDO:</b></h5>
        		</div>
        		<div class="col-md-3">
        			<h5 style="color: #000;"></h5>
        			 {{ $solicitud->apellido_repre_legal ? $solicitud->apellido_repre_legal : ''}}
        		</div>
        	</div>	
        </div><br><br><br>

        @elseif($solicitud->cat_tipo_usuario_id == 2)


        <div class="row">
        	<div class="col-md-12">	
        		<div class="col-md-3">
                 <h5 style="color: #000;"><b>3. NOMBRE O RAZON SOCIAL:</b></h5>
        		</div>
        		<div class="col-md-5">
        			<h5 style="color: #000;"></h5>
                      {{ $det_usuario->razon_social ? $det_usuario->razon_social : ''}}
        		</div>
        	</div>	
        </div>
        <div class="row">
        	<div class="col-md-12">	
        		<div class="col-md-3">
        			<h5 style="color: #000;"><b>4. DOMICILIO EN EL EXTERIOR:</b></h5>
        		</div>
        		<div class="col-md-9">
        			<h5 style="color: #000;"></h5>
        			  {{ $det_usuario->direccion ? $det_usuario->direccion : ''}}
        		</div>	
        	</div>
		</div>
		<div class="row">
        	<div class="col-md-12">	
        		<div class="col-md-3">
        			<h5 style="color: #000;"><b>5. NUMERO TELEFÓNICO:</b></h5>
        		</div>
        		<div class="col-md-3">
        			<h5 style="color: #000;"></h5>
        			 <b>CELULAR : </b>{{ $det_usuario->telefono_movil ? $det_usuario->telefono_movil : ''}} <br>
        			 <b>FIJO:</b> {{ $det_usuario->telefono_local ?  $det_usuario->telefono_local : ''}}
        		</div>
        		<div class="col-md-3">
        			<h5 style="color: #000;"><b>6. CORREO ELECTRONICO:</b></h5>
        			<h5 style="color: #000;"><b>7. PAGINA WEB:</b></h5>
        		</div>
        		<div class="col-md-3">
        			{{ $det_usuario->usuario ? $det_usuario->usuario->email : ''}}<br><br>
        			{{ $det_usuario->pagina_web ? $det_usuario->pagina_web : ''}}
        		</div>	
        	</div>
		</div>
		<div class="row">
        	<div class="col-md-12">	
        		<div class="col-md-3">
                 <h5 style="color: #000;"><b>8. TIPO DE INVERSION:</b></h5>
        		</div>
        		<div class="col-md-5">
        			<h5 style="color: #000;"></h5>
                       {{ $solicitud->tipo_inversion ? $solicitud->tipo_inversion : ''}}
        		</div>
        	</div>	
        </div>
        <div class="row">
        	<div class="col-md-12">	
        		<div class="col-md-3">
                 <h5 style="color: #000;"><b>9. MONTO DE LA INVERSION:</b></h5>
        		</div>
        		<div class="col-md-3">
        			<h5 style="color: #000;"></h5>
                    <b>{{ $solicitud->divisas ? $solicitud->divisas->ddivisa_abr : ''}}</b> {{ $solicitud->monto_inversion ? $solicitud->monto_inversion : ''}}
        		</div>
        		<div class="col-md-3">
        			<h5 style="color:#000;"><b>ESCALA DE INVERSIÓN:</b></h5>
        		</div>
        		<div class="col-md-3">
        			<h5 style="color:#000;"></h5>
        			@if($solicitud->monto_inversion >= 10000 && $solicitud->monto_inversion < 40000)
                        Baja
                      @elseif($solicitud->monto_inversion >= 40000 && $solicitud->monto_inversion < 80000)
                        Media
                      @elseif($solicitud->monto_inversion >= 80000)
                        Alta
                      </td>
                    @endif
        		</div>
        	</div>	
        </div>
        <div class="row">
        	<div class="col-md-12">	
        		<div class="col-md-3">
                 <h5 style="color: #000;"><b>10. BREVE DESCRIPCION DEL PROYECTO DE INVERSION:</b></h5>
        		</div>
        		<div class="col-md-5">
        			<h5 style="color: #000;"></h5>
                     {{ $solicitud->descrip_proyec_inver ? $solicitud->descrip_proyec_inver : ''}}
        		</div>
        	</div>	
        </div>
         <div class="row">
        	<div class="col-md-12">	
        		<div class="col-md-3">
                 <h5 style="color: #000;"><b>11. ACTIVIDAD ECONOMICA DE LA EMPRESA:</b></h5>
        		</div>
        		<div class="col-md-5">
        			<h5 style="color: #000;"></h5>
                      {{ $solicitud->CatSectorInversionista ? $solicitud->CatSectorInversionista->nombre : ''}} :
                     @if($solicitud->cat_sector_inversionista_id==11)
                     	{{$solicitud->especif_otro_sector ? $solicitud->especif_otro_sector : ''}}
                     @endif

                     <b style="color: black"> - </b>
                     {{ $solicitud->CatActividadEcoInversionista ? $solicitud->CatActividadEcoInversionista->nombre : ''}} :
                      @if($solicitud->actividad_eco_emp==66 || $solicitud->actividad_eco_emp==67 || $solicitud->actividad_eco_emp==68 || $solicitud->actividad_eco_emp==69 || $solicitud->actividad_eco_emp==70 || $solicitud->actividad_eco_emp==71 || $solicitud->actividad_eco_emp==72 || $solicitud->actividad_eco_emp==73 || $solicitud->actividad_eco_emp==74 || $solicitud->actividad_eco_emp==75 || $solicitud->actividad_eco_emp==76)
                     	{{ $solicitud->especif_otro_actividad ?  $solicitud->especif_otro_actividad : ''}}
                     @endif
        		</div>
        	</div>	
        </div>
        <div class="row">
        	<div class="col-md-12">	
        		<div class="col-md-3">
        			<h5 style="color: #000;"><b>12. CORREO ELECTRONICO:</b></h5>
        		</div>
        		<div class="col-md-3">
        			<h5 style="color: #000;"></h5>
        			  {{ $solicitud->correo_emp_recep ? $solicitud->correo_emp_recep : ''}}
        		</div>
        		<div class="col-md-3">
        			<h5 style="color: #000;"><b>13. PAGINA WEB:</b></h5>
        		</div>
        		<div class="col-md-3">
        			<h5 style="color: #000;"></h5>
        			 {{ $solicitud->pag_web_emp_recep ? $solicitud->pag_web_emp_recep : ''}}
        		</div>
        	</div>	
        </div> <br><br>

        <div class="row">
            <center><div class="col-md-12"> 
             <h5 style="color: #000;"><b>REPRESENTANTE LEGAL O APODERADO</b></h5>
            </div></center>
        </div>
        <div class="row">
        	<div class="col-md-12">	
        		<div class="col-md-3">
        			<h5 style="color: #000;"><b>NOMBRE:</b></h5>
        		</div>
        		<div class="col-md-3">
        			<h5 style="color: #000;"></h5>
        			  {{ $solicitud->nombre_repre_legal ? $solicitud->nombre_repre_legal : ''}}
        		</div>
        		<div class="col-md-3">
        			<h5 style="color: #000;"><b>APELLIDO:</b></h5>
        		</div>
        		<div class="col-md-3">
        			<h5 style="color: #000;"></h5>
        			 {{ $solicitud->apellido_repre_legal ? $solicitud->apellido_repre_legal : ''}}
        		</div>
        	</div>	
        </div>

        @endif
			<div class="row">
			 <div class="col-md-12">
               <div class="form-group">
                  <label>Estado de la Observación</label><br>
                  {!!Form::select('gen_status_id',$estado_observacion,null,['placeholder'=>'Seleccione','class'=>'form-control','id'=>'gen_status_id','onchange'=>'validarobservacion ()'])!!}
                </div>
               </div>
			</div>
            <br>

			<div class="row" id="observacion" style="display:none;">
				<b><u>OBSERVACIONES:</u></b>
				<textarea rows="4"  class="form-control text-justify" name="observacion_inversionista" id="observacion_inversionista" style="font-weight:bold; text-transform: capitalize;" ></textarea>
			</div>
	        <div class="row text-center"><br><br>
                    <a class="btn btn-primary" href="{{route('ListaCoordExtranjero.index')}}">Cancelar</a>
		     		<input type="submit" name="Guardar Observacion" class="btn btn-danger center" value="Guardar Observacion" onclick="return analisisinversion ()">
		     		
			</div>
		</div>
		</div>
	</div>
</div>
{{Form::close()}}



@stop
<script src="{{asset('js/ajax_v2.1.1.js')}}"></script>

<script type="text/javascript">
$(document).ready(function() {
	$('#mostrar_info').click(function(event) {
          $('#mostrar_info').hide(1000).animate({height: "hide"}, 1000,"linear");
          $('#det_info, #ocultar_info').show(1000).animate({width: "show"}, 1000,"linear");  

	});
	$('#ocultar_info').click(function(event) {
          $('#det_info,#ocultar_info').hide(1000).animate({height: "hide"}, 1000,"linear");
          $('#mostrar_info').show(1000).animate({width: "show"}, 1000,"linear");
	});
	$('#mostrar_info_soport').click(function(event) {
          $('#mostrar_info_soport').hide(1000).animate({height: "hide"}, 1000,"linear");
          $('#det_info_soport, #ocultar_info_soport').show(1000).animate({width: "show"}, 1000,"linear");  

	});
	$('#ocultar_info_soport').click(function(event) {
          $('#det_info_soport,#ocultar_info_soport').hide(1000).animate({height: "hide"}, 1000,"linear");
          $('#mostrar_info_soport').show(1000).animate({width: "show"}, 1000,"linear");
	});
	
});
</script>	 


