<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Planilla3 extends Model
{
    protected $table = 'planilla_3';
    protected $fillable = [

           'det_declaracion_produc_id',
           'gen_divisa_id',
           'utilidad',
           'precio_puerta_fabri',
           'flete_interno',
           'precio_fob_export',
           'descrip_observacion',
           'bactivo',
          
    ];
   
   public function declaracionProduc() {

        return $this->hasOne('App\Models\DetDeclaracionProduc','id','det_declaracion_produc_id');

    }

    public function GenDivisa() {

        return $this->hasOne('App\Models\GenDivisa','id','gen_divisa_id');

    }

    public function costosDirectos(){
        return $this->hasOne('App\Models\CostosDirectos','planilla_3_id','id');
    }

    public function costosInDirectos(){
        return $this->hasOne('App\Models\CostosInDirectos','planilla_3_id','id');
    }

}
