<?php

namespace App\Http\Controllers;

use App\Models\Permiso;
use App\Models\GenAduanaSalida;
use App\Models\Pais;
use App\Models\TipoPermiso;
use App\Models\DetProdFactura;
use App\Models\GenUnidadMedida;
use App\Models\Productos;
use App\Models\PermisoProductos;
use App\Http\Requests\PermisoRequest;
use Session;
use Auth;
use Laracasts\Flash\Flash;
use Alert;
use Illuminate\Http\Request;

class PermisoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $titulo="Permisos";
        $descripcion="Consulta de Permisos";
        $permiso=Permiso::where('gen_usuario_id',Auth::user()->id)->where('bactivo',1)->where('numero_permiso','!=','Sin Permiso')->get();
        //dd($permiso);
        return view('Permisos.index',compact('permiso','titulo','descripcion'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

        $titulo="Registro de Permisos";
        $descripcion="Registro de Permisos";
        $aduanasal=GenAduanaSalida::all()->pluck('daduana','id');
        $pais=Pais::all()->pluck('dpais','id');
        $tpermiso=TipoPermiso::where('nombre_permiso','!=','Sin Permiso')->pluck('nombre_permiso','id');
        return view('Permisos.create', compact('aduanasal','pais','tpermiso','titulo','descripcion'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PermisoRequest $request)
    {   $fecha_actual = date("d-m-Y");
        if((strtotime($request->fecha_aprobacion)) < (strtotime($request->fecha_permiso))){
        Alert::warning('No puede ser inferior a la Fecha del Permiso', 'Error en la Fecha Aprobación de Permiso!')->persistent("OK");
        return redirect()->action('PermisoController@create');
      }if((strtotime($request->fecha_permiso)) > (strtotime($fecha_actual))){
          Alert::warning('No puede ser superior a la fecha Actual', 'Error en la Fecha de Permiso!')->persistent("OK");
        return redirect()->action('PermisoController@create');
      } else {

             //consulto ususario que acabo de registrar
            $sin_permiso=Permiso::where('numero_permiso', '=', 'Sin Permiso')->where('gen_usuario_id',Auth::user()->id)->first();

            if (empty($sin_permiso)) {
                $permiso=new Permiso;
                $permiso->tipo_permiso_id=5;
                $permiso->gen_usuario_id=Auth::user()->id;
                $permiso->gen_aduana_salida_id=1;
                $permiso->pais_id=168;
                $permiso->numero_permiso='Sin Permiso';
                $permiso->monto_aprobado=0;
                $permiso->fecha_permiso=date('Y-m-d');
                $permiso->fecha_aprobacion=date('Y-m-d');
                $permiso->save();

            }

            $permiso=new Permiso;
            $permiso->tipo_permiso_id=$request->tipo_permiso_id;
            $permiso->gen_usuario_id=Auth::user()->id;
            $permiso->gen_aduana_salida_id=$request->gen_aduana_salida_id;
            $permiso->pais_id=$request->pais_id;
            $permiso->numero_permiso=$request->numero_permiso;
            $permiso->monto_aprobado=$request->monto_aprobado;
            $permiso->fecha_permiso=$request->fecha_permiso;
            $permiso->fecha_aprobacion=$request->fecha_aprobacion;
            $permiso->save();


            //consulto ususario que acabo de registrar
            $datos_permiso=Permiso::where('numero_permiso', '=', $request['numero_permiso'])->first();
            Session::put('permiso_id',$datos_permiso->id);
              
               //Alert::success('REGISTRO EXITOSO','Permiso agregado')->persistent("Ok");
               //return view('ProdutPermisos.create');
            return redirect()->action('ProdutPermisosController@create');
      }   
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\permiso  $permiso
     * @return \Illuminate\Http\Response
     */
    public function show(Permiso $permiso)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\permiso  $permiso
     * @return \Illuminate\Http\Response
     */
    public function edit(Permiso $permiso, $id)
    {
        $titulo="Editar Permisos";
        $descripcion="Editar Datos";
        $permis=Permiso::find($id);
        $aduanasal=GenAduanaSalida::all()->pluck('daduana','id');
        $pais=Pais::all()->pluck('dpais','id');
        $tpermiso=TipoPermiso::all()->pluck('nombre_permiso','id');
        $medidas=GenUnidadMedida::all()->pluck('dunidad','id');
        return view('Permisos.edit', compact('permis','aduanasal','pais','tpermiso','titulo','descripcion','medidas'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\permiso  $permiso
     * @return \Illuminate\Http\Response
     */
    public function update(PermisoRequest $request, Permiso $permiso, $id)
    {   
        $fecha_actual = date("d-m-Y");
        if((strtotime($request->fecha_aprobacion)) < (strtotime($request->fecha_permiso))){
        Alert::warning('No puede ser inferior a la Fecha del Permiso', 'Error en la Fecha Aprobación de Permiso!')->persistent("OK");
        $titulo="Editar Permisos";
        $descripcion="Editar Datos";
        $permis=Permiso::find($id);
        $aduanasal=GenAduanaSalida::all()->pluck('daduana','id');
        $pais=Pais::all()->pluck('dpais','id');
        $tpermiso=TipoPermiso::all()->pluck('nombre_permiso','id');
        $medidas=GenUnidadMedida::all()->pluck('dunidad','id');
        return view('Permisos.edit', compact('permis','aduanasal','pais','tpermiso','titulo','descripcion','medidas'));
      }if((strtotime($request->fecha_permiso)) > (strtotime($fecha_actual))){
          Alert::warning('No puede ser superior a la fecha Actual', 'Error en la Fecha de Permiso!')->persistent("OK");
        $titulo="Editar Permisos";
        $descripcion="Editar Datos";
        $permis=Permiso::find($id);
        $aduanasal=GenAduanaSalida::all()->pluck('daduana','id');
        $pais=Pais::all()->pluck('dpais','id');
        $tpermiso=TipoPermiso::all()->pluck('nombre_permiso','id');
        $medidas=GenUnidadMedida::all()->pluck('dunidad','id');
        return view('Permisos.edit', compact('permis','aduanasal','pais','tpermiso','titulo','descripcion','medidas'));
      }else{
        Permiso::find($id)->update($request->all());
       
       
        return redirect("exportador/ProdutPermisos/$id/edit");
      }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\permiso  $permiso
     * @return \Illuminate\Http\Response
     */
    public function destroy(Permiso $permiso, $id)
    {
         $eliper=DetProdFactura::where('permiso_id',$id)->first();
        if (!empty($eliper)) {
            return 5;
        }else{
             $permiso=Permiso::find($id)->update(['bactivo' => 0, 'updated_at' => date("Y-m-d H:i:s") ]);
        if ($permiso) {
           return 1;
        }else{
            return 2;
        }
        }
    }
}
