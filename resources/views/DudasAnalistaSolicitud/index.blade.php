@extends('templates/layoutlte_analist_djo')
@section('content')
<div class="panels">
   <hr>
    <table id="listaAgenteAduanal" class="table table-striped table-bordered" style="width:100%">
            <thead>
                <tr>
                    <th class="col-md-1 text-center">Nº</th>
                    <th class="col-md-2 text-center">Tipo</th>
                    <th class="col-md-3 text-center">Email Solicitante</th>
                    <th class="col-md-3 text-center">Fecha de Solicitud</th>
                    <th class="col-md-1 text-center">Estatus</th>
                    <th class="col-md-2 text-center">Observación Analista</th>
                    <th class="col-md-2 text-center">Acciones</th>
                </tr>
            </thead>
            <tbody>
                @foreach($solicitudes as $key=> $solicitud)
                <tr>
                    <td class="col-md-1 text-center">{{$solicitud->id}}</td>
                    <td class="col-md-2 text-center">{{$solicitud->motivo_de_contacto}}</td>
                    <td class="col-md-3 text-center">{{$solicitud->email}}</td>
                    <td class="col-md-3 text-center">{{$solicitud->created_at}}</td>
                    <td class="col-md-1 text-center">{{$solicitud->nombre_status}}</td>
                    <td class="col-md-2 text-center"><a href="{{ route('DudasAnalistaSolicitud.show',$solicitud->id) }}" class="text-center btn btn-sm btn-info list-inline"><i class="glyphicon glyphicon-eye-open"></i><b> Ver </b></a></td>
                    <td class="col-md-2 text-center">
                        @if($solicitud->status==9)
                            <a href="{{ route('DudasAnalistaSolicitud.edit',$solicitud->id) }}" class="text-center btn btn-sm btn-danger list-inline"><i class="glyphicon glyphicon-share"></i> <b>Atender</b></a>
                        @elseif($solicitud->status == 10)
                            <a class="text-center btn btn-sm btn-success list-inline"><i class="glyphicon glyphicon-ok"></i> <b>Atendido</b></a>
                        @endif
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
  </div>
</div>
</div>

@stop
