@extends('templates/layoutlte_seniat')
@section('content')
<div class="content" style="background-color: #fff">
	<div class="panel-group" style="background-color: #fff">
		<div class="panel-default">
			<div class="panel-heading"><h3>Certificado de Origen</h3></div>
			<div class="panel-body">
      
			<div class="row">
				<div class="col-md-12">
					<div class="row">
						<div class="col-md-2">
							<form method="GET" action="{{url('/Seniat/ViceministroSeniat-lista')}}">
								<button class=" btn btn-primary form-control"><b>Nuevas</b></button>
								<input type="hidden" name="status" value="27">
							</form>
							
						</a>
						</div>
						<div class="col-md-2">
							<form method="GET" action="{{url('/Seniat/ViceministroSeniat-lista')}}">
								<button class=" btn btn-primary form-control"><b>Desaprobadas</b></button>
								<input type="hidden" name="status" value="28">
							</form>
						</div>
						<div class="col-md-2">
							<form method="GET" action="{{url('/Seniat/ViceministroSeniat-lista')}}">
								<button class=" btn btn-primary form-control"><b>Aprobadas</b></button>
								<input type="hidden" name="status" value="12">
							</form>
						</div>
					</div><!--Segunda row--> <br><br>


					<table id="listaCertificados" class="table table-striped table-bordered" style="width:100%">
						<thead style="font-size: 12px;">
							<tr class="text-center">
								<th class="col-md-1 text-center">Código Arancelario</th>
								<th class="col-md-1 text-center">Fecha Creado</th>
								<th class="text-center"></th>
							</tr>
						</thead>
						<tbody>
                        @foreach($certificados as $row)
							<tr style="font-size:12px;">
                            <td class="col-md-2"><b class="text-primary">{{ $row->codigo_arancelario }}</b></td>
							<td class="col-md-2"><b class="text-primary">{{ $row->fecha_certificado }}</b></td>
							<td class="col-md-2 text-center"><b class="text-primary">
								@if($row->gen_status_id == 27)
                                	<a href="{{ route('ViceministroSeniat.show', $row->id) }}" class="glyphicon glyphicon-edit btn btn-primary btn-sm" title="Modificar" aria-hidden="true">Evaluar</a>
                                @elseif($row->gen_status_id == 28)
								<a href="{{ url('/Seniat/ViceministroSeniat-show', $row->id) }}" class="glyphicon glyphicon-eye-open btn btn-primary btn-sm" title="Ver" aria-hidden="true">Evaluar</a> 
                                @elseif($row->gen_status_id == 12)
								<a href="{{ url('/Seniat/ViceministroSeniat-show', $row->id) }}" class="glyphicon glyphicon-eye-open btn btn-primary btn-sm" title="Ver" aria-hidden="true">Evaluar</a>       
                                @endif
                            </td>
							</tr>
                            @endforeach
						</tbody>
					</table>
				</div>
			</div>
			</div>
		</div>
	</div>
</div>
@stop