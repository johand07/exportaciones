@extends('templates/layoutlte')
@section('content')

@php
function namefile($name){
  $explode=explode("/",$name);
  return $explode[3];

}

 $contador1=count($docListDinamica1); 
 $contador2=count($docListDinamica2); 


@endphp


{{Form::model($inver,['route'=>['BandejaInversionista.update',$inver->id],'method'=>'PATCH','id'=>'forInversionista','enctype' => 'multipart/form-data'])}}

{{Form::hidden('contador1',$contador1)}}
{{Form::hidden('contador2',$contador2)}}
<input type="hidden" value="{{$id}}" name="gen_sol_inversionista_id" id="gen_sol_inversionista_id">
<div class="alert alert-dismissible alert-success">
  <button type="button" class="close" data-dismiss="alert">&times;</button>
  <h4 class="alert-heading">Observación!</h4>
  <p class="mb-0">{{$inver->observacion_inversionista}}</p>
  <p class="mb-0">{{$inver->observacion_doc_incomp}}</p>
  <p class="mb-0">{{$inver->observacion_anulacion}}</p>
</div>
<br>
<div class="panels">
  <div class="content">
    <h3 class="modal-title">
      Usuario Natural
    </h3>
    <div class="panel panel-primary">
      <div class="panel-primary" style=" #fff">
        <div class="panel-heading"><h4>Carga de Documentos Recaudos</h4></div>
          <div class="panel-body">
            <h5 style="color: red">Los campos marcados con (*) son Obligatorios. (Formatos permitidos .doc, .docx, .pdf)</h5>
              <div class="row">
                <div class="col-md-12">
                  <div class="col-md-4">
                  <tr>
                    <td class="text-center" valign="top" > 
                      <!--<span><b>1.) Documento de identidad (Cédula o Pasaporte Vigente)</b></span>-->
                        <span><b>1.) Documentos de Identidad (Cédula, Pasaporte y RIF).</b></span><br><br>
                        <strong style="color: red"> *</strong>
                        <span class="btn btn-default btn-file" style="margin: 3px;">
                          Cargar Archivo <span class="glyphicon glyphicon-file"></span>
                          {!! Form::file('file_1', array('class' => 'file-input','style'=>'margin: 10px;','id'=>'file_1','title'=>'formatos permitidos .doc, .docx, .pdf')) !!}
                          <input type="hidden" value="{{$documentos->id}}" name="file_id">
                        </span>
                      </td>
                    <td class="text-center" height="40px"><img id="vista_previa_file_1" src="" alt="" width="300px" style="margin: 3px;"><img id="imgdocumento_file_1" src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px; display: none;"><div id="nombre_file_1"></div>

                      @if(isset($documentos->file_1))
                      <div id="imgdocumentoEdit_file_1">
                        <img src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px;">
                        {{namefile($documentos->file_1)}}
                      </div>
                      @endif

                    </td>
                  </tr>
                  </div>
                  <div class="col-md-4">
                    <tr>
                      <td class="text-center" valign="top" > 
                      <!--<span><b>2.) Antecedentes penales debidamente apostillados.</b></span>--><span><b>2.) Certificado de domicilio en Venezuela o en el extranjero.</b></span>
                      <br><br>
                      <span class="btn btn-default btn-file" style="margin: 3px;">
                        Cargar Archivo <span class="glyphicon glyphicon-file"></span>
                        {!! Form::file('file_2', array('class' => 'file-input','style'=>'margin: 10px;','id'=>'file_2','title'=>'formatos permitidos .doc, .docx, .pdf')) !!}
                      </span>
                      </td>
                      <td class="text-center" height="40px"><img id="vista_previa_file_2" src="" alt="" width="300px" style="margin: 3px;"><img id="imgdocumento_file_2" src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px; display: none;"><div id="nombre_file_2"></div>

                        @if(isset($documentos->file_2))
                          <div id="imgdocumentoEdit_file_2">
                          <img  src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px;">
                          {{namefile($documentos->file_2)}}
                          </div>
                        @endif

                      </td>
                    </tr>
                  </div>
                  <div class="col-md-4">
                  <tr>
                    <td class="text-center" valign="top" > 
                      <!--<span><b>3.) Poder de representación debidamente autenticado.</b></span>--><span><b>3.) Poderes de representación del inversionista extranjero debidamente apostillados y traducidos al idioma español.</b></span></td><br><br>
                      <span class="btn btn-default btn-file" style="margin: 3px;">
                        Cargar Archivo <span class="glyphicon glyphicon-file"></span>
                        {!! Form::file('file_3', array('class' => 'file-input','style'=>'margin: 10px;','id'=>'file_3','title'=>'formatos permitidos .doc, .docx, .pdf')) !!}
                      </span>
                    </td>
                    <td class="text-center" height="40px"><img id="vista_previa_file_3" src="" alt="" width="300px" style="margin: 3px;"><img id="imgdocumento_file_3" src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px; display: none;"><div id="nombre_file_3"></div>
                        @if(isset($documentos->file_3))
                          <div id="imgdocumentoEdit_file_3">
                            <img src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px;">
                            {{namefile($documentos->file_3)}}
                          </div>
                        @endif
                    </td>
                  </tr>
                  </div>
                </div>
              </div>
          </div>
        </div> 
        <!--<div class="panel-heading"><h3>Carga de Documentos Soportes</h3></div>-->
        <div class="panel-heading">
          <div class="row">
            <div class="col-md-12">
              <h4>Carga de Documentos Soportes</h4>
              
              <!--<div class="col-md-4"><br>
                  <a href="{{asset('formatos_inversiones/Carta_solicitud.doc')}}" class="btn btn-sm btn-success" target='_blank'>Modelo carta de solicitud <span class="glyphicon glyphicon-cloud-download"></span></a>
                </div>
                <div class="col-md-4"><br>
                  <a href="{{ asset('formatos_inversiones/Declaración_fuente_licita_de_fondos.doc') }}" class="btn btn-sm btn-success" target='_blank'>Modelo declaración fuente lícita de fondos <span class="glyphicon glyphicon-cloud-download"></span></a>
                </div>
                <div class="col-md-4"><br>
                  <a href="{{ asset('formatos_inversiones/Otros_entes_publicos.doc') }}" class="btn btn-sm btn-success" target='_blank'>Modelo carta otros entes públicos <span class="glyphicon glyphicon-cloud-download"></span></a>
                </div>-->
            
            </div>
          </div>
        </div>
        <div class="panel-body">
          <h5 style="color: red">Los campos marcados con (*) son Obligatorios. (Formatos permitidos .doc, .docx, .pdf)</h5>
            <div class="row">
              <div class="col-md-12">
                <div class="col-md-6">
                  <tr>
                  <!-- <td class="text-center" valign="top" > 
                      <span><b><p style="text-align: justify;">1.) Carta de solicitud de inscripción en el SURI, indicando el cumplimiento de los requisitos legales y  una descripción del proyecto de inversión que pretende desarrollar (brochure).</p></b></span>
                      <strong style="color: red"> *</strong>
                      <span class="btn btn-default btn-file" style="margin: 3px;">
                        Cargar Archivo <span class="glyphicon glyphicon-file"></span>
                        {!! Form::file('file_4', array('class' => 'file-input','style'=>'margin: 10px;','id'=>'file_4','title'=>'formatos permitidos .doc, .docx, .pdf')) !!}
                      </span>
                      </td>
                      <td class="text-center" height="40px"><img id="vista_previa_file_4" src="" alt="" width="300px" style="margin: 3px;"><img id="imgdocumento_file_4" src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px; display: none;"> <div id="nombre_file_4"></div>
                        @if(isset($documentos->file_4))
                          <div id="imgdocumentoEdit_file_4">
                            <img  src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px;">
                            {{namefile($documentos->file_4)}}
                          </div>
                        @endif

                      </td>
                    </tr><br><br>
                    <!--<tr>
                    <td class="text-center" valign="top" > 
                      <span><b><p style="text-align: justify;">2.) Estados de Cuenta, referencias bancarias, balances contables o cualquier otro documento que evidencie el cumplimiento en activos físicos o monetarios del monto legal establecido en la Ley Constitucional de Inversión extranjera Productiva para optar a la condición de Inversionista Extranjero.</p>
                    </b></span>
                      <strong style="color: red"> *</strong>
                      <span class="btn btn-default btn-file" style="margin: 3px;">
                        Cargar Archivo <span class="glyphicon glyphicon-file"></span>
                        {!! Form::file('file_5', array('class' => 'file-input','style'=>'margin: 10px;','id'=>'file_5','title'=>'formatos permitidos .doc, .docx, .pdf')) !!}
                      </span>
                    </td>
                    <td class="text-center" height="40px"><img id="vista_previa_file_5" src="" alt="" width="300px" style="margin: 3px;"><img id="imgdocumento_file_5" src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px; display: none;"><div id="nombre_file_5"></div>
                      @if(isset($documentos->file_5))
                          <img id="imgdocumentoEdit_file_5" src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px;">
                      @endif

                    </td>
                  </tr>-->
                  <div class="row"><!--Lista dinamica para Agregar mas Documentos en el Doc 2)-->
              
                    <table class="table table-bordered text-left" id="soportes2">
                  

                      <!--<tr>
                        <th><span><b><p style="text-align: justify;">2.) Estados de Cuenta, referencias bancarias, balances contables o cualquier otro documento que evidencie el cumplimiento en activos físicos o monetarios del monto legal establecido en la Ley Constitucional de Inversión extranjera Productiva para optar a la condición de Inversionista Extranjero.</p>
                        </b></span> </th>
                        
                      </tr>-->
                      <tr>
                        <th><span><b><p style="text-align: justify;">1.) Original y una (1) copia del documento constitutivo de la empresa receptora de inversión, con sus modificaciones estatutarias.</p>
                        </b></span> </th>
                        
                      </tr>

                      <tr> <td><button  name="add" type="button" id="add-soportes2" value="add more" class="btn btn-success" >Agregar</button></td></tr>
                      @if (isset($docListDinamica1))

                        @foreach ($docListDinamica1 as $key=>$ListDinamica1)

                          <tr id="row{{$key+1}}">

                            <td>
                              <strong style="color: red"> *</strong>
                          
                              <!--input type="hidden" name="cat_documentos_id[]" id="cat_documentos_id" value="12"-->

                              <!--<input type="text" name="file[]" id="file" value="{{$ListDinamica1->file}}">-->

                            
                              <!--input type="file" name='file[]' id="file_5{{$key+1}}" class="file_multiple" onclick="ocultarNameLista1({{$key+1}})"-->
                              <div id='nombreCargado1{{$key+1}}'>
                                {{namefile($ListDinamica1->file)}}
                              </div>

                            </td>
                            <td> {!!Form::hidden('documentos_varios_inversiones_id[]',$ListDinamica1->id)!!}<button  name="remove" type="button" id="{{$key+1}}" value="" class="btn btn-danger btn-remove">x</button>
                            </td>
                            <!--<td class="text-center" height="40px"><img id="vista_previa_file_5" src="" alt="" width="300px" style="margin: 3px;"><img id="imgdocumento_file_5" src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px; display: none;"><div id="nombre_file_5"></div>
                            </td>-->
                          </tr>
                        @endforeach
                      @endif
                    </table>
                  </div>

                </div><!--cierre div de 6-->
                <div class="col-md-6">
                  <!--<tr>
                    <td class="text-center" valign="top" > 
                      <span><b>3.) Declaración de fuente lícita de fondos.<br>en el país.</b></span><br>
                      <strong style="color: red"> *</strong>
                      <span class="btn btn-default btn-file" style="margin: 3px;">
                        Cargar Archivo <span class="glyphicon glyphicon-file"></span>
                        {!! Form::file('file_6', array('class' => 'file-input','style'=>'margin: 10px;','id'=>'file_6','title'=>'formatos permitidos .doc, .docx, .pdf')) !!}
                      </span>
                    </td>
                    <td class="text-center" height="40px"><img id="vista_previa_file_6" src="" alt="" width="300px" style="margin: 3px;"><img id="imgdocumento_file_6" src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px; display: none;"> <div id="nombre_file_6"></div>
                      @if(isset($documentos->file_6))
                        <div id="imgdocumentoEdit_file_6" >
                          <img src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px;">
                          {{namefile($documentos->file_6)}}
                        </div>
                      @endif
                    </td>
                  </tr><br><br><br>
                  <!--<tr>
                    <td class="text-center" valign="top" > 
                      <br><span><b><p style="text-align: justify;">4.) Copia de documentos en los que se demuestre haber realizado los <br>contactos oficiales para realizar actividades económicas o de inversión <br>en el país.
                    </p></b></span>
                      <strong style="color: red"> *</strong>
                      <span class="btn btn-default btn-file" style="margin: 3px;">
                        Cargar Archivo <span class="glyphicon glyphicon-file"></span>
                        {!! Form::file('file_7', array('class' => 'file-input','style'=>'margin: 10px;','id'=>'file_7','title'=>'formatos permitidos .doc, .docx, .pdf')) !!}
                      </span>
                    </td>
                    <td class="text-center" height="40px"><img id="vista_previa_file_7" src="" alt="" width="300px" style="margin: 3px;"><img id="imgdocumento_file_7" src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px; display: none;"><div id="nombre_file_7"></div>
                      @if(isset($documentos->file_7))
                          <img id="imgdocumentoEdit_file_7" src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px;">
                      @endif
                    </td>
                  </tr>-->
                  <div class="row"><!--Lista dinamica para Agregar mas Documentos en el Doc 4)-->
                      
                      <table class="table table-bordered text-left" id="soportes4">
                      
                        <tr>
                        <th><span><b><p style="text-align: justify;">2.) Datos y documentos de identificación del Representante legal de la empresa <br>receptora de inversión.(ej.: documento de identidad, pasaporte, visa, etc).
                          </p></b></span></th>
                        </tr>
                        <tr> <td><br><button  name="add" type="button" id="add-soportes4" value="add more" class="btn btn-success" >Agregar</button></td></tr>

                        @if (isset($docListDinamica2))
                          @foreach ($docListDinamica2 as $key=>$ListDinamica2)

                            
                            <tr id="row2{{$key+1}}"><!--row2 es para la lista dinamica nsantos-->
                            <td>
                              <strong style="color: red"> *</strong>

                              <!--input type="hidden" name="cat_documentos_id[]" id="cat_documentos_id" value="14"-->

                              <!-- <input type="text" name="file[]" id="file" value="{{$ListDinamica2->file}}">-->

                            
                              <!--input type="file" name='file[]' id="file_7{{$key+1}}" class="file_multiple" onclick="ocultarNameLista2({{$key+1}})"-->
                              <div id='nombreCargado2{{$key+1}}'>
                                {{namefile($ListDinamica2->file)}}
                              </div>
                            </td>
                            <td> {!!Form::hidden('documentos_varios_inversiones_id[]',$ListDinamica2->id)!!}<button  name="remove" type="button" id="{{$key+1}}" value="" class="btn btn-danger btn-remove2">x</button>
                            </td>

                            </tr>
                          @endforeach
                        @endif
                      </table>
                  </div>
                </div><!-- div columna 6---->
                <!-- <div class="row">
                <div class="content">
                  <div class="col-md-12">
                    <tr>
                      <td class="text-center" valign="top" > 
                        <br><span><b><p style="text-align: justify;">5.) Comprobante de pago de trámite por un monto equivalente a 2 petros.</p></b></span>
                        <strong style="color: red"> *</strong>
                        <span class="btn btn-default btn-file" style="margin: 3px;">
                          Cargar Archivo <span class="glyphicon glyphicon-file"></span>
                          {!! Form::file('file_8', array('class' => 'file-input','style'=>'margin: 10px;','id'=>'file_8','title'=>'formatos permitidos .doc, .docx, .pdf')) !!}
                        </span>
                      </td>
                      <td class="text-center" height="40px"><img id="vista_previa_file_8" src="" alt="" width="300px" style="margin: 3px;"><img id="imgdocumento_file_8" src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px; display: none;"> <div id="nombre_file_8"></div>
                        @if(isset($documentos->file_8))
                            <div id="imgdocumentoEdit_file_8">
                              <img src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px;">
                              {{namefile($documentos->file_8)}}
                            </div>
                        @endif
                      </td>
                    </tr>
                  </div>
                </div>
                </div>-->
              </div>
        </div>
      </div>
      <br>
      <div class="panel-heading"><h4>Datos Básicos Inversionista</h4></div>
        <div class="panel-body">
            <div class="row">
            <div class="col-md-12">
              <div class="col-md-4">
              {!! Form::hidden('cat_tipo_usuario_id',$cat_tipo_usuario_id,['class'=>'form-control','id'=>'cat_tipo_usuario_id'])!!}
                <div class="form-group">
                  {!! Form::label('','Nombre o razón social')!!}
                  {!! Form::text('razon_social',(!is_null($datos))?$datos->razon_social:old('razon_social'),['class'=>'form-control','id'=>'razon_social','onkeypress'=>'return soloLetras(event)'])!!}
                </div>  
              </div>
              <div class="col-md-4">
                <div class="form-group">
                {!! Form::label('','País de Origen') !!}
                {!! Form::select('pais_id',$pais,null,['class'=>'form-control', 'id'=>'pais_id_inv','placeholder'=>'Seleccione el pais']) !!}
                </div>  
              </div>  
              <div class="col-md-4">
                <div class="form-group">
                  {!! Form::label('','N° de documento de identidad/Rif')!!}
                  {!! Form::text('n_doc_identidad',null,['class'=>'form-control','id'=>'n_doc_identidad','onkeypress'=>'return soloNumerosyLetras(event)'])!!}
                </div>  
              </div>  

            </div>  
          </div><br><br><br>
          <div class="row">
            <div class="col-md-12"> 
              <div class="col-md-4">
                <div class="form-group">
                  {!! Form::label('','N° de Pasaporte')!!}
                  {!! Form::text('ci_repre',(!is_null($datos))?$datos->ci_repre:old('ci_repre'),['class'=>'form-control','id'=>'ci_repre','onchange'=>'valorletrarif(this)','onkeypress'=>'return soloNumerosyLetras(event)'])!!}
                </div>  
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  {!! Form::label('','Dirección de domicilio en el exterior')!!}
                  {!! Form::text('direccion',(!is_null($datos))?$datos->direccion:old('direccion'),['class'=>'form-control','id'=>'direccion','onkeypress'=>'return soloNumerosyLetras(event)'])!!}
                </div>  
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  {!! Form::label('','Número telefónico/personal')!!}
                  {!! Form::text('telefono_movil',(!is_null($datos))?$datos->telefono_movil:old('telefono_movil'),['class'=>'form-control','id'=>'telefono_movil','onkeypress'=>'return soloNumeros(event)'])!!}
                </div>  
            </div> 
            </div>  
          </div><br><br><br>
          <div class="row">
            <div class="col-md-12"> 
              <div class="col-md-4">
                <div class="form-group">
                {!! Form::label('','Número telefónico/oficina')!!}
                  {!! Form::text('telefono_local',(!is_null($datos))?$datos->telefono_local:old('telefono_local'),['class'=>'form-control','id'=>'telefono_local','onkeypress'=>'return soloNumeros(event)'])!!}
                </div>  
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  {!! Form::label('','Correo electrónico')!!}
                  {!! Form::email('correo',(!is_null($datos))?$datos->correo:old('correo'),['class'=>'form-control','id'=>'correo','onkeyup'=>'minuscula(this.value,"#correo")'])!!}
                </div>  
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  {!! Form::label('','Página Web')!!}
                  {!! Form::text('pagina_web',(!is_null($datos))?$datos->pagina_web:old('pagina_web'),['class'=>'form-control','id'=>'pagina_web','onkeypress'=>'return soloNumerosyLetras(event)'])!!}
                </div>  
            </div> 
            </div>  
          </div><br><br><br>
        
        </div>
        <div class="panel-heading"><h4>Descripción de inversión</h4></div>
        <div class="panel-body">
            <div class="row">
              <div class="col-md-12"> 
                <div class="col-md-4">
                  <div class="form-group">
                  <label><b>Tipo de inversión</b></label>
                        {!! Form::select('tipo_inversion',$TipoInversion,null,['class'=>'form-control','id'=>'tipo_inversion',
                      'placeholder'=>'Seleccione un tipo de inversión']) !!}

                  </div>  
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                  {!! Form::label('','Monto de inversión')!!}
                    {!! Form::text('monto_inversion',null,['class'=>'form-control','id'=>'monto_inversion','onchange'=>'valMontoInversion()','onkeypress'=>'return soloNumeros(event)'])!!}
                  </div>  
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                  {!! Form::label('', 'Divisas') !!}
                  {!! Form::select('gen_divisa_id',$divisas,null,['placeholder'=>'Seleccione','class'=>'form-control','id'=>'gen_divisa_id']) !!}
                  </div>  
                </div>
              </div>
            </div><br>
            <div class="row">

              <div class="col-md-12"> 
                <div class="col-md-6">
                  <div class="form-group">
                      {!! Form::label('','Sector')!!}
                      {!! Form::select('cat_sector_inversionista_id',$sectorActEco,null,['class'=>'form-control','id'=>'cat_sector_inversionista_id_edit',
                      'placeholder'=>'Seleccione actividad economica','onchange'=>'OtraSector()']) !!}

                  </div> 
                  <div class="form-group" id="otro_sector" style="display: none;">
                    {!! Form::label('','(Especifique):')!!}
                    {!! Form::text('especif_otro_sector',null,['class'=>'form-control','id'=>'especif_otro_sector','onkeypress'=>'return soloNumerosyLetras(event)'])!!}
                  </div>

              </div>
              <div class="col-md-6">
                <div class="form-group">
                    <label>Actividad Económica</label><br>
                    <select class="form-control" id="actividad_eco_emp_edit" name="actividad_eco_emp" onchange="OtraActividad()">
                      <option value="{{$inver->actividad_eco_emp}}">{{$inver->CatActividadEcoInversionista ? $inver->CatActividadEcoInversionista->nombre : ''}}</option>
                      <option value="">--Seleccione una Opción--</option>
                      

                    </select>
                  </div> 
                  <div class="form-group" id="otro_act_eco" style="display: none;">
                    {!! Form::label('','(Especifique):')!!}
                    {!! Form::text('especif_otro_actividad',null,['class'=>'form-control','id'=>'especif_otro_actividad','onkeypress'=>'return soloNumerosyLetras(event)'])!!}
                  </div>
              </div>
              </div>
            </div><br>

            <div class="row">
              <div class="col-md-12"> 
                <div class="col-md-12"> 
                  <div class="form-group">
                  {!! Form::label('','Breve descripción del proyecto de inversión')!!}
                    <div id="contador"></div>
                  {!! Form::textarea('descrip_proyec_inver',null,['class'=>'form-control','id'=>'descrip_proyec_inver','maxlength'=>'255','onkeyup'=>'maxTextDescripProyecInver()','onkeypress'=>'return soloNumerosyLetras(event)','title'=>'Reseña de las características del proyecto de inversión que el solicitante pretende ejecutar en el país.'])!!}
                  </div> 
                </div>
              </div>
            </div><br>
            <div class="row">
              <div class="col-md-12"> 
                <div class="col-md-6"> 
                  <div class="form-group">
                  {!! Form::label('','Correo electrónico empresa')!!}
                    {!! Form::email('correo_emp_recep',null,['class'=>'form-control','id'=>'correo_emp_recep','onkeyup'=>'minuscula(this.value,"#correo_emp_recep")'])!!}
                  </div> 
                </div>
                <div class="col-md-6"> 
                  <div class="form-group">
                    {!! Form::label('','Página web empresa')!!}
                    {!! Form::text('pag_web_emp_recep',null,['class'=>'form-control','id'=>'pag_web_emp_recep','onkeypress'=>'return soloNumerosyLetras(event)'])!!}
                  </div>
                </div>
              </div>
            </div><br><br><br>

            <div class="row">
              <center><div class="col-md-12"> 
              {!! Form::label('','Representante legal o apoderado')!!}
              </div></center>
            </div>

            <div class="row">
              <div class="col-md-12"> 
                <div class="col-md-6"> 
                  {!! Form::label('','Nombre')!!}
                  {!! Form::text('nombre_repre',(!is_null($datos))?$datos->nombre_repre:old('nombre_repre'),['class'=>'form-control','id'=>'nombre_repre'])!!}
                </div>
                <div class="col-md-6"> 
                  {!! Form::label('','Apellido')!!}
                  {!! Form::text('apellido_repre',(!is_null($datos))?$datos->apellido_repre:old('apellido_repre'),['class'=>'form-control','id'=>'apellido_repre'])!!}
                </div>
              </div>
            </div>
        </div>

          <br><br><br>
          <div class="row text-center">
            <a href="{{ url('/exportador/BandejaInversionista')}}" class="btn btn-primary">Cancelar</a>
            {{--<!-- <input type="submit" class="btn btn-success" value="Enviar" name="Enviar" onclick="enviarInversionistaEdit()">  -->--}}
            <input type="button" class="btn btn-success" value="Enviar" name="Enviar" onclick="enviarUsuarioNatural()"> 
          </div><br>
        </div>
      </div>
  </div>
</div>

{{Form::close()}}



<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="{{asset('js/ajax_v2.1.1.js')}}"></script>

<script src="https://unpkg.com/@popperjs/core@2"></script>
<script src="https://unpkg.com/tippy.js@6"></script>

<script type="text/javascript">

/********************Scrip para carga de archivos**************** */
 function cambiarImagen(event) {
    var id= event.target.getAttribute('name');
    var reader = new FileReader();
    reader.onload = function(){
      var output = document.getElementById('vista_previa_'+id);
      output.src = reader.result;
    };
    let archivo = $(".file-input").val();
    if (!archivo) {
      archivo=$('#'+id).val();
    }
    var extensiones = archivo.substring(archivo.lastIndexOf(".")).toLowerCase();
    
    //var imgfile="/exportaciones/public/img/file.png";
    
    let elem_boton = $('#'+id).parent().parent().find('.btn-file');
    //alert(imgfile);
    if(extensiones != ".doc" && extensiones != ".docx" && extensiones != ".pdf"){
      elem_boton.css('background-color','#d9534f');
      elem_boton.css('color','white');
      elem_boton.css('overflow', 'visible');
      swal("Formato invalido", "Formato de archivo invalido debe ser .doc, .docx o .pdf", "warning");
      
        
    }else{
      $('#imgdocumento_'+id).show();
      $('#imgdocumentoEdit_'+id).hide();
      let file=$('#'+id).val();
      // alert(file);
      //Y pintame ruta y nombre del archivo acargado con nombre_
      $('#nombre_'+id).html('<strong>'+file+'</strong>');
      elem_boton.css('background-color','green');
      elem_boton.css('color','white');
      reader.readAsDataURL(event.target.files[0]);
    }

  }

   
    
    function ocultarNameLista1(num){
        //Ocultar nombre cargado
      $('#nombreCargado1'+num).hide();

    }
    ////////////////////////////////////
    function ocultarNameLista2(num){
        //Ocultar nombre cargado
      $('#nombreCargado2'+num).hide();

    }
    
   
  /********************************************************************** */
    ///////////////Para validar documentos//////////////////

  function cambiarImagenDoc(event) {

    
    //console.log(event);

    var id= event.target.getAttribute('id');
    //alert(id);
    
  
      archivo=$('#'+id).val();
    //}
    var extensiones = archivo.substring(archivo.lastIndexOf(".")).toLowerCase();
    
    //var imgfile="/exportaciones/public/img/file.png";

    
    if(extensiones != ".doc" && extensiones != ".docx" && extensiones != ".pdf"){
      swal("Formato invalido", "Formato de archivo invalido debe ser .doc, .docx o pdf", "warning"); 
        $('#'+id).val('');
        
    }else{
      $('#imgdocumento_'+id).show();
      let file=$('#'+id).val();
     // alert(file);
      $('#nombre_'+id).html('<strong>'+file+'</strong>');
     
      reader.readAsDataURL(event.target.files[0]);

    }

  }

    $(document).on('change', '.file-input', function(evt) {
      
      let size=this.files[0].size;
      if (size>8500000) {

         swal("Tamaño de archivo invalido", "El tamaño del archivo No puede ser mayor a 8.5 megabytes","warning"); 
          this.value='';
         //this.files[0].value='';
      }else{
        cambiarImagen(evt);
      }
      
           
    });
/////////////////Par lista dinamica////////////////
  $(document).on('change', '.file_multiple', function(evt) {
           

          let size=this.files[0].size;
      if (size>2500000) {

         swal("Tamaño de archivo invalido", "El tamaño del archivo No puede ser mayor a 8.5 megabytes","warning"); 
         this.value='';
      }else{
        cambiarImagenDoc(evt);
      }

    });



  /********************************************************************** */



function CedulaFormat1(vCedulaName,mensaje,postab,escribo,evento) {

  //  var tipoPerson=document.getElementsByName('tipo_usuario');
    tecla=getkey(evento);
    vCedulaName.value=vCedulaName.value.toUpperCase();
    vCedulaValue=vCedulaName.value;
    valor=vCedulaValue.substring(2,12);
    var numeros='0123456789/';
    var digit;
    var noerror=true;
    if (shift && tam>1) {
    return false;
    }
    for (var s=0;s<valor.length;s++){
    digit=valor.substr(s,1);
    if (numeros.indexOf(digit)<0) {
    noerror=false;
    break;
    }
    }
    tam=vCedulaValue.length;
    if (escribo) {
    if ( tecla==8 || tecla==37) {
    if (tam>2)
    vCedulaName.value=vCedulaValue.substr(0,tam-1);
    else
    vCedulaName.value='';
    return false;
    }
    if (tam==0 && tecla==69) {
    vCedulaName.value='E-';
    return false;
    }
    if (tam==0 && tecla==69) {
    //vCedulaName.value='E-';
    return false;
    }


    if (tam==0 && tecla==86) {
    vCedulaName.value='V-';
    return false;
    }


        if (tam==0 && tecla==74) {
    vCedulaName.value='J-';
    return false;
    }
    if (tam==0 && tecla==71) {
    vCedulaName.value='G-';
    return false;
    }
    if (tam==0 && tecla==80) {
    vCedulaName.value='P-';
    return false;
    }

    else if ((tam==0 && ! (tecla<14 || tecla==69 || tecla==86 || tecla==46)))
    return false;
    else if ((tam>1) && !(tecla<14 || tecla==16 || tecla==46 || tecla==8 || (tecla >= 48 && tecla <= 57) || (tecla>=96 && tecla<=105)))
    return false;
    }

}


///////////////////////////////////////////////////////////////////////////////////////
function valMontoInversion(){
   const min = 10000;    // Valor mínimo
    //const max = 10000; // Valor máximo

    
    // Obtenemos el valor actual
    let value = $('#monto_inversion').val();
    //alert(value);
    // Si el valor obtenido es menor a nuestro valor mínimo
    // o nuestro valor valor obtenido es mayor a nuestro valor máximo
    // Le decimos al usuario que no está dentro del rango 
    // y limpiamos nuestro campo
    if(value < min){
      console.log('El número ingresado no está dentro del rango permitido');
      swal("Valor superado", "El número ingresado no está dentro del rango permitido debe ser mayor o igual a 10.000","warning");
      $('#monto_inversion').val('');
    }
    
  // })

}

/////////////////////////////////Campo descripcion////////////////////////////////////

function maxTextDescripProyecInver(){

    var max_chars = 255;

    $('#max').html(max_chars);

    // $('#descrip_proyec_inver').keyup(function() {
        var chars = $('#descrip_proyec_inver').val().length;
        var diff = max_chars - chars;
        $('#contador').html(diff);   
    // });
}


/////////////////////////////////Para otros sector  y para otra actividad 2 funciones////////////////////////////////////
function OtraSector() {

  let sector= $('#cat_sector_inversionista_id_edit').val();
  

  if (sector==11) {
    $('#otro_sector').show();
    $('#otro_act_eco').show();

  }else{
    $('#otro_sector').hide();
    $('#otro_act_eco').hide();
    $('#especif_otro_sector').val('');
    $('#especif_otro_actividad').val('');



  }

}


function OtraActividad() {

  let actividad= $('#actividad_eco_emp_edit').val();

  if (actividad==66 || actividad==67 || actividad==68 || actividad==69 || actividad==70 || actividad==71 || actividad==72 || actividad==73 || actividad==74 || actividad==75 || actividad==76){

    $('#otro_act_eco').show();
  }else{
    $('#otro_act_eco').hide();
      $('#especif_otro_actividad').val('');
  }


}   

/*************************Lista dinamica para Soportes 2 ********************** */

$(document).ready(function (){

  let sector= $('#cat_sector_inversionista_id_edit').val();
  let actividad= $('#actividad_eco_emp_edit').val();

  console.log('actividad'+actividad);
  console.log('sector'+sector);
  if (sector==11 && (actividad==66 || actividad==67 || actividad==68 || actividad==69 || actividad==70 || actividad==71 || actividad==72 || actividad==73 || actividad==74 || actividad==75 || actividad==76)){
    $('#otro_sector').show();
    $('#otro_act_eco').show();

  }else{
    $('#otro_sector').hide();
    $('#otro_act_eco').hide();
    $('#especif_otro_sector').val('');
    $('#especif_otro_actividad').val('');

  }



  /* agregar para productos completos en el caso colombia*/

  //var x = $('input[name="contador1"]').val();
  var i=0;//parseInt(x);

  
  $('#add-soportes2').click(function(){
  i++;

    $('#soportes2 tr:last').after('<tr id="row'+i+'"display="block" class="show_div"><td><strong style="color: red"> *</strong> <input type="hidden" name="cat_documentos_id[]" id="cat_documentos_id" value="12"><input type="file" name="file[]" id="file_5'+i+'" class="file_multiple"></td><td>{!!Form::hidden("gen_sol_inversionista_id",$id)!!}{!!Form::hidden("documentos_varios_inversiones_id[]",0)!!}<button  name="remove" type="button" id="'+i+'" value="" class="btn btn-danger btn-remove">x</button></td></tr>');

              
  });

  $(document).on('click','.btn-remove',function(){
        var id_boton= $(this).attr("id");
        var id=$(this).prev().val();

    if(id!=0){

         if (confirm('Deseas eliminar este Archivo?')) {

      $.ajax({

       'type':'get',
       'url':'eliminarSoporte2',
       'data':{ 'id':id},
       success: function(data){

        $('#row'+id_boton).remove();

         }

       });

       }



    }else{

      $('#row'+id_boton).remove();
    }

  });


  /*********************Lista Dinamica para soporte 4 **************************/
  //var k = $('input[name="contador2"]').val();
  var j=0;//parseInt(k);

  $('#add-soportes4').click(function(){
  j++;

    $('#soportes4 tr:last').after('<tr id="row2'+j+'"display="block" class="show_div"><td><strong style="color: red"> *</strong> <input type="hidden" name="cat_documentos_id[]" id="cat_documentos_id" value="14"><input type="file" name="file[]" id="file_7'+j+'" class="file_multiple"><td>{!!Form::hidden("gen_sol_inversionista_id",$id)!!}{!!Form::hidden("documentos_varios_inversiones_id[]",0)!!}<button  name="remove" type="button" id="'+j+'" value="" class="btn btn-danger btn-remove2">x</button></td></tr>');


  });

  $(document).on('click','.btn-remove2',function(){
        var id_boton= $(this).attr("id");
        var id=$(this).prev().val();

    if(id!=0){

         if (confirm('Deseas eliminar este Archivo?')) {

      $.ajax({

       'type':'get',
       'url':'eliminarSoporte2',
       'data':{ 'id':id},
       success: function(data){

        $('#row2'+id_boton).remove();

         }

       });

       }



    }else{

      $('#row2'+id_boton).remove();
    }

  });


    //////////Tooltip Guia para cada campo///////// 
  tippy('#razon_social', {
  content: 'Colocar nombre y apellido de la persona natural según se indique en el documento de identidad o pasaporte que solicita el Certificado de Registro de Inversión Extranjera, según aparezca en sus estatutos de creación.',
  placement: 'bottom',
  });

   tippy('#pais_id_inv', {
  content: 'Lugar del cual proviene el solicitante del Certificado de Registro de Inversionista Extranjero.',
  placement: 'bottom',
  });

     tippy('#n_doc_identidad', {
  content: 'Se debe indicar el número de cédula, carnet, D.N.I, u otro documento que acredite la identidad del solicitante (en caso de persona natural.)',
  placement: 'bottom',
  });

   tippy('#ci_repre', {
  content: 'En este renglón debe indicarse el número del pasaporte (en caso de persona natural.)',
  placement: 'bottom',
  });   

    tippy('#direccion', {
  content: 'Colocar el lugar en el que la persona natural o la sociedad  mercantil solicitante tiene su residencia en el país de origen',
  placement: 'bottom',
  });

    tippy('#telefono_movil', {
  content: 'Debe indicarse el teléfono celular de contacto del solicitante en el exterior.',
  placement: 'bottom',
  }); 

    tippy('#telefono_local', {
  content: 'Debe indicarse el teléfono fijo de contacto del solicitante en el exterior.',
  placement: 'bottom',
  }); 

    tippy('#correo', {
  content: 'Email de la persona natural o de la empresa, a través del cual puede establecerse contacto vía electrónica con el solicitante.',
  placement: 'bottom',
  }); 

    tippy('#pagina_web', {
  content: 'Si el solicitante cuenta con una página web, se recomienda suministrarla. No es indispensable.',
  placement: 'bottom',
  }); 

   tippy('#tipo_inversion', {
  content: 'Corresponde al tipo de inversion a realizar.',
  placement: 'bottom',
  }); 

    tippy('#monto_inversion', {
  content: 'La cantidad de dinero que el solicitante pretende invertir en el proyecto de inversión.',
  placement: 'bottom',
  }); 

    tippy('#actividad_eco_emp_edit', {
  content: 'Debe señalarse a qué sector de la economía se dedica la empresa receptora de la inversión extranjera (si aplica).',
  placement: 'bottom',
  });  

    tippy('#correo_emp_recep', {
  content: 'Dirección de email de la empresa receptora.',
  placement: 'bottom',
  });

    tippy('#pag_web_emp_recep', {
  content: 'Suministrar la página web de la empresa receptora de la inversión extranjera, en caso de que posea una.',
  placement: 'bottom',
  });

    tippy('#nombre_repre', {
  content: 'Indicar nombre Representante legal o apoderado.',
  placement: 'bottom',
  });

    tippy('#apellido_repre', {
  content: 'Indicar apellido Representante legal o apoderado.',
  placement: 'bottom',
  });



});/*llave del document).ready*/


/* Validación Registro DUDA*/
function enviarUsuarioNatural() {
  let input_basicos = $('#forInversionista').find('input, select, textarea').not(':button,:hidden');
  let sector= $('#cat_sector_inversionista_id').val();
  let campos = [];
  let m = 0;
  for(let i=0; i < input_basicos.length; i++){
    if(input_basicos[i].id != "especif_otro_actividad" && input_basicos[i].id != "especif_otro_sector" && input_basicos[i].id != "pag_web_emp_recep"){
      campos.push(input_basicos[i].id);
    }
  }

  if (sector == 11) {
    campos.push('especif_otro_sector');
    campos.push('especif_otro_actividad');
  }
  
  $("div").remove(".msg_alert");

  let err_duda = 0;
  let error_dudas = '<div class="msg_alert"><span style="color:red" class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span><strong id="cod_aran-error" class="added"> El campo es obligatorio.</strong></div>';
  let valido_dudas = '<div class="msg_alert"><span style="color:green" class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span></div>';
  
  let n = campos.length;
  let input_duda = '';

  for (var i = 0; i < n; i++) {
    input_duda = $('#'+campos[i]);
    
    if (input_duda.val() == '')
    {
      err_duda = 1;
      input_duda.css('border', '1px solid red').after(error_dudas);
      if(err_duda==1){
        event.preventDefault(); 
        swal("¡Por Favor!", 'Estimado usuario. Debe completar los campos solicitados', "warning");
      }
    }
    else{
      if (err_duda == 1) {err_duda = 1;}else{err_duda = 0;}
      input_duda.css('border', '1px solid green').after(valido_dudas);
    }
  }

  if(err_edit == 1 && err_duda == 0){
    event.preventDefault();
    swal("Por favor, debe hacer un modificación para poder actualizar el registro!", '', "warning")
  }
  if(err_duda == 0 && err_edit == 0){
    $('#forInversionista').trigger('submit');
  }
  
}

var err_edit = 1;
$(document).ready(function(){
  // escucha el evento submit del formulario miFormulario
  
  //Funcion para detectar si existe algÃºn cambio dentro del formulario

  /*
  $('#forInversionista').submit(function(e){
      let error = '<div class="msg_alert"><span style="color:red" class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span><strong id="cod_aran-error" class="added"> El campo es obligatorio.</strong></div>';
      let valido = '<div class="msg_alert"><span style="color:green" class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span></div>';
      let mens=['Estimado Usuario. Debe completar los campos solicitados',]
      
      let campos = $('#forInversionista').find('input:text, select, input:file, textarea');
      let n = campos.length;
      let err = 0;
      
      $("div").remove(".msg_alert");
      //bucle que recorre todos los elementos del formulario
      for (let i = 0; i < n; i++) {
        let cod_input = $('#forInversionista').find('input:text, select, input:file, textarea').eq(i);
        if (!cod_input.attr('noreq')) {
            
          if (cod_input.val() == '' || cod_input.val() == null)
          {
            err = 1;
            cod_input.css('border', '1px solid red').after(error);
          }
          else{
            if (err == 1) {err = 1;}else{err = 0;}
            cod_input.css('border', '1px solid green').after(valido);
          }
            
        }
      }

    //Si hay errores se detendrá el submit del formulario y devolverá una alerta
    if(err==1){
      event.preventDefault();
      swal("Por Favor!", mens[0], "warning")
    }  
    if(err_edit==1){
		  event.preventDefault();
			swal("Por favor, debe hacer un modificación para poder actualizar el registro!", mens[1], "warning")
		}

  });
  */
  
  $("input:text, select, input:file, textarea").change(function(){
      err_edit = 0;
  });
});


</script>

<style type="text/css">




</style>
@stop