<?php

namespace App\Http\Controllers;
use App\Models\GenUnidadMedida;
use App\Models\PermisoProductos;
use App\Models\Productos;
use App\Http\Requests\PermisoProductosRequest;
use Session;
use Auth;
use Laracasts\Flash\Flash;
use Alert;
use Illuminate\Http\Request;

class ProdutPermisosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if($request->ajax())
         {
         if($request['valor']==""){
            return $arancel=Productos::take(50)->where('gen_usuario_id',Auth::user()->id)->get();
           }else{
              $arancel=Productos::where('gen_usuario_id',Auth::user()->id)
              ->where(function($q) use ($request)
              {$q->where('cod_arancel','like','%'.$request['valor'].'%')->orWhere('descrip_arancel','like','%'.$request['valor'].'%');
               })
               ->take(50)->get();



           return $arancel;

            }

       }else{

           $arancel=Productos::take(50)->where('gen_usuario_id',Auth::user()->id)->get();

       }


       $titulo="Registro de Productos";
       $descripcion="Productos Correspondientes al Permiso";
       $medidas=GenUnidadMedida::all()->pluck('dunidad','id');
       //dd($medidas);
       return view('ProdutPermisos.create', compact('titulo','descripcion','medidas','arancel'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PermisoProductosRequest $request)
    {
        $cont=count($request->descrip_arancel);
        for($i=0;$i<=($cont-1);$i++){

            $det=new PermisoProductos;
            $det->permiso_id=Session::get('permiso_id');
            $det->codigo_arancel=$request->codigo_arancel[$i];
            $det->descrip_arancel=$request->descrip_arancel[$i];
            $det->descrip_comercial=$request->descrip_comercial[$i];
            $det->cantidad=$request->cantidad[$i];
            $det->gen_unidad_medida_id=$request->gen_unidad_medida_id[$i];
            $det->save();
        }
          Alert::success('REGISTRO EXITOSO','Permiso agregado')->persistent("Ok");
       //return view('ProdutPermisos.create');
          return redirect()->action('PermisoController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PermisoProductos  $permisoProductos
     * @return \Illuminate\Http\Response
     */
    public function show(PermisoProductos $permisoProductos)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\PermisoProductos  $permisoProductos
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {

       if($request->ajax())
         {
         if($request['valor']==""){
            return $arancel=Productos::take(50)->where('gen_usuario_id',Auth::user()->id)->get();
           }else{
              $arancel=Productos::where('gen_usuario_id',Auth::user()->id)
              ->where(function($q)
              {$q->where('cod_arancel','like','%010%')->orWhere('descrip_arancel','like','%papa%');
               })
               ->take(50)->get();


           return $arancel;

            }

       }else{

           $arancel=Productos::take(50)->where('gen_usuario_id',Auth::user()->id)->get();

       }



       $permiso=PermisoProductos::where('permiso_id',$id)->get();
       Session::put('permiso',$id);
       $titulo="Registro de Productos";
       $descripcion="Productos Correspondientes al Permiso";
       $medidas=GenUnidadMedida::all()->pluck('dunidad','id');
       $id=$id;

       return view('ProdutPermisos.edit', compact('titulo','descripcion','medidas','arancel','permiso','id'));



    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\PermisoProductos  $permisoProductos
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PermisoProductos $permisoProductos)
    {

      // dd($request->all());
        $cont=count($request->descrip_arancel);
         for($i=0;$i<=($cont-1);$i++){

            PermisoProductos::updateOrCreate(
            ['id'=>$request->id[$i]],
            ['codigo_arancel'=>$request->codigo_arancel[$i],
            'descrip_arancel'=>$request->descrip_arancel[$i],
            'permiso_id'=>Session::get('permiso'),
            'descrip_comercial'=>$request->descrip_comercial[$i],
            'cantidad'=>$request->cantidad[$i],
            'gen_unidad_medida_id'=>$request->gen_unidad_medida_id[$i],

           ]

            );

       }

        Alert::success('Actualización Exitosa!')->persistent("Ok");
        return redirect('exportador/Permisos');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PermisoProductos  $permisoProductos
     * @return \Illuminate\Http\Response
     */
    public function destroy(PermisoProductos $permisoProductos)
    {
        //
    }

    public function eliminar()
    {
         $delete=PermisoProductos::find($_GET['id'])->delete();
       if($delete)
       {
          return "Producto Eliminado";
       }else
       {
         return "Proucto No existe";
       }


    }

}
