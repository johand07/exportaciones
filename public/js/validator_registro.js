$(document).ready(function (){

    var exporta=document.getElementsByName('produc_tecno');
    if(exporta[0].checked){
    $('#tecno_especifique').show(1000).animate({width: "show"}, 1000,"linear");
    }
    var i = 0;
        $('#add').click(function(){
         i++;
   $('#fields tr:last').after('<tr id="row'+i+'" display="block" class="show_div"><td>{!!Form::text("nombre_accionista[]",null,["class"=>"form-control ejem","onkeypress"=>"return soloLetras(event)","maxlength"=>"50"])!!}</td><td>{!!Form::text("apellido_accionista[]",null,["class"=>"form-control","onkeypress"=>"return soloLetras(event)","maxlength"=>"50"])!!}</td><td>{!!Form::text("cedula_accionista[]",null,["class"=>"form-control","id"=>"cedula_accionista","onkeypress"=>"return CedulaFormat1(this,"Cedula de Indentidad Invalida",-1,true,event)","maxlength"=>"15","pattern"=>"^([VEJPG]{1})([-]{1})([0-11]{9})$"])!!}<small>Ejemplo:V-00000000, J-00000000</small></td><td>{!!Form::text("cargo_accionista[]",null,["class"=>"form-control","onkeypress"=>"return soloLetras(event)","maxlength"=>"80"])!!}</td><td>{!!Form::text("telefono_accionista[]",null,["class"=>"form-control","id"=>"telefono_accionista","onkeypress"=>"return soloNumeros(event)","maxlength"=>"11"])!!}<small>Ejemplo:02125554555</small></td><td>{!!Form::text("nacionalidad_accionista[]",null,["class"=>"form-control","id"=>"nacionalidad_accionista","onkeypress"=>"return soloLetras(event)","maxlength"=>"50"])!!}</td> <td>{!!Form::text("participacion_accionista[]",null,["class"=>"form-control","id"=>"participacion_accionista","onkeypress"=>"return soloNumerosletrasyPorcentaje(event)","maxlength"=>"50"])!!}</td><td>{!!Form::text("correo_accionista[]",null,["class"=>"form-control","id"=>"correo_accionista","onkeyup"=>"minuscula(this.value,"#email");","maxlength"=>"80"])!!}</td><td><button  name="remove" type="button" id="'+i+'" value="" class="btn btn-danger btn-remove">x</button></td></tr>');
         });
    var j = 0;

    
        $('#add2').click(function(){
        j++;
  
  /*****************************************Campos dinamicos de codigos arancelarios********************************************************************/
  
      $('#fields2 tr:last').after('<tr id="fila'+j+'" display="block" class="show_div"><td>{!!Form::text("codigo[]",null,["class"=>"form-control ejem","data-toggle"=>"modal","data-target"=>"#modal","id"=>"codigo","readonly"=>"true"])!!}</td><td>{!!Form::text("descripcion[]",null,["class"=>"form-control","onkeypress"=>"return soloLetras(event)"])!!}</td><td>{!!Form::text("descrip_comercial[]",null,["class"=>"form-control","onkeypress"=>"return soloLetras(event)"])!!}</td><td>{!!Form::select("gen_unidad_medida_id[]",$unidad_medida,null,["placeholder"=>"--Seleccione una Unidad de Medida--", "class"=>"form-control"]) !!}</td><td>{!!Form::text("cap_opera_anual[]",null,["class"=>"form-control","onkeypress"=>"return soloNumeros(event)"])!!}</td><td>{!!Form::text("cap_insta_anual[]",null,["class"=>"form-control","onkeypress"=>"return soloNumerosletrasyPorcentaje(event)"])!!}</td><td>{!!Form::text("cap_alamcenamiento[]",null,["class"=>"form-control","onkeypress"=>"return soloNumeros(event)"])!!}</td><td>{!!Form::text("cap_exportacion[]",null,["class"=>"form-control","id"=>"cap_exportacion","onkeypress"=>"return soloNumeros(event)"])!!}</td><td><button  name="remove" type="button" id="'+j+'" value="" class="btn btn-danger btn-remove2">x</button></td></tr>');
  
  
         });
  
  
       $(document).on('click','.btn-remove',function(){
          var id_boton= $(this).attr("id");
          $("#row"+id_boton+"").remove();
  
       });
       $(document).on('click','.btn-remove2',function(){
          var id_boton= $(this).attr("id");
          $("#fila"+id_boton+"").remove();
  
       });
  
  /*function tipoArancel() {
  
      $('input:radio[name="tipo_arancel"]').change( function(){
  
        if($(this).is(':checked')) {
  
          if($(this).val()==1){
  
             return "nandina";
  
          }else{
  
             return "mercosur";
          }
  
        }
  
      });
  
  }*/
  
  //////////Accionista pregunta//////////////////////////////////////////////////////////////////////////////////////////////////////
    $('#accion_si').click(function(event) {
  
      $('#fields').show(1000).animate({width: "show"}, 1000,"linear");
  
    });
  
    $('#accion_no').click(function(event) {
  
      $('#fields').hide(1000).animate({height: "hide"}, 1000,"linear");
  
    });
  
  
  //////////////////////////////Parte_1//////////////////////////////////////
  
        $('#seguir1').click(function(event) {
  
          key = validacionForm("datos_persona1");
          if(key == 0){
          $('#datos_persona2, #datos_accionistas').show(1000).animate({width: "show"}, 1000,"linear");
          $('#datos_persona1,#datos_persona3,#datos_persona4,#datos_persona5').hide(1000).animate({height: "hide"}, 1000,"linear");
          }
  
        });
  //////////////////////////////Parte_2//////////////////////////////////////
  
        $('#seguir2').click(function(event) {
  
          key = validacionForm("datos_persona2");
          if(key == 0){
          $('#datos_persona1,#datos_persona2,#datos_accionistas,#datos_persona4,#datos_persona5').hide(1000).animate({height: "hide"}, 1000,"linear");
          $('#datos_persona3').show(1000).animate({width: "show"}, 1000,"linear");
          }
  
        });
        $('#atras1').click(function(event) {
          $('#datos_persona2,#datos_accionistas,#datos_persona3,#datos_persona4,#datos_persona5').hide(1000).animate({height: "hide"}, 1000,"linear");
          $('#datos_persona1').show(1000).animate({width: "show"}, 1000,"linear");
  
  
        });
        $('#paso1').click(function(event) {
          $('#datos_persona2,#datos_accionistas,#datos_persona3,#datos_persona4,#datos_persona5').hide(1000).animate({height: "hide"}, 1000,"linear");
          $('#datos_persona1').show(1000).animate({width: "show"}, 1000,"linear");
  
  
        });
  
  //////////////////////////////Parte_3//////////////////////////////////////
  
        $('#seguir3').click(function(event) {
          key = validacionForm("datos_persona3");
          if(key == 0){
          $('#datos_persona3,#datos_persona2,#datos_accionistas,#datos_persona1,#datos_persona5').hide(1000).animate({height: "hide"}, 1000,"linear");
          $('#datos_persona4').show(1000).animate({width: "show"}, 1000,"linear");
          }
  
        });
        $('#atras2').click(function(event) {
          $('#datos_persona3,#datos_persona4,#datos_persona1,#datos_persona5').hide(1000).animate({height: "hide"}, 1000,"linear");
          $('#datos_persona2,#datos_accionistas').show(1000).animate({width: "show"}, 1000,"linear");
  
  
        });
        $('#paso22').click(function(event) {
          $('#datos_persona3,#datos_persona4,#datos_persona1,#datos_persona5').hide(1000).animate({height: "hide"}, 1000,"linear");
          $('#datos_persona2,#datos_accionistas').show(1000).animate({width: "show"}, 1000,"linear");
  
  
        });
        $('#paso11').click(function(event) {
          $('#datos_persona2,#datos_accionistas,#datos_persona3,#datos_persona4,#datos_persona5').hide(1000).animate({height: "hide"}, 1000,"linear");
          $('#datos_persona1').show(1000).animate({width: "show"}, 1000,"linear");
  
  
        });
  //////////////////////////////Parte_4//////////////////////////////////////
        $('#atras3').click(function(event) {
          $('#datos_persona4,#datos_persona2,#datos_accionistas,#datos_persona1,#datos_persona5').hide(1000).animate({height: "hide"}, 1000,"linear");
          $('#datos_persona3').show(1000).animate({width: "show"}, 1000,"linear");
  
  
        });
        $('#paso333').click(function(event) {
          $('#datos_persona4,#datos_persona2,#datos_accionistas,#datos_persona1,#datos_persona5').hide(1000).animate({height: "hide"}, 1000,"linear");
          $('#datos_persona3').show(1000).animate({width: "show"}, 1000,"linear");
  
  
        });
        $('#paso222').click(function(event) {
          $('#datos_persona3,#datos_persona4,#datos_persona1,#datos_persona5').hide(1000).animate({height: "hide"}, 1000,"linear");
          $('#datos_persona2,#datos_accionistas').show(1000).animate({width: "show"}, 1000,"linear");
  
  
        });
        $('#paso111').click(function(event) {
          $('#datos_persona2,#datos_accionistas,#datos_persona3,#datos_persona4,#datos_persona5').hide(1000).animate({height: "hide"}, 1000,"linear");
          $('#datos_persona1').show(1000).animate({width: "show"}, 1000,"linear");
  
  
        });
  
    /***************************************Seguir 4***************************************************************************/
     
     $('#seguir4').click(function(event) {
  
          //Validamos que seleccionó el usuario en este paso y en de acuerdo al valor le pasamos el valor a la función de validación, para que solo valide en ese segmento
          if ($('input:radio[name=produc_tecno]:checked').val() == 2) {
            key = validacionForm("codigo_arancelarios");
          }else if ($('input:radio[name=produc_tecno]:checked').val() == 1) {
            key = validacionForm("tecno_especifique");
          } else {
            key = validaRadios("radios_export");
          }
            if(key == 0){
              $('#datos_persona3,#datos_persona2,#datos_accionistas,#datos_persona1,#datos_persona4').hide(1000).animate({height: "hide"}, 1000,"linear");
              $('#datos_persona5').show(1000).animate({width: "show"}, 1000,"linear");
            }
  
        });
  
     $('#atras4').click(function(event) {
          $('#datos_persona3,#datos_persona2,#datos_accionistas,#datos_persona1,#datos_persona5').hide(1000).animate({height: "hide"}, 1000,"linear");
          $('#datos_persona4').show(1000).animate({width: "show"}, 1000,"linear");
  
        });
  
     $('#paso1111').click(function(event) {
          $('#datos_persona2,#datos_accionistas,#datos_persona3,#datos_persona4,#datos_persona5').hide(1000).animate({height: "hide"}, 1000,"linear");
          $('#datos_persona1').show(1000).animate({width: "show"}, 1000,"linear");
        });
  
     $('#paso2222').click(function(event) {
          $('#datos_persona3,#datos_persona4,#datos_persona1,#datos_persona5').hide(1000).animate({height: "hide"}, 1000,"linear");
          $('#datos_persona2,#datos_accionistas').show(1000).animate({width: "show"}, 1000,"linear");
  
        });
  
     $('#paso3333').click(function(event) {
          $('#datos_persona4,#datos_accionistas,#datos_persona2,#datos_persona1,#datos_persona5').hide(1000).animate({height: "hide"}, 1000,"linear");
          $('#datos_persona3').show(1000).animate({width: "show"}, 1000,"linear");
  
        });
  
     $('#paso4444').click(function(event) {
          $('#datos_persona3,#datos_persona2,#datos_accionistas,#datos_persona1,#datos_persona5').hide(1000).animate({height: "hide"}, 1000,"linear");
          $('#datos_persona4').show(1000).animate({width: "show"}, 1000,"linear");
  
        });
  
  
    $('#seguir5').click(function(event) {
      key = validacionForm("datos_persona5");
    });
  
  
  
  
  /****************************Validacion de radio Casa Matriz********************************************/
  
  $(document).ready(function (){
      
  if ($("#casa_matriz_si").is(':checked')){
    $('#pais_origen').show(1000).animate({width: "show"}, 1000,"linear");//Si casa matriz tiene el checked muestro el pais de origen///
  
  }
  
  if ($("#casa_matriz_no").is(':checked')){
    $('#pais_origen').hide(1000).animate({height: "hide"}, 1000,"linear");//Casa matriz tiene el checked en no oculto el pais de origen///
  }
  
  $('#casa_matriz_si').click(function(event) {
  
          $('#pais_origen').show(1000).animate({width: "show"}, 1000,"linear");  
  
         });
  
          $('#casa_matriz_no').click(function(event) {
            $('#pais_origen').hide(1000).animate({height: "hide"}, 1000,"linear");
  
        });
  
  });
  
  
        /****************************Validacion de radio tecnologia********************************************/
  
        $('#prod_tecnologia_si').click(function(event) {
          $('#fields2,#codigo_arancelarios').hide(1000).animate({height: "hide"}, 1000,"linear");
          $('#tecno_especifique').show(1000).animate({width: "show"}, 1000,"linear");
  
        });
  
         $('#prod_tecnologia_no').click(function(event) {
          $('#tecno_especifique').hide(1000).animate({height: "hide"}, 1000,"linear");
          $('#fields2,#codigo_arancelarios').show(1000).animate({width: "show"}, 1000,"linear");
        });
        
    });
  
    $('input:radio[name="tipo_arancel"]').change( function(){
  
       if($(this).is(':checked')) {
  
         var parametros = {
                 'id': $(this).val()
  
         };
         var token= $('input[name="_token"]').val();
         console.log(token);
         $('#id_arancel').val(parametros.id);
        $('#codigo').prop('disabled',false);
         $.ajax({
             type: 'GET',
             url: '/Arancel',
             data: {'arancel': $(this).val(),'_token': token},
             success: function(data){
  
                $table='<table class="table table-bordered table-hover" id="tabla"><tr><th>Código Arancelario</th><th>Descripción</th></tr>';
  
                $('#arancel').html($table);
                $.each(data,function(index,value){
  
               $('#tabla tr:last').after('<tr class="data"><td>'+value.codigo+'</td><td>'+value.descripcion+'</td></tr></table>');
  
                });
  
             }
  
         });
  
      }
  });
  
  $('#codigo').show('modal');
  
  /**------------------------------*/
   var global_id=localStorage.setItem('arancel'," ");
  
  
   $(document).on('click','#codigo',function(){
  
     var id=$(this).parents("tr").attr('id');
     localStorage.setItem('arancel',id);
  
  });
  
   $(document).on('click','tr.data',function(){
  
    var id=localStorage.getItem('arancel');
    var cod=$(this).find("td").eq(0).text();
    var ara=$(this).find("td").eq(1).text();
  
    $("tr#"+id).find("td").eq(0).children("input").val(cod);
    $("tr#"+id).find("td").eq(1).children("input").val(ara);
  
    $('#modal button').click();
    $('#buscador_arancel').val("");
  
  
  
    });
  
   $(document).on('keyup','#buscador_arancel',function(){
  
   var tipo=$('input[name="tipo_arancel"]').val();
  
     $.ajax({
  
        type:'get',
        url: '/Arancel',
        data: {'valor': $(this).val(), 'tipoAran':tipo},
        success: function(data){
  
          $table='<table class="table table-bordered table-hover" id="tabla"><tr><th>Código Arancelario</th><th>Descripción</th></tr>';
  
          $('#arancel').html($table);
          if(data.length==0){
  
           $('#tabla tr:last').after('<tr><td colspan="2" style="color:red;text-align:center" >Códigos no existen</td></tr>');
  
         }else{
          $.each(data,function(index,value){
  
         $('#tabla tr:last').after('<tr class="data"><td>'+value.codigo+'</td><td>'+value.descripcion+'</td><tr></table');
  
          });
  
        }
        }
  
     });
  
  });
  
  
  
  
  var error = '<div class="msg_alert"><span style="color:red" class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span><strong id="cod_aran-error" class="added"> El campo es obligatorio.</strong></div>';
  var valido = '<div class="msg_alert"><span style="color:green" class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span></div>';
  
  //Validación de formulario, se puede llamar y pasar la sección que contenga los inputs a validar
  validacionForm = function(seccion) {
  
        var campos = $('#'+seccion).find('input:text, select, input:file, input:password');
        var n = campos.length;
        var err = 0;
  
        $("div").remove(".msg_alert");
        //bucle que recorre todos los elementos del formulario
        for (var i = 0; i < n; i++) {
                var cod_input = $('#'+seccion).find('input:text, select, input:file, input:password').eq(i);
                if (!cod_input.attr('noreq')) {
                  if (cod_input.val() == '' || cod_input.val() == null)
                  {
                    err = 1;
                    cod_input.css('border', '1px solid red').after(error);
                  }
                  else{
                    if (err == 1) {err = 1;}else{err = 0;}
                    cod_input.css('border', '1px solid green').after(valido);
                  }
              }
        }
        //validación individual de checkbox, al menos 1 debe estar seleccionado
        if ($('#export').is(':checked') || $('#invers').is(':checked') || $('#produc').is(':checked') || $('#comerc').is(':checked')) {
            if (err == 1) {err = 1;}else{err = 0;}
            $('#export').after(valido);
          }
          else{
            err = 1;
            $('#export').after(error);
          }
  
          //excepcion para la parte 2 - accionista
          if (seccion=='datos_persona2') {
            if ($('input:radio[name=accionista]:checked').val()=='Si') {
                key = validaAccionistas('datos_accionistas');
                if(key == 0){if (err == 1) {err = 1;}else{err = 0;}}else{err = 1;}
          }
        }

                  //excepcion para la parte 5 - datos de acceso
                  if (seccion=='datos_persona5') {

                    if(($('#password').val().length < 8) || ($('#password_confirm').val().length < 8)){
                      $('#password').css('border', '1px solid red').after('<div class="msg_alert"><span style="color:red" class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span><strong id="cod_aran-error" class="added"> La contraseña debe tener al menos 8 dígitos</strong></div>');
                    }
    
                    if($('#password').val() !== $('#password_confirm').val()){
                      $('#password').css('border', '1px solid red').after('<div class="msg_alert"><span style="color:red" class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span><strong id="cod_aran-error" class="added"> Las contraseñas no coinciden</strong></div>');
                    }
    
                    if(!$('#password').val().match(/[A-z]/)){
                      $('#password').css('border', '1px solid red').after('<div class="msg_alert"><span style="color:red" class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span><strong id="cod_aran-error" class="added"> La contraseña debe tener al menos una letra</strong></div>');
                    }
    
                    if(!$('#password').val().match(/\d/)){
                      $('#password').css('border', '1px solid red').after('<div class="msg_alert"><span style="color:red" class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span><strong id="cod_aran-error" class="added"> La contraseña debe tener al menos un número</strong></div>');
                    }
                }
  
  
          //Si no hay errores retorna 0 y si hay retornará 1
         if(err==0){return 0;}else{return 1;}
      }
  
  
      validaAccionistas = function(data) {
        var campos = $('#'+data).find('input:text');
        var n = campos.length;
        var err = 0;
        $("div").remove(".msg_alert");
        for (var i = 0; i < n; i++) {
                var cod_input = $('#'+data).find('input:text').eq(i);
                if (cod_input.val() == '' || cod_input.val() == null)
                {
                  err = 1;
                  cod_input.css('border', '1px solid red').after(error);
                }
                else{
                  if (err == 1) {err = 1;}else{err = 0;}
                  cod_input.css('border', '1px solid green').after(valido);
                }
        }
        if(err==0){return 0;}else{return 1;}
      }
  
      validaRadios = function(data) {
        $("div").remove(".msg_alert");
        $('#prod_tecnologia_no').after(error);
        return 1
      }