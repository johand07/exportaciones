@extends('templates/layoutlte')

@section('content')
                
{!!Form::open(['route'=>'Anticipo.store','method'=>'POST','id'=>'anticipo']) !!}
<div class="container">
	<div class="row">
		<div class="12">
			<div class="col-md-3">	
			</div>
			<div class="col-md-6">
        {!! Form::hidden('gen_usuario_id',Auth::user()->id,['readonly'=>'true'])!!}
				
        <div class="form-group"><br>
					{!! Form::label('','Número de Anticipo')!!}
					{!! Form::text('nro_anticipo',null,['class'=>'form-control','id'=>'nro_anticipo','onkeypress'=>'return soloNumerosyLetras(event)','maxlength'=>'30']) !!}
				
        		</div>    
        		<div class="form-group">
                {!! Form::label('','Fecha de Anticipo') !!} 
                <div class="input-group date">
                  {!! Form::text('fecha_anticipo',null,['class'=>'form-control','readonly'=>'readonly','id'=>'fecha_anticipo']) !!}
                  <span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
                </div>                
            </div>
        		<div class="form-group">
        			{!! Form::label('','Consignatario')!!}
          		{!! Form::select('gen_consignatario_id',$consig,null,['class'=>'form-control','id'=>'gen_consignatario_id',
              'placeholder'=>'Seleccione un Consignatario']) !!}
        		</div>
            <div class="form-group">
              {!! Form::label('','Divisa')!!}
              {!! Form::select('gen_divisa_id',$divisa,null,['class'=>'form-control','id'=>'gen_divisa_id',
              'placeholder'=>'Seleccione una Divisa']) !!}
            </div>
        		<div class="form-group">
        			{!! Form::label('','Número Documento Comercial')!!}
          			{!!Form::text('nro_doc_comercial',null,['class'=>'form-control','id'=>'nro_doc_comercial','onkeypress'=>'return soloNumerosyLetras(event)','maxlength'=>'30'])!!}	
        		</div>
        		<div class="form-group">
        			{!! Form::label('','Monto Anticipo')!!}
          			{!!Form::text('monto_anticipo',null,['class'=>'form-control','id'=>'monto_anticipo','onkeypress'=>'return soloNumeros(event)','maxlength'=>'13'])!!}	
        		</div>
            <div class="form-group">
              {!! Form::label('','Monto Total Anticipo')!!}
                {!!Form::text('monto_total_ant',null,['class'=>'form-control','id'=>'monto_total_ant','onkeypress'=>'return soloNumeros(event)','maxlength'=>'40'])!!}  
            </div>
            <div class="form-group">
              {!! Form::label('','Observaciones')!!}
                {!!Form::text('observaciones',null,['class'=>'form-control','id'=>'observaciones','onkeypress'=>'return soloNumerosyLetras(event)','maxlength'=>'100'])!!}  
            </div>	
			</div>
			<div class="col-md-3">
      </div>			
		</div>		
	</div>	
	<div class="row text-center"><br><br>
		<a class="btn btn-primary" href="{{url('exportador/Anticipo')}}">Regresar</a>
     	<input type="submit" name="Enviar" class="btn btn-success center" value="Enviar" onclick="enviaranticipo()">	
	</div>
</div>

{{Form::close()}}
@stop
