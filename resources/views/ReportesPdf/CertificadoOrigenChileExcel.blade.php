<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>CHILE-VENEZUELA</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/style_Certificado.css">
</head>
<body>
 
    <div class="centrar titulo text-normal espacio-superior">
        <table border="0" class="ancho-full">
            <tr>
                <td></td><td></td>
                <td class="ancho-medio centrar" colspan="5" style="height:30px;">
                    <div class="text-normal">CERTIFICADO DE ORIGEN </div>
                </td>
                
                <td></td>
                <td></td>
                <td></td>
                
            </tr>
            
            <tr>
                <td></td><td></td>
                <td class="ancho-medio centrar" colspan="5" style="height:30px;">
                <div class="text-normal"> ASOCIACION LATINOAMERICANA DE INTEGRACION</div>
                </td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            
            <tr>
                <td></td><td></td>
                <td class="ancho-medio centrar" colspan="5" style="height:30px;">
                <div class="text-normal"> ACUERDO DE COMPLEMENTACION ECONOMICA No. 23</div>
                </td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
        </table>
    </div>
    <div class="centrar espacio-superior">
        <table border="1" class="ancho-full">
        <tr>
           
            <td class="ancho-medio centrar borde" colspan="4" style="height:30px;">
            <div class="text-normal">(1) PAÍS EXPORTADOR: <u>{{$certificado->pais_exportador}}</u></div>
            </td>
            <td class="ancho-medio centrar borde" colspan="7" style="height:30px;">
            <div class="text-normal">(2) PAÍS IMPORTADOR: <u>{{$certificado->pais_importador}}</u></div>
            </td>
        </tr>
        </table>
    </div>
    <div class="centrar espacio-superior">
        <table border="1" class="ancho-full borde">
        <tr>
            <td class="ancho-medio centrar top borde" colspan="3" style="width: 70%; height:80px;">
                <div class="text-normal">3. DATOS DEL PRODUCTOR <br>{{$certificado->razon_social_productor}} <br> {{$certificado->direccion_productor}}</div>
            </td>
            <td class="ancho-medio centrar top borde" colspan="3" style="width: 100%; height:80px;">
                <div class="text-normal">4. DATOS DEL EXPORTADOR: <br>{{$det_usuario->razon_social}}<br>
                    {{$det_usuario->direccion}}</div>
            </td>
            <td class="ancho-medio centrar top borde" colspan="5" style="width: 50%; height:80px;">
                <div class="text-normal">5. DATOS DEL IMPORTADOR: <br>{{$importadorCertificado->razon_social_importador}}<br>
                    {{$importadorCertificado->direccion_importador}}<br></div>
            </td>
        </tr>
        </table>
    </div>

<div class="centrar espacio-superior">
        <table class="ancho-full borde table">
            
            <tr class="text-9">
                <td class="top titulo borde" colspan="3" style="height:20px;">6. RUT/RIF PRODUCTOR: {{$certificado->rif_productor}}</td>
                <td class="top titulo borde" colspan="3" style="height:20px;">7. RUT/RIF EXPORTADOR: {{$det_usuario->rif}}<br><br></td>
                <td class="top titulo borde" colspan="5" style="height:20px;">8. RUT/RIF IMPORTADOR: {{$importadorCertificado->rif_importador}} <br><br></td>
            </tr>

            <tr class="text-11">
                <td class="top borde" colspan="3" style="height:20px;"><b>9. PUERTO O LUGAR DE EMBARQUE</b><br><br>{{$importadorCertificado->lugar_embarque_importador}}<br><br><br></td>
                <td class="top borde" colspan="3" style="height:20px;"><b>10. FACTURA COMERCIAL No.:</b><br><br>{{$detdeclaracionCert[0]->numero_factura}}<br><br><br><br></td>
                <td class="top borde" colspan="5" style="height:20px;"> <b>FECHA:</b><br><br>{{$detdeclaracionCert[0]->f_fac_certificado}}<br><br><br></td>
            </tr>

            <tr class="text-11">
                <td class="top titulo borde" colspan="2" style="width: 25%; height:20px;">11. No. DE ORDEN</td>
                <td class="top titulo borde" colspan="2" style="width: 25%; height:20px;">12. CODIGO ARANCELARIO</td>
                <td class="top titulo borde" colspan="3" style="width: 25%; height:20px;">13. DENOMINACION DE LAS MERCANCIAS</td>
                <td class="top titulo borde" colspan="2" style="width: 25%; height:20px;">14. CANTIDAD Y MEDIDA</td>
                <td class="top titulo borde" colspan="2" style="width: 25%; height:20px;">15. VALORES FOB EN US$</td>
            </tr>
            @foreach ($detalleProduct as $key => $detalleP)
            <tr class="text-11">
                <td class="top borde" align="center" colspan="2" style="width: 25%; height:20px;">{{$detalleP->num_orden}}<br><br></td>
                <td class="top borde" align="center" colspan="2" style="width: 25%; height:20px;">{{$detalleP->codigo_arancel}}<br><br></td>
                <td class="top borde" align="center" colspan="3" style="width: 25%; height:20px;">{{$detalleP->denominacion_mercancia}}<br><br></td>
                <td class="top borde" align="center" colspan="2" style="width: 25%; height:20px;">{{$detalleP->unidad_fisica}}<br><br></td>
                <td class="top borde" align="center" colspan="2" style="width: 25%; height:20px;">{{$detalleP->valor_fob}}<br><br></td>
            </tr>
            @endforeach

            <tr class="text-11">
                <td class="top borde" colspan="4"><b>16. No. DE ORDEN</b></td>
                <td class="top borde" colspan="7"><b>17. NORMAS DE ORIGEN</b></td>
            </tr>
            @foreach ($detdeclaracionCert as $key => $detalleCert)
            <tr class="text-11">
                <td class="top borde" align="center" colspan="4">{{$detalleCert->num_orden_declaracion}}<br><br></td>
                <td class="top borde" colspan="7">{{$detalleCert->normas_criterio}}<br><br></td>
            </tr>
            @endforeach
            
            <tr class="text-11">
                <td class="top borde" colspan="11" style="height:50px;"><b>18. OBSERVACIONES</b><br><br>{{$certificado->observaciones}}<br><br><br></td>
            </tr>

            <tr class="text-11">
                <td class="top borde" colspan="4" style="height:20px;"><b>19. DECLARACION DEL EXPORTADOR</b><br><br>{{$det_usuario->razon_social}}</td>
                <td class="top borde" colspan="7" style="height:20px;"><b>20. DECLARACION DE ORIGEN </b><br><br><br></td>

            </tr>
            <tr class="text-medio">
                <td class="top titulo borde" colspan="4" style="height:80px;">Declaro que las mercancías indicadas en el presente formulario,<br>correspondientes a la factura comercial que se cita, cumplen con lo <br>establecido en las Normas de Origen del Acuerdo de <br>Complementación Económica No. 23.<br><br><br><br><br><br><br><br>Sello y Firma del Exportador</td>

                <td class="top titulo borde" colspan="7" style="height:80px;">Certifico la veracidad de la presente declaración que formalizo <br>en la ciudad de ___________________en esta fecha<br><br><br><br><br><br><br><br><br><br>Nombre, Sello y Firma de la Entidad Certificadora</td>

            </tr>
             
        </table>
    <div class="derecha titulo text-11">NOTA: ESTE FORMATO ES DE LIBRE REPRODUCCION</div>

</div>






</body>
</html>