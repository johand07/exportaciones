<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GenSolCvc extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gen_sol_cvc', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('gen_usuario_id')->nullable();
            $table->foreign('gen_usuario_id')->references('id')->on('gen_usuario');
            $table->string('num_sol_cvc',150)->nullable();
            $table->unsignedInteger('gen_status_id')->nullable();
            $table->foreign('gen_status_id')->references('id')->on('gen_status');
            $table->date('fstatus',150)->nullable();
            $table->text('direccion_notif')->nullable();
            $table->integer('clave_pais')->nullable();
            $table->unsignedInteger('gen_aduana_salida_id')->nullable();
            $table->foreign('gen_aduana_salida_id')->references('id')->on('gen_aduana_salida');
            $table->integer('num_serie')->nullable();
            $table->unsignedInteger('pais_destino')->nullable();
            $table->foreign('pais_destino')->references('id')->on('pais');
            $table->unsignedInteger('pais_productor')->nullable();
            $table->foreign('pais_productor')->references('id')->on('pais');
            $table->unsignedInteger('pais_transbordo')->nullable();
            $table->foreign('pais_transbordo')->references('id')->on('pais');
            $table->date('fexportacion',150)->nullable();
            $table->string('nombre_transporte',150)->nullable();
            $table->string('marca_ident',150)->nullable();
            $table->unsignedInteger('cat_cargados_id')->nullable();
            $table->foreign('cat_cargados_id')->references('id')->on('cat_cargados');
            $table->float('peso_neto')->nullable();
            $table->unsignedInteger('gen_unidad_medida_id')->nullable();
            $table->foreign('gen_unidad_medida_id')->references('id')->on('gen_unidad_medida');
            $table->unsignedInteger('cat_tipo_cafe_id')->nullable();
            $table->foreign('cat_tipo_cafe_id')->references('id')->on('cat_tipo_cafe');
            $table->unsignedInteger('cat_metodo_elab_id')->nullable();
            $table->foreign('cat_metodo_elab_id')->references('id')->on('cat_metodo_elab');
            $table->string('norma_calidad_cafe',150)->nullable();
            $table->text('caract_especiales')->nullable();
            $table->string('codio_sist_sa',150)->nullable();
            $table->float('valor_fob')->nullable();
            $table->unsignedInteger('gen_divisa_id')->nullable();
            $table->foreign('gen_divisa_id')->references('id')->on('gen_divisa');
            $table->string('codigo_arancelario',150)->nullable();
            $table->text('info_adicional')->nullable();
            $table->boolean('bactivo')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gen_sol_cvc');
    }
}
