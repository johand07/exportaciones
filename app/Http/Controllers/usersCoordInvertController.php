<?php

namespace App\Http\Controllers;

use App\Models\GenUsuario;
use App\Models\detUsersInversiones;
use App\Models\CatPreguntaSeg;
use App\Models\GenTipoUsuario;
use App\Models\GenStatus;
use Illuminate\Http\Request;
use App\Http\Requests\GenUsuarioRequest;
use Illuminate\Support\Facades\DB;
use Alert;
use Auth;

class usersCoordInvertController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $titulo= 'Usuarios gestores de Inversión';
      $descripcion= 'Gestión de Usuarios';
      $users=GenUsuario::where('bactivo',1)->whereIn('gen_tipo_usuario_id',[9,10])->whereNotIn('id',[2, 4, 1040, 1041, 1325, 1326, 1327, 1328, 1329, 1330, 1798, 1799, 1800, 2059])->get();
      
     
      return view('userManagerInversiones.index',compact('users','titulo','descripcion'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $titulo= 'Creación de Usuarios';
        $descripcion= 'Usuarios del sistema';
        
        $preguntas=CatPreguntaSeg::all()->pluck('descripcion_preg','id');
        $tipousu=GenTipoUsuario::whereIn('id',[9,10])->pluck('nombre_tipo_usu','id');
        $statususu=GenStatus::whereIn('id', [1, 2])->pluck('nombre_status','id');
        
       // dd($oca);
        //dd($statususu);
        return view('userManagerInversiones.create',compact('preguntas','tipousu','statususu', 'titulo', 'descripcion'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(GenUsuarioRequest $request)
    {
        if (isset($request->cat_preguntas_seg_id)) {
        if (isset($request->pregunta_alter)) {

           $pregunta=$request['pregunta_alter'];
        }else{

           $pregunta='N/A';
        }
      }
      $usu= new GenUsuario;
      $usu->email=$request->email;
      $usu->email_seg=$request->email_seg;
      $usu->gen_tipo_usuario_id=$request->gen_tipo_usuario_id;
      $usu->gen_status_id=$request->gen_status_id;
      $usu->password=bcrypt($request->password);
      $usu->cat_preguntas_seg_id=$request->cat_preguntas_seg_id;
      $usu->pregunta_alter=$pregunta;
      $usu->respuesta_seg=$request->respuesta_seg;
      $usu->remember_token=str_random(10);
      $usu->save();

      $Det_usu= new detUsersInversiones;
      $Det_usu->gen_usuario_id=$usu->id;
      $Det_usu->nombre_user=$request->nombre_user;
      $Det_usu->apellido_user=$request->apellido_user;
      $Det_usu->cedula_user=$request->cedula_user;
      $Det_usu->save();

      Alert::success('Registro Exitoso!', 'Usuario agregado.')->persistent("Ok");
      return redirect()->action('usersCoordInvertController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\GenUsuario  $genUsuario
     * @return \Illuminate\Http\Response
     */
    public function show(GenUsuario $genUsuario)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\GenUsuario  $genUsuario
     * @return \Illuminate\Http\Response
     */
    public function edit(GenUsuario $genUsuario, $id)
    {
        $titulo= 'Editar Usuarios Exportadores';
        $descripcion= 'Editar Usuario';
        $usu=GenUsuario::find($id);
        $usu_edit = DB::table('gen_usuario')
              
            ->join('det_users_inversiones', 'gen_usuario.id','=','det_users_inversiones.gen_usuario_id')
              
            ->select(
                    'gen_usuario.id',
                    'gen_usuario.email',
                    'gen_usuario.email_seg',
                    'gen_usuario.gen_tipo_usuario_id',
                    'gen_usuario.gen_status_id',
                    'gen_usuario.cat_preguntas_seg_id',
                    'gen_usuario.respuesta_seg',
                    'det_users_inversiones.nombre_user',
                    'det_users_inversiones.apellido_user',
                    'det_users_inversiones.cedula_user'
                      )
            ->where('gen_usuario.id','=',$id)
            ->first();
        
        if(empty($usu_edit)) {
            $usu_edit = $usu;
        }
        
        // dd($usu_edit);
        $tipousu=GenTipoUsuario::whereIn('id',[9,10])->pluck('nombre_tipo_usu','id');
       
        $statususu=GenStatus::whereIn('id', [1, 2])->pluck('nombre_status','id');
        $preguntas=CatPreguntaSeg::all()->pluck('descripcion_preg','id');
        
        return view('userManagerInversiones.edit', compact('usu_edit','preguntas','titulo','descripcion','tipousu','statususu'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\GenUsuario  $genUsuario
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, GenUsuario $genUsuario, $id)
    {
        if (isset($request->cat_preguntas_seg_id)) {
            if (isset($request->pregunta_alter)) {

               $pregunta=$request['pregunta_alter'];
            }else{

               $pregunta='N/A';
            }
        }
      
        //dd($pregunta);
        $usu= GenUsuario::find($id);
        $usu->email=$request->email;
        $usu->email_seg=$request->email_seg;
        $usu->gen_tipo_usuario_id=$request->gen_tipo_usuario_id;
        $usu->gen_status_id=$request->gen_status_id;
        $usu->password=bcrypt($request->password);
        $usu->cat_preguntas_seg_id=$request->cat_preguntas_seg_id;
        $usu->pregunta_alter=$pregunta;
        $usu->respuesta_seg=$request->respuesta_seg;
        $usu->updated_at=date("Y-m-d H:i:s");
        $usu->save();
      
        detUsersInversiones::updateOrCreate(
            ['gen_usuario_id'=>$id],
            [
                'gen_usuario_id' => $id,
                'nombre_user' => $request->nombre_user,
                'apellido_user' => $request->apellido_user,
                'cedula_user' => $request->cedula_user,
            ]

        );

        Alert::success('Registro Actualizado!', 'Usuario Actualizado.')->persistent("Ok");
        return redirect()->action('usersCoordInvertController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\GenUsuario  $genUsuario
     * @return \Illuminate\Http\Response
     */
    public function destroy(GenUsuario $genUsuario)
    {
        //
    }
}
