@extends('templates/layoutlte_analist_djo')
@section('content')
<div class="panels">

    <table id="listaAgenteAduanal" class="table table-striped table-bordered" style="width:100%">
            <thead>
                <tr>
                   
                    <th>Número de Solicitud </th>
                    <th>Tipo de Solicitud</th>
                    <th>Estatus Solicitud</th>
                    <th>Fecha Solicitud</th>
                    <th>Monto Solicitud</th>
                    <th>Acciones</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
              @foreach($solicitudes as $key=> $solicitud)

                <tr>

                    <td>{{$solicitud->id}}</td>
                    <td>{{$solicitud->tiposolictud->solicitud}}</td>
                    <td>@if(!is_null($solicitud->gen_status_id)){{$solicitud->status->nombre_status}}@endif</td>
                    <td>{{$solicitud->fsolicitud}}</td>
                    <td>{{$solicitud->monto_solicitud}}</td>
                    <td>
                 

                   @if( !is_null($solicitud->monto_solicitud)) 
                      @if(Helper::consulta($solicitud->id,1)) 
                         @if($solicitud->tipo_solicitud_id==1)
                            <a href="{{action('PdfController@planillaEr',array('id'=>$solicitud->id))}}" class="glyphicon glyphicon-file btn btn-default btn-sm" title="Planilla ER" aria-hidden="true"></a>
                             @else
                            <a href="{{action('PdfController@planillaTecno',array('id'=>$solicitud->id))}}" class="glyphicon glyphicon-file btn btn-default btn-sm" title="Planilla ER" aria-hidden="true"></a>
                          @endif
                       @endif
                   @endif
                    </td>
                 <td>@if(!is_null($solicitud->monto_solicitud) && ($solicitud->monto_solicitud > 0))
                     <span style="color:green">Finalizada</span>
                     @else
                     <span style="color:red">Incompleto</span>
                    @endif
                </td>
                </tr>
               @endforeach
            </tbody>
        </table>
  </div>
</div>
</div>
@stop
