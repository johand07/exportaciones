<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NotaCreditoSolicitud extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('NotaCreditoSolicitud', function (Blueprint $table) {
          $table->engine = 'InnoDB';
          $table->increments('id');
          $table->unsignedInteger('gen_nota_credito_id')->index()->nullable();
          $table->foreign('gen_nota_credito_id')->references('id')->on('gen_nota_credito');
          $table->unsignedInteger('gen_solicitud_id')->index()->nullable();
          $table->foreign('gen_solicitud_id')->references('id')->on('gen_solicitud');
          $table->boolean('bactivo')->default(1);
          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::dropIfExists('NotaCreditoSolicitud');
    }
}
