<?php

namespace App\Http\Controllers;


use App\Models\Accionistas;
use Illuminate\Http\Request;

use Auth;
use Session;
use Laracasth\Flash\Flash;
use Alert;

class AccionistaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      
          return view('DatosJuridicos.botones_juridicos');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\DetUsuario  $detUsuario
     * @return \Illuminate\Http\Response
     */
    public function show(Accionistas $accionistas)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\DetUsuario  $detUsuario
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        /************************Consulto la tabla accionista para traerme los que tengo registrado*****************/

        $id=$id;
       
         $accionistas=Accionistas::where('gen_usuario_id',Auth::user()->id)->where('bactivo',1)->get();

        // dd($accionistas);
         $nacionalidad=['V'=>'V','E'=>'E'];
        
       

        ########Defino las variables que contienen el titulo y descripcion del modulo Datos empresa###############################
        $titulo="Registro del Exportador";
        $descripcion="Actualización de Datos ";


         return view('DatosJuridicos.edit_accionista',compact('titulo','descripcion','accionistas','id','nacionalidad'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\DetUsuario  $detUsuario
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Accionistas $accionistas)
    {
         

         $cont_accionista=count($request->nombre_accionista);
         for($i=0;$i<=($cont_accionista-1);$i++){

            Accionistas::updateOrCreate(
            ['id'=>$request->id[$i]],
            ['gen_usuario_id'=>Auth::user()->id,
             'nombre_accionista'=>$request->nombre_accionista[$i],
             'apellido_accionista'=>$request->apellido_accionista[$i],
             'cedula_accionista'=>$request->cedula_accionista[$i],
             'cargo_accionista'=>$request->cargo_accionista[$i],
             'telefono_accionista'=>$request->telefono_accionista[$i],
             'nacionalidad_accionista'=>$request->nacionalidad_accionista[$i],
             'participacion_accionista'=>$request->participacion_accionista[$i],
             'correo_accionista'=>$request->correo_accionista[$i],
             ]

            );

       }

        Alert::success('ACTUALIZACIÓN EXISTOSA','Accionistas Actualizados')->persistent("Ok");
       return redirect()->action('AccionistaController@index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\DetUsuario  $detUsuario
     * @return \Illuminate\Http\Response
     */
    public function destroy(Accionistas $accionistas)
    {
        //
    }

    public function eliminarAccionista(Request $request,$id)
    {
        //dd($_GET['id']);
       $delete=Accionistas::find($_GET['id'])->delete();
       if($delete)
       {
        //dd(1);
          return "Accionista Eliminado";
       }else
       {
         return "Accionista No existe";
       }



    }
}
