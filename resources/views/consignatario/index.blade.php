@extends('templates/layoutlte')

@section('content')
<div class="panels">
<div class="row">
      <div class="col-md-3">
        <a class="btn btn-primary" href="{{url('/exportador/consignatario/create')}}">
          <span class="glyphicon glyphicon-file" title="Agregar" aria-hidden="true"></span>
           Registro Comprador / Proveedor
        </a>
      </div>
    </div>
    <hr>
    <table id="listaAgenteAduanal" class="table table-striped table-bordered" style="width:100%">
            <thead>
                <tr>
                    <th>Comprador/Proveedor</th>
                    <th>Rif</th>
                    <th>Razón Social</th>
                    <th>Contacto</th>
                    <!--th>Cargo</!--th-->
                    <th>País</th>
                    <th>Convenio</th>
                    <th>Ciudad</th>
                    <!--th>Dirección</!--th-->
                    <!--th>Teléfono</!--th-->
                    <!--th>Correo Eléctronico</!--th-->
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody>
              @foreach($consigs as $consig)
                <tr>
                    <td>{{$consig->contacto->descrip_tipocartera}}</td>
                    <td>@if(isset($consig->rif_empresa)){{$consig->rif_empresa}}@else N/A @endif</td>
                    <td>{{$consig->nombre_consignatario}}</td>
                    <td>{{$consig->perso_contac}}</td>
                    <!--td>{{$consig->cargo_contac}}</td-->
                    <td>{{$consig->pais->dpais}}</td>
                    <td>{{$consig->convenio->nombre_convenio}}</td>
                    <td>{{$consig->ciudad_consignatario}}</td>
                    <!--td>{{$consig->direccion}}</!--td-->
                    <!--td>{{$consig->telefono_consignatario}}</td!-->
                    <!--td>{{$consig->correo}}</td-->
                    <td>
                       <a href="{{route('consignatario.edit',$consig->id)}}" class="glyphicon glyphicon-edit btn btn-primary btn-sm" title="Modificar" aria-hidden="true"></a>    
                       
                       <input id="token" type="hidden" name="_token" value="{{ csrf_token() }}">
                       <a href="#" class="btn btn-danger btn-sm" title="Eliminar" onclick="return eliminar('consignatario/',{{$consig->id}},'listaAgenteAduanal')"> <i class="glyphicon glyphicon-trash"></i> </a>
                  </td>
                </tr>  
               @endforeach
            </tbody>
        </table>
  </div>
</div>
</div>
@stop
