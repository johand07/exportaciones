<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PermisoEspecialBovino extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permiso_especial_bovino', function (Blueprint $table) {
           $table->engine = 'InnoDB';
           $table->increments('id');
           $table->unsignedInteger('gen_usuario_id')->index()->nullable();
           $table->foreign('gen_usuario_id')->references('id')->on('gen_usuario');
           $table->string('codigo_arancel', 150)->nullable();
           $table->string('descripcion_arancel', 150)->nullable();
           $table->string('volumen', 150)->nullable();
           $table->string('puerto_salida', 150)->nullable();
           $table->string('puerto_entrada', 150)->nullable();
           $table->string('num_proforma', 150)->nullable();
           $table->date('fembarque_bovino', 150)->nullable();
           $table->date('fvalido', 150)->nullable();
           $table->boolean('bactivo')->default(1)->nullable();
           $table->timestamps();
        }); 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('permiso_especial_bovino');
    }
}
