<?php

namespace App\Http\Controllers;

use App\Models\GenUsuario;
use App\Models\Pais;
use App\Models\GenAduanaSalida;
use Illuminate\Http\Request;

class CalcularEnvioConviasaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         $titulo= 'Conviasa';
        $descripcion= 'Peril del Exportador';/* nsantos*/

        $pais=Pais::where('bactivo',1)->pluck('dpais','id');
        $aduanasal=GenAduanaSalida::all()->pluck('daduana','id');
         return view('CalcularEnvioConviasa.create',compact('titulo','descripcion','pais','aduanasal'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\GenUsuario  $genUsuario
     * @return \Illuminate\Http\Response
     */
    public function show(GenUsuario $genUsuario)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\GenUsuario  $genUsuario
     * @return \Illuminate\Http\Response
     */
    public function edit(GenUsuario $genUsuario)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\GenUsuario  $genUsuario
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, GenUsuario $genUsuario)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\GenUsuario  $genUsuario
     * @return \Illuminate\Http\Response
     */
    public function destroy(GenUsuario $genUsuario)
    {
        //
    }
}
