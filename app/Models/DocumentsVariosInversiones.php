<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DocumentsVariosInversiones extends Model
{
    protected $table = 'documentos_varios_inversiones';

    protected $fillable = [

           'gen_sol_inversionista_id',
           'gen_declaracion_inversion_id',
           'cat_documentos_id',
           'file',
           'bactivo',
    ];
}
