<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableBandejaCoordinadorCertificado extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bandeja_coordinador_certificado', function (Blueprint $table) {
            //
             $table->date('fstatus')->nullable()->after('gen_status_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bandeja_coordinador_certificado', function (Blueprint $table) {
            //
            $table->dropColumn('fstatus');
        });
    }
}
