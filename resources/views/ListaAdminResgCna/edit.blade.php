@extends('templates/layoutlte_admin_resgu_cna')
@section('content')
@php

function extnamefile($sopAdicionalesInv2){
  $explode=explode(".",$sopAdicionalesInv2);
  for($i=0; $i <= count($explode)-1; $i++){
    if($explode[$i] == 'pdf' || $explode[$i] == 'doc' || $explode[$i] =='docx'){
         return $explode[$i];
    }
  }

}
  function extnamefile1($sopAdicionalesInv4){
  $explode=explode(".",$sopAdicionalesInv4);
  for($i=0; $i <= count($explode)-1; $i++){
    if($explode[$i] == 'pdf' || $explode[$i] == 'doc' || $explode[$i] =='docx'){
         return $explode[$i];
    }
  }

}
@endphp
@section('content')

<style type="text/css">
        .centrar{
            text-align: center;
        }

        .titulo{
            
            font-weight: bold;
        }

        .nro-correlativo{
            color: red;
            font-size: 16px;
        }

        .derecha{
            text-align: left;

        }

        .text-normal{
            font-size: 14px;
        }

        .text-medio{
            font-size: 12px;
        }
        .text-11{
            font-size: 11px;
        }
        .text-small{
            font-size: 10px;
        }

        .text-9{
            font-size: 9px;
        }

        .text-8{
            font-size: 8px;
        }

        .espacio-superior{
            margin-top: 10px;
        }

        .espacio-superior-small{
            margin-top: 5px;
        }

        .ancho-medio{
            width: 50%;
        }

        .ancho-full{
            width: 100%;
        }

        .ancho-medio-alto{
            width: 75%;
        }

        .ancho-90{
            width: 90%;
        }

        .table{
            border-collapse: collapse;
        }
        
        .borde{
            border: 1px solid #000;
        }

        .sin-borde{
            border: none;
        }

        .top{
            vertical-align: top;
        }

        .under-dotted{    
            border-bottom: 1px dotted #000;
            text-decoration: none;
        }

        .content-1{
            width: 5%;
        }

        .content-9{
            width: 20%;
        }

        .sangria-1{
            margin-left: 28px;
        }

        .sangria-2{
            margin-left: 36px;
        }

        .sangria-3{
            margin-left: 54px;
        }

        .sangria-sub{
            margin-left: 14px;
            
        }

        
        .page-break { page-break-before: always; }

        .table-center{
            margin: 0 auto; 
        }

        .justify{
            text-align: justify;
        }

        td.justify {
            padding-left: 8px;
            padding-right: 8px; 
        }
        .lineal{
            
        }
        .lineal>div{
            max-width:91%;

        }
        .lineal>*{
           display: inline-block;
           vertical-align:top;
        }

        .sangria-lista{
            list-style-type: none;
            margin: 0;
            padding: 0;
            margin-bottom: 4px;
        }

        .sangria-lista>li{
            margin-top: 5px;
            padding-left: 10px;
            padding-right: 10px;
        }
    </style>
<div class="content"  style="background-color: #fff">
 {{Form::model($solicitud_cna,['route'=>['ListaAnalistCna.update',$solicitud_cna->id],'method'=>'PATCH','id'=>'formanalistaCna'])}}

  <div class="panel-group">
    <div class="panel-primary">
      <div class="panel-heading">
        <div class="row">
          <div class="col-md-8"><h3>ANÁLISIS DE SOLICITUD DE CNA</h3></div>
          <div class="col-md-2"><!-- Traerme la consulta con el numero de solicitud-->
          
             <h4><b>Nº Solicitud:</b></h4>
              {{ $solicitud_cna->num_sol_resguardo}}
          </div>
          
          <div class="col-md-2"><!-- Traerme la consulta con la fecha de solicitud-->
            <h4><b>Fecha Solicitud:</b></h4>
            {{ $solicitud_cna->created_at}}
          </div>
        </div>
      </div>
      <br><br>
      <div class="panel-body">
              <div class="row">
                  <div class="col-md-12">
                  <div class="col-md-4"><button type="button" class="btn btn-warning btn-lg" data-toggle="modal" data-target="#myModal">1er Grupo de Requisitos</button></div>
                  <div class="col-md-4"><button type="button" class="btn btn-warning btn-lg text-right" data-toggle="modal" data-target="#Resguardo">2do Grupo de Requisitos</button></div>
                  <br><br><br><br>

            <div id="myModal" class="modal fade" role="dialog">
                <div class="modal-dialog modal-lg">
                  <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h3 class="justify modal-title text-primary"><b>REQUISITOS DOCUMENTALES EXIGIDOS POR LA UNIDAD ESPECIAL ANTIDROGAS (UEA-45) LA GUAIRA PARA LAS EMPRESAS EXPORTADORAS</b></h3>
                        <div id="mostrar_info">
                          <a href="#"><span class="glyphicon glyphicon-info-sign text-success"> Ver info.</span> </a>
                        </div>
                        <div id="ocultar_info" style="display: none;">
                          <a href="#"><span class="glyphicon glyphicon-resize-small text-warning"> Ocultar info.</span></a>
                        </div>
                        <div class="col-md-12" id="det_info" style="display: none;">
                          <ul>
                            <li>1.) Carta de inscripción ante la Aduana Marítima de la Guaira (SENIAT) - Solo para Agentes Aduanales.
                            </li>
                            <li>2.) Registro Mercantil: Primera y última asamblea y/o asambleas extraordinarias donde se haya modificado el objeto de la empresa, su domicilio, accionistas o representantes legales.</li>
                            <li>3.) Registro de información Fiscal (Rif) de la empresa y del representante legal.</li>
                            <li>4.) Cedula de identidad del representante legal de la empresa.</li>
                            <li>5.) Fotografía tamaño postal del representante legal de la empresa (Debe estar impresa en papel fotográfico).</li>
                            <li>6.) Constancia de residencia Original del representante legal de la empresa (CNE, Consejo Comunal O Junta de Condominio).</li>
                            <li>7.) Fotografías de la fachada de la empresa, desde un plano general hasta un plano especifico (Mínimo 06 fotografías y deben estar impresas en papel fotográfico).</li>
                            <li>8.) Original del recibo de un servicio público de la empresa (Agua, Electricidad, Teléfono, otros).</li>
                            <li>9.) Contrato de arrendamiento del local o Documento de Propiedad.</li>
                          </ul> 
                      </div>  
                    </div>
                    <div class="modal-body">
                        <div class="modal-body">
                            <div role="tabpanel">
                                <ul class="nav nav-tabs" role="tablist">
                                    <li role="presentation" class="active"><a href="#p1" class="cargador-planilla" aria-controls="p1" role="tab" data-toggle="tab" id="tab-inicial"><b>Documento 1</b></a>
                                    </li>
                                    <li role="presentation"><a href="#p2" class="cargador-planilla" aria-controls="p2" role="tab" data-toggle="tab"><b>Documentos 2</b></a>
                                    </li>
                                    <li role="presentation"><a href="#p3" class="cargador-planilla" aria-controls="p3" role="tab" data-toggle="tab"><b>Documentos 3</b></a>
                                    </li>
                                     <li role="presentation"><a href="#p4" class="cargador-planilla" aria-controls="p4" role="tab" data-toggle="tab"><b>Documentos 4</b></a>
                                    </li>
                                     <li role="presentation"><a href="#p5" class="cargador-planilla" aria-controls="p5" role="tab" data-toggle="tab"><b>Documentos 5</b></a>
                                    </li>
                                     <li role="presentation"><a href="#p6" class="cargador-planilla" aria-controls="p6" role="tab" data-toggle="tab"><b>Documentos 6</b></a>
                                    </li>
                                     <li role="presentation"><a href="#p7" class="cargador-planilla" aria-controls="p7" role="tab" data-toggle="tab"><b>Documentos 7</b></a>
                                    </li>
                                     <li role="presentation"><a href="#p13" class="cargador-planilla" aria-controls="p13" role="tab" data-toggle="tab"><b>Documentos 8</b></a>
                                    </li>
                                     <li role="presentation"><a href="#p14" class="cargador-planilla" aria-controls="p14" role="tab" data-toggle="tab"><b>Documentos 9</b></a>
                                    </li>
                                </ul>
                            </div>
                        <div class="tab-content">
                          <div role="tabpanel" class="tab-pane active text-center" id="p1">
                           @if(isset($extencion_file_1) && $extencion_file_1 == 'pdf')
                              <span class="text-success"></span>
                              <br>
                              <embed src="{{asset($docResguardoCna['file_1'])}}" style="width: 100%; height: 100%;"></embed>
                              @elseif(isset($extencion_file_1) && ($extencion_file_1 == 'docx' || $extencion_file_1 == 'doc'))
                                <br><br>
                                <a href="{{asset($docResguardoCna['file_1'])}}" class="btn btn-lg btn-primary"><span><i class="glyphicon glyphicon-cloud-download"></i> </span>Descargar documento 1</a>
                                <br><br>
                              @else
                                .
                          @endif
                          </div>
                          <div role="tabpanel" class="tab-pane text-center" id="p2">
                             @if(isset($extencion_file_2) && $extencion_file_2 == 'pdf')
                                <span class="text-success"></span>
                                <br>
                                 <embed id="fred" src="{{asset($docResguardoCna['file_2'])}}" width="800" height="600" type="application/pdf">
                                @elseif(isset($extencion_file_2) && ($extencion_file_2 == 'docx' || $extencion_file_2 == 'doc'))
                                  <br><br>
                                  <a href="{{asset($docResguardoCna['file_2'])}}" class="btn btn-lg btn-primary"><span><i class="glyphicon glyphicon-cloud-download"></i> </span>Descargar documento 1</a>
                                  <br><br>
                                @else
                                  .
                            @endif
                        </div>
                        <div role="tabpanel" class="tab-pane text-center" id="p3">
                             @if(isset($extencion_file_3) && $extencion_file_3 == 'pdf')
                                <span class="text-success"></span>
                                <br>
                                 <embed id="fred" src="{{asset($docResguardoCna['file_3'])}}" width="800" height="600" type="application/pdf">
                                @elseif(isset($extencion_file_3) && ($extencion_file_3 == 'docx' || $extencion_file_3 == 'doc'))
                                  <br><br>
                                  <a href="{{asset($docResguardoCna['file_3'])}}" class="btn btn-lg btn-primary"><span><i class="glyphicon glyphicon-cloud-download"></i> </span>Descargar documento 1</a>
                                  <br><br>
                                @else
                                  .
                            @endif
                        </div>
                         <div role="tabpanel" class="tab-pane text-center" id="p4">
                             @if(isset($extencion_file_4) && $extencion_file_4 == 'pdf')
                                <span class="text-success"></span>
                                <br>
                                 <embed id="fred" src="{{asset($docResguardoCna['file_4'])}}" width="800" height="600" type="application/pdf">
                                @elseif(isset($extencion_file_4) && ($extencion_file_4 == 'docx' || $extencion_file_4 == 'doc'))
                                  <br><br>
                                  <a href="{{asset($docResguardoCna['file_4'])}}" class="btn btn-lg btn-primary"><span><i class="glyphicon glyphicon-cloud-download"></i> </span>Descargar documento 1</a>
                                  <br><br>
                                @else
                                  .
                            @endif
                        </div>
                        <div role="tabpanel" class="tab-pane text-center" id="p5">
                             @if(isset($extencion_file_5) && $extencion_file_5 == 'pdf')
                                <span class="text-success"></span>
                                <br>
                                 <embed id="fred" src="{{asset($docResguardoCna['file_5'])}}" width="800" height="600" type="application/pdf">
                                @elseif(isset($extencion_file_5) && ($extencion_file_5 == 'docx' || $extencion_file_5 == 'doc'))
                                  <br><br>
                                  <a href="{{asset($docResguardoCna['file_5'])}}" class="btn btn-lg btn-primary"><span><i class="glyphicon glyphicon-cloud-download"></i> </span>Descargar documento 1</a>
                                  <br><br>
                                @else
                                  .
                            @endif
                        </div>
                        <div role="tabpanel" class="tab-pane text-center" id="p6">
                             @if(isset($extencion_file_6) && $extencion_file_6 == 'pdf')
                                <span class="text-success"></span>
                                <br>
                                 <embed id="fred" src="{{asset($docResguardoCna['file_6'])}}" width="800" height="600" type="application/pdf">
                                @elseif(isset($extencion_file_6) && ($extencion_file_6 == 'docx' || $extencion_file_6 == 'doc'))
                                  <br><br>
                                  <a href="{{asset($docResguardoCna['file_6'])}}" class="btn btn-lg btn-primary"><span><i class="glyphicon glyphicon-cloud-download"></i> </span>Descargar documento 1</a>
                                  <br><br>
                                @else
                                  .
                            @endif
                        </div>
                        <div role="tabpanel" class="tab-pane text-center" id="p7">
                            @if(isset($extencion_file_7) && $extencion_file_7 == 'pdf')
                              <span class="text-success"></span>
                              <br>
                               <embed id="fred" src="{{asset($docResguardoCna['file_7'])}}" width="800" height="600" type="application/pdf">
                            @elseif(isset($extencion_file_7) && ($extencion_file_7 == 'docx' || $extencion_file_7 == 'doc'))
                              <br><br>
                              <a href="{{asset($docResguardoCna['file_7'])}}" class="btn btn-lg btn-primary"><span><i class="glyphicon glyphicon-cloud-download"></i> </span>Descargar documento 1</a>
                              <br><br>
                            @elseif(isset($extencion_file_7) && ($extencion_file_7 == 'jpg' || $extencion_file_7 == 'jpeg')|| $extencion_file_7 == 'png'))
                            <img src="{{asset($docResguardoCna['file_7'])}}">
                            @else
                              .
                            @endif

                            @if(isset($extencion_file_8) && $extencion_file_8 == 'pdf')
                              <span class="text-success"></span>
                              <br>
                               <embed id="fred" src="{{asset($docResguardoCna['file_8'])}}" width="800" height="600" type="application/pdf">
                            @elseif(isset($extencion_file_8) && ($extencion_file_8 == 'docx' || $extencion_file_8 == 'doc'))
                              <br><br>
                              <a href="{{asset($docResguardoCna['file_8'])}}" class="btn btn-lg btn-primary"><span><i class="glyphicon glyphicon-cloud-download"></i> </span>Descargar documento 1</a>
                              <br><br>
                            @elseif(isset($extencion_file_8) && ($extencion_file_8 == 'jpg' || $extencion_file_8 == 'jpeg')|| $extencion_file_8 == 'png'))
                            <img src="{{asset($docResguardoCna['file_8'])}}">
                            @else
                              .
                            @endif
                            @if(isset($extencion_file_9) && $extencion_file_9 == 'pdf')
                              <span class="text-success"></span>
                              <br>
                               <embed id="fred" src="{{asset($docResguardoCna['file_9'])}}" width="800" height="600" type="application/pdf">
                            @elseif(isset($extencion_file_9) && ($extencion_file_9 == 'docx' || $extencion_file_9 == 'doc'))
                              <br><br>
                              <a href="{{asset($docResguardoCna['file_9'])}}" class="btn btn-lg btn-primary"><span><i class="glyphicon glyphicon-cloud-download"></i> </span>Descargar documento 1</a>
                              <br><br>
                            @elseif(isset($extencion_file_9) && ($extencion_file_9 == 'jpg' || $extencion_file_9 == 'jpeg')|| $extencion_file_9 == 'png'))
                            <img src="{{asset($docResguardoCna['file_9'])}}">
                            @else
                              .
                            @endif

                            @if(isset($extencion_file_10) && $extencion_file_10 == 'pdf')
                              <span class="text-success"></span>
                              <br>
                               <embed id="fred" src="{{asset($docResguardoCna['file_10'])}}" width="800" height="600" type="application/pdf">
                            @elseif(isset($extencion_file_10) && ($extencion_file_10 == 'docx' || $extencion_file_10 == 'doc'))
                              <br><br>
                              <a href="{{asset($docResguardoCna['file_10'])}}" class="btn btn-lg btn-primary"><span><i class="glyphicon glyphicon-cloud-download"></i> </span>Descargar documento 1</a>
                              <br><br>
                            @elseif(isset($extencion_file_10) && ($extencion_file_10 == 'jpg' || $extencion_file_10 == 'jpeg')|| $extencion_file_10 == 'png'))
                            <img src="{{asset($docResguardoCna['file_10'])}}">
                            @else
                              .
                            @endif
                            @if(isset($extencion_file_11) && $extencion_file_11 == 'pdf')
                              <span class="text-success"></span>
                              <br>
                               <embed id="fred" src="{{asset($docResguardoCna['file_11'])}}" width="800" height="600" type="application/pdf">
                            @elseif(isset($extencion_file_11) && ($extencion_file_11 == 'docx' || $extencion_file_11 == 'doc'))
                              <br><br>
                              <a href="{{asset($docResguardoCna['file_11'])}}" class="btn btn-lg btn-primary"><span><i class="glyphicon glyphicon-cloud-download"></i> </span>Descargar documento 1</a>
                              <br><br>
                            @elseif(isset($extencion_file_11) && ($extencion_file_11 == 'jpg' || $extencion_file_11 == 'jpeg')|| $extencion_file_11 == 'png'))
                            <img src="{{asset($docResguardoCna['file_11'])}}">
                            @else
                              .
                            @endif
                            @if(isset($extencion_file_12) && $extencion_file_12 == 'pdf')
                              <span class="text-success"></span>
                              <br>
                               <embed id="fred" src="{{asset($docResguardoCna['file_12'])}}" width="800" height="600" type="application/pdf">
                            @elseif(isset($extencion_file_12) && ($extencion_file_12 == 'docx' || $extencion_file_12 == 'doc'))
                              <br><br>
                              <a href="{{asset($docResguardoCna['file_12'])}}" class="btn btn-lg btn-primary"><span><i class="glyphicon glyphicon-cloud-download"></i> </span>Descargar documento 1</a>
                              <br><br>
                            @elseif(isset($extencion_file_12) && ($extencion_file_12 == 'jpg' || $extencion_file_12 == 'jpeg')|| $extencion_file_12 == 'png'))
                            <img src="{{asset($docResguardoCna['file_12'])}}">
                            @else
                              .
                            @endif
                        </div>
                         <div role="tabpanel" class="tab-pane text-center" id="p13">
                             @if(isset($extencion_file_13) && $extencion_file_13 == 'pdf')
                                <span class="text-success"></span>
                                <br>
                                 <embed id="fred" src="{{asset($docResguardoCna['file_13'])}}" width="800" height="600" type="application/pdf">
                                @elseif(isset($extencion_file_13) && ($extencion_file_13 == 'docx' || $extencion_file_13 == 'doc'))
                                  <br><br>
                                  <a href="{{asset($docResguardoCna['file_13'])}}" class="btn btn-lg btn-primary"><span><i class="glyphicon glyphicon-cloud-download"></i> </span>Descargar documento 1</a>
                                  <br><br>
                                @else
                                  .
                            @endif
                        </div>
                         <div role="tabpanel" class="tab-pane text-center" id="p14">
                             @if(isset($extencion_file_14) && $extencion_file_14 == 'pdf')
                                <span class="text-success"></span>
                                <br>
                                 <embed id="fred" src="{{asset($docResguardoCna['file_14'])}}" width="800" height="600" type="application/pdf">
                                @elseif(isset($extencion_file_14) && ($extencion_file_14 == 'docx' || $extencion_file_14 == 'doc'))
                                  <br><br>
                                  <a href="{{asset($docResguardoCna['file_14'])}}" class="btn btn-lg btn-primary"><span><i class="glyphicon glyphicon-cloud-download"></i> </span>Descargar documento 1</a>
                                  <br><br>
                                @else
                                  .
                            @endif
                        </div>
                         <div role="tabpanel" class="tab-pane text-center" id="p15">
                             @if(isset($extencion_file_15) && $extencion_file_15 == 'pdf')
                                <span class="text-success"></span>
                                <br>
                                 <embed id="fred" src="{{asset($docResguardoCna['file_15'])}}" width="800" height="600" type="application/pdf">
                                @elseif(isset($extencion_file_15) && ($extencion_file_15 == 'docx' || $extencion_file_15 == 'doc'))
                                  <br><br>
                                  <a href="{{asset($docResguardoCna['file_15'])}}" class="btn btn-lg btn-primary"><span><i class="glyphicon glyphicon-cloud-download"></i> </span>Descargar documento 1</a>
                                  <br><br>
                                @else
                                  .
                            @endif
                        </div>
                        <div role="tabpanel" class="tab-pane text-center" id="p16">
                             @if(isset($extencion_file_16) && $extencion_file_16 == 'pdf')
                                <span class="text-success"></span>
                                <br>
                                 <embed id="fred" src="{{asset($docResguardoCna['file_16'])}}" width="800" height="600" type="application/pdf">
                                @elseif(isset($extencion_file_16) && ($extencion_file_16 == 'docx' || $extencion_file_16 == 'doc'))
                                  <br><br>
                                  <a href="{{asset($docResguardoCna['file_16'])}}" class="btn btn-lg btn-primary"><span><i class="glyphicon glyphicon-cloud-download"></i> </span>Descargar documento 1</a>
                                  <br><br>
                                @else
                                  .
                            @endif
                        </div>
                        <div role="tabpanel" class="tab-pane text-center" id="p17">
                             @if(isset($extencion_file_17) && $extencion_file_17 == 'pdf')
                                <span class="text-success"></span>
                                <br>
                                 <embed id="fred" src="{{asset($docResguardoCna['file_17'])}}" width="800" height="600" type="application/pdf">
                                @elseif(isset($extencion_file_17) && ($extencion_file_17 == 'docx' || $extencion_file_17 == 'doc'))
                                  <br><br>
                                  <a href="{{asset($docResguardoCna['file_17'])}}" class="btn btn-lg btn-primary"><span><i class="glyphicon glyphicon-cloud-download"></i> </span>Descargar documento 1</a>
                                  <br><br>
                                @else
                                  .
                            @endif
                            </div>
                          </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="modal-footer"></div>
              </div>
    <div id="Resguardo" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title text-primary"><b>REQUISITOS DOCUMENTALES EXIGIDOS POR LA UNIDAD ESPECIAL ANTIDROGAS (UEA-45) LA GUAIRA PARA LAS EMPRESAS EXPORTADORAS</b></h3>
                <div id="mostrar_info_soport">
                  <a href="#"><span class="glyphicon glyphicon-info-sign text-success"> Ver info.</span><p></p></a>
                </div>
                <div id="ocultar_info_soport" style="display: none;">
                  <a href="#"><span class="glyphicon glyphicon-resize-small text-warning"> Ocultar info.</span></a>
                </div>
                <div class="col-md-12" id="det_info_soport" style="display: none;">
                  <ul>
                    <li>10. Ultima declaración del ISLR.</li>
                    <li>11. Patente Municipal.</li>
                    <li>12. Permiso de Bomberos vigente.</li>
                    <li>13. Relación completa de la nómina de Trabajadores, indicando nombres, apellidos, cedula de identidad y cargo desempeñado.</li>
                    <li>14. Tres (03) referencias personales del representante legal (Debe incluir, copia de la cedula de identidad, número telefónico, dirección domiciliaria y estampa de las huellas dactilares (ambos pulgares) de quien refiere). Deben ser originales.</li>
                    <li>15. Tres (03) Números de teléfonos alternos o de localización del representante legal, local y celular.</li>
                    <li>16. Ubicación Geo referencial de la Empresa, donde se refleje la ubicación exacta y la indicación de las Coordenadas Geográficas.</li>
                    <li>17. Oficio de remisión de todos los requisitos (dos originales).</li>
                    <li>18.)Oficio de declaración de autenticidad de todos los requisitos consignados (Firmado, sellado y con huelas dactilares). Original..</li>
                    <li>19. Reseña fotográfica de la mercancía a Exportador (NOTA: En caso que las agencias navieras, agencias aduanales o empresas exportadoras, interrumpan las operaciones de     exportación que constantemente realizan, este registro tendrá una vigencia de Seis (06) meses a partir de la última exportación realizada).</li>
                  </ul>

              </div>
            </div>
            <div class="modal-body">
                <div class="modal-body">
                    <div role="tabpanel">
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active">
                            <a href="#Resg1" class="cargador-planilla" aria-controls="Resg1" role="tab" data-toggle="tab" id="tab-inicial">Documentos 13</a>
                            </li>
                            <li role="presentation"><a href="#Resg2" class="cargador-planilla" aria-controls="Resg2" role="tab" data-toggle="tab">Documentos 14</a>
                            </li>
                            <li role="presentation"><a href="#Resg3" class="cargador-planilla" aria-controls="Resg3" role="tab" data-toggle="tab">Documentos 15</a>
                            </li>
                            <li role="presentation"><a href="#Resg4" class="cargador-planilla" aria-controls="Resg4" role="tab" data-toggle="tab">Documentos 16</a>
                            </li>
                            <li role="presentation"><a href="#Resg5" class="cargador-planilla" aria-controls="Resg5" role="tab" data-toggle="tab">Documentos 17</a>
                            </li>
                            <li role="presentation"><a href="#Resg6" class="cargador-planilla" aria-controls="Resg6" role="tab" data-toggle="tab">Documentos 18</a>
                            </li>
                             <li role="presentation"><a href="#Resg7" class="cargador-planilla" aria-controls="Resg7" role="tab" data-toggle="tab">Documentos 19</a>
                            </li>
                            <li role="presentation"><a href="#Resg8" class="cargador-planilla" aria-controls="Resg8" role="tab" data-toggle="tab">Documentos 20</a>
                            </li>
                        </ul>
                    </div>
                    <div class="tab-content">
                      <div role="tabpanel" class="tab-pane active text-center" id="Resg1">
                              @if(isset($extencion_file_15) && $extencion_file_15 == 'pdf')
                                  <span class="text-success"></span>
                                  <br>
                                  <embed src="{{asset($docResguardoCna['file_15'])}}" style="width: 100%; height: 100%;"></embed>
                                  @elseif(isset($extencion_file_15) && ($extencion_file_15 == 'docx' || $extencion_file_15 == 'doc'))
                                    <br><br>
                                    <a href="{{asset($docResguardoCna['file_15'])}}" class="btn btn-lg btn-primary"><span><i class="glyphicon glyphicon-cloud-download"></i> </span>Descargar documento 1</a>
                                    <br><br>
                                  @else
                                    .
                              @endif
                          </div>
                          <div role="tabpanel" class="tab-pane active text-center" id="Resg2">
                              @if(isset($extencion_file_16) && $extencion_file_16 == 'pdf')
                                  <span class="text-success"></span>
                                  <br>
                                  <embed src="{{asset($docResguardoCna['file_16'])}}" style="width: 100%; height: 100%;"></embed>
                                  @elseif(isset($extencion_file_16) && ($extencion_file_16 == 'docx' || $extencion_file_16 == 'doc'))
                                    <br><br>
                                    <a href="{{asset($docResguardoCna['file_16'])}}" class="btn btn-lg btn-primary"><span><i class="glyphicon glyphicon-cloud-download"></i> </span>Descargar documento 1</a>
                                    <br><br>
                                  @else
                                    .
                              @endif
                          </div>
                          <div role="tabpanel" class="tab-pane active text-center" id="Resg3">
                              @if(isset($extencion_file_17) && $extencion_file_17 == 'pdf')
                                  <span class="text-success"></span>
                                  <br>
                                  <embed src="{{asset($docResguardoCna['file_17'])}}" style="width: 100%; height: 100%;"></embed>
                                  @elseif(isset($extencion_file_17) && ($extencion_file_17 == 'docx' || $extencion_file_17 == 'doc'))
                                    <br><br>
                                    <a href="{{asset($docResguardoCna['file_17'])}}" class="btn btn-lg btn-primary"><span><i class="glyphicon glyphicon-cloud-download"></i> </span>Descargar documento 1</a>
                                    <br><br>
                                  @else
                                    .
                              @endif
                          </div>
                           <div role="tabpanel" class="tab-pane active text-center" id="Resg4">
                              @if(isset($extencion_file_18) && $extencion_file_18 == 'pdf')
                                  <span class="text-success"></span>
                                  <br>
                                  <embed src="{{asset($docResguardoCna['file_18'])}}" style="width: 100%; height: 100%;"></embed>
                                  @elseif(isset($extencion_file_18) && ($extencion_file_18 == 'docx' || $extencion_file_18 == 'doc'))
                                    <br><br>
                                    <a href="{{asset($docResguardoCna['file_18'])}}" class="btn btn-lg btn-primary"><span><i class="glyphicon glyphicon-cloud-download"></i> </span>Descargar documento 1</a>
                                    <br><br>
                                  @else
                                    .
                              @endif
                          </div>
                          <div role="tabpanel" class="tab-pane active text-center" id="Resg5">
                              @if(isset($extencion_file_19) && $extencion_file_19 == 'pdf')
                                  <span class="text-success"></span>
                                  <br>
                                  <embed src="{{asset($docResguardoCna['file_19'])}}" style="width: 100%; height: 100%;"></embed>
                                  @elseif(isset($extencion_file_19) && ($extencion_file_19 == 'docx' || $extencion_file_19 == 'doc'))
                                    <br><br>
                                    <a href="{{asset($docResguardoCna['file_19'])}}" class="btn btn-lg btn-primary"><span><i class="glyphicon glyphicon-cloud-download"></i> </span>Descargar documento 1</a>
                                    <br><br>
                                  @else
                                    .
                              @endif
                              @if(isset($extencion_file_20) && $extencion_file_20 == 'pdf')
                                  <span class="text-success"></span>
                                  <br>
                                  <embed src="{{asset($docResguardoCna['file_20'])}}" style="width: 100%; height: 100%;"></embed>
                                  @elseif(isset($extencion_file_20) && ($extencion_file_20 == 'docx' || $extencion_file_20 == 'doc'))
                                    <br><br>
                                    <a href="{{asset($docResguardoCna['file_20'])}}" class="btn btn-lg btn-primary"><span><i class="glyphicon glyphicon-cloud-download"></i> </span>Descargar documento 1</a>
                                    <br><br>
                                  @else
                                    .
                              @endif
                              @if(isset($extencion_file_21) && $extencion_file_21 == 'pdf')
                                  <span class="text-success"></span>
                                  <br>
                                  <embed src="{{asset($docResguardoCna['file_21'])}}" style="width: 100%; height: 100%;"></embed>
                                  @elseif(isset($extencion_file_21) && ($extencion_file_21 == 'docx' || $extencion_file_21 == 'doc'))
                                    <br><br>
                                    <a href="{{asset($docResguardoCna['file_21'])}}" class="btn btn-lg btn-primary"><span><i class="glyphicon glyphicon-cloud-download"></i> </span>Descargar documento 1</a>
                                    <br><br>
                                  @else
                                    .
                              @endif
                          </div>
                           <div role="tabpanel" class="tab-pane active text-center" id="Resg6">
                              @if(isset($extencion_file_22) && $extencion_file_22 == 'pdf')
                                  <span class="text-success"></span>
                                  <br>
                                  <embed src="{{asset($docResguardoCna['file_22'])}}" style="width: 100%; height: 100%;"></embed>
                                  @elseif(isset($extencion_file_22) && ($extencion_file_22 == 'docx' || $extencion_file_22 == 'doc'))
                                    <br><br>
                                    <a href="{{asset($docResguardoCna['file_22'])}}" class="btn btn-lg btn-primary"><span><i class="glyphicon glyphicon-cloud-download"></i> </span>Descargar documento 1</a>
                                    <br><br>
                                  @else
                                    .
                              @endif
                          </div>
                          <div role="tabpanel" class="tab-pane active text-center" id="Resg7">
                              @if(isset($extencion_file_23) && $extencion_file_23 == 'pdf')
                                  <span class="text-success"></span>
                                  <br>
                                  <embed src="{{asset($docResguardoCna['file_23'])}}" style="width: 100%; height: 100%;"></embed>
                                  @elseif(isset($extencion_file_23) && ($extencion_file_23 == 'docx' || $extencion_file_23 == 'doc'))
                                    <br><br>
                                    <a href="{{asset($docResguardoCna['file_23'])}}" class="btn btn-lg btn-primary"><span><i class="glyphicon glyphicon-cloud-download"></i> </span>Descargar documento 1</a>
                                    <br><br>
                                  @else
                                    .
                              @endif
                          </div>
                          <div role="tabpanel" class="tab-pane active text-center" id="Resg8">
                              @if(isset($extencion_file_24) && $extencion_file_24 == 'pdf')
                                  <span class="text-success"></span>
                                  <br>
                                  <embed src="{{asset($docResguardoCna['file_24'])}}" style="width: 100%; height: 100%;"></embed>
                                  @elseif(isset($extencion_file_24) && ($extencion_file_24 == 'docx' || $extencion_file_24 == 'doc'))
                                    <br><br>
                                    <a href="{{asset($docResguardoCna['file_24'])}}" class="btn btn-lg btn-primary"><span><i class="glyphicon glyphicon-cloud-download"></i> </span>Descargar documento 1</a>
                                    <br><br>
                                  @elseif(isset($extencion_file_24) && ($extencion_file_24 == 'jpg' || $extencion_file_24 == 'jpeg')|| $extencion_file_24 == 'png'))
                            <img src="{{asset($docResguardoCna['file_24'])}}">
                                  @else
                                    .
                              @endif
                          </div>
                        </div>
                  </div>
                </div>
                </div>
            </div>
          </div>
          <div class="modal-footer"></div>
      </div><!--soporte-->
    </div>
    <br>
     <div class="row">
          <div class="col-md-3">
             <div class="form-group">
              <h4><b>Código Empresa:</b></h4>
              {{$solicitud_cna->DetUsuario->cod_empresa}}
            </div>
          </div>
          <div class="col-md-3">
              <div class="form-group">
              <h4><b>Rif:</b></h4>
              {{$solicitud_cna->DetUsuario->rif}}
            </div>
          </div>
          <div class="col-md-3">
              <div class="form-group">
              <h4><b>Razón Social:</b></h4>
              {{$solicitud_cna->DetUsuario->razon_social}}
            </div>
        </div>
         <div class="col-md-3">
             <div class="form-group">
              <h4><b>Teléfono 1:</b></h4>
              {{$solicitud_cna->tlf_1}}
            </div>
          </div>
      </div>
      <div class="row">
          <div class="col-md-3">
              <div class="form-group">
              <h4><b>Teléfono 2:</b></h4>
              {{$solicitud_cna->tlf_2}}
            </div>
          </div>
          <div class="col-md-3">
              <div class="form-group">
              <h4><b>Teléfono 3:</b></h4>
              {{$solicitud_cna->tlf_3}}
            </div>
        </div>
         <div class="col-md-3">
              <div class="form-group">
              <h4><b>Ubicación Geo referencial de la Empresa:</b></h4>
              {{$solicitud_cna->ubicacion_georeferencial}}
            </div>
        </div>
         <div class="col-md-3">
              <div class="form-group">
              <h4><b>Nombre del Representante legal:</b></h4>
              {{$solicitud_cna->DetUsuario->nombre_repre}}
            </div>
          </div>
      </div>
      <div class="row">
          <div class="col-md-6">
              <div class="form-group">
              <h4><b>Apellido Representante:</b></h4>
              {{$solicitud_cna->DetUsuario->apellido_repre}}
            </div>
        </div>
        <div class="col-md-6">
             <div class="form-group">
              <h4><b>Cedula Representante:</b></h4>
              {{$solicitud_cna->DetUsuario->ci_repre}}
            </div>
          </div>
      </div>
<br><br><br>
      <!---Botonos de observacion--->
        {{--<div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label>Estado de la Observación</label><br>
                {!!Form::select('gen_status_id',$estado_observacion,null,['placeholder'=>'Seleccione','class'=>'form-control','id'=>'gen_status_id','onchange'=>'validarobservacion ()'])!!}
              </div>
            </div>
          </div>
            <br>
      <div class="row" id="observacion" style="display:none;">
          <b><u>OBSERVACIONES:</u></b>
          <textarea rows="4"  class="form-control text-justify" name="observacion_cna" id="observacion" style="font-weight:bold; text-transform: capitalize;" ></textarea>
      </div>--}}  
          <div class="row text-center"><br><br>
            <a class="btn btn-primary" href="{{route('ListaAdminResgCna.index')}}">Cancelar</a>
            {{--<input type="submit" name="Guardar Observacion" class="btn btn-danger center" value="Guardar Observacion" onclick="return analisisinversion ()">  --}}  
      </div>
    </div>
    </div>
  </div>
</div>
{{Form::close()}}
@stop
<script src="{{asset('js/ajax_v2.1.1.js')}}"></script>

<script type="text/javascript">
$(document).ready(function() {
  $('#mostrar_info').click(function(event) {
          $('#mostrar_info').hide(1000).animate({height: "hide"}, 1000,"linear");
          $('#det_info, #ocultar_info').show(1000).animate({width: "show"}, 1000,"linear");  

  });
  $('#ocultar_info').click(function(event) {
          $('#det_info,#ocultar_info').hide(1000).animate({height: "hide"}, 1000,"linear");
          $('#mostrar_info').show(1000).animate({width: "show"}, 1000,"linear");
  });
  $('#mostrar_info_soport').click(function(event) {
          $('#mostrar_info_soport').hide(1000).animate({height: "hide"}, 1000,"linear");
          $('#det_info_soport, #ocultar_info_soport').show(1000).animate({width: "show"}, 1000,"linear");  

  });
  $('#ocultar_info_soport').click(function(event) {
          $('#det_info_soport,#ocultar_info_soport').hide(1000).animate({height: "hide"}, 1000,"linear");
          $('#mostrar_info_soport').show(1000).animate({width: "show"}, 1000,"linear");
  });
  
});

</script>  
