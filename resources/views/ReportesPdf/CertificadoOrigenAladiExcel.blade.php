<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>ALADI-VENEZUELA</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/style_Certificado.css"> 
</head>
<body>
    
    <!--div class="izquierda titulo text-medio"><b style="color:red">N°</b><span class="">{{ $certificado->num_certificado}}</span></div-->
   <div class="centrar titulo text-normal espacio-superior">
        <table border="0" class="ancho-full">
             <tr>
                <td class="ancho-medio centrar" colspan="8">
                    <div class="text-normal"></div>
                </td>
                <td></td>
                <td></td>
                
            </tr>
            <tr>
                <td class="ancho-medio centrar" colspan="9">
                    <div class="text-normal">CERTIFICADO DE ORIGEN ALADI</div>
                </td>
                <td></td>
                <td></td>
                
            </tr>
            <tr>
                <td class="ancho-medio centrar" colspan="8">
                    <div class="text-normal"></div>
                </td>
                <td></td>
                <td></td>
                
            </tr>
        </table>
       </div>
         <div class="centrar espacio-superior">
                <table border="0" class="ancho-full">
                <tr>
                    
                    <td class="ancho-medio" colspan="3">
                        <div class="text-normal">(1) PAÍS EXPORTADOR: <u>{{$certificado->pais_exportador}}</u></div>
                    </td>
                    <td class="ancho-medio" colspan="3">
                        <div class="text-normal">(2) PAÍS IMPORTADOR: <u>{{$certificado->pais_importador}}</u></div>
                    </td>
                </tr>
                <tr>
                        <td class="ancho-medio centrar" colspan="3">
                            <div class="text-normal"></div>
                        </td>
                        <td></td>
                        <td></td>
                        
                    </tr>
                </table>
            </div>
    <!--Productos-->
            <div class="centrar espacio-superior">
                <table border="0" class="ancho-full">
                    <tr class="text-normal">
                        <td class="ancho-medio centrar" colspan="1">(3) Nº de Orden</td>
                        <td class="ancho-medio" colspan="1">(4) Código Arancelario</td>
                        <td class="ancho-medio" colspan="1">(5) DENOMINACIóN DE LAS MERCADERíAS</td>
                    </tr>
                   
                    @foreach($detalleProduct as $value)
                    
                    <tr class="text-normal">
                        <td class="ancho-medio centrar">{{$value->num_orden}}</td>
                        <td class="ancho-medio">{{$value->codigo_arancel}}</td>
                        <td class="ancho-medio">{{$value->denominacion_mercancia}}</td>
                    </tr>
                    @endforeach

                    @if(count($detalleProduct)<=1)
                        @for($auxConut = 0; $auxConut<=3-count($detalleProduct); $auxConut++)
                        <tr class="text-normal">
                            <td class="ancho-medio">&nbsp;</td>
                            <td class="ancho-medio">&nbsp;</td>
                            <td class="ancho-medio">&nbsp;</td> 
                        </tr>
                        @endfor
                    @endif
                
                </table>
            </div>
    <!--Productos-->
        <!--Declaracion-->
            <table border="0" class="ancho-full">
                <tr class="text-medio">
                    <td></td>
                    <td class="centrar top titulo" colspan="2">DECLARACION DE ORIGEN</td>
                </tr>
                <tr class="text-medio">
                    <td class="ancho-medio" colspan="16">
                        DECLARAMOS que las mercaderías indicadas en el presente formulario, correspondientes a la Factura Comercial Nº {{ $detdeclaracionCert[0]->numero_factura }} cumplen con lo establecido en las Normas de Origen
                    </td> 
                </tr>
                <tr class="text-medio">
                    <td class="ancho-medio" colspan="16">{{ $detdeclaracionCert[0]->nombre_norma_origen }} de conformidad con el siguiente desglose.
                    </td> 
                </tr>
            </table>



            <div class="centrar espacio-superior">
                <table border="0" class="ancho-full">
                    <tr class="text-medio">
                        <td class="ancho-medio" style="width: 15%;"> (6) Nº de Orden</td><br>
                        <td class="ancho-medio" style="width: 80%;">(7) NORMAS </td>
                    </tr>
                    @foreach($detdeclaracionCert as $value1)
                    
                        <tr class="text-center">
                            <td class="ancho-medio centrar">{{$value1->num_orden_declaracion}}</td>
                            <td class="ancho-medio">{{$value1->normas_criterio}}
                        </tr>
                    @endforeach

                    @if(count($detdeclaracionCert)<=1)
                        @for($auxConut = 0; $auxConut<=3-count($detdeclaracionCert); $auxConut++)
                        <tr class="text-medio">
                            <td class="ancho-medio">&nbsp;</td>
                            <td class="ancho-medio">&nbsp;</td>
                        </tr>
                        @endfor
                    @endif
                </table>
            </div>

        <table border="0" class="ancho-full">
            <tr class="text-normal">
                <td class="ancho-medio" colspan="1">(8) Fecha, {{$certificado->fecha_emision_exportador}}</td>
            </tr>
            
            <tr class="text-normal">
                <td class="ancho-medio" colspan="2">(9) Razón social, sello y firma del exportador o productor: {{ $certificado->GenUsuario->DetUsuario->razon_social}}</td>
                <td class="ancho-medio" colspan="2">(9) FIRMA AUTORIZADA</td>
            </tr>
        </table>







    <div class="izquierda text-normal"><b>(10) OBSERVACIONES: ------------------------------------------------------------------------------------------------------------------------------------------------</b><br>{{$certificado->observaciones}}<br><br></div>

        <table border="0" class="ancho-full">
            <tr class="text-normal">
                <td class="ancho-medio" colspan="10">
                    <div class="text-normal">(11) Certifico la veracidad de la presente declaración, que sello y firmo en la ciudad de_____________________________________a los __________________________________________________________________________________________________________</div>
                </td>
            </tr>
        </table>




          <table border="0" class="ancho-full">
                <tr class="text-medio">
                    <td class="centrar top titulo" colspan="10"> <div class="text-normal justify">NOTAS: (*) Esta columna indica el orden en que se individualiza las mercaderías comprendidas en el presente certificado. En caso de ser insuficiente se continuara la </div>
                    </td>
                </tr>
                <tr class="text-medio">
                    <td class="centrar top titulo" colspan="10"> <div class="text-normal justify">individualización de las mercaderias en ejemplares suplementarios de este certificado numerados correlativamente.</div>
                    </td>
                </tr>
                 <tr class="text-medio">
                    <td class="centrar top titulo" colspan="10"> <div class="text-normal justify"></div>
                    </td>
                </tr>
                <tr class="text-medio">
                    <td class="centrar top titulo" colspan="16">
                        <div class="text-normal justify">(**) En este recuadro especificar la NORMA DE ORIGEN, con que cumplen cada mercadería individualizada por su numero de orden.</div>
                    </td>
                    
                </tr>
            </table>



   <div class="centrar espacio-superior">
        <table border="0" class="ancho-full">
            <tr class="text-medio">
                 <td class="ancho-medio" colspan="1">
                    <b class="text-medio">ORIGINAL: ADUANA DE DESTINO</b>
                </td>
            </tr>

             <tr class="text-medio">
                 <td class="ancho-medio" colspan="1">
                    <b class="text-medio">DUPLICADO: EXPORTADOR</b>
                </td>
                <td class="ancho-medio" colspan="1">
                    <b class="text-medio">CUADRUPLICADO: MINISTERIO</b>
                </td>
                <td class="ancho-medio" colspan="11">
                    <b class="text-medio">EL FORMULARIO NO PODRA PRESENTAR RASPADURAS, TACHADURAS O ENMIENDAS</b>
                </td>
            </tr>

            <tr class="text-medio">
                 <td class="ancho-medio" colspan="1">
                    <b class="text-medio">TRIPLICADO: MINISTERIO</b>
                </td>
            </tr>
        </table>
   </div>
</body>
</html>