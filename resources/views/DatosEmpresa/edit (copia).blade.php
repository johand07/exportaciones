@extends('templates/layoutlte')
@section('content')

<!--Modal de los codigos Arancelarios-->
@component('modal_dialogo',['arancel'=>$arancel])

@slot('header') <span>Código Arancelario</span> @endslot

@slot('body')  <div id="arancel"></div>

@endslot

@slot('footer')@endslot

@endcomponent

<!--Fin de la modal de codigos Arancelarios-->
  <div class="container">
	{{Form::model($datosempresa,['route'=>['DatosEmpresa.update',$datosempresa->id],'method'=>'PATCH','id'=>'FormDatosEmpresa']) }}
	<!--@php $contador=count($datosempresa); @endphp-->
@php $contador=count($accionistas); @endphp
@php $contador_prod=count($productos); @endphp

  {{Form::hidden('metodo','Editar')}}
  {{Form::hidden('contador',$contador)}}
  {{Form::hidden('contador_prod',$contador_prod)}}
    <!--{{Form::hidden('contador',$contador)}}--><!--Creo un contador oculto!-->

<!--***************Parte 1 Registro****************************-->

   <div id="datos_persona1">
      <ol class="breadcrumb">
        <li class="active">Paso 1</li>
      </ol><!--Se le coloco una miga de pan a cada parte del registro para identificar el orden-->
	  <div class="panel panel-primary">
		<div class="panel-heading">DATOS DE LA EMPRESA</div>
		<div class="panel-body">

			<div class="row">
				<div class="col-md-3">
					<div class="form-group">
						{!! Form::label('','Nombre o Razón Social')!!}
						{!! Form::text('razon_social',null, ['class'=>'form-control','id'=>'razon_social','onkeypress'=>'return soloNumerosyLetras(event)','maxlength'=>'100']) !!}
					</div>
				</div>
				<div class="col-md-1">
					<div class="form-group">
						{!! Form::label('','Siglas')!!}
						{!! Form::text('siglas',null, ['class'=>'form-control','id'=>'siglas','onkeypress'=>'return soloLetras(event)','maxlength'=>'50']) !!}
					</div>
				</div>
				<div class="col-md-2">
					<div class="form-group">
						{!! Form::label('','Rif')!!}
						{!! Form::text('rif',null, ['class'=>'form-control','id'=>'rif','readonly'=>'true','maxlength'=>'11']) !!}
					</div>
				</div>
				<div class="col-md-2">
					<div class="form-group">
						{!! Form::label('','Sector')!!}
						{!!Form::select('id_gen_sector',$sectores,null, ['class'=>'form-control','id'=>'id_gen_sector']) !!}
					</div>
				</div>
				<div class="col-md-2">  
					<div class="form-group">
						{!! Form::label('','Actividad Económica')!!}
						{!!Form::select('gen_actividad_eco_id',$actividadeco,null, ['placeholder'=>'Seleccione','class'=>'form-control','id'=>'id_gen_actividad_eco']) !!}
					</div>
				</div>
				<div class="col-md-2">
					<div class="form-group">
						{!! Form::label('','Tipo de Empresa')!!}
						{!!Form::select('gen_tipo_empresa_id',$tipos_empresas,null,['placeholder'=>'Seleccione','class'=>'form-control','id'=>'gen_tipo_empresa_id']) !!}
					</div>
				</div>
			</div>
			<div class="panel panel-primary">
                <div class="panel-heading">TIPO DE ACTIVIDAD ECONÓMICA</div>
              </div>
              <div class="row">
                <div class="col-md-3">
                   {!! Form::checkbox('export','Exportador',$datosempresa->export, ['class'=>'checkbox-inline','id'=>'export']) !!}
                   {!! Form::label('','Exportador')!!}
                </div>
                <div class="col-md-3">
                   {!! Form::checkbox('invers','Inversionista',$datosempresa->invers,['class'=>'checkbox-inline','id'=>'invers']) !!}
                   {!! Form::label('','Inversionista')!!}
                </div>
                <div class="col-md-3">
                   {!! Form::checkbox('produc','Productor',$datosempresa->produc, ['class'=>'checkbox-inline','id'=>'produc']) !!}
                   {!! Form::label('','Productor')!!}
                </div>
                <div class="col-md-3">
                   {!! Form::checkbox('comerc','Comercializadora',$datosempresa->comerc, ['class'=>'checkbox-inline','id'=>'comerc']) !!}
                   {!! Form::label('','Comercializadora')!!}
                </div>
               </div>
               <br>
			<div class="panel panel-primary">
				<div class="panel-heading">DIRECCION</div>
			</div>
			<div class="row">
				<div class="col-md-3">
					<div class="form-group">
						{!! Form::label('','País')!!}
						{!!Form::select('pais_id',$paises,null,['class'=>'form-control','placeholder'=>'Seleccione un pais','id'=>'pais_id']) !!}
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						{!! Form::label('','Estado')!!}
						{!!Form::select('estado_id',$estados,null,['placeholder'=>'Seleccione','class'=>'form-control','id'=>'id_estado']) !!}
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						{!! Form::label('','Municipio')!!}
						{!!Form::select('municipio_id',$municipios,null, ['placeholder'=>'Seleccione','class'=>'form-control','id'=>'id_municipio']) !!}
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						{!! Form::label('','Parroquia')!!}
						{!!Form::select('parroquia_id',$parroquias,null, ['placeholder'=>'Seleccione','class'=>'form-control','id'=>'id_parroquia']) !!}
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						{!! Form::label('','Dirección')!!}
						{!! Form::text('direccion',null, ['class'=>'form-control','id'=>'direccion','onkeypress'=>'return soloNumerosyLetras(event)','maxlength'=>'200']) !!}
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						{!! Form::label('','Teléfono Local')!!}
						{!! Form::text('telefono_local',null, ['class'=>'form-control','id'=>'telefono_local','onkeypress'=>'return soloNumeros(event)','maxlength'=>'11']) !!}
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						{!! Form::label('','Teléfono Celular')!!}
						{!! Form::text('telefono_movil',null, ['class'=>'form-control','id'=>'telefono_movil','onkeypress'=>'return soloNumeros(event)','maxlength'=>'11']) !!}
					</div>
				</div>
			</div>
			<div class="row">
              <div class="col-md-12">
                <div class="col-md-4"></div>
                <div class="col-md-4 text-center">
                  <div class="form-group">

                    <a class="btn btn-primary" href="#" id="seguir1">Siguiente</a><!--Boton siguiente del Paso 1 del Formulario-->
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
       </div> 




 <!--***************Paso 2  del Registro********************************************************-->
      <div id="datos_persona2" style="display: none;">
            <ol class="breadcrumb">
              <li><a href="#" id="paso1">Paso 1</a></li>
              <li class="active" id="paso2">Paso 2</li>
            </ol>
			<div class="panel panel-primary">
				<div class="panel-heading">DATOS DE LA PERSONA JURIDICA O FIRMA PERSONAL</div>

			  <div class="panel-body">
			  <div class="row">
				<div class="col-md-4">
					<div class="form-group">
						{!! Form::label('','Registro de Circunscripción Judicial')!!}
						{!!Form::select('circuns_judicial_id',$circuns_judiciales,null,['placeholder'=>'Seleccione','class'=>'form-control','id'=>'circuns_judicial_id']) !!}
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						{!! Form::label('','Número')!!}
						{!! Form::text('num_registro',null, ['class'=>'form-control','id'=>'num_registro','onkeypress'=>'return soloNumerosyLetras(event)','maxlength'=>'60']) !!}
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						{!! Form::label('','Tomo')!!}
						{!! Form::text('tomo',null, ['class'=>'form-control','id'=>'tomo','onkeypress'=>'return soloNumerosyLetras(event)','maxlength'=>'30']) !!}
					</div>
				</div>
			</div>

				<div class="row">
				<div class="col-md-4">
					<div class="form-group">
						{!! Form::label('','Folio')!!}
						{!! Form::text('folio',null, ['class'=>'form-control','id'=>'folio','onkeypress'=>'return soloNumerosyLetras(event)','maxlength'=>'30']) !!}
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						{!! Form::label('','Capital Social')!!}
						{!! Form::text('capital_social',null, ['class'=>'form-control','id'=>'capital_social','onkeypress'=>'return soloNumeros(event)','maxlength'=>'60']) !!}
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						{!! Form::label('','Fecha de Registro Mercantil')!!}
						<div class="input-group date">
							{!! Form::text('f_registro_mer',null, ['class'=>'form-control','id'=>'f_registro_mer','onchange'=>'return validarfechaRM(this.value,"f_registro_mer")','maxlength'=>'','readonly'=>'readonly']) !!}
							<span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
						</div> 
					</div>
				</div>
			</div>
			<div class="panel panel-primary">
				<div class="panel-heading">DATOS DEL PRESIDENTE</div>
			</div>
			<div class="row">
				<div class="col-md-3">
					<div class="form-group">
						{!! Form::label('','Nombre')!!}
						{!! Form::text('nombre_presid',null, ['class'=>'form-control','id'=>'nombre_presid','onkeypress'=>'return soloLetras(event)','maxlength'=>'50']) !!}
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						{!! Form::label('','Apellido')!!}
						{!! Form::text('apellido_presid',null, ['class'=>'form-control','id'=>'apellido_presid','onkeypress'=>'return soloLetras(event)','maxlength'=>'50']) !!}
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						{!! Form::label('','Cédula')!!}
						{!! Form::text('ci_presid',null, ['class'=>'form-control','id'=>'ci_presid','onkeypress'=>'return CedulaFormatrep(this,"Cedula de Indentidad Invalida",-1,true,event)','maxlength'=>'11']) !!}<small>Ejemplo:V-00000000</small>
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						{!! Form::label('','Cargo')!!}
						{!! Form::text('cargo_presid',null, ['class'=>'form-control','id'=>'cargo_presid','onkeypress'=>'return soloLetras(event)','maxlength'=>'80','value'=>'Presidente']) !!}
					</div>
				</div>
			</div>
			<div class="panel panel-primary">
				<div class="panel-heading">DATOS DEL REPRESENTANTE LEGAL</div>
			</div>
			<div class="row">
				<div class="col-md-3">
					<div class="form-group">
						{!! Form::label('','Nombre')!!}
						{!! Form::text('nombre_repre',null, ['class'=>'form-control','id'=>'nombre_repre','onkeypress'=>'return soloLetras(event)','maxlength'=>'50']) !!}
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						{!! Form::label('','Apellido')!!}
						{!! Form::text('apellido_repre',null, ['class'=>'form-control','id'=>'apellido_repre','onkeypress'=>'return soloLetras(event)','maxlength'=>'50']) !!}
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
					
						{!! Form::label('','Cédula')!!}
						{!! Form::text('ci_repre',null, ['class'=>'form-control','id'=>'ci_repre','onkeypress'=>'return CedulaFormatrep(this,"Cedula de Indentidad Invalida",-1,true,event)','maxlength'=>'11']) !!}<small>Ejemplo:V-00000000</small>
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						{!! Form::label('','Cargo')!!}
						{!! Form::text('cargo_repre',null, ['class'=>'form-control','id'=>'cargo_repre','onkeypress'=>'return soloLetras(event)','maxlength'=>'80']) !!}
					</div>
				</div>
			</div>

			
			<div class="panel panel-primary">
                  <div class="panel-heading">DATOS DE LOS ACCIONISTAS</div>
                </div>
	            <div class="row">
	              <div class="col-md-3"></div>
	              <div class="col-md-6 text-center">
	                <div class="form-group">
	                  {!! Form::label('','¿Posée Accionista?')!!}
	                  {!! Form::label('','Si')!!}
	                  {!! Form::radio('accionista','Si',false,['class'=>'radio-inline','id'=>'accion_si'])!!}
	                  {!! Form::label('','No')!!}
	                  {!! Form::radio('accionista','No',true,['class'=>'radio-inline','id'=>'accion_no'])!!}
	                </div>
	              </div>
	              <div class="col-md-3"></div>
	            </div>
                
                  <table class="table table-bordered" id="fields" style="display:none;">
                   <tr> <td><button  name="add" type="button" id="add" value="add more" class="btn btn-success" onclick="">Agregar</button></td></tr>

                    <tr>
                      <th>Nombre</th>
                      <th>Apellido</th>
                      <th>Cédula</th>
                      <th>Cargo</th>
                    </tr>
                   
                    @if (isset($accionistas))
                        @foreach ($accionistas as $key=>$accionista)
                       		<tr id="row{{$key+1}}">

			                      <td>{!!Form::hidden('id',$accionista->id)!!}{!!Form::text('nombre_accionista[]',$accionista->nombre_accionista,['class'=>'form-control','id'=>'nombre_accionista','onkeypress'=>'return soloLetras(event)','maxlength'=>'50'])!!}</td>
			                      <td>{!!Form::text('apellido_accionista[]',$accionista->apellido_accionista,['class'=>'form-control','id'=>'apellido_accionista','onkeypress'=>'return soloLetras(event)','maxlength'=>'50'])!!}</td>

			                      <td>{!!Form::text('cedula_accionista[]',$accionista->cedula_accionista,['class'=>'form-control','id'=>'cedula_accionista','onkeypress'=>'return CedulaFormat1(this,"Cedula de Indentidad Invalida",-1,true,event)','maxlength'=>'11'])!!}<small>Ejemplo:V-00000000</small></td>

			                     <td>{!!Form::text('cargo_accionista[]',$accionista->cargo_accionista,['class'=>'form-control','id'=>'cargo_accionista','onkeypress'=>'return soloLetras(event)','maxlength'=>'80'])!!}</td>
			                     <td> {!!Form::hidden('id[]',$accionista->id)!!}<button  name="remove" type="button" id="{{$key+1}}" value="" class="btn btn-danger btn-remove">x</button></td>
                    		</tr> 
                        @endforeach
                    @endif
                  </table>
                
                <div class="row">
                  <div class="col-md-12">
                    <div class="col-md-4"></div>
                    <div class="col-md-4 text-center">
                      <div class="form-group">
                        <a class="btn btn-primary" href="#" id="atras1">Atras</a>
                        <a class="btn btn-primary" href="#" id="seguir2">Siguiente</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>






<!--***************Parte 3 Registro*********************************************************-->
          <div id="datos_persona3" style="display: none;">
            <ol class="breadcrumb">
              <li><a href="#" id="paso11">Paso 1</a></li>
              <li><a href="#" id="paso22">Paso 2</a></li>
              <li class="active" id="paso3">Paso 3</li>
            </ol>
            
          
            <div class="panel panel-primary">
              <div class="panel-heading">DATOS DE LOS PRODUCTOS QUE PRODUCE Y/O COMERCIALIZA</div>
             <div class="panel-body">
             	<div class="row">
                <div class="col-md-1"></div>
                   <div class="col-md-10 text-center">
                      {!! Form::label('','¿Exporta Productos de Servicios y/o Tecnología?',['class'=>'form-control'])!!}
                      {!! Form::label('','Si',['class'=>'radio-inline'])!!}
                      {!! Form::radio('prod_tecnologia','3',false,['class'=>'radio-inline','id'=>'prod_tecnologia_si'])!!}
                      {!! Form::label('','No',['class'=>'radio-inline'])!!}
                      {!! Form::radio('prod_tecnologia','4',false,['class'=>'radio-inline','id'=>'prod_tecnologia_no'])!!}
                      
                    </div>
                  <div class="col-md-1"></div>
                </div><br>
                <div class="row" id="tecno_especifique" style="display: none;">
                  <div class="col-md-1"></div>
                  <div class="col-md-10">
                    {!! Form::label('','Especifique')!!}
                    {!!Form::text('especifique_tecno',null,['class'=>'form-control','id'=>'especifique_tecno','onkeypress'=>'return soloLetras(event)','maxlength'=>'100'])!!}
                  </div>
                  <div class="col-md-1"></div>
                </div>
                
               <div class="row" id="codigo_arancelarios" style="display: none;">
                   <div class="col-md-3"></div>
                    <div class="col-md-6 text-center">
                      {!!Form::radio('tipo_arancel','1',false,['class'=>'radio-inline','id'=>'cod_andina'])!!}
                      {!! Form::label('','Nandina',['class'=>'radio-inline'])!!}
                      {!! Form::radio('tipo_arancel','2',false,['class'=>'radio-inline','id'=>'cod_mercosur'])!!}
                      {!! Form::label('','Mercosur',['class'=>'radio-inline'])!!}
                    </div>
                  <div class="col-md-3"></div>
                </div><br>
                <table class="table table-bordered text-center" id="fields2" style="display: none;">
                    <tr><td><button  name="add2" type="button" id="add2" value="add more" class="btn btn-success">Agregar</button></td></tr>
                  <tr>
                    <th>Código Arancelario </th>
                    <th>Descripción Arancelaria</th>
                    <th>Descripción Comercial</th>
                    <th>Unidad de Medida</th>
                    <th>Capacidad Operativa Anual</th>
                    <th>Capacidad Instalada Anual</th>
                    <th>Capacidad de Almacenamiento Anual</th>                   
                    <th></th>

                  </tr>
                  	@if (isset($productos))
                        @foreach ($productos as $key=>$producto)
 
			                <tr id="fila{{$key+1}}">
								
			                    <td>
			                    {!!Form::text('cod_arancel[]',$producto->cod_arancel,['class'=>'form-control','data-toggle'=>"modal",'data-target'=>"#modal",'id'=>'cod_arancel_edit','readonly'=>'true'])!!}
			                    {!!Form::hidden('id',$producto->id)!!}
			                    </td>
			                    <td>{!!Form::text('descrip_arancel[]',$producto->descrip_arancel,['class'=>'form-control','id'=>'descrip_arancel','onkeypress'=>'return soloLetras(event)'])!!}</td>
			                    <td>{!!Form::text('descrip_comercial[]',$producto->descrip_comercial,['class'=>'form-control','id'=>'descrip_comercial','onkeypress'=>'return soloLetras(event)'])!!}</td>
			                    <td>{!!Form::select('gen_unidad_medida_id[]',$unidad_medida,$producto->gen_unidad_medida_id,['placeholder'=>'--Seleccione una Unidad de Medida--', 'class'=>'form-control','id'=>'gen_unidad_medida_id']) !!}</td>
			                    <td>{!!Form::text('cap_opera_anual[]',$producto->cap_opera_anual,['class'=>'form-control','id'=>'cap_opera_anual','onkeypress'=>'return soloNumeros(event)'])!!}</td>
			                    <td>{!!Form::text('cap_insta_anual[]',$producto->cap_insta_anual,['class'=>'form-control','id'=>'cap_insta_anual','onkeypress'=>'return soloNumerosletrasyPorcentaje(event)'])!!}</td>
			                    <td>{!!Form::text('cap_alamcenamiento[]',$producto->cap_alamcenamiento,['class'=>'form-control','id'=>'cap_alamcenamiento','onkeypress'=>'return soloNumeros(event)'])!!}</td>
			                    <td> {!!Form::hidden('id[]',$producto->id)!!}<button  name="remove" type="button" id="{{$key+1}}" value="" class="btn btn-danger btn-remove2">x</button></td>
			                </tr>
                 		@endforeach
                    @endif
                </table>
                <div class="row">
                  <div class="col-md-12">
                    <div class="col-md-4"></div>
                    <div class="col-md-4 text-center">
                      <div class="form-group">
                        <a class="btn btn-primary" href="#" id="atras2">Atras</a>
                        <a class="btn btn-primary" href="#" id="seguir3">Siguiente</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>








<!--***************Parte 4 Registro**********************************************************-->

		 <div id="datos_persona4" style="display: none;">
            <ol class="breadcrumb">
              <li><a href="#" id="paso111">Paso 1</a></li>
              <li><a href="#" id="paso222">Paso 2</a></li>
              <li><a href="#" id="paso333">Paso 3</a></li>
              <li class="active" id="paso4">Paso 4</li>
            </ol>   
            <div class="panel panel-primary">
              <div class="panel-heading">DATOS DE ACCESO</div>
            <div class="panel-body">

			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						{!! Form::label('','Correo Electrónico')!!}
						{!! Form::text('correo',null, ['class'=>'form-control','id'=>'correo','onkeyup'=>"minuscula(this.value,'#email');",'maxlength'=>'80']) !!}
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						{!! Form::label('','Correo Alternativo')!!}
						{!! Form::text('correo_sec',null, ['class'=>'form-control','id'=>'correo_sec','onkeyup'=>"minuscula(this.value,'#email');",'maxlength'=>'80']) !!}
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-4"></div>
					<div class="col-md-4 text-center">
						<div class="form-group">
							<!--<a href="{{url('/home')}}" class="btn btn-primary">Cancelar</a>-->
							<a href="#" class="btn btn-primary" id="atras3">Atras</a>
							{!! Form::submit('Enviar', ['class' => 'btn btn-primary','onclick'=>'enviardatoempresa()'] ) !!}
						</div>
					</div>
					<div class="col-md-4"></div>
				</div>
			</div>
		</div>
	</div>
	{{Form::close()}}
</div>
<!--***************************Script !***************************************************-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>


<script type="text/javascript">

$(document).ready(function (){
 var x = $('input[name="contador"]').val();
 var i=parseInt(x);

      $('#add').click(function(){
       i++;


///campos dinamicos de los accionista***********************************/
  $('#fields tr:last').after('<tr id="row'+i+'" display="block" class="show_div"><td>{!!Form::text('nombre_accionista[]',null,['class'=>'form-control ejem','onkeypress'=>'return soloLetras(event)','maxlength'=>'50'])!!}</td><td>{!!Form::text('apellido_accionista[]',null,['class'=>'form-control','onkeypress'=>'return soloLetras(event)','maxlength'=>'50'])!!}</td><td>{!!Form::text('cedula_accionista[]',null,['class'=>'form-control','onkeypress'=>'return CedulaFormatrep(this,"Cedula de Indentidad Invalida",-1,true,event)','maxlength'=>'11'])!!}<small>Ejemplo:V-00000000</small></td><td>{!!Form::text('cargo_accionista[]',null,['class'=>'form-control','onkeypress'=>'return soloLetras(event)','maxlength'=>'80'])!!}</td><td>{!!Form::hidden('accionista_id',$id)!!}{!!Form::hidden('id[]',0)!!}<button  name="remove" type="button" id="'+i+'" value="" class="btn btn-danger btn-remove3">x</button></td></tr>');


       });

 var xx = $('input[name="contador_prod"]').val();
 var j=parseInt(xx);
      $('#add2').click(function(){
       j++;

/*****************************************Campos dinamicos de codigos arancelarios********************************************************************/ 

   $('#fields2 tr:last').after('<tr id="fila'+j+'" display="block" class="show_div"><td>{!!Form::text('cod_arancel[]',null,['class'=>'form-control ejem','data-toggle'=>"modal",'data-target'=>"#modal",'id'=>"cod_arancel",'readonly'=>'true'])!!}</td><td>{!!Form::text('descrip_arancel[]',null,['class'=>'form-control','onkeypress'=>'return soloLetras(event)'])!!}</td><td>{!!Form::text('descrip_comercial[]',null,['class'=>'form-control','onkeypress'=>'return soloLetras(event)'])!!}</td><td>{!!Form::select('gen_unidad_medida_id[]',$unidad_medida,null,['placeholder'=>'--Seleccione una Unidad de Medida--', 'class'=>'form-control']) !!}</td><td>{!!Form::text('cap_opera_anual[]',null,['class'=>'form-control','onkeypress'=>'return soloNumeros(event)'])!!}</td><td>{!!Form::text('cap_insta_anual[]',null,['class'=>'form-control','onkeypress'=>'return soloNumerosletrasyPorcentaje(event)'])!!}</td><td>{!!Form::text('cap_alamcenamiento[]',null,['class'=>'form-control','onkeypress'=>'return soloNumeros(event)'])!!}</td><td>{!!Form::hidden('productos_id',$id)!!}{!!Form::hidden('id[]',0)!!}<button  name="remove" type="button" id="'+j+'" value="" class="btn btn-danger btn-remove4">x</button></td></tr>');


       });

$(document).on('click','.btn-remove',function(){

       var id_boton= $(this).attr("id");
       var id=$(this).prev().val();

    if(id!=0){

        if (confirm('Deseas eliminar este Producto?')) {

	      $.ajax({

	       'type':'get',
	       'url':'eliminarAccionista',
	       'data':{ 'id':id},
	       success: function(data){

	        //$('#row'+id_boton).remove();

	         }

	       });

        }



     }else{

      $('#row'+id_boton).remove();
     }



    });




     $(document).on('click','.btn-remove3',function(){
        var id_boton= $(this).attr("id");
        $("#row"+id_boton+"").remove();

     });
     $(document).on('click','.btn-remove4',function(){
        var id_boton= $(this).attr("id");
        $("#fila"+id_boton+"").remove();

     });

/***************Accionista pregunta*******************************************************************/
  $('#accion_si').click(function(event) {

    $('#fields').show(1000).animate({width: "show"}, 1000,"linear");///Mostrar tabla de los accionista al seleccionar la opcion si///

  });

  $('#accion_no').click(function(event) {

    $('#fields').hide(1000).animate({height: "hide"}, 1000,"linear");///Ocultar la tabla de los accionista al seleccionar la opcion no////

  });


//////////////////////////////Paso_1//////////////////////////////////////

      $('#seguir1').click(function(event) {
        
          $('#datos_persona2').show(1000).animate({width: "show"}, 1000,"linear");
          $('#datos_persona1,#datos_persona3,#datos_persona4').hide(1000).animate({height: "hide"}, 1000,"linear");
         
      });
//////////////////////////////Paso_2//////////////////////////////////////

      $('#seguir2').click(function(event) {
        $('#datos_persona1,#datos_persona2,#datos_persona4').hide(1000).animate({height: "hide"}, 1000,"linear");
        $('#datos_persona3').show(1000).animate({width: "show"}, 1000,"linear");
         
         
      });
      $('#atras1').click(function(event) {
        $('#datos_persona2,#datos_persona3,#datos_persona4').hide(1000).animate({height: "hide"}, 1000,"linear");
        $('#datos_persona1').show(1000).animate({width: "show"}, 1000,"linear");
         
         
      });
      $('#paso1').click(function(event) {
        $('#datos_persona2,#datos_persona3,#datos_persona4').hide(1000).animate({height: "hide"}, 1000,"linear");
        $('#datos_persona1').show(1000).animate({width: "show"}, 1000,"linear");
         
         
      });
      
//////////////////////////////Paso_3//////////////////////////////////////

      $('#seguir3').click(function(event) {
        $('#datos_persona3,#datos_persona2,#datos_persona1').hide(1000).animate({height: "hide"}, 1000,"linear");
        $('#datos_persona4').show(1000).animate({width: "show"}, 1000,"linear");
         
         
      });
      $('#atras2').click(function(event) {
        $('#datos_persona3,#datos_persona4,#datos_persona1').hide(1000).animate({height: "hide"}, 1000,"linear");
        $('#datos_persona2').show(1000).animate({width: "show"}, 1000,"linear");
         
         
      });
      $('#paso22').click(function(event) {
        $('#datos_persona3,#datos_persona4,#datos_persona1').hide(1000).animate({height: "hide"}, 1000,"linear");
        $('#datos_persona2').show(1000).animate({width: "show"}, 1000,"linear");
         
         
      });
      $('#paso11').click(function(event) {
        $('#datos_persona2,#datos_persona3,#datos_persona4').hide(1000).animate({height: "hide"}, 1000,"linear");
        $('#datos_persona1').show(1000).animate({width: "show"}, 1000,"linear");
         
         
      });
//////////////////////////////Paso_4//////////////////////////////////////
      $('#atras3').click(function(event) {
        $('#datos_persona4,#datos_persona2,#datos_persona1').hide(1000).animate({height: "hide"}, 1000,"linear");
        $('#datos_persona3').show(1000).animate({width: "show"}, 1000,"linear");
         
         
      });
      $('#paso333').click(function(event) {
        $('#datos_persona4,#datos_persona2,#datos_persona1').hide(1000).animate({height: "hide"}, 1000,"linear");
        $('#datos_persona3').show(1000).animate({width: "show"}, 1000,"linear");
         
         
      });
      $('#paso222').click(function(event) {
        $('#datos_persona3,#datos_persona4,#datos_persona1').hide(1000).animate({height: "hide"}, 1000,"linear");
        $('#datos_persona2').show(1000).animate({width: "show"}, 1000,"linear");
         
         
      });
      $('#paso111').click(function(event) {
        $('#datos_persona2,#datos_persona3,#datos_persona4').hide(1000).animate({height: "hide"}, 1000,"linear");
        $('#datos_persona1').show(1000).animate({width: "show"}, 1000,"linear");
         
         
      });

    /****************************Validacion de radio tecnologia********************************************/

      $('#prod_tecnologia_si').click(function(event) {
        $('#fields2,#codigo_arancelarios').hide(1000).animate({height: "hide"}, 1000,"linear");
        $('#tecno_especifique').show(1000).animate({width: "show"}, 1000,"linear");  
            
      });

       $('#prod_tecnologia_no').click(function(event) {
        $('#tecno_especifique').hide(1000).animate({height: "hide"}, 1000,"linear");
        $('#fields2,#codigo_arancelarios').show(1000).animate({width: "show"}, 1000,"linear");   
      });


});   

/*************************Script de los codigos arancelarios *******************************************************************/

  $('input:radio[name="tipo_arancel"]').change( function(){

  


     if($(this).is(':checked')) {

       var parametros = {
               'id': $(this).val()

       };
       var token= $('input[name="_token"]').val();
       console.log(token);
       $('#id_arancel').val(parametros.id);
      $('#cod_arancel').prop('disabled',false);
       $.ajax({
           type: 'GET',
           url: '/Arancel',
           data: {'arancel': $(this).val(),'_token': token},
           success: function(data){

              $table='<table class="table table-bordered table-hover" id="tabla"><tr><th>Código Arancelario</th><th>Descripción</th></tr>';

              $('#arancel').html($table);
              $.each(data,function(index,value){

             $('#tabla tr:last').after('<tr class="data"><td>'+value.codigo+'</td><td>'+value.descripcion+'</td></tr></table>');

              });

           }

       });

    }
});

$('#cod_arancel').show('modal');

/**------------------------------------------------------------------------**/
 var global_id=localStorage.setItem('arancel'," ");


 $(document).on('click','#cod_arancel',function(){

   var id=$(this).parents("tr").attr('id');
   localStorage.setItem('arancel',id);

});

 $(document).on('click','tr.data',function(){

  var id=localStorage.getItem('arancel');
  var cod=$(this).find("td").eq(0).text();
  var ara=$(this).find("td").eq(1).text();

  $("tr#"+id).find("td").eq(0).children("input").val(cod);
  $("tr#"+id).find("td").eq(1).children("input").val(ara);

  $('#modal button').click();
  $('#buscador_arancel').val("");

  });

 /*****************Script Buscador del Codigo Arancelario******************************************/

 $(document).on('keyup','#buscador_arancel',function(){

 var tipo=$('input[name="tipo_arancel"]').val();
   
   $.ajax({

      type:'get',
      url: '/Arancel',
      data: {'valor': $(this).val(), 'tipoAran':tipo},
      success: function(data){

        $table='<table class="table table-bordered table-hover" id="tabla"><tr><th>Código Arancelario</th><th>Descripción</th></tr>';

        $('#arancel').html($table);
        if(data.length==0){

         $('#tabla tr:last').after('<tr><td colspan="2" style="color:red;text-align:center" >Códigos no existen</td></tr>');

       }else{
        $.each(data,function(index,value){

       $('#tabla tr:last').after('<tr class="data"><td>'+value.codigo+'</td><td>'+value.descripcion+'</td><tr></table');

        });

      }
      }

   });

});

</script>
@stop
