INSERT INTO `gen_status` (`id`, `nombre_status`, `descripcion_status`, `bactivo`, `created_at`, `updated_at`) VALUES (NULL, 'Atendida por Resguardo', 'Atendida por Resguardo', '1', '2022-02-18 00:00:00', '2022-02-18 00:00:00');

INSERT INTO `gen_status` (`id`, `nombre_status`, `descripcion_status`, `bactivo`, `created_at`, `updated_at`) VALUES (NULL, 'Atendida por CNA', 'Atendida por CNA', '1', '2022-02-18 00:00:00', '2022-02-18 00:00:00');

INSERT INTO `gen_status` (`id`, `nombre_status`, `descripcion_status`, `bactivo`, `created_at`, `updated_at`) VALUES (NULL, 'Aprobada por Resguardo', 'Aprobada por Resguardo', '1', '2022-02-18 00:00:00', '2022-02-18 00:00:00');

INSERT INTO `gen_status` (`id`, `nombre_status`, `descripcion_status`, `bactivo`, `created_at`, `updated_at`) VALUES (NULL, 'Aprobada por CNA', 'Aprobada por CNA', '1', '2022-02-18 00:00:00', '2022-02-18 00:00:00');

INSERT INTO `gen_status` (`id`, `nombre_status`, `descripcion_status`, `bactivo`, `created_at`, `updated_at`) VALUES (NULL, 'Negada por Resguardo', 'Negada por Resguardo', '1', '2022-02-18 00:00:00', '2022-02-18 00:00:00');

INSERT INTO `gen_status` (`id`, `nombre_status`, `descripcion_status`, `bactivo`, `created_at`, `updated_at`) VALUES (NULL, 'Negado por CNA', 'Negado por CNA', '1', '2022-02-18 00:00:00', '2022-02-18 00:00:00');
