<?php

namespace App\Http\Controllers;
use App\Models\FacturaSolicitud;
use App\Models\NotaCreditoSolicitud;
use Illuminate\Http\Request;
use App\Models\GenFactura;
use App\Models\GenSolicitud;
use App\Models\GenUnidadMedida;
use App\Models\GenDua;
use App\Models\GenNotaCredito;
use App\Models\TipoSolicitud;
use App\Http\Requests\GenSolicitudRequest;
use App\Models\GenConsignatario;
use App\Models\ConsigTecno;
use App\Models\DetUsuario;
use App\Models\DetProdFactura;
use App\Models\DetProdTecnologia;
use App\Http\Requests\FacturaSolicitudRequest;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use Auth;
use Laracasts\Flash\Flash;
use Alert;

class FacturaSolicitudController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $gen_factura=GenFactura::select('monto_fob')->where('id',$request->gen_factura_id)->first();
        //dd($gen_factura);
        $Monto_cred=GenNotaCredito::where('gen_factura_id',$request->gen_factura_id)->sum('monto_nota_credito');
         //dd($Monto_cred);
        //$nota=FinanciamientoSolicitud::where('gen_solicitud_id',$request->gen_solicitud_id)->first();
       /*if($gen_factura<$Monto_cred){
            return 2;

       }*/
       $monto=array(
                  'monto_fob'  => $gen_factura->monto_fob ? $gen_factura->monto_fob : 0,
                  'monto_nota_credito'  => $Monto_cred ? $Monto_cred : 0
                );
//dd($monto);
        return response()->json($monto);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        Session::forget('factura_er_id');
        $titulo="Ingrese Las Facturas Asociada a su Solicitud";
        $descripcion="Lista de Factura";
        $tiposolicitud=Session::get('tipo_solicitud_id');
        //$factsolicitud=GenFactura::where('gen_usuario_id',Auth::user()->id)->where('tipo_solicitud_id',$tiposolicitud)->where('bactivo',1)->pluck('numero_factura','id');
        $factsolicitud=GenFactura::where('gen_usuario_id',Auth::user()->id)->where('bactivo',1)->pluck('numero_factura','id');
       // $id_duas=GenFactura::where('gen_usuario_id',Auth::user()->id)->where('tipo_solicitud_id',$tiposolicitud)->where('bactivo',1)->get();
        $id_duas=GenFactura::where('gen_usuario_id',Auth::user()->id)->where('bactivo',1)->get();
        if($tiposolicitud==1){ 

            foreach ($id_duas as $value) {
               $duas[$value->id]=$value->gen_dua_id;
            }

        }else{

          $duas=[];
        }
        $fecha=date('Y-m-d');
        $consig=GenConsignatario::where('gen_usuario_id',Auth::user()->id)->pluck('nombre_consignatario','id');
        $tipoExpor=DetUsuario::where('gen_usuario_id',Auth::user()->id)->first();
        ($tipoExpor->produc_tecno==1) ? $num=2 : $num=1;
        $tiposolic=TipoSolicitud::whereIn('id',[$num])->pluck('solicitud','id');
          return view('FacturaSolicitud.create',compact('titulo','descripcion','factsolicitud','duas','tiposolic','fecha','consig','num'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(FacturaSolicitudRequest $request)
    {
    ///////////////////Crea la solicitud/////////////////////////////
    function generarRandom($longitud) {
      $key = '';
      $pattern = '1234567890abcdefghijklmnopqrstuvwxyz';
      $max = strlen($pattern)-1;
      for($i=0;$i < $longitud;$i++) $key .= $pattern{mt_rand(0,$max)};
      return $key;
  }
     $user=Auth::user()->id;
     $estatus=3;
     $fecha=date('Y-m-d');
     $random=generarRandom(4);
     $firma=$fecha.$user.$random;
     $firma_seguridad=md5($firma);
     $solicitud = new GenSolicitud;
     $solicitud->tipo_solicitud_id=$request->tipo_solicitud_id;
     $solicitud->gen_usuario_id=$user;
     $solicitud->gen_status_id=$estatus;
     $solicitud->fstatus=$fecha;
     $solicitud->fsolicitud=$fecha;
     $solicitud->opcion_tecnologia=$request->opcion_tecnologia;
     $solicitud->cfirma_seguridad=$firma_seguridad;
     $solicitud->save();
     //consultamos mi ultima solictud registrada
     $ult_solicitud=GenSolicitud::where('gen_usuario_id',Auth::user()->id)->where('gen_status_id',3)->orderby('created_at','DESC')->take(1)->first();
     //dd($request->all());
     $consi_tecno=new ConsigTecno;
     $consi_tecno->gen_tipo_solicitud=$ult_solicitud->id;
     $consi_tecno->gen_id_consignatario=$request->consig_tecno;
     $consi_tecno->bactivo=1;
     $consi_tecno->save();
     //dd($ult_solicitud->id);
     //creamos variable de sesion con mi ultima solicitud registrada
     Session::put('gen_solictud_id',$ult_solicitud->id);
     //Creamos variables de session con el tipo de solicitud de mi ultima solicitud registrada///
     Session::put('tipo_solicitud_id',$ult_solicitud->tipo_solicitud_id);
  /////////////////////////////////////////////////////////////////

    ////////////////////////Se guarda las facturas asociadas/////////////////////////////////////////
    /*****************************************************************************************/
    //dd($request->gen_nota_credito_id);
    // dd(session()->all());

    //*****No se le hizo explode a FacturaSolicitud porque NO se usa en el flujo de proceso*********//
    $soli=Session::get('tipo_solicitud_id');
    $id_solicitud=Session::get('gen_solictud_id');
    $cont=count($request->gen_factura_id);
         for($i=0;$i<=($cont-1);$i++){
            $Fact_solicitud=new FacturaSolicitud;
            $Fact_solicitud->gen_factura_id=$request->gen_factura_id[$i];
            $Fact_solicitud->gen_solicitud_id=$id_solicitud;
            $Fact_solicitud->numero_nota_credito=$request->numero_nota_credito[$i];
            $Fact_solicitud->save();     
       }

///////////////////////////////////////////////////////////////////////////
   $contador=count($request->gen_nota_credito_id);

 //****************Se realizo un Explode solo a NotaCreditoSolicitud******// 

   //dd($request->gen_nota_credito_id);
    for($i=0;$i<=($contador-1);$i++){
           
        if($request->gen_nota_credito_id[$i] != 0){
            $explode=explode(",",$request->gen_nota_credito_id[$i]);//queda como $i???
            
            //dd al ($explode); para ver el array por separado que envio
            
            for ($j=0;$j<=count($explode)-1;$j++) { 

                //dump($explode[$i]);
                $Fact_soli=new NotaCreditoSolicitud;
                $Fact_soli->gen_nota_credito_id=$explode[$j];//le hice explode Paso valor 
                $Fact_soli->gen_solicitud_id=$id_solicitud;
                $Fact_soli->save();   
            }
        }
   }
/////////////////////Se calclula el monto fob para la solicitud//////////////////////////////
$totalfob=0;
$cont=count($request->gen_factura_id);
     for($i=0;$i<=($cont-1);$i++){
      $id_factura=$request->gen_factura_id[$i];
      $gen_factura = DB::select('select * from gen_factura where id = ?', [$request->gen_factura_id[$i]]); 
      GenFactura::where('id',$request->gen_factura_id[$i])->update(['bactivo'=>'0']); 
      if (!empty($gen_factura)) {
        $monto_fob = $gen_factura[0]->monto_fob;
        $totalfob=$totalfob+$monto_fob ;
    }
  }
  GenSolicitud::where('id',Session::get('gen_solictud_id'))->update(['monto_solicitud'=>$totalfob,'monto_cif'=>$totalfob]);
////////////////////////////////////////////////////

       if($soli==1) {
        return redirect()->action('FinancSolicitudController@create');
      }else{
       // return redirect()->action('DetProdSolicitudController@create');
       return redirect()->action('FinancSolicitudController@create');
      }
}
    /**
     * Display the specified resource.
     *
     * @param  \App\Models\FacturaSolicitud  $facturaSolicitud
     * @return \Illuminate\Http\Response
     */
    public function show(FacturaSolicitud $facturaSolicitud)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\FacturaSolicitud  $facturaSolicitud
     * @return \Illuminate\Http\Response
     */
    public function deleteEr(Request $request)
    {
      $id=$request->id;
      //echo "se elimina";
      //select * from exportaciones.gen_solicitud WHERE id ="20187"; 
      
      $FacturaSol=FacturaSolicitud::where('gen_solicitud_id',$id)->get();
      foreach ($FacturaSol as $facturaSolicitud) {
        DB::table('gen_factura')->where('id', $facturaSolicitud->gen_solicitud_id)->update(['bactivo' => 1]);     
      }
      DB::table('factura_solicitud')->where('gen_solicitud_id', $id)->delete();
      DB::table('bandeja_asignaciones_intranet')->where('gen_solicitud_id', $id)->delete();
      DB::table('bandeja_analista_intranet')->where('gen_solicitud_id', $id)->delete();
      DB::table('bandeja_coordinador_intranet')->where('gen_solicitud_id', $id)->delete();
      $dvd= DB::table('gen_dvd_solicitud')->where('gen_solicitud_id', $id)->get();
      foreach ($dvd as $dvd) {
        DB::table('documentos_dvd')->where('gen_dvd_solicitud_id', $dvd->id)->delete();
        DB::table('er_recepcion_oca')->where('gen_dvd_solicitud_id', $dvd->id)->delete();
        
      }
      DB::table('gen_dvd_solicitud')->where('gen_solicitud_id', $id)->delete();
      DB::table('consig_tecno')->where('gen_tipo_solicitud', $id)->delete();
      DB::table('det_producto_solicitud')->where('gen_solicitud_id', $id)->delete();
      DB::table('financiamiento_solicitud')->where('gen_solicitud_id', $id)->delete();
      DB::table('NotaCreditoSolicitud')->where('gen_solicitud_id', $id)->delete();
      DB::table('gen_solicitud')->where('id', $id)->delete();

      $titulo="Solicitudes de Exportación Realizada";
      $descripcion="Solicitudes Registradas";
      $solicitudes=GenSolicitud::where('gen_usuario_id',Auth::user()->id)->where('bactivo',1)->get();

             
      return 1;
      //return view('SolicitudER.index',compact('solicitudes','titulo','descripcion'));
    } 
    public function edit(FacturaSolicitud $facturaSolicitud, $id)
    {

     ////////////////////////////////////
          $solicitud_er=GenSolicitud::find($id);
          $ses=Session::put('gen_solicitud_id',$solicitud_er->id);
          $obtener_sol=Session::get('gen_solicitud_id');
          $solicitud=GenSolicitud::find($id);  
          $consig_id=ConsigTecno::where('gen_tipo_solicitud',$id)->first();
          $consig=GenConsignatario::where('id',$consig_id->gen_id_consignatario)->pluck('nombre_consignatario','id');
          $tipoExpor=DetUsuario::where('gen_usuario_id',Auth::user()->id)->first();
          ($tipoExpor->produc_tecno==1) ? $num=2 : $num=1;   
          $tiposolic=TipoSolicitud::whereIn('id',[$num])->pluck('solicitud','id');
      /////////////////////////////////
      /////////////////////////////////
      $array_elem=[];
      $id_fac_solic=[];
      $facturas=array();
      Session::forget('facturas_er_id');
      $titulo=" Editar Las Facturas Asociadas a su Solicitud";
      $descripcion="Actualizar Datos de las Facturas";
      // Id del tipo de exportacion.
      $tipo_solicitud=Session::get('tipo_solicitud_id');
      // Id de la solicitud er
      $solictud_er=Session::get('gen_solictud_id');
      $id_sol=FacturaSolicitud::where('gen_solicitud_id',$id)->first();  
      $solicitudes=GenFactura::where('gen_usuario_id',Auth::user()->id)->where('bactivo',1)->get(); 
        
     if(is_null($id_sol)){ 
         $status=0;
         $solicitudes=GenFactura::where('gen_usuario_id',Auth::user()->id)->where('bactivo',1)->get(); 
         $facturas=$solicitudes->pluck('numero_factura','id');
         
      }else{
          $status=1;
          
          $x=FacturaSolicitud::where('gen_solicitud_id',$id)->where('bactivo',1)->get();      
          
          foreach ($x as $key => $value) {
          $array_elem[$key]=$value->gen_factura_id;
      
          $id_fac_solic[$key]=$value->id;
          Session::put('idFacSoli',$id_fac_solic);
       }
      
      /**************    Consulta para saber si esa er ha sido finalizada   **************/ 
          $idSoli=GenSolicitud::find($solictud_er);
          (is_null(@$idSoli->monto_cif)) ? $activo=1 :$activo=0;

           $solicitudes=GenFactura::where('gen_usuario_id',Auth::user()->id)->where('bactivo',$activo)->whereIn('id',$array_elem)->get();
          
           $facturas=GenFactura::where('gen_usuario_id',Auth::user()->id)->where('bactivo',1)->pluck('numero_factura','id');
             if($activo==0) {
                 foreach ($solicitudes as $key => $value) {
                   $facturas[$value->id]=$value->numero_factura;
                 }         
             }
        

     /*******************************************************************************/
     
    }
    
    
    $duas=array();
    $id_duas=GenFactura::where('gen_usuario_id',Auth::user()->id)->where('tipo_solicitud_id',$tipo_solicitud)->where('bactivo',1)->get();
    if($tipo_solicitud==1) { 
       if($id_duas!=null) { 
                   foreach ($id_duas as $value) {
                      $duas[$value->id]=$value->gen_dua_id;     
                      } 
                   }else{
                   $duas=[];
                 } 
                 if(!is_null($id_sol)) {
                      foreach ($solicitudes as $value) {
                        $duas[$value->id]=$value->gen_dua_id;
                      } 
                    }
             }else{
             $duas=[];
          }
   ///////////////////////////////////

  ///////////////////////////////////
 //dd(session()->all());

$data=FacturaSolicitud::select('gen_factura_id')->where('gen_solicitud_id',Session::get('gen_solicitud_id'))->get();
$monto=0;$montoCif=0; $id_solicitud=[];
  foreach ($data as $value) {
      $id_solicitud[]=$value->gen_factura_id;
    }
    //hacerle un array unico  $id_solicitud[] luego pasarselo a todas las demas consultas
    $id_solicitud=array_unique($id_solicitud);
Session::put('facturasId',$id_solicitud);
$cons_factura=GenFactura::whereIn('id',$id_solicitud)->get();
foreach ($cons_factura as $cif) {
    /*Se le agrego la resta del monto fob*/
    $montoCif=$montoCif+$cif->monto_total-$cif->monto_fob;
  }

 Session::put('monto_cif',$montoCif);
 if (isset($data->first()->gen_factura_id)) {
  $tipo_solicitud=GenFactura::find($data->first()->gen_factura_id);
} 
 

if (isset($tipo_solicitud->tipo_solicitud_id)) {
  if($tipo_solicitud->tipo_solicitud_id==1) {
        @$productos=DetProdFactura::whereIn('gen_factura_id',$id_solicitud)->get();
        foreach ($productos as $montoTotal) {
            $monto=$monto+$montoTotal->monto_total;
          }
        Session::put('monto_total_facturas',$monto);

     }else{
        @$productos=DetProdTecnologia::whereIn('gen_factura_id',$id_solicitud)->get();
        foreach ($productos as $montoTotal) {
            $monto=$monto+$montoTotal->valor_fob;
          }
        Session::put('monto_total_facturas',$monto);
  }
}

  ///////////////////////////////////

  
      return view('FacturaSolicitud.edit',compact('titulo','descripcion','facturas','id','solicitudes','status','duas','solicitud','tiposolic','consig','num' ));

  }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\FacturaSolicitud  $facturaSolicitud
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FacturaSolicitud $facturaSolicitud,$id)
    {
     
     ///////////////////////////////////////////////////////////////////////////////
     GenSolicitud::find($id)->update($request->all());
     $solicitud_er=GenSolicitud::find($id);
     ///////////////////////////////////////////////////////////////////////////////



    /******************* Eliminar factura que haya sido reemplazada por otra **************/
     $z=$request->gen_factura_id;
     //dd($request->numero_nota_credito);
     $consulta=FacturaSolicitud::where('gen_solicitud_id',$id)->first();     
     if(!is_null($consulta)) { 
     $x=FacturaSolicitud::where('gen_solicitud_id',$id)->get();
          foreach ($x as $key => $value) {
             $data[$key]=$value->gen_factura_id;
             DB::table('gen_factura')->where('id', $value->gen_factura_id)->update(['bactivo' => 1]);
          }
       $result=array_diff($data, $z);
       $vstring = implode('-', $result);
       $varray=explode('-',$vstring);
      //dd($varray);
      if (count($varray) > 0) {
       if($varray!=null){
     
           for ($i=0; $i < count($varray); $i++) { 

             //$monto_solicitud = DB::table('gen_factura')->where('id', $varray[$i])->first();
             //DB::table('gen_solicitud')->where('id', $id)->decrement('monto_solicitud', $monto_solicitud->monto_fob);
             //GenFactura::where('id',$varray[$i])->update(['bactivo'=>'1']);

              #Elimino la factura de la tabla FacturaSolicitud
              $eli=FacturaSolicitud::where('gen_solicitud_id',$id)->where('gen_factura_id',$varray[$i])->delete();
              $gen_factura = DB::select('select * from gen_factura where id = ?', [$varray[$i]]); 
           
             // $gen_factura = DB::select('select * from gen_factura where id = ?', [$varray[$i]]);
             // if (count($gen_factura) > 0) {

               //   $monto_fob = $gen_factura[0]->monto_fob;
                 // DB::table('gen_solicitud')->where('id', '=', $id)->decrement('monto_solicitud', @$totalfob);
              //} 
              #Actualizo el bactivo de esa factura a 1
              
           }    
       }
      } 
      
    }

      /**********************************************************************************/  
    $solictud_er=Session::get('gen_solictud_id');
    $z = ""; // Define la variable con un valor inicial vacío
    $z=$request->gen_factura_id;
    
    $c=[];
    $almacen_id=[];
    $data=[];

   if(is_array($z)) {
    for ($i=0; $i < count($z) ; $i++) { 

    $x=FacturaSolicitud::where('gen_solicitud_id',$id)->where('gen_factura_id',$z[$i])->where('bactivo',1)->first();

          ($x==null) ? $c[$i]=0 : $c[$i]=$x->id;
                
     }
    } 
    if(is_array($z)) {
     $cont_solicitudes=count($request->gen_factura_id);
    }
      //dd($request->numero_nota_credito);
     //dd($c);
     if(is_array($z)) {
     for($i=0;$i<=($cont_solicitudes-1);$i++){
      //dd( $c[$i]);

    //dd($request->numero_nota_credito[$i]);

         
            $facsolicitudes=FacturaSolicitud::updateOrCreate(
            ['id'=>$c[$i]],
            [ 'gen_factura_id'=>$request->gen_factura_id[$i],
              'numero_nota_credito'=>$request->numero_nota_credito[$i],
              'gen_solicitud_id'=>$id
            ]);
          
          // DB::table('gen_solicitud')->where('id', '=', $id)->decrement('monto_solicitud', @$totalfob);
      
           //$monto_solicitud = DB::table('gen_factura')->where('id', $asoci->gen_factura_id)->first();
          // DB::table('gen_solicitud')->where('id', $id)->increment('monto_solicitud', $monto_solicitud->monto_fob);
          
           $montof=0;
           $asocis = FacturaSolicitud::where('gen_solicitud_id', $id)->where('bactivo', 1)->get();
           foreach ($asocis as $asoci) {
           $gen_factura_id = $asoci->gen_factura_id;
           $monto_solicitud = DB::table('gen_factura')->where('id', $gen_factura_id)->first();
           $montof=$montof+$monto_solicitud->monto_fob;
           DB::table('gen_factura')->where('id', $gen_factura_id)->update(['bactivo' => 0]);
           }
           DB::table('gen_solicitud')->where('id', $id)->update(['monto_solicitud' => $montof]);
           
 
       }

      }
///////////////////////////////////////////////////////////////////////////////////
$k=$request->gen_nota_credito_id;
    $n=[];
    if(is_array($k)) {
    for ($i=0; $i < count($k) ; $i++) { //itera lista Dinamica

      $explode=explode(",",$k[$i]);//Explode de $Ḳ [$i])
      //$request->k[$i]);

          
        for ($j=0;$j<=count($explode)-1;$j++) { 

           
          $nota=NotaCreditoSolicitud::where('gen_solicitud_id',$id)->where('gen_nota_credito_id',$explode[$j])->where('bactivo',1)->first();//Y en la consulta paso a explode [$i]
   
          ($nota==null) ? $n[$j]=0 : $n[$j]=$nota->id;

        }
             
    }

  }
  if(is_array($request->gen_nota_credito_id)) {
     $contador=count($request->gen_nota_credito_id);
    }
     //$id_solicitud=Session::get('gen_solictud_id');
     if (!empty($contador)) {
      for($i=0;$i<=($contador-1);$i++){

        if($request->gen_nota_credito_id[$i] !=0){

            $explode=explode(",",$request->gen_nota_credito_id[$i]);//Explode

            for ($j=0;$j<=count($explode)-1;$j++) { 

                //dump($explode[$j]);
              //updateOrCreate Si existe lo actualiza sino lo crea (inserta)
                $notasolicitudes=NotaCreditoSolicitud::updateOrCreate( 
                ['id'=>$n[$j]],
                [ 'gen_nota_credito_id'=>$explode[$j],//Paso valor
                  'gen_solicitud_id'=>$id //Aqui desde un principio No estaba insertando probar con $id_solicitud y definir 
                ]);
            }
        }
      }
    }
///////////////////////////////Aqui va//////////////////////////////////

/////////////////////////////////////////////////////////////////////////////////
       $soli=Session::get('tipo_solicitud_id');
       if($soli==2){ 
         // return redirect()->action('DetProdSolicitudController@create');
         return redirect("exportador/FinanciamientoSolicitud/$solictud_er/edit");

       }else{
       Alert::success('Actualización Exitosa!')->persistent("OK");
       return redirect("exportador/FinanciamientoSolicitud/$solictud_er/edit");

       }

    }

    public function eliminar($id)
    {

      
       if($_GET['id']!=0) { 

       $delete=FacturaSolicitud::where('gen_factura_id',$_GET['id'])->where('gen_solicitud_id',$_GET['id_sol'])->delete();

         
           if($delete) { 

            GenFactura::where('id',$_GET['id'])->update(['bactivo'=>1]);
            
            return 1; 

           }else {

         return 0;  }

      }else{

         @$x=FacturaSolicitud::where('gen_solicitud_id',$_GET['id_sol'])->where('bactivo',1)->get();
         return count(@$x);
      }
      
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\FacturaSolicitud  $facturaSolicitud
     * @return \Illuminate\Http\Response
     */
    public function destroy(FacturaSolicitud $facturaSolicitud)
    {
        //
    }
}
