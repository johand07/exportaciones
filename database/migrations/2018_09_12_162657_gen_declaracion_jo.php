<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GenDeclaracionJo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
    {
         Schema::create('gen_declaracion_jo', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('gen_usuario_id')->index()->nullable();
            $table->foreign('gen_usuario_id')->references('id')->on('gen_usuario');
            $table->unsignedInteger('gen_status_id')->index()->nullable();
            $table->foreign('gen_status_id')->references('id')->on('gen_status');
            $table->date('fstatus')->nullable();
            $table->string('num_solicitud',60)->nullable();
            $table->string('num_productos',60)->nullable();
            $table->string('num_paises',60)->nullable()->default(1);
            $table->boolean('bactivo')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         /*Schema::table('gen_declaracion_jo', function (Blueprint $table) {
            $table->dropforeingn(['gen_usuario_id']);
            $table->dropColumn('gen_usuario_id');
             $table->dropforeingn(['gen_status_id']);
            $table->dropColumn('gen_status_id');
         });*/
       Schema::dropIfExists('gen_declaracion_jo');
    }
}
