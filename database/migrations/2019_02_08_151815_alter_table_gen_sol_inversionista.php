<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableGenSolInversionista extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::table('gen_sol_inversionista', function (Blueprint $table) {
            //
            $table->string('observacion_anulacion')->nullable()->after('observacion_inversionista');
            $table->string('observacion_doc_incomp')->nullable()->after('observacion_anulacion');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('gen_sol_inversionista', function (Blueprint $table) {
            //
            $table->dropColumn('observacion_anulacion');
            
        });
    }
}
