<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ConsigTecno extends Model
{

    protected $table= 'consig_tecno';

    protected $fillable =[

     'id',
     'gen_tipo_solicitud',
     'gen_id_consignatario',
     'bactivo'


    ];

}
