<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableAnalisis extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('analisis_calificacion', function (Blueprint $table) {
            //
            $table->string('num_solicitud')->nullable()->after('gen_status_id');
            $table->string('observacion_analisis')->nullable()->after('num_solicitud');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('analisis_calificacion', function (Blueprint $table) {
            //
            $table->dropColumn('num_solicitud');
            
        });
    }
}
