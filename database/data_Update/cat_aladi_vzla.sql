/*
-- Query: SELECT * FROM exportaciones.cat_ald_tp
LIMIT 0, 500

-- Date: 2019-08-23 01:58
*/
INSERT INTO `cat_ald_tp` (`id`,`codigo`,`descripcion`,`bactivo`,`created_at`,`updated_at`) VALUES (1,'a','literal a)',1,NULL,NULL);
INSERT INTO `cat_ald_tp` (`id`,`codigo`,`descripcion`,`bactivo`,`created_at`,`updated_at`) VALUES (2,'b','literal b)',1,NULL,NULL);
INSERT INTO `cat_ald_tp` (`id`,`codigo`,`descripcion`,`bactivo`,`created_at`,`updated_at`) VALUES (3,'c','literal c)',1,NULL,NULL);
INSERT INTO `cat_ald_tp` (`id`,`codigo`,`descripcion`,`bactivo`,`created_at`,`updated_at`) VALUES (4,'d','literal d)',1,NULL,NULL);
INSERT INTO `cat_ald_tp` (`id`,`codigo`,`descripcion`,`bactivo`,`created_at`,`updated_at`) VALUES (5,'e','literal e)',1,NULL,NULL);
INSERT INTO `cat_ald_tp` (`id`,`codigo`,`descripcion`,`bactivo`,`created_at`,`updated_at`) VALUES (6,'2','Artículo 2°',1,NULL,NULL);
INSERT INTO `cat_ald_tp` (`id`,`codigo`,`descripcion`,`bactivo`,`created_at`,`updated_at`) VALUES (7,'n','No califica Origen Venezolano para ese Acuerdo o País',1,NULL,NULL);
INSERT INTO `cat_ald_tp` (`id`,`codigo`,`descripcion`,`bactivo`,`created_at`,`updated_at`) VALUES (8,'*','Información insuficiente para la determinación del Origen',1,NULL,NULL);
INSERT INTO `cat_ald_tp` (`id`,`codigo`,`descripcion`,`bactivo`,`created_at`,`updated_at`) VALUES (9,'(1)','no',1,NULL,NULL);
INSERT INTO `cat_ald_tp` (`id`,`codigo`,`descripcion`,`bactivo`,`created_at`,`updated_at`) VALUES (10,'(2)','no ',1,NULL,NULL);
INSERT INTO `cat_ald_tp` (`id`,`codigo`,`descripcion`,`bactivo`,`created_at`,`updated_at`) VALUES (11,'(3)','no',1,NULL,NULL);
