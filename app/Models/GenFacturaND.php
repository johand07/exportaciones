<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GenFacturaND extends Model
{
   protected $table = 'gen_factura_nd';

   protected $fillable =[

     'id',
     'gen_factura_id',
     'nota_debito_id  ',
     'bactivo',
    ];

   protected $date =['created_at','updated_at']; 


  public function factura(){
     
     return $this->hasOne('App\Models\GenFactura','id','gen_factura_id');
  }


  public function notadebito(){
     
     return $this->hasOne('App\Models\NotaDebito','id','nota_debito_id');
  }
}
