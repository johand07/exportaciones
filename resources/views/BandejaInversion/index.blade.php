@extends('templates/layoutlte')
@section('content')
<div class="panels">
  <div class="row">
  <div class="col-md-12">
    <div class="row">
      <div class="col-md-3 text-center">
        <a class="btn btn-primary" href="{{url('/exportador/BandejaInversion/create')}}">
          <span class="glyphicon glyphicon-file" title="Agregar" aria-hidden="true"></span>
           Registro de Declaración de Inversión
        </a>
      </div>
      <div class="col-md-9"></div>
    </div>
    <br><br>
    <table id="listaDeclaracionInversion" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>

                <th>#</th>
                <th>Nro. Solicitud</th>
                <th>Rif</th>
                <th>Razon Social</th>
                <th>Estatus</th>
                <th>Detalle estatus</th>
                <th>Fecha status</th>
                <th>Fecha Solicitud</th>
                <th>Acciones</th>
            </tr>
        </thead>
        <tbody>
          @if(isset($declaracionesInversion))
            @foreach($declaracionesInversion as $key =>  $decInversion)
              <tr>
                <td>
                  {{$key+1}}
                </td>
                <td>
                  {{$decInversion->num_declaracion}}
                </td>
                <td>
                  {{$decInversion->genUsuario->detUsuario->rif}}
                </td>
                <td>
                  {{$decInversion->genUsuario->detUsuario->razon_social}}
                </td>
                <td>
                  @if($decInversion->gen_status_id == 16)
                    Inversión Declarada
                  @else
                    {{$decInversion->genStatus->nombre_status}}
                  @endif
                </td>
                <td>
                  @if((isset($decInversion->rPlanillaDjir01) && $decInversion->rPlanillaDjir01->gen_status_id == 9)&& (isset($decInversion->rPlanillaDjir02) && $decInversion->rPlanillaDjir02->gen_status_id == 9 )&& (isset($decInversion->rPlanillaDjir03) && $decInversion->rPlanillaDjir03->gen_status_id == 9))
                      Solicitud Completada y enviada, en espera de analisis!
                  @elseif(isset($decInversion->rPlanillaDjir01) && !isset($decInversion->rPlanillaDjir02) && !isset($decInversion->rPlanillaDjir03))
                    Declaracion Incompleta, Planilla Djir02 y Djir03 incompleta!
                  @elseif(isset($decInversion->rPlanillaDjir02) && isset($decInversion->rPlanillaDjir01) && !isset($decInversion->rPlanillaDjir03))
                    Declaracion Incompleta, Planilla Djir03 incompleta!
                  @elseif(!isset($decInversion->rPlanillaDjir01) && !isset($decInversion->rPlanillaDjir02) && !isset($decInversion->rPlanillaDjir03))
                    Declaracion totalmente Incompleta, Debe completar todas las planillas !
                  @elseif((isset($decInversion->rPlanillaDjir01) && $decInversion->rPlanillaDjir01->estado_observacion == 1 && ($decInversion->rPlanillaDjir01->gen_status_id == 11 || $decInversion->rPlanillaDjir01->gen_status_id == 15)) || (isset($decInversion->rPlanillaDjir02) && $decInversion->rPlanillaDjir02->estado_observacion == 1 && ($decInversion->rPlanillaDjir02->gen_status_id == 11 || $decInversion->rPlanillaDjir02->gen_status_id == 15) || (isset($decInversion->rPlanillaDjir03) && $decInversion->rPlanillaDjir03->estado_observacion == 1 && ($decInversion->rPlanillaDjir03->gen_status_id == 11 || $decInversion->rPlanillaDjir03->gen_status_id == 15))))
                      <b>PLanilla Djir01:</b> {{$decInversion->rPlanillaDjir01->descripcion_observacion ? 'Con observaciones' : 'Sin observaciones'}}
                      <br>
                      <b>PLanilla Djir02:</b> {{$decInversion->rPlanillaDjir02->descrip_observacion ? 'Con observaciones' : 'Sin observaciones'}}
                      <br>
                      <b>PLanilla Djir03:</b> {{$decInversion->rPlanillaDjir03->descrip_observacion ? 'Con observaciones' : 'Sin observaciones'}}
                  @endif
                </td>
                <td>
                  {{$decInversion->fstatus}}
                </td>
                <td>
                  {{$decInversion->created_at}}
                </td>
                <td>
                  @if(isset($decInversion->rPlanillaDjir01) && isset($decInversion->rPlanillaDjir02) && isset($decInversion->rPlanillaDjir03))
                      
                  @elseif(isset($decInversion->rPlanillaDjir01) && !isset($decInversion->rPlanillaDjir02) && !isset($decInversion->rPlanillaDjir03))
                    <a href="{{url('exportador/FormDeclaracionInversion/')}}/{{$decInversion->id}}" class="btn btn-warning"> Completar </a>
                  @elseif(isset($decInversion->rPlanillaDjir02) && isset($decInversion->rPlanillaDjir01) && !isset($decInversion->rPlanillaDjir03))
                    <a href="{{url('exportador/FormDeclaracionInversion/')}}/{{$decInversion->id}}" class="btn btn-warning"> Completar </a>
                  @elseif(!isset($decInversion->rPlanillaDjir01) && !isset($decInversion->rPlanillaDjir02) && !isset($decInversion->rPlanillaDjir03))
                    <a href="{{url('exportador/FormDeclaracionInversion/')}}/{{$decInversion->id}}" class="btn btn-warning"> Completar </a>
                  @endif

                  @if(isset($decInversion->rPlanillaDjir01) && isset($decInversion->rPlanillaDjir02) && isset($decInversion->rPlanillaDjir03) && $decInversion->rPlanillaDjir01->gen_status_id != 17 && $decInversion->rPlanillaDjir02->gen_status_id != 17 && $decInversion->rPlanillaDjir03->gen_status_id != 17)

                    <a href="{{action('PdfController@DeclaracionJuradaInversionRealizadaP01',array('id'=>$decInversion->rPlanillaDjir01->gen_declaracion_inversion_id))}}" class="glyphicon glyphicon-file btn btn-default btn-sm" target="_blank" title="Declaracion Jurada Inversion Realizada 01"title Declaracion Jurada Inversion Realizada 01" aria-hidden="true"></a>

                  <a href="{{action('PdfController@DeclaracionJuradaInversionRealizadaP02',array('id'=>$decInversion->rPlanillaDjir02->gen_declaracion_inversion_id))}}" class="glyphicon glyphicon-file btn btn-default btn-sm" target="_blank" title="Declaracion Jurada Inversion Realizada 02" aria-hidden="true"></a>

                    <a href="{{action('PdfController@DeclaracionJuradaInversionRealizadaP03',array('id'=>$decInversion->rPlanillaDjir03->gen_declaracion_inversion_id))}}" class="glyphicon glyphicon-file btn btn-default btn-sm" target="_blank" title="Declaracion Jurada Inversion Realizada 03" aria-hidden="true"></a>

                  @endif

                  @if(isset($decInversion->rPlanillaDjir01) && isset($decInversion->rPlanillaDjir02) && isset($decInversion->rPlanillaDjir03))
                    @if($decInversion->rPlanillaDjir01->gen_status_id == 11 || $decInversion->rPlanillaDjir02->gen_status_id == 11 || $decInversion->rPlanillaDjir03->gen_status_id == 11)
                      <a href="{{url('exportador/FormDeclaracionInversion/')}}/{{$decInversion->id}}" class="btn btn-primary"> Corregir </a>
                    @endif
                  @endif

                  @if(isset($decInversion->rPlanillaDjir01) && isset($decInversion->rPlanillaDjir02) && isset($decInversion->rPlanillaDjir03))
                    @if($decInversion->rPlanillaDjir01->gen_status_id == 12 && $decInversion->rPlanillaDjir02->gen_status_id == 12 && $decInversion->rPlanillaDjir03->gen_status_id == 12)
                      Aprobado
                  @endif
                  @endif
                  @if(isset($decInversion->rPlanillaDjir01) && isset($decInversion->rPlanillaDjir02) && isset($decInversion->rPlanillaDjir03))
                    @if($decInversion->rPlanillaDjir01->gen_status_id == 15 || $decInversion->rPlanillaDjir02->gen_status_id == 15 || $decInversion->rPlanillaDjir03->gen_status_id == 15)
                        <a href="{{url('exportador/FormDeclaracionInversion/')}}/{{$decInversion->id}}" class="btn btn-primary"> Doc. imcompletos </a>
                    @endif
                  @endif

                  @if(isset($decInversion->rPlanillaDjir01) && isset($decInversion->rPlanillaDjir02) && isset($decInversion->rPlanillaDjir03))
                    @if($decInversion->rPlanillaDjir01->gen_status_id == 16 && $decInversion->rPlanillaDjir02->gen_status_id == 16 && $decInversion->rPlanillaDjir03->gen_status_id == 16)
                      <a href="{{action('PdfController@CertificadoInversionRealizada',array('id'=>$decInversion->id))}}" class="glyphicon glyphicon-file btn btn-success btn-sm" target="_blank" title="Certificado Inversionista" aria-hidden="true">Certificado</a>
                      <!--a href="{{url('exportador/ActualizarDeclaracion/')}}/{{$decInversion->id}}" class="glyphicon glyphicon-file btn btn-primary btn-sm" title="Certificado Inversionista" aria-hidden="true">Actualizar</a-->
                    @endif
                  @endif
                </td>
              </tr>
            @endforeach
          @endif
        </tbody>
    </table>
  </div>
</div>

@stop
