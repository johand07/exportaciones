@extends('templates/layoutlte')

@section('content')

{{ Form::model($certificado, array('route' => array('CertificadoOrigen.update', $certificado->id), 'method' => 'PUT', 'id'=>'FormProd', 'enctype' => 'multipart/form-data')) }}  
<div class="content">

@php
$observacion = isset($certificado->observacion_analista)? $certificado->observacion_analista : $certificado->observacion_coordinador;

function namefile($name){
  $explode=explode("/",$name);
  return $explode[3];
  
}

@endphp

@if( $certificado->gen_status_id == 11 && (!empty($certificado->observacion_analista ) || !empty($certificado->observacion_coordinador ) ) )
@php $contador_prod=count($det_prod_cer); $contador_criterio=count($det_decla_cert); @endphp
{{Form::hidden('contador',$contador_prod)}}
{{Form::hidden('contador_criterio',$contador_criterio)}}           
			<script>

              document.onreadystatechange = function () {
                var state = document.readyState;
                if (state == 'complete') {
                  swal("Estimado usuario debera corregír las siguientes observaciones", "{{$observacion}}", "warning");
                }
              }
            </script>
            <br>

			<div id="space_alert1" class="alert alert-danger" role="alert" style="display: block;">
				<strong><span class="glyphicon glyphicon-circle-arrow-right"></span> Observación Indicada por el Analista: {{$observacion}}</strong>
			  </div>

          @endif
		<div class="panel-group">
			
				<div class="panel-body">
					<div class="form-group">
						
						<input type="hidden" name="gen_tipo_certificado_id" id="gen_tipo_certificado_id" value="{{$certificado->gen_tipo_certificado_id}}">
					</div>


				</div>

		
			<!-----------------------------------Fin Select-------------------------------------------->


			<div class="panel-primary" style="background-color: #fff">
				<div class="panel-heading"><h3>Carga de Documentos </h3>
		
				</div>
				<div class="panel-body">
					
					<div class="row"><br>
						<div class="col-md-12">
							
							<div class="col-md-6">
								<tr>
								
									<td class="text-center" valign="top" > 
									<span><b><p style="font-size:14px;">1.) Cargar el formato de Solicitud de Certificado de Origen</p></b></span>
									
									<span class="btn btn-default btn-file" style="margin: 3px;">
										Cargar Archivo <span class="glyphicon glyphicon-file"></span>
										{!! Form::file('file_1', array('class' => 'file-input','style'=>'margin: 10px;','id'=>'file_1','title'=>'formatos permitidos .doc, .docx, .ppt, .pptx, .pdf')) !!}
									</span>
									<input type="hidden" value="{{$doc_cert->id}}" name="file_id">
									</td>
									<td class="text-center" height="40px"><img id="" src="" alt="" width="300px" style="margin: 3px;"><img id="imgdocumento_file_1" src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px; display: none;">
											<div id="nombre_file_1"></div><!--Craga la 1era vez el nombre del Documento-->

                                          
                    @if(isset($doc_cert->file_1))
                    	<div id="imgdocumentoEdit_file_1">
                     		 <img id="imgdocumentoEdit_file_1" src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px;">
                     		  {{namefile($doc_cert->file_1)}}<!--Carga Nombre del documento en el edit con namefile que esta arriba dentro de php en una funcion-->
                     		 </div>
                    @endif
                  </td>
								</tr>
							</div>
							<div class="col-md-6">
								<tr>
									<td class="text-center" valign="top" > 
									<span><b><p style="font-size:14px;">4.) Pago por concepto de solicitud de certificado de origen (0,25 petros)</p></b></span>
							
									<span class="btn btn-default btn-file" style="margin: 3px;">
										Cargar Archivo <span class="glyphicon glyphicon-file"></span>
										{!! Form::file('file_4', array('class' => 'file-input','style'=>'margin: 10px;','id'=>'file_4','title'=>'formatos permitidos .doc, .docx, .ppt, .pptx, .pdf')) !!}
									</span>
									</td>
									<td class="text-center" height="40px"><img id="" src="" alt="" width="300px" style="margin: 3px;"><img id="imgdocumento_file_4" src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px; display: none;">
										<div id="nombre_file_4"></div><!--Craga la 1era vez el nombre del Documento-->

										
										@if(isset($doc_cert->file_4))
											 <div id="imgdocumentoEdit_file_4">
													<img id="imgdocumentoEdit_file_4" src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px;">
													{{namefile($doc_cert->file_4)}}
											</div>
										@endif
									</td>
								</tr>
							</div>
							
						</div>
						<br><br><br>

						<div class="col-md-12">	<br><br><br>
							<div class="col-md-6">
								<tr>
									<td class="text-center" valign="top" > 
									<span><b><p style="font-size:14px;">2.) Declaración jurada de Origen</p></b></span>
					
									<span class="btn btn-default btn-file" style="margin: 3px;">
										Cargar Archivo <span class="glyphicon glyphicon-file"></span>
										{!! Form::file('file_2', array('class' => 'file-input','style'=>'margin: 10px;','id'=>'file_2','title'=>'formatos permitidos .doc, .docx, .ppt, .pptx, .pdf')) !!}
									</span>
									</td>
									<td class="text-center" height="40px"><img id="" src="" alt="" width="300px" style="margin: 3px;"><img id="imgdocumento_file_2" src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px; display: none;">
										<div id="nombre_file_2"></div><!--Craga la 1era vez el nombre del Documento-->
										
										@if(isset($doc_cert->file_2))
											<div id="imgdocumentoEdit_file_2">
												<img id="imgdocumentoEdit_file_2" src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px;">
												{{namefile($doc_cert->file_2)}}
											</div>
										@endif
									</td>
								</tr>
							</div>
							<div class="col-md-6">
								<tr>
									<td class="text-center" valign="top" > 
									<span><b><p style="font-size:14px;">5.) Pago de 0,02 U.T Gobierno del Distrito Capital</p></b></span>
								
									<span class="btn btn-default btn-file" style="margin: 3px;">
										Cargar Archivo <span class="glyphicon glyphicon-file"></span>
										{!! Form::file('file_5', array('class' => 'file-input','style'=>'margin: 10px;','id'=>'file_5','title'=>'formatos permitidos .doc, .docx, .ppt, .pptx, .pdf')) !!}
									</span>
									</td>
									<td class="text-center" height="40px"><img id="" src="" alt="" width="300px" style="margin: 3px;"><img id="imgdocumento_file_5" src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px; display: none;">
											<div id="nombre_file_5"></div><!--Craga la 1era vez el nombre del Documento-->

										@if(isset($doc_cert->file_5))
											<div id="imgdocumentoEdit_file_5">
												<img src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px;">
												{{namefile($doc_cert->file_5)}}
											</div>
										@endif
									</td>
								</tr>
							</div>
							
							
						</div><br><br><br>


						<div class="col-md-12"><br><br><br>
							<div class="col-md-6">
								<tr>
									<td class="text-center" valign="top" > 
									<span><b><p>3.) Factura comercial de exportador</p></b></span>
								
									<span class="btn btn-default btn-file" style="margin: 3px;">
										Cargar Archivo <span class="glyphicon glyphicon-file"></span>
										{!! Form::file('file_3', array('class' => 'file-input','style'=>'margin: 10px;','id'=>'file_3','title'=>'formatos permitidos .doc, .docx, .ppt, .pptx, .pdf')) !!}
									</span>
									</td>
									<td class="text-center" height="40px"><img id="" src="" alt="" width="300px" style="margin: 3px;"><img id="imgdocumento_file_3" src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px; display: none;">
										<div id="nombre_file_3"></div><!--Craga la 1era vez el nombre del Documento-->
											
											@if(isset($doc_cert->file_3))
												<div id="imgdocumentoEdit_file_3">
													<img id="imgdocumentoEdit_file_3" src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px;">
													 {{namefile($doc_cert->file_3)}}<!--Carga Nombre del documento en el edit con namefile que esta arriba dentro de php en una funcion-->
												</div>
											@endif

									</td>
								</tr>
							</div>
							<div class="col-md-6">
								<tr>
									<td class="text-center" valign="top" > 
									<span><b><p style="font-size:14px;">6.) Factura de productor (Si es <b>Comercializadora</b>)</p></b></span>
									
									<span class="btn btn-default btn-file" style="margin: 3px;">
										Cargar Archivo <span class="glyphicon glyphicon-file"></span>
										{!! Form::file('file_6', array('class' => 'file-input','style'=>'margin: 10px;','id'=>'file_6','title'=>'formatos permitidos .doc, .docx, .ppt, .pptx, .pdf', "noreq"=>"noreq")) !!}
									</span>
									</td>
									<td class="text-center" height="40px"><img id="vista_previa_file_6" src="" alt="" width="300px" style="margin: 3px;"><img id="imgdocumento_file_6" src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px; display: none;">
										<div id="nombre_file_6"></div><!--Craga la 1era vez el nombre del Documento-->
										
										@if(isset($doc_cert->file_6))
											<div id="imgdocumentoEdit_file_6">
												<img src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px;">
													 {{namefile($doc_cert->file_6)}}<!--Carga Nombre del documento en el edit con namefile que esta arriba dentro de php en una funcion-->
										@endif
									</td>
								</tr>
							</div>
							
							
						</div>



					</div><!--row-->

						
				</div>
				

				<br><br>
				<div class="panel-group">
					<div class="panel-primary" style="background-color: #fff">
						<div class="panel-heading"><h3>Certificado de origen acuerdo de alcance parcial Venezuela - Perú</h3>
					
						</div>
						<div class="panel-body">
							<div id="pais_exp_imp">
								<div class="row"><br>
									<div class="col-md-6">
										<div class="form-group">
											
											{!! Form::label('','(1) País Exportador') !!}
											{!! Form::text('pais_exportador',$certificado->pais_exportador,['class'=>'form-control','id'=>'pais_exportador']) !!}
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											
											{!! Form::label('','(2) País Importador') !!}
											{!! Form::text('pais_importador',$certificado->pais_importador,['class'=>'form-control','id'=>'pais_importador']) !!}
										</div>
										
									</div>
								</div>
							</div>

							<div id="productos_planilla">
								<div class="row"><br><!--Agregar productos-->
									
									<table class="table table-bordered text-left" id="productos">
										<tr> <td><button  name="add" type="button" id="add-productos" value="add more" class="btn btn-success" >Agregar</button></td></tr>

									<tr>
											<th>(3)Nº. De Orden </th>
							
											<th>(4)Código arancelario</th>

											<th>(5)Descripción de las
											mercancías
											</th>

											<th>(6)Unidad Física según factura
											</th>

											<th>(7)Valor de cada<br>
												mercancía según<br>
												factura en US$
											</th>
				
						
									</tr>
										@foreach($det_prod_cer as $key => $prod_certifi)
											<tr id="row1{{$key+1}}"><!--row1 es para la lista dinamica de agregar productos nsantos-->
													<!--Lista dinamica Prodcutos-->
											
												<td>{!!Form::text('num_orden[]',$prod_certifi->num_orden,['class'=>'form-control',''=>"modal",'data-target'=>"",'id'=>'num_orden',''=>'true', "noreq"=>"noreq"])!!}
												</td>

												<td>{!!Form::text('codigo_arancel[]',$prod_certifi->codigo_arancel,['class'=>'form-control','id'=>'codigo_arancel', "noreq"=>"noreq"])!!}
												</td>

												<td>{!!Form::text('denominacion_mercancia[]',$prod_certifi->denominacion_mercancia,['class'=>'form-control','id'=>'denominacion_mercancia', "noreq"=>"noreq"])!!}
												</td>

												<td>{!!Form::text('unidad_fisica[]',$prod_certifi->unidad_fisica,['class'=>'form-control','id'=>'unidad_fisica', "noreq"=>"noreq"])!!}
												</td>

												<td>{!!Form::text('valor_fob[]',$prod_certifi->valor_fob,['class'=>'form-control','id'=>'valor_fob', "noreq"=>"noreq"])!!}
												</td>
											
												<td>{!!Form::hidden('id_productos[]',$prod_certifi->id)!!}<button  name="remove" type="button" id="{{$key+1}}" value="" class="btn btn-danger btn-remove">x</button></td>
												
											</tr>
										@endforeach
									</table>
								</div>
							</div>
							<div id="declaracion_origen">
								<div class="row">
									<center><h2>(8)Declaración de origen</h2></center>
									<div class="col-md-6">
										<div class="form-group">
											{!! Form::label('','Factura Comercial No.') !!}
											{!! Form::text('numero_factura',$det_decla_cert[0]->numero_factura,['class'=>'form-control','id'=>'numero_factura', "noreq"=>"noreq"]) !!}
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											{!! Form::label('','Fecha') !!}
											<div class="input-group date">
												{!! Form::text('f_fac_certificado',$det_decla_cert[0]->f_fac_certificado,['class'=>'form-control','readonly'=>'readonly','id'=>'f_fac_certificado', "noreq"=>"noreq"]) !!}
												<span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
											</div>
										</div>
									</div>
								</div>
							</div><br><br>

							<div id="criterios_normas">
								<div class="row"><br>
									
									<table class="table table-bordered text-left" id="criterios">
										<tr> <td><button  name="add" type="button" id="add-criterios" value="add more" class="btn btn-success" >Agregar</button></td></tr>

										<tr>
											<th>(9) Nº. De Orden </th>
										
											<th>(10) Criterio para la Calificación de Origen
											</th>

										
										</tr>
										@foreach($det_decla_cert as $key => $det_declaracion)
										<tr id="row2{{$key+1}}">
										
												<td>
												{!!Form::text('num_orden_declaracion[]',$det_declaracion->num_orden_declaracion,['class'=>'form-control','id'=>'num_orden_declaracion', "noreq"=>"noreq"])!!}
												<input type="hidden" value="{{$det_declaracion->id}}" name="det_decla_certif_id[]">
												</td>

												<td>{!!Form::text('normas_criterio[]',$det_declaracion->normas_criterio,['class'=>'form-control','id'=>'normas_criterio', "noreq"=>"noreq"])!!}
												</td>
												<td>{!!Form::hidden('id_criterios[]',$det_declaracion->id)!!}<button  name="remove" type="button" id="{{$key+1}}" value="" class="btn btn-danger btn-remove-criterio">x</button></td>
												
										</tr>
										
										@endforeach
									</table>
								</div>
							</div>
                           
							<div id="datos_productor">
								
							</div>
							<div class="row"><br>

								<div class="col-md-6">
									<h2>(11)Productor o exportador</h2>
									<div id="razon_social_exp">
										<div class="form-group">
													
											{!! Form::label('','11.1 Nombre o Razón Social') !!}
											{!! Form::text('razon_social',$det_usuario->razon_social,['class'=>'form-control','id'=>'razon_social', 'readonly'=>'readonly']) !!}
										</div>
									</div>
									<div id="identificacion_fiscal" style="">
										<div class="form-group">
									
											{!! Form::label('','11.2 Número de identificacion Fiscal') !!}
											{!! Form::text('rif',$det_usuario->rif,['class'=>'form-control','id'=>'rif', 'readonly'=>'readonly']) !!}
										</div>
									</div>
									<div id="direccion_exp">
										<div class="form-group">
											
											{!! Form::label('','11.3 Dirección') !!}
											{!! Form::text('direccion',$det_usuario->direccion,['class'=>'form-control','id'=>'direccion', 'readonly'=>'readonly']) !!}
										</div>
									</div>
										<div class="form-group">
											{!! Form::label('','11.4 E-mail') !!}
											{!! Form::text('correo',$det_usuario->correo,['class'=>'form-control','id'=>'correo', 'readonly'=>'readonly']) !!}
										</div>
									
										<div class="form-group">
											{!! Form::label('','11.5 Teléfono') !!}
											{!! Form::text('telefono_movil',$det_usuario->telefono_movil,['class'=>'form-control','id'=>'telefono_movil', 'readonly'=>'readonly']) !!}
										</div>
								
									<div id="fecha_exp">
										<div class="form-group">
											{!! Form::label('','Fecha de Emision') !!}
											<div class="input-group date">		
												{!! Form::text('fecha_emision_exportador',null,['class'=>'form-control','readonly'=>'readonly','id'=>'fecha_emision_exportador', "noreq"=>"noreq"])!!}<span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
											</div>
										</div>
									</div>

								</div>
								<div class="col-md-6">
									<h2>(13) Importador</h2>
									<div id="nombre_direcc_impo">
										<div class="form-group">
											
											{!! Form::label('','13.1 Nombre o Razón Social') !!}
											{!! Form::text('razon_social_importador',$import_cert->razon_social_importador,['class'=>'form-control','id'=>'razon_social_importador', "noreq"=>"noreq"]) !!}
										</div>
										<div class="form-group">
											
											{!! Form::label('','13.2 Dirección') !!}
											{!! Form::text('direccion_importador',$import_cert->direccion_importador,['class'=>'form-control','id'=>'direccion_importador', "noreq"=>"noreq"]) !!}
										</div>
									</div>
									<div id="rif_imp">
										
									</div>
								
									
										<div class="form-group">
											
											{!! Form::label('','13.3 Teléfono') !!}
											{!! Form::text('telefono',$import_cert->telefono,['class'=>'form-control','id'=>'telefono', "noreq"=>"noreq"]) !!}
										</div>
										<div class="form-group">
											
											{!! Form::label('','13.4 E-mail') !!}
											{!! Form::text('email_importador',$import_cert->email_importador,['class'=>'form-control','id'=>'email_importador', "noreq"=>"noreq"]) !!}
										</div>
								
									<div id="transporte_puerto_impor">
										<div class="form-group">
											
											{!! Form::label('','(14) Medio de Transporte') !!}
											{!! Form::text('medio_transporte_importador',$import_cert->medio_transporte_importador,['class'=>'form-control','id'=>'medio_transporte_importador', "noreq"=>"noreq"]) !!}
										</div>
										<div class="form-group">
											
											{!! Form::label('','(15) Puerto o Lugar Embarque') !!}
											{!! Form::text('lugar_embarque_importador',$import_cert->lugar_embarque_importador,['class'=>'form-control','id'=>'lugar_embarque_importador', "noreq"=>"noreq"]) !!}
										</div>
									</div>
								</div>
							</div>
							<div id="observaciones">
								<div class="row">
									<div  class="col-md-12">
										<div class="form-group">
											{!! Form::label('','(16)Observaciones') !!}
											{!! Form::textarea('observaciones',null,['class'=>'form-control','id'=>'observaciones', "noreq"=>"noreq"]) !!}
										</div>
									</div>
								</div>
							</div>

						</div>
					</div>
					<div class="row text-center">
						<a href="{{ url('/exportador/CertificadoOrigen')}}" class="btn btn-primary">Cancelar</a>
						<input type="submit" class="btn btn-success" value="Corregir" name="submit" id="submit" onclick="validacionForm()"> 
					
					</div><br><br>


				</div>
		</div>
	</div><!--div content-->
    {{Form::close()}}

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="{{asset('js/ajax_v2.1.1.js')}}"></script>
<script src="{{asset('js/CertificadoOrigenJS/COEditPeru.js')}}"></script>






@stop
