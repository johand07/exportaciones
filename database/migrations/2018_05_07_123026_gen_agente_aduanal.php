<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GenAgenteAduanal extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('gen_agente_aduanal', function (Blueprint $table) {
          $table->engine = 'InnoDB';
          $table->increments('id');
          $table->unsignedInteger('gen_usuario_id')->index()->nullable();
          $table->foreign('gen_usuario_id')->references('id')->on('gen_usuario');
          $table->string('nombre_agente', 100)->nullable();
          $table->string('letra_rif_agente', 15)->nullable();
          $table->string('rif_agente', 15)->nullable();
          $table->string('numero_registro', 60)->nullable();
          $table->string('ciudad_agente', 100)->nullable();
          $table->string('telefono_agente', 15)->nullable();
          $table->boolean('bactivo')->default(1);
          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::dropIfExists('gen_agente_aduanal');
    }
}
