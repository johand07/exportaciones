@extends('templates/layoutlte')
@section('content')

   <div class="row">
  <div class="col-md-12">
    <div class="row">
      <div class="col-md-10"></div>
      <div class="col-md-2">
        <a class="btn btn-primary" href="{{url('/exportador/NDebito/create')}}">
          <span class="glyphicon glyphicon-file" title="Agregar" aria-hidden="true"></span>
           Registro Nota Débito
        </a>
      </div>
    </div>
    <table id="listaDebito" class="table table-striped table-bordered" style="width:100%">
            <thead>
                <tr>
                   
                    <th>Consignatario</th>
                    <th>Nro. Nota de Débito</th>
                    <th>Fecha de Emisión</th>
                    <th>Concepto de la Nota de Débito</th>
                    <th>Monto Nota de Débito</th>
                    <th>Nro. Factura Asociada</th>
                    <!--<th>ER</th>-->
                    <th>Justificacion</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody>
              @foreach($NDebito as  $debito)
                <tr>                
                    <td>{{$debito->consignatario->nombre_consignatario}}</td>   
                    <td>{{$debito->num_nota_debito}}</td>
                    <td>{{$debito->fecha_emision}}</td>
                    <td>{{$debito->concepto_nota_debito}}</td>
                    <td>{{$debito->monto_nota_debito}}</td>
                    <td>{{$debito->facturand->numero_factura}}</td>
                  <!--  <td>{{$debito->er}}</td> -->
                    <td>{{$debito->justificacion}}</td>
                    
                    <td>
                       <a href="{{route('NDebito.edit',$debito->id)}}" class="glyphicon glyphicon-edit btn btn-primary btn-sm" title="Modificar" aria-hidden="true"></a>    
                       
                       <input id="token" type="hidden" name="_token" value="{{ csrf_token() }}">
                       <a href="#" class="btn btn-danger btn-sm" title="Eliminar" onclick="return eliminar('NDebito/',{{$debito->id}},'listaDebito')"> <i class="glyphicon glyphicon-trash"></i> </a>

                    </td>
                </tr>
              @endforeach
            </tbody>
        </table>
  </div>
</div>
@stop