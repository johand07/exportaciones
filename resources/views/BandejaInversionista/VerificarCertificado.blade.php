<!DOCTYPE html>

<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="{{asset('adminlte/bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('adminlte/bower_components/font-awesome/css/font-awesome.min.css')}}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="{{asset('adminlte/bower_components/Ionicons/css/ionicons.min.css')}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('adminlte/dist/css/AdminLTE.min.css')}}">
    <!-- css calendario bootstrap -->
    <link rel="stylesheet" href="{{asset('css/bootstrap-datepicker3.min.css')}}">
    <!-- css data table bootstrap -->
    <link rel="stylesheet" href="{{asset('css/dataTables.bootstrap.min.css')}}">
    <!-- css switalert -->
    <link rel="stylesheet" href="{{asset('css/sweetalert.css')}}">

 
    <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
        page. However, you can choose any other skin. Make sure you
        apply the skin class to the body tag so the changes take effect. -->
  <link rel="stylesheet" href="{{asset('adminlte/dist/css/skins/skin-blue-light.min.css')}}">

    <script src='https://www.google.com/recaptcha/api.js'></script>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>.:Exportaciones:.</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <style type="text/css">
        .title {
            font-size: 35px;
            font-weight: bold;
            background: -webkit-linear-gradient(#eee, #333);
          -webkit-background-clip: text;
          -webkit-text-fill-color: transparent;
          color:#2390BE;
        }
        #pie{
           margin-top: 15px;
        }


        /*#contact{

        background-repeat: no-repeat;
        background: #3a6186; *//* fallback for old browsers */
        /*background: -webkit-linear-gradient(to left, #ECF2F4 , #D6E6F3); /* Chrome 10-25, Safari 5.1-6 */
        /*background: linear-gradient(to left, #ECF2F4 , #D6E6F3); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */

        /*}*/
        
        body {
        background: url('{{asset("img/fondo_mapa.jpg")}}') no-repeat center center fixed;
        background-size: cover;
        -moz-background-size: cover;
        -webkit-background-size: cover;
        -o-background-size: cover;
        }

        footer {
          background-color: #1E3165;
          position: fixed;
          margin: auto;
          bottom: 0;
          width: 100%;
          height: 50px;
          color: white;
          font-size: 16px;

        }
        a{
            font-weight: bold;
            font-size: 16px;
        }

      .bandera{

        position: absolute;
        top:  68px; 
      }

    </style>
</head>
<body class="fondo" >
    <div id="app" >
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">
                    <a class="navbar-brand" href="{{ url('/') }}">
                        <img class="img-responsive" style="width: 565px;" src="{{asset('img/cintillo.png')}}">

                    </a>
                </div>
            </div>
        </nav>
        <img  class="img-responsive bandera" src="{{asset('img/bandera.png')}}">

        <div class="container">
          @if(!empty($solicitud))
          <h2>Codigo Validado con Exito</h2>
              <div class="row">
                <div class="col-md-8"></div>         
                  <div class="col-md-4">
                    <table  style="width:100%" class="table table-striped table-bordered table-responsive-sm">
                      <thead>
                        <tr>
                          <th>1) N° SOLICITUD</th>

                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td><b>{{$solicitud->num_sol_inversionista}}</b></td>

                        </tr>
                      </tbody>
                    </table>
                    <table  style="width:100%" class="table table-striped table-bordered table-responsive-sm">
                      <thead>
                        <tr>
                          <th>2) FECHA/HORA</th>

                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>{{$solicitud->created_at}}</td>

                        </tr>
                      </tbody>
                    </table>
                  </div>
              </div>
              <div class="row text-center">
                <center><strong>CERTIFICADO DE REGISTRO</strong></center>
              </div>
              <div class="row text-center">
                <center><strong>INVERSIONISTA EXTRANJERO</strong></center>
              </div>     
                <div class="tabla">
                    <div align="left">
                      <span style="font-size:12px"><b>Datos del Exportador</b></span>
                    </div>
                      <table  style="width:100%" class="table table-striped table-bordered table-responsive-sm">
                        <thead>
                          <tr>
                            <th>3) NÚMERO DE RIF</th>
                            <th>4) NOMBRE O RAZÓN SOCIAL</th>
                            <th>5) PAÍS DE ORIGEN </th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>{{$solicitud->DetUsuario->rif}}</td>
                            <td>{{ucwords($solicitud->DetUsuario->razon_social)}}</td>
                            <td>{{$solicitud->pais->dpais}}</td>
                          </tr>
                        </tbody>
                      </table>
                </div>


                <div class="tabla">
                    <h4>Datos de Ubicación</h4>
                    <table  style="width:100%" class="table table-striped table-bordered table-responsive-sm">
                      <thead>
                        <tr>
                          <th>6) DIRECCIÓN</th>
                          <th>7) NÚMERO TELEFÓNICO </th>
                          <th>8) CORREO ELECTRÓNICO</th>
                          <th>9) PÁGINA WEB</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>{{$solicitud->DetUsuario->direccion}}</td>
                          <td>{{$solicitud->DetUsuario->telefono_movil}}</td>
                          <td>{{$solicitud->DetUsuario->correo}}</td>
                          <td>{{$solicitud->DetUsuario->pagina_web}}</td>
                        </tr>
                      </tbody>
                    </table>
                </div>

                <div class="tabla">
                    <h4>Datos Representante Legal </h4>
                    <table  style="width:100%" class="table table-striped table-bordered table-responsive-sm">
                      <thead>
                        <tr>
                          <th>10) NOMBRE DEL REPRSENTANTE LEGAL</th>
                          <th>11) APELLIDO DEL REPRSENTANTE LEGAL</th>
                          <th>12) NÚMERO DEL PASAPORTE</th>
                          <th>13) TIPO DE VISA</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          
                          <td>{{$solicitud->DetUsuario->nombre_repre}}</td>
                          <td>{{$solicitud->DetUsuario->apellido_repre}}</td>
                          <td>{{$solicitud->DetUsuario->ci_repre}}</td>
                          <td>{{$solicitud->tipovisa->nombre_visa}}</td>
                         
                        </tr>
                      </tbody>
                    </table>
                </div>

            
          
          @else
          <div class="alert alert-dismissible alert-warning">
            
            <h4 class="alert-heading">Datos Invalidos!</h4>
            <p class="mb-0"> <h4>No se puede generar la Verificación, la solicitud que intenta realizar es invalida..</h4></p>
          </div>
          
          @endif
        
    
        
    </div>
    @include('templates/footer')
    <!-- Scripts -->

    <!-- jQuery 3 -->
    <script src="{{asset('adminlte/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <!-- Bootstrap 3.3.7 -->
    <script src="{{asset('adminlte/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <!-- js calendario bootstrap -->
    <script src="{{asset('js/jquery.min.js')}}"></script>
    <script src="{{asset('js/bootstrap-datepicker.min.js')}}"></script>
    <script src="{{asset('js/locales/bootstrap-datepicker.es.min.js')}}"></script>

    <!-- script js y jquery del sistema -->
    <script src="{{asset('js/script_sesion.js')}}"></script>
    <!-- js sweetalert -->
    <script src="{{asset('js/sweetalert.js')}}"></script>
    <!-- AdminLTE App -->
    <script src="{{asset('adminlte/dist/js/adminlte.min.js')}}"></script>

   

</body>
</html>
