<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddGenDeclaracionInversionIdToDocumentosVariosInversionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {   
        Schema::table('documentos_varios_inversiones', function (Blueprint $table) {
            $table->unsignedInteger('gen_declaracion_inversion_id')->nullable()->after('gen_sol_inversionista_id');
            $table->foreign('gen_declaracion_inversion_id', 'g_d_i_fk100')->references('id')->on('gen_declaracion_inversion');
        });
    }   

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('documentos_varios_inversiones', function (Blueprint $table) {
            //
            $table->dropColumn('gen_declaracion_inversion_id');
        });
    }
}
