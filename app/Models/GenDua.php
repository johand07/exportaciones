<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GenDua extends Model
{
    protected $table = 'gen_dua';

    protected $fillable = [

        'gen_usuario_id',
        'gen_agente_aduana_id',
        'gen_transporte_id',
        'gen_aduana_salida_id',
        'gen_consignatario_id',
        'lugar_salida',
        'numero_dua',
        'numero_referencia',
        'aduana_llegada',
        'lugar_llegada',
        'fregistro_dua_aduana',
        'fembarque',
        'farribo',
        'bactivo',
    ];

    protected $date =['created_at','updated_at'];

    public function usuario() {

     return $this->hasOne('App\Models\GenUsuario','id','gen_usuario_id');

    }

    public function agente() {

     return $this->hasOne('App\Models\GenAgenteAduanal','id','gen_agente_aduana_id');

    }

    public function transporte() {

     return $this->hasOne('App\Models\GenTransporte','id','gen_transporte_id');

    }

    public function aduanassal() {

     return $this->hasOne('App\Models\GenAduanaSalida','id','gen_aduana_salida_id');


    }

    public function consignatario() {

     return $this->hasOne('App\Models\GenConsignatario','id','gen_consignatario_id');


    }
}
