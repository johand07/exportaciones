<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Parroquia extends Model
{
    protected $table = 'parroquia';

    public static function parroquias ($id){
    	return Parroquia::where('municipio_id','=',$id)->get();
    }
}
