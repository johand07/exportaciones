<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GenDvdNd extends Model
{
    protected $table = 'gen_dvd_nd';

    protected $fillable = [

        'nota_debito_id',
        'gen_solicitud_id',
        'gen_status_id',
        'gen_operador_cambiario_id',
        'cod_seguridad',
        'fdisponibilidad',
        'gen_factura_id',
        'mfob',
        'mpercibido',
        'mvendido',
        'mretencion',
        'mpendiente',
        'descripcion',
        'fstatus',
        'fventa_bcv',
        'realizo_venta',
        'bactivo',
    ];
      protected $date =['created_at','updated_at'];

    public function solicitud() {

     return $this->hasOne('App\Models\GenSolicitud','id','gen_solicitud_id');

    }

     public function ndebito() {

     return $this->hasOne('App\Models\NotaDebito','id','nota_debito_id');

    }

    public function estatus() {

     return $this->hasOne('App\Models\GenStatus','id','gen_status_id');

    }

    public function oca() {

     return $this->hasOne('App\Models\GenOperadorCamb','id','gen_operador_cambiario_id');

    }

    public function factura() {

     return $this->hasOne('App\Models\GenFactura','id','gen_factura_id');

    }


    public function ndoca() {

     return $this->hasOne('App\Models\NdRecepcionOca','id','nd_recepcion_oca_id');

    }


    public function docNd() {

        return $this->hasOne('App\Models\DocumentosDvdNdBdv','gen_dvd_nd_id','id');

    }
}
