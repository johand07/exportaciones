@extends('templates/layoutlte')
@section('content')

<div class="content">

 {{Form::model(@$ultima_exportacion,['route'=>['UltimaExportacion.update',@$ultima_exportacion->id],'method'=>'PATCH','id'=>'FormUlt_Export']) }}

 <div class="panel panel-primary">
  <div class="panel-heading">ULTIMA EXPORTACIÓN</div>
  <div class="panel-body">
    <div class="row">
      <div class="col-md-6">
        <div class="form-group">
          {!! Form::label('','País')!!}<!--Pais de la ultima exportacion-->
          {!!Form::select('pais_id',$paises,null,['class'=>'form-control','placeholder'=>'Seleccione un País de la Última Exportacion','id'=>'pais_id']) !!}
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group">
          {!! Form::label('','Fecha de Embarque de Última Exportación')!!}
          <div class="input-group date">
            {!! Form::text('fecha_ult_export',null, ['class'=>'form-control','id'=>'fecha_ult_export','onchange'=>'return validarfechaRM(this.value,"fecha_ult_export")','maxlength'=>'','readonly'=>'readonly']) !!}
            <span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
          </div> 
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="col-md-4"></div>
        <div class="col-md-4 text-center">
          <div class="form-group">
            <a class="btn btn-primary" href="{{route('DatosEmpresa.index')}}" id="atras1">Atrás</a>
            <input type="submit" name="Guardar" value="Guardar" class="btn btn-success">
          </div>
        </div>
      </div>
    </div>
  </div><!--Cierra el Panel body!-->
</div>
</div>
<br><br>
<br>
{{Form::close()}}
@stop
