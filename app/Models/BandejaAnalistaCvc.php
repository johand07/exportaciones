<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BandejaAnalistaCvc extends Model
{
     protected $table = 'bandeja_analista_cvc';
    
    protected $fillable = [
        'gen_sol_cvc_id',
        'gen_usuario_id',
        'gen_status_id',
        'fstatus',
        'bactivo',
    ];


     public function rGenSolCvc()
    {
        return $this->hasOne('App\Models\GenSolCvc','id','gen_sol_cvc_id');
    }

    public function rGenUsuario()
    {
        return $this->hasOne('App\Models\GenUsuario','id','gen_usuario_id');
    }

     public function rGenStatus()
    {
        return $this->hasOne('App\Models\GenStatus','id','gen_status_id');
    }
}
