<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GenSolicitud extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('gen_solicitud', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('tipo_solicitud_id')->index()->nullable();
            $table->foreign('tipo_solicitud_id')->references('id')->on('tipo_solicitud');
            $table->unsignedInteger('gen_usuario_id')->index()->nullable();
            $table->foreign('gen_usuario_id')->references('id')->on('gen_usuario');
            $table->unsignedInteger('gen_status_id')->index()->nullable();
            $table->foreign('gen_status_id')->references('id')->on('gen_status');
            $table->date('fstatus')->nullable();
            $table->date('fsolicitud')->nullable();
            $table->string('opcion_tecnologia', 30)->nullable();
            $table->double('monto_solicitud')->nullable();
            $table->double('monto_cif')->nullable();
            $table->string('cfirma_seguridad', 50)->nullable();
            $table->string('cagencia', 50)->nullable();
            $table->string('coperador_cambiario', 50)->nullable();
            $table->boolean('bactivo')->default(1);
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::dropIfExists('gen_solicitud');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
