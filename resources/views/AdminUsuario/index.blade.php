@extends('templates/layoutlte_admin')

@section('content')

<div class="row">
  
  <div class="col-md-12">
    <div class="row">
      <div class="col-md-10"></div>
      <div class="col-md-2">
        <a class="btn btn-success" href="{{url('/admin/reportePdf')}}" target="_blank">
          <span class="glyphicon glyphicon-print" title="Agregar" aria-hidden="true"></span>
           Reporte de Usuarios
        </a>
      </div>
    </div>
    <table id="listaUsuarios" class="table table-striped table-bordered" style="width:100%">
            <thead>
                <tr>
                    <th>Tipo de Usuario</th>
                    <th>Nombre o Razon Social</th>
                    <th>Rif</th>
                    <th>Estatus</th>
                    <th>Fecha Modificación</th>
                    <th>Email</th>
                    <th>Nombre Representate Legal</th>
                    <th>Cédula Representate Legal</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody>
              @foreach($users as $user)
                <tr>           
                    <td>{{$user->usuario->tipoUsuario->nombre_tipo_usu}}</td>
                    <td>{{$user->razon_social}}</td> 
                    <td>{{$user->rif}}</td> 
                    <td>{{$user->usuario->estatus->nombre_status}}</td> 
                    <td>{{$user->usuario->updated_at}}</td> 
                    <td>{{$user->usuario->email}}</td>
                    <td>{{$user->nombre_repre}}</td>
                    <td class="col-md-2">{{$user->ci_repre}}</td>
                    <td class="col-md-2 text-center">
                    <!--Ruta y metodo para Editar-->
                    <a href="{{route('AdminUsuario.edit',$user->gen_usuario_id)}}" title="Actualizar" class="btn btn-primary btn-sm"><i class="glyphicon glyphicon-edit"></i></a>
                    <!--Boton eliminar-->
                    <input id="token" type="hidden" name="_token" value="{{ csrf_token() }}">
                     <!--Ruta, id del Usuario y el id del DataTable
                       <a href="#" class="btn btn-danger btn-sm" title="Eliminar" onclick="return eliminar('/admin/AdminUsuario/',{{$user->gen_usuario_id}},'listaUsuarios')"> <i class="glyphicon glyphicon-trash"></i> </a>-->
                     <!-- Ruta, para consultar el Usuario 
                     <a href="{{route('AdminUsuarioDetalle.show',$user->gen_usuario_id)}}" class="btn btn-success btn-sm" title="Ver"> <i class="glyphicon glyphicon-search"></i> </a>-->
                    </td>  

                    </tr>
              @endforeach

            </tbody>
        </table>
  </div>
  
</div>

@stop
