<?php

namespace App\Http\Controllers;

use App\Models\GenUsuario;
use App\Models\DetUsuario;
use Illuminate\Http\Request;
use App\Http\Requests\CorpCafeRequest;

use App\Models\Pais;
use App\Models\GenAduanaSalida;
use App\Models\GenUnidadMedida;
use App\Models\GenDivisa;
use App\Models\GenSolCvc;
use Illuminate\Support\Str;

use Auth;
use File;
use Session;
use Alert;

class CorporacionCafeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $solicitudes = GenSolCvc::where('gen_usuario_id',Auth::user()->id)->where('bactivo',1)->get();
        return view('corporacionCafe.index', compact('solicitudes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $titulo= 'Certificado de Origen';
        $descripcion= 'Peril del Exportador';/* nsantos*/

        $det_usuario=DetUsuario::where('gen_usuario_id',Auth::user()->id)->where('bactivo',1)->first();

        $pais=Pais::where('bactivo',1)->pluck('dpais','id');

        $aduanasal=GenAduanaSalida::all()->pluck('daduana','id');

        $UnidadMedida=GenUnidadMedida::where('bactivo',1)->whereIn('id', [10,13,32,45])->pluck('dunidad','id');

        /*$divisas=GenDivisa::all()->pluck('ddivisa_abr','id');*/

        $divisas=GenDivisa::where('bactivo',1)->whereIn('id', [32,18,26,])->pluck('ddivisa_abr','id');

        return view('corporacionCafe.create',compact('titulo','descripcion','det_usuario','pais','aduanasal','UnidadMedida','divisas'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CorpCafeRequest $request)
    {
        
        $gen_solCVC = new GenSolCvc;
        $gen_solCVC->gen_usuario_id = Auth::user()->id;
    	$gen_solCVC->num_sol_cvc = $this->generarRamdom();   
    	$gen_solCVC->gen_status_id = 9;
    	$gen_solCVC->fstatus = date('Y-m-d');
    	$gen_solCVC->direccion_notif = $request->direccion_notif;
    	$gen_solCVC->clave_pais = $request->clave_pais;
    	$gen_solCVC->gen_aduana_salida_id = $request->gen_aduana_salida_id;
    	$gen_solCVC->num_serie = $request->num_serie;
    	$gen_solCVC->pais_destino = $request->pais_destino;
    	$gen_solCVC->pais_productor = 168;
    	$gen_solCVC->pais_transbordo = $request->pais_transbordo;
    	$gen_solCVC->fexportacion = $request->fexportacion;
    	$gen_solCVC->nombre_transporte = $request->nombre_transporte;
    	$gen_solCVC->marca_ident = $request->marca_ident;
    	$gen_solCVC->cat_cargados_id = $request->cat_cargados_id;
    	$gen_solCVC->peso_neto = $request->peso_neto;
    	$gen_solCVC->gen_unidad_medida_id = $request->gen_unidad_medida_id;
    	$gen_solCVC->cat_tipo_cafe_id = $request->cat_tipo_cafe_id;
    	$gen_solCVC->cat_metodo_elab_id = $request->cat_metodo_elab_id;
    	$gen_solCVC->norma_calidad_cafe = $request->norma_calidad_cafe;
    	$gen_solCVC->caract_especiales = $request->caract_especiales;
    	$gen_solCVC->codio_sist_sa = $request->codio_sist_sa;
    	$gen_solCVC->valor_fob = $request->valor_fob;
    	$gen_solCVC->gen_divisa_id = $request->gen_divisa_id;
    	$gen_solCVC->codigo_arancelario = $request->codigo_arancelario;
    	$gen_solCVC->info_adicional = $request->info_adicional;
        $gen_solCVC->save();

        Alert::success('REGISTRO EXITOSO','Registro Agregado')->persistent("Ok");
        return redirect()->action('CorporacionCafeController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\GenUsuario  $genUsuario
     * @return \Illuminate\Http\Response
     */
    public function show(GenUsuario $genUsuario)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\GenUsuario  $genUsuario
     * @return \Illuminate\Http\Response
     */
    public function edit(GenUsuario $genUsuario,$id)
    {
        $det_usuario=DetUsuario::where('gen_usuario_id',Auth::user()->id)->where('bactivo',1)->first();
        $aduanasal=GenAduanaSalida::all()->pluck('daduana','id');

        $pais=Pais::where('bactivo',1)->pluck('dpais','id');

        $UnidadMedida=GenUnidadMedida::where('bactivo',1)->whereIn('id', [10,13,32,45])->pluck('dunidad','id');

        /*$divisas=GenDivisa::all()->pluck('ddivisa_abr','id');*/

        $divisas=GenDivisa::where('bactivo',1)->whereIn('id', [32,18,26,])->pluck('ddivisa_abr','id');

        $cafecvc=GenSolCvc::find($id);

        return view('corporacionCafe.edit',compact('cafecvc','det_usuario','aduanasal','pais','UnidadMedida','divisas'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\GenUsuario  $genUsuario
     * @return \Illuminate\Http\Response
     */
    public function update(CorpCafeRequest $request, GenUsuario $genUsuario, $id)
    {
        $gen_solCVC = GenSolCvc::find($id);
          
    	$gen_solCVC->gen_status_id = 18;
    	$gen_solCVC->fstatus = date('Y-m-d');
    	$gen_solCVC->direccion_notif = $request->direccion_notif;
    	$gen_solCVC->clave_pais = $request->clave_pais;
    	$gen_solCVC->gen_aduana_salida_id = $request->gen_aduana_salida_id;
    	$gen_solCVC->num_serie = $request->num_serie;
    	$gen_solCVC->pais_destino = $request->pais_destino;
    	$gen_solCVC->pais_productor = 168;
    	$gen_solCVC->pais_transbordo = $request->pais_transbordo;
    	$gen_solCVC->fexportacion = $request->fexportacion;
    	$gen_solCVC->nombre_transporte = $request->nombre_transporte;
    	$gen_solCVC->marca_ident = $request->marca_ident;
    	$gen_solCVC->cat_cargados_id = $request->cat_cargados_id;
    	$gen_solCVC->peso_neto = $request->peso_neto;
    	$gen_solCVC->gen_unidad_medida_id = $request->gen_unidad_medida_id;
    	$gen_solCVC->cat_tipo_cafe_id = $request->cat_tipo_cafe_id;
    	$gen_solCVC->cat_metodo_elab_id = $request->cat_metodo_elab_id;
    	$gen_solCVC->norma_calidad_cafe = $request->norma_calidad_cafe;
    	$gen_solCVC->caract_especiales = $request->caract_especiales;
    	$gen_solCVC->codio_sist_sa = $request->codio_sist_sa;
    	$gen_solCVC->valor_fob = $request->valor_fob;
    	$gen_solCVC->gen_divisa_id = $request->gen_divisa_id;
    	$gen_solCVC->codigo_arancelario = $request->codigo_arancelario;
    	$gen_solCVC->info_adicional = $request->info_adicional;
        $gen_solCVC->save();

        Alert::success('ACTUALIZACIÓN EXITOSA','Registro Actualizado')->persistent("Ok");
        return redirect()->action('CorporacionCafeController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\GenUsuario  $genUsuario
     * @return \Illuminate\Http\Response
     */
    public function destroy(GenUsuario $genUsuario)
    {
        //
    }

    private function generarRamdom()
    {
        $user = Auth::user();
        $random = Str::random(4);
        $id  = $user->id;
        $rif = substr($user->detUsuario->rif, -2);
        $fecha = date('Ymd');
        return $id.'-'.$random.$fecha.$rif;
    }
}
