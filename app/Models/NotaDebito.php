<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NotaDebito extends Model
{
   	protected $table = 'nota_debito';

   	protected $fillable = [


   		'gen_usuario_id',
		'gen_factura_id',
		'gen_consignatario_id',
		'num_nota_debito',
		'fecha_emision',
		'concepto_nota_debito',
		'er',
		'monto_nota_debito',
		'justificacion',
		'bactivo',
	];

		protected $date =['created_at','updated_at'];

	     public function facturand(){

	     	return $this->hasOne('App\Models\GenFactura','id','gen_factura_id');
	}

		public function usuario() {

     		return $this->hasOne('App\Models\GenUsuario','id','gen_usuario_id');

    }
    public function consignatario() {

     		return $this->hasOne('App\Models\GenConsignatario','id','gen_consignatario_id');

    }

}
