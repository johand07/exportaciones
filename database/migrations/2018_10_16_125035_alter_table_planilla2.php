<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTablePlanilla2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('planilla_2', function (Blueprint $table) {
            //
            $table->string('descrip_observacion')->nullable()->after('bactivo');
            $table->unsignedInteger('estado_observacion')->index()->nullable()->after('bactivo');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('planilla_2', function (Blueprint $table) {
            //
            $table->dropColumn('descrip_observacion');
            $table->dropColumn('estado_observacion');
        });
    }
}
