->nullable()<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Incoterm extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('incoterm', function (Blueprint $table) {
          $table->engine = 'InnoDB';
          $table->increments('id');
          $table->string('nombre_incoterm', 100)->nullable();
          $table->string('siglas_incoterm', 50)->nullable();
          $table->string('resena', 1000)->nullable();
          $table->string('obliga_vendedor', 1000)->nullable();
          $table->string('obliga_comprador', 1000)->nullable();
          $table->boolean('bactivo')->default(1);
          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::dropIfExists('incoterm');
    }
}
