<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CriterioAnalisis extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('criterio_analisis', function (Blueprint $table) {
          $table->engine = 'InnoDB';
          $table->increments('id');
          $table->unsignedInteger('analisis_calificacion_id')->index()->nullable();
          $table->foreign('analisis_calificacion_id')->references('id')->on('analisis_calificacion');
          $table->unsignedInteger('det_declaracion_produc_id')->index()->nullable();
          $table->foreign('det_declaracion_produc_id')->references('id')->on('det_declaracion_produc');
          $table->string('bol',100)->nullable();
          $table->string('col',100)->nullable();
          $table->string('ecu',100)->nullable();
          $table->string('per',100)->nullable();
          $table->string('cub',100)->nullable();
          $table->string('ald',100)->nullable();
          $table->string('arg',100)->nullable();
          $table->string('bra',100)->nullable();
          $table->string('par',100)->nullable();
          $table->string('uru',100)->nullable();
          $table->string('usa',100)->nullable();
          $table->string('ue',100)->nullable();
          $table->string('cnd',100)->nullable();
          $table->string('tp',100)->nullable();
          $table->boolean('bactivo')->default(1);
          $table->timestamps();
      });
    }

    /**
     * Reverse the migra.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('criterio_analisis'); 
    }
}
