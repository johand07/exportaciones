<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GenAccionistaInversionista extends Model
{
     protected $table = 'gen_accionista_inversionista';

    protected $fillable = [

           'gen_sol_inversionista_id',
           'accionistas_id',
           'bactivo',
    ];

    protected $date =['created_at','updated_at'];

    public function solicitudinversionista() {

    	return $this->hasOne('App\Models\GenSolInversionista','id','gen_sol_inversionista_id');

	}

	public function accionistas_2() {

    	return $this->hasOne('App\Models\Accionistas','id','accionistas_id');

	}

}
