@extends('templates/layoutlte_analista_cna')
@section('content')
<div class="content" style="background-color: #fff">
    <div class="panel-group" style="background-color: #fff;">
      <div class="panel-primary">
        <div class="panel-heading"><h3>SOLICITUDES DE CNA</h3> </div>
        <div class="panel-body">
            <div class="row">
              <div class="col-md-12">
                <div class="row">
                  <div class="col-md-8">
                     <h4 class="text-info">Bandeja de Solicitudes:</h4><hr>   
                  </div>
                  <div class="col-md-2">
                  </div>
                  <div class="col-md-2">
                    <a class="btn btn-primary" href="{{route('ListaAnalistCna.index')}}">
                      <span class="glyphicon glyphicon-refresh" title="Agregar" aria-hidden="true"></span>
                        Actualizar
                    </a>
                  </div>
                </div>
                <table id="listaCna" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                      <tr>
                        <th class="text-center">N° Solicitud</th>
                        <th class="text-center">Código Empresa</th>
                        <th class="text-center">Razón Social</th>
                        <th class="text-center">Rif Empresa</th>
                        <th class="text-center">Estatus</th>
                        <th class="text-center">Acciones</th>
                      </tr>
                    </thead>
                    <tbody>
                    @foreach($solicitud_cna as $key=> $sol_cna)
                        <tr>
                          <td class="text-primary"><b>{{$sol_cna->num_sol_resguardo}}</b></td>
                          <td class="text-center">{{$sol_cna->DetUsuario->cod_empresa}}</td>
                          <td class="text-center">{{$sol_cna->DetUsuario->razon_social}}</td>
                          <td class="text-center">{{$sol_cna->DetUsuario->rif}}</td>
                          <td class="text-center">{{$sol_cna->rEstatusCna->nombre_status}}</td>
                          <td class="text-center">
                          @if($sol_cna->status_cna==21)
                            <a href="{{ route('ListaAnalistCna.edit',$sol_cna->id)}}" class="btn btn-sm btn-danger" id="atendida"><i class="glyphicon glyphicon-user"></i><b> Atendida por CNA</b></a>

                          @endif
                          @if ($sol_cna->status_cna==23)
                            {{--<a href="{{ route('ListaAnalistCna.edit',$sol_cna->id)}}" class="btn btn-sm btn-success" id="aprobada"><i class="glyphicon glyphicon-user"></i><b>Aprobada por CNA</b></a>--}}

                             <a href="{{action('PdfController@CertificadoVuceCna',array('id'=>$sol_cna->id))}}" class="glyphicon glyphicon-download  btn btn-success  btn-sm" target="_blank"  aria-hidden="true" title="Certificado CNA"><b>Descargar PDF</b></a>
                          @endif
                        
                          @if($sol_cna->status_cna==9)
                            <a href="{{ route('ListaAnalistCna.edit',$sol_cna->id)}}" class="btn btn-sm btn-danger" id="negada"><i class="glyphicon glyphicon-user"></i><b>Atender</b></a>

                          @endif

                          @if($sol_cna->status_cna==18)
                            <a href="{{ route('ListaAnalistCna.edit',$sol_cna->id)}}" class="btn btn-sm btn-warning" title="corregida por el usuario" id="negada"><i class="glyphicon glyphicon-user"></i><b>Corregida</b></a>

                          @endif
                          @if($sol_cna->status_cna==25)
                            <a href="{{ route('ListaAnalistCna.edit',$sol_cna->id)}}" class="btn btn-sm btn-warning" title="corregida por el usuario" id="negada"><i class="glyphicon glyphicon-user"></i><b>Negada</b></a>

                          @endif
                          
                          <!--a href="{{ route('ListaAnalistCna.edit',$sol_cna->id)}}" class="btn btn-sm btn-danger" id="negada"><i class="glyphicon glyphicon-user"></i><b>Documentos Incompletos</b></a-->

                      </td>
                    </tr>
                @endforeach 
                    </tbody> 
                </table>
              </div>
            </div>
        </div>
      </div>
    </div>
</div>
@stop