UPDATE `gen_ente` SET `descripcion_ente` = 'Centro de Inversión CIIP' WHERE `gen_ente`.`id` = 2;

UPDATE `cat_documentos` SET `nombre_documento` = 'Forma RPI01 completada por el solicitante' WHERE `cat_documentos`.`id` = 10;

UPDATE `cat_documentos` SET `nombre_documento` = 'Carta de solicitud de inscripción en el SURI, indicando el cumplimiento de los requisitos legales y una descripción del proyecto de inversión que pretende desarrollar (brochure).' WHERE `cat_documentos`.`id` = 11;

UPDATE `cat_documentos` SET `nombre_documento` = 'Estados de Cuenta, referencias bancarias, balances contables o cualquier otro documento que evidencie el cumplimiento en activos físicos o monetarios del monto legal establecido en la Ley Constitucional de Inversión Extranjera Productiva para optar a la condición de Inversionista Extranjero. ' WHERE `cat_documentos`.`id` = 12;

UPDATE `cat_documentos` SET `nombre_documento` = 'Declaración de fuente licita de fondos.' WHERE `cat_documentos`.`id` = 13;

UPDATE `cat_documentos` SET `nombre_documento` = 'Copia de documentos en los que se demuestre haber realizado los contactos oficiales para realizar actividades económicas o de inversión en el país.' WHERE `cat_documentos`.`id` = 14;

UPDATE `cat_documentos` SET `nombre_documento` = 'Comprobante de pago de trámite por un monto equivalente a 2 petros.' WHERE `cat_documentos`.`id` = 15;

UPDATE `cat_documentos` SET `nombre_documento` = 'Documento de identidad (Cédula o Pasaporte Vigente).' WHERE `cat_documentos`.`id` = 16; 

UPDATE `cat_documentos` SET `nombre_documento` = 'Antecedentes penales debidamente apostillados y traducidos en idioma español.' WHERE `cat_documentos`.`id` = 17;


INSERT INTO `cat_documentos` (`id`, `gen_ente_id`, `nombre_documento`, `bactivo`, `created_at`, `updated_at`) VALUES (NULL, '2', 'Poder de representación debidamente autenticado. En caso de extranjeros, apostillado y traducido en idioma español. (En caso de ser requerido).', '1', '2021-05-12 00:00:00', '2021-05-12 00:00:00');

INSERT INTO `cat_documentos` (`id`, `gen_ente_id`, `nombre_documento`, `bactivo`, `created_at`, `updated_at`) VALUES (NULL, '2', 'Registro mercantil o personería jurídica.', '1', '2021-05-12 00:00:00', '2021-05-12 00:00:00');

INSERT INTO `cat_documentos` (`id`, `gen_ente_id`, `nombre_documento`, `bactivo`, `created_at`, `updated_at`) VALUES (NULL, '2', 'Datos y documentos de identificación del Representante Legal de la persona jurídica (ej.: documento de identidad, pasaporte, visa, etc).', '1', '2021-05-12 00:00:00', '2021-05-12 00:00:00');

INSERT INTO `cat_documentos` (`id`, `gen_ente_id`, `nombre_documento`, `bactivo`, `created_at`, `updated_at`) VALUES (NULL, '2', 'Documento apostillado que evidencie la cualidad del Representante Legal de la persona jurídica solicitante (ej.: actas de asamblea, de Junta Directiva, o de acuerdo a la denominación legal existente en el país de origen).', '1', '2021-05-12 00:00:00', '2021-05-12 00:00:00');


INSERT INTO `cat_documentos` (`id`, `gen_ente_id`, `nombre_documento`, `bactivo`, `created_at`, `updated_at`) VALUES (NULL, '2', 'Estados financieros de la persona jurídica solicitante.', '1', '2021-05-12 00:00:00', '2021-05-12 00:00:00');
