@extends('templates/layoutlte_admin')

@section('content')

@if (isset($detusers))
  
    <div class="bs-example">
        <div class="panel-group" id="accordion">
            <div class="panel panel panel-primary">
                <div class="panel-heading">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" class="list-group-item active">
                      <h4 class="panel-title">
                          1. DATOS BÁSICOS
                      </h4>
                    </a>
                </div>
                <div id="collapseOne" class="panel-collapse collapse in">
                    <div class="panel-body">
                    <div class="row">
                      <div class="col-md-12">
                        <div class="col-md-3">
                          <b>Nombre o Razón Social:</b><br>{{$detusers->razon_social}}
                        </div>
                        <div class="col-md-3">
                          <b>Siglas:</b><br>{{$detusers->siglas}}
                        </div>
                        <div class="col-md-3">
                          <b>Rif:</b><br>{{$detusers->rif}}
                        </div>
                        
                        <div class="col-md-3">
                          <b>Tipo de Empresa:</b><br>{{$detusers->tipoempresa->d_tipo_empresa}}
                        </div><br>
                      </div>
                    </div>
              
                    </div>
                </div>
            </div>
            <div class="panel panel-primary">
                <div class="panel-heading">
                  <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" class="list-group-item active"> 
                      <h4 class="panel-title">
                        2. TIPO DE ACTIVIDAD ECONÓMICA
                      </h4>
                    </a>
                </div>
                <div id="collapseTwo" class="panel-collapse collapse">
                    <div class="panel-body">
                        <div class="row">
                      <div class="col-md-12">
                        <div class="col-md-2"><b>Exportador:</b>
                          @if($detusers->export==1) Si @else No @endif 
                        </div>
                        <div class="col-md-2"><b>Inversionista:</b>
                          @if($detusers->invers==1) Si @else No @endif 
                        </div>
                        <div class="col-md-2"><b>Productor:</b>
                          @if($detusers->product==1) Si @else No @endif 
                        </div>
                        <div class="col-md-2"><b>comerciolizadora:</b>
                          @if($detusers->comerc==1) Si @else No @endif 
                        </div>
                        <div class="col-md-2">
                          <b>Sector:</b><br>{{$detusers->actividadeco->actividadecosector->descripcion_sector}}
                        </div>
                        
                        <div class="col-md-2">
                          <b>Actividad Economica:</b><br>{{$detusers->actividadeco->dactividad}}
                        </div>
                      </div>
                    </div>
                    </div>
                </div>
            </div>
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree" class="list-group-item active">
                      <h4 class="panel-title">
                           3. DOMICILIO FISCAL
                      </h4>
                    </a>
                </div>
                <div id="collapseThree" class="panel-collapse collapse">
                    <div class="panel-body">
                       <div class="row">
                      <div class="col-md-12">
                        <div class="col-md-4"><b>Estado:</b><br>{{$detusers->estado->estado}}
                        </div>
                        <div class="col-md-4"><b>Municipio:</b><br>{{$detusers->municipio->municipio}}</div>
                        <div class="col-md-4"><b>Parroquia:</b><br>{{$detusers->parroquia->parroquia}}</div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <div class="col-md-6"><b>Direccion:</b><br>{{$detusers->direccion}}</div>
                        <div class="col-md-3"><td><b>Telefono Local:</b><br>{{$detusers->telefono_local}}</div>
                        <div class="col-md-3"><b>Telefono Celular:</b></br>{{$detusers->telefono_movil}}</div>
                      </div>
                    </div>

                    </div>
                </div>
            </div>
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse4" class="list-group-item active">
                      <h4 class="panel-title">
                           4. DATOS DE LA PERSONA JURIDICA O FIRMA PERSONAL
                      </h4>
                    </a>
                </div>
                <div id="collapse4" class="panel-collapse collapse">
                  <div class="panel-body">
                    <div class="row">
                      <div class="col-md-12">
                        <div class="col-md-6">
                          <b>Registro de Circunscripción Judicial:</b><br>
                          <b>{{$detusers->circunsjudicial->iso_3166_2}}</b>&nbsp;&nbsp;
                          {{$detusers->circunsjudicial->registro}}
                        </div>
                        <div class="col-md-3">
                          <b>Número de Documento:</b><br>{{$detusers->num_registro}}
                        </div>

                        <div class="col-md-3">
                          <b>Número de Tomo:</b></br>{{$detusers->tomo}}
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <div class="col-md-6"><b>Número de Folio:</b><br>
                          
                          {{$detusers->folio}}
                        </div>
                        <div class="col-md-3">
                          <b>Oficina:</b><br>{{$detusers->Oficina}}
                        </div>

                        <div class="col-md-3">
                          <b>Fecha de Constitución:</b></br>{{$detusers->f_registro_mer}}
                        </div>
                      </div>
                    </div>                                            
                  </div>
                </div>
              </div>
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse5" class="list-group-item active">
                      <h4 class="panel-title">
                           5. DATOS DEL REPRESENTANTE LEGAL
                      </h4>
                    </a>
                </div>
                <div id="collapse5" class="panel-collapse collapse">
                    <div class="panel-body">
                      <div class="row">
                        <div class="col-md-12">
                          <div class="col-md-3">
                            <b>Nombre Representante:</b><br>{{$detusers->nombre_repre}}
                          </div>
                          <div class="col-md-3">
                            <b>Apellido Representante:</b><br>{{$detusers->apellido_repre}}
                          </div>
                          <div class="col-md-3">
                            <b>Cedula Representante:</b></br>{{$detusers->ci_repre}}
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-12">
                          <div class="col-md-6">
                            <b>Cargo Representante:</b></br>{{$detusers->cargo_repre}}
                          </div>
                          <div class="col-md-6">
                            <b>Correo Electrónico:</b></br>{{$detusers->correo_repre}}
                          </div>
                        </div>
                      </div>
                    </div>
                </div>

            </div>

            
              <div class="panel panel-primary">
                  <div class="panel-heading">
                      <a data-toggle="collapse" data-parent="#accordion" href="#collapse6" class="list-group-item active">
                        <h4 class="panel-title">
                             6. DATOS DE LOS ACCIONISTAS
                        </h4>
                      </a>
                  </div>
                  <div id="collapse6" class="panel-collapse collapse">
                      <div class="panel-body">
                      @foreach($accionistas as $accionista)
                        <div class="row">
                          <div class="col-md-12">
                            <div class="col-md-3">
                              <b>Nombre:</b><br>{{$accionista->nombre_accionista}}
                            </div>
                            <div class="col-md-3">
                              <b>Apellido:</b><br>{{$accionista->apellido_accionista}}
                            </div>
                            <div class="col-md-3">
                              <b>Cédula:</b></br>{{$accionista->cedula_accionista}}
                            </div>
                            <div class="col-md-3">
                              <b>Cargo:</b></br>{{$accionista->cargo_accionista}}
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-12">
                            <div class="col-md-3">
                              <b>Teléfono:</b></br>{{$accionista->telefono_accionista}}
                            </div>
                            <div class="col-md-3">
                              <b>Nacionalidad:</b></br>{{$accionista->nacionalidad_accionista}}
                            </div>
                            <div class="col-md-3">
                              <b>% Participación:</b></br>{{$accionista->participacion_accionista}}
                            </div>
                            <div class="col-md-3">
                              <b>Correo Electrónico:</b></br>{{$accionista->correo_accionista}}
                            </div>
                          </div>
                        </div>
                        <hr>
                      @endforeach
                      </div>
                  </div>

              </div>

              <div class="panel panel-primary">
                  <div class="panel-heading">
                      <a data-toggle="collapse" data-parent="#accordion" href="#collapse7" class="list-group-item active">
                        <h4 class="panel-title">
                             7. DATOS DE LOS PRODUCTOS QUE PRODUCE Y/O COMERCIALIZA
                        </h4>
                      </a>
                  </div>
                  <div id="collapse7" class="panel-collapse collapse">
                      <div class="panel-body">
                      @if($detusers->produc_tecno==1)
                        <div class="row">
                          <div class="col-md-12">
                            <b>Producto de Tecnoligia:</b><br>{{$detusers->especifique_tecno}}
                          </div>
                        </div>
                      @else
                        @foreach($productos as $producto)
                          <div class="row">
                            <div class="col-md-12">
                              <div class="col-md-3">
                                <b>Código Arancelario:</b><br>{{$producto->codigo}}
                              </div>
                              <div class="col-md-3">
                                <b>Descripción Arancelaria:</b><br>{{$producto->descripcion}}
                              </div>
                              <div class="col-md-3">
                                <b>Descripción Comercial:</b></br>{{$producto->descrip_comercial}}
                              </div>
                              <div class="col-md-3">
                                <b>Unidad de Medida:</b></br>{{$producto->unidadMedida->dunidad}}
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-12">
                              <div class="col-md-3">
                                <b>Capacida Operativa Anual:</b></br>{{$producto->cap_opera_anual}}
                              </div>
                              <div class="col-md-3">
                                <b>Capacidad Instalada Anual:</b></br>{{$producto->cap_insta_anual}}
                              </div>
                              <div class="col-md-3">
                                <b>Capacidad de Almacenamiento Anual:</b></br>{{$producto->cap_almacenamiento}}
                              </div>
                              <div class="col-md-3">
                                <b>Capacidad de Exportación Anual:</b></br>{{$producto->cap_exportacion}}
                              </div>
                            </div>
                          </div>
                          <hr>
                        @endforeach
                      @endif
                      </div>
                  </div>

              </div>
            <br><br>
            <div class="row text-center">
              <a class="btn btn-primary" href="{{url('admin/AdminUsuario')}}">Regresar</a>
            </div>
            
        </div>    
    </div>
   
@endif
@stop


