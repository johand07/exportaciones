<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GenUnidadMedida extends Model
{
    protected $table = 'gen_unidad_medida';
}
