<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DocumentosCertificado extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('documentos_certicado', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('gen_certificado_id')->default(1)->index()->nullable();
            $table->foreign('gen_certificado_id')->references('id')->on('gen_certificado');
            $table->string('file_1',300)->nullable();
            $table->string('file_2',300)->nullable();
            $table->string('file_3',300)->nullable();
            $table->string('file_4',300)->nullable();
            $table->string('file_5',300)->nullable();
            $table->string('file_6',300)->nullable();
            $table->integer('estado_observacion')->nullable();
            $table->string('descrip_observacion',100)->nullable();
            $table->boolean('bactivo')->default(1);
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('documentos_certicado');
    }
}
