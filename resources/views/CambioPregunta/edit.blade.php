@extends('templates/layoutlte')
@section('content')


{{Form::model($usuario,['route'=>['CambioPregunta.update',$usuario->id],'method'=>'PATCH','id'=>'formCambioPregSeg'])}}

	<div class="row">
	  <div class="col-md-12"><br>
	      <div class="col-md-3">

	      </div>
	      
	      <div class="col-md-6">
	        	     
	        <div class="form-group">
	        	{!! Form::label('','Pregunta Anterior')!!}
	          	{!! Form::text('pregunta_seg',$usuario->pregunta->descripcion_preg,['class'=>'form-control','id'=>'pregunta_seg', 'readonly'=>'true']) !!}
	        </div>
	        <div class="form-group" id="lista_preg">
	          {!! Form::label('','Pregunta Nueva')!!}
	          {!! Form::select('cat_preguntas_seg_id',$preguntaseg,null, ['class'=>'form-control','placeholder'=>'Seleccione una pregunta','id'=>'preguntas_seg', 'onchange'=>'preguntaSeg()','onchange'=>'preguntaSeg()']) !!}
	        </div>
	        <div class="form-group " id="preg_alternativa" style="display:none;" >
	          {!! Form::label('','Pregunta Alternativa')!!}
	          {!! Form::text('pregunta_alter',null,['class'=>'form-control','id'=>'pregunta_alter','maxlength'=>'200','onkeypress'=>'return soloNumerosyLetras(event)']) !!}
	          <span style="color:#3097D1;cursor:pointer;" id="boton">lista de preguntas</span>
	        </div>
	        <div class="form-group">
	          {!! Form::label('','Respuesta')!!}
	          {!! Form::text('respuesta_seg',null, ['class'=>'form-control','id'=>'respuesta_seg','onkeypress'=>'return soloNumerosyLetras(event)','maxlength'=>'100']) !!}
	        </div>
	      </div>
	     
	    
	      <div class="col-md-3">

	      </div>
	      <div class="row">
			   <div class="col-md-12 text-center">
			     <div class="form-group">
			       <br><input class="btn btn-primary" type="submit" name="" value="Cancelar">
			       <input class="btn btn-primary" type="submit" name="" value="Enviar" onclick="enviarpreguntaseg()">
			     </div>
			   </div>
 			</div>
	  </div>
	</div>
{{Form::close()}}
@stop