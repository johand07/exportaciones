@extends('templates/layoutlte')
@section('content')


<!--Fin de la modal de codigos Arancelarios-->
  <div class="content">

	{{Form::model($accionistas,['route'=>['DatosAccionista.update',$id],'method'=>'PATCH','id'=>'FormAccionista']) }}

@php $contador=count($accionistas); @endphp
{{Form::hidden('contador',$contador)}}

			<div >
          <div class="panel-heading">DATOS DE LOS ACCIONISTAS</div>
          <div class="panel-body">
              <table class="table table-bordered text-center" id="fields">

                   <tr> <td><button  name="add" type="button" id="add" value="add more" class="btn btn-success" >Agregar</button></td></tr>

                    <tr>
                      <th>Nombre</th>
                      <th>Apellido</th>
                      <th>Rif</th>
                      <th>Cargo</th>
                      <th>Teléfono</th>
                      <th>Nacionalidad</th>
                      <th>% Participación</th>
                      <th>Correo Electrónico</th>
                      <th></th>
                    </tr>

                    @if (isset($accionistas))
                        @foreach ($accionistas as $key=>$accionista)
                          <tr id="row{{$key+1}}">

                            <td>{!!Form::text('nombre_accionista[]',$accionista->nombre_accionista,['class'=>'form-control','id'=>'nombre_accionista','onkeypress'=>'return soloLetras(event)','maxlength'=>'50'])!!}</td>

                            <td>{!!Form::text('apellido_accionista[]',$accionista->apellido_accionista,['class'=>'form-control','id'=>'apellido_accionista','onkeypress'=>'return soloLetras(event)','maxlength'=>'50'])!!}</td>

                            <td>{!!Form::text('cedula_accionista[]',$accionista->cedula_accionista,['class'=>'form-control','id'=>'cedula_accionista','onkeypress'=>'return CedulaFormatrep(this,"Cedula de Indentidad Invalida",-1,true,event)','maxlength'=>'10'])!!}<small>Ejemplo:V-00000000, J-00000000</small></td>
   
			                     <td>{!!Form::text('cargo_accionista[]',$accionista->cargo_accionista,['class'=>'form-control','id'=>'cargo_accionista','onkeypress'=>'return soloLetras(event)','maxlength'=>'80'])!!}</td>


                           <td>{!!Form::text('telefono_accionista[]',$accionista->telefono_accionista,['class'=>'form-control','id'=>'telefono_accionista','onkeypress'=>'return soloNumeros(event)','maxlength'=>'11'])!!}<small>Ejemplo:02125554555</small></td>

                          <td>{!!Form::text('nacionalidad_accionista[]',$accionista->nacionalidad_accionista,['class'=>'form-control','id'=>'nacionalidad_accionista','onkeypress'=>'return soloLetras(event)','maxlength'=>'50'])!!}</td>

                           <td>{!!Form::text('participacion_accionista[]',$accionista->participacion_accionista,['class'=>'form-control','id'=>'participacion_accionista','onkeypress'=>'return soloNumerosletrasyPorcentaje(event)','maxlength'=>'10'])!!}</td>

                           <td>{!!Form::text('correo_accionista[]',$accionista->correo_accionista,['class'=>'form-control','id'=>'correo_accionista','onkeyup'=>"minuscula(this.value,'#email');",'maxlength'=>'80'])!!}</td>
 
                          <td> {!!Form::hidden('id[]',$accionista->id)!!}<button  name="remove" type="button" id="{{$key+1}}" value="" class="btn btn-danger btn-remove">x</button></td>
                        </tr> 
                        @endforeach
                    @endif
                  </table>
          </div>
      </div>
                  

                <div class="row">
                  <div class="col-md-12">
                    <div class="col-md-4"></div>
                    <div class="col-md-4 text-center">
                      <div class="form-group">
                         <a class="btn btn-primary" href="{{route('DatosEmpresa.index')}}" id="atras1">Atrás</a>
                        <input type="submit" class="btn btn-success" value="Guardar" name="">
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
	{{Form::close()}}
</div>



<script src="{{asset('js/ajax_v2.1.1.js')}}"></script>

<script type="text/javascript">

function CedulaFormat1(vCedulaName,mensaje,postab,escribo,evento) {

  //  var tipoPerson=document.getElementsByName('tipo_usuario');
    tecla=getkey(evento);
    vCedulaName.value=vCedulaName.value.toUpperCase();
    vCedulaValue=vCedulaName.value;
    valor=vCedulaValue.substring(2,12);
    var numeros='0123456789/';
    var digit;
    var noerror=true;
    if (shift && tam>1) {
    return false;
    }
    for (var s=0;s<valor.length;s++){
    digit=valor.substr(s,1);
    if (numeros.indexOf(digit)<0) {
    noerror=false;
    break;
    }
    }
    tam=vCedulaValue.length;
    if (escribo) {
    if ( tecla==8 || tecla==37) {
    if (tam>2)
    vCedulaName.value=vCedulaValue.substr(0,tam-1);
    else
    vCedulaName.value='';
    return false;
    }
    if (tam==0 && tecla==69) {
    vCedulaName.value='E-';
    return false;
    }
    if (tam==0 && tecla==69) {
    //vCedulaName.value='E-';
    return false;
    }


    if (tam==0 && tecla==86) {
    vCedulaName.value='V-';
    return false;
    }


        if (tam==0 && tecla==74) {
    vCedulaName.value='J-';
    return false;
    }
    if (tam==0 && tecla==71) {
    vCedulaName.value='G-';
    return false;
    }
    if (tam==0 && tecla==80) {
    vCedulaName.value='P-';
    return false;
    }

    else if ((tam==0 && ! (tecla<14 || tecla==69 || tecla==86 || tecla==46)))
    return false;
    else if ((tam>1) && !(tecla<14 || tecla==16 || tecla==46 || tecla==8 || (tecla >= 48 && tecla <= 57) || (tecla>=96 && tecla<=105)))
    return false;
    }

}

$(document).ready(function (){

var x = $('input[name="contador"]').val();
var i=parseInt(x);

      $('#add').click(function(){
       i++;



    $('#fields tr:last').after('<tr id="row'+i+'" display="block" class="show_div"><td>{!!Form::text('nombre_accionista[]',null,['class'=>'form-control ejem','onkeypress'=>'return soloLetras(event)','maxlength'=>'50'])!!}</td><td>{!!Form::text('apellido_accionista[]',null,['class'=>'form-control','onkeypress'=>'return soloLetras(event)','maxlength'=>'50'])!!}</td><td>{!!Form::text('cedula_accionista[]',null,['class'=>'form-control','id'=>'cedula_accionista','onkeypress'=>'return CedulaFormat1(this,"Cedula de Indentidad Invalida",-1,true,event)','maxlength'=>'15'])!!}<small>Ejemplo:V-00000000, J-00000000</small></td><td>{!!Form::text('cargo_accionista[]',null,['class'=>'form-control','onkeypress'=>'return soloLetras(event)','maxlength'=>'80'])!!}</td><td>{!!Form::text('telefono_accionista[]',null,['class'=>'form-control','id'=>'telefono_accionista','onkeypress'=>'return soloNumeros(event)','maxlength'=>'11'])!!}<small>Ejemplo:02125554555</small></td><td>{!!Form::text('nacionalidad_accionista[]',$accionista->nacionalidad_accionista,['class'=>'form-control','id'=>'nacionalidad_accionista','onkeypress'=>'return soloLetras(event)','maxlength'=>'50'])!!}</td><td>{!!Form::text('participacion_accionista[]',null,['class'=>'form-control','id'=>'participacion_accionista','onkeypress'=>'return soloNumerosletrasyPorcentaje(event)','maxlength'=>'50'])!!}</td><td>{!!Form::text('correo_accionista[]',null,['class'=>'form-control','id'=>'correo_accionista','onkeyup'=>"minuscula(this.value,'#email');",'maxlength'=>'80'])!!}</td><td>{!!Form::hidden('accionista_id',$id)!!}{!!Form::hidden('id[]',0)!!}<button  name="remove" type="button" id="'+i+'" value="" class="btn btn-danger btn-remove">x</button></td></tr>');

    });   


     $(document).on('click','.btn-remove',function(){
        var id_boton= $(this).attr("id");
        var id=$(this).prev().val();

    if(id!=0){

         if (confirm('Deseas eliminar este Acionista?')) {

      $.ajax({

       'type':'get',
       'url':'/eliminarAccionista',
       'data':{ 'id':id},
       success: function(data){

        $('#row'+id_boton).remove();

         }

       });

       }



     }else{

      $('#row'+id_boton).remove();
     }

     });


});

</script>
@stop
