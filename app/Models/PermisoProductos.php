<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PermisoProductos extends Model
{
    protected $table = 'permiso_productos';


	protected $fillable = [

           'permiso_id',
           'codigo',
           'descripcion',
           'descrip_comercial',
           'cantidad',
           'gen_unidad_medida_id',
           'bactivo',
    ];

    protected $date =['created_at','updated_at'];

    public function permiso() {

     return $this->hasOne('App\Models\Permiso','id','permiso_id');
    }
    
}