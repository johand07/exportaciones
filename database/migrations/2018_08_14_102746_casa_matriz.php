<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CasaMatriz extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('casa_matriz', function (Blueprint $table) {
           $table->engine = 'InnoDB';
           $table->increments('id');
           $table->unsignedInteger('gen_usuario_id')->index()->nullable();
           $table->foreign('gen_usuario_id')->references('id')->on('gen_usuario');
           $table->string('casa_matriz', 20)->nullable();
           $table->unsignedInteger('pais_id')->index()->nullable();//Se agrego el pais de ult exportacion
           $table->foreign('pais_id')->references('id')->on('pais');
           $table->boolean('bactivo')->default(1);
           $table->timestamps();
        });   
    }
    

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('casa_matriz');
    }
}
