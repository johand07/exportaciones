<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class GenTipoUsuario extends Model
{
    protected $table = 'gen_tipo_usuario';


    protected $fillable = [

        'nombre_tipo_usu','descripcion_tipo_usu','bactivo'
    ];



    public function Usuario()
   {

       return $this->hasOne('App\Models\GenUsuario','id_gen_tipo_usu');

   }

}
