<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PlanillaDjir_01 extends Model
{
    protected $table = 'planilla_djir01';
    
    protected $fillable = [
        'gen_declaracion_inversion_id',
        'gen_status_id',
        'grupo_emp_internacional',
        'razon_social_solicitante',
        'rif_solicitante',
        'finicio_op',
        'fingreso_inversion',
        'emp_privada',
        'emp_publica',
        'emp_mixto',
        'emp_otro',
        'especifique_otro',
        'registro_publico',
        'estado_emp',
        'municipio_emp',
        'numero_r_mercantil',
        'folio',
        'tomo',
        'finspeccion',
        'direccion_emp',
        'tlf_emp',
        'pagina_web_emp',
        'correo_emp',
        'rrss',
        'emple_actual_directo',
        'emple_actual_indirecto',
        'emple_generar_directo',
        'emple_generar_indirecto',
        'capital_suscrito',
        'capital_apagado',
        'num_acciones_emitidas',
        'valor_nominal_accion',
        'fmodificacion_accionaria',
        'finscripcion',
        'nombre_repre',
        'apellido_repre',
        'numero_doc_identidad',
        'numero_pasaporte_repre',
        'cod_postal_repre',
        'rif_repre',
        'telefono_1',
        'telefono_2',
        'correo_repre',
        'direccion_repre',
        'estado_observacion',
        'descripcion_observacion',
        
    ];

    public function genDecInversion()
	{
        return $this->hasOne('App\Models\GenDeclaracionInversion','id','gen_declaracion_inversion_id');
    }

    public function genStatus()
	{
        return $this->hasOne('App\Models\GenStatus','id','gen_status_id');
    }

     public function estado()
    {
        return $this->hasOne('App\Models\Estado','id','estado_emp');
    }

     public function municipio()
    {
        return $this->hasOne('App\Models\Municipio','id','municipio_emp');
    }
}
