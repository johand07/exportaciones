<?php

namespace App\Http\Requests;
use App\Models\GenFactura;

use Illuminate\Foundation\Http\FormRequest;

class FacturaSolicitudRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()

    {
       /*$data=GenFactura::whereIn('id',$this->gen_factura_id)->get();
       $dua=[];
       $id="";

          foreach ($data as $key => $value) {

            if(empty($dua)) {

               $dua=$value->gen_dua_id;
               $id=0;
            }else{

               if($dua!=$value->gen_dua_id)
                  {
                    $id=1;
                 }else{
                   $id=0;
                 }
            }

         }
*/


        return [

             'gen_factura_id'=>'required|unique:factura_solicitud,gen_factura_id',
        ];

       //}
    }
}
