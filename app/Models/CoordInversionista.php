<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CoordInversionista extends Model
{
    protected $table = 'bandeja_coord_sol_inversion';

    protected $fillable = [

       'bandeja_sol_inversionista_id',
       'gen_usuario_id',
       'gen_status_id',
       'fstatus',
       'bactivo',
    ];

   ///Relacion con bandejasolinversionista
    public function rbandejainversionista(){
    
    return $this->hasMany('App\Models\BandejaSolInversionista','id','bandeja_sol_inversionista_id');

   }

   public function rusuario(){

    return $this->hasOne('App\Models\GenUsuario','id','gen_usuario_id');

   }

   public function restatus(){
    
   return $this->hasOne('App\Models\GenStatus','id','gen_status_id');

   }

}
