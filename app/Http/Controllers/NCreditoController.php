<?php

namespace App\Http\Controllers;

use App\Models\GenNotaCredito;
use App\Models\GenConsignatario;
use App\Models\GenFactura;
use App\Http\Requests\GenNotaCreditoRequest;
use App\Http\Requests\FinanciamientoSolicitud;
use Session;
use Auth;
use Laracasts\Flash\Flash;
use Alert;
use Illuminate\Http\Request;

class NCreditoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $titulo="Nota de Crédito";
        $descripcion="Nota de Crédito Registradas";
        $ncredito=GenNotaCredito::where('gen_usuario_id',Auth::user()->id)->where('bactivo',1)->get();
        return view('NCredito.index', compact('titulo','descripcion','ncredito'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       $titulo="Nota de Crédito";
       $descripcion="Registro de Nota de Crédito";
       $consig=GenConsignatario::where('gen_usuario_id',Auth::user()->id)->where('bactivo',1)->pluck('nombre_consignatario','id');
       $factura=GenFactura::where('gen_usuario_id',Auth::user()->id)->pluck('numero_factura','id');
       return view('NCredito.create', compact('titulo','descripcion','consig','factura'));
    }

    public function facturaNC(Request $request){
        //dd($request->gen_factura_id);
        $facturand = GenNotaCredito::where('gen_factura_id',$request->gen_factura_id)->first();

        if (!empty($facturand)) {
          return 1;
        } else {
          return 2;
        }
        

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(GenNotaCreditoRequest $request)
    {
        $fecha_actual = date("d-m-Y");
        if ((strtotime($request->femision)) > (strtotime($fecha_actual))) {
           Alert::warning('No puede ser superior a la Fecha Actual', 'Error en la Fecha de Emisión !')->persistent("OK");
        return redirect()->action('NCreditoController@create');
        } else {
           GenNotaCredito::create($request->all());
           //dd($request->all());
           Alert::success('REGISTRO EXITOSO','Nota de Crédito agregada')->persistent("Ok");
           return redirect()->action('NCreditoController@index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\GenNotaCredito  $genNotaCredito
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,GenNotaCredito $genNotaCredito,$id)
    {   
        //dd($id);
        $gen_factura=GenFactura::select('monto_fob')->where('id',$request->gen_factura_id)->first();
        //dd($gen_factura);
        return response()->json($gen_factura);
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\GenNotaCredito  $genNotaCredito
     * @return \Illuminate\Http\Response
     */
    public function edit(GenNotaCredito $genNotaCredito, $id)
    {
        $titulo="Editar Nota de Crédito";
        $descripcion="Editar Datos";
        $notacredito=GenNotaCredito::find($id);
        $consig=GenConsignatario::where('gen_usuario_id',Auth::user()->id)->where('bactivo',1)->pluck('nombre_consignatario','id');
        $factura=GenFactura::where('gen_usuario_id',Auth::user()->id)->pluck('numero_factura','id');
        return view('NCredito.edit', compact('titulo','descripcion','notacredito','consig','factura'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\GenNotaCredito  $genNotaCredito
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, GenNotaCredito $genNotaCredito, $id)
    {
       $fecha_actual = date("d-m-Y");
        if ((strtotime($request->femision)) > (strtotime($fecha_actual))) {
           Alert::warning('No puede ser superior a la Fecha Actual', 'Error en la Fecha de Emisión !')->persistent("OK");        
          return redirect('exportador/NCredito/'.$id.'/edit');
        } else {
           GenNotaCredito::find($id)->update($request->all());
           //dd($request->all());
           Alert::success('Actualización Exitosa!', 'Actulización Exitosa.')->persistent("OK");
           return redirect()->action('NCreditoController@index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\GenNotaCredito  $genNotaCredito
     * @return \Illuminate\Http\Response
     */
    public function destroy(GenNotaCredito $genNotaCredito)
    {
       //
    }
}
