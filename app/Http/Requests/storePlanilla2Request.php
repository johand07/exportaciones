<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class storePlanilla2Request extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            
            'grupo_emp_internacional'               =>'required',
            'emp_relacionadas_internacional'        =>'required_if:grupo_emp_internacional,1',
            'casa_matriz'                           =>'required_if:grupo_emp_internacional,1',
            'emp_repre_casa_matriz'                 =>'required_if:grupo_emp_internacional,1',
            'afiliados_internacional'               =>'required_if:grupo_emp_internacional,1',
            'sub_emp_extranjero'                    =>'required_if:grupo_emp_internacional,1',

            'grupo_emp_nacional'                    =>'required',
            'emp_haldig_coorp'                      =>'required_if:grupo_emp_nacional,1',
            'afiliados_nacional'                    =>'required_if:grupo_emp_nacional,1',
            'sub_locales'                           =>'required_if:grupo_emp_nacional,1',

            'present_doc_repre_legal'               =>'required',
            'num_apostilla'                         =>'required_if:present_doc_repre_legal,1',
            'fecha_apostilla'                       =>'required_if:present_doc_repre_legal,1',
            'pais'                                  =>'required_if:present_doc_repre_legal,1',
            'estado'                                =>'required_if:present_doc_repre_legal,1',
            'autoridad_apostilla'                   =>'required_if:present_doc_repre_legal,1',
            'cargo'                                 =>'required_if:present_doc_repre_legal,1',
            'traductor'                             =>'required_if:present_doc_repre_legal,1',
            'datos_adicionales'                     =>'required_if:present_doc_repre_legal,1',

            'ingresos_anual_ult_ejer'               =>'required',
            'egresos_anual_ult_ejer'                =>'required',
            'total_balance_ult_ejer'                =>'required',
            'anio_informacion_financiera'           =>'required',
            'moneda_extranjera'                     =>'required',
            'utilidades_reinvertidas'               =>'required',
            'credito_casa_matriz'                   =>'required',
            'tierras_terrenos'                      =>'required',
            'edificios_construcciones'              =>'required',
            'maquinarias_eqp_herra'                 =>'required',
            'muebles_enceres'                       =>'required',
            'otros_activos_tangibles'               =>'required',
            'software'                              =>'required',
            'derecho_prop_intelectual'              =>'required',
            'eqp_transporte'                        =>'required',
            'contribuciones_tecno'                  =>'required',
            'otros_activos_intangibles'             =>'required',
            'total_costos_declaracion'              =>'required',
            'tipo_inversion'                        =>'required',
            'invert_divisa_cambio'                  =>'required',
            'bienes_cap_fisico_tangibles'           =>'required',
            'bienes_inmateriales_intangibles'       =>'required',
            'reinversiones_utilidades'              =>'required',
            'especifique_otro'                      =>'required',
            //'valor_libros'                          =>'required',
            //'valor_neto_activos'                    =>'required',
            //'valor_emp_similar'                     =>'required',
            'otro_especifique'                      =>'required',
            'total_modalidad_inv'                   =>'required',
            //'estado_observacion'                    =>'required',
            //'descrip_observacion'                   =>'required',
        ];
    }
}
