<?php

namespace App\Http\Controllers;

use App\Models\DetProdTecnologia;
use App\Models\GenUnidadMedida;
use App\Models\FacturaSolicitud;
use App\Models\GenFactura;
use App\Models\DetProdFactura;
use App\Http\Requests\ProdTecnoRequest;
use Session;
use Auth;
use Laracasts\Flash\Flash;
use Alert;
use Illuminate\Http\Request;

class DetProdTecnologiaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $medidas=GenUnidadMedida::all()->pluck('dunidad','id');

        $monto_fob=GenFactura::where('gen_usuario_id',Auth::user()->id)->orderBy('id','desc')->take(1)->first();
        $monto=$monto_fob->monto_fob;


        $titulo="Productos-Tecnología";
        $descripcion="Registrar Productos";

        return view('productos_tecno.create',compact('medidas','titulo','descripcion','monto'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProdTecnoRequest $request)
    {

     
    
      for($i=0;$i<count($request->descripcion);$i++)
         {
           $insert2=DetProdTecnologia::create([
          'gen_factura_id'=>Session::get('factura_id'),
          'descripcion'=>$request->descripcion[$i],
          'cantidad'=>$request->cantidad[$i],
          'unidad_medida_id'=>$request->unidad_medida_id[$i],
          'precio'=>$request->precio[$i],
          'valor_fob'=>$request->valor_fob[$i],
           ]);
         }

        Alert::success('Registro Exitoso!')->persistent("Ok");
        return redirect("exportador/factura");

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\DetProdTecnologia  $detProdTecnologia
     * @return \Illuminate\Http\Response
     */
    public function show(DetProdTecnologia $detProdTecnologia)
    {

       $data=FacturaSolicitud::select('gen_factura_id')->where('gen_solicitud_id',Session::get('gen_solictud_id'))->get();

         if(count($data) > 1){

          $tipo_solicitud=GenFactura::find($data->first()->gen_factura_id);

            if($tipo_solicitud->tipo_solicitud_id==1) {


                  $id_solicitud=array();

                   foreach ($data as $value) {

                       $id_solicitud=$value->gen_factura_id;
                     }


                  $productos=DetProdFactura::whereIn('gen_factura_id',$id_solicitud)->get();




               }else{

                  $productos=DetProdTecnologia::whereIn('gen_factura_id',$id_solicitud)->get();

            }



         }else{


                 $tipo_solicitud=GenFactura::find($data[0]->gen_factura_id);

                 if($tipo_solicitud->tipo_solicitud_id==1){

                  $productos=DetProdFactura::where('gen_factura_id',$data[0]->gen_factura_id)->get();

                }else{

                  $productos=DetProdTecnologia::where('gen_factura_id',$data[0]->gen_factura_id)->get();

                }


         }


      return view('productos_tecno.show',compact('productos'));




    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\DetProdTecnologia  $detProdTecnologia
     * @return \Illuminate\Http\Response
     */
    public function edit(DetProdTecnologia $request,$id)
    {
       $titulo="Productos-Tecnología";
       $descripcion="Aviso: Las sumatorias de los valores FOB no debe ser mayor al monto FOB registrado. Si no se cumple con esta condición no podrá registrar los productos.";
       $productos_tecno=DetProdTecnologia::where('gen_factura_id',$id)->get();
       $medidas=GenUnidadMedida::all()->pluck('dunidad','id');

       $monto_fob=GenFactura::where('gen_usuario_id',Auth::user()->id)->where('id',$id)->orderBy('id','desc')->take(1)->first();
       $monto=$monto_fob->monto_fob;


       Session::put('factura_tecno_id',$id);


       return view('productos_tecno.edit',compact('id','productos_tecno','medidas','titulo','descripcion','monto'));


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\DetProdTecnologia  $detProdTecnologia
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

    // dd($request->all());
     
     $cont=count($request->descripcion);
        for($i=0;$i<=($cont-1);$i++){

        DetProdTecnologia::updateOrCreate(
        ['id'=>$request->id[$i]],
        ['gen_factura_id'=>Session::get('factura_tecno_id'),
         'descripcion'=>$request->descripcion[$i],
         'cantidad'=>$request->cantidad[$i],
         'unidad_medida_id'=>$request->unidad_medida_id[$i],
         'precio'=>$request->precio[$i],
         'valor_fob'=>$request->valor_fob[$i],
         'bactivo'=>1]

        );

   }
       Alert::success('Actualización Exitosa!')->persistent("Ok");
       return redirect("exportador/factura");


    }

    public function eliminar($id)
    {

       $delete=DetProdTecnologia::find($_GET['id'])->delete();
       if($delete)
       {
          return 1;
       }else
       {
         return 0;
       }

    }

    public function formateo($data){

       $a=[];

        foreach ($data as $key => $value) {
     
           if(strpos($value,'.') && strpos($value,',')){

              $a[$key]=str_replace(',','.',str_replace('.','',$value));

           }elseif(strpos($value,',')){

               $a[$key]=str_replace(',', '.',$value);
           
           }else{
 
              $a[$key]=$value;

           }
       }

       return $a;

    }

    public function guardarEr(Request $request){


           dd($request->all());

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\DetProdTecnologia  $detProdTecnologia
     * @return \Illuminate\Http\Response
     */
    public function destroy(DetProdTecnologia $detProdTecnologia)
    {
        //
    }
}
