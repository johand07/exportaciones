@extends('templates/layoutlte_analist_djo')
@section('content')
<div class="content" style="background-color: #fff">
    <div class="panel-group" style="background-color: #fff;">
      <div class="panel-primary">
        <div class="panel-heading"><h3>Solicitudes de Certificado de Origen</h3> </div>
        <div class="panel-body">
  
            <div class="row">
              <div class="col-md-12">
                <div class="row">
                  <div class="col-md-8">
                    <h4 class="text-info">Bandeja de Entrada</h4><hr>
                  </div>
                  <div class="col-md-2">
                  </div>
                  <div class="col-md-2">
                    <a class="btn btn-primary" href="{{route('AnalistaCertificadoOrigen.index')}}">
                      <span class="glyphicon glyphicon-refresh" title="Agregar" aria-hidden="true"></span>
                        Actualizar
                    </a>
                  </div>
                </div>
                <table id="listaUsuarios" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                        <tr class="text-center">
                        	<th class="col-md-1 text-center" >Nº</th>
                            <th class="col-md-2 text-center">Nº de Certificado</th>                     
                            <th class="col-md-2 text-center">Nombre Certificado</th>                     
                            <th class="col-md-3 text-center">Fecha de Solicitud</th> 
                            <th class="col-md-1 text-center">Estatus</th>                                 
                            <th class="col-md-2 text-center">Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                      @foreach($certificado as $key =>  $certi)
                            <tr >           
                            	<td  class="col-md-1 text-center" id=""><b></b>{{ $key + 1 }}</td>          
                                <td class="col-md-2 text-center"><b class="text-primary">{{$certi->num_certificado}}</b></td>
                               <td class="col-md-2 text-center"><b class="text-primary">{{$certi->GenTipoCertificado->nombre_certificado}}</b></td>
                                <td class="col-md-3 text-center"><b></b>{{ date ('d-m-Y',strtotime($certi->created_at))}}</td>
                                <td class="col-md-2 text-center"><b>
                                        @if($certi->gen_status_id==10)
                                          <i class="glyphicon glyphicon-user"></i> ¡Atendida  por el Analista!
                                        @elseif($certi->gen_status_id==11)
                                          <i class="glyphicon glyphicon-user"></i> ¡En Observación!
                                        @elseif($certi->gen_status_id==12)
                                          <i class="glyphicon glyphicon-ok"></i> ¡Verificado!
                                        @elseif($certi->gen_status_id==18)
                                          <i class="glyphicon glyphicon-ok"></i> Corregida por el usuario!
                                        @elseif($certi->gen_status_id==13)
                                          <i class="glyphicon glyphicon-remove"></i> ¡Solicitud Anulada!
                                        @elseif($certi->gen_status_id==14)
                                          <i class="glyphicon glyphicon-user"></i> ¡Atendida por el Coordinador!
                                          @elseif($certi->gen_status_id==16)
                                          <i class="glyphicon glyphicon-user"></i> ¡Aprobada!
                                          @else
                                            <i class="glyphicon glyphicon-user"></i> Por Atender 
                                        @endif
                                  </b>
                                </td>
                                <td class="col-md-2 text-center"">

                                     @if($certi->gen_status_id==10)
                                              <a href="{{route('AnalistaCertificadoOrigen.edit',$certi->id)}}" class="text-center btn btn-sm btn-success list-inline"><i class="glyphicon glyphicon-share"></i><b>Atendida</b></a>
                                          @elseif($certi->gen_status_id==11)
                                          <a href="{{route('AnalistaCertificadoOrigen.edit',$certi->id)}}" class="text-center btn btn-sm btn-success list-inline"><i class="glyphicon glyphicon-pencil"></i></a>
                                          @elseif($certi->gen_status_id==14)
                                          @elseif($certi->gen_status_id==16)
                                          @elseif($certi->gen_status_id==18)
                                              <a href="{{route('AnalistaCertificadoOrigen.edit',$certi->id)}}" class="text-center btn btn-sm btn-warning list-inline"><i class="glyphicon glyphicon-pencil"></i><b>Corregido por usuario</b></a>
                                          @elseif($certi->gen_status_id==12)
                                              <a class="text-center btn btn-sm btn-primary list-inline"><i class="glyphicon glyphicon-ok"></i><b>Verificado</b></a>
                                           @elseif($certi->gen_status_id==13)
                                              <i class="glyphicon glyphicon-remove"></i><b>Anulada</b>
                                          @else
                                              <button class="text-center btn btn-sm btn-success list-inline" onclick="confirmView({{$certi->id}})"><i class="glyphicon glyphicon-pencil"></i><b> Atender</b> </button>
                                          @endif 
                                </td>
                              </tr>              
            				     @endforeach
                    </tbody>
                </table>
              </div>
              
            </div>
        </div>
      </div>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="{{asset('js/ajax_v2.1.1.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.2/moment.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.2/locale/es.js"></script>

<script>
$(document).ready(function() {

  confirmView = function(idcert) {

    $(document).on('click', '.SwalBtn1', function() {
        //Some code 1
        window.location.href = "AnalistaCertificadoOrigen/"+idcert+"";
    });
    $(document).on('click', '.SwalBtn2', function() {
        //Some code 2
        window.location.href = "AnalistaCertificadoOrigen/"+idcert+"/edit";
    });
    
    swal({
      title: "¿Qué deseas realizar con esta solicitud?",
      html: true,
      text: '<button type="button" role="button" class="SwalBtn1 text-center btn btn-sm btn-primary list-inline" style="margin-right:5px">' + 'Visualizar' + '</button> ' +
            '<button type="button" role="button" class="SwalBtn2 text-center btn btn-sm btn-success list-inline">' + 'Atender' + '</button>',
      type: "warning",
      showConfirmButton: false,
      showCancelButton: false
    });
  }

});


</script>



@stop
