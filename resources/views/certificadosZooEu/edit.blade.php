@extends('templates/layoutlte')
@section('content')

@php $contador=count($DescripPartidaZoo); @endphp
{{Form::hidden('metodo','Edit')}}
{{Form::hidden('contador',$contador)}}


{{Form::model($genCertZooEu,['route'=>['certificadosZooEu.update',$genCertZooEu->id],'method'=>'PATCH','id'=>'forInversionista']) }}
<div class="content">
  <div class="panel panel-primary">
    <div class="panel-primary" style=" #fff">
      <div class="panel-heading"><h4>Expedidor/Exportador</h4></div>
        <div class="panel-body">
       		<div class="row"><br>
				<div class="col-md-6">
					<div class="form-group">
						{!! Form::label('','País Exportador') !!}
						 {!! Form::select('pais_export',$pais,null,['class'=>'form-control', 'id'=>'pais_export','placeholder'=>'Seleccione un País']) !!}
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						{!! Form::label('','Código Iso del país ') !!}
						{!! Form::text('cod_iso_pais_export',null,['class'=>'form-control','id'=>'cod_iso_pais_export']) !!}
					</div>
				</div>
			</div>
        </div>
      </div> 
    
       <div class="panel-heading">
        <div class="row">
          <div class="col-md-12">
           <h4>Destinatario/Importador</h4>
          </div>
        </div>
       </div>
       <div class="panel-body">
        <div class="row">
        		<div class="col-md-6">
		        	<div class="form-group">
		             {!! Form::label('','Nombre Importador') !!}
		              {!! Form::text('nombre_import',null,['class'=>'form-control','id'=>'nombre_import','onkeypress'=>'return soloLetras(event)'])!!}
		            </div> 
        		</div>
        		<div class="col-md-6">
		        	<div class="form-group">
		             {!! Form::label('','Dirección Importador') !!}
		              {!! Form::text('direccion_import',null,['class'=>'form-control','id'=>'direccion_import'])!!}
		            </div> 
        		</div>
        </div>
        <div class="row">
        		<div class="col-md-6">
		        	<div class="form-group">
		             {!! Form::label('','País Importador') !!}
		             {!! Form::select('pais_import',$pais,null,['class'=>'form-control', 'id'=>'pais_import','placeholder'=>'Seleccione un País']) !!}
		            </div> 
        		</div>
        		<div class="col-md-6">
		        	<div class="form-group">
		             {!! Form::label('','Código Iso del país ') !!}
						{!! Form::text('cod_iso_pais_inport',null,['class'=>'form-control','id'=>'cod_iso_pais_inport']) !!}
		            </div> 
        		</div>
        </div>
        </div>

    <br>
    <div class="panel-heading"><h4>Operador responsable de la partida </h4></div>
    <div class="panel-body">
       <div class="row">
        		<div class="col-md-6">
		        	<div class="form-group">
		             {!! Form::label('','Nombre') !!}
		              {!! Form::text('nombre_oper_respons',null,['class'=>'form-control','id'=>'nombre_oper_respons','onkeypress'=>'return soloLetras(event)'])!!}
		            </div> 
        		</div>
        		<div class="col-md-6">
		        	<div class="form-group">
		             {!! Form::label('','Dirección') !!}
		              {!! Form::text('direccion_oper_respons',null,['class'=>'form-control','id'=>'direccion_oper_respons'])!!}
		            </div> 
        		</div>
        </div>
        <div class="row">
        		<div class="col-md-6">
		        	<div class="form-group">
		             {!! Form::label('','País') !!}
		             {!! Form::select('pais_oper_respons',$pais,null,['class'=>'form-control', 'id'=>'pais_oper_respons','placeholder'=>'Seleccione un País']) !!}
		            </div> 
        		</div>
        		<div class="col-md-6">
		        	<div class="form-group">
		             {!! Form::label('','Código Iso del país ') !!}
						{!! Form::text('cod_iso_pais_oper_respons',null,['class'=>'form-control','id'=>'cod_iso_pais_oper_respons']) !!}
		            </div> 
        		</div>
        </div>
      
    </div>
    <div class="panel-heading"><h4></h4></div>
    <div class="panel-body">
       <div class="row">
        		<div class="col-md-6">
		        	<div class="form-group">
		             {!! Form::label('','País de Origen') !!}
		             {!! Form::select('pais_origen',$pais,null,['class'=>'form-control', 'id'=>'pais_origen','placeholder'=>'Seleccione un País']) !!}
		            </div> 
        		</div>
        		<div class="col-md-6">
		        	<div class="form-group">
		            {!! Form::label('','Código Iso del país ') !!}
						{!! Form::text('cod_iso_pais_origen',null,['class'=>'form-control','id'=>'cod_iso_pais_origen']) !!}
		            </div> 
        		</div>
        </div>
        <div class="row">
        		<div class="col-md-6">
		        	<div class="form-group">
		             {!! Form::label('','País Destino') !!}
		             {!! Form::select('pais_destino',$pais,null,['class'=>'form-control', 'id'=>'pais_destino','placeholder'=>'Seleccione un País']) !!}
		            </div> 
        		</div>
        		<div class="col-md-6">
		        	<div class="form-group">
		             {!! Form::label('','Código Iso del país ') !!}
						{!! Form::text('cod_iso_pais_destino',null,['class'=>'form-control','id'=>'cod_iso_pais_destino']) !!}
		            </div> 
        		</div>
        </div>

         <div class="row">
        		<div class="col-md-6">
		        	<div class="form-group">
		             {!! Form::label('','Región de Origen') !!}
		             {!! Form::text('region_origen',null,['class'=>'form-control','id'=>'region_origen']) !!}
		            </div> 
        		</div>
        		<div class="col-md-6">
		        	<div class="form-group">
		             {!! Form::label('','Código') !!}
						{!! Form::text('cod_reg_origen',null,['class'=>'form-control','id'=>'cod_reg_origen']) !!}
		            </div> 
        		</div>
        </div>

         <div class="row">
        		<div class="col-md-6">
		        	<div class="form-group">
		             {!! Form::label('','Región de Destino') !!}
		             {!! Form::text('region_destino',null,['class'=>'form-control','id'=>'region_destino']) !!}
		            </div> 
        		</div>
        		<div class="col-md-6">
		        	<div class="form-group">
		             {!! Form::label('','Código') !!}
						{!! Form::text('cod_reg_dest',null,['class'=>'form-control','id'=>'cod_reg_dest']) !!}
		            </div> 
        		</div>
        </div>
      
    </div>
    <div class="panel-heading"><h4>Lugar de Expedición</h4></div>
    <div class="panel-body">
       <div class="row">
        		<div class="col-md-6">
		        	<div class="form-group">
		             {!! Form::label('','Nombre') !!}
		             {!! Form::text('nombre_lug_exp',null,['class'=>'form-control','id'=>'nombre_lug_exp']) !!}
		            </div> 
        		</div>
        		<div class="col-md-6">
		        	<div class="form-group">
		            {!! Form::label('','Número de Registro/Autorizacion') !!}
						{!! Form::text('num_reg_exp',null,['class'=>'form-control','id'=>'num_reg_exp']) !!}
		            </div> 
        		</div>
        </div>
         <div class="row">
        		<div class="col-md-12">
		        	<div class="form-group">
		             {!! Form::label('','Dirección') !!}
		             {!! Form::text('dir_lug_exp',null,['class'=>'form-control','id'=>'dir_lug_exp']) !!}
		            </div> 
        		</div>
        </div>
        <div class="row">
        		<div class="col-md-6">
		        	<div class="form-group">
		             {!! Form::label('','País') !!}
		             {!! Form::select('pais_lug_exp',$pais,null,['class'=>'form-control', 'id'=>'pais_lug_exp','placeholder'=>'Seleccione un País']) !!}
		            </div> 
        		</div>
        		<div class="col-md-6">
		        	<div class="form-group">
		             {!! Form::label('','Código Iso del país ') !!}
						{!! Form::text('cod_iso_lug_exp',null,['class'=>'form-control','id'=>'cod_iso_lug_exp']) !!}
		            </div> 
        		</div>
        </div>
      
    </div>

    <div class="panel-heading"><h4>Lugar de Destino</h4></div>
    <div class="panel-body">
       <div class="row">
        		<div class="col-md-6">
		        	<div class="form-group">
		             {!! Form::label('','Nombre') !!}
		             {!! Form::text('nombre_lug_dest',null,['class'=>'form-control','id'=>'nombre_lug_dest']) !!}
		            </div> 
        		</div>
        		<div class="col-md-6">
		        	<div class="form-group">
		            {!! Form::label('','Número de Registro/Autorizacion') !!}
						{!! Form::text('num_reg_lug_dest',null,['class'=>'form-control','id'=>'num_reg_lug_dest']) !!}
		            </div> 
        		</div>
        </div>
         <div class="row">
        		<div class="col-md-12">
		        	<div class="form-group">
		             {!! Form::label('','Dirección') !!}
		             {!! Form::text('dir_lug_dest',null,['class'=>'form-control','id'=>'dir_lug_dest']) !!}
		            </div> 
        		</div>
        </div>
        <div class="row">
        		<div class="col-md-6">
		        	<div class="form-group">
		             {!! Form::label('','País') !!}
		             {!! Form::select('pais_lug_dest',$pais,null,['class'=>'form-control', 'id'=>'pais_lug_dest','placeholder'=>'Seleccione un País']) !!}
		            </div> 
        		</div>
        		<div class="col-md-6">
		        	<div class="form-group">
		             {!! Form::label('','Código Iso del país ') !!}
						{!! Form::text('cod_iso_lug_dest',null,['class'=>'form-control','id'=>'cod_iso_lug_dest']) !!}
		            </div> 
        		</div>
        </div>
      
    </div>

      <div class="panel-heading"><h4></h4></div>
       <div class="panel-body">
          <div class="row">
          	<div class="col-md-6">
		        	<div class="form-group">
		             {!! Form::label('','Lugar de Carga') !!}
						{!! Form::text('lug_carga',null,['class'=>'form-control','id'=>'lug_carga']) !!}
		            </div> 
        		</div>
        		<div class="col-md-6">
		        	  <div class="form-group">
	            	{!! Form::label('','Fecha y hora de Salida') !!}
	            	<div class="input-group date">
	            		{!! Form::text('fecha_hora_salida',null,['class'=>'form-control','readonly'=>'readonly','id'=>'fecha_hora_salida']) !!}
	            		<span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
	            	</div>
	            </div>
        	</div>
          </div>
           <div class="row">
          	<div class="col-md-4">
		        	<div class="form-group">
		             {!! Form::label('','Medio de Transporte') !!}
		             {!! Form::select('cat_medio_transport_id',$catMediotrans,null,['class'=>'form-control', 'id'=>'cat_medio_transport_id','placeholder'=>'Seleccione un País']) !!}
		            </div> 
        	</div>
        	<div class="col-md-4">
		        	<div class="form-group">
		             {!! Form::label('','Identificación') !!}
		             {!! Form::text('ident_medio_transport',null,['class'=>'form-control','id'=>'ident_medio_transport']) !!}
		            </div> 
        	</div>
        	<div class="col-md-4">
		        	<div class="form-group">
		             {!! Form::label('','Puesto de control fronterizo de entrada') !!}
		             {!! Form::text('puest_control_front',null,['class'=>'form-control','id'=>'puest_control_front']) !!}
		            </div> 
        	</div>
          </div>
       </div>

        <div class="panel-heading"><h4>Documentos de Acompañamiento</h4></div>
    <div class="panel-body">
       <div class="row">
        		<div class="col-md-6">
		        	<div class="form-group">
		             {!! Form::label('','Tipo') !!}
		              {!! Form::text('tipo_doc_acomp',null,['class'=>'form-control','id'=>'tipo_doc_acomp','onkeypress'=>'return soloLetras(event)'])!!}
		            </div> 
        		</div>
        		<div class="col-md-6">
		        	<div class="form-group">
		             {!! Form::label('','Código') !!}
		              {!! Form::text('cod_doc_acomp',null,['class'=>'form-control','id'=>'cod_doc_acomp'])!!}
		            </div> 
        		</div>
        </div>
        <div class="row">
        		<div class="col-md-6">
		        	<div class="form-group">
		             {!! Form::label('','País') !!}
		             {!! Form::select('pais_doc_acomp',$pais,null,['class'=>'form-control', 'id'=>'pais_doc_acomp','placeholder'=>'Seleccione un País']) !!}
		            </div> 
        		</div>
        		<div class="col-md-6">
		        	<div class="form-group">
		             {!! Form::label('','Código Iso del país ') !!}
						{!! Form::text('cod_iso_doc_acomp',null,['class'=>'form-control','id'=>'cod_iso_doc_acomp']) !!}
		            </div> 
        		</div>
        </div>
           <div class="row">
        		<div class="col-md-12">
		        	<div class="form-group">
		             {!! Form::label('','Referencia del documento comercial') !!}
		             {!! Form::text('ref_doc_comer',null,['class'=>'form-control','id'=>'ref_doc_comer']) !!}
		            </div> 
        		</div>
        </div>
      
    </div>

    <div class="panel-heading"><h4>Condiciones de Transporte</h4></div>
    <div class="panel-body">
       <div class="row">
        		<div class="col-md-6">
		        	<div class="form-group">
		             {!! Form::label('','Condiciones de Transporte') !!}
		             {!! Form::select('cat_cond_transport_id',$catCondTrans,null,['class'=>'form-control', 'id'=>'cat_cond_transport_id','placeholder'=>'Seleccione una Condiciones de Transporte']) !!}
		            </div> 
        		</div>
        </div>
    </div>

    <div class="panel-heading"><h4>Número de recipiente/Número de precinto</h4></div>
    <div class="panel-body">
       <div class="row">
        		<div class="col-md-6">
		        	<div class="form-group">
		             {!! Form::label('','Número del recipiente') !!}
		             {!! Form::number('num_recipiente',null,['class'=>'form-control','id'=>'num_recipiente','onkeypress'=>'return soloNumeros(event)']) !!}
		            </div> 
        		</div>
        		<div class="col-md-6">
		        	<div class="form-group">
		             {!! Form::label('','Número del precinto') !!}
		             {!! Form::number('num_precinto',null,['class'=>'form-control','id'=>'num_precinto','onkeypress'=>'return soloNumeros(event)']) !!}
		            </div> 
        		</div>
        </div>
    </div>

     <div class="panel-heading"><h4>Certificados como o a efectos de</h4></div>
    <div class="panel-body">
       <div class="row">
        		<div class="col-md-12">
		        	<div class="form-group">
		            {!! Form::label('','Certificados como o a efectos de:') !!}
		             {!! Form::select('cat_certificado_efectos_de_id',$catCertEfect,null,['class'=>'form-control', 'id'=>'cat_certificado_efectos_de_id','placeholder'=>'Seleccione']) !!}
		            </div> 
        		</div>
        </div>
    </div>

    <div class="panel-heading"><h4></h4></div>
    <div class="panel-body">
       <div class="row">
        		<div class="col-md-6">
		        	<div class="form-group">
		            {!!Form::checkbox('para_merc_interior','para_merc_interior',1,['class'=>'checkbox-inline','id'=>'para_merc_interior'])!!}
		            {!! Form::label('','Para el mercado interior') !!}
		            </div> 
        		</div>
        </div>
           <div class="row">
        		<div class="col-md-3">
		        	<div class="form-group">
		             {!! Form::label('','Número total de bultos') !!}
		             {!! Form::number('num_total_bultos',null,['class'=>'form-control','id'=>'num_total_bultos','onkeypress'=>'return soloNumeros(event)']) !!}
		            </div> 
        		</div>
        		<div class="col-md-3">
		        	<div class="form-group">
		            {!! Form::label('','Cantidad total') !!}
		             {!! Form::number('cant_total',null,['class'=>'form-control','id'=>'cant_total','onkeypress'=>'return soloNumeros(event)']) !!}
		            </div> 
        		</div>
        		<div class="col-md-3">
		        	<div class="form-group">
		             {!! Form::label('','Peso Neto') !!}
		             {!! Form::number('peso_neto_pn',null,['class'=>'form-control','id'=>'peso_neto_pn','onkeypress'=>'return soloNumeros(event)']) !!}
		            </div> 
        		</div>
        		<div class="col-md-3">
		        	<div class="form-group">
		             {!! Form::label('','Peso bruto total (Kg)') !!}
		             {!! Form::number('peso_bruto',null,['class'=>'form-control','id'=>'peso_bruto','onkeypress'=>'return soloNumeros(event)']) !!}
		            </div> 
        		</div>
        </div>
    </div>



  <div class="panel-heading"><h4>Descripción de la partida</h4></div>
    <div class="panel-body">
          <div class="row" >
            
              
              <div class="col-md-8 text-danger"><h4></h4></div>
              <div class="col-md-4" align="right">
                <button  name="add" type="button" id="add" value="add more" class="btn btn-success btn-md"><span><i class="glyphicon glyphicon-plus"></i></span> Agregar un Nuevo Producto</button><br><br>
              </div>
              
              <table class="table table-bordered" id="fieldse1">
              	
                    <tr>                      
                      <th>Código NC</th>
                      <th>Especie</th>
                      <th>Almacén frigorifico</th>
                      <th>Marca de identificación</th>
                      <th>Tipos de embalaje</th>
                      <th>Naturaleza de la mercancía fábrica / planta procesadora</th>
                    </tr>
 @foreach($DescripPartidaZoo as $key=>$valor)
                    <tr id="row">
                    

		              <td>{!!Form::text('codigo_nc[]',$valor->codigo_nc,['class'=>'form-control','id'=>'codigo_nc'])!!}
		              </td>
                      <td>
                        {!!Form::text('especie[]',$valor->especie,['class'=>'form-control','id'=>'especie'])!!}
                      </td>
                      <td>
                        {!!Form::text('almacen_frigo[]',$valor->almacen_frigo,['class'=>'form-control','id'=>'almacen_frigo'])!!}
                      </td>
                       <td>                
                          {!!Form::text('marc_ident[]',$valor->marc_ident,['class'=>'form-control','id'=>'marc_ident'])!!}
                      </td>
                      <td>
                          {!!Form::text('tipos_embalaje[]',$valor->tipos_embalaje,['class'=>'form-control','id'=>'tipos_embalaje'])!!}
                      </td>
                      <td>
                         {!!Form::text('nat_merc[]',$valor->nat_merc,['class'=>'form-control','id'=>'nat_merc'])!!}
                      </td>

                      <td> {!!Form::hidden('id[]',$valor->id,['class'=>'form-control','id'=>'id'])!!}

                    </tr>
      @endforeach
              </table>

               <table class="table table-bordered" id="fieldse2">

                    <tr>                      
                      <th>Consumidor final</th>
                      <th>Tipo de tratamiento</th>
                      <th>Fecha de recogida / producción</th>
                      <th>Peso neto</th>
                      <th>Número de bultos</th>
                      <th>Número de lote</th>
                    </tr>
	@foreach($DescripPartidaZoo as $key=>$valor)
					<tr id="row_{{$key+1}}">
                      <td> 
                           {!!Form::text('consumidor_final[]',$valor->consumidor_final,['class'=>'form-control','id'=>'consumidor_final'])!!}
                      </td>
                      <td>

                          {!!Form::text('tipo_tratamiento[]',$valor->tipo_tratamiento,['class'=>'form-control','id'=>'tipo_tratamiento'])!!}
                      </td>
                      <td>

			            <div class="form-group">
			            	<div class="input-group date">
			            		 {!!Form::text('fecha_recogida[]',$valor->fecha_recogida,['class'=>'form-control','id'=>'fecha_recogida'])!!}
			            		<span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
			            	</div>
				        </div>
        	
                      </td>
                       <td>               

                            {!!Form::text('peso_neto[]',$valor->peso_neto,['class'=>'form-control','id'=>'peso_neto','onkeypress'=>'return soloNumeros(event)'])!!}
                      </td>
                      <td>

                          {!!Form::text('num_bultos[]',$valor->num_bultos,['class'=>'form-control','id'=>'num_bultos','onkeypress'=>'return soloNumeros(event)'])!!}
                      </td>
                      <td>

                           {!!Form::text('num_lotes[]',$valor->num_lotes,['class'=>'form-control','id'=>'num_lotes','onkeypress'=>'return soloNumeros(event)'])!!}
                      </td>

                       <td> {!!Form::hidden('id[]',$valor->id,['class'=>'form-control','id'=>'id'])!!}
                      	<button  name="remove" type="button" id="{{$key+1}}" value="" class="btn btn-danger btn-remove">x</button>
                      </td>
                    </tr>
 @endforeach
              </table>

          </div>

        <br>
        <div class="row text-center">
          <a href="{{route('certificadosZooEu.index')}}" class="btn btn-primary">Cancelar</a>
          <input type="submit" class="btn btn-success" value="Enviar" name="Enviar" onclick="enviarInversionista()"> 
        </div><br>



    


    </div>

  </div>
</div>

{{Form::close()}}


<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

<script type="text/javascript">


$(document).ready(function (){


    var i = 0;
    var j = 0; 
    var k = 0; 
 
   
    $('#add').click(function()
    {
       i++;

      $('#fieldse1 tr:last').after('<tr id="row'+i+'" display="block" class="show_div"><td> {!!Form::text('codigo_nc[]',null,['class'=>'form-control','id'=>"codigo_nc",'placeholder'=>'Código NC'])!!}</td><td>{!!Form::text('especie[]',null,['class'=>'form-control','id'=>'especie','placeholder'=>'Especie'])!!}</td><td>{!!Form::text('almacen_frigo[]',null,['class'=>'form-control','placeholder'=>'Almacén frigorifico'])!!}</td><td> {!!Form::text('marc_ident[]',null,['class'=>'form-control','id'=>"marc_ident",'placeholder'=>'Marca de identificación'])!!}</td><td>{!!Form::text('tipos_embalaje[]',null,['class'=>'form-control','id'=>'tipos_embalaje','placeholder'=>'Tipos de embalaje'])!!}</td><td>{!!Form::text('nat_merc[]',null,['class'=>'form-control','placeholder'=>'Naturaleza de la mercancía fábrica / planta procesadora'])!!}</td><td>{!!Form::hidden('id[]',0)!!}<button  name="remove" type="button" id="'+i+'" value="" class="btn btn-danger btn-remove">x</button></td></tr>');

      	$('#fieldse2 tr:last').after('<tr id="row'+i+'" display="block" class="show_div"><td> {!!Form::text('consumidor_final[]',null,['class'=>'form-control','id'=>"consumidor_final",'placeholder'=>'Consumidor final'])!!}</td><td>{!!Form::text('tipo_tratamiento[]',null,['class'=>'form-control','id'=>'tipo_tratamiento','placeholder'=>'Tipo de tratamiento'])!!}</td><td><div class="form-group"><div class="input-group date">{!! Form::date('fecha_recogida[]',null,['class'=>'form-control','id'=>'fecha_recogida[]']) !!}<span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span></div></div></td><td> {!!Form::text('peso_neto[]',null,['class'=>'form-control','id'=>'peso_neto','onkeypress'=>'return soloNumerosDouble(event)','placeholder'=>'Peso neto'])!!}</td><td>{!!Form::text('num_bultos[]',null,['class'=>'form-control','id'=>'num_bultos','placeholder'=>'Número de bultos','onkeypress'=>'return soloNumerosDouble(event)'])!!}</td><td>{!!Form::text('num_lotes[]',null,['class'=>'form-control','onkeypress'=>'return soloNumerosDouble(event)','placeholder'=>'Número de lote'])!!}</td></tr>');
    });







        $(document).on('click','.btn-remove',function(){
        /*var id_boton= $(this).attr("id");
        $("#row"+id_boton+"").remove();
          */
        var dataComercializador = $(this).data('comercializador');
        if( dataComercializador == undefined)
          this.parentNode.parentNode.remove();
        else
        this.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.remove();
      });





$(document).on('click','.btn-remove',function(){
		var id_boton= $(this).attr("id");
        var id=$(this).prev().val();
		
		if(id!=0){

			if (confirm('Deseas eliminar este Registro?')) {

				$.ajax({

				'type':'get',
				'url': 'eliminarRegistro',
				'data':{ 'id':id},
				success: function(data){

					//$('#row'+id_boton).hide();
					location.reload();

					}

				});

			}



		}else{

		//$('#row'+id_boton).remove();
		/*var id_boton= $(this).attr("id");
        $("#row"+id_boton+"").remove();
          */
         var dataComercializador = $(this).data('comercializador');
        if( dataComercializador == undefined)
          this.parentNode.parentNode.remove();
        else
        this.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.remove();
		}

		



});

});






</script>

@stop