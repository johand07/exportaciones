<?php

namespace App\Http\Controllers;
use App\Models\GenArancelNandina;
use App\Models\GenArancelMercosur;
use App\Models\Productos;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class ReporteProductosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $titulo="Detalle del Producto";
        $descripcion="Productos Registrados"; 
        //dd($request->desde);
       if($request->ajax()){

                //dd($request['arancel']);
            if($request['arancel']==1){

                $arancel=GenArancelNandina::take(50)->get();
                return $arancel;

            }elseif($request['arancel']==2){

                 $arancel=GenArancelMercosur::take(50)->get();
                 return $arancel;

            }elseif($request['valor']!="") {

                if( ($request['tipoAran']==1) && ($request['valor']!="")){

                    $data=GenArancelNandina::where('codigo','like','%'.$request['valor'].'%')->orWhere('descripcion','like','%'.$request['valor'].'%')->take(50)->get();

                return $data;

                }elseif(($request['tipoAran']==2) && ($request['valor']!="")){

                    $data=GenArancelMercosur::where('codigo','like','%'.$request['valor'].'%')->orWhere('descripcion','like','%'.$request['valor'].'%')->take(50)->get();

                    return $data;

               }

            }else{
                if($request['tipoAran']==1){

                $arancel=GenArancelNandina::take(50)->get();
                 return $arancel;

                }else{

                $arancel=GenArancelMercosur::take(50)->get();
                return $arancel;

               }
            }

        }else{


           $arancel=$arancel=GenArancelMercosur::all();

        }
        $productos[]='';
        //dd($productos);
      return view('DetalleProductos.index',compact('titulo','descripcion','arancel','productos'));
    }

    public function Arancel(Request $request) {


        if($request->ajax()){



            if($request->arancel==1){

                $arancel=GenArancelNandina::take(50)->get();
                return $arancel;

            }elseif($request->arancel==2){

                 $arancel=GenArancelMercosur::take(50)->get();

                 return $arancel;

            }elseif($request->valor!="") {

                if( ($request->tipoAran==1) && ($request->valor!="")){

                    $data=GenArancelNandina::where('codigo','like','%'.$request->valor.'%')->orWhere('descripcion','like','%'.$request->valor.'%')->take(50)->get();

                return $data;

                }elseif(($request->tipoAran==2) && ($request->valor!="")){

                    $data=GenArancelMercosur::where('codigo','like','%'.$request->valor.'%')->orWhere('descripcion','like','%'.$request->valor.'%')->take(50)->get();

                    return $data;

               }

            }else{
                if($request->tipoAran==1){

                $arancel=GenArancelNandina::take(50)->get();
                 return $arancel;

                }else{

                $arancel=GenArancelMercosur::take(50)->get();
                return $arancel;

               }
            }

        }else{


           $arancel=$arancel=GenArancelMercosur::all();

        }

        return view('DetalleProductos.index',compact('arancel'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $titulo="Detalle del Producto";
        $descripcion="Productos Registrados"; 
        $fecha=date('d-m-Y');
        //dd($request->codigo);
            if($request->ajax()){



            if($request->arancel==1){

                $arancel=GenArancelNandina::take(50)->get();
                return $arancel;

            }elseif($request->arancel==2){

                 $arancel=GenArancelMercosur::take(50)->get();

                 return $arancel;

            }elseif($request->valor!="") {

                if( ($request->tipoAran==1) && ($request->valor!="")){

                    $data=GenArancelNandina::where('codigo','like','%'.$request->valor.'%')->orWhere('descripcion','like','%'.$request->valor.'%')->take(50)->get();

                return $data;

                }elseif(($request->tipoAran==2) && ($request->valor!="")){

                    $data=GenArancelMercosur::where('codigo','like','%'.$request->valor.'%')->orWhere('descripcion','like','%'.$request->valor.'%')->take(50)->get();

                    return $data;

               }

            }else{
                if($request->tipoAran==1){

                $arancel=GenArancelNandina::take(50)->get();
                 return $arancel;

                }else{

                $arancel=GenArancelMercosur::take(50)->get();
                return $arancel;

               }
            }

        }else{


           $arancel=$arancel=GenArancelMercosur::all();

        }
            /**----------------------------------------------------------------------*/
           $productos=DB::table('productos')
            ->join('det_usuario','productos.gen_usuario_id','=','det_usuario.gen_usuario_id')
            ->select(
                'det_usuario.rif',
                'det_usuario.razon_social',
                'det_usuario.correo',
                'productos.codigo',
                'productos.descripcion',
                'productos.descrip_comercial',
                'productos.cap_insta_anual',
                'productos.cap_opera_anual',
                'productos.cap_alamcenamiento',
                'productos.cap_exportacion'
                )

            ->where('productos.codigo','=', $request->codigo)
            ->where('productos.descripcion','=', $request->descripcion)
            //->groupBy('det_usuario.rif')
            ->whereNotIn('productos.gen_usuario_id',[1,2,3,4,5,11,12])
            ->get();

            //dd($productos);

            //return redirect()->action('ReporteProductosController@index');
            return view('DetalleProductos.index',compact('titulo','descripcion','productos','arancel','fecha'));
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Productos  $productos
     * @return \Illuminate\Http\Response
     */
    public function show(Productos $productos)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Productos  $productos
     * @return \Illuminate\Http\Response
     */
    public function edit(Productos $productos)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Productos  $productos
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Productos $productos)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Productos  $productos
     * @return \Illuminate\Http\Response
     */
    public function destroy(Productos $productos)
    {
        //
    }

    public function DetalleProductosExcel(Request $request, Productos $productos)
    {
        
        //dd($request->all());
         $productos=DB::table('productos')
            ->join('det_usuario','productos.gen_usuario_id','=','det_usuario.gen_usuario_id')
            ->select(
                'det_usuario.rif',
                'det_usuario.razon_social',
                'det_usuario.correo',
                'productos.codigo',
                'productos.descripcion',
                'productos.descrip_comercial',
                'productos.cap_insta_anual',
                'productos.cap_opera_anual',
                'productos.cap_alamcenamiento',
                'productos.cap_exportacion'
                )

            ->where('productos.codigo','=', $request->codigo)
            ->where('productos.descripcion','=', $request->descripcion)
            //->groupBy('det_usuario.rif')
            ->whereNotIn('productos.gen_usuario_id',[1,2,3,4,5,11,12])
            ->get();

        //dd($productos);
             $name="Reportes Detalle de Productos";
        //dd($empresas);
        $myFile   = Excel::create('Reportes Detalle de Productos',function($excel)use($productos){ // Nombre princial del archivo
            $excel->sheet('productos',function($sheet)use ($productos){ // se crea la hoja con el nombre de datos
               // dd($empresas);
                //header
                $sheet->setStyle(array(
                'font' => array(
                    'name' =>  'itálico',
                    'size' =>  8,
                    'bold' =>  true
                )
                ));

                $sheet->mergeCells('A1:I1'); // union de celdas desde A a F
                $sheet->row(1,['Reportes Detalle de Productos']);
                $sheet->row(2,[]);
                $sheet->row(3,['Rif','Razon Social','Correo','Código Arancelario','Descripción Arancelaria','Descripción Comercial','Capacidad Instalada Anual','Capacidad Operativa Anual','Capacidad de Almacenamiento Anual','Capacidad de Exportación']);

                //$sheet->fromArray($empresas, null, 'A4', null, false);
                // consulta

                
               //dd($empresas);
                //recorrido
                foreach ($productos as $pro) {


                   $row=[]; // un arreglo vacio que se va a llenar con las posicion 
                   $row[0]=$pro->rif;
                   $row[1]=$pro->razon_social;
                   $row[2]=$pro->correo;
                   $row[3]=$pro->codigo;
                   $row[4]=$pro->descripcion;
                   $row[5]=$pro->descrip_comercial;
                   $row[6]=$pro->cap_insta_anual;
                   $row[7]=$pro->cap_opera_anual;
                   $row[8]=$pro->cap_alamcenamiento;
                   $row[9]=$pro->cap_exportacion;

                   $sheet->appendRow($row); // agrega una fila vacia al final del reporte

                }


            });
           // dd($empresas);
           
        });
        $myFile   = $myFile->string('xls'); 
        $response = array(
            'name' => $name, 
            'file' => "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64," . base64_encode($myFile), 
        );

        return response()->json($response);




      

    }






}


