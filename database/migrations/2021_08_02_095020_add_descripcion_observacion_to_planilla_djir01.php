<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDescripcionObservacionToPlanillaDjir01 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('planilla_djir01', function (Blueprint $table) {
            
            $table->string('descripcion_observacion')->nullable()->after('estado_observacion');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('planilla_djir01', function (Blueprint $table) {
            
            $table->dropColumn('descripcion_observacion');
        
       });
    }
}
