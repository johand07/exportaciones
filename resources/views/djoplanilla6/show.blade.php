@extends('templates/layoutlte')
@section('content')
<div class="content">
<div class="panel-group" style="background-color: #fff;">
      <div class="panel-primary">
        <div class="panel-heading"><h3>(P-6) - 8. Fotografía del producto a exportar</h3></div>
        <div class="panel-body">


                  <div class="row" style="background-color: #fff">
                      <div class="col-md-12">

                          <div class="row">
                              
                              <div class="col-md-12">
                                  <table border="0" class="col-md-12">
                                    <thead>
                                      <tr>
                                        <th class="text-center" width="50%" height="40px" colspan="2"><h4><b>Producto</b></h4></th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      <tr>
                                        <td class="text-center" valign="top">
                                          @if(file_exists(substr($planilla6->imagen1, 1))) 
                                          <img src="{{asset($planilla6->imagen1)}}" alt="Imagen 1" width="300px" style="border-radius: 5px;">
                                          @else
                                          <div class="alert alert-warning" style="color:#fff"><strong>Atención!</strong> No es posible ubicar este fichero, por favor comunicarse con el administrador de sistemas.</div>
                                          @endif
                                        </td>
                                        <td class="text-center">
                                          @if(file_exists(substr($planilla6->imagen2, 1))) 
                                          <img src="{{asset($planilla6->imagen2)}}" alt="Imagen 2" width="300px" style="border-radius: 5px;">
                                          @else
                                          <div class="alert alert-warning" style="color:#fff"><strong>Atención!</strong> No es posible ubicar este fichero, por favor comunicarse con el administrador de sistemas.</div>
                                          @endif
                                        </td>
                                      </tr>
                                      <tr>
                                        <td class="text-center" colspan="2"><br>
                                          @if(file_exists(substr($planilla6->imagen3, 1))) 
                                          <img src="{{asset($planilla6->imagen3)}}" alt="Imagen 3" width="300px" style="border-radius: 5px;">
                                          @else
                                          <div class="alert alert-warning" style="color:#fff"><strong>Atención!</strong> No es posible ubicar este fichero, por favor comunicarse con el administrador de sistemas.</div>
                                          @endif
                                        </td>
                                      </tr>

                                    </tbody>
                                  </table>
                              </div>
                          </div>



                      </div>
                  </div>

        </div>
      </div>
    </div>

  </div><!-- Fin content -->

<!--script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script-->


@endsection