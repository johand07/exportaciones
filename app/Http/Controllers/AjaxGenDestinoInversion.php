<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\CatDestinoInversion;
use Auth;

class AjaxGenDestinoInversion extends Controller
{
    public function listar(){

         /*$paises=Pais::select('text','dpais','cpais')->where('bactivo',1)->get();*/

        $sect_produc_eco=CatDestinoInversion::select('nombre_destino','id')->where('bactivo',1)->get();

        $listasector=[]; 

        foreach ($sect_produc_eco as $key => $value) {

            $listasector[]=['text'=>''.$value->nombre_destino.'','value'=>''.$value->id.''];
        }
         
        return response()->json($listasector);    }
}
