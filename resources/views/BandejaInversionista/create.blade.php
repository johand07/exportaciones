@extends('templates/layoutlte')

@section('content')

<!--Aqui Form::open-->
{!! Form::open(['route' => 'BandejaInversionista.FormInversionista' ,'method'=>'POST']) !!}

<div class="panels">
  <div class="content">
    <div class="panel-group">
      <div class="panel-primary" style="background-color: #fff">
        <div class="panel-heading"><h3>Seleccione el tipo de usuario</h3>
    
        </div>
        <div class="panel-body">
          <div class="form-group">
            {!! Form::label('Usuarios','Usuarios') !!}


            {!! Form::select('cat_tipo_usuario_id',$tipousuario,null,['class'=>'form-control','id'=>'cat_tipo_usuario_id',
            'placeholder'=>'Seleccione el tipo de usuario','onchange'=>'valorSelect()','required'=>'true']) !!}

          
          </div>

          <div class="row text-center">
            <a href="{{ url('/exportador/BandejaInversionista')}}" class="btn btn-primary">Cancelar</a>
            <input type="submit" class="btn btn-success" value="Siguiente" name="Siguiente"> 
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

{{Form::close()}}
@stop

    