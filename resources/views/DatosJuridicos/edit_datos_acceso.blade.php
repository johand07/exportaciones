@extends('templates/layoutlte')
@section('content')
<br><br><br>

{{Form::model($DatosAcceso,['route'=>['DatosAcceso.update',$DatosAcceso->id],'method'=>'PATCH','id'=>'FormDatosAcceso']) }}



<div>
    <div class="panel-heading">DATOS DE ACCESO</div>
    <div class="panel-body">
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					{!! Form::label('','Correo Electrónico')!!}
					{!! Form::text('correo',null, ['class'=>'form-control','id'=>'correo','onkeyup'=>"minuscula(this.value,'#email');",'maxlength'=>'80']) !!}
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					{!! Form::label('','Correo Alternativo')!!}
					{!! Form::text('correo_sec',null, ['class'=>'form-control','id'=>'correo_sec','onkeyup'=>"minuscula(this.value,'#email');",'maxlength'=>'80']) !!}
				</div>
			</div>
              <div class="col-md-12">
                <div class="col-md-4"></div>
                <div class="col-md-4 text-center">
                  <div class="form-group">
                    <a class="btn btn-primary" href="{{route('DatosEmpresa.index')}}" id="atras1">Atras</a>
                   <input type="submit" name="Guardar" value="Guardar" class="btn btn-success">
                  </div>
                </div>
              </div>
        </div>
	</div>
	</div>
{{Form::close()}}

@stop