@extends('templates/layoutlte')
@section('content')


{{Form::open(['route' =>'productos_tecno.store' ,'method'=>'POST','id'=>'formProdTecno','onSubmit="validarProductosTecno(); return false"'])}}

{{Form::hidden('metodo','Crear')}}




 @include('productos_tecno.fields')

 

 


@push('productos_tecno')

var i = 0;
     $('#add').click(function(e){
   e.preventDefault();
       i++;
      var row = 'row'+i;


$('#fields_tec tr:last').after('<tr id="row'+i+'" display="block" class="show_div"><td>{!!Form::text('cod[]','0000.00.0',['class'=>'form-control','readonly' => 'readonly'])!!}</td><td>{!!Form::text('descripcion[]',null,['class'=>'form-control','onkeypress'=>'return soloLetras(event)'])!!}</td><td>{!!Form::text('cantidad[]',null,['class'=>'form-control format','id'=>'cantidadFob'])!!}</td><td>{!!Form::text('precio[]',null,['class' => 'form-control format text-right','id'=>'precioFob'])!!}</td><td>{!!Form::text('valor_fob[]',null,['class' => 'form-control format text-right','readOnly'=>'readOnly','id'=>'montoFob'])!!}</td><td>{!!Form::select('unidad_medida_id[]',$medidas,null,['class'=>'form-control','placeholder'=>'Seleccione'])!!}</td><td><button  name="remove" type="button" id='+i+' value="" class="btn btn-danger btn-remove">x</button></td></tr>');


  var main = document.getElementById(row);
 main.querySelectorAll('#cantidadFob')[0].setAttribute("onInput", "calcularMonto('"+row+"','cantidadFob','precioFob','montoFob')");

  main.querySelectorAll('#precioFob')[0].setAttribute("onInput", "calcularMonto('"+row+"','cantidadFob','precioFob','montoFob')");


      $(".format").on({
  
      "keypress": function(e) {
        
           key = e.keyCode || e.which;
           tecla = String.fromCharCode(key).toLowerCase();
           letras = "0123456789.";
           especiales = "8-37-39-46";

           tecla_especial = false
           for(var i in especiales){
                if(key == especiales[i]){
                    tecla_especial = true;
                    break;
                }
            }

            if(letras.indexOf(tecla)==-1 && !tecla_especial){
                return false;
            }

          }

        }); 

    });


@endpush

<script src="{{asset('js/ajax_v2.1.1.js')}}"></script>

<script type="text/javascript">
  
  $(document).ready(function(){

  @stack('productos_tecno');

});

</script>

@stop
