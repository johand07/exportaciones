<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GenUsuario extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gen_usuario', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('gen_tipo_usuario_id')->default(1)->index()->nullable();
            $table->foreign('gen_tipo_usuario_id')->references('id')->on('gen_tipo_usuario');
            $table->unsignedInteger('cat_tipo_usuario_id')->default(1)->index()->nullable();
            $table->foreign('cat_tipo_usuario_id')->references('id')->on('cat_tipo_usuario');
            $table->unsignedInteger('gen_status_id')->default(1)->index()->nullable();
            $table->foreign('gen_status_id')->references('id')->on('gen_status');
            $table->string('email', 150)->unique()->nullable();
            $table->string('email_seg', 150)->nullable();
            $table->string('password', 100)->nullable();
            $table->unsignedInteger('cat_preguntas_seg_id')->index()->nullable();
            $table->foreign('cat_preguntas_seg_id')->references('id')->on('cat_preguntas_seg');
            $table->string('pregunta_alter', 200)->nullable();
            $table->string('respuesta_seg', 100)->nullable();
            $table->rememberToken()->default(1);
            $table->boolean('bactivo')->default(1);
            $table->timestamps();




        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gen_usuario');
    }
}
