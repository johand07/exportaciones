<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        //$this->call(cat_sector_inversionista::class);
        //$this->call(CatActEcoInversionista::class);
        //$this->call(DestinoInversion::class);
        //$this->call(SectorProductivo::class);
        //$this->call(OtroSector::class);
        //$this->call(OtroActividadEco::class);
        //$this->call(CatMedioTransp::class);
        //$this->call(CatCondiTransp::class);
        //$this->call(CatCertEfecDe::class);
        $this->call(CatCargadosSeeder::class);
        $this->call(CatDocumentos::class);
    }
}
