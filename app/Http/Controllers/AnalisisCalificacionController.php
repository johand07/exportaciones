<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\DetUsuario;
use App\Models\GenDeclaracionJO;
use App\Models\DetDeclaracionProduc;
use App\Models\GenArancelNandina;
use App\Models\GenArancelMercosur;
use App\Models\Productos;
use App\Models\CatAldTp;
use App\Models\CatBoliviaVzla;
use App\Models\CatCanMercosur;
use App\Models\CatCubaVzla;
use App\Models\CatColombiaVzla;
use App\Models\CatPeruVzla;
use App\Models\CatCndVzla;
use App\Models\CatUeVzla;
use App\Models\CatUsaVzla;
use App\Models\CatTurquiaVzla;
use App\Models\AnalisisCalificacion;
use App\Models\CriterioAnalisis;
use App\Models\GenBandejaEntradaDjo;
use App\Models\DocAdicionalDjo;
use App\Models\Planilla2;
use App\Models\Planilla3;
use App\Models\Planilla4;
use App\Models\Planilla5;
use App\Models\Planilla6;
//use App\Models\DetUsuario;
use Illuminate\Support\Facades\Crypt;
//use App\Http\Requests\AnalisisDJO;
use Session;
use Alert;
use Auth;

class AnalisisCalificacionController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $model= new GenDeclaracionJO;

        if(!empty($request->start_date) && !empty($request->end_date)){

                $declaraciones= $model
                        ->with('rDeclaracionProduc.rProducto.rArancelNan')
                        ->with('rDeclaracionProduc.rProducto.rArancelMer')
                        ->with('rDeclaracionProduc.rDeclaracionPaises.rPais')
                        ->with('rDeclaracionProduc.rPlanilla2')
                        ->with('rDeclaracionProduc.rPlanilla3')
                        ->with('rDeclaracionProduc.rPlanilla4')
                        ->with('rDeclaracionProduc.rPlanilla5')
                        ->with('rDeclaracionProduc.rPlanilla6')
                        ->with('rDetUsuario')
                        ->with('rGenStatus')
                        ->with('rBandejaEntradaDjo')
                        ->where('bactivo',1)
                        ->whereBetween('fstatus', [$request->start_date, $request->end_date])
                        ->orderBy('id','desc')
                        ->get();
                        
        }else{
                $declaraciones= $model
                        ->with('rDeclaracionProduc.rProducto.rArancelNan')
                        ->with('rDeclaracionProduc.rProducto.rArancelMer')
                        ->with('rDeclaracionProduc.rDeclaracionPaises.rPais')
                        ->with('rDeclaracionProduc.rPlanilla2')
                        ->with('rDeclaracionProduc.rPlanilla3')
                        ->with('rDeclaracionProduc.rPlanilla4')
                        ->with('rDeclaracionProduc.rPlanilla5')
                        ->with('rDeclaracionProduc.rPlanilla6')
                        ->with('rDetUsuario')
                        ->with('rGenStatus')
                        ->with('rBandejaEntradaDjo')
                        ->where('bactivo',1)
                        ->where('gen_status_id',9)
                        ->orwhere('gen_status_id',10)
                        ->orwhere('gen_status_id',11)
                        ->orwhere('gen_status_id',12)
                        ->orwhere('gen_status_id',16)
                        ->orwhere('gen_status_id',36)
                        ->orwhere('gen_status_id',37)
                        ->orwhere('gen_status_id',38)
                        ->orderBy('id','desc')
                        ->get();
        }

             



                $titulo= 'Declaración Jurada de Origen';
                $descripcion= 'Perfil del Analista';

        return view('AnalisisDjo.index',compact('declaraciones', 'descripcion', 'titulo'));

    }
    
    //Funcion creada para pintar la Observacion del mismo Analista en Djo
    //Consulta las Observaciones de CADA PRODUCTO
    public function verObservacion(Request $request, $id)
    {
       //dd($request); 
      // dd($id);
     // Realizado con query builder; escribir queries a la base de datos
       $observaciones=DB::table('det_declaracion_produc as det')

      
        ->select('prod.codigo',   
                 'prod.descripcion',
                 'p2.descrip_observacion as obsp2',
                 'p3.descrip_observacion as obsp3',
                 'p4.descrip_observacion as obsp4',
                 'p5.descrip_observacion as obsp5',
                 'p6.descrip_observacion as obsp6'
                )
        ->join('planilla_2 as p2', 'det.id', '=', 'p2.det_declaracion_produc_id')//
        ->join('planilla_3 as p3', 'det.id', '=', 'p3.det_declaracion_produc_id')//
        ->join('planilla_4 as p4', 'det.id','=', 'p4.det_declaracion_produc_id')//
        ->join('planilla_5 as p5', 'det.id','=', 'p5.det_declaracion_produc_id')//
        ->join('planilla_6 as p6', 'det.id','=', 'p6.det_declaracion_produc_id')//
        ->join('productos as prod', 'det.productos_id','=', 'prod.id')//
        //
        //->whereNotIn('det.gen_usuario_id',[1,2,3,4,5,11,12,2430,2268])
        
        ->where('prod.bactivo',1)
        ->where('det.gen_declaracion_jo_id',$id)
        
        ->get();

      
        return response()->json($observaciones);

    }


    public function create(Request $request)
    {
     // dd(1);
       $idDeclaracion=$request->djo ? $request->djo : '';

        // Validando si viene por get el id de la declaracion
        if (!empty($idDeclaracion)) 
        {

            $idDeclaracion=decrypt($idDeclaracion) ? decrypt($idDeclaracion) : 'false';

        // o Validando si esta en la sesion
        }else{

            $idDeclaracion = Session::get('gen_declaracion_jo_id');
        }

        // Validamos que  exista el id de la declaracion
        if(empty($idDeclaracion) or  $idDeclaracion=='false')
        {
              
              Alert::warning('No puede ser procesada la Solicitud.','Acción Denegada!')->persistent("OK");

              return redirect()->action('AnalisisCalificacionController@index'); 

        }




      $declaraciondjo=GenDeclaracionJO::find($idDeclaracion);
      //dd($declaraciondjo);

      $det_usuario=DetUsuario::where('gen_usuario_id',$declaraciondjo->gen_usuario_id)->first();
     //dd($det_usuario);

      $rutacedula=$det_usuario->ruta_doc;
      //dd($rutacedula);
      $rutarif=$det_usuario->ruta_rif;
       //dd($rutarif);
      $rutaregistromer=$det_usuario->ruta_reg_merc;
    // dd($rutaregistromer);


       if(!empty($rutacedula)){
             $exten_file_1=explode(".", $rutacedula);
            $extencion_file_1 = $exten_file_1[1];
        } else {
            $extencion_file_1 = '';
        }
//dd($extencion_file_1);

        if (!empty($rutarif)) {
            $exten_file_2=explode(".", $rutarif);
            $extencion_file_2 = $exten_file_2[1];
        }else {
            $extencion_file_2 = '';
        }
        if (!empty($rutaregistromer)) {
            $exten_file_3=explode(".", $rutaregistromer);
            $extencion_file_3 = $exten_file_3[1];
        }else {
            $extencion_file_3 = '';
        }

      //////////////////////////////////////////////////////
      // validamos si la solicitud esta libre para atender//
      //////////////////////////////////////////////////////
      $atender_solicitud=$this->atendersolicitud($idDeclaracion,10);
      //////////////////////////////
      // Si esta atendida por otro//
      //////////////////////////////
     
      if ($atender_solicitud==2) {
          Alert::warning('Esta solicitud esta siendo atendida por otro Analista.','Acción Denegada!')->persistent("OK");
          return redirect()->action('AnalisisCalificacionController@index');
      }
      if ($atender_solicitud==10000000) {
          Alert::warning('No puede atender esta solicitud, usted posee solicitudes pendientes por atender.','Acción Denegada!')->persistent("OK");
          return redirect()->action('AnalisisCalificacionController@index');
      }
      ///////////////////////////////////////////////////////

      // Consulta de la declaracion jurada de origen y todos sus datos asociados
      $modelDeclaracion= new GenDeclaracionJO;
     
      $producDeclaracion= $modelDeclaracion
                        ->where('id',$idDeclaracion)
                        ->with('rDeclaracionProduc.rProducto.rArancelNan')
                        ->with('rDeclaracionProduc.rProducto.rArancelMer')
                        ->with('rDeclaracionProduc.rDeclaracionPaises.rPais')
                        ->with('rDetUsuario')
                        ->first();
        // dd($producDeclaracion->rPais);
      // fin
    
    $doc_adicional= DocAdicionalDjo::where('gen_declaracion_jo_id',$idDeclaracion)->first(); 
    //dd($doc_adicional);         
      // Validamos que se encontro registros de la declaracion 
      if(empty($producDeclaracion->id)){

          Alert::warning('La solicitud no puede ser procesada Intente nuevamente.','Acción Denegada!')->persistent("OK");

          return redirect()->action('AnalisisCalificacionController@index');
      }

      // Consulta de los catalogos referente a los acuerdos internacionales
          $cat_bolivia=CatBoliviaVzla::all()->pluck('codigo','codigo');
           $cat_mercosur=CatCanMercosur::all()->pluck('codigo','codigo');
            $cat_colombia=CatColombiaVzla::all()->pluck('codigo','codigo');
             $cat_cuba=CatCubaVzla::all()->pluck('codigo','codigo');
              $cat_aladi=CatAldTp::all()->pluck('codigo','codigo');
              $cat_peru=CatPeruVzla::all()->pluck('codigo','codigo');

              $cat_cnd=CatCndVzla::all()->pluck('codigo','codigo');
              $cat_ue=CatUeVzla::all()->pluck('codigo','codigo');
              $cat_usa=CatUsaVzla::all()->pluck('codigo','codigo');
              $cat_turquia=CatTurquiaVzla::all()->pluck('codigo','codigo');
              //dd($cat_turquia);
      // fin


      //////////////////////////////////////////////////////////////////////////////////////////////
      // Validamos nuevamente si la solicitud fue seleccionada por otro usuario en el mismo tiempo /
      /////////////////////////////////////////////////////////////////////////////////////////////
      $atender_solicitud=$this->atendersolicitud($producDeclaracion->id,10);
      //////////////////////////////
      // Si esta atendida por otro//
      //////////////////////////////
      if ($atender_solicitud==2) {
          Alert::warning('Esta solicitud esta siendo atendida por otro Analista.','Acción Denegada!')->persistent("OK");
          return redirect()->action('AnalisisCalificacionController@index');
      }
      ///////////////////////////////////////////////////////////////////////////////////////


      $titulo= 'DECLARACIÓN JURADA DE ORIGEN';
      $descripcion= 'Analisis';

      return view('AnalisisDjo.create',compact('titulo','descripcion','producDeclaracion','cat_bolivia','cat_mercosur','cat_colombia','cat_cuba','cat_aladi','cat_peru','cat_cnd','cat_ue','cat_usa','cat_turquia','idDeclaracion','doc_adicional','rutacedula','rutarif','rutaregistromer','det_usuario','extencion_file_1','extencion_file_2','extencion_file_3'));
    }


    public function store(Request $request)
    {
       //dd($request->all());
       
       $existe_observaciones=$this->detectar_observaciones($request->num_solicitud);
       if($existe_observaciones==1){

          $estatus_gen_declaracion_jo=11;
            $mensajeModal="La Solicitud sera enviada al usuario Exportador para que sea Corregída";
            $tituloModal="Solicitud en Observación";
            
        }else{

          $estatus_gen_declaracion_jo=12;
          $mensajeModal="Esta Declaracion se ha Validado Exitosamnte!";
          $tituloModal="¡Solicitud Verificada!";
         
         
          $this->validate($request, [

            'fecha_emision'=>'required',
            'fecha_vencimiento'=>'required',
            'gen_declaracion_jo_id'=>'required',
            'num_solicitud'=>'required',
            'bol.*'=>'required',
            'col.*'=>'required',
            'ecu.*'=>'required',
            'per.*'=>'required',
            'cub.*'=>'required',
            'ald.*'=>'required',
            'arg.*'=>'required',
            'bra.*'=>'required',
            'par.*'=>'required',
            'uru.*'=>'required',
            'usa.*'=>'required',
            'ue.*'=>'required',
            'cnd.*'=>'required',
            'tp.*'=>'required',
            'tr.*'=>'required',

            ]);
        }
             
        DB::beginTransaction();

        $criterios=[ 
                    "ald",
                    "arg",
                    "ue",
                    "bra",
                    "uru",
                    "cnd",
                    "tp",
                    "bol",
                    "col",
                    "ecu",
                    "per",
                    "cub",
                    "par",
                    "usa",
                    "tr"  
                  ];

        // Validacion para la de solicitud
        if (empty($request->num_solicitud)) 
        {

            Alert::warning('No es Posible Procesar la Solicitud. Intente Nuevamente.','Acción Denegada!')->persistent("OK");
            return redirect()->back()->withInput();

        }

        // Validacion para los Productos
        if (empty($request->productoId)) 
        {

            Alert::warning('No es Posible Procesar la Solicitud. Intente Nuevamente.','Acción Denegada!')->persistent("OK");
            return redirect()->back()->withInput();

        }

        // Validar si existe alguna observación
            $existe_observaciones=$this->detectar_observaciones($request->num_solicitud);
            //dd($existe_observaciones);
            if($existe_observaciones==1){

              $estatus_gen_declaracion_jo=11;
                $mensajeModal="La Solicitud sera enviada al usuario Exportador para que sea Corregída";
                $tituloModal="Solicitud en Observación";
                
            }else{

              $estatus_gen_declaracion_jo=12;
              $mensajeModal="Esta Declaracion se ha Validado Exitosamnte!";
              $tituloModal="¡Solicitud Verificada!";
            }

        // Modelo Analisis  (Insertamos)
       
            $modelAnalisis= new AnalisisCalificacion;
            $modelAnalisis->gen_declaracion_jo_id=$request->gen_declaracion_jo_id;
            $modelAnalisis->gen_usuario_id=Auth::user()->id;
            $modelAnalisis->num_solicitud=$request->num_solicitud;
            $modelAnalisis->observacion_analisis=$request->observacion_analisis;
            $modelAnalisis->gen_status_id=$estatus_gen_declaracion_jo; 
            
            if ($modelAnalisis->save()) 
            {
                    // Recorremos los Productos
                    foreach ($request->productoId as $key => $idProducto) 
                    {
                     
                          $modelCriterio= new CriterioAnalisis;

                          $modelCriterio->analisis_calificacion_id=$modelAnalisis->id;
                          $modelCriterio->det_declaracion_produc_id=$idProducto;
                          $modelCriterio->arancel_nan=$request->cod_nan[$idProducto];
                          $modelCriterio->arancel_mer=$request->cod_mer[$idProducto];
                          $modelCriterio->arancel_descrip_adicional=$request->arancel_descrip_adicional[$idProducto];

                          // recorremos los 14 criterios y asignamos los valores del modelo donde correspondan 
                            foreach ($criterios as $key2 => $idCriterio) 
                            {

                                if (isset($request->$idCriterio[$idProducto]))
                                {
              
                                  $modelCriterio->$idCriterio=$request->$idCriterio[$idProducto];
                                
                                }

                            }

                            if ($modelCriterio->save())
                            {
                              
                            }else{

                                DB::rollback();

                                Alert::warning('No es Posible Validar el Criterio','Acción Denegada!')->persistent("OK");
                                return redirect()->back()->withInput();
                               
                            }
   
                    }

                    // Actualizar la fecha de Emision y vencimiento de la declaracion
                    $modelDeclaracion=GenDeclaracionJO::find($modelAnalisis->gen_declaracion_jo_id);
                    $modelDeclaracion->fecha_emision=$request->fecha_emision;
                    $modelDeclaracion->fecha_vencimiento=$request->fecha_vencimiento;
                    $modelDeclaracion->save();
                 
                  // Actualizar los estados de la solicitud en bandeja de entrada y en declaracion_jo
                     $id_declaracion=$modelAnalisis->gen_declaracion_jo_id;
                        $this->actualizar_estatus_bandeja($id_declaracion,$estatus_gen_declaracion_jo);
                  // fin validar estatus
                  
                DB::commit();
                Alert::success($mensajeModal, $tituloModal)->persistent("Ok");
                return redirect()->action('AnalisisCalificacionController@index');

            }else{
                

                DB::rollback();
                Alert::warning('No es Posible Validar la Solicitud. Intente Nuevamente.','Acción Denegada!')->persistent("OK");
                return redirect()->back()->withInput();
            }



           
         


    }

    public function edit(Request $request,$djo)
    {


//dd(1);
      $idDeclaracion=decrypt($djo)?:00;
//dd($idDeclaracion);
      $declaraciondjo=GenDeclaracionJO::find($idDeclaracion);
      //dd($declaraciondjo);

      $det_usuario=DetUsuario::where('gen_usuario_id',$declaraciondjo->gen_usuario_id)->first();
   // dd($det_usuario->usuario->tipoUsuario);

      $rutacedula=$det_usuario->ruta_doc;
      //dd($rutacedula);
      $rutarif=$det_usuario->ruta_rif;
       //dd($rutarif);
      $rutaregistromer=$det_usuario->ruta_reg_merc;
     // dd($rutaregistromer);


       if(!empty($rutacedula)){
             $exten_file_1=explode(".", $rutacedula);
            $extencion_file_1 = $exten_file_1[1];
        } else {
            $extencion_file_1 = '';
        }
    //dd($extencion_file_1);

        if (!empty($rutarif)) {
            $exten_file_2=explode(".", $rutarif);
            $extencion_file_2 = $exten_file_2[1];
        }else {
            $extencion_file_2 = '';
        }
        if (!empty($rutaregistromer)) {
            $exten_file_3=explode(".", $rutaregistromer);
            $extencion_file_3 = $exten_file_3[1];
        }else {
            $extencion_file_3 = '';
        }


      // consulta de la declaracion jurada de origen y todos sus datos asociados
      $modelDeclaracion= new  AnalisisCalificacion;
      $producDeclaracion= $modelDeclaracion
                        ->where('gen_declaracion_jo_id',$idDeclaracion)                
                        ->with('rCriterioAnalisis.declaracionProduc.rProducto.rArancelNan')
                        ->with('rCriterioAnalisis.declaracionProduc.rProducto.rArancelMer') 
                        ->with('rCriterioAnalisis.declaracionProduc.rDeclaracionPaises')                      
                        ->first();
        //dd($producDeclaracion);

        $det_declaracionProd = DetDeclaracionProduc::where('gen_declaracion_jo_id', $declaraciondjo->id)->first();
       
        $planilla2  = Planilla2::where('det_declaracion_produc_id', $det_declaracionProd->id)->first();
        $planilla3  = Planilla3::where('det_declaracion_produc_id', $det_declaracionProd->id)->first();
        $planilla4  = Planilla4::where('det_declaracion_produc_id', $det_declaracionProd->id)->first();
        $planilla5  = Planilla5::where('det_declaracion_produc_id', $det_declaracionProd->id)->first();
        $planilla6  = Planilla6::where('det_declaracion_produc_id', $det_declaracionProd->id)->first();

        //dd($planilla5);
       /* foreach ($producDeclaracion->rCriterioAnalisis as $key => $productos) {
            dd($productos->arancel_mer);
        }*/

        $doc_adicional = DocAdicionalDjo::where('gen_declaracion_jo_id',$idDeclaracion)->first();


      // fin

      // validamos si la solicitud existe
      if (empty($producDeclaracion->id)) {

           Alert::warning('No es Posible Validar la Solicitud. Intente Nuevamente.','Acción Denegada!')->persistent("OK");
           return redirect()->back()->withInput();
      }


      // validamos si la solicitud fue analizada si no enviame para el create para analizarla
      /*if (empty($producDeclaracion->rAnalisisCalificacion)) {
       
          Session::put('gen_declaracion_jo_id',$idDeclaracion);

          return redirect()->action('AnalisisCalificacionController@create');
      }*/

      // Consulta de los catalogos referente a los acuerdos internacionales
          $cat_bolivia=CatBoliviaVzla::all()->pluck('codigo','codigo');
          $cat_mercosur=CatCanMercosur::all()->pluck('codigo','codigo');
          $cat_colombia=CatColombiaVzla::all()->pluck('codigo','codigo');
          $cat_cuba=CatCubaVzla::all()->pluck('codigo','codigo');
          $cat_aladi=CatAldTp::all()->pluck('codigo','codigo');
          $cat_peru=CatPeruVzla::all()->pluck('codigo','codigo');

          $cat_cnd=CatCndVzla::all()->pluck('codigo','codigo');
          $cat_ue=CatUeVzla::all()->pluck('codigo','codigo');
          $cat_usa=CatUsaVzla::all()->pluck('codigo','codigo');
          $cat_turquia=CatTurquiaVzla::all()->pluck('codigo','codigo');

          
      // fin

      $titulo= 'DECLARACIÓN JURADA DE ORIGEN';
      $descripcion= 'Analista';

              // Obtengo el código de status actual - By WETS
              $status_id = GenDeclaracionJO::where('id', $idDeclaracion)->first();

      return view('AnalisisDjo.edit',compact('declaraciondjo','titulo','descripcion','producDeclaracion','cat_bolivia','cat_mercosur','cat_colombia','cat_cuba','cat_aladi','cat_peru','cat_cnd','cat_ue','cat_usa','cat_turquia','idDeclaracion','doc_adicional','extencion_file_1','extencion_file_2','extencion_file_3','det_usuario','rutacedula','rutarif','rutaregistromer','planilla2',
      'planilla3',
      'planilla4',
      'planilla5',
      'planilla6', 
      'status_id'));

    }



    public function show(Request $request,$djo)
    {

      $idDeclaracion=decrypt($djo)?:00;
      //dd($idDeclaracion);
      // consulta de la declaracion jurada de origen y todos sus datos asociados
      $modelDeclaracion= new  AnalisisCalificacion;
      $producDeclaracion= $modelDeclaracion
                        ->where('gen_declaracion_jo_id',$idDeclaracion)                
                        ->with('rCriterioAnalisis.declaracionProduc.rProducto.rArancelNan')
                        ->with('rCriterioAnalisis.declaracionProduc.rProducto.rArancelMer') 
                        ->with('rCriterioAnalisis.declaracionProduc.rDeclaracionPaises')                      
                        ->first();

        //dd($producDeclaracion->rCriterioAnalisis);
        $doc_adicional= DocAdicionalDjo::where('gen_declaracion_jo_id',$idDeclaracion)->first(); 


        $gen_Declaracion=GenDeclaracionJO::where('id',$idDeclaracion)->first();
      //dd($gen_Declaracion);


      // fin

      // validamos si la solicitud existe
      if (empty($producDeclaracion->id)) {

           Alert::warning('No es Posible Validar la Solicitud. Intente Nuevamente.','Acción Denegada!')->persistent("OK");
           return redirect()->back()->withInput();
      }


      // validamos si la solicitud fue analizada si no enviame para el create para analizarla
      /*if (empty($producDeclaracion->rAnalisisCalificacion)) {
       
          Session::put('gen_declaracion_jo_id',$idDeclaracion);

          return redirect()->action('AnalisisCalificacionController@create');
      }*/

      // Consulta de los catalogos referente a los acuerdos internacionales
          $cat_bolivia=CatBoliviaVzla::all()->pluck('codigo','codigo');
          $cat_mercosur=CatCanMercosur::all()->pluck('codigo','codigo');
          $cat_colombia=CatColombiaVzla::all()->pluck('codigo','codigo');
          $cat_cuba=CatCubaVzla::all()->pluck('codigo','codigo');
          $cat_aladi=CatAldTp::all()->pluck('codigo','codigo');
          $cat_peru=CatPeruVzla::all()->pluck('codigo','codigo');

          $cat_cnd=CatCndVzla::all()->pluck('codigo','codigo');
          $cat_ue=CatUeVzla::all()->pluck('codigo','codigo');
          $cat_usa=CatUsaVzla::all()->pluck('codigo','codigo');
          $cat_turquia=CatTurquiaVzla::all()->pluck('codigo','codigo');
          
      // fin

      $titulo= 'DECLARACIÓN JURADA DE ORIGEN';
      $descripcion= 'Analisis';


      /*** Documentos de usuario****/

       $doc_Usuario= DetUsuario::where('gen_usuario_id',$gen_Declaracion->gen_usuario_id)->first();

       //dd($doc_Usuario);
       


        if(!empty($doc_Usuario['ruta_doc'])){
             $exten_file_1=explode(".", $doc_Usuario['ruta_doc']);
            $extencion_file_1 = $exten_file_1[1];
        } else {
            $extencion_file_1 = '';
        }
        if (!empty($doc_Usuario['ruta_rif'])) {
            $exten_file_2=explode(".", $doc_Usuario['ruta_rif']);
            $extencion_file_2 = $exten_file_2[1];
        }else {
            $extencion_file_2 = '';
        }
        if (!empty($doc_Usuario['ruta_reg_merc'])) {
            $exten_file_3=explode(".", $doc_Usuario['ruta_reg_merc']);
            $extencion_file_3 = $exten_file_3[1];
        }else {
            $extencion_file_3 = '';
        }


      return view('AnalisisDjo.show',compact('gen_Declaracion','titulo','descripcion','producDeclaracion','cat_bolivia','cat_mercosur','cat_colombia','cat_cuba','cat_aladi','cat_peru','cat_cnd','cat_ue','cat_usa','cat_turquia','idDeclaracion','doc_adicional','doc_Usuario','extencion_file_1','extencion_file_2','extencion_file_3','gen_Declaracion'));

    }



    public function update(Request $request)
    {
      // dd($request->all());
       
       $existe_observaciones=$this->detectar_observaciones($request->num_solicitud);
    
        // Obtengo el código de status actual - By WETS
        $status_id = GenDeclaracionJO::where('num_solicitud', $request->num_solicitud)->first();

    // Validamos de que no sea una solicitud de apelación
    if ($status_id->gen_status_id!==37) {

        if($existe_observaciones==1){

            $estatus_gen_declaracion_jo=11;
              $mensajeModal="La Solicitud sera enviada al usuario Exportador para que sea Corregída";
              $tituloModal="Solicitud en Observación";
              
          }else{
  
            $estatus_gen_declaracion_jo=12;
            $mensajeModal="Esta Declaracion se ha Validado Exitosamente!";
            $tituloModal="¡Solicitud Verificada!";
           
           
            $this->validate($request, [
  
              'fecha_emision'=>'required',
              'fecha_vencimiento'=>'required',
              'gen_declaracion_jo_id'=>'required',
              'num_solicitud'=>'required',
              'bol.*'=>'required',
              'col.*'=>'required',
              'ecu.*'=>'required',
              'per.*'=>'required',
              'cub.*'=>'required',
              'ald.*'=>'required',
              'arg.*'=>'required',
              'bra.*'=>'required',
              'par.*'=>'required',
              'uru.*'=>'required',
              'usa.*'=>'required',
              'ue.*'=>'required',
              'cnd.*'=>'required',
              'tp.*'=>'required',
  
              ]);
          }
    }


      
      DB::beginTransaction();

        $criterios=[ 
                    "ald",
                    "arg",
                    "ue",
                    "bra",
                    "uru",
                    "cnd",
                    "tp",
                    "bol",
                    "col",
                    "ecu",
                    "per",
                    "cub",
                    "par",
                    "usa"  
                  ];

        // Validacion para la de solicitud
        if (empty($request->num_solicitud)) 
        {

            Alert::warning('No es Posible Procesar la Solicitud. Intente Nuevamente.','Acción Denegada!')->persistent("OK");
            return redirect()->back()->withInput();

        }

        // Validacion para los Productos
        if (empty($request->productoId))
        {

            Alert::warning('No es Posible Procesar la Solicitud. Intente Nuevamente.','Acción Denegada!')->persistent("OK");
            return redirect()->back()->withInput();

        }

        // Validar si existe alguna observación
            $existe_observaciones=$this->detectar_observaciones($request->num_solicitud);

        //validamos de que exista una solicitud de apeliación, y si existe validar si se declina o se aprueba
        if ($status_id->gen_status_id==37 && $request->selec_apelacion == "0") {
                $estatus_gen_declaracion_jo=10;
                $mensajeModal="La Solicitud ha sido abierta nuevamente";
                $tituloModal="Solicitud abierta";
        }
        elseif ($status_id->gen_status_id==37 && $request->selec_apelacion == "1") {
            $estatus_gen_declaracion_jo=38;
            $mensajeModal="La Solicitud ha sido declinada";
            $tituloModal="Solicitud declinada";
        }
        else
        {

            if($existe_observaciones==1){

                $estatus_gen_declaracion_jo=11;
                  $mensajeModal="La Solicitud sera enviada al usuario Exportador para que sea Corregída";
                  $tituloModal="Solicitud en Observación";
                  
              }else{
  
                $estatus_gen_declaracion_jo=12;
                $mensajeModal="Esta Declaracion se ha Validado Exitosamente!";
                $tituloModal="¡Solicitud Verificada!";
              }

        }
            



        //validamos de que la solicitud esté en apelción
        // Modelo Analisis  (Insertamos)
        if ($status_id->gen_status_id == 37) {
            $modelAnalisis=AnalisisCalificacion::where('num_solicitud',$request->num_solicitud)->first();
            $modelAnalisis->gen_status_id=$estatus_gen_declaracion_jo;
        }
        else{
            $modelAnalisis=AnalisisCalificacion::where('num_solicitud',$request->num_solicitud)->first();
            $modelAnalisis->observacion_analisis=$request->observacion_analisis;
            $modelAnalisis->gen_status_id=$estatus_gen_declaracion_jo; 
        }

            if ($modelAnalisis->save()) 
            {
                    // Recorremos los Productos
                    foreach ($request->productoId as $key => $idProducto) 
                    {
                     
                          $modelCriterio=CriterioAnalisis::where('analisis_calificacion_id',$modelAnalisis->id)
                                                          ->where('det_declaracion_produc_id',$idProducto)
                                                          ->first();

                          $modelCriterio->arancel_nan=$request->cod_nan[$idProducto];
                          $modelCriterio->arancel_mer=$request->cod_mer[$idProducto];
                          $modelCriterio->arancel_descrip_adicional=$request->arancel_descrip_adicional[$idProducto];

                          // recorremos los 14 criterios y asignamos los valores del modelo donde correspondan 
                            foreach ($criterios as $key2 => $idCriterio) 
                            {

                                if (isset($request->$idCriterio[$idProducto]))
                                {
              
                                  $modelCriterio->$idCriterio=$request->$idCriterio[$idProducto];
                                
                                }

                            }

                            if ($modelCriterio->save())
                            {
                              
                            }else{

                                DB::rollback();

                                Alert::warning('No es Posible Validar el Criterio','Acción Denegada!')->persistent("OK");
                                return redirect()->back()->withInput();
                               
                            }
   
                    }


                    // Actualizar la fecha de Emision y vencimiento de la declaracion
                    $modelDeclaracion=GenDeclaracionJO::find($modelAnalisis->gen_declaracion_jo_id);
                    $modelDeclaracion->fecha_emision=$request->fecha_emision;
                    $modelDeclaracion->fecha_vencimiento=$request->fecha_vencimiento;
                    $modelDeclaracion->observacion_corregida=0;
                    $modelDeclaracion->save();
                
                  // Actualizar los estados de la solicitud en bandeja de entrada y en declaracion_jo
                     $id_declaracion=$modelAnalisis->gen_declaracion_jo_id;
                        $this->actualizar_estatus_bandeja($id_declaracion,$estatus_gen_declaracion_jo);
                  // fin validar estatus
                  
                DB::commit();
                Alert::success($mensajeModal, $tituloModal)->persistent("Ok");
                return redirect()->action('AnalisisCalificacionController@index');

            }else{
                

                DB::rollback();
                Alert::warning('No es Posible Validar la Solicitud. Intente Nuevamente.','Acción Denegada!')->persistent("OK");
                return redirect()->back()->withInput();
            }
    }

    public function atendersolicitud($id_declaracion,$estatus)
    {
      $model=New GenBandejaEntradaDjo;

      // Consultamos cuantas solicitudes esta atendiendo el Analista logueado
      $cantidad_antendidas=$model->where('gen_usuario_id',Auth::user()->id)->where('estado','=',10)->orwhere('estado','=',11)->count();
     
      // Consultamos si esta atendida la declaracion
      $solicitud_atendida=$model->where('gen_declaracion_jo_id',$id_declaracion)->get()->last();
      //dd($cantidad_antendidas);
       // Validamos si la solicitud no esta atendida
        if (empty($solicitud_atendida->id)) 
         {
            // Se valida si el analista tiene menos de 10000000 solicitudes Atendidas sin Culminar su Proceso de analisis          
            if ($cantidad_antendidas<10000000) {

               // Se Atiende asignandola al analista logueado 
                 $atender=$this->actualizar_estatus_bandeja($id_declaracion,$estatus);
                 
                 return $atender;

             }else{
              
                // retornamos el numero 10000000 que indica que tiene 10000000 o mas solicitudes pendientes por atender
                 return $noPuedeAtender=10000000;
             }
             
         }

      // Validamos si la solicitud esta atendida por otro Analista
         if ($solicitud_atendida->gen_usuario_id!=Auth::user()->id) 
         {
            return $fueAtendida=2; // retornamos fue atendida por otro
         }

      return 0; 
    }


    public function detectar_observaciones($num_solicitud)
    {

        $model=new GenDeclaracionJO;
        $declaracion=$model->where('num_solicitud',$num_solicitud)
                            
                            ->with('rDeclaracionProduc')
                            ->with('rDeclaracionProduc.rPlanilla2')
                            ->with('rDeclaracionProduc.rPlanilla3')
                            ->with('rDeclaracionProduc.rPlanilla4')
                            ->with('rDeclaracionProduc.rPlanilla5')
                            ->with('rDeclaracionProduc.rPlanilla6')
                            ->get()
                            ->last();

            // verificamos si tiene una observacion
            $tieneObservacion=[];

                $id_declaracion=$declaracion->id;

                foreach ($declaracion->rDeclaracionProduc as $key => $productos) 
                {

                    $id_declaracion_product=$productos->id;

                    if ($productos->rPlanilla2->estado_observacion==1) 
                    {
                        // idproducto + campo_planilla
                        $tieneObservacion[$id_declaracion_product]['estado_p2']='estado_p2';

                    }if ($productos->rPlanilla3->estado_observacion==1) 
                    {
                         $tieneObservacion[$id_declaracion_product]['estado_p3']='estado_p3';

                    }if ($productos->rPlanilla4->estado_observacion==1) 
                    {
                         $tieneObservacion[$id_declaracion_product]['estado_p4']='estado_p4';

                    }if ($productos->rPlanilla5->estado_observacion==1) 
                    {
                         $tieneObservacion[$id_declaracion_product]['estado_p5']='estado_p5';

                    }if ($productos->rPlanilla6->estado_observacion==1) 
                    {
                         $tieneObservacion[$id_declaracion_product]['estado_p6']='estado_p6';
                    }

                    /** */

                    if ($productos->rPlanilla2->estado_observacion==0) 
                    {
                        $planilla2 = Planilla2::find($productos->rPlanilla2->id);//
                        $planilla2->estado_observacion = 3;
                        $planilla2->save();
                    }
                    if ($productos->rPlanilla3->estado_observacion==0) 
                    {
                        $planilla3 = Planilla3::find($productos->rPlanilla3->id);//
                        $planilla3->estado_observacion = 3;
                        $planilla3->save();
                    }
                    if ($productos->rPlanilla4->estado_observacion==0) 
                    {
                        $planilla4 = Planilla4::find($productos->rPlanilla4->id);//
                        $planilla4->estado_observacion = 3;
                        $planilla4->save();
                    }
                    if ($productos->rPlanilla5->estado_observacion==0) 
                    {
                        $planilla5 = Planilla5::find($productos->rPlanilla5->id);//
                        $planilla5->estado_observacion = 3;
                        $planilla5->save();
                    }
                    if ($productos->rPlanilla6->estado_observacion==0) 
                    {
                        $planilla6 = Planilla6::find($productos->rPlanilla6->id);//
                        $planilla6->estado_observacion = 3;
                        $planilla6->save();
                    }

                }

            //$resultado = array_merge($array1, $array2);
            // dd($declaracion->id);
            $detDocAdicionalDeclaracion= DocAdicionalDjo::where('gen_declaracion_jo_id',$declaracion->id)->first();
            

            if (!empty($tieneObservacion)) 
            {
 
              // Enviamos para Actualizar los estados de las planillas en det_declaracion_produc 
                  $estatusPlanillas=$this->actualizar_estatus_planillas($tieneObservacion,$id_declaracion);

              // Si se actualizaron los estados de las planillas devuelve 0 si no 1
                  if($estatusPlanillas==0){

                      $repuestaTieneObservacion=1;
                  
                  }else{

                      // no actualizaron los estados de las planillas mostrar error
                      Alert::warning('No es Posible Validar la Solicitud.','Error:D-O_01!')->persistent("OK");
                      return redirect()->back()->withInput();
                  }
                

                return  $repuestaTieneObservacion; // Tiene almenos una observacion
               
            }elseif (!empty($detDocAdicionalDeclaracion->descrip_observacion) && $detDocAdicionalDeclaracion->estado_observacion == 1) {
              $repuestaTieneObservacion=1;
              return $repuestaTieneObservacion;
            }
            else{

                return $repuestaTieneObservacion=0; 
                 
            }
            
        
    } 

    
    public function actualizar_estatus_declaracion($id_declaracion,$estatus)
    {
        $model = new GenDeclaracionJO;
        $declaracion=$model->where('id',$id_declaracion)->get()->last();
        $declaracion->gen_status_id=$estatus;


        if ($declaracion->save()) {

              $estadoDeclaracionActualizada=1; 

          return $estadoDeclaracionActualizada;
        }
        
    }

    public function actualizar_estatus_planillas($id_declaracion_product=[],$id_declaracion)
    {
          DB::beginTransaction();
          foreach ($id_declaracion_product as $id_producto => $planilla) 
          {

            foreach ($planilla as $key => $estado_planilla) {

                  $model = new DetDeclaracionProduc;
                  $declaracion=$model->where('id',$id_producto)->get()->last();
                  $declaracion->$estado_planilla=0;
                  
                  if ($declaracion->save()) 
                  {
                      
                  }else{
                          $error=1;
                          Alert::warning('No es posible procesar la solicitud.','Error:A-E-P01!')->persistent("danger");
                          DB::rollback();
                  }
              }  
          }

          $error=@$error ?: 0;
          if ($error==0) {
            DB::commit();
          }

        return $error;

    }

    public function actualizar_estatus_bandeja($id_declaracion,$estatus)
    {

            $model= new GenBandejaEntradaDjo;
            $estatus_bandeja=$model->where('gen_declaracion_jo_id',$id_declaracion)->get()->last();
             
          // Si no esta la solicitud en bandeja asignala al usuario analista logueado y cambia el estado a atendida                                      
            if (empty($estatus_bandeja->id)) 
            {

               $atender_solicitud=$model;
               $atender_solicitud->gen_declaracion_jo_id=$id_declaracion;
               $atender_solicitud->gen_usuario_id=Auth::user()->id;
               $atender_solicitud->estado=$estatus;
  
                if ($atender_solicitud->save()) 
                {
                    // Actualiza tambien el estado de la solicitud en la tabla declaracion jo
                      $declaracionAtendida=$this->actualizar_estatus_declaracion($id_declaracion,$estatus);
                   
                    return $declaracionAtendida; 
                }                
            }

          // Si existe la solicitud en la bandeja actualiza los estados
            if (!empty($estatus_bandeja->id))
            {
                // Verifica que sea del analista logueado 
                // Verifica que se encuentre en estatus de atendida
                // Verifica que que estado actual sea con observacion o aprobada 
                if ($estatus_bandeja->gen_usuario_id==Auth::user()->id && $estatus_bandeja->estado>=10 && $estatus>=10) {

                   $estatus_bandeja->estado=$estatus;
                     
                     if($estatus_bandeja->save())
                     {
                         // Actualiza tambien el estado de la solicitud en la tabla declaracion jo
                          $declaracionActualizada=$this->actualizar_estatus_declaracion($id_declaracion,$estatus);
                       
                        return $declaracionActualizada; 
                     }
                }
            }

      return 0;
    }
    public function eliminarRP(Request $request)
    {
      // Metodo para Eliminar declaracion jurada de origen de prueba
      // idsolicitud
      // usuario
      // autorizacion
      
      //dd($request);
      // Consulta de la declaracion jurada de origen y todos sus datos asociados
      
      $modelAnalisis= new AnalisisCalificacion;
           $consulta=$modelAnalisis->where('num_solicitud',$idsolicitud)->get();

      //dd($consulta);

      $modelDeclaracion= new GenDeclaracionJO;
      $producDeclaracion= $modelDeclaracion
                        ->where('id',$idDeclaracion)
                        ->with('rDeclaracionProduc.rProducto.rArancelNan')
                        ->with('rDeclaracionProduc.rProducto.rArancelMer')
                        ->with('rDeclaracionProduc.rDeclaracionPaises.rPais')
                        ->with('rDetUsuario')
                        ->first();
    }

}
