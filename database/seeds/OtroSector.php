<?php

use Illuminate\Database\Seeder;
use App\Models\CatSectorInversionista;

class OtroSector extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $sectorInversionista = [
            [
                'id'         => 11,
                'nombre'       => 'Otro',
                'created_at' => '2021-08-24 20:59:08',
                'updated_at' => '2021-08-24 20:59:08',
            ],
            
            
        ];


        CatSectorInversionista::insert($sectorInversionista);
    }
}
