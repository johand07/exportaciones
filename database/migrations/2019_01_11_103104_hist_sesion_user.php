<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class HistSesionUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hist_sesion_user', function (Blueprint $table) {
          $table->engine = 'InnoDB';
          $table->increments('id');
          $table->unsignedInteger('gen_usuario_id')->index()->nullable();
          $table->foreign('gen_usuario_id')->references('id')->on('gen_usuario');
          $table->string('ip_usuario',100)->nullable();
          $table->dateTime('last_login')->nullable();
          $table->boolean('bactivo')->default(1);
          $table->boolean('sesion')->default(1);
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hist_sesion_user');
    }
}
