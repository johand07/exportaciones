<?php

use Illuminate\Database\Seeder;
use App\Models\CatMediotransporte;

class CatMedioTransp extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $mediotransp = [
            [
                'id'         => 1,
                'nombre'       => 'Aeronave',
                'created_at' => '2021-12-22 13:59:08',
                'updated_at' => '2021-12-22 13:59:08',
            ],
            [
                'id'         => 2,
                'nombre'       => 'Buque',
                'created_at' => '2021-12-22 13:59:08',
                'updated_at' => '2021-12-22 13:59:08',
            ],
            [
                'id'         => 3,
                'nombre'       => 'Ferrocarril',
                'created_at' => '2021-12-22 13:59:08',
                'updated_at' => '2021-12-22 13:59:08',
            ],
            [
                'id'         => 4,
                'nombre'       => 'Vehiculo de Carretera',
                'created_at' => '2021-12-22 13:59:08',
                'updated_at' => '2021-12-22 13:59:08',
            ],
        ];

        CatMediotransporte::insert($mediotransp);
    }
}
