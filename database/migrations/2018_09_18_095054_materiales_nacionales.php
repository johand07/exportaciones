<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MaterialesNacionales extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('materiales_nacionales', function (Blueprint $table) {
          $table->engine = 'InnoDB';
          $table->increments('id');
          $table->unsignedInteger('planilla_2_id')->index()->nullable();
          $table->foreign('planilla_2_id')->references('id')->on('planilla_2');
          $table->string('codigo', 100)->nullable();
          $table->string('descripcion', 100)->nullable();
          $table->string('nombre_produc_nac', 100)->nullable();
          $table->string('nombre_provedor', 100)->nullable();
          $table->string('incidencia_costo', 100)->nullable();
          $table->boolean('bactivo')->default(1);
          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('materiales_nacionales');
    }
}
