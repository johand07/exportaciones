<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>CERTIFICADO EXPORTA FACIL</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style type="text/css">
        .centrar{
            text-align: center;
        }

        .titulo{
            
            font-weight: bold;
        }

        .nro-correlativo{
            color: red;
            font-size: 16px;
        }

        .derecha{
            text-align: left;

        }

        .text-normal{
            font-size: 14px;
        }

        .text-medio{
            font-size: 12px;
        }
        .text-11{
            font-size: 11px;
        }
        .text-small{
            font-size: 10px;
        }

        .text-9{
            font-size: 9px;
        }

        .text-8{
            font-size: 8px;
        }

        .espacio-superior{
            margin-top: -40px;
        }

        .espacio-superior-small{
            margin-top: 5px;
        }

        .ancho-medio{
            width: 50%;
        }

        .ancho-full{
            width: 100%;
        }

        .ancho-medio-alto{
            width: 75%;
        }

        .ancho-90{
            width: 90%;
        }

        .table{
            border-collapse: collapse;
        }
        
        .borde{
            border: 1px solid #000;
        }

        .sin-borde{
            border: none;
        }

        .top{
            vertical-align: top;
        }

        .under-dotted{    
            border-bottom: 1px dotted #000;
            text-decoration: none;
        }

        .content-1{
            width: 5%;
        }

        .content-9{
            width: 20%;
        }

        .sangria-1{
            margin-left: 28px;
        }

        .sangria-2{
            margin-left: 36px;
        }

        .sangria-3{
            margin-left: 54px;
        }

        .sangria-sub{
            margin-left: 14px;
            
        }

        
        .page-break { page-break-before: always; }

        .table-center{
            margin: 0 auto; 
        }

        .justify{
            text-align: justify;
        }

        td.justify {
            padding-left: 8px;
            padding-right: 8px; 
        }
        .lineal{
            
        }
        .lineal>div{
            max-width:91%;

        }
        .lineal>*{
           display: inline-block;
           vertical-align:top;
        }

        .sangria-lista{
            list-style-type: none;
            margin: 0;
            padding: 0;
            margin-bottom: 4px;
        }

        .sangria-lista>li{
            margin-top: 5px;
            padding-left: 10px;
            padding-right: 10px;
        }

       .vuce{
             margin-top: 35px;
        float:right;
        }
    </style>
</head>
<body>
    <div class=" espacio-superior">
        <table class="ancho-full">
            <tr>
                <td>
                    <img class="img-responsive exporta" style="width: 200px;" colspan="50" src="img/exportafacil.png">
                </td>
                <td class="img-responsive vuce"><b><img src="data:image/png;base64,{!! base64_encode(QrCode::format('png')->size(300)->generate(URL::to('/api/VerificacionCertificadoExportaFacil/'.$certificado->id))) !!}" width="160px" height="145px"></b>
                </td>
                <td>
                    <img class="img-responsive vuce" style="width: 200px;" colspan="50" src="img/logo_exportadores.png">
                </td>
            </tr>
        </table>
    </div><br><br>
    <!--div class="centrar espacio-superior">
        <table class="ancho-full">
            <tr>
                <td class="nro-correlativo" style="width: 70%; font-size:107%; text-align: left;">
                     <b>N°</b><b style="color: black">{{ $certificado->id}}</b>
                </td>
                 <td class="nro-correlativo" style="width: 70%; font-size:107%; text-align: right;">
                     <b>CEF</b><b style="color: black"></b>
                </td>
            </tr>
        </table>
    </div><br-->
    <div class="centrar espacio-superior">
        <table class="ancho-full">
            <tr>
                <td class="center" style="width: 70%; font-size:100%; text-align: center;">
                     CERTIFICADO SIMPLIFICADO EXPORTA FACIL<br><br>
                </td>
            </tr>
        </table>
    </div>

<div class="centrar">
    <table class="ancho-full">
        <tr>
            <td class="top" style="text-align: left; font-size: 11px" colspan="50"><b>1. CODIGO/RÉGIMEN </b></td>
        </tr>
         <tr>
            <td class="top" style="text-align: left; font-size: 11px" colspan="30"><b>Código: </b>{{ $certificado->codigo}}</td>
            <td class="top" style="text-align: left; font-size: 11px" colspan="30"><b>Régimen: </b>{{$certificado->regimen}}</td>
            <td class="top" style="text-align: left; font-size: 11px" colspan="40"><b>Fecha: </b>{{date('d/m/Y',strtotime($certificado->created_at))}}</td>
        </tr>
    </table>
</div>
    <br>
<div class="centrar">
    <table class="ancho-full">
        <tr>
            <td class="top" style="text-align: left; font-size: 11px" colspan="50"><b>2. EXPORTADOR/REMITENTE </b></td>
        </tr>
         <tr>
            <td class="top" style="text-align: left; font-size: 11px" colspan="30"><b>Nombre de representante/Exportador: </b>{{ $certificado->nombre_repre_export}}</td>
            <td class="top" style="text-align: left; font-size: 11px" colspan="30"><b>Teléfono: </b>{{($certificado->telefono_exportador)}}</td>
            <td class="top" style="text-align: left; font-size: 11px" colspan="40"><b>Email: </b>{{$certificado->correo}}</td>
        </tr>
         <tr>
            <td class="top" style="text-align: left; font-size: 11px" colspan="30"><b>Dirección: </b>{{ $certificado->direccion_emisor}}</td>
            <td class="top" style="text-align: left; font-size: 11px" colspan="30"><b>RUD: </b>{{ $certificado->rud}}</td>
            <td class="top" style="text-align: left; font-size: 11px" colspan="40"><b>País Origen: </b>{{ $certificado->Pais->dpais}}</td>
        </tr>
         <tr>
            <td class="top" style="text-align: left; font-size: 11px" colspan="30"><b>Código Postal: </b>{{ $certificado->cod_postal}}</td>
            <td class="top" style="text-align: left; font-size: 11px" colspan="30"><b>Ciudad: </b>{{ $certificado->ciudad}}</td>
            <td class="top" style="text-align: left; font-size: 11px" colspan="40"><b>Fecha de Embarque: </b>{{date('d/m/Y',strtotime($certificado->created_at))}}</td>
        </tr>
    </table>
</div>
    <br>


<div class="centrar">
    <table class="ancho-full">
        <tr>
            <td class="top" style="text-align: left; font-size: 11px" colspan="50"><b>3. DESTINATARIO </b></td>
        </tr>
         <tr>
            <td class="top" style="text-align: left; font-size: 11px" colspan="50"><b>Destinatario: </b>{{$certificado->destinario}}</td>
            <td class="top" style="text-align: left; font-size: 11px" colspan="50"><b>Teléfono Destinatario: </b>{{$certificado->telefono_destinario}}</td>
            <td class="top" style="text-align: left; font-size: 11px" colspan="50"><b>Dirección: </b>{{$certificado->direccion_receptor}}</td>
        </tr>
         <tr>
            <td class="top" style="text-align: left; font-size: 11px" colspan="50"><b>País Destino: </b>{{$certificado->Pais->dpais}}</td>
            <td class="top" style="text-align: left; font-size: 11px" colspan="50"><b>Código Postal: </b>{{$certificado->cod_postal_destino}}</td>
            <td class="top" style="text-align: left; font-size: 11px" colspan="50"><b>Ciudad Destino: </b>{{$certificado->ciudad_destino}}</td>
        </tr>
    </table>
</div>
    <br>

<div class="centrar">
    <table class="ancho-full">
        <tr>
            <td class="top" style="text-align: left; font-size: 11px" colspan="50"><b>4. DETERMINACIÓN DE IMPUESTOS </b></td>
        </tr>
         <tr>
            <td class="top" style="text-align: left; font-size: 11px" colspan="25"><b>Tipo de Mercancia: </b>{{ $certificado->tipo_meracncia}}</td>
            <td class="top" style="text-align: left; font-size: 11px" colspan="25"><b>Total FOB: </b>{{ $certificado->total_fob}}</td>
            <td class="top" style="text-align: left; font-size: 11px" colspan="25"><b>Total Peso (kg): </b>{{$certificado->peso_estimado}}</td>
            <td class="top" style="text-align: left; font-size: 11px" colspan="25"><b>Alto: </b>{{$certificado->alto}}</td>
        </tr>
         <tr>
            <td class="top" style="text-align: left; font-size: 11px" colspan="30"><b>Ancho: </b>{{ $certificado->ancho}}</td>
            <td class="top" style="text-align: left; font-size: 11px" colspan="35"><b>Tratamiento Especial : </b>{{$certificado->tratamiento_especial}}</td>
            <td class="top" style="text-align: left; font-size: 11px" colspan="35"><b>Total bultos: </b>{{$certificado->total_bultos}}</td>
        </tr>
         <tr>
            <td class="top" style="text-align: left; font-size: 11px" colspan="25"><b>Tipo de Servicio: </b>{{$certificado->tipo_servicio}}</td>
            <td class="top" style="text-align: left; font-size: 11px" colspan="25"><b>Valor Total Flete sin I.V.A: </b>{{$certificado->valor_total_flete_sinIva}}</td>
            <td class="top" style="text-align: left; font-size: 11px" colspan="25"><b>Valor Total Flete con I.V.A: </b>{{$certificado->valor_total_flete_conIva}}</td>
            <td class="top" style="text-align: left; font-size: 11px" colspan="25"><b>Total Seguro: </b>{{$certificado->total_seguro}}</td>
        </tr>
    </table>
</div>
    <br>
     <div class="centrar">
        <table class="ancho-full">
        <tr>
            <td class="top" style="text-align: left; font-size: 11px" colspan="50"><b>5. DESCRIPCIÓN DE LA MERCANCÍA </b></td>
        </tr>
            <tr>
                <td  style="font-size: 11px" colspan="10"><b>Serie</b></td>
                 <td  style="font-size: 11px" colspan="20"><b>Subpartida Arancelaria</b></td>
                 <td  style="font-size: 11px" colspan="20"><b>Descripción de la Mercancia</b></td>
                 <td  style="font-size: 11px" colspan="10"><b>Valor Fob</b></td>
                 <td  style="font-size: 11px" colspan="10"><b>Cantidad</b></td>
                 <td  style="font-size: 11px" colspan="20"><b>Fecha</b></td>
                 <td  style="font-size: 11px" colspan="10"><b>Regimen Aplicable</b></td>
            </tr>


            @foreach($descrip_mercancia as $descrip)
            
                <tr>
                    <td style="font-size: 11px" colspan="10">{{$descrip->serie}}</td>
                    <td style="font-size: 11px" colspan="20">{{$descrip->sub_partida_arancel}}</td>
                    <td style="font-size: 11px" colspan="20">{{$descrip->descripcion}}</td>
                    <td style="font-size: 11px" colspan="10">{{$descrip->valor_fob}}</td>
                    <td style="font-size: 11px" colspan="10">{{$descrip->cantidad}}</td>
                    <td style="font-size: 11px" colspan="20">{{date('d/m/Y',strtotime($descrip->fecha))}}</td>
                    <td style="font-size: 11px" colspan="10">{{$descrip->regimen}}</td>
                </tr>
            @endforeach
           
        </table>
    </div>

     <div class="centrar">
        <table class="ancho-full">
        <tr>
            <td class="top" style="text-align: left; font-size: 11px" colspan="50"><b>6. DOCUMENTOS EXTRAS </b></td>
        </tr>
            <tr>
                <td  style="font-size: 11px" colspan="25"><b>Descripción de Documento</b></td>
                 <td  style="font-size: 11px" colspan="25"><b>Número de Documento</b></td>
                 <td  style="font-size: 11px" colspan="25"><b>Entidad Emisora</b></td>
                 <td  style="font-size: 11px" colspan="25"><b>Fecha</b></td>
            </tr>


            @foreach($catdocumentos as $documentos)
            
                <tr>
                    <td style="font-size: 11px" colspan="25">{{$documentos->CatDescripcionDoc->nombre}}</td>
                    <td style="font-size: 11px" colspan="25">{{$documentos->numero_documento}}</td>
                    <td style="font-size: 11px" colspan="25">{{$documentos->entidad_emisora}}</td>
                    <td style="font-size: 11px" colspan="25">{{date('d/m/Y',strtotime($documentos->fecha))}}</td>
                </tr>
            @endforeach
           
        </table>
    </div><br><br><br>

<div>
    <table>
        <tr>
            <td style="text-align: center; font-size: 12px;" colspan="80"><b>{{ $datosfirmante->nombre_firmante}} {{ $datosfirmante->apellido_firmante}}</b><br><b>{{ $datosfirmante->cargo_firmante}}</b><br>{{ $datosfirmante->gaceta_firmante}}</td>
        </tr>
    </table>
</div>
<br>









</body>
</html>