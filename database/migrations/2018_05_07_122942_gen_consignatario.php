<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GenConsignatario extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('gen_consignatario', function (Blueprint $table) {
          $table->engine = 'InnoDB';
          $table->increments('id');
          $table->unsignedInteger('gen_usuario_id')->index()->nullable();
          $table->foreign('gen_usuario_id')->references('id')->on('gen_usuario');
          $table->unsignedInteger('cat_tipo_cartera_id')->index()->nullable();         
          $table->foreign('cat_tipo_cartera_id')->references('id')->on('cat_tipo_cartera');
          $table->unsignedInteger('cat_tipo_convenio_id')->index()->nullable();         
          $table->foreign('cat_tipo_convenio_id')->references('id')->on('cat_tipo_convenio');
          $table->unsignedInteger('pais_id')->index()->nullable();
          $table->foreign('pais_id')->references('id')->on('pais');
          $table->string('nombre_consignatario', 100)->nullable();
          $table->string('perso_contac', 80)->nullable();
          $table->string('cargo_contac', 60)->nullable();
          $table->string('ciudad_consignatario', 100)->nullable();
          $table->string('direccion', 200)->nullable();
          $table->string('telefono_consignatario', 18)->nullable();
          $table->string('correo', 80)->nullable();
          $table->boolean('bactivo')->default(1);
          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      DB::statement('SET FOREIGN_KEY_CHECKS = 0');
      Schema::dropIfExists('gen_consignatario');
      DB::statement('SET FOREIGN_KEY_CHECKS = 1');
      
    }
}
