<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>COLOMBIA - VENEZUELA</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="css/style_Certificado.css">
</head>
<body>
    
    <!--div class="derecha titulo text-medio"><b style="color:red">N°</b> del Certificado <span class="">{{ $certificado->num_certificado}}</span></div-->
    <div class="centrar titulo text-normal espacio-superior">
        <table border="0" class="ancho-full">
            <tr>
                <td></td>
                <td class="ancho-medio centrar" colspan="3">
                    <div class="text-normal">CERTIFICADO DE ORIGEN ALADI</div>
                </td>
                <td></td>
                <td></td>
                
            </tr>
            <tr>
                <td></td>
                <td class="ancho-medio centrar" colspan="3">
                <div class="text-normal"> ACUERDO DE ALCANCE PARCIAL COLOMBIA - VENEZUELA</div>
                </td>
                <td></td>
                <td></td>
            </tr>
        </table>
       </div>
    <div class="centrar espacio-superior">
        <table border="0" class="ancho-full">
        <tr>
            <td class="ancho-medio centrar" colspan="3">
            <div class="text-normal">(1) PAÍS EXPORTADOR: <u>{{$certificado->pais_exportador}}</u></div>
            </td>
            <td class="ancho-medio centrar" colspan="3">
            <div class="text-normal">(2) PAÍS IMPORTADOR: <u>{{$certificado->pais_importador}}</u></div>
            </td>
        </tr>
        </table>
    </div>
    <div class="centrar espacio-superior">
        <table class="ancho-full borde table">
            <tr class="text-normal">
                <td class="centrar top titulo borde" style="width: 13%;" colspan="1">(3) No. de Orden</td>
                <td class="centrar top titulo borde" style="width: 30%;" colspan="1">(4) Código Arancelario en la nomenclatura de  la parte exportadora</td>
                <td class="centrar top titulo borde" style="width: 29%;" colspan="1">(5) Denominación de las Mercancías descripción arancelaria y comercial</td>
                <td class="centrar top titulo borde" style="width: 10%;" colspan="1">(6) Unidad física y Cantidad según Factura</td>
                <td class="centrar top titulo borde" style="width: 17%;" colspan="1">(7) Valor FOB de cada Mercancía según factura en US$</td>
                <td class="centrar top titulo borde" style="width: 20%; height:20%;" colspan="1">(8) Cantidad según  factura para cupos Capítulo 72</td>
            </tr>
           
            @foreach($detalleProduct as $value)
            
            <tr class="text-medio">
                <td class="centrar top borde" colspan="1" rowspan="5" style="width:13%;">{{$value->num_orden}}</td>
                <td class="centrar top borde" colspan="1" rowspan="5" style="width:30%;">{{$value->codigo_arancel}}</td>
                <td class="centrar top borde" colspan="1" rowspan="5">{{$value->denominacion_mercancia}}</td>
                <td class="centrar top borde" colspan="1" rowspan="5">{{$value->unidad_fisica}}</td>
                <td class="centrar top borde" colspan="1" rowspan="5">{{$value->valor_fob}}</td>
                <td class="centrar top borde" colspan="1" rowspan="5">{{$value->cantidad_segun_factura}}</td>
            </tr>
            
            
            @endforeach

            @if(count($detalleProduct)<=1)
                @for($auxConut = 0; $auxConut<=3-count($detalleProduct); $auxConut++)
                <tr class="text-medio">
                    <td class="centrar top">&nbsp;</td>
                    <td class="centrar top">&nbsp;</td>
                    <td class="centrar top">&nbsp;</td>
                    <td class="centrar top">&nbsp;</td>
                    <td class="centrar top">&nbsp;</td>
                    <td class="centrar top">&nbsp;</td>
                </tr>
                @endfor
            @endif
        
        </table>
    </div>
    <table class="ancho-full borde table">
        <tr class="text-normal">
            <td></td>
            <td class="centrar top titulo" style="width: 15%;" colspan="3" align="center">(9) DECLARACION DE ORIGEN
            </td>
            <td></td>
        </tr>
        <tr class="text-medio">
            <td class="centrar top titulo" style="width: 15%;" colspan="6">
                DECLARAMOS que las mercancías indicadas en el presente formulario, correspondientes a la(s) Factura(s) Comercial(es) No. <u>{{ $detdeclaracionCert[0]->numero_factura }}</u> de fecha <u>{{ $detdeclaracionCert[0]->f_fac_certificado }}</u> cumplen con lo establecido en las Normas de Origen del presente Acuerdo, de conformidad con el siguiente desglose.
            </td>
            
        </tr>
    </table>
    <div class="centrar espacio-superior">
        <table class="ancho-full borde table">
            <tr class="text-medio">
                <td class="centrar top titulo borde" style="width: 15%;">(10) No. de Orden</td>
                <td class="centrar top titulo borde" align="center" style="width: 85%;" colspan="5">(11) Criterio para la Calificación de Origen</td>
            </tr>
            @foreach($detdeclaracionCert as $value1)
            
                <tr class="text-center">
                    <td class="centrar top text-medio borde" rowspan="5">{{$value1->num_orden_declaracion}}</td>
                    <td class="centrar top text-medio" rowspan="5" style="width: 85%;">{{$value1->normas_criterio}}</td>
                </tr>

            @endforeach

            @if(count($detdeclaracionCert)<=1)
                @for($auxConut = 0; $auxConut<=3-count($detdeclaracionCert); $auxConut++)
                <tr class="text-medio">
                    <td class="centrar top">&nbsp;</td>
                    <td class="centrar top">&nbsp;</td>
                </tr>
                @endfor
            @endif
        
        </table>
    </div>

    <div class="centrar espacio-superior">
        <table class="ancho-full table borde">
            <tr class="text-medio">
                <td class="top titulo borde" style="width: 65%;" colspan="4">(12) PRODUCTOR O EXPORTADOR</td>
                 <td class="top titulo borde" colspan="3">(13) Sello Y firma del Productor o Exportador</td>
            </tr>
           
            <tr class="text-medio">
                <td class="top borde" colspan="4">12.1 Nombre o Razón Social: {{ $certificado->GenUsuario->DetUsuario->razon_social }}</td>
                <td colspan="2"></td>
            </tr>
            <tr class="text-medio">
                <td class="top borde" colspan="4">12.2 Número de Identificación Fiscal: {{ $certificado->GenUsuario->DetUsuario->rif }}</td>
                <td colspan="2"></td>
            </tr>
            <tr class="text-medio">
                <td class="top borde" colspan="4">12.3 Dirección: {{ $certificado->GenUsuario->DetUsuario->direccion }}</td>
                <td colspan="2"></td>
            </tr>
            <tr class="text-medio">
                <td class="top borde" colspan="4">12.4 País y Ciudad: {{ strtoupper($certificado->pais_cuidad_exportador) }}</td>
                <td colspan="2"></td>
            </tr>
            <tr class="text-medio">
                <td class="top borde" colspan="4">12.5 E-mail: {{ $certificado->GenUsuario->DetUsuario->correo }}</td>
                <td colspan="2"></td>
            </tr>
            <tr class="text-medio">
                <td class="top borde" colspan="4">12.6 Teléfono:{{$certificado->GenUsuario->DetUsuario->telefono_movil}}</td>
                <td colspan="2"></td>
            </tr>
            <tr class="text-medio">
                <td class="top borde" colspan="4">12.7 Fecha de Emisión: {{ $certificado->fecha_emision_exportador}}</td>
                <td colspan="2"></td>
            </tr>

            <tr class="text-medio">
                <td class="top titulo borde" colspan="4">(14) IMPORTADOR</td>
                <td colspan="2"></td>
            </tr>
            <tr class="text-medio">
                <td class="top borde" colspan="4">14.1 Nombre o Razón Social: {{ strtoupper($importadorCertificado->razon_social_importador) }}</td>
                <td colspan="2"></td>
            </tr>
            <tr class="text-medio">
                <td class="top borde" colspan="4">14.2 Dirección: {{ strtoupper($importadorCertificado->direccion_importador) }}</td>
                <td colspan="2"></td>
            </tr>
            <tr class="text-medio">
                <td class="top borde" colspan="4">14.3 País y Ciudad: {{ strtoupper($importadorCertificado->pais_ciudad_importador) }}</td>
                <td colspan="2"></td>
            </tr>
            <tr class="text-medio">
                <td class="top borde" colspan="4">14.4 E-mail: {{ $importadorCertificado->email_importador}}</td>
                <td colspan="2"></td>
            </tr>
            <tr class="text-medio">
                <td class="top borde" colspan="4">14.5 Teléfono: {{ $importadorCertificado->telefono}}</td>
                <td colspan="2"></td>
            </tr>
            <tr class="text-medio">
                <td class="top borde" colspan="4">(15) Medio de Transporte (si es conocido): {{ ($importadorCertificado->medio_transporte_importador) }}</td>
                <td colspan="2"></td>
            </tr>
            <tr class="text-medio">
                <td class="top borde" colspan="4">(16) Puerto o Lugar de Embarque (si es conocido): {{ ($importadorCertificado->lugar_embarque_importador) }}</td>
                <td colspan="2"></td>
            </tr>
        </table>
         <table class="ancho-full borde table">
            <tr class="text-normal">
                <td class="centrar top titulo borde" style="width: 15%; height:50%;" colspan="6" ><b>17. OBSERVACIONES: {{$certificado->observaciones}}</b>
                </td>
            </tr>
        </table>
    </div>
    <div class="centrar espacio-superior">
        <table class="ancho-full borde table">
            <tr class="text-medio">
                <td class="top titulo" style="width: 50%; height: 50%;" colspan="3">(18) CERTIFICACIÓN DE ORIGEN</td>
                <td class="top borde text-medio" style="width: 50%; height:50%;" colspan="3" rowspan="1">(19) Nombre y firma del funcionario habilitado y sello de la Autoridad Competente</td>
            </tr>
            <tr class="text-medio">
                <td class="top text-medio" style="width: 50%;" colspan="3" rowspan="1"><div class="">Certifico la veracidad de la presente Declaracion en la ciudad de:<br><span class="under-dotted espacio-superior">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><br><span class="espacio-superior">A los: <u>{{date('d')}}</u>/<u>{{date('m')}}</u>/<u>{{date('Y')}}</u><br><br></span></div></td>
            </tr>
        </table>
    </div>

<div class="page-break"></div>
    <table class="ancho-full borde table">
        <tr class="text-medio">
            <td class="centrar top titulo borde"  align="center "style="width: 15%;" colspan="6">(INSTRUCTIVO PARA EL LLENADO DEL CERTIFICADO DE ORIGEN ACUERDO DE ALCANCE PARCIAL COLOMBIA - VENEZUELA
            </td>
        </tr>
        <tr>
            <td class="centrar top titulo borde text-medio" style="width: 15%;" colspan="6">
                CAMPOS DEL CERTIFICADO DE ORIGEN
            </td>
        </tr>
        <tr>
            <td class="centrar top titulo borde" style="width: 10%;" colspan="6">
              <b>NÚMERO DEL CERTIFICADO:</b> Corresponde a un número que la Autoridad Competente asigna a los certificados de origen que emite. Este campo solo debe ser llenado por dicha<br/> autoridad  
            </td>
        </tr>
        <tr>
            <td class="centrar top titulo borde" style="width: 10%;" colspan="6">
              (1)</b><div class="text-9 sangria-sub"><b>PAÍS EXPORTADOR:</b> Indicar el nombre del país de exportacion del cual la mercancía es originaria.</div> 
            </td>
        </tr>
        <tr>
            <td class="centrar top titulo borde" style="width: 10%;" colspan="6">
               <b class="text-9 sangria-1">(2)</b><div class="text-9 sangria-sub"><b>PAÍS IMPORTADOR:</b> Indicar el nombre del país de destino de la mercancía a exportar.</div>
            </td>
        </tr>
        <tr>
            <td class="centrar top titulo borde" style="width: 10%;" colspan="6">
                <b class="text-9 sangria-1">(3)</b><div class="text-9 sangria-sub"><b>NÚMERO DE ORDEN:</b> Numerar en forma consecutiva las mercancias que ampara el Certificado. En caso que el espacio sea insuficiente, se continuará la numeración de la mercancía en otro ejemplar, situación que se deberá indicar en el campo de "Observaciones".</div>
            </td>
        </tr>
        <tr>
            <td class="centrar top titulo borde" style="width: 10%;" colspan="6">
               <b class="text-9 sangria-1">(4)</b><div class="text-9 sangria-sub"><b>CÓDIGO ARANCELARIO:</b> Indicar la clasificación arancelaria de la mercancia a exportar utilizando la nomenclatura vigente de la parte exportadora.</div> 
            </td>
        </tr>
        <tr>
            <td class="centrar top titulo borde" style="width: 10%;"  colspan="6">
               <b class="text-9 sangria-1">(5)</b><div class="text-9 sangria-sub"><b>DENOMINACIÓN DE LAS MERCANCÍAS:</b> Indicar la descripción arancelaria de la mercancia y su denominación comercial, expresada en términos suficientemente como para permitir su identificacion y clasificacion a nivel de subpartida, asi como las caracteristicas, tipo, clase, modelo, dimensiones, capacidad, etc. </div> 
            </td>
        </tr>
        <tr>
            <td class="centrar top titulo borde" style="width: 10%;"  colspan="6">
               <div class="lineal"><b class="text-9 sangria-1">(6)</b><div class="text-9 sangria-sub"><b>UNIDAD FÍSICA SEGUN FACTURA:</b> Indicar la cantidad y la unidad de medida por cada número de orden.</div></div> 
            </td>
        </tr>
        <tr>
            <td class="centrar top titulo borde" style="width: 10%;" colspan="6">
               <div class="lineal"><b class="text-9 sangria-1">(7)</b><div class="text-9 sangria-sub"><b>VALOR FOB EN DÓLARES:</b> Indicar el Valor FOB de la mercancia en dólares de los Estados Unidos por cada número de orden. Este valor debe coincidir con el indicado en la factura comercial.</div></div> 
            </td>
        </tr>
        <tr>
            <td class="centrar top titulo borde" style="width: 10%;" colspan="6">
               <div class="lineal"><b class="text-9 sangria-1">(8)</b><div class="text-9 sangria-sub"><b>CANTIDAD SEGÚN FACTURA PARA CUPOS CAPITULO 72:</b> Para los cupos con criterio de origen general de las partidas 7210, 7214, 7215, 7216 y 7217 indicar únicamente el monto total de la exportacion en toneladas.</div></div> 
            </td>
        </tr>
        <tr>
            <td class="centrar top titulo borde" style="width: 10%;" colspan="6">
                <div class="lineal"><b class="text-9 sangria-1">(9)</b><div class="text-9 sangria-sub"><b>DECLARACION DE ORIGEN:</b> Deben llenarse los espacios correspondientes a factura comercial No. y Fecha.</div></div>
            </td>
        </tr>
        <tr>
            <td class="centrar top titulo borde  text-10" style="width: 10%;" colspan="6">
               <li><span><u>Factura(s) Comercial(es) No.:</u> indicar el(los) números de factura(s) comercial(es) que ampara la exportacion</span></li> 
            </td> 
        </tr>
        <tr>
           <td class="centrar top titulo borde text-10" style="width: 10%;" colspan="6">
                <li><span><u>Fecha:</u> Indicar la efecha de emisi{on de la factura comercial.</span></li> 
            </td> 
        </tr>
        <tr>
            <td class="centrar top titulo borde text-10" style="width: 10%;" colspan="6">
               <li>Cuando la mercancia originaria sea facturada por un operador de un país distinto al de origen de la mercancial, sea o no Parde del Cacuerdo, en el campo relativo "Observaciones" del certificado de origen se deberá señalar que la mercancia será facturada por ese operador, indicando el nombre, denominacion o razon social y domicilio de quien en definitiva facture a destino, asi como el numero y la fecha de factura comercial correspondiente.</li>  
            </td> 
        </tr>
        <tr>
            <td class="centrar top titulo borde text-10" style="width: 10%;" colspan="6">
                <li>En caso de existir mas de una factura comercial debera hacerce la aclaracion en la casilla de "Observaciones", indicacndo numeros y fechas corrrespondientes.</li> 
            </td> 
        </tr>
        <tr>
            <td class="centrar top titulo borde text-10" style="width: 10%;" colspan="6">
              <b class="text-10">(10)<b>NÚMERO DE ORDEN:</b> Este número de orden deberá corresponder con el(los) número(s) de orden especificado(s) en la casilla (3). 
            </td>
        </tr>
        <tr>
            <td class="centrar top titulo borde text-medio" colspan="6">
                <b>(11)</b><b>CRITERIO PARA LA CALIFICACIÓN DE ORIGEN:</b> En esta casilla se debe identificar el criterio de origen que cumple la mercancia a exportar individualizada por su número de orden Los criterios de origen establecidos en este Acuerdo, deberán citarse de la forma en que aparecen en la columna derecha del siguiente cuadro explicativo.
            </td>
        </tr>
    </table>
    <br><br>
<div class="lineal"><b class="text-9 sangria-1">(10)</b><div class="text-9 sangria-sub"><b>NÚMERO DE ORDEN:</b> Este número de orden deberá corresponder con el(los) número(s) de orden especificado(s) en la casilla (3).</div></div>
<div class="lineal"><b class="text-9 sangria-1">(11)</b><div class="text-9 sangria-sub"><b>CRITERIO PARA LA CALIFICACIÓN DE ORIGEN:</b> En esta casilla se debe identificar el criterio de origen que cumple la mercancia a exportar individualizada por su número de orden Los criterios de origen establecidos en este Acuerdo, deberán citarse de la forma en que aparecen en la columna derecha del siguiente cuadro explicativo.</div></div>
<div class="text-8 ancho-full espacio-superior centrar">
    <table class=" table borde table-center ancho-90">  
            <tr><td class="ancho-medio borde titulo centrar text-medio" colspan="3" style="height:50%">CRITERIO DE ORIGEN</td><td class="ancho-medio borde titulo centrar text-medio" colspan="3" style="height:50%;">IDENTIFICACIÓN DEL CRITERIO EN EL CERTIFICADO</td></tr>
            <tr><td class="borde top justify text-medio" colspan="3" style="height:50%;">Las mercancias obtenidas en su totalidad o producidas entramente en territorio de una o ambas Partes.</td><td class="borde centrar text-medio" colspan="3" style="height:50%;">Anexo II, Arículo 3 parrado I. Literal a)</td></tr>
            <tr><td class="borde top justify text-medio" colspan="3" style="height:50%;">Las mercancias que sean producidas enteramente en territorio de una Parte, a partir exclusivamente de materiales que califican como originarios de conformidad con el Régimen de Origen del Anexo II del Acuerdo.</td><td class="borde centrar text-medio" colspan="3" style="height:50%;">Anexo II, Articulo 3 parrado I. Literal b)</td></tr>
            <tr><td class="borde top justify text-medio" colspan="3" style="height:50%;">Las mercancias que en su elaboración utilicen matriales no originarios, seran considerados originarias cuando cumplan con los requisitos especificos de origen previstos en el Apéndice 1 del Anexo II del Acuerdo. Los requisitos especificos de origen prevalecerán sobre los criterios establecidos en el Anexo II. Artículo 3 párrafo 1. Literal d).</td><td class="borde centrar text-medio" colspan="3" style="height:50%;">Anexo II, Articulo 3 parrado I. Literal c)</td></tr>
            <tr><td class="borde top justify text-medio" colspan="3" style="height:50%;">Las mercancias que incorporen en su producción materiales no originarios, que resulten de un proceso de ensamblaje o montaje, realizado en el territorio de cualquiera de las Partes, siempre que en su elaboración se utilicen materiales originarios y el valor CIF de los materiales no originarios no exceda el 50 porciento valor FOB de exportacion de la mercancia.</td><td class="borde centrar text-medio" colspan="3" style="height:50%;">Anexo II, Articulo 3 parrado I. Literal d), inciso i.</td></tr>
            <tr><td class="borde top justify text-medio" colspan="3" style="height:50%;">Las mercancias que incorporen en si produccion materiales no originarios que resulten de un proceso de transformacion, distinto al ensamblaje o montaje, realizado en el territorio de cualquiera de las partes, que les confiera una nueva individualidad. Esa nueva individualidad implica que, en el Sistema Armonizado, las mercancias se clasifiquen en una partida diferente a aquellas en las que se clasifiquen cada uno de los materiales no originarios.</td><td class="borde centrar text-medio" colspan="3" style="height:50%;">Anexo II, Articulo 3 parrado I. Literal d), inciso ii.</td></tr>
            <tr><td class="borde top justify text-medio" colspan="3" style="height:50%;">Las mercancias que incorporen en su produccion materiales no originarios y que no complan con lo establecido en el literal d) inciso ii, Articulo 3 del Anexo II porque el proceso de transformacion, distinto al ensamblaje o montaje, realizado en el territorio de cualquiera de las Partes no implica un cambio de partida arancelaria, deberan cumplir con que el valor CIF de los materiales no originarios no exceda el 50% del valor FOB de exportacion de la mercancia y que en su elaboracion se utilicen materiales originarios de Las Partes.</td><td class="borde centrar text-medio" colspan="3" style="height:50%;">Anexo II, Articulo 3 parrado I. Literal d), inciso iii.</td></tr>
            <tr><td class="borde top justify text-medio" colspan="3" style="height:50%;">Los juegos surtidos, clasificados segun las Reglas Generales para la interpretacion de la Nomenclatura 1, 3 o 6 del Sistema Armonizado, seran considerados originarios cuando todas las mercancias que los componen sean originarias. Sin embargo, cuando un juego o surtido esté compuesto por mercancias originarias y mercancias no originarias, ese juego o surtido sera considerado originario en su conjunto, si el valor CIF de las mercancias no originarios no excede el 15% del precio FOB del juego o surtido.</td><td class="borde centrar text-medio" colspan="3" style="height:50%;">Anexo II, Articulo 7</td></tr>
    </table>
</div>
<table>
    <tr>
        <td colspan="6" class="text-medio">
          <b>12) PRODUCTOR O EXPORTADOR:</b></div> 
        </td>
    </tr>
     <tr>
        <td colspan="6" class="text-medio">
         12.1<u>Nombre o Razón Social:</u> Indicar los datos de la persona natural o juridica qie realiza la exportación. 
        </td>
    </tr>
    <tr>
        <td colspan="6" class="text-medio">
            12.2<u>Número de Identificacion Fiscal:</u> Indicar el número de identificacion fiscal o tributaria de la persona natural o juridica que realiza la exportacion.
        </td>
    </tr>
    <tr>
        <td colspan="6" class="text-medio">
          12.3<u>Direccion:</u> Indicar el domicilio legal o registrado para los efectos fiscales de la persona natural o juridica que realiza la exportacion.  
        </td>
    </tr>
    <tr>
        <td colspan="6" class="text-medio">
           12.4<u>País y Ciudad:</u> Indicar el nombre del país y la ciudad donde se encuentra el domicilio legal o registrado para efectos fiscales de la persona natural o juridica que realiza la exportacion.
        </td>
    </tr>
    <tr>
        <td colspan="6" class="text-medio">
         12.5<u>E-mail:</u> Indicar el correo electronico de la persona natural o juridica que realiza la exportacion.   
        </td>
    </tr>
    <tr>
        <td colspan="6" class="text-medio">
           12.6</span><u>Teléfono:</u> Indicar el numero telefonico de la persona natural o juridica que realiza la exportacion. 
        </td>
    </tr>
    <tr>
        <td colspan="6" class="text-medio">
          12.7<u>Fecha de emisión:</u> Indicar la fecha en la cual el certificado de origen fue llenado y firmado por la persona natural o juridica que realiza la exportacion.  
        </td>
    </tr>
    <tr>
        <td colspan="6" class="text-medio">
         <b>(13) SELLO Y FIRMA DEL PRODUCTOR O EXPORTADOR:</b> Este campo debe ser completado con la firma de la persona natural o juridica que realiza la exportación. 
        </td>
    </tr>
    <tr>
        <td colspan="6" class="text-medio">
          <b>(14)IMPORTADOR</b>  
        </td>
    </tr>
    <tr>
        <td colspan="6" class="text-medio">
         14.1 Nombre o Razón Social: Indicar los datos de la persona natural o juridica qie realiza la importación.
        </td>
    </tr>
    <tr>
        <td colspan="6" class="text-medio">
          14.2 Direccion: Indicar el domicilio legal o registrado para los efectos fiscales de la persona natural o juridica que realiza la importación.
        </td>
    </tr>
    <tr>
        <td colspan="6" class="text-medio">
          14.3 País y Ciudad: Indicar el nombre del país y la ciudad donde se encuentra el domicilio legal o registrado para efectos fiscales de la persona natural o juridica que realiza la importación. 
        </td>
    </tr>
    <tr>
        <td colspan="6" class="text-medio">
         14.4 E-mail: Indicar el correo electronico de la persona natural o juridica que realiza la importación. 
        </td>
    </tr>
    <tr>
        <td colspan="6" class="text-medio">
          14.5 Teléfono: Indicar el numero telefonico de la persona natural o juridica que realiza la importación. 
        </td>
    </tr>
    <tr>
        <td colspan="6" class="text-medio">
           <b>(15)MEDIO DE TRANSPORTE:</b> Indique el tipo de transporte previsto para el desplazamiento de la mercancia. 
        </td>
    </tr>
    <tr>
        <td colspan="6" class="text-medio">
         <b>(16)PUERTO O LUGAR DE EMBARQUE:</b> Indique el nombre del lugar de embarque de las mercancias.  
        </td>
    </tr>
    <tr>
        <td colspan="6" class="text-medio">
         <b>(17) OBSERVACIONES:</b> En este espacio se puede incluir cualquier observacion y/o aclaracion que considere necesaria, además de las previstas especificamente en este instructivo y/o el Acuerdo, tales como: 
        </td>
    </tr>
    <tr>
        <td colspan="6" class="text-medio top titulo borde" style="width: 10%;">
            <li>Fecha de recepcion de la declaracion jurada a que hace refeencia el Articulo 16 Anexo II.</li> 
        </td>
    </tr>
    <tr>
        <td colspan="6" class="text-medio top titulo borde" style="width: 10%;">
             <li>La continuacion de la numeracion de las mercancias en otro ejemplar del certificado en caso que el espacio sea insuficiente.</span></li>
        </td>
    </tr>
    <tr>
        <td colspan="6" class="text-medio top titulo borde" style="width: 10%;">
             <li>Facturacion de la mercancia por un operador de un pais distinto al de origen de la mercancia sea o no Parte del Acuerdo, didicando el nombre , denominacion o razon social y domicilio de quien en definitiva facture la operacion a destino asi como el numero y la fecha de la factura comercial correspondiente.</li>
        </td>
    </tr>
    <tr>
        <td colspan="6" class="text-medio top titulo borde" style="width: 10%;">
            <li>Para el caso de los cupos para los productos del Capitulo 72 indique "los bienes clasificados en la partida cumplen con lo establecido en el Apéndice I Anexo II".</li>
        </td>
    </tr>
    <tr>
        <td colspan="6" class="text-medio top titulo borde" style="width: 10%;">
           <b>(18) CERTIFICACION DE ORIGEN:</b> Este campo solo debe ser llenado por la Autoridad Competente.
        </td>
    </tr>
    <tr>
        <td colspan="6" class="text-medio top titulo borde" style="width: 10%;">
          <b class="text-10">(19)</b><b>NOMBRE Y FIRMA DE FUNCIONARIO HABILITADO Y SELLO DE LA AUTORIDAD COMPETENTE:</b> Este campo debe ser llenado con el nombre y la firma autógrafa del funcionario habilitado por las Partes para tal efecto, así como el sello de la Autoridad Competente
        </td>
    </tr>
    <tr>
        <td colspan="6" class="text-medio top titulo borde" style="width: 10%;">
          <div class="text-medio">Nota:</div>
            <div class="text-medio">El llenado de todas las casillas del certificado de origen será obligatorio, con excepción de las casillas 14 y 15 que serán llenadas si la informacion es conocida, y la casilla 17 que sera llenada cuando corresponda.</div>  
        </td>
    </tr>
</table>
</body>
</html>