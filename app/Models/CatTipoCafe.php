<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CatTipoCafe extends Model
{
    protected $table = 'cat_tipo_cafe';

    protected $guarded = [];
}
