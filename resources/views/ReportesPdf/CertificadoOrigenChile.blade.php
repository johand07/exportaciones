<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>CERTIFICADO DE ORIGEN ASOCIACION LATINOAMERICANA DE INTEGRACION ACUERDO DE COMPLEMENTACION ECONOMICA No. 23</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style type="text/css">
        .centrar{
            text-align: center;
        }

        .titulo{
            
            font-weight: bold;
        }

        .nro-correlativo{
            color: red;
            font-size: 16px;
        }

        .derecha{
            text-align: left;

        }

        .text-normal{
            font-size: 14px;
        }

        .text-medio{
            font-size: 12px;
        }
        .text-11{
            font-size: 11px;
        }
        .text-small{
            font-size: 10px;
        }

        .text-9{
            font-size: 9px;
        }

        .text-8{
            font-size: 8px;
        }

        .espacio-superior{
            margin-top: 10px;
        }

        .espacio-superior-small{
            margin-top: 5px;
        }

        .ancho-medio{
            width: 50%;
        }

        .ancho-full{
            width: 100%;
        }

        .ancho-medio-alto{
            width: 75%;
        }

        .ancho-90{
            width: 90%;
        }

        .table{
            border-collapse: collapse;
        }
        
        .borde{
            border: 1px solid #000;
        }

        .sin-borde{
            border: none;
        }

        .top{
            vertical-align: top;
        }

        .under-dotted{    
            border-bottom: 1px dotted #000;
            text-decoration: none;
        }

        .content-1{
            width: 5%;
        }

        .content-9{
            width: 20%;
        }

        .sangria-1{
            margin-left: 28px;
        }

        .sangria-2{
            margin-left: 36px;
        }

        .sangria-3{
            margin-left: 54px;
        }

        .sangria-sub{
            margin-left: 14px;
            
        }

        
        .page-break { page-break-before: always; }

        .table-center{
            margin: 0 auto; 
        }

        .justify{
            text-align: justify;
        }

        td.justify {
            padding-left: 8px;
            padding-right: 8px; 
        }
        .lineal{
            
        }
        .lineal>div{
            max-width:91%;

        }
        .lineal>*{
           display: inline-block;
           vertical-align:top;
        }

        .sangria-lista{
            list-style-type: none;
            margin: 0;
            padding: 0;
            margin-bottom: 4px;
        }

        .sangria-lista>li{
            margin-top: 5px;
            padding-left: 10px;
            padding-right: 10px;
        }
    </style>
</head>
<body>
 <div class="centrar espacio-superior">
        <table class="ancho-full">
            <tr>
                <td class="top titulo borde" style="width: 85%; font-size:107%;">
                     CERTIFICADO DE ORIGEN<br>
                     ASOCIACION LATINOAMERICANA DE INTEGRACION<br>
                     ACUERDO DE COMPLEMENTACION ECONOMICA No. 23
                </td>
                <td>&nbsp;&nbsp;</td>
            </tr>
        </table>
    </div>

<div class="centrar espacio-superior">
        <table class="ancho-full borde table">
            <tr class="text-11">
                <td class="top borde" colspan="50"><b>1. PAIS EXPORTADOR</b> <br><br> {{$certificado->pais_exportador}}</td>
                <td class="top borde" colspan="50"><b>2. PAIS IMPORTADOR</b> <br><br> {{$certificado->pais_importador}}<br><br></td>
            </tr>

            <tr class="text-11">
                <td class="top borde" colspan="35"><b>3. DATOS DEL PRODUCTOR</b> <br><br>
                    {{$certificado->razon_social_productor}} <br>
                    {{$certificado->direccion_productor}}
                </td>
                <td class="top borde" colspan="30"><b>4. DATOS DEL EXPORTADOR</b><br><br>
                    {{$det_usuario->razon_social}}<br>
                    {{$det_usuario->direccion}}
                    <br><br></td>
                <td class="top borde" colspan="35"><b>5. DATOS DEL IMPORTADOR</b> <br><br>
                    {{$importadorCertificado->razon_social_importador}}<br>
                    {{$importadorCertificado->direccion_importador}}<br><br></td>
            </tr>

            <tr class="text-9">
                <td class="top titulo borde" colspan="35">6. RUT/RIF PRODUCTOR: {{$certificado->rif_productor}}</td>
                <td class="top titulo borde" colspan="30">7. RUT/RIF EXPORTADOR: {{$det_usuario->rif}}<br><br></td>
                <td class="top titulo borde" colspan="35">8. RUT/RIF IMPORTADOR: {{$importadorCertificado->rif_importador}} <br><br></td>
            </tr>

            <tr class="text-11">
                <td class="top borde" colspan="35"><b>9. PUERTO O LUGAR DE EMBARQUE</b><br><br>{{$importadorCertificado->lugar_embarque_importador}}<br><br><br></td>
                <td class="top borde" colspan="30"><b>10. FACTURA COMERCIAL No.:</b><br><br>{{$detdeclaracionCert[0]->numero_factura}}<br><br><br><br></td>
                <td class="top" colspan="35"> <b>FECHA:</b><br><br>{{$detdeclaracionCert[0]->f_fac_certificado}}<br><br><br></td>
            </tr>

            <tr class="text-11">
                <td class="top titulo borde" colspan="12">11. No. DE ORDEN</td>
                <td class="top titulo borde" colspan="10">12. CODIGO ARANCELARIO</td>
                <td class="top titulo borde" colspan="54">13. DENOMINACION DE LAS MERCANCIAS</td>
                <td class="top titulo borde" colspan="12">14. CANTIDAD Y MEDIDA</td>
                <td class="top titulo borde" colspan="12">15. VALORES FOB EN US$</td>
            </tr>
            @foreach ($detalleProduct as $key => $detalleP)
            <tr class="text-11">
                <td class="top borde" align="center" colspan="12">{{$detalleP->num_orden}}<br><br></td>
                <td class="top borde" align="center" colspan="10">{{$detalleP->codigo_arancel}}<br><br></td>
                <td class="top borde" align="center" colspan="54">{{$detalleP->denominacion_mercancia}}<br><br></td>
                <td class="top borde" align="center" colspan="12">{{$detalleP->unidad_fisica}}<br><br></td>
                <td class="top borde" align="center" colspan="12">{{$detalleP->valor_fob}}<br><br></td>
            </tr>
            @endforeach

            <tr class="text-11">
                <td class="top borde" colspan="12"><b>16. No. DE ORDEN</b></td>
                <td class="top borde" colspan="88"><b>17. NORMAS DE ORIGEN</b></td>
            </tr>
            @foreach ($detdeclaracionCert as $key => $detalleCert)
            <tr class="text-11">
                <td class="top borde" align="center" colspan="12">{{$detalleCert->num_orden_declaracion}}<br><br></td>
                <td class="top borde" colspan="88">{{$detalleCert->normas_criterio}}<br><br></td>
            </tr>
            @endforeach
            
            <tr class="text-11">
                <td class="top borde" colspan="100"><b>18. OBSERVACIONES</b><br><br>{{$certificado->observaciones}}<br><br><br></td>
            </tr>

            <tr class="text-11">
                <td class="top borde" colspan="50"><b>19. DECLARACION DEL EXPORTADOR</b><br><br>{{$det_usuario->razon_social}}</td>
                <td class="top borde" colspan="50"><b>20. DECLARACION DE ORIGEN </b><br><br><br></td>

            </tr>
            <tr class="text-medio">
                <td class="top titulo borde" colspan="50">Declaro que las mercancías indicadas en el presente formulario,<br>correspondientes a la factura comercial que se cita, cumplen con lo <br>establecido en las Normas de Origen del Acuerdo de <br>Complementación Económica No. 23.<br><br><br><br><br><br><br><br>Sello y Firma del Exportador</td>

                <td class="top titulo borde" colspan="50">Certifico la veracidad de la presente declaración que formalizo <br>en la ciudad de ___________________en esta fecha<br><br><br><br><br><br><br><br><br><br>Nombre, Sello y Firma de la Entidad Certificadora</td>

            </tr>
             
        </table>
    <div class="derecha titulo text-11">NOTA: ESTE FORMATO ES DE LIBRE REPRODUCCION</div>

</div>






</body>
</html>