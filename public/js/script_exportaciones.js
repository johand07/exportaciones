/* script data table agente aduanal y data table consignatari*/
$(document).ready(function() {
    $('#listaAgenteAduanal').DataTable({  
      searching: true,      
      language: {
              "lengthMenu": "Mostrar _MENU_ registros",
              "zeroRecords": "No se encontraron resultados",
              "info": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
              "infoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
              "infoFiltered": "(filtrado de un total de _MAX_ registros)",
              "sSearch": "Buscar:",
              "oPaginate": {
                  "sFirst": "Primero",
                  "sLast":"Último",
                  "sNext":"Siguiente",
                  "sPrevious": "Anterior"
         },
         "sProcessing":"Procesando...",
          },
      //para usar los botones   
      responsive: "true",
      dom: 'Bfrtilp',       
      buttons:[ 
    {
      extend:    'excelHtml5',
      text:      '<i class="glyphicon glyphicon-download-alt"></i>Excel ',
      titleAttr: 'Exportar a Excel',
      className: 'btn btn-outline-secondary'
    },
    {
      extend:    'pdfHtml5',
      text:      '<i class="glyphicon glyphicon-download-alt"></i>PDF ',
      titleAttr: 'Exportar a PDF',
      className: 'btn btn-outline-secondary'
    },
    
  ]	        
  });
    $('#tablaconsignatario').DataTable();

    
    $('#listaUsuarios').DataTable();


    $('#listadua').DataTable();
    $('#listapermiso').DataTable();
    $('#tablaFacturas').DataTable({  
      searching: true,      
      language: {
              "lengthMenu": "Mostrar _MENU_ registros",
              "zeroRecords": "No se encontraron resultados",
              "info": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
              "infoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
              "infoFiltered": "(filtrado de un total de _MAX_ registros)",
              "sSearch": "Buscar:",
              "oPaginate": {
                  "sFirst": "Primero",
                  "sLast":"Último",
                  "sNext":"Siguiente",
                  "sPrevious": "Anterior"
         },
         "sProcessing":"Procesando...",
          },
      //para usar los botones   
      responsive: "true",
      dom: 'Bfrtilp',       
      buttons:[ 
    {
      extend:    'excelHtml5',
      text:      '<i class="glyphicon glyphicon-download-alt"></i>Excel ',
      titleAttr: 'Exportar a Excel',
      className: 'btn btn-outline-secondary'
    },
    {
      extend:    'pdfHtml5',
      text:      '<i class="glyphicon glyphicon-download-alt"></i>PDF ',
      titleAttr: 'Exportar a PDF',
      className: 'btn btn-outline-secondary'
    },
    
  ]	        
  });
    $('#listacredito').DataTable();
    $('#listaDebito').DataTable();
    $('#listaAnticipo').DataTable();
    $('#listaBotones').DataTable();
    $('#listaer').DataTable({  
      searching: true,      
      language: {
              "lengthMenu": "Mostrar _MENU_ registros",
              "zeroRecords": "No se encontraron resultados",
              "info": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
              "infoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
              "infoFiltered": "(filtrado de un total de _MAX_ registros)",
              "sSearch": "Buscar:",
              "oPaginate": {
                  "sFirst": "Primero",
                  "sLast":"Último",
                  "sNext":"Siguiente",
                  "sPrevious": "Anterior"
         },
         "sProcessing":"Procesando...",
          },
      //para usar los botones   
      responsive: "true",
      dom: 'Bfrtilp',       
      buttons:[ 
    {
      extend:    'excelHtml5',
      text:      '<i class="glyphicon glyphicon-download-alt"></i>Excel ',
      titleAttr: 'Exportar a Excel',
      className: 'btn btn-outline-secondary'
    },
    {
      extend:    'pdfHtml5',
      text:      '<i class="glyphicon glyphicon-download-alt"></i>PDF ',
      titleAttr: 'Exportar a PDF',
      className: 'btn btn-outline-secondary'
    },
    
  ]	        
  });
    $('#notasdedebito').DataTable();
    $('#listaventa').DataTable({  
      searching: true,      
      language: {
              "lengthMenu": "Mostrar _MENU_ registros",
              "zeroRecords": "No se encontraron resultados",
              "info": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
              "infoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
              "infoFiltered": "(filtrado de un total de _MAX_ registros)",
              "sSearch": "Buscar:",
              "oPaginate": {
                  "sFirst": "Primero",
                  "sLast":"Último",
                  "sNext":"Siguiente",
                  "sPrevious": "Anterior"
         },
         "sProcessing":"Procesando...",
          },
      //para usar los botones   
      responsive: "true",
      dom: 'Bfrtilp',       
      buttons:[ 
    {
      extend:    'excelHtml5',
      text:      '<i class="glyphicon glyphicon-download-alt"></i>Excel ',
      titleAttr: 'Exportar a Excel',
      className: 'btn btn-outline-secondary'
    },
    {
      extend:    'pdfHtml5',
      text:      '<i class="glyphicon glyphicon-download-alt"></i>PDF ',
      titleAttr: 'Exportar a PDF',
      className: 'btn btn-outline-secondary'
    },
    
  ]	        
  });
    $('#listaAnalista').DataTable();
    $('#listaExtranjera').DataTable();
    $('#listaTransfeTecno').DataTable();
    $('#listaCoordExtrajro').DataTable();
    $('#listaCdInvExtranjera').DataTable();
    $('#listaCoordTransfTecno').DataTable();
    $('#listaCertificados').DataTable();
    $('#listaDeclaracionInversion').DataTable();
    $('#listaUsersInversiones').DataTable();
    $('#listaInsai').DataTable();
    $('#listaResguardo').DataTable();
    $('#listaCna').DataTable();
    $('#listaCvc').DataTable();
    $('#asignadasIntra').DataTable();
    //$('#totalSolicitudes').DataTable();
    $('#solicitudesSuspendidos').DataTable();
    $('#solicitudesCierreConformes').DataTable();
    $('#solicitudesCierreParciales').DataTable();
    $('#solicitudesEviadasArchivos').DataTable();
    $('#ReporsolicitudesSuspendidos').DataTable();
    $('#ReporsolicitudesCierreConformes').DataTable();
    $('#ReporsolicitudesCierreParciales').DataTable();
    $('#ReporsolicitudesEviadasArchivos').DataTable();
    
    var pregunta=$('#preguntas_seg').val()
    if(pregunta==8){
      $('#preg_alternativa').show('show');
    }
    //script sector y actividad economica
var sector=$("#cat_sector_inversionista_id").change(function(event) {
        //console.log('sector');
        //console.log(event.target.value);
        $.get('BandejaInversionista/ActividadEcoInversionista/'+event.target.value+'',function(response, sector){
            //console.log(response);
            $("#actividad_eco_emp").empty();
            for (var i =0; i < response.length; i++) {
                $("#actividad_eco_emp").append("<option value='"+response[i].id+"'>"+response[i].nombre+"</option>");
            }
        });
    });

var sector=$("#cat_sector_inversionista_id_edit").change(function(event) {
        //console.log('sector');
        //console.log(event.target.value);
        
        let sol=$('#gen_sol_inversionista_id').val();
        console.log('gen_sol_inversionista_id');
        console.log(sol);
        $.get('/ActividadEcoInversionista/'+event.target.value+'',function(response, sector){
            //console.log(response);
            $("#actividad_eco_emp_edit").empty();
            for (var i =0; i < response.length; i++) {
                $("#actividad_eco_emp_edit").append("<option value='"+response[i].id+"'>"+response[i].nombre+"</option>");
            }
        });
    });



    //script sector y actividad economica
var sector=$("#id_gen_sector").change(function(event) {
        $.get('/ActividadEco/'+event.target.value+'',function(response, sector){
            //console.log(response);
            $("#id_gen_actividad_eco").empty();
            for (var i =0; i < response.length; i++) {
                $("#id_gen_actividad_eco").append("<option value='"+response[i].id+"'>"+response[i].dactividad+"</option>");
            }
        });
    });


//Script para combo estado municipio parroquia

    var estado=$("#id_estado").change(function(event) {
        $.get('/minicipios/'+event.target.value+'',function(response, estado){
            //console.log(response);
            $("#id_municipio").empty();
            for (var i =0; i < response.length; i++) {
                if(response.length == 1){
                    $("#id_municipio").append("<option value=''> --Seleccione una Opción--</option");
                    //$("#id_municipio").append("<option value='"+response[i].id+"'>"+response[i].municipio+"</option>");

                }
                $("#id_municipio").append("<option value='"+response[i].id+"'>"+response[i].municipio+"</option>");
            }
        });
    });

    var municipio=$("#id_municipio").change(function(event) {
        $.get('/parroquias/'+event.target.value+'',function(response, municipio){
            //console.log(response);
            $("#id_parroquia").empty();
            for (var i =0; i < response.length; i++) {
                if(response.length == 1){
                    $("#id_parroquia").append("<option value=''> --Seleccione una Opción--</option");
                    //$("#id_parroquia").append("<option value='"+response[i].id+"'>"+response[i].municipio+"</option>");

                }
                $("#id_parroquia").append("<option value='"+response[i].id+"'>"+response[i].parroquia+"</option>");
            }
        });
    });


    operadorcambiario();

    //$("#modal").modal("show");
} );


/*script para calendarios bootstrap*/
$('.input-group.date').datepicker({
    format: "yyyy-mm-dd",//formato de la fecha
    todayBtn: "linked",//al oprimir el boton hoy automaticamente te agrela la fecha actual en el campo
    clearBtn: true,//boton para limpiar el campo
    language: "es",//lenguaje del calendario
    orientation: "bottom left",//ubicacion del calendario alrededor del campo
    keyboardNavigation: true,//ni idea
    //daysOfWeekDisabled: "0,6", //bloquea los dias que se le indiquen
    daysOfWeekHighlighted: "0,6", //resalta los dias que se le indiquen
    autoclose: true, //permite que se cierre el calendario al seleccionar la fecha
    todayHighlight: true, //resalta la fecha de hoy
    beforeShowMonth: function (date){
                  if (date.getMonth() == 8) {
                    return false;
                  }
                },
    //defaultViewDate: { year: 1977, month: 04, day: 25 }
});

//js que configura las calendario entre dos campos, o dos fechas
    $('.input-daterange').datepicker({
        format: "yyyy/mm/dd",//formato de la fecha
        todayBtn: "linked",//al oprimir el boton hoy automaticamente te agrela la fecha actual en el campo
        clearBtn: true,//boton para limpiar el campo
        language: "es",//lenguaje del calendario
        orientation: "bottom left",//ubicacion del calendario alrededor del campo
        keyboardNavigation: true,//ni idea
        forceParse: false,
        //daysOfWeekDisabled: "0,6", //bloquea los dias que se le indiquen
        daysOfWeekHighlighted: "0,6", //resalta los dias que se le indiquen
        autoclose: true, //permite que se cierre el calendario al seleccionar la fecha
        todayHighlight: true, //resalta la fecha de hoy
        beforeShowMonth: function (date){
            if (date.getMonth() == 8) {
                return false;
            }
        },
        //defaultViewDate: { year: 1977, month: 04, day: 25 }
    });
/* calendario con solo año*/
$('#fecha_grafico').datepicker({
  format: " yyyy",
  viewMode: "years", 
  minViewMode: "years",
  todayBtn: "linked",//al oprimir el boton hoy automaticamente te agrela la fecha actual en el campo
  clearBtn: true,//boton para limpiar el campo
  language: "es",//lenguaje del calendario
  orientation: "bottom left",//ubicacion del calendario alrededor del campo
  keyboardNavigation: true,//ni idea
  forceParse: false,
  //daysOfWeekDisabled: "0,6", //bloquea los dias que se le indiquen
  daysOfWeekHighlighted: "0,6", //resalta los dias que se le indiquen
  autoclose: true, //permite que se cierre el calendario al seleccionar la fecha
  todayHighlight: true, //resalta la fecha de hoy
});  

/*Habilitar campos ocultos una vez seleccionada una opción */

$('input:radio[name="forma_pago_id"]').change( function(){

 if($(this).is(':checked')) {

    if($(this).val()==2) {

    $('#plazo').css('display', 'block');
    $('#forma_plazo').css('display', 'block');
    $('#fecha_estimada_pago').val("");
     } else {

    $('#plazo').css('display', 'none');
    $('#forma_plazo').css('display', 'none');
    $('#plazo_pago').val('');

    }
    if($(this).val()==1) {
    $('#include_cuotas').css('display','none');
    }
  }
});
/****************************************************************/
$('input:radio[name="tipo_instrumento_id"]').change( function(){

 if($(this).is(':checked')) {

    if($(this).val()==3) {

    $('#cual_instrumento').css('display', 'block');



     } else {

    $('#cual_instrumento').css('display', 'none');
    $('#especif_instrumento').val('');
    }
  }
});

/*****************************************************************/
$('input:radio[name="forma_pago_id"]').change( function(){

 if($(this).is(':checked')) {

    if( ($(this).val()==1) || ($(this).val()==3) ) {

    $('#fecha_est_pago').css('display', 'block');
    $('#plazo_pago').val("");
    $('input[name="tipo_plazo"]').each(function(){

      $(this).attr('checked',false);

    });



     } else {

    $('#fecha_est_pago').css('display', 'none');
    $('#especif_instrumento').val('');
    }
  }
});

/**********************************************************************/

$('input:radio[name="tipo_plazo"]').change( function(){

 if($(this).is(':checked')) {

    if( $(this).val()=="Total" ) {

    $('#fecha_est_pago').css('display', 'block');
    $('#include_cuotas').css('display','none');
    $('input[name="num_cuotas[]"],input[name="monto[]"],input[name="fecha_pago[]"]').each(function() { $(this).val(""); });

    $('#seccion2').css('display', 'block');
    $('#botones').css('display', 'none');

     } else {

    $('#fecha_est_pago').css('display', 'none');
    $('#especif_instrumento').val('');
    $('#seccion2').css('display','none');
    $('#include_cuotas').css('display','block');
    $('#botones').css('display','block');

     }
  }
});
/***********************************************************************/

$(document).on('click','#next',function(){

     $('#seccion1').css('display','none');
     $('#seccion2').css('display','block');
     $('#include_cuotas').css('display','none');
     $('#botones').css('display','none');

});


/**********************************************************************/
$('#otro_monto').keyup( function(){

 if(this.value.length >=1) {

    $('#cual_pago').css('display', 'block');

     } else {

    $('#cual_pago').css('display', 'none');


  }
});


function sleep(time){


   return new Promise((resolve) => setTimeout(resolve, time));
  
}


/***********************************************************************/

function calcularMonto(row_id,cantidad,precio,xtotal){


  sleep(1000).then(() => {


  var xtotal='montoFob';
  var row_table = document.getElementById(row_id);
  var cant_formateada=row_table.querySelectorAll('#'+cantidad)[0];
  var cant = row_table.querySelectorAll('#'+cantidad)[0].value;
 
  var precio_formateada=row_table.querySelectorAll('#'+precio)[0];
  var prec = row_table.querySelectorAll('#'+precio)[0].value;
  

  var total = row_table.querySelectorAll('#'+xtotal)[0];

  //var calculo =(cant.replace('.','').replace(',','.')* prec.replace('.','').replace(',','.'));
  var calculo=cant*prec;

  total.value = calculo.toFixed(2);

 /******** Obtener la suma total de los productos ********************/
  var indice;
  var suma=0;
  

  var x=document.querySelectorAll('#montoFob');

  
     for(indice in x){

    
      if(typeof x[indice].value !="undefined") {
  
       suma+=((x[indice].value));
                   
       }

      
   }


  console.log(suma);
 /*******************************************************************/

 /***Mostrar mensaje de error si el monto ingresado es mayor al monto fob ***/

  var monto=$('#monto_data').data('monto');


     if(suma > monto){

        swal('Operación denegada','La sumatoria de los montos es mayor al monto fob ingresado','warning');
        document.querySelector('#id_boton').style.display="none";
      
      }else{

       document.querySelector('#id_boton').style.display="block";

   }

  /********************************************************************/


 


 });

}

function calcularMontoTotal(){

  var con=0;
  var mfob=document.getElementById('monto_fob').value;
  var mflete=document.getElementById('monto_flete').value;
  var mseguro=document.getElementById('monto_seguro').value;
  var motro=document.getElementById('otro_monto').value;

 if(mfob!=""){ con+=parseFloat(mfob); }

 if(mflete!=""){ con+=parseFloat(mflete); }

 if(mseguro!=""){ con+=parseFloat(mseguro); }

 if(motro!=""){ con+=parseFloat(motro); }

  var total=document.getElementById('monto_total');
  
  total.value=con.toFixed(2);
 
  document.getElementById('monto_fob').value = mfob.toFixed(2);
}
/*******************************************************************/
$(document).ready(function() {



$(document).on('click','#add',function(){


    $('#cod_arancel*').prop('disabled',false);


});

$(document).on('click','#cod_arancel, #cod_arancel1',function(){


    $('#buscador_arancel').keyup();


});

/************ Mostrar modal y buscador de productos en ( create-update) ***/
$(document).on('keyup','#buscador_arancel',function(){

  var metodo=$('input[name="metodo"]').val();
  var tipoAran=localStorage.getItem('arancel_id');
  if(metodo=="Crear")
   {
   $.ajax({

      type:'get',
      url: 'create',
      data: {'valor': $(this).val(),'tipoAran': tipoAran},

      success: function(data){

        $table='<table class="table table-bordered table-hover" id="tabla"><tr><th>Código Arancelario</th><th>Descripción</th></tr>';

        $('#arancel').html($table);
        if(data.length==0){

         $('#tabla tr:last').after('<tr><td colspan="2" style="color:red;text-align:center" >Códigos no existen</td></tr>');

       }else{


        $.each(data,function(index,value){

       $('#tabla tr:last').after('<tr class="data"><td>'+value.codigo+'</td><td>'+value.descripcion+'</td><tr></table');

        });

      }
      }

   });

 }else if(metodo == 'Edit'){

   $.ajax({

      type:'get',
      url: 'edit',
      data: {'valor': $(this).val(),'tipoAran': tipoAran},
      success: function(data){

        $table='<table class="table table-bordered table-hover" id="tabla"><tr><th>Código Arancelario</th><th>Descripción</th></tr>';

        $('#arancel').html($table);
        if(data.length==0){

         $('#tabla tr:last').after('<tr><td colspan="2" style="color:red;text-align:center" >Códigos no existen</td></tr>');

       }else{
        $.each(data,function(index,value){



       $('#tabla tr:last').after('<tr class="data1"><td>'+value.codigo+'</td><td>'+value.descripcion+'</td><tr></table');

        });

        }
      }

   });




 }else{

  $.ajax({

     type:'get',
     url: 'edit',
     data: {'valor': $(this).val(),'tipoAran': tipoAran},
     success: function(data){

       $table='<table class="table table-bordered table-hover" id="tabla"><tr><th>Código Arancelario</th><th>Descripción</th></tr>';

       $('#arancel').html($table);
       if(data.length==0){

        $('#tabla tr:last').after('<tr><td colspan="2" style="color:red;text-align:center" >Códigos no existen</td></tr>');

      }else{
       $.each(data,function(index,value){



      $('#tabla tr:last').after('<tr class="data"><td>'+value.codigo+'</td><td>'+value.descripcion+'</td><tr></table');

       });

       }
     }

  });




}




});
/*----------------------Edición de datos de la empresa---------------------------------------------------*/

$('input:radio[name="tipo_arancel"]').change( function(){




     if($(this).is(':checked')) {

       var parametros = {
               'id': $(this).val()

       };
       localStorage.setItem('arancel_id',parametros.id);
       var token= $('input[name="_token"]').val();
    //   console.log(token);
       $('#id_arancel').val(parametros.id);
      $('#cod_arancel_edit').prop('disabled',false);
       $.ajax({
           type: 'GET',
           url: '/Arancel',
           data: {'arancel': $(this).val(),'_token': token},
           success: function(data){

              $table='<table class="table table-bordered table-hover" id="tabla"><tr><th>Código Arancelario</th><th>Descripción</th></tr>';

              $('#arancel').html($table);
              $.each(data,function(index,value){

             $('#tabla tr:last').after('<tr class="data"><td>'+value.codigo+'</td><td>'+value.descripcion+'</td></tr></table>');

              });

           }

       });

    }
});

$('#cod_arancel_edit').show('modal');


/**------------------------------*/
 var global_id=localStorage.setItem('arancel'," ");


 $(document).on('click','#cod_arancel',function(){

   var id=$(this).parents("tr").attr('id');
   localStorage.setItem('arancel',id);

});

 $(document).on('click','tr.data',function(){

  var id=localStorage.getItem('arancel');
  var cod=$(this).find("td").eq(0).text();
  var ara=$(this).find("td").eq(1).text();

  $("tr#"+id).find("td").eq(0).children("input").val(cod);
  $("tr#"+id).find("td").eq(1).children("input").val(ara);

  $('#modal button').click();
  $('#buscador_arancel').val("");



  });

  $(document).on('click','tr.data1',function(){

    var id=localStorage.getItem('arancel');
    var cod=$(this).find("td").eq(0).text();
    var ara=$(this).find("td").eq(1).text();

    if ( $('#valid').val() == 1 ) {
      $("#cod_arancel1").val(cod);
      $("#descrip_arancel1").val(ara);
    }else{
      $("tr#"+id).find("td").eq(0).children("input").val(cod);
      $("tr#"+id).find("td").eq(1).children("input").val(ara);
    }
  
    
    var id = '';
    $('#modal button').click();
    $('#buscador_arancel').val("");
  
  
  
    });

 /*$(document).on('keyup','#buscador_arancel',function(){

 var tipo=$('input[name="tipo_arancel"]').val();

   $.ajax({

      type:'get',
      url: '/Arancel',
      data: {'valor': $(this).val(), 'tipoAran':tipo},
      success: function(data){

        $table='<table class="table table-bordered table-hover" id="tabla"><tr><th>Código Arancelario</th><th>Descripción</th></tr>';

        $('#arancel').html($table);
        if(data.length==0){

         $('#tabla tr:last').after('<tr><td colspan="2" style="color:red;text-align:center" >Códigos no existen</td></tr>');

       }else{
        $.each(data,function(index,value){

       $('#tabla tr:last').after('<tr class="data"><td>'+value.codigo+'</td><td>'+value.descripcion+'</td><tr></table');

        });

      }
      }

   });

});

 */

/*---------------------------------------------------------------------------*/

/*************************************************************************/

var forma_pago=document.getElementsByName('forma_pago_id');
var tipo_inst=document.getElementsByName('tipo_instrumento_id');
var plazo=document.getElementsByName('tipo_plazo');
var forma_plazo=document.getElementById('forma_plazo');
var x= document.getElementById('plazo');
var y=document.getElementById('cual_instrumento');
var z=document.getElementById('cual_pago');
var fecha=document.getElementById('fecha_est_pago');
var fecha_id=document.getElementById('fecha_estimada_pago');
var mon_fle=document.getElementById('monto_flete');
var mon_fob=document.getElementById('monto_fob');
var mon_seg=document.getElementById('monto_seguro');
var otroM=document.getElementById('otro_monto');
var montoTotal=document.getElementById('monto_total');


if(typeof forma_pago[0] !== "undefined" && forma_pago[0].checked){

   fecha.style.display="block";
    x.value="";
    plazo.value="";


}else if(typeof forma_pago[1] !== "undefined" &&  forma_pago[1].checked){

  fecha.style.display="block";
  x.value="";
  plazo.value="";

}else if(typeof forma_pago[2] !== "undefined" && forma_pago[2].checked){

    fecha_id.value="";
    x.style.display="block";
    forma_plazo.style.display="block";

}

if(typeof plazo[0] !== "undefined" && plazo[0].checked){

  fecha.style.display="block";

}else if(typeof plazo[1] !== "undefined" && plazo[1].checked){

  $('#seccion2').css('display','none');
  $('#include_cuotas').css('display','block');
  $('#botones').css('display','block');

}

if(!y == false)
 (typeof tipo_inst[2] !== "undefined" && tipo_inst[2].checked) ? y.style.display="block": y.style.display="none";
if(!z == false)
 (otroM.value !="") ? z.style.display="block": z.style.display="none";
if(typeof montoTotal !== "undefined" && !montoTotal == false)
   montoTotal.readOnly=true;


 /*$(document).on('keyup','#monto_fob,#monto_flete,#monto_seguro,#otro_monto',function(){

    montoTotal.value=parseInt(mon_fob.value)+parseInt(mon_fle.value)+parseInt(mon_seg.value)+parseInt(otroM.value);

    montoTotal.readOnly=true;
 });*/





});



/********************************************************************/
//funcion para cambio de pregunta de seguridad por pregunta alternativa

function preguntaSeg (){
    var pregunta=$('#preguntas_seg').val()
    //alert(pregunta);
    if (pregunta == 8) {
        $('#lista_preg').hide('slow');
        $('#preg_alternativa').show('show');
        $('#preguntas_seg').attr('noreq','noreq');
        $('#pregunta_alter').removeAttr('noreq');
    }else{

        $('#preg_alternativa').hide('slow');
        $('#preguntas_seg').removeAttr('noreq');
        $('#pregunta_alter').attr('noreq','noreq');

    }

    $('#botonbacklist').click(function(event) {
        $('#preg_alternativa').hide('slow');
        $('#lista_preg').show('show');
        $('#preguntas_seg').removeAttr('noreq');
        $('#pregunta_alter').attr('noreq','noreq');
    });

}

function regimenExport (){
    var regimen=$('#cat_regimen_export_id').val()
    //alert(regimen);

    if (regimen == 2) {
        /*$('#lista_preg').hide('slow');*/
        $('#RegExportacion').show('show');
        $('#ordinaria,#Financiamiento').hide('slow');
        $('#cat_regimen_especial_id').removeAttr('noreq');


    }
    if(regimen == 1){
      $('#RegExportacion,#OtroRegimenExpo').hide('slow');
      $('#ordinaria').show('show');
      $('#cat_regimen_especial_id').val(null);
      $('#cat_regimen_especial_id').attr('noreq','noreq');
        //var v=$('#cat_regimen_especial_id').val()
        //alert(v);
    }





}

function regimenEspecial(){

  var regimen_especi=$('#cat_regimen_especial_id').val()
       // alert(regimen_especi);

          if (regimen_especi == 3) {
            $('#OtroRegimenExpo').show('show');
            $('#OtroRegimenExpo').removeAttr('noreq');
          }

          if (regimen_especi == 2 || regimen_especi == 1) {
            $('#OtroRegimenExpo').hide('show');
            $('#OtroRegimenExpo').attr('noreq','noreq');
          }
}

function tiposolictud (){
    var tiposolicitud=$('#tipo_solicitud_id').val()
    //alert(pregunta);
    if (tiposolicitud == 2) {
        //$('#lista_preg').hide('slow');
        $('#tipotecnologia').show('show');
    }else{

        $('#tipotecnologia').hide('slow');

    }

    /*$('#boton').click(function(event) {
        $('#preg_alternativa').hide('slow');
        $('#lista_preg').show('show');
    });*/

}


/*Funcion ocultar y mostrar el gen operador cambiario*/
function operadorcambiario (){

  var oca=$('#gen_tipo_usuario_id').val()
    if (oca == 3) {
        $('#oca').show('show');
    }

    if (oca == 2) {
       $('#oca').hide('show');
    }

    
}
/************************************************************************/

/************ Metodo para seleccionar un arancel del modal ************/

 var global_id=localStorage.setItem('id_gl'," ");


 $(document).on('click','#cod_arancel',function(){

   var id=$(this).parents("tr").attr('id');
   localStorage.setItem('id_gl',id);
   console.log(id);

});

 

/************************************************************************/



//funcion para eliminar************************************
function eliminar(ruta,eli, idtabla){

/*creo una cookie con la id a eliminar*/
    document.cookie = "elimi="+eli;
swal({
/*Cargo el alert para confirmar o declinar*/
    title: "¿Eliminar?",
    text: "¿Está seguro de eliminar este Registro?",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: "#DD6B55",
    confirmButtonText: "Si!, Borrarlo!",
    cancelButtonText: "No, Cancelar!",
    closeOnConfirm: false,
    closeOnCancel: false,
    showLoaderOnConfirm: true
    },

    function(isConfirm){
    if (isConfirm) {

/*si confirmo ubico la id en las cookies*/
        var lista = document.cookie.split(";");
         for (i in lista) {
             var busca = lista[i].search("elimi");
             if (busca > -1) {micookie=lista[i]}
             }
         var igual = micookie.indexOf("=");
         var valor = micookie.substring(igual+1);

/*Asigno la id a una variable*/
    valorCaja2 = valor;
/*paso la id para su eliminacion via ajax*/

        $.ajax({    //AgenteAduanal.destroy O exportador/AgenteAduanal/{AgenteAduanal}
            url: ruta+valorCaja2,
            type: 'DELETE',

            data: {
                    '_token'  : $('#token').val(),
                    'id': valorCaja2

                },
        })
        .done(function(data) {

            //console.log();
            //var valor= data.valor_caso;

            //alert(valor);
            console.log(data);
       /* for (var i = 0; i < data.length; i++) {

            var valor = data.valor_caso;
            alert(data[i]['valor_caso']);

        }*/

       // var datas=data;
       // alert(datas);


            /*$.each(data, function(index, val) {
                alert(val);
            });*/

            if (data == 1) {
                swal('Eliminado !', 'Eliminado Exitosamente', 'success',{ button: "Ok!",});
                $("#"+idtabla).load(" #"+idtabla);
                //location.reload();
                //setTimeout('document.location.reload()',1000);
            }
            if (data == 2) {
                swal('Error al Eliminar !', 'No se pudo Eliminar', 'error',{ button: "Ok!",});
                $("#"+idtabla).load(" #"+idtabla);
                //location.reload();
                //setTimeout('document.location.reload()',1000);
            }

            if (data == 3) {
                var mnsj=data.mensaje;
                swal('NO PUEDE SER ELIMINADO!', 'Esta asociado a una DUA', 'error',{ button: "Ok!",});
                $("#"+idtabla).load(" #"+idtabla);
            }
            if (data == 4) {
                var mnsj=data.mensaje;
                swal('NO PUEDE SER ELIMINADO!', 'Esta asociado a una FACTURA', 'error',{ button: "Ok!",});
                $("#"+idtabla).load(" #"+idtabla);
            }
            if (data == 5) {
                var mnsj=data.mensaje;
                swal('NO PUEDE SER ELIMINADO!', 'Esta asociado a una FACTURA', 'error',{ button: "Ok!",});
                $("#"+idtabla).load(" #"+idtabla);
            }
            /*if (data.mnsj == 'DUA') {
                swal('NO PUEDE SER ELIMINADO!', 'Esta asociado a una '+data, 'error',{ button: "Ok!",});
                $("#"+idtabla).load(" #"+idtabla);
                //location.reload();
                //setTimeout('document.location.reload()',1000);
            }*/




        })
        .fail(function(jqXHR, textStatus, thrownError) {
            //errorAjax(jqXHR,textStatus)
            console.log(jqXHR)
            console.log(textStatus)

        })

    } else {
    swal("Cancelado", "No se ha procesado la eliminación", "warning");   } });
}


/***********************eliminar accionistas******************/
function eliminarAccio(ruta,eli, idtabla){
    alert(eli)
/*creo una cookie con la id a eliminar*/
    document.cookie = "elimi="+eli;
swal({
/*Cargo el alert para confirmar o declinar*/
    title: "¿Eliminar?",
    text: "¿Está seguro de eliminar este Registro?",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: "#DD6B55",
    confirmButtonText: "Si!, Borrarlo!",
    cancelButtonText: "No, Cancelar!",
    closeOnConfirm: false,
    closeOnCancel: false,
    showLoaderOnConfirm: true
    },

    function(isConfirm){
    if (isConfirm) {

/*si confirmo ubico la id en las cookies*/
        var lista = document.cookie.split(";");
         for (i in lista) {
             var busca = lista[i].search("elimi");
             if (busca > -1) {micookie=lista[i]}
             }
         var igual = micookie.indexOf("=");
         var valor = micookie.substring(igual+1);

/*Asigno la id a una variable*/
    valorCaja2 = valor;
/*paso la id para su eliminacion via ajax*/

        $.ajax({    //AgenteAduanal.destroy O exportador/AgenteAduanal/{AgenteAduanal}
            url: ruta,
            type: 'DELETE',

            data: {
                    '_token'  : $('#token').val(),
                    'id': eli

                },
        })
        .done(function(data) {

            //console.log();
            //var valor= data.valor_caso;

            //alert(valor);
            console.log(data);
       /* for (var i = 0; i < data.length; i++) {

            var valor = data.valor_caso;
            alert(data[i]['valor_caso']);

        }*/

       // var datas=data;
       // alert(datas);


            /*$.each(data, function(index, val) {
                alert(val);
            });*/

            if (data == 1) {
                swal('Eliminado !', 'Eliminado Exitosamente', 'success',{ button: "Ok!",});
                $("#"+idtabla).load(" #"+idtabla);
                //location.reload();
                //setTimeout('document.location.reload()',1000);
            }
            if (data == 2) {
                swal('Error al Eliminar !', 'No se pudo Eliminar', 'error',{ button: "Ok!",});
                $("#"+idtabla).load(" #"+idtabla);
                //location.reload();
                //setTimeout('document.location.reload()',1000);
            }

            if (data == 3) {
                var mnsj=data.mensaje;
                swal('NO PUEDE SER ELIMINADO!', 'Esta asociado a una DUA', 'error',{ button: "Ok!",});
                $("#"+idtabla).load(" #"+idtabla);
            }
            if (data == 4) {
                var mnsj=data.mensaje;
                swal('NO PUEDE SER ELIMINADO!', 'Esta asociado a una FACTURA', 'error',{ button: "Ok!",});
                $("#"+idtabla).load(" #"+idtabla);
            }
            if (data == 5) {
                var mnsj=data.mensaje;
                swal('NO PUEDE SER ELIMINADO!', 'Esta asociado a una FACTURA', 'error',{ button: "Ok!",});
                $("#"+idtabla).load(" #"+idtabla);
            }
            /*if (data.mnsj == 'DUA') {
                swal('NO PUEDE SER ELIMINADO!', 'Esta asociado a una '+data, 'error',{ button: "Ok!",});
                $("#"+idtabla).load(" #"+idtabla);
                //location.reload();
                //setTimeout('document.location.reload()',1000);
            }*/




        })
        .fail(function(jqXHR, textStatus, thrownError) {
            errorAjax(jqXHR,textStatus)

        })

    } else {
    swal("Cancelado", "No se ha procesado la eliminación", "warning");   } });
}
/********fin eliminar accionistas*****************************/


/**************************************************************/

//VALIDACION DE RIF ,CEDULA REGISTRO USUARIO
function CedulaFormat(vCedulaName,mensaje,postab,escribo,evento) {
    tecla=getkey(evento);


    vCedulaName.value=vCedulaName.value.toUpperCase();
    vCedulaValue=vCedulaName.value;
    valor=vCedulaValue.substring(2,12);
    var numeros='0123456789/';
    var digit;
    var noerror=true;
    if (shift && tam>1) {
    return false;
    }
    for (var s=0;s<valor.length;s++){
    digit=valor.substr(s,1);
    if (numeros.indexOf(digit)<0) {
    noerror=false;
    break;
    }
    }
    tam=vCedulaValue.length;
    if (escribo) {
    if ( tecla==8 || tecla==37) {
    if (tam>2)
    vCedulaName.value=vCedulaValue.substr(0,tam-1);
    else
    vCedulaName.value='';
    return false;
    }
    if (tam==0 && tecla==69) {
    vCedulaName.value='E-';
    return false;
    }
    if (tam==0 && tecla==69) {
    //vCedulaName.value='E-';
    return false;
    }
    if (tam==0 && tecla==86) {
    vCedulaName.value='V-';
    return false;
    }
        if (tam==0 && tecla==74) {
    vCedulaName.value='J-';
    return false;
    }
    if (tam==0 && tecla==71) {
    vCedulaName.value='G-';
    return false;
    }
    if (tam==0 && tecla==80) {
    vCedulaName.value='P-';
    return false;
    }

    else if ((tam==0 && ! (tecla<14 || tecla==69 || tecla==86 || tecla==46)))
    return false;
    else if ((tam>1) && !(tecla<14 || tecla==16 || tecla==46 || tecla==8 || (tecla >= 48 && tecla <= 57) || (tecla>=96 && tecla<=105)))
    return false;
    }
}




/**********************************VALIDACION PARA LOS CAMPOS CON PORCENTAJE***********************/

function soloNumerosletrasyPorcentaje(e){
   key = e.keyCode || e.which;
   //alert(key)
   tecla = String.fromCharCode(key).toLowerCase();
   letras = " %0123456789.";
   especiales = "8-39";

   tecla_especial = false
   for(var i in especiales){
        if(key == especiales[i]){
            tecla_especial = true;
            break;
        }
    }
    //alert(letras.indexOf(tecla))
    if(letras.indexOf(tecla)==-1 && !tecla_especial){
        return false;
    }
}


//////////////////////////////////////////////////////////////////////////////////////////////
function soloLetras(e){
   key = e.keyCode || e.which;
   tecla = String.fromCharCode(key).toLowerCase();
   letras = "-abcdefghijklmnñopqrstuvwxyz. ";
   especiales = "8-37-39-46";

   tecla_especial = false
   for(var i in especiales){
        if(key == especiales[i]){
            tecla_especial = true;
            break;
        }
    }

    if(letras.indexOf(tecla)==-1 && !tecla_especial){
        return false;
    }
}


function soloNumerosyLetras(e){
   key = e.keyCode || e.which;
   //alert(key)
   tecla = String.fromCharCode(key).toLowerCase();
   letras = "-abcdefghijklmnñopqrstuvwxyz0123456789. ";
   especiales = "8-37-39-162";

   tecla_especial = false
   for(var i in especiales){
        if(key == especiales[i]){
            tecla_especial = true;
            break;
        }
    }
    //alert(letras.indexOf(tecla))
    if(letras.indexOf(tecla)==-1 && !tecla_especial){
        return false;
    }
}

function soloNumeros(e){
   key = e.keyCode || e.which;
   tecla = String.fromCharCode(key).toLowerCase();
   letras = "+0123456789";
   especiales = "8-37-39-46";

   tecla_especial = false
   for(var i in especiales){
        if(key == especiales[i]){
            tecla_especial = true;
            break;
        }
    }

    if(letras.indexOf(tecla)==-1 && !tecla_especial){
        return false;
    }
}

function soloNumerosDouble(e){
   key = e.keyCode || e.which;
   tecla = String.fromCharCode(key).toLowerCase();
   letras = "+0123456789,";
   especiales = "8-37-39-46";

   tecla_especial = false
   for(var i in especiales){
        if(key == especiales[i]){
            tecla_especial = true;
            break;
        }
    }

    if(letras.indexOf(tecla)==-1 && !tecla_especial){
        return false;
    }
}


     $(".format").on({
  
      "keypress": function(e) {
        
           key = e.keyCode || e.which;
           tecla = String.fromCharCode(key).toLowerCase();
           letras = "0123456789.";
           especiales = "8-37-39-46";

           tecla_especial = false
           for(var i in especiales){
                if(key == especiales[i]){
                    tecla_especial = true;
                    break;
                }
            }

            if(letras.indexOf(tecla)==-1 && !tecla_especial){
                return false;
            }

          }

        });      
  
 
function getkey(e){
    if (window.event) {
    shift= event.shiftKey;
    ctrl= event.ctrlKey;
    alt=event.altKey;
    return window.event.keyCode;
    }
    else if (e) {
    var valor=e.which;
    if (valor>96 && valor<123) {
    valor=valor-32;
    }
    return valor;
    }
    else
    return null;
}

/* Validación Registro DUA*/
function enviardua() {

var mens=[
    'Estimado usuario. Debe ingresar el Número de DUA para completar el registro',
    'Estimado usuario. Debe ingresar el Número de Referencia para completar el registro',
    'Estimado usuario. Debe seleccionar el Agente Aduanal para completar el registro',
    'Estimado usuario. Debe seleccionar la Modalidad de Transporte para completar el registro',
    'Estimado usuario. Debe seleccionar la Aduana Salida para completar el registro',
    'Estimado usuario. Debe ingresar el Lugar de Salida para completar el registro',
    'Estimado usuario. Debe ingresar la Aduana de LLegada para completar el registro',
    'Estimado usuario. Debe ingresar el Lugar de LLegada para completar el registro',
    'Estimado usuario. Debe ingresar la Fecha de Registro de DUA en Aduanas para completar el registro',
    'Estimado usuario. Debe ingresar la Fecha de Embarque para completar el registro',
    'Estimado usuario. Debe ingresar la Fecha de Estimada de Arribo para completar el registro',
    'Estimado usuario. Debe seleccionar el consignatario para completar el registro',
    'Estimado usuario. Debe ingresar el mensaje de duda para completar el registro',
    'Estimado usuario. Debe ingresar la respuesta a la duda para completar el registro'
];



$("div").remove(".msg_alert");

let err_duda = 0;
let error_dudas = '<div class="msg_alert"><span style="color:red" class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span><strong id="cod_aran-error" class="added"> El campo es obligatorio.</strong></div>';
let valido_dudas = '<div class="msg_alert"><span style="color:green" class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span></div>';
let campos = ["correo", "razon_social", "telefono_movil", "mensaje", "mensaje_registrado", "repuesta", "respuesta_analista", "tipo"];
let nombre_campos = ["correo", "razon social", "telefono móvil", "mensaje", "mensaje", "respuesta", "respuesta", "motivo de contacto"];
let n = campos.length;
let input_duda = '';
for (var i = 0; i < n; i++) {
  input_duda = $('#'+campos[i]);
  
  if (input_duda.val() == '')
  {
    err_duda = 1;
    input_duda.css('border', '1px solid red').after(error_dudas);
    if(err_duda==1){
      event.preventDefault(); 
      swal("Por Favor!", 'Estimado Usuario. Debe Ingresar '+nombre_campos[i].toUpperCase()+' para Completar el Registro', "warning")
      setTimeout(function(){
        $("div").remove(".msg_alert");
      }, 5000);
    }
  }
  else{
    if (err_duda == 1) {err_duda = 1;}else{err_duda = 0;}
    input_duda.css('border', '1px solid green').after(valido_dudas);
  }
}

if(err_duda == 1){
  setTimeout(function(){
    $("div").remove(".msg_alert");
  }, 5000);
  setTimeout(function(){
    for (var i = 0; i < n; i++) {
      input_duda = $('#'+campos[i]);
      input_duda.css('border', '1px solid #ccc');
    }
  }, 5000);
}


function validarformdua(input, mensaje,num) {
    if ($("#"+input).val()=="") {

        swal("Por Favor!", mensaje, "warning")
        //swal(mensaje, "info");
        //alert(mensaje)
        $("#"+input).focus();
        key=1;
        $('#ok'+num).remove()
        $("#"+input).css('border', '1px solid red').after('<span id="ok'+num+'" style="color:red" class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span>');
        return 1;
    }else{
        $('#ok'+num).remove()
        $("#"+input).css('border', '1px solid green').after('<span id="ok'+num+'" style="color:green" class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span>');
        return 0;
    }
}

    var key=0;
$("#formdua").submit(function() {
    key = validarformdua('numero_dua', mens[0],0)
    if(key == 1){return false}
    key = validarformdua('numero_referencia', mens[1],1)
    if(key == 1){return false}
    key = validarformdua('gen_agente_aduana_id', mens[2],2)
    if(key == 1){return false}
    key = validarformdua('gen_transporte_id', mens[3],3)
    if(key == 1){return false}
    key = validarformdua('gen_aduana_salida_id', mens[4],4)
    if(key == 1){return false}
    key = validarformdua('lugar_salida', mens[5],5)
    if(key == 1){return false}
    key = validarformdua('aduana_llegada', mens[6],6)
    if(key == 1){return false}
    key = validarformdua('lugar_llegada', mens[7],7)
    if(key == 1){return false}
    key = validarformdua('fregistro_dua_aduana', mens[8],8)
    if(key == 1){return false}
    key = validarformdua('fembarque', mens[9],9)
    if(key == 1){return false}
    key = validarformdua('farribo', mens[10],10)
    if(key == 1){return false}
    key = validarformdua('gen_consignatario_id', mens[11],11)
    if(key == 1){return false}
    key = validarformdua('mensaje', mens[12],11)
    if(key == 1){return false}
    key = validarformdua('respuesta', mens[13],11)
    if(key == 1){return false}

    if (key==0) {
        return true;
    }
});
}

/*****************Validaciones de factura **********************/
function enviarFactura() {


var mens=[
   'Estimado usuario. Debe seleccionar el tipo de Solicitud a la cual asociará esta factura para completar el registro',
   'Estimado usuario. Debe ingresar el Número de Factura para completar el registro',
    'Estimado usuario. Debe ingresar la Fecha de la Factura para completar el registro',
   'Estimado usuario. Debe seleccionar un Consignatario para completar el registro',
   'Estimado usuario. Debe seleccionar una Divisa para completar el registro',
   'Estimado usuario. Debe seleccionar un Incoterm para completar el registro',
   'Estimado usuario. Debe ingresar el Monto Fob para completar el registro',
   'Estimado usuario. Debe ingresar el Monto Flete para completar el registro',
   'Estimado usuario. Debe ingresar el Monto Seguro para completar el registro',
   'Estimado usuario. Debe ingresar Otro Monto para completar el registro',
   'Estimado usuario. Debe seleccionar la Dua para completar el registro',
   'Estimado usuario. Debe seleccionar la Forma de Pago para completar el registro',
   'Estimado usuario. Debe seleccionar el Tipo de Instrumento para completar el registro',

]

 function validarIf(input,value){


 }

$.fn.selectRange = function(start, end) {
    if(!end) end = start;
    return this.each(function() {
        if (this.setSelectionRange) {
            this.focus();
            this.setSelectionRange(start, end);
        } else if (this.createTextRange) {
            var range = this.createTextRange();
            range.collapse(true);
            range.moveEnd('character', end);
            range.moveStart('character', start);
            range.select();
        }
    });
};

function validarformFactura(input, mensaje,num) {

  if( ($("#"+input)[0].type=="text") || ($("#"+input)[0].type=="select-one")){


    if ($("#"+input).val()=="") {

        swal("Por Favor!", mensaje, "warning")
        //swal(mensaje, "info");
        //alert(mensaje)
        $("#"+input).focus();
        key=1;
        $('#ok'+num).remove();
        $("#"+input).css('border', '1px solid red').after('<span id="ok'+num+'" style="color:red" class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span>');
          $('#'+input).selectRange(4);

        return 1;
    }else{


        $('#ok'+num).remove()
        $("#"+input).css('border', '1px solid green').after('<span id="ok'+num+'" style="color:green" class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span>');
        return 0;
    }

  }else{
     var almacen="";
     $('input[name='+input+']').each(function(index,value){

          if(value.checked) { almacen=1; }
     });

    //console.log(almacen);
    if(almacen==""){

      swal("Por Favor!", mensaje, "warning")
      //swal(mensaje, "info");

      $("#"+input).focus();
      key=1;
      $('#ok'+num).remove();
      $("#"+input).css('border', '1px solid red').after('<span id="ok'+num+'" style="color:red" class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span>');

      return 1;

      }else{



          $('#ok'+num).remove()
          $("#"+input).css('border', '1px solid green').after('<span id="ok'+num+'" style="color:green" class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span>');
          return 0;


      }
  }


}

var key=0;
$("#formFactura").submit(function() {

key = validarformFactura('tipo_solicitud_id', mens[0],0)
if(key == 1){return false}
key = validarformFactura('numero_factura', mens[1],1)
if(key == 1){return false}
key = validarformFactura('fecha_factura', mens[2],2)
if(key == 1){return false}
key = validarformFactura('gen_consignatario_id', mens[3],3)
if(key == 1){return false}
key = validarformFactura('gen_divisa_id', mens[4],4)
if(key == 1){return false}
key = validarformFactura('forma_pago_id', mens[11],11)
if(key == 1){return false}
key = validarformFactura('tipo_instrumento_id', mens[12],12)
if(key == 1){return false}
key = validarformFactura('incoterm_id', mens[5],5)
if(key == 1){return false}
key = validarformFactura('monto_fob', mens[6],6)
if(key == 1){return false}
key = validarformFactura('monto_flete', mens[7],7)
if(key == 1){return false}
key = validarformFactura('monto_seguro', mens[8],8)
if(key == 1){return false}
key = validarformFactura('otro_monto', mens[9],9)
if(key == 1){return false}
key = validarformFactura('gen_dua_id', mens[10],10)
if(key == 1){return false}



if (key==0) {
    return true;
}
});



};

/******************************VALIDACIONES DE PRODUCTOS-TECNOLOGIA********/
function enviarProduc() {


var mens=[
   'Estimado usuario. Debe escribir la Descripción del Producto para completar el registro',
   'Estimado usuario. Debe indicar la cantidad para completar el registro',
    'Estimado usuario. Debe seleccioanr la Unidad de Medida para completar el registro',
   'Estimado usuario. Debe indicar el precio para completar el registro',

]

$.fn.selectRange = function(start, end) {
    if(!end) end = start;
    return this.each(function() {
        if (this.setSelectionRange) {
            this.focus();
            this.setSelectionRange(start, end);
        } else if (this.createTextRange) {
            var range = this.createTextRange();
            range.collapse(true);
            range.moveEnd('character', end);
            range.moveStart('character', start);
            range.select();
        }
    });
};

/*function validarFormProd(input, mensaje,num) {


$('input[name="'+input+'[]"]').each(function(index,value){



  if(value.value==""){

        swal("Por Favor!", mensaje, "warning")
        //swal(mensaje, "info");
        //alert(mensaje)
        $("#"+input).focus();
        key=1;
        $('#ok'+num).remove();
        $("#"+input).css('border', '1px solid red').after('<span id="ok'+num+'" style="color:red" class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span>');
        $('#'+input).selectRange(4);

       return false;


    }else{


        $('#ok'+num).remove()
        $("#"+input).css('border', '1px solid green').after('<span id="ok'+num+'" style="color:green" class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span>');
        return 0;
    }







});

  //return false;

}

var key=0;
$("#FormProd").submit(function() {


  $('input[name="cantidad_producto[]"]').each(function(index,value){



    if(value.value==""){

          swal("Por Favor!", "sss", "warning")
          //swal(mensaje, "info");
          //alert(mensaje)
          $("#"+input).focus();
          key=1;
          $('#ok0').remove();
        /*  $("#"+input).css('border', '1px solid red').after('<span id="ok'+num+'" style="color:red" class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span>');
          $('#'+input).selectRange(4);

        // return false;


      }else{


          $('#ok'+num).remove()
          $("#"+input).css('border', '1px solid green').after('<span id="ok'+num+'" style="color:green" class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span>');
          return 0;
      }



  });



});


*/


};

/**************************************************************************/




/*Validacion Formulario Registro Consignatario*/
function validarconvenio(){
  var pais=$('#pais_id').val();
  var convenio=$('#cat_tipo_convenio_id').val();
  var  msg_convenio1= "El país seleccionado no corresponde al convenio Aladi";
  var  msg_convenio2= "El país seleccionado no corresponde al convenio Can";
  var  msg_convenio3= "El país seleccionado no corresponde al convenio G3";
  var  msg_convenio4= "El país seleccionado no corresponde al convenio Sucre";
  var  msg_convenio5= "El país seleccionado no corresponde al convenio Mercosur";
  var  x=true;

if (convenio==1) {
  if (pais==10 || pais==23 || pais==25 || pais==34 || pais==37 || pais==46 || pais==127 || pais==128 || pais==164 || pais==168) {
      $("#cat_tipo_convenio_id").css('border', '1px solid green').after('<span id="ok6" style="color:green" class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span>');  
      return 0;

  }else{
    swal("Disculpe!", msg_convenio1, "warning");
      $("#cat_tipo_convenio_id").focus();
      $('#ok6').remove()
      $("#cat_tipo_convenio_id").css('border', '1px solid red').after('<span id="ok6" style="color:red" class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span>');
      x=false;
      return 1;
  }
}

if (convenio==2) {
  if (pais==23 || pais==37 || pais==46 || pais==128 || pais==168) {
    $("#cat_tipo_convenio_id").css('border', '1px solid green').after('<span id="ok6" style="color:green" class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span>');  
      return 0;

  }else{
    swal("Disculpe!", msg_convenio2, "warning");
      $("#cat_tipo_convenio_id").focus();
      $('#ok6').remove()
      $("#cat_tipo_convenio_id").css('border', '1px solid red').after('<span id="ok6" style="color:red" class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span>');
      x=false;
      return 1;
  }
}

if (convenio==3) {
  if (pais==37 || pais==106 || pais==168) {
       $("#cat_tipo_convenio_id").css('border', '1px solid green').after('<span id="ok6" style="color:green" class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span>');  
      return 0;

  }else{
    swal("Disculpe!", msg_convenio3, "warning");
      $("#cat_tipo_convenio_id").focus();
      $('#ok6').remove()
      $("#cat_tipo_convenio_id").css('border', '1px solid red').after('<span id="ok6" style="color:red" class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span>');
      x=false;
      return 1;
  }
}

if (convenio==4) {
  if (pais==23 || pais==43 || pais==46 || pais==116 || pais==168) {
       $("#cat_tipo_convenio_id").css('border', '1px solid green').after('<span id="ok6" style="color:green" class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span>');  
      return 0;

  }else{
    swal("Disculpe!", msg_convenio4, "warning");
      $("#cat_tipo_convenio_id").focus();
      $('#ok6').remove()
      $("#cat_tipo_convenio_id").css('border', '1px solid red').after('<span id="ok6" style="color:red" class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span>');
      x=false;
      return 1;
  }
}

if (convenio==5) {
  if (pais==10 || pais==23 || pais==25 || pais==127 || pais==164 || pais==168) {
    $("#cat_tipo_convenio_id").css('border', '1px solid green').after('<span id="ok6" style="color:green" class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span>');  
      return 0;

  }else{
    swal("Disculpe!", msg_convenio5, "warning");
      $("#cat_tipo_convenio_id").focus();
      $('#ok6').remove()
      $("#cat_tipo_convenio_id").css('border', '1px solid red').after('<span id="ok6" style="color:red" class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span>');
      x=false;
      return 1;
  }
  }
}


function enviarconsig() {

var mens=[
    'Estimado usuario. Debe seleccionar Cartera Comprador / Proveedor para completar el registro',
    'Estimado usuario. Debe ingresar el Rif de la Empresa para completar el registro',
    'Estimado usuario. Debe ingresar el Nombre o Razón social para completar el registro',
    'Estimado usuario. Debe ingresar la Persona de Contacto para completar el registro',
    'Estimado usuario. Debe ingresar el Cargo de la Persona de Contacto para completar el registro',
    'Estimado usuario. Debe seleccionar el País para completar el registro',
    'Estimado usuario. Debe seleccionar el Tipo de Convenio para completar el registro',
    'Estimado usuario. Debe ingresar la Ciudad para completar el registro',
    'Estimado usuario. Debe ingresar la Dirección para completar el registro',
    'Estimado usuario. Debe ingresar el Teléfono de Contacto para completar el registro',
    'Estimado usuario. Debe ingresar el Correo Electronico para completar el registro',
]

function validarformconsig(input, mensaje,num) {
    if ($("#"+input).val()=="") {

        swal("Por Favor!", mensaje, "warning")
        //swal(mensaje, "info");
        //alert(mensaje)
        $("#"+input).focus();
        key=1;
        $('#ok'+num).remove()
        $("#"+input).css('border', '1px solid red').after('<span id="ok'+num+'" style="color:red" class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span>');
        return 1;
    }else{
        $('#ok'+num).remove()
        $("#"+input).css('border', '1px solid green').after('<span id="ok'+num+'" style="color:green" class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span>');
        return 0;
    }
}
    var cartera=$("#cat_tipo_cartera_id").val();
    var key=0;
    $("#formconsig").submit(function() {
    key = validarformconsig('cat_tipo_cartera_id', mens[0],0)
    if(key == 1){return false}
    if (cartera == 2) {
      key = validarformconsig('rif_empresa', mens[1],1)
      if(key == 1){return false}
    }
    
    key = validarformconsig('nombre_consignatario', mens[2],2)
    if(key == 1){return false}
      key = validarformconsig('perso_contac', mens[3],3)
    if(key == 1){return false}
      key = validarformconsig('cargo_contac', mens[4],4)
    if(key == 1){return false}
    if (cartera == 1) {
      key = validarformconsig('pais_id', mens[5],5)
      if(key == 1){return false}
        key = validarformconsig('cat_tipo_convenio_id', mens[6],6)
      if(key == 1){return false}
    }
    key = validarformconsig('ciudad_consignatario', mens[7],7)
    if(key == 1){return false}
    key = validarformconsig('direccion', mens[8],8)
    if(key == 1){return false}
    key = validarformconsig('telefono_consignatario', mens[9],9)
    if(key == 1){return false}
    key = validarformconsig('correo', mens[10],10)
    if(key == 1){return false}
    key=validarconvenio()
    if(key == 1)
    {
       return false;

    }
    if (key==0) {
        return true;
    }
});
}


/*Validacion Formulario Agente Aduanal*/

function enviaragenteaduanal() {


var mens=[
    'Estimado usuario. Debe ingresar NOMBRE O RAZÓN SOCIAL para completar el registro',
    'Estimado usuario. Debe ingresar RIF para completar el registro',
    'Estimado usuario. Debe ingresar NÚMERO DE REGISTRO para completar el registro',
    'Estimado usuario. Debe ingresar CIUDAD para completar el registro',
    'Estimado usuario. Debe ingresar TÉLEFONO para completar el registro',
]

function validarAgenteAduanal(input, mensaje,num) {
    if ($("#"+input).val()=="") {

        swal("Por Favor!", mensaje, "warning")
        //swal(mensaje, "info");
        //alert(mensaje)
        $("#"+input).focus();
        key=1;
        $('#ok'+num).remove()
        $("#"+input).css('border', '1px solid red').after('<span id="ok'+num+'" style="color:red" class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span>');
        return 1;
    }else{
        $('#ok'+num).remove()
        $("#"+input).css('border', '1px solid green').after('<span id="ok'+num+'" style="color:green" class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span>');
        return 0;
    }
}

    var key=0;
    $("#formagenteaduanal").submit(function() {
    key = validarAgenteAduanal('nombre_agente', mens[0],0)
    if(key == 1){return false}
    key = validarAgenteAduanal('rif_agente', mens[1],1)
    if(key == 1){return false}
    key = validarAgenteAduanal('numero_registro', mens[2],2)
    if(key == 1){return false}
    key = validarAgenteAduanal('ciudad_agente', mens[3],3)
    if(key == 1){return false}
    key = validarAgenteAduanal('telefono_agente', mens[4],4)
    if(key == 1){return false}


    if (key==0) {
        return true;
    }
});

}
/*Validacion Formulario AdminUsuario*/
function enviaradminusuarioInversiones() {
    /*inicio validacion para el formato de la fecha
    var fTemp = $("#fregistro_mer").val();
    var fcampo = fTemp.split("/")
    var result = fcampo[2].substr(2, 4)
        $("#fregistro_mer").val(fcampo[0]+"/"+fcampo[1]+"/"+result);
    /*fin validacion para el formato de la fecha*/

var mens=[
    'Estimado usuario. Debe ingresar NOMBRE DE USUARIO  para completar el registro',
    'Estimado usuario. Debe ingresar APELLIDO DE USURIO  para completar el registro',
    'Estimado usuario. Debe ingresar CEDULA DE USUARIO  para completar el registro',
    'Estimado usuario. Debe ingresar EMAIL PRINCIPAL  para completar el registro',
    'Estimado usuario. Debe ingresar EMAIL SECUNDARIO  para completar el registro',
    'Estimado usuario. Debe ingresar TIPO DE USUARIO  para completar el registro',
    'Estimado usuario. Debe ingresar ESTATUS para completar el registro',
    'Estimado usuario. Debe ingresar CONTRASEÑA para completar el registro',
    'Estimado usuario. Debe ingresar CONFIRMAR CONTRASEÑA para completar el registro',
    'Estimado usuario. Debe ingresar PREGUNTA DE SEGURIDAD para completar el registro',
    'Estimado usuario. Debe ingresar RESPUESTA  para completar el registro'

]

function validarAdminUsuarioInvert(input, mensaje,num) {
    if ($("#"+input).val()=="") {

        swal("Por Favor!", mensaje, "warning")
        //swal(mensaje, "info");
        //alert(mensaje)
        $("#"+input).focus();
        key=1;
        $('#ok'+num).remove()
        $("#"+input).css('border', '1px solid red').after('<span id="ok'+num+'" style="color:red" class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span>');
        return 1;
    }else{
        $('#ok'+num).remove()
        $("#"+input).css('border', '1px solid green').after('<span id="ok'+num+'" style="color:green" class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span>');
        return 0;
    }
}

    var key=0;
    $("#formadminusuarioInvert").submit(function() {
    key = validarAdminUsuarioInvert('nombre_user', mens[0],0)
    if(key == 1){return false}
    key = validarAdminUsuarioInvert('apellido_user', mens[1],1)
    if(key == 1){return false}
    key = validarAdminUsuarioInvert('cedula_user', mens[2],2)
    if(key == 1){return false}
    key = validarAdminUsuarioInvert('email', mens[3],3)
    if(key == 1){return false}
    /*key = validarAdminUsuarioInvert('email_seg', mens[4],4)
    if(key == 1){return false}*/
    key = validarAdminUsuarioInvert('gen_tipo_usuario_id', mens[5],5)
    if(key == 1){return false}
    key = validarAdminUsuarioInvert('gen_status_id', mens[6],6)
    if(key == 1){return false}
    key = validarAdminUsuarioInvert('password', mens[7],7)
    if(key == 1){return false}
    key = validarAdminUsuarioInvert('password_confirm', mens[8],8)
    if(key == 1){return false}
    key = validarAdminUsuarioInvert('preguntas_seg', mens[9],9)
    if(key == 1){return false}
    key = validarAdminUsuarioInvert('respuesta_p1', mens[10],10)


   /* key = validarForm('cmail_prim', mens[20],20)
    if(key == 1){return false}*/
  //  key=validarEmail($('#cmail_prim').val());


    key=validarCorreoAdmin($('#email').val(),$('#email_seg').val())
    if(key == 1)
    {
       return false;

    }

    key=validarclaveadmin($('#password').val(),$('#password_confirm').val())
    if(key == 1)
    {
       return false;

    }

    if (key==0) {
        return true;
    }
});

}

/*Validacion Formulario AdminUsuario*/
function enviaradminusuarioResguardo() {
    /*inicio validacion para el formato de la fecha
    var fTemp = $("#fregistro_mer").val();
    var fcampo = fTemp.split("/")
    var result = fcampo[2].substr(2, 4)
        $("#fregistro_mer").val(fcampo[0]+"/"+fcampo[1]+"/"+result);
    /*fin validacion para el formato de la fecha*/

    var mens=[
        'Estimado usuario. Debe ingresar NOMBRE DE USUARIO  para completar el registro',
        'Estimado usuario. Debe ingresar APELLIDO DE USURIO  para completar el registro',
        'Estimado usuario. Debe ingresar CEDULA DE USUARIO  para completar el registro',
        'Estimado usuario. Debe ingresar RANGO DE USUARIO  para completar el registro',
        'Estimado usuario. Debe ingresar UNIDAD DEL USUARIO  para completar el registro',
        'Estimado usuario. Debe ingresar UBICACIÓN DEL USUARIO  para completar el registro',
        'Estimado usuario. Debe ingresar TELÉFONO DEL USUARIO  para completar el registro',
        'Estimado usuario. Debe ingresar EMAIL PRINCIPAL  para completar el registro',
        'Estimado usuario. Debe ingresar EMAIL SECUNDARIO  para completar el registro',
        'Estimado usuario. Debe ingresar TIPO DE USUARIO  para completar el registro',
        'Estimado usuario. Debe ingresar ESTATUS para completar el registro',
        'Estimado usuario. Debe ingresar CONTRASEÑA para completar el registro',
        'Estimado usuario. Debe ingresar CONFIRMAR CONTRASEÑA para completar el registro',
        'Estimado usuario. Debe ingresar PREGUNTA DE SEGURIDAD para completar el registro',
        'Estimado usuario. Debe ingresar RESPUESTA  para completar el registro'

    ]

    function validarAdminUsuarioResg(input, mensaje,num) {
        if ($("#"+input).val()=="") {

            swal("Por Favor!", mensaje, "warning")
            //swal(mensaje, "info");
            //alert(mensaje)
            $("#"+input).focus();
            key=1;
            $('#ok'+num).remove()
            $("#"+input).css('border', '1px solid red').after('<span id="ok'+num+'" style="color:red" class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span>');
            return 1;
        }else{
            $('#ok'+num).remove()
            $("#"+input).css('border', '1px solid green').after('<span id="ok'+num+'" style="color:green" class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span>');
            return 0;
        }
    }

    var key=0;
    $("#formadminusuarioResguardo").submit(function() {
        key = validarAdminUsuarioResg('nombre_funcionario', mens[0],0)
        if(key == 1){return false}
        key = validarAdminUsuarioResg('apellido_funcionario', mens[1],1)
        if(key == 1){return false}
        key = validarAdminUsuarioResg('cedula_funcionario', mens[2],2)
        if(key == 1){return false}
        key = validarAdminUsuarioResg('rango_funcionario', mens[3],3)
        if(key == 1){return false}
        key = validarAdminUsuarioResg('unidad', mens[4],4)
        if(key == 1){return false}
        key = validarAdminUsuarioResg('ubicacion', mens[5],5)
        if(key == 1){return false}
        key = validarAdminUsuarioResg('gen_aduana_salida_id', mens[6],6)
        if(key == 1){return false}
        key = validarAdminUsuarioResg('email', mens[7],7)
        if(key == 1){return false}
        /*key = validarAdminUsuarioResg('email_seg', mens[4],4)
        if(key == 1){return false}*/
        key = validarAdminUsuarioResg('gen_tipo_usuario_id', mens[9],9)
        if(key == 1){return false}
        key = validarAdminUsuarioResg('gen_status_id', mens[10],10)
        if(key == 1){return false}
        key = validarAdminUsuarioResg('password', mens[11],11)
        if(key == 1){return false}
        key = validarAdminUsuarioResg('password_confirm', mens[12],12)
        if(key == 1){return false}
        key = validarAdminUsuarioResg('preguntas_seg', mens[13],13)
        if(key == 1){return false}
        key = validarAdminUsuarioResg('respuesta_p1', mens[14],14)


       /* key = validarForm('cmail_prim', mens[20],20)
        if(key == 1){return false}*/
      //  key=validarEmail($('#cmail_prim').val());


        key=validarCorreoAdmin($('#email').val(),$('#email_seg').val())
        if(key == 1)
        {
           return false;

        }

        key=validarclaveadmin($('#password').val(),$('#password_confirm').val())
        if(key == 1)
        {
           return false;

        }

        if (key==0) {
            return true;
        }
    });

}

/*Validacion Formulario AdminUsuario*/
function enviaradminusuario() {
    /*inicio validacion para el formato de la fecha
    var fTemp = $("#fregistro_mer").val();
    var fcampo = fTemp.split("/")
    var result = fcampo[2].substr(2, 4)
        $("#fregistro_mer").val(fcampo[0]+"/"+fcampo[1]+"/"+result);
    /*fin validacion para el formato de la fecha*/

var mens=[
    'Estimado usuario. Debe ingresar EMAIL PRINCIPAL  para completar el registro',
    'Estimado usuario. Debe ingresar EMAIL SECUNDARIO  para completar el registro',
    'Estimado usuario. Debe ingresar TIPO DE USUARIO  para completar el registro',
    'Estimado usuario. Debe ingresar ESTATUS para completar el registro',
    'Estimado usuario. Debe ingresar CONTRASEÑA para completar el registro',
    'Estimado usuario. Debe ingresar CONFIRMAR CONTRASEÑA para completar el registro',
    'Estimado usuario. Debe ingresar PREGUNTA DE SEGURIDAD para completar el registro',
    'Estimado usuario. Debe ingresar RESPUESTA  para completar el registro'

]

function validarAdminUsuario(input, mensaje,num) {
    if ($("#"+input).val()=="") {

        swal("Por Favor!", mensaje, "warning")
        //swal(mensaje, "info");
        //alert(mensaje)
        $("#"+input).focus();
        key=1;
        $('#ok'+num).remove()
        $("#"+input).css('border', '1px solid red').after('<span id="ok'+num+'" style="color:red" class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span>');
        return 1;
    }else{
        $('#ok'+num).remove()
        $("#"+input).css('border', '1px solid green').after('<span id="ok'+num+'" style="color:green" class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span>');
        return 0;
    }
}

    var key=0;
    $("#formadminusuario").submit(function() {
    key = validarAdminUsuario('email', mens[0],0)
    if(key == 1){return false}
    /*key = validarAdminUsuario('email_seg', mens[1],1)
    if(key == 1){return false}*/
    key = validarAdminUsuario('gen_tipo_usuario_id', mens[2],2)
    if(key == 1){return false}
    key = validarAdminUsuario('gen_status_id', mens[3],3)
    if(key == 1){return false}
    key = validarAdminUsuario('password', mens[4],4)
    if(key == 1){return false}
    key = validarAdminUsuario('password_confirm', mens[5],5)
    if(key == 1){return false}
    key = validarAdminUsuario('preguntas_seg', mens[6],6)
    if(key == 1){return false}
    key = validarAdminUsuario('respuesta_p1', mens[7],7)


   /* key = validarForm('cmail_prim', mens[20],20)
    if(key == 1){return false}*/
  //  key=validarEmail($('#cmail_prim').val());


    key=validarCorreoAdmin($('#email').val(),$('#email_seg').val())
    if(key == 1)
    {
       return false;

    }

    key=validarclaveadmin($('#password').val(),$('#password_confirm').val())
    if(key == 1)
    {
       return false;

    }

    if (key==0) {
        return true;
    }
});

}

/*----------------------------------------------------------------------------*/
function validarclaveadmin(clave1,clave2){
    var  msg= "El Campo Contraseñaaaaa debe tener minimo 6 caracteres";
    var  msj2="El Campo Contraseña no puede estar Vacio";
    var  msj3="El Campo Confirmación de Contraseña tiene que ser igual al Campo Contraseña";
    var clave1 = document.getElementById('password').value;
    var clave2 = document.getElementById('password_confirm').value;
    var  x=true;


    if (clave1.length< 6) {
        swal("Por Favor!", msg, "warning")
        //swal(msj2, "info");
         //alert(msj2);
         x=false;
         return 1;
    }

    if (clave2 != clave1) {
        swal("Por Favor!", msj3, "warning")
        //swal(msj2, "info");
         //alert(msj2);
        x=false;
        return 1;
    }

    if(x==true){

       $("#password").css('border', '1px solid green').after('<span id="ok20" style="color:green" class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span>');
       $("#password_confirm").css('border', '1px solid green').after('<span id="ok21" style="color:green" class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span>');
       return 0;
    }
}
/*----------------------------------------------------------------------------*/

function validarCorreoAdmin(email,email1){

    expr = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    var  msg= "El Campo esta Vacio...Debe Ingresar un Email ";
    var  msj2="La Direccion de Email Ingresada es Incorrecta";
    var  msj3="La Dirección de Email Secundario no debe ser igual al Email Principal";
    var email = document.getElementById('email').value;
    var email_seg = document.getElementById('email_seg').value;
    var  x=true;


      if( ( (!expr.test(email)) && (email!='') )) {


        swal("Por Favor!", msj2, "warning")
        //swal(msj2, "info");
         //alert(msj2);
         x=false;
        $("#email").focus();

        $('#ok1').remove()
        $("#email").css('border', '1px solid red').after('<span id="ok1" style="color:red" class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span>');
         return 1;





        }

      if ( (!expr.test(email1)) && (email1!='') ) {

        swal("Por Favor!", msj2, "warning")
        //swal(msj2, "info");
         //alert(msj2);
         x=false;
        $("#email_seg").focus();

        $('#ok2').remove()
        $("#email_seg").css('border', '1px solid red').after('<span id="ok2" style="color:red" class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span>');
         return 1;
      }

      if( email=='' ) {
        swal("Por Favor!", msg, "warning")

        //swal(msg, "info");
        //alert(msg);
        x=false;
        return 1;
        }


       if(email==email1 ) {
        swal("Por Favor!", msj3, "warning")
        //swal(msj3, "info");
       //alert(msj3);
       x=false;
       $("#email_seg").focus();

        $('#ok2').remove()
        $("#email_seg").css('border', '1px solid red').after('<span id="ok2" style="color:red" class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span>');
         return 1;

       }


       if(x==true){

       $("#email").css('border', '1px solid green').after('<span id="ok20" style="color:green" class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span>');
       $("#email_seg").css('border', '1px solid green').after('<span id="ok21" style="color:green" class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span>');
       return 0;

       }

  }


/*Validacion Formulario Pregunta de Seguridad*/

function enviarpreguntaseg() {

var mens=[
    'Estimado usuario. Debe seleccionar la Pregunta Nueva para completar el registro',
    'Estimado usuario. Debe ingresar una Pregunta alternativa para completar el Registro',
    'Estimado usuario. Debe ingresar una Respuesta para completar el Registro',
]

function validarpreguntaseg(input, mensaje,num) {
    if ($("#"+input).val()=="") {

        swal("Por Favor!", mensaje, "warning")
        //swal(mensaje, "info");
        //alert(mensaje)
        $("#"+input).focus();
        key=1;
        $('#ok'+num).remove()
        $("#"+input).css('border', '1px solid red').after('<span id="ok'+num+'" style="color:red" class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span>');
        return 1;
    }else{
        $('#ok'+num).remove()
        $("#"+input).css('border', '1px solid green').after('<span id="ok'+num+'" style="color:green" class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span>');
        return 0;
    }
}

    var key=0;
$("#formCambioPregSeg").submit(function() {
    key = validarpreguntaseg('preguntas_seg', mens[0],0)
    if(key == 1){return false}
    var pregunta=$('#preguntas_seg').val()
        if(pregunta==8){
            key = validarpreguntaseg('pregunta_alter', mens[1],1)
            if(key == 1){return false}
        }
                 key = validarpreguntaseg('respuesta_seg', mens[2],2)
            if(key == 1){return false}


            if (key==0) {
                return true;
            }
});
}

/*Validacion Formulario Datos Empresa*/

//function enviardatoempresa(){
function enviardatoempresa() {
    /*inicio validacion para el formato de la fecha
    var fTemp = $("#fregistro_mer").val();
    var fcampo = fTemp.split("/")
    var result = fcampo[2].substr(2, 4)
        $("#fregistro_mer").val(fcampo[0]+"/"+fcampo[1]+"/"+result);
    /*fin validacion para el formato de la fecha*/

var mens=[
     ////////////////////Parte 1 Del Registro ingresar los datos personales///////////////////////////////////////////
    'Estimado usuario. Debe ingresar NOMBRE O RAZÓN SOCIAL En el Paso 1 para completar la inscripción',//0
    'Estimado usuario. Debe ingresar SIGLAS  En el Paso 1 para completar la inscripción',//1
    'Estimado usuario. Debe ingresar RIF En el Paso 1 para completar la inscripción',//2
    'Estimado usuario. Debe ingresar un SECTOR En el Paso 1  para completar la inscripción',//3
    'Estimado usuario. Debe seleccionar ACTIVIDAD ECONÓMICA En el Paso 1  para completar la inscripción',//4
    'Estimado usuario. Debe seleccionar TIPO DE EMPRESA En el Paso 1 para completar la inscripción',//5
    'Estimado usuario. Debe seleccionar Si es EXPORTADOR, INVERSIONISTA, PRODUCTOR, COMERCIALIZADORA En el Paso 1 para completar la inscripción',//6
    'Estimado usuario. Debe seleccionar PAÍS  En el Paso 1 para completar la inscripción',//7
    'Estimado usuario. Debe seleccionar ESTADO En el Paso 1  para completar la inscripción',//8
    'Estimado usuario. Debe seleccionar MUNICIPIO  En el Paso 1 para completar la inscripción',//9
    'Estimado usuario. Debe seleccionar PARROQUIA En el Paso 1  para completar la inscripción',//10
    'Estimado usuario. Debe ingresar una DIRECCIÓN En el Paso 1 para completar la inscripción',//11
    'Estimado usuario. Debe ingresar TELÉFONO LOCAL En el Paso 1  para completar la inscripción',//12
    'Estimado usuario. Debe ingresar TELÉFONO CELULAR En el Paso 1  para completar la inscripción', //13

 ///////////Parte 2 del registro ingresa los datos //////////////////////////////////////////////////////
    'Estimado usuario. Debe ingresar REGISTRO Y CIRCUNSCRIPCIÓN JUDICIAL En el Paso 2 para completar la inscripción',//14
    'Estimado usuario. Debe ingresar NÚMERO En el Paso 2 para completar la inscripción',//15
    'Estimado usuario. Debe ingresar TOMO En el Paso 2 para completar la inscripción',//16
    'Estimado usuario. Debe ingresar FOLIO En el Paso 2 para completar la inscripción',//17
    'Estimado usuario. Debe ingresar CAPITAL SOCIAL En el Paso 2 para completar la inscripción',//18
    'Estimado usuario. Debe seleccionar FECHA REGISTRO En el Paso 2 para completar la inscripción',//19
    'Estimado usuario. Debe ingresar NOMBRE DEL PRESIDENTE  En el Paso 2 para completar la inscripción',//20
    'Estimado usuario. Debe ingresar APELLIDO DEL PRESIDENTE Parte 2 para completar la inscripción',//21
    'Estimado usuario. Debe ingresar CÉDULA DEL PRESIDENTE En el Paso 2 para completar la inscripción',//22
    'Estimado usuario. Debe ingresar CARGO  DEL PRESIDENTE En el Paso 2 para completar la inscripción',//23
    'Estimado usuario. Debe ingresar NOMBRE REPRESENTANTE LEGAL En el Paso 2 para completar la inscripción',//24
    'Estimado usuario. Debe ingresar APELLIDO REPRESENTANTE LEGAL  En el Paso 2 para completar la inscripción',//25
    'Estimado usuario. Debe ingresar CÉDULA  REPRESENTANTE LEGAL En el Paso 2 para completar la inscripción',//26
    'Estimado usuario. Debe ingresar CARGO REPRESENTANTE LEGAL  En el Paso 2 para completar la inscripción',//27
    'Estimado usuario. Debe Seleccionar Si Posee o No Accionista En el Paso 2 para completar la inscripción',//28
    'Estimado usuario. Debe ingresar NOMBRE ACCIONISTA  En el Paso 2 para completar la inscripción',//29
    'Estimado usuario. Debe ingresar APELLIDO ACCIONISTA En el Paso 2 para completar la inscripción',//30
    'Estimado usuario. Debe ingresar CÉDULA ACCIONISTA En el Paso 2 para completar la inscripción',//31
    'Estimado usuario. Debe ingresar CARGO ACCIONISTA En el Paso 2 Pasopara completar la inscripción',//32

    ///////////////////////Parte 3 del  Registro ingresa los productos /////////////////////////////////////
    'Estimado usuario. Debe Seleccionar si Exporta o no Productos de Tecnologia En el Paso 3 Paso para completar la inscripción',//33
    'Estimado usuario. Debe Seleccionar un CÓDIGO ARANCELARIO NANDINA O MERCOSUR En el Paso 3 para completar la inscripción',//34
    'Estimado usuario. Debe ingresar CÓDIGO ARANCELARIO En el Paso 3 para completar la inscripción',//35
    'Estimado usuario. Debe ingresar una DESCRIPCIÓN ARANCELARIA Paso En el Paso 3  para completar la inscripción',//36
    'Estimado usuario. Debe ingresar una DESCRIPCIÓN COMERCIAL En el Paso 3 para completar la inscripción',//37
    'Estimado usuario. Debe ingresar una UNIDAD DE MEDIDA En el Paso 3 para completar la inscripción',//38
    'Estimado usuario. Debe ingresar la CAPACIDAD OPERATIVA ANUAL En el En el Paso 3 para completar la inscripción',//39
    'Estimado usuario. Debe ingresar la CAPACIDAD INSTALADA ANUAL En el Paso 3 para completar la inscripción',//40
    'Estimado usuario. Debe ingresar la CAPACIDAD DE ALMACENAMIENTO ANUAL En el Paso 3  para completar la inscripción',//41

    ///////////////////Paso 4 ingresar el email///////////////////////////////////////
    'Estimado usuario. Debe ingresar CORREO ELECTRÓNICO  para completar la inscripción',//42
    'Estimado usuario. Debe ingresar CORREO ELECTRÓNICO ALTERNATIVO para completar la inscripción',//43
    'Estimado usuario. Debe ingresar LA ESPECIFICACIÓN DE LA PRODUCCIÓN DE SERVICIOS Y/O TECNLOGÍA En el Paso 3 para completar la inscripción',//44

  ]
function validarFormDatosEmpresa(input, mensaje,num) {
    if ($("#"+input).val()=="") {

        swal("Por Favor!", mensaje, "warning")
        //swal(mensaje, "info");
        //alert(mensaje)
        $("#"+input).focus();
        key=1;
        $('#ok'+num).remove()
        $("#"+input).css('border', '1px solid red').after('<span id="ok'+num+'" style="color:red" class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span>');
        return 1;
    }else{
        $('#ok'+num).remove()
        $("#"+input).css('border', '1px solid green').after('<span id="ok'+num+'" style="color:green" class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span>');
        return 0;
    }

}



/**************************** valiadciones de radio y checkbox*************************/
function validarradio(input, mensaje,num){
    if ($("#"+input).is(':checked')) {
        $('#ok'+num).remove()
        $("#"+input).css('border', '1px solid green').after('<span id="ok'+num+'" style="color:green" class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span>');
        return 0;

    }else{

        swal("Por Favor!", mensaje, "warning")
        $("#"+input).focus();
        key=1;
        $('#ok'+num).remove()
        $("#"+input).css('border', '1px solid red').after('<span id="ok'+num+'" style="color:red" class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span>');
        return 1;
    }
}


    var key=0;
$("#FormDatosEmpresa").submit(function() {
    key = validarFormDatosEmpresa('razon_social', mens[0],0)//
    if(key == 1){return false}
    key = validarFormDatosEmpresa('siglas', mens[1],1)//
    if(key == 1){return false}
    key = validarFormDatosEmpresa('rif', mens[2],2)//
    if(key == 1){return false}
    key = validarFormDatosEmpresa('id_gen_sector', mens[3],3)//
    if(key == 1){return false}
    key = validarFormDatosEmpresa('gen_actividad_eco_id', mens[4],4)//
    if(key == 1){return false}
    key = validarFormDatosEmpresa('gen_tipo_empresa_id', mens[5],5)//
    if(key == 1){return false}
    key = validarradio('export', mens[6],6)//
    if(key == 1){return false}
    key = validarFormDatosEmpresa('pais_id', mens[7],7)//
    if(key == 1){return false}
    key = validarFormDatosEmpresa('estado_id', mens[8],8)//
    if(key == 1){return false}
    key = validarFormDatosEmpresa('municipio_id', mens[9],9)//
    if(key == 1){return false}
    key = validarFormDatosEmpresa('parroquia_id', mens[10],10)//
    if(key == 1){return false}
    key = validarFormDatosEmpresa('direccion', mens[11],11)//
    if(key == 1){return false}
    key = validarFormDatosEmpresa('telefono_local', mens[12],12)//
    /*if(key == 1){return false}
    key = validarFormDatosEmpresa('cfax', mens[11],11)*/
    if(key == 1){return false}
    key = validarFormDatosEmpresa('telefono_movil', mens[13],13)//
    if(key == 1){return false}
    key = validarFormDatosEmpresa('circuns_judicial_id', mens[14],14)//
    if(key == 1){return false}
    key = validarFormDatosEmpresa('num_registro', mens[15],15)//
    if(key == 1){return false}
    key = validarFormDatosEmpresa('tomo', mens[16],16)//
    if(key == 1){return false}
    key = validarFormDatosEmpresa('folio', mens[17],17)//
    if(key == 1){return false}
    key = validarForm('capital_social', mens[18],18)//
    if(key == 1){return false}
    key = validarFormDatosEmpresa('f_registro_mer', mens[19],19)//
    if(key == 1){return false}
    key = validarFormDatosEmpresa('nombre_presid', mens[20],20)//
    if(key == 1){return false}
    key = validarFormDatosEmpresa('apellido_presid', mens[21],21)//
    if(key == 1){return false}
    key = validarFormDatosEmpresa('ci_presid', mens[22],22)//
    if(key == 1){return false}
    key = validarFormDatosEmpresa('cargo_presid', mens[23],23)//
    if(key == 1){return false}
    key = validarFormDatosEmpresa('nombre_repre', mens[24],24)//
    if(key == 1){return false}
    key = validarFormDatosEmpresa('apellido_repre', mens[25],25)//
    if(key == 1){return false}
    key = validarFormDatosEmpresa('ci_repre', mens[26],26)//
    if(key == 1){return false}
    key = validarFormDatosEmpresa('cargo_repre', mens[27],27)//
    if(key == 1){return false}

////$('#accion_si').click(function(event) {///Cuando esta Chequeado en No me manda a la pregunta de productos/////
       if ($("#accion_si").is(':checked')) {
         key = validarForm('nombre_accionista', mens[29],29)//
        if(key == 1){return false}
         key = validarForm('apellido_accionista', mens[30],30)//
        if(key == 1){return false}
         key = validarForm('cedula_accionista', mens[31],31)//
        if(key == 1){return false}
        key = validarForm('cargo_accionista', mens[32],32)//
        if(key == 1){return false}
        }
    //});

    if ($("#prod_tecnologia_si").is(':checked') || $("#prod_tecnologia_no").is(':checked')) {

    }else{
        swal("Por Favor!", "Estimado Usuario. Debe Seleccionar si Exporta o no Productos de Servicios y/o Tecnología En el Paso 3 para completar la inscripción", "warning")
            $("#prod_tecnologia_no").focus();
            key=1;
            $('#ok33').remove()
            $("#prod_tecnologia_no").css('border', '1px solid red').after('<span id="ok'+'33'+'" style="color:red" class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span>');
            return 1;
    }

    if ($("#prod_tecnologia_si").is(':checked')){

        key = validarForm('especifique_tecno', mens[48],48)
        if(key == 1){return false}
    }

    if ($("#prod_tecnologia_no").is(':checked')) {

      $('#cod_arancel_edit').click(function(event) {
        if ($("#cod_mercosur").is(':checked') || $("#cod_andina").is(':checked')) {
          /*key = validarradio('cod_mercosur', mens[32],32)
            if(key == 1){return false}*/
           key = validarFormDatosEmpresa('cod_arancel', mens[34],34)
           if(key == 1){return false}
           key = validarFormDatosEmpresa('descrip_arancel', mens[36],36)
           if(key == 1){return false}
           key = validarFormDatosEmpresa('descrip_comercial', mens[37],37)
           if(key == 1){return false}
           key = validarForm('gen_unidad_medida_id', mens[38],38)
           if(key == 1){return false}
           key = validarFormDatosEmpresa('cap_opera_anual', mens[39],39)
           if(key == 1){return false}
           key = validarFormDatosEmpresa('cap_insta_anual', mens[40],40)
           if(key == 1){return false}
           key = validarFormDatosEmpresa('cap_alamcenamiento', mens[41],41)
           if(key == 1){return false}
        }else{
            key = validarradio('cod_mercosur', mens[35],35)
            if(key == 1){return false}
        }
      });
    }



    /*key = validarFormDatosEmpresa('tipo_arancel', mens[32],32)
    if(key == 1){return false}*/
    key = validarFormDatosEmpresa('correo', mens[42],42)
    if(key == 1){return false}
    key = validarFormDatosEmpresa('correo_sec', mens[43],43)
    if(key == 1){return false}

   /* key = validarForm('cmail_prim', mens[20],20)
    if(key == 1){return false}*/
  //  key=validarEmail($('#cmail_prim').val());


    key=validarCorreo($('#correo').val(),$('#correo_sec').val())
    if(key == 1)
    {
       return false;

    }


    if (key==0) {
        return true;
    }
});

}

/**************************************Validación de Correo**********************************************/

function validarCorreo(email,email1){

    expr = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    var  msg= "El Campo esta Vacio...Debe Ingresar un Correo ";
    var  msj2="La Direccion de Correo Ingresada es Incorrecta";
    var  msj3="La Dirección de Correo Secundario no debe ser igual al Correo Electronico";
    var email = document.getElementById('correo').value;
    var email_seg = document.getElementById('correo_sec').value;
    var  x=true;


      if( ( (!expr.test(email)) && (email!='') ) ){


        swal("Por Favor!", msj2, "warning")
        //swal(msj2, "info");
         //alert(msj2);
         x=false;
         $("#correo,#email").css('border', '1px solid red').after('<span id="ok20" style="color:red" class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span>');


        $("#correo,#email").focus();
         return 1;


        }

        if ((!expr.test(email1)) && (email1!='')) {

             swal("Por Favor!", msj2, "warning")
        //swal(msj2, "info");
         //alert(msj2);
         x=false;
             $("#correo_sec,#email_seg").css('border', '1px solid red').after('<span id="ok20" style="color:red" class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span>');
         $("#correo_sec,#email_seg").focus();
         return 1;
        }

      if( email=='' ) {
        swal("Por Favor!", msg, "warning")

        //swal(msg, "info");
        //alert(msg);
        x=false;
        return 1;
        }


       if(email==email1 ) {
        swal("Por Favor!", msj3, "warning")
        //swal(msj3, "info");
       //alert(msj3);
       x=false;
       $("#correo_sec,#email_seg").css('border', '1px solid red').after('<span id="ok20" style="color:red" class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span>');
         $("#correo_sec,#email_seg").focus();
       return 1;

       }

       if(x==true){

       $("#correo,#email").css('border', '1px solid green').after('<span id="ok20" style="color:green" class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span>');
       $("#correo_sec,#email_seg").css('border', '1px solid green').after('<span id="ok21" style="color:green" class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span>');
       return 0;

       }



  }

  /********************Validación de la fecha de Registro Mercantil**************************************/
function validarfechaRM(fechaCalendario){
var  msg= "La fecha ingresada no puede ser mayor a la fecha actual";
var f = new Date();
var fAhora = f.getFullYear() + "/" + (f.getMonth()+1)+ "/" +f.getDate();
//var fechaCalendario = $('#f_registro_mer').val();
var  x=true;
if((Date.parse(fechaCalendario)) > (Date.parse(fAhora))){
        swal("Por Favor!", msg, "warning")
         x=false;
         $("#f_registro_mer").val('');

}
if(x==true){

       $("#f_registro_mer").css('border', '1px solid green').after('<span id="ok20" style="color:green" class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span>');
    }else{
        $("#f_registro_mer").focus();
         $("#f_registro_mer").css('border', '1px solid red').after('<span id="ok20" style="color:red" class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span>');
    }
}

/********************************************************************************************/

/*Validacion Formulario Cambio Contraseña*/

function enviarcambioContrasena() {

var mens=[
    'Estimado usuario. Debe ingresar CORREO ELECTRONICO para completar el registro',
    'Estimado usuario. Debe ingresar CONTRASEÑA para completar el registro',
    'Estimado usuario. Debe ingresar CONFIRMACIÓN DE CONTRASEÑA para completar el registro',
]

function validarformcambContras(input, mensaje,num) {
    if ($("#"+input).val()=="") {

        swal("Por Favor!", mensaje, "warning")
        //swal(mensaje, "info");
        //alert(mensaje)
        $("#"+input).focus();
        key=1;
        $('#ok'+num).remove()
        $("#"+input).css('border', '1px solid red').after('<span id="ok'+num+'" style="color:red" class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span>');
        return 1;
    }else{
        $('#ok'+num).remove()
        $("#"+input).css('border', '1px solid green').after('<span id="ok'+num+'" style="color:green" class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span>');
        return 0;
    }
}

 var key=0;
$("#formcambContras").submit(function() {
    key = validarformcambContras('email', mens[0],0)
    if(key == 1){return false}
    key = validarformcambContras('password', mens[1],1)
    if(key == 1){return false}
    key = validarformcambContras('password_confirm', mens[2],2)
    if(key == 1){return false}


    key=validarcambioContrasena($('#password').val(),$('#password_confirm').val())
    if(key == 1)
    {
       return false;

    }

    if (key==0) {
        return true;
    }
  });
}

/*------------------------Validación de la Cambio de Contraseña-------------------------*/
function validarcambioContrasena(clave1,clave2){
    var  msg= "El Campo Contraseñaaaaa debe tener minimo 6 caracteres";
    var  msj2="El Campo Contraseña no puede estar Vacio";
    var  msj3="El Campo Confirmación de Contraseña tiene que ser igual al Campo Contraseña";
    var clave1 = document.getElementById('password').value;
    var clave2 = document.getElementById('password_confirm').value;
    var  x=true;


    if (clave1.length< 6) {
        swal("Por Favor!", msg, "warning")
        //swal(msj2, "info");
         //alert(msj2);
         x=false;
         return 1;
    }

    if (clave2 != clave1) {
        swal("Por Favor!", msj3, "warning")
        //swal(msj2, "info");
         //alert(msj2);
        x=false;
        return 1;
    }

    if(x==true){

       $("#password").css('border', '1px solid green').after('<span id="ok20" style="color:green" class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span>');
       $("#password_confirm").css('border', '1px solid green').after('<span id="ok21" style="color:green" class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span>');
       return 0;
    }
}
/***********Validar Formulario de Financiamiento Solicitud********/
/*function notaCredito() {
  //if ($('#gen_nota_credito_id').val() == "Seleccione un Número de Nota de Crédito") {
        var v=$('#num_nota').val();
        alert(v);
   // }
}*/

function FinacSolicitud() {

var mens=[
/*0*/'Estimado usuario. Debe seleccionar si posee o no una Nota de Crédito para completar el registro',
/*1*/'Estimado usuario. Debe seleccionar una Nota de Crédito para completar el registro',
/*2*/'Estimado usuario. Debe seleccionar un Régimen de Exportación para completar el registro',
/*3*/'Estimado usuario. Debe ingresar la fecha de emisión para completar el registro',
/*4*/'Estimado usuario. Debe Seleccionar un Régimen Especial para completar el registro',
/*5*/'Estimado usuario. Debe Seleccionar una Entidad Bancaria para completar el registro',
/*6*/'Estimado usuario. Debe Seleccionar un Tipo de Instrumento de Crédito para completar el registro',
/*7*/'Estimado usuario. Debe ingresar el Número de Contrato para completar el registro',
/*8*/'Estimado usuario. Debe ingresar la Fecha de Aprobación del Contrato para completar el registro',
/*9*/'Estimado usuario. Debe ingresar la Fecha de Vencimiento del Contrato para completar el registro',
/*10*/'Estimado usuario. Debe ingresar el Monto Aprobado para completar el registro',
/*11*/'Estimado usuario. Debe ingresar el Número de Cuotas para completar el registro',
/*12*/'Estimado usuario. Debe ingresar el Monto Amortizado para completar el registro',
/*13*/'Estimado usuario. Debe ingresar el Número de Cuota Amortizada para completar el registro',
/*14*/'Estimado usuario. Debe Especificar el Régimen para completar el registro',
/*14*/'Estimado usuario. Debe Seleccionar Un Número de Anticipo para completar el registro',


]
//alert(mens);
function FinacSolicitud(input, mensaje,num) {
    if ($("#"+input).val()=="") {

        swal("Por Favor!", mensaje, "warning")
        //swal(mensaje, "info");
        //alert(mensaje)
        $("#"+input).focus();
        key=1;
        $('#ok'+num).remove()
        $("#"+input).css('border', '1px solid red').after('<span id="ok'+num+'" style="color:red" class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span>');
        return 1;
    }else{
        $('#ok'+num).remove()
        $("#"+input).css('border', '1px solid green').after('<span id="ok'+num+'" style="color:green" class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span>');
        return 0;
    }
}

    var key=0;
    $("#FinacSolicitud").submit(function() {
     key = FinacSolicitud('nota_credito', mens[0],0)
    if(key == 1){return false}

    if ($("#credito_si").is(':checked')){
     //if ($('gen_nota_credito_id').val()=='Seleccione un Número de Nota de Crédito') {

     //}
      key = FinacSolicitud('gen_nota_credito_id', mens[1],1)
      if(key == 1){return false}
  }




    key = FinacSolicitud('cat_regimen_export_id', mens[2],2)
    if(key == 1){return false}

    var RegExpor= $('#cat_regimen_export_id').val()
//alert(RegExpor);
    if(RegExpor==1){
    key = FinacSolicitud('financiamiento', mens[3],3)
    if(key == 1){return false}

  }

  var regimenEspe=$('#cat_regimen_export_id').val()
  if(regimenEspe==2){
  key = FinacSolicitud('cat_regimen_especial_id', mens[4],4)
    if(key == 1){return false}
      }
    if ($("#financiamiento_si").is(':checked')){


    key = FinacSolicitud('entidad_financiera', mens[5],5)
    if(key == 1){return false}


      key = FinacSolicitud('tipo_instrumento_id', mens[6],6)
    if(key == 1){return false}


      key = FinacSolicitud('nro_contrato', mens[7],7)
    if(key == 1){return false}


      key = FinacSolicitud('fapro_contrato', mens[8],8)
    if(key == 1){return false}


   key = FinacSolicitud('fvenci_contrato', mens[9],9)
    if(key == 1){return false}

    key = FinacSolicitud('monto_apro_contrato', mens[10],10)
    if(key == 1){return false}

    key = FinacSolicitud('num_cuota_contrato', mens[11],11)
    if(key == 1){return false}

    key = FinacSolicitud('monto_amort_contrato', mens[12],12)
    if(key == 1){return false}

    key = FinacSolicitud('num_cuota_amortizado', mens[13],13)
    if(key == 1){return false}

    }

  var Especireg= $('#cat_regimen_especial_id').val()
  //alert(Especireg);
  if(Especireg==3){
  key = FinacSolicitud('especifique_regimen', mens[14],14)
    if(key == 1){return false}
  }

  if ($("#accion_si").is(':checked')){
  key = FinacSolicitud('gen_anticipo_id', mens[15],15)
    if(key == 1){return false}
    }




    if (key==0) {
        return true;
    }
});
}



/*Validacion Formulario Registro de Permisos*/

function enviarpermiso() {

var mens=[
    'Estimado usuario. Debe seleccionar el Tipo de Permiso para completar el registro',
    'Estimado usuario. Debe ingresar el Número de Permiso para completar el registro',
    'Estimado usuario. Debe ingresar la Fecha de Permiso para completar el registro',
    'Estimado usuario. Debe ingresar la Fecha Aprobación del Permiso para completar el registro',
    'Estimado usuario. Debe seleccionar la Aduana de Salida para completar el registro',
    'Estimado usuario. Debe seleccionar el País Destino para completar el registro',
]

function validarformpermiso(input, mensaje,num) {
    if ($("#"+input).val()=="") {

        swal("Por Favor!", mensaje, "warning")
        //swal(mensaje, "info");
        //alert(mensaje)
        $("#"+input).focus();
        key=1;
        $('#ok'+num).remove()
        $("#"+input).css('border', '1px solid red').after('<span id="ok'+num+'" style="color:red" class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span>');
        return 1;
    }else{
        $('#ok'+num).remove()
        $("#"+input).css('border', '1px solid green').after('<span id="ok'+num+'" style="color:green" class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span>');
        return 0;
    }
}

    var key=0;
    $("#formpermiso").submit(function() {
    key = validarformpermiso('tipo_permiso_id', mens[0],0)
    if(key == 1){return false}
    key = validarformpermiso('numero_permiso', mens[1],1)
    if(key == 1){return false}
    key = validarformpermiso('fecha_permiso', mens[2],2)
    if(key == 1){return false}
    key = validarformpermiso('fecha_aprobacion', mens[3],3)
    if(key == 1){return false}
    key = validarformpermiso('gen_aduana_salida_id', mens[4],4)
    if(key == 1){return false}
    key = validarformpermiso('pais_id', mens[5],5)
    if(key == 1){return false}

    if (key==0) {
        return true;
    }
});
}

/*Validacion Formulario Registro Nota de Credito*/

function enviarcredito() {

var mens=[

    'Estimado usuario. Debe seleccionar el Consignatario para completar el registro',
    'Estimado usuario. Debe ingresar el Numero de Nota de Crédito para completar el registro',
    'Estimado usuario. Debe ingresar la Fecha de Emisión para completar el registro',
    'Estimado usuario. Debe ingresar el Concepto de la Nota de Crédito para completar el registro',
    'Estimado usuario. Debe ingresar el Monto de Nota Crédito para completar el registro',
    'Estimado usuario. Debe seleccionar el Numero de Factura Asociada para completar el registro',
    'Estimado usuario. Debe ingresar la Justificación de la Nota de Crédito para completar el registro',
    'Estimado usuario. El Monto de la Nota de Crédito no Debe ser Mayor al Monto de la Factura!',
]

function validarformnotacredito(input, mensaje,num) {
    if ($("#"+input).val()=="") {

        swal("Por Favor!", mensaje, "warning")
        //swal(mensaje, "info");
        //alert(mensaje)
        $("#"+input).focus();
        key=1;
        $('#ok'+num).remove()
        $("#"+input).css('border', '1px solid red').after('<span id="ok'+num+'" style="color:red" class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span>');
        return 1;
    }else{
        $('#ok'+num).remove()
        $("#"+input).css('border', '1px solid green').after('<span id="ok'+num+'" style="color:green" class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span>');
        return 0;
    }
}

function validarmontomayor(input1,input2, mensaje,num) {
    if (parseFloat($("#"+input1).val()) > parseFloat($("#"+input2).val())) {
        swal("Por Favor!", mensaje, "warning")
        //swal(mensaje, "info");
        //alert(mensaje)
        $("#"+input1).focus();
        key=1;
        $('#ok'+num).remove()
        $("#"+input1).css('border', '1px solid red').after('<span id="ok'+num+'" style="color:red" class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span>');
        return 1;
    }else{
        $('#ok'+num).remove()
        $("#"+input1).css('border', '1px solid green').after('<span id="ok'+num+'" style="color:green" class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span>');
        return 0;
    }
}
   
    var key=0;
    $("#formncredito").submit(function() {
    key = validarformnotacredito('gen_consignatario_id', mens[0],0)
    if(key == 1){return false}
    key = validarformnotacredito('numero_nota_credito', mens[1],1)
    if(key == 1){return false}
    key = validarformnotacredito('femision', mens[2],2)
    if(key == 1){return false}
    key = validarformnotacredito('concepto_nota_credito', mens[3],3)
    if(key == 1){return false}
    key = validarformnotacredito('monto_nota_credito', mens[4],4)
    if(key == 1){return false}

    key = validarformnotacredito('gen_factura_id', mens[5],5)
    if(key == 1){return false}
     
      key = validarformnotacredito('justificacion', mens[6],6)
    if(key == 1){return false}
    key = validarmontomayor('monto_nota_credito','monto_fob', mens[7],4)
    if(key == 1){return false}

    if (key==0) {
        return true;
    }
});
}


/*Validacion Formulario Registro Nota de Debito*/

function enviardebito() {

var mens=[
    'Estimado usuario. Debe seleccionar un Consignatario para completar el registro',
    'Estimado usuario. Debe ingresar el número nota de debito para completar el registro',
    'Estimado usuario. Debe ingresar la fecha de emisión para completar el registro',
    'Estimado usuario. Debe ingresar el concepto de la Nota de Débito para completar el registro',
    'Estimado usuario. Debe ingresar el Monto de Nota Débito para completar el registro',
    'Estimado usuario. Debe ingresar el el número de factura asociada para completar el registro',
    'Estimado usuario. Debe ingresar la ER para completar el registro',
    'Estimado usuario. Debe ingresar la justificación para completar el registro',


]

function validarFormDebito(input, mensaje,num) {
    if ($("#"+input).val()=="") {

        swal("Por Favor!", mensaje, "warning")
        //swal(mensaje, "info");
        //alert(mensaje)
        $("#"+input).focus();
        key=1;
        $('#ok'+num).remove()
        $("#"+input).css('border', '1px solid red').after('<span id="ok'+num+'" style="color:red" class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span>');
        return 1;
    }else{
        $('#ok'+num).remove()
        $("#"+input).css('border', '1px solid green').after('<span id="ok'+num+'" style="color:green" class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span>');
        return 0;
    }
}

    var key=0;
    $("#NDebito").submit(function() {
     key = validarFormDebito('gen_consignatario_id', mens[0],0)
    if(key == 1){return false}
    key = validarFormDebito('num_nota_debito', mens[1],1)
    if(key == 1){return false}
    key = validarFormDebito('fecha_emision', mens[2],2)
    if(key == 1){return false}
    key = validarFormDebito('concepto_nota_debito', mens[3],3)
    if(key == 1){return false}
    key = validarFormDebito('monto_nota_debito', mens[4],4)
    if(key == 1){return false}
      key = validarFormDebito('gen_factura_id', mens[5],5)
    if(key == 1){return false}
      key = validarFormDebito('er', mens[6],6)
    if(key == 1){return false}
      key = validarFormDebito('justificacion', mens[7],7)
    if(key == 1){return false}


    if (key==0) {
        return true;
    }
});
}

/*******************Validacion Formulario Registro ANTiCiPO********************/

function enviaranticipo() {

var mens=[

    'Estimado usuario. Debe ingresar el Número de Anticipo para completar el registro',
    'Estimado usuario. Debe ingresar la Fecha de Anticpo para completar el registro',
    'Estimado usuario. Debe ingresar el Consignatario completar el registro',
    'Estimado usuario. Debe ingresar el Número de Documento Comercial para completar el registro',
    'Estimado usuario. Debe ingresar el Monto de Anticipo para completar el registro',
    'Estimado usuario. Debe ingresar el Monto Total de Anticipo para completar el registro',
    'Estimado usuario. Debe ingresar la Observación para completar el registro',
    'Estimado usuario. Debe seleccionar la Divisa para completar el registro',

]

function validarAnticipo(input, mensaje,num) {
    if ($("#"+input).val()=="") {

        swal("Por Favor!", mensaje, "warning")
        //swal(mensaje, "info");
        //alert(mensaje)
        $("#"+input).focus();
        key=1;
        $('#ok'+num).remove()
        $("#"+input).css('border', '1px solid red').after('<span id="ok'+num+'" style="color:red" class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span>');
        return 1;
    }else{
        $('#ok'+num).remove()
        $("#"+input).css('border', '1px solid green').after('<span id="ok'+num+'" style="color:green" class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span>');
        return 0;
    }
}

    var key=0;
    $("#anticipo").submit(function() {
    key = validarAnticipo('nro_anticipo', mens[0],0)
    if(key == 1){return false}
    key = validarAnticipo('fecha_anticipo', mens[1],1)
    if(key == 1){return false}
    key = validarAnticipo('gen_consignatario_id', mens[2],2)
    if(key == 1){return false}
    key = validarAnticipo('gen_divisa_id', mens[7],7)
    if(key == 1){return false}
    key = validarAnticipo('nro_doc_comercial', mens[3],3)
    if(key == 1){return false}
    key = validarAnticipo('monto_anticipo', mens[4],4)
    if(key == 1){return false}
    key = validarAnticipo('monto_total_ant', mens[5],5)
    if(key == 1){return false}
    key = validarAnticipo('observaciones', mens[6],6)
    if(key == 1){return false}




    if (key==0) {
        return true;
    }
});
}

/********************* Validación de Productos-Factura *********************/

function validarProductosFactura(){


   var cant="";
   var precio=""
   var unidadM="";
   var permiso="";
   var desComercial="";
   var desArancelaria="";
   var codigo="";



 /*************************** Validación del select permiso *****************/

   selectedopts = $('[id^=permiso] option:selected');
       for(i=0;i<selectedopts.length;i++)
        {
          if(selectedopts.eq(i).text() == 'Seleccione')
            {
           permiso+=1;
           swal("Por Favor!","Permiso es requerido para completar el registro", "warning")

         }
     }


/******************** VALIDACION DE PRECIO ********************************/

          $('input[name="precio_producto[]"]').each(function(index1,value1){


            if(value1.value==""){

              precio+=1;
               swal("Por Favor!","Precio es requerido", "warning")
             }

           });

 /********************** validacion del select *******************************/
               selectedopts = $('[id^=unidad_medida_id] option:selected');
                   for(i=0;i<selectedopts.length;i++)
                    {
                      if(selectedopts.eq(i).text() == 'Seleccione')
                        {
                       unidadM+=1;
                       swal("Por Favor!","Unidad de Medida es requerido para completar el registro", "warning")
                       //return false;
                     }
                 }

/***************************************************************************/

  $('input[name="cantidad_producto[]"]').each(function(index,value){


    if(value.value==""){

      cant+=1;
       swal("Por Favor!","Cantidad es requerido", "warning")
     }


   });

  /****************** Validación del Campo Descripción Comercial *******/


  $('input[name="descripcion_comercial[]"]').each(function(index2,value2){


    if(value2.value==""){

      desComercial+=1;
       swal("Por Favor!","Descripción Comercial es requerido para completar el registro", "warning")
     }

   });

 /****************** Validación del Campo Descripción Arancelaria *******/

 $('input[name="descripcion_arancelaria[]"]').each(function(index3,value3){


   if(value3.value==""){

     desArancelaria+=1;
      swal("Por Favor!","Descripción Arancelaria es requerido para completar el registro", "warning")
    }

  });

/********************** Validación del Campo Código Arancel ***descripcion_arancelaria*/

$('input[name="codigo_arancel[]"]').each(function(index4,value4){


  if(value4.value==""){

    codigo+=1;
     swal("Por Favor!","Código Arancelario es requerido para completar el registro", "warning")
   }

 });



 /***************************************************************************/
   if( (cant=="") && (precio=="") && (unidadM=="") && (desComercial=="") && (desArancelaria=="") && (codigo=="") && (permiso=="")) {

         $('#FormProd').submit();

       }

}

function validarProductosTecno(){


   var codigo="";
   var descripcion="";
   var cant="";
   var unidadM="";
   var precio="";


/***************************************************************************/

   $('input[name="precio[]"]').each(function(index1,value1){


     if(value1.value==""){

       precio+=1;
        swal("Por Favor!","Precio es requerido", "warning")
      }

    });

/********************** validacion del select *******************************/
    input_select= $('[id^=unidad_medida_id] option:selected');
        for(i=0;i<input_select.length;i++)
         {
           if(input_select.eq(i).text() == 'Seleccione')
             {
            unidadM+=1;
            swal("Por Favor!","Unidad de Medida es requerido para completar el registro", "warning")
            //return false;
          }
      }

 /************************************************************************/

 $('input[name="cantidad[]"]').each(function(index,value){


   if(value.value==""){

     cant+=1;
      swal("Por Favor!","Cantidad es requerido", "warning")
    }


  });


  /****************** Validación del Campo Descripción  *******/


  $('input[name="descripcion[]"]').each(function(index2,value2){


    if(value2.value==""){

      descripcion+=1;
       swal("Por Favor!","Descripción es requerido para completar el registro", "warning")
     }

   });


/********************** Validación del Campo Código Arancel ***descripcion_arancelaria*/

$('input[name="codigo_arancel[]"]').each(function(index4,value4){


  if(value4.value==""){

    codigo+=1;
     swal("Por Favor!","Código Arancelario es requerido para completar el registro", "warning")
   }

 });



 /***************************************************************************/
 // && (unidadM=="") && (descripcion=="")
   if((codigo=="") && (descripcion=="")  && (cant=="") && (unidadM=="") &&  (precio=="") ) {

         $('#formProdTecno').submit();

       }

}


function validarProductos(){


   var codigo="";
   var descripcion_aran="";
   var descripcion_comer="";
   var cant="";
   var unidadM="";
   var cap_oa="";
   var cap_ins="";
   var cap_alm="";


   /***************************************************************************/

      $('input[name="cap_alamcenamiento[]"]').each(function(index1,value1){


        if(value1.value==""){

          cap_alm+=1;
           swal("Por Favor!","Capacidad de Almacenamiento Anual es requerido", "warning")
         }

       });


   /***************************************************************************/

      $('input[name="cap_insta_anual[]"]').each(function(index1,value1){


        if(value1.value==""){

          cap_ins+=1;
           swal("Por Favor!","Capacidad Instalada Anual es requerido", "warning")
         }

       });


/***************************************************************************/

   $('input[name="cap_opera_anual[]"]').each(function(index1,value1){


     if(value1.value==""){

       cap_oa+=1;
        swal("Por Favor!","Capacidad Operativa Anual es requerido", "warning")
      }

    });

/********************** validacion del select *******************************/
    input_select= $('[id^=gen_unidad_medida_id] option:selected');
        for(i=0;i<input_select.length;i++)
         {
           if(input_select.eq(i).text() == '--Seleccione una Unidad de Medida--')
             {
            unidadM+=1;
            swal("Por Favor!","Unidad de Medida es requerido para completar el registro", "warning")
            //return false;
          }
      }



  /****************** Validación del Campo Descripción-Comercial  *******/


  $('input[name="descrip_comercial[]"]').each(function(index2,value2){


    if(value2.value==""){

      descripcion_comer+=1;
       swal("Por Favor!","Descripción Comercial es requerido para completar el registro", "warning")
     }

   });

  /****************** Validación del Campo Descripción  *******/


  $('input[name="descrip_arancel[]"]').each(function(index2,value2){


    if(value2.value==""){

      descripcion_aran+=1;
       swal("Por Favor!","Descripción Arancelaria es requerido para completar el registro", "warning")
     }

   });


/********************** Validación del Campo Código Arancel ***descripcion_arancelaria*/

$('input[name="codigo_arancel[]"]').each(function(index4,value4){


  if(value4.value==""){

    codigo+=1;
     swal("Por Favor!","Código Arancelario es requerido para completar el registro", "warning")
   }

 });



 /***************************************************************************/
 // && (unidadM=="") && (descripcion=="")
   if((codigo=="") && (descripcion_aran=="")  && (descripcion_comer=="") && (unidadM=="") &&  (cap_oa=="") && (cap_ins=="") && (cap_alm=="")) {

         $('#formProductos').submit();

       }

}


function enviarNaturalEdit() {
    /*inicio validacion para el formato de la fecha
    var fTemp = $("#fregistro_mer").val();
    var fcampo = fTemp.split("/")
    var result = fcampo[2].substr(2, 4)
        $("#fregistro_mer").val(fcampo[0]+"/"+fcampo[1]+"/"+result);
    /*fin validacion para el formato de la fecha*/

var mens=[
     ////////////////////Parte 1 Del Registro ingresa los datos personales///////////////////////////////////////////
    'Estimado usuario. Debe ingresar NOMBRE O RAZÓN SOCIAL para completar la inscripción',//0
    'Estimado usuario. Debe ingresar RIF para completar la inscripción',//1
    'Estimado usuario. Debe ingresar un SECTOR   para completar la inscripción',//2
    'Estimado usuario. Debe seleccionar ACTIVIDAD ECONÓMICA  para completar la inscripción',//3
    'Estimado usuario. Debe seleccionar Si es EXPORTADOR, INVERSIONISTA, PRODUCTOR, COMERCIALIZADORA  para completar la inscripción',//4
    'Estimado usuario. Debe seleccionar PAÍS  para completar la inscripción',//5
    'Estimado usuario. Debe seleccionar ESTADO  para completar la inscripción',//6
    'Estimado usuario. Debe seleccionar MUNICIPIO para completar la inscripción',//7
    'Estimado usuario. Debe seleccionar PARROQUIA  para completar la inscripción',//8
    'Estimado usuario. Debe ingresar una DIRECCIÓN  para completar la inscripción',//9
    'Estimado usuario. Debe ingresar TELÉFONO LOCAL   para completar la inscripción',//10
    'Estimado usuario. Debe ingresar TELÉFONO CELULAR  para completar la inscripción',//11

    ///////////////////////Parte 3 del  Registro ingresa los productos /////////////////////////////////////
    'Estimado usuario. Debe Seleccionar si Exporta o no Productos de Tecnologia Paso para completar la inscripción',//12
    'Estimado usuario. Debe Seleccionar un CÓDIGO ARANCELARIO NANDINA O MERCOSUR En el Paso 3 para completar la inscripción',//13
    'Estimado usuario. Debe ingresar CÓDIGO ARANCELARIO  para completar la inscripción',////14
    'Estimado usuario. Debe ingresar una DESCRIPCIÓN ARANCELARIA  para completar la inscripción',//15
    'Estimado usuario. Debe ingresar una DESCRIPCIÓN COMERCIAL para completar la inscripción',//16
    'Estimado usuario. Debe ingresar una UNIDAD DE MEDIDA  para completar la inscripción',//17
    'Estimado usuario. Debe ingresar la CAPACIDAD OPERATIVA ANUAL  para completar la inscripción',//18
    'Estimado usuario. Debe ingresar la CAPACIDAD INSTALADA ANUAL  para completar la inscripción',//19
    'Estimado usuario. Debe ingresar la CAPACIDAD DE ALMACENAMIENTO ANUAL  para completar la inscripción',//20
    ///////////////////////Parte 4 del  Registro ingresa los email y pregunta de seguridad/////////////////////////////////////
    'Estimado usuario. Debe ingresar CORREO ELECTRÓNICO para completar la inscripción',//21
    'Estimado usuario. Debe ingresar CORREO ELECTRÓNICO SECUNDARIO  para completar la inscripción',//22
    'Estimado usuario. Debe ingresar PASSWORD para completar la inscripción',//23
    'Estimado usuario. Debe ingresar CONFIRMACIÓN DE PASSWORD para completar la inscripción',//24
    'Estimado usuario. Debe ingresar UNA PREGUNTA DE SEGURIDAD  para completar la inscripción',//25
    'Estimado usuario. Debe ingresar UNA RESPUESTA DE SEGURIDAD  para completar la inscripción',//26
    'Estimado usuario. Debe ingresar LA ESPECIFICACIÓN DE LA PRODUCCIÓN DE SERVICIOS Y/O TECNLOGÍA  para completar la inscripción',//27



    'Estimado usuario. Debe ACEPTAR LAS CONDICIONES ANTES EXPUESTAS para completar la inscripción',//28
]

function validarForm_pnatural(input, mensaje,num) {



    if ($("#"+input).val()=="") {

      //  console.log($("#"+input).val());
        swal("Por Favor!", mensaje, "warning")
        //swal(mensaje, "info");
        //alert(mensaje)
        $("#"+input).focus();
        key=1;
        $('#ok'+num).remove()
        $("#"+input).css('border', '1px solid red').after('<span id="ok'+num+'" style="color:red" class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span>');
        return 1;
    }else{
        $('#ok'+num).remove()
        $("#"+input).css('border', '1px solid green').after('<span id="ok'+num+'" style="color:green" class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span>');
        return 0;
    }

}


function validarradio(input, mensaje,num){


  if ( ($("#"+input).is(':checked')) || ($("#invers").is(':checked')) || ($("#produc").is(':checked')) || ($("#comerc").is(':checked')) ) {


        $('#ok'+num).remove()
        $("#"+input).css('border', '1px solid green').after('<span id="ok'+num+'" style="color:green" class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span>');
        return 0;

    }else{

        swal("Por Favor!", mensaje, "warning")
        $("#"+input).focus();
        key=1;
        $('#ok'+num).remove()
        $("#"+input).css('border', '1px solid red').after('<span id="ok'+num+'" style="color:red" class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span>');
        return 1;
    }
}


function validarCondicion(input,mensaje,num){

  if ( $("#"+input).is(':checked') ) {


        $('#ok'+num).remove()
        $("#"+input).css('border', '1px solid green').after('<span id="ok'+num+'" style="color:green" class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span>');
        return 0;

    }else{

        swal("Por Favor!", mensaje, "warning")
        $("#"+input).focus();
        key=1;
        $('#ok'+num).remove()
        $("#"+input).css('border', '1px solid red').after('<span id="ok'+num+'" style="color:red" class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span>');
        return 1;
    }

}

 function validarRadio(input,mensaje,num){


   var radio_expor_prod="";


   $('input[name='+input+']').each(function(index,value){

        if(value.checked) { radio_expor_prod=1; }
   });

  //console.log(almacen);
  if(radio_expor_prod==""){

    swal("Por Favor!", mensaje, "warning")
    //swal(mensaje, "info");

    $("#"+input).focus();
    key=1;
    $('#ok'+num).remove();
    $("#"+input).css('border', '1px solid red').after('<span id="ok'+num+'" style="color:red" class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span>');

    return 1;

    }else{


        $('#ok'+num).remove()
        $("#"+input).css('border', '1px solid green').after('<span id="ok'+num+'" style="color:green" class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span>');
        return 0;

       }

 }

    var key=0;

    key = validarForm_pnatural('razon_social', mens[0],0)//
    if(key == 1){return false}
    key = validarForm_pnatural('rif', mens[1],1)///
    if(key == 1){return false}
    key = validarForm_pnatural('id_gen_sector', mens[2],2)//
    if(key == 1){return false}
    key = validarForm_pnatural('id_gen_actividad_eco', mens[3],3)//
    if(key == 1){return false}

   key = validarradio('export', mens[4],4)//
    if(key == 1){return false}

   key = validarForm_pnatural('id_pais', mens[5],5)//
    if(key == 1){return false}
    key = validarForm_pnatural('id_estado', mens[6],6)//
    if(key == 1){return false}
    key = validarForm_pnatural('id_municipio', mens[7],7)//
    if(key == 1){return false}
    key = validarForm_pnatural('id_parroquia', mens[8],8)//
    if(key == 1){return false}
    key = validarForm_pnatural('direccion', mens[9],9)//
    if(key == 1){return false}
    key = validarForm_pnatural('telefono_local', mens[10],10)//
    if(key == 1){return false}
    key = validarForm_pnatural('telefono_movil', mens[11],11)//
    if(key == 1){return false}

    key = validarRadio('produc_tecno', mens[12],12)//
    if(key == 1){return false}


if ($("#prod_tecnologia_si").is(':checked')){

    key = validarForm_pnatural('especifique_tecno', mens[27],27)
    if(key == 1){return false}
}



    if (key==0) {

      document.forms["FormDatosEmpresaEdit"].submit();


    }

}


/*******************Validacion Formulario DVD********************/

function enviardvd() {

var mens=[

    'Estimado usuario. Debe Seleccionar Usted ha realizo con éxito su exportación? para completar el registro',
    'Estimado usuario. Debe Seleccionar el Operador Cambiario para completar el registro',
    'Estimado usuario. Debe ingresar la Fecha Disponibilidad de las Divisas completar el registro',
    'Estimado usuario. Debe ingresar el Número de Numero de Factura para completar el registro',
    'Estimado usuario. Debe ingresar el Monto Percibido de la Venta para completar el registro',
    'Estimado usuario. Debe ingresar el Monto Venta BCV para completar el registro',
    'Estimado usuario. Debe ingresar el Monto a retener por el usuario según el Monto percibido de la venta para completar el registro',
    'Estimado usuario. Debe ingresar la Observaciones para completar el registro',
    'Estimado usuario. Debe ingresar Indique brevemente el por qué no se concretó la exportación para completar el registro',
    'Estimado usuario. El monto percibido de la venta no puede ser mayor al monto pendiente por vender',
]

function validarDvdNd(input, mensaje,num) {
    if ($("#"+input).val()=="") {
        swal("Por Favor!", mensaje, "warning")
        //swal(mensaje, "info");
        //alert(mensaje)
        $("#"+input).focus();
        key=1;
        $('#ok'+num).remove()
        $("#"+input).css('border', '1px solid red').after('<span id="ok'+num+'" style="color:red" class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span>');
        return 1;
    }else{
        $('#ok'+num).remove()
        $("#"+input).css('border', '1px solid green').after('<span id="ok'+num+'" style="color:green" class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span>');
        return 0;
    }
}

function validarmayor(input1, input2, mensaje,num) {
  
  var mpercibido = parseInt($("#"+input1).val());
  var pendienteventa = parseInt($("#"+input2).val());
  
    if (mpercibido > pendienteventa) {
      //alert($("#"+input1).val()+'>'+$("#"+input2).val());
        swal("Por Favor!", mensaje, "warning")
        //swal(mensaje, "info");
        //alert(mensaje)
        $("#"+input1).focus();
        key=1;
        $('#ok'+num).remove()
        $("#"+input1).css('border', '1px solid red').after('<span id="ok'+num+'" style="color:red" class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span>');
        return 1;
    }else{
        $('#ok'+num).remove()
        $("#"+input1).css('border', '1px solid green').after('<span id="ok'+num+'" style="color:green" class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span>');
        return 0;
    }
}

    var key=0;
    $("#formventasolicitud").submit(function() {

    /*key = validarDvd('realizoventa', mens[0],0)
    if(key == 1){return false}*/

    if ($("#accion_si").is(':checked')){

    key = validarDvd('gen_operador_cambiario_id', mens[1],1)
    if(key == 1){return false}
      key = validarDvd('fdisponibilidad', mens[2],2)
    if(key == 1){return false}
      key = validarDvd('gen_factura_id', mens[3],3)
    if(key == 1){return false}
      key = validarDvd('mpercibido', mens[4],4)
    if(key == 1){return false}
       key = validarmayor('mpercibido','pendienteventa', mens[9],4)
    if(key == 1){return false}
      key = validarDvd('mvendido', mens[5],5)
    if(key == 1){return false}
      key = validarDvd('mretencion', mens[6],6)
    if(key == 1){return false}
      key = validarDvd('descripcion', mens[7],7)
    if(key == 1){return false}

}


   if ($("#accion_no").is(':checked')){

      key = validarDvd('descripciondes', mens[8],8)
    if(key == 1){return false}

}


    if (key==0) {
        return true;
    }


});
}


function verificarFacturas_old(){

      $.ajax({

       'type':'GET',
       'url':'verificarFac',
       success: function(data){

         if(data==1){

        swal("Acción Denegada","Debe tener facturas registradas para poder realizar una solicitud de tipo ER.", "warning");

         }else{

         location.href="SolicitudER/create";

          }
      }
   });
}

/****************************************Enviar dvd nota debito************************************/
function enviardvdnd() {

var mens=[

    'Estimado usuario. Debe Seleccionar Usted ha realizo con éxito su exportación? para completar el registro',
    'Estimado usuario. Debe Seleccionar el Operador Cambiario para completar el registro',
    'Estimado usuario. Debe ingresar la Fecha Disponibilidad de las Divisas completar el registro',
    'Estimado usuario. Debe ingresar el Número de Numero de Factura para completar el registro',
    'Estimado usuario. Debe ingresar el Monto Percibido de la Venta para completar el registro',
    'Estimado usuario. Debe ingresar el Monto Venta BCV para completar el registro',
    'Estimado usuario. Debe ingresar el Monto a retener por el usuario según el Monto percibido de la venta para completar el registro',
    'Estimado usuario. Debe ingresar la Observaciones para completar el registro',
    'Estimado usuario. Debe ingresar Indique brevemente el por qué no se concretó la exportación para completar el registro',
    'Estimado usuario. El monto percibido de la venta no puede ser mayor al monto pendiente por vender',

]

function validarDvdNd(input, mensaje,num) {
    if ($("#"+input).val()=="") {
        swal("Por Favor!", mensaje, "warning")
        //swal(mensaje, "info");
        //alert(mensaje)
        $("#"+input).focus();
        key=1;
        $('#ok'+num).remove()
        $("#"+input).css('border', '1px solid red').after('<span id="ok'+num+'" style="color:red" class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span>');
        return 1;
    }else{
        $('#ok'+num).remove()
        $("#"+input).css('border', '1px solid green').after('<span id="ok'+num+'" style="color:green" class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span>');
        return 0;
    }
}

function validarmayor(input1, input2, mensaje,num) {
  
  var mpercibido = parseInt($("#"+input1).val());
  var pendienteventa = parseInt($("#"+input2).val());
  
    if (mpercibido > pendienteventa) {
      //alert($("#"+input1).val()+'>'+$("#"+input2).val());
        swal("Por Favor!", mensaje, "warning")
        //swal(mensaje, "info");
        //alert(mensaje)
        $("#"+input1).focus();
        key=1;
        $('#ok'+num).remove()
        $("#"+input1).css('border', '1px solid red').after('<span id="ok'+num+'" style="color:red" class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span>');
        return 1;
    }else{
        $('#ok'+num).remove()
        $("#"+input1).css('border', '1px solid green').after('<span id="ok'+num+'" style="color:green" class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span>');
        return 0;
    }
}

    var key=0;
    $("#formventasolicitudnd").submit(function() {

    /*key = validarDvdNd('realizoventa', mens[0],0)
    if(key == 1){return false}*/

    if ($("#accion_si").is(':checked')){

    key = validarDvdNd('gen_operador_cambiario_id', mens[1],1)
    if(key == 1){return false}
      key = validarDvdNd('fdisponibilidad', mens[2],2)
    if(key == 1){return false}
      key = validarDvdNd('gen_factura_id', mens[3],3)
    if(key == 1){return false}
      key = validarDvdNd('mpercibido', mens[4],4)
    if(key == 1){return false}
      key = validarmayor('mpercibido','pendienteventa', mens[9],4)
    if(key == 1){return false}
      key = validarDvdNd('mvendido', mens[5],5)
    if(key == 1){return false}
      key = validarDvdNd('mretencion', mens[6],6)
    if(key == 1){return false}
      key = validarDvdNd('descripcion', mens[7],7)
    if(key == 1){return false}
    

}


   if ($("#accion_no").is(':checked')){

      key = validarDvdNd('descripciondes', mens[8],8)
    if(key == 1){return false}

}


    if (key==0) {
        return true;
    }
});
}

/*****************************Validacion para mostrar rif******************************************/
function MostrarRif(){
    var cartera=$('#cat_tipo_cartera_id').val();

  //alert(cartera);

  if(cartera == 1){
    //alert(cartera);
    $('#rif').hide('slow');
    $('#pais').show('show');
    $('#convenio').show('show');

    $("#rif_empresa").attr("noreq","noreq");
    $("#pais_id").removeAttr("noreq");
    $("#cat_tipo_convenio_id").removeAttr("noreq");
  }
  if(cartera == 2){
    //alert(cartera);
    $('#rif').show('show');
    $('#pais').hide('slow');
    $('#convenio').hide('slow');

    $("#pais_id").attr("noreq","noreq");
    $("#cat_tipo_convenio_id").attr("noreq","noreq");
    $("#rif_empresa").removeAttr("noreq","noreq");
  }
}



/*****************************************************Script formulario registro de Inversion*********************************************/

$('#seguir1').click(function(event) {
    if(noseguir == 0){
        $('#section2').show(1000).animate({width: "show"}, 1000,"linear");
        $('#section1,#section3,#section4').hide(1000).animate({height: "hide"}, 1000,"linear");
    }
});

$('#seguir2').click(function(event) {
    if(noseguir == 0){
        $('#section1,#section2,#section4').hide(1000).animate({height: "hide"}, 1000,"linear");
        $('#section3').show(1000).animate({width: "show"}, 1000,"linear");
    }

});

$('#atras1').click(function(event) {
          $('#section2,#section3,#section4').hide(1000).animate({height: "hide"}, 1000,"linear");
          $('#section1').show(1000).animate({width: "show"}, 1000,"linear");
          

});

$('#seguir3').click(function(event) {
    if(noseguir == 0){
        $('#section1,#section2,#section3').hide(1000).animate({height: "hide"}, 1000,"linear");
        $('#section4').show(1000).animate({width: "show"}, 1000,"linear");
    }
});


$('#atras2').click(function(event) {
          $('#section1,#section3,#section4').hide(1000).animate({height: "hide"}, 1000,"linear");
          $('#section2').show(1000).animate({width: "show"}, 1000,"linear");
          

});

$('#atras3').click(function(event) {
          $('#section1,#section2,#section4').hide(1000).animate({height: "hide"}, 1000,"linear");
          $('#section3').show(1000).animate({width: "show"}, 1000,"linear");
          

});

$('#otro').click(function(event) {
          $('#destinver').show();
          
});

$('#empresas,#negocios,#inmuebles,#entidades,#lucro,#instrumentos,#actos,#fondo').click(function(event) {
  $('input[type="text"]').val('');
          $('#destinver').hide();
          
});


$('#rvalorsi').click(function(event) {
    if ($('#proyecnew,#ampliacion').is(':checked')) {
      $('#proyecnew,#ampliacion').removeAttr('checked');
    }
    $('#emp').show();

          
});

$('#rvalorno').click(function(event) {
   if ($('#proyecnew,#ampliacion').is(':checked')) {
      $('#proyecnew,#ampliacion').removeAttr('checked');
    }
   if ($('#particiarancel,#utilidades,#csamatriz,#terceros,#otras,#partiarancel,#bonospagares,#otro').is(':checked')) {
      $('#particiarancel,#utilidades,#csamatriz,#terceros,#otras,#partiarancel,#bonospagares,#otro').prop("checked", false);
    }
    $('input[type="text"]').val(''); 
    $('#emp1').hide();
    $('#emp').hide();
    $('#emp2').hide();
    $('#directa').hide();
    $('#cartera').hide();
          
});


$('#proyecnew').click(function(event) {
    if ($('#ied,#ic').is(':checked')) {
      $('#ied,#ic').removeAttr('checked');
    }
   $('#emp2').hide();
   $('#directa').hide();
   $('#cartera').hide();
   $('#emp1').show();
          
});

$('#ampliacion').click(function(event) { //remover el check de directa y de cartera
    $('input[type="text"]').val('');  
      $('#emp1').hide();
      $('#emp2').show();
       if ($('#ied,#ic').is(':checked')) {
      $('#ied,#ic').removeAttr('checked');
    }
          
});

$('#ied').click(function(event) {
      $('#directa').show();
       $('#cartera').hide();
          
});

$('#ic').click(function(event) {
      $('#cartera').show();
      $('#directa').hide();

          
});

$('#checkspe').click(function(event) {
      $('#spe').show();

          
});




$('#ic').click(function(event) {
      if ($('#particiarancel,#utilidades,#csamatriz,#terceros,#otras').is(':checked')) {
      $('#particiarancel,#utilidades,#csamatriz,#terceros,#otras').prop("checked", false);
    }

});


$('#ied').click(function(event) {
      if ($('#partiarancel,#bonospagares,#otro').is(':checked')) {
      $('#partiarancel,#bonospagares,#otro').prop("checked", false);
    }

});

















/************************************************************************************************************/


/***********************************************************************************************************/






/****************************Validacion de radio Empresarial Internacional********************************************/

      $('#inver1').click(function(event) {
        $('#datos_repre').show(1000).animate({width: "show"}, 1000,"linear");

      });

       $('#inver2').click(function(event) {
        $('#datos_repre').hide(1000).animate({height: "hide"}, 1000,"linear");
      });



/***********************************************************************/


      $('#nacional1').click(function(event) {
        $('#datos_emp').show(1000).animate({width: "show"}, 1000,"linear");

      });

       $('#nacional2').click(function(event) {
        $('#datos_emp').hide(1000).animate({height: "hide"}, 1000,"linear");
      });



  /***********************************************************************/


      $('#reprelegal1').click(function(event) {
        $('#representante').show(1000).animate({width: "show"}, 1000,"linear");

      });

       $('#reprelegal2').click(function(event) {
        $('#representante').hide(1000).animate({height: "hide"}, 1000,"linear");
      });






/*****************************************************Script planilla DJIR02 inversion*********************************************/
 
$('#seguir1').click(function(event) {
    if(noseguir == 0){
        $('#datos_inver2').show(1000).animate({width: "show"}, 1000,"linear");
        $('#datos_inver1,#datos_inver3').hide(1000).animate({height: "hide"}, 1000,"linear");
    }
});

$('#seguir2').click(function(event) {
    if(noseguir == 0){
        $('#datos_inver1,#datos_inver2').hide(1000).animate({height: "hide"}, 1000,"linear");
        $('#datos_inver3').show(1000).animate({width: "show"}, 1000,"linear");
    }  
});

$('#atras1').click(function(event) {
          $('#datos_inver2,#datos_inver3').hide(1000).animate({height: "hide"}, 1000,"linear");
          $('#datos_inver1').show(1000).animate({width: "show"}, 1000,"linear");
          

});

$('#seguir3').click(function(event) {
    if(noseguir == 0){
        $('#datos_inver1,#datos_inver2').hide(1000).animate({height: "hide"}, 1000,"linear");
        $('#datos_inver3').show(1000).animate({width: "show"}, 1000,"linear");
    }

});


$('#atras2').click(function(event) {
          $('#datos_inver1,#datos_inver3').hide(1000).animate({height: "hide"}, 1000,"linear");
          $('#datos_inver2').show(1000).animate({width: "show"}, 1000,"linear");
          

});


/************************************************************************************************************/
























/*****************************Validacion para USUARIO INVERSIONISTA******************************************/


/*Validacion Formulario Bandeja Inversionista*/
function enviarInversionista() {
////////////////////////////////////////////////////

  function cambiarImagenDocmultiple(event) {

    
    //console.log(event);

    var id= event.target.getAttribute('id');
    //alert(id);
     key = BandejaInversionista(id, 'Estimado usuario. El campo archivo soporte es Obligatorio',0)
      if(key == 1){return false}
  }

  $(document).on('change', '.file_multiple', function(evt) {    
      cambiarImagenDocmultiple(evt);
  });



///////////////////////////////////////////////////
 

function BandejaInversionista(input, mensaje,num) {
    if ($("#"+input).val()=="") {

        swal("Por Favor!", mensaje, "warning")
        //swal(mensaje, "info");
        //alert(mensaje)
        $("#"+input).focus();
        key=1;
        $('#ok'+num).remove()
        $("#"+input).css('border', '1px solid red').after('<span id="ok'+num+'" style="color:red" class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span>');
        return 1;
    }else{
        $('#ok'+num).remove()
        $("#"+input).css('border', '1px solid green').after('<span id="ok'+num+'" style="color:green" class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span>');
        return 0;
    }
}

  /*
  var key=0;
  $("#forInversionista").submit(function() {
    
    var tipo_usr=$("#cat_tipo_usuario_id").val();
    //alert(tipo_usr);
    if(tipo_usr == 1){
     
      key = BandejaInversionista('file_1', 'Estimado usuario. El campo archivo Recaudo 1 es Obligatorio',0)
      if(key == 1){return false}

      key = BandejaInversionista('file_5', 'Estimado usuario. El campo archivo Soporte 1 es Obligatorio',1)
      if(key == 1){return false}
      key = BandejaInversionista('file_7', 'Estimado usuario. El campo archivo Soporte 2 es Obligatorio',2)
      if(key == 1){return false}
      /*key = BandejaInversionista('file_6', 'Estimado usuario. El campo archivo Soporte 3 es Obligatorio',3)
      if(key == 1){return false}
      key = BandejaInversionista('file_7', 'Estimado usuario. El campo archivo Soporte 4 es Obligatorio',4)
      if(key == 1){return false}
      key = BandejaInversionista('file_8', 'Estimado usuario. El campo archivo Soporte 5 es Obligatorio',5)
      if(key == 1){return false}*/
  /*    key = BandejaInversionista('razon_social', 'Estimado usuario. Debe ingresar Nombre o razón social para completar el registro',3)
      if(key == 1){return false}
      key = BandejaInversionista('pais_id', 'Estimado usuario. Debe seleccionar País de Origen para completar el registro',4)
      if(key == 1){return false}
      key = BandejaInversionista('n_doc_identidad', 'Estimado usuario. Debe ingresar Número de documento de identidad para completar el registro',5)
      if(key == 1){return false}
      key = BandejaInversionista('ci_repre', 'Estimado usuario. Debe ingresar Número de pasaporte para completar el registro',6)
      if(key == 1){return false}
      key = BandejaInversionista('direccion', 'Estimado usuario. Debe ingresar Dirección de residencia para completar el registro',7)
      if(key == 1){return false}
      key = BandejaInversionista('telefono_movil', 'Estimado usuario. Debe ingresar Número telefónico celular para completar el registro',8)
      if(key == 1){return false}
          key = BandejaInversionista('telefono_local', 'Estimado usuario. Debe ingresar Número telefónico fijo para completar el registro',9)
      if(key == 1){return false}
      key = BandejaInversionista('correo', 'Estimado usuario. Debe ingresar Correo electrónico de usuario para completar el registro',10)
      if(key == 1){return false}
      key = BandejaInversionista('tipo_inversion', 'Estimado usuario. Debe ingresar Tipo de inversión para completar el registro',11)
      if(key == 1){return false}
      key = BandejaInversionista('monto_inversion', 'Estimado usuario. Debe ingresar Monto de inversión para completar el registro',12)
      if(key == 1){return false}
            key = BandejaInversionista('cat_sector_inversionista_id', 'Estimado usuario. Debe seleccionar un Sector para completar el registro',13)
      if(key == 1){return false}

      key = BandejaInversionista('actividad_eco_emp', 'Estimado usuario. Debe ingresar Actividad económica de la empresa para completar el registro',14)
      if(key == 1){return false}
      key = BandejaInversionista('descrip_proyec_inver', 'Estimado usuario. Debe ingresar Breve descripción del proyecto de inversión para completar el registro',15)
      if(key == 1){return false}
      key = BandejaInversionista('correo_emp_recep', 'Estimado usuario. Debe ingresar Correo electrónico empresa para completar el registro',16)
      if(key == 1){return false}
      key = BandejaInversionista('nombre_repre', 'Estimado usuario. Debe ingresar Nombre representante legal o apoderado para completar el registro',17)
      if(key == 1){return false}
      key = BandejaInversionista('apellido_repre', 'Estimado usuario. Debe ingresar Apellido representante legal o apoderado para completar el registro',18)
      if(key == 1){return false}
     
    }else{
      key = BandejaInversionista('file_1','Estimado usuario. El campo archivo recaudo 1 es Obligatorio',0)
      if(key == 1){return false}

      key = BandejaInversionista('file_2', 'Estimado usuario. El campo archivo recaudo 2 es Obligatorio',1)
      if(key == 1){return false}
      /*key = BandejaInversionista('file_3', 'Estimado usuario. El campo archivo recaudo 3 es Obligatorio',2)
      if(key == 1){return false}*/
    /*  key = BandejaInversionista('file_4', 'Estimado usuario. El campo archivo recaudo 4 es Obligatorio',3)
      if(key == 1){return false}
      key = BandejaInversionista('file_5', 'Estimado usuario. El campo archivo recaudo 5 es Obligatorio',4)
      if(key == 1){return false}
      key = BandejaInversionista('file_6', 'Estimado usuario. El campo archivo soporte 1 es Obligatorio',5)
      if(key == 1){return false}
      key = BandejaInversionista('file_7', 'Estimado usuario. El campo archivo soporte 1 es Obligatorio',6)
      if(key == 1){return false}
      /*key = BandejaInversionista('file_8', 'Estimado usuario. El campo archivo soporte 3 es Obligatorio',7)
      if(key == 1){return false}*/
      /*key = BandejaInversionista('file_9', 'Estimado usuario. El campo archivo soporte 4 es Obligatorio',8)
      if(key == 1){return false}*/
    /*  key = BandejaInversionista('file_10', 'Estimado usuario. El campo archivo soporte 5 es Obligatorio',9)
      if(key == 1){return false}
      key = BandejaInversionista('razon_social', 'Estimado usuario. Debe ingresar Nombre o razón social para completar el registro',10)
      if(key == 1){return false}
      key = BandejaInversionista('pais_id', 'Estimado usuario. Debe seleccionar País de Origen de la empresa para completar el registro',11)
      if(key == 1){return false}
      key = BandejaInversionista('direccion', 'Estimado usuario. Debe ingresar Dirección de domicilio en el exterior para completar el registro',12)
      if(key == 1){return false}
      key = BandejaInversionista('telefono_movil', 'Estimado usuario. Debe ingresar Número telefónico para completar el registro',13)
      if(key == 1){return false}
      key = BandejaInversionista('correo', 'Estimado usuario. Debe ingresar Correo electrónico de usuario para completar el registro',14)
      if(key == 1){return false}
      key = BandejaInversionista('tipo_inversion', 'Estimado usuario. Debe ingresar Tipo de inversión para completar el registro',15)
      if(key == 1){return false}
      key = BandejaInversionista('monto_inversion', 'Estimado usuario. Debe ingresar Monto de inversión para completar el registro',16)
      if(key == 1){return false}
         key = BandejaInversionista('cat_sector_inversionista_id', 'Estimado usuario. Debe ingresar el Sector para completar el registro',17)
      if(key == 1){return false}
      key = BandejaInversionista('actividad_eco_emp', 'Estimado usuario. Debe ingresar Actividad económica de la empresa para completar el registro',18)
      if(key == 1){return false}
      key = BandejaInversionista('descrip_proyec_inver', 'Estimado usuario. Debe ingresar Breve descripción del proyecto de inversión para completar el registro',19)
      if(key == 1){return false}
      key = BandejaInversionista('correo_emp_recep', 'Estimado usuario. Debe ingresar Correo electrónico empresa para completar el registro',20)
      if(key == 1){return false}
      key = BandejaInversionista('nombre_repre', 'Estimado usuario. Debe ingresar Nombre representante legal o apoderado para completar el registro',21)
      if(key == 1){return false}
      key = BandejaInversionista('apellido_repre', 'Estimado usuario. Debe ingresar Apellido representante legal o apoderado para completar el registro',22)
      if(key == 1){return false}
      
    }

    
    if (key==0) {
        return true;
    }
  });
  */
}
/////////////////////////////////////////////////////




/*Validacion Formulario Bandeja Inversionista*/
function enviarInversionistaEdit() {

 

  function BandejaInversionista(input, mensaje,num) {
      if ($("#"+input).val()=="") {
  
          swal("Por Favor!", mensaje, "warning")
          //swal(mensaje, "info");
          //alert(mensaje)
          $("#"+input).focus();
          key=1;
          $('#ok'+num).remove()
          $("#"+input).css('border', '1px solid red').after('<span id="ok'+num+'" style="color:red" class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span>');
          return 1;
      }else{
          $('#ok'+num).remove()
          $("#"+input).css('border', '1px solid green').after('<span id="ok'+num+'" style="color:green" class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span>');
          return 0;
      }
  }
  
      var key=0;
      $("#forInversionista").submit(function() {
      
      var tipo_usr=$("#cat_tipo_usuario_id").val();
      //alert(tipo_usr);
      if(tipo_usr == 1){
       
       /* key = BandejaInversionista('file_1', 'Estimado usuario. El campo archivo recaudo 1 es Obligatorio',0)
        if(key == 1){return false}
  
        key = BandejaInversionista('file_4', 'Estimado usuario. El campo archivo soporte 1 es Obligatorio',1)
        if(key == 1){return false}
        key = BandejaInversionista('file_5', 'Estimado usuario. El campo archenviarInversionistaivo soporte 2 es Obligatorio',2)
        if(key == 1){return false}
        key = BandejaInversionista('file_6', 'Estimado usuario. El campo archivo soporte 4 es Obligatorio',3)
        if(key == 1){return false}
        key = BandejaInversionista('file_7', 'Estimado usuario. El campo archivo soporte 5 es Obligatorio',4)
        if(key == 1){return false}
        key = BandejaInversionista('file_8', 'Estimado usuario. El campo archivo soporte 3 es Obligatorio',5)
        if(key == 1){return false}*/
        key = BandejaInversionista('razon_social', 'Estimado usuario. Debe ingresar Nombre o razón social para completar el registro',6)
        if(key == 1){return false}
        key = BandejaInversionista('pais_id', 'Estimado usuario. Debe seleccionar País de Origen para completar el registro',7)
        if(key == 1){return false}
        key = BandejaInversionista('n_doc_identidad', 'Estimado usuario. Debe ingresar Número de documento de identidad para completar el registro',8)
        if(key == 1){return false}
        key = BandejaInversionista('ci_repre', 'Estimado usuario. Debe ingresar Número de pasaporte para completar el registro',9)
        if(key == 1){return false}
        key = BandejaInversionista('direccion', 'Estimado usuario. Debe ingresar Dirección de residencia para completar el registro',10)
        if(key == 1){return false}
        key = BandejaInversionista('telefono_movil', 'Estimado usuario. Debe ingresar Número telefónico para completar el registro',11)
        if(key == 1){return false}
        key = BandejaInversionista('correo', 'Estimado usuario. Debe ingresar Correo electrónico de usuario para completar el registro',12)
        if(key == 1){return false}
        key = BandejaInversionista('tipo_inversion', 'Estimado usuario. Debe ingresar Tipo de inversión para completar el registro',13)
        if(key == 1){return false}
        key = BandejaInversionista('monto_inversion', 'Estimado usuario. Debe ingresar Monto de inversión para completar el registro',14)
        if(key == 1){return false}
        key = BandejaInversionista('actividad_eco_emp', 'Estimado usuario. Debe ingresar Actividad económica de la empresa para completar el registro',15)
        if(key == 1){return false}
        key = BandejaInversionista('descrip_proyec_inver', 'Estimado usuario. Debe ingresar Breve descripción del proyecto de inversión para completar el registro',16)
        if(key == 1){return false}
        key = BandejaInversionista('correo_emp_recep', 'Estimado usuario. Debe ingresar Correo electrónico empresa para completar el registro',17)
        if(key == 1){return false}
        key = BandejaInversionista('nombre_repre', 'Estimado usuario. Debe ingresar Nombre representante legal o apoderado para completar el registro',18)
        if(key == 1){return false}
        key = BandejaInversionista('apellido_repre', 'Estimado usuario. Debe ingresar Apellido representante legal o apoderado para completar el registro',19)
        if(key == 1){return false}
       
      }else{
        /*key = BandejaInversionista('file_1','Estimado usuario. El campo archivo recaudo 1 es Obligatorio',0)
        if(key == 1){return false}
  
        key = BandejaInversionista('file_2', 'Estimado usuario. El campo archivo recaudo 2 es Obligatorio',1)
        if(key == 1){return false}
        key = BandejaInversionista('file_3', 'Estimado usuario. El campo archivo recaudo 3 es Obligatorio',2)
        if(key == 1){return false}
        key = BandejaInversionista('file_4', 'Estimado usuario. El campo archivo recaudo 4 es Obligatorio',3)
        if(key == 1){return false}
        key = BandejaInversionista('file_5', 'Estimado usuario. El campo archivo recaudo 5 es Obligatorio',4)
        if(key == 1){return false}
        key = BandejaInversionista('file_6', 'Estimado usuario. El campo archivo soporte 1 es Obligatorio',5)
        if(key == 1){return false}
        key = BandejaInversionista('file_7', 'Estimado usuario. El campo archivo soporte 2 es Obligatorio',6)
        if(key == 1){return false}
        key = BandejaInversionista('file_8', 'Estimado usuario. El campo archivo soporte 3 es Obligatorio',7)
        if(key == 1){return false}
        key = BandejaInversionista('file_9', 'Estimado usuario. El campo archivo soporte 4 es Obligatorio',8)
        if(key == 1){return false}
        key = BandejaInversionista('file_10', 'Estimado usuario. El campo archivo soporte 5 es Obligatorio',9)
        if(key == 1){return false}*/
        key = BandejaInversionista('razon_social', 'Estimado usuario. Debe ingresar Nombre o razón social para completar el registro',10)
        if(key == 1){return false}
        key = BandejaInversionista('pais_id', 'Estimado usuario. Debe seleccionar País de Origen para completar el registro',11)
        if(key == 1){return false}
        key = BandejaInversionista('direccion', 'Estimado usuario. Debe ingresar Dirección de residencia para completar el registro',12)
        if(key == 1){return false}
        key = BandejaInversionista('telefono_movil', 'Estimado usuario. Debe ingresar Número telefónico para completar el registro',13)
        if(key == 1){return false}
        key = BandejaInversionista('correo', 'Estimado usuario. Debe ingresar Correo electrónico de usuario para completar el registro',14)
        if(key == 1){return false}
        key = BandejaInversionista('tipo_inversion', 'Estimado usuario. Debe ingresar Tipo de inversión para completar el registro',15)
        if(key == 1){return false}
        key = BandejaInversionista('monto_inversion', 'Estimado usuario. Debe ingresar Monto de inversión para completar el registro',16)
        if(key == 1){return false}
        key = BandejaInversionista('actividad_eco_emp', 'Estimado usuario. Debe ingresar Actividad económica de la empresa para completar el registro',17)
        if(key == 1){return false}
        key = BandejaInversionista('descrip_proyec_inver', 'Estimado usuario. Debe ingresar Breve descripción del proyecto de inversión para completar el registro',18)
        if(key == 1){return false}
        key = BandejaInversionista('correo_emp_recep', 'Estimado usuario. Debe ingresar Correo electrónico empresa para completar el registro',19)
        if(key == 1){return false}
        key = BandejaInversionista('nombre_repre', 'Estimado usuario. Debe ingresar Nombre representante legal o apoderado para completar el registro',20)
        if(key == 1){return false}
        key = BandejaInversionista('apellido_repre', 'Estimado usuario. Debe ingresar Apellido representante legal o apoderado para completar el registro',21)
        if(key == 1){return false}
        
      }
  
      
      if (key==0) {
          return true;
      }
  });
  
  }
  ////////////////////////////////////////////////////////////////
  //funcion para que todos los correos se guarden en minuscula
  function minuscula(cadena, selector){
    var result;
    $(selector).val(cadena.toLowerCase());
}




/******************** Validacion del Analista Inversiones mostrar campo de observacion**************************/

function validarobservacion (){

    var estatus=$('#gen_status_id').val();
    //alert(estatus);
          if(estatus == 11 || estatus== 13 || estatus==15 || estatus== 17 || estatus== 19 || estatus == 26 ) {
            
            $('#observacion').show('show');
            $('#observacion_coordinador').prop("required", true);
              //$('#observacion').hide(1000).animate({width: "hide"}, 1000,"linear");
          }else{
            $('#observacion_coordinador').prop('required',false);
            $('#observacion_coordinador').removeAttr("required");
            $('#observacion').hide('slow');

            
          }

}


/******************** Validacion del Analista Inversiones mostrar campo de observacion**************************/

function validarobservacionresguardo (){

    var estatus=$('#gen_status_id').val();
    ///alert(estatus);
          if(estatus == 11 || estatus== 15 || estatus==24) {
            
            $('#observacion').show('show');
            $('#observacion_resguardo').prop("required", true);
              //$('#observacion').hide(1000).animate({width: "hide"}, 1000,"linear");
          }else{
            $('#observacion_resguardo').prop('required',false);
            $('#observacion_resguardo').removeAttr("required");
            $('#observacion').hide('slow');

            
          }

}




/*************************** Validacion de Analista de Inversión**************************************/
function analisisinversion() {

var mens=[
    'Estimado usuario. Debe seleccionar un ESTATUS para completar la solicitud',
    'Estimado usuario. Debe ingresar una OBSERVACIÓN para completar la Solicitud',
    
]

function validaranalista (input, mensaje,num) {
    if ($("#"+input).val()=="") {

        swal("Por Favor!", mensaje, "warning")
        //swal(mensaje, "info");
        //alert(mensaje)
        $("#"+input).focus();
        key=1;
        $('#ok'+num).remove()
        $("#"+input).css('border', '1px solid red').after('<span id="ok'+num+'" style="color:red" class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span>');
        return 1;
    }else{
        $('#ok'+num).remove()
        $("#"+input).css('border', '1px solid green').after('<span id="ok'+num+'" style="color:green" class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span>');
        return 0;
    }
}
// Cambiar formato de fecha
            $('.date-exported').each(function(i, span) {
                span = $(span);
                var dateTime = span.text();
                moment.updateLocale('es', {
                    months : ['enero', 'febrero', 'marzo', 'abril', 'mayo', 'junio', 'julio', 'agosto', 'septiembre', 'octubre', 'noviembre', 'diciembre']
                });
                dateTime = moment.utc(dateTime).toDate();
                span.text(moment(dateTime).local('es').format('LLL'));
            });
            
 var key=0;
$("#formanalistainv").submit(function() {
    key =validaranalista('gen_status_id', mens[0],0)
    if(key == 1){return false}
      if ($('#gen_status_id').val() == 11  || $('#gen_status_id').val() == 13 || $('#gen_status_id').val() == 15 || $('#gen_status_id').val() == 17) {
//Validamos si el estatus esta llegando en 11, 13 y 15 muestra el campo de observacion.. 
        key =validaranalista('observacion_inversionista', mens[1],1)
        if(key == 1){return false}
      
      }
    

    if (key==0) {
        return true;
    }
  });
}


/**************//////////Planilla3///////////////////////////////////////////////////7
function enviarplanilla3() {

var mens=[
    'Estimado usuario. Debe seleccionar un ESTATUS para completar la solicitud',
    'Estimado usuario. Debe ingresar una OBSERVACIÓN para completar la Solicitud',
    
]

function validaranalistaplanilla3 (input, mensaje,num) {
    if ($("#"+input).val()=="") {

        swal("Por Favor!", mensaje, "warning")

        $("#"+input).focus();
        key=1;
        $('#ok'+num).remove()
        $("#"+input).css('border', '1px solid red').after('<span id="ok'+num+'" style="color:red" class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span>');
        return 1;
    }else{
        $('#ok'+num).remove()
        $("#"+input).css('border', '1px solid green').after('<span id="ok'+num+'" style="color:green" class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span>');
        return 0;
    }
}
 var key=0;
$("#formanalistaplanilla3").submit(function() {
    key =validaranalistaplanilla3('gen_status_id', mens[0],0)
    if(key == 1){return false}
      if ($('#gen_status_id').val() == 11  || $('#gen_status_id').val() == 13 || $('#gen_status_id').val() == 15 || $('#gen_status_id').val() == 17) {
//Validamos si el estatus esta llegando en 11, 13 y 15 muestra el campo de observacion.. 
        key =validaranalistaplanilla3('observacion_inversionista', mens[1],1)
        if(key == 1){return false}
      
      }
    

    if (key==0) {
        return true;
    }
  });
}














function enviarplanillaD2() {

var mens=[
    'Estimado usuario. Debe seleccionar un ESTATUS para completar la solicitud',
    'Estimado usuario. Debe ingresar una OBSERVACIÓN para completar la Solicitud',
    
]

function validaranalistaplanilla2 (input, mensaje,num) {
    if ($("#"+input).val()=="") {

        swal("Por Favor!", mensaje, "warning")

        $("#"+input).focus();
        key=1;
        $('#ok'+num).remove()
        $("#"+input).css('border', '1px solid red').after('<span id="ok'+num+'" style="color:red" class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span>');
        return 1;
    }else{
        $('#ok'+num).remove()
        $("#"+input).css('border', '1px solid green').after('<span id="ok'+num+'" style="color:green" class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span>');
        return 0;
    }
}
 var key=0;
$("#formanalistaplanilla2").submit(function() {
    key =validaranalistaplanilla2('gen_status_id', mens[0],0)
    if(key == 1){return false}
      if ($('#gen_status_id').val() == 11  || $('#gen_status_id').val() == 13 || $('#gen_status_id').val() == 15 || $('#gen_status_id').val() == 17) {
//Validamos si el estatus esta llegando en 11, 13 y 15 muestra el campo de observacion.. 
        key =validaranalistaplanilla2('observacion_inversionista', mens[1],1)
        if(key == 1){return false}
      
      }
    

    if (key==0) {
        return true;
    }
  });
}




/*********************************Validacion planilla DJIR02 ************************/

function enviarplanilladjir02() {

var mens=[
    'Estimado usuario. Debe seleccionar en el Paso 1 de informacion general si pertenece a un grupo empresarial Internacional',
    'Estimado usuario. Debe mencionar las empresas relacionadas',//2
    'Estimado usuario. Debe ingresar la casa matriz',//3
    'Estimado usuario. Debe ingresar las empresa representante de la casa matriz para America Latina',//4
    'Estimado usuario. Debe ingresar las empresas afiliadas al grupo empresarial Internacional',//5
    'Estimado usuario. Debe ingresar las subsidiarias de su empresa en el extranjero',//6
    'Estimado usuario. Debe seleccionar en el paso 1 de informacion general si pertenece a un grupo empresarial Nacional',//7
    'Estimado usuario. Debe ingresar las empresa holding, corporacion o consorcio',//8
    'Estimado usuario. Debe ingresar las empresas afiliadas del grupo empresarial nacional',//9
    'Estimado usuario. Debe ingresar las empresas subsidiarias locales',//10
    'Estimado usuario. Debe seleccionar en el paso 1 de informacion general si ha presentado ante el Viceministerio de Comercio exterior y promocion de inversiones',//11
    'Estimado usuario. Debe ingresar el numero de apostilla de la empresa',//12
    'Estimado usuario. Debe seleccionar la fecha',//13
    'Estimado usuario. Debe ingresar el pais de la empresa del representante',//14
    'Estimado usuario. Debe ingresar el estao o Ciudad de la empresa del representante',//15
    'Estimado usuario. Debe ingresar la autoridad competente para apostillar',//16
    'Estimado usuario. Debe indicar el cargo',//17
    'Estimado usuario. Debe indicar el traductor',//18
    'Estimado usuario. Debe indicar los datos adicionales',//19
    'Estimado usuario. Debe indicar los ingresos anuales promedio del ultimo ejercicio',//20
    'Estimado usuario. Debe indicar los egresos anuales promedio del ultimo ejercicio',//21
    'Estimado usuario. Debe ingresar el total del balance del ultimo ejercicio',//22
    'Estimado usuario. Debe seleccionar el año de declaracion jurada de la inversion realizada',//23
    'Estimado usuario. Debe ingresar la moneda extranjera de la inversion financiera',//24
    'Estimado usuario. Debe ingresar la utilidades reinvertidas de la inversion financiera',//25
    'Estimado usuario. Debe ingresar los creditos con casa matriz y/o filial extranjera de la inversion financiera',//26
    'Estimado usuario. Debe indicar las tierras y terrenos de los bienes de capital fisico o tangibles',//27
    'Estimado usuario. Debe indicar los edificios y otras construcciones de los bienes de capital fisico o tangibles',//28
    'Estimado usuario. Debe indicar las maquinarias, equipos y herramientas de los bienes de capital fisico o tangibles',//29
    'Estimado usuario. Debe indicar otros activos fijos tangibles de los bienes de capital fisico o tangibles',//30
    'Estimado usuario. Debe indicar los muebles, enseres y equipos de oficinas de los bienes de capital fisico o tangibles',//31
    'Estimado usuario. Debe indicar el software de bienes inmateriales o intangibles',//32
    'Estimado usuario. Debe indicar los derechos de propiedad intelectual de bienes inmateriales o intangibles',//33
    'Estimado usuario. Debe indicar las contribucciones tecnologicas intangibles de bienes inmateriales o intangibles',//34
    'Estimado usuario. Debe indicar otros activos fijos intangibles de bienes inmateriales o intangibles',//35
    'Estimado usuario. Debe indicar el total del detalle de la declaracion jurada de la inversion realizada',//36
    'Estimado usuario. Debe seleccionar en el paso 3 el tipo de inversion extranjera directa o de cartera',//37
    'Estimado usuario. Debe ingresar  en el paso 3 la inversion financiera en divisas y/o cualquier otro medio de cambio de la estimacion de la inversion realizada',//38
    'Estimado usuario. Debe ingresar  en el paso 3 los bienes de capital fisico o tangibles de la estimacion de la inversion realizada',//39
    'Estimado usuario. Debe ingresar en el paso 3 los bienes inmateriales o intangibles de la estimacion de la inversion realizada',//40
    'Estimado usuario. Debe ingresar en el paso 3 la reinversiones de utilidades de la estimacion de la inversion realizada',//41
    'Estimado usuario. Debe ingresar en el paso 3 otra (especifique) la estimacion de la inversion realizada',//42
    'Estimado usuario. Debe ingresar en el paso 3 el total de la estimacion de la inversion realizada',//43
    'Estimado usuario. Debe ingresar en el paso 3 el valor en libros (valor del patrimonio)',//44
    'Estimado usuario. Debe ingresar en el paso 3 el valor neto de activos',//45
    'Estimado usuario. Debe ingresar en el paso 3 el valor de una empresa similar',//46
    'Estimado usuario. Debe ingresar en el paso 3 otros (especificar) la base de estimacion',//47
    'Estimado usuario. Debe Aceptar la Declaracion Jurada de la inversion realizada para continuar con el proceso de declaracion de inversion',//48*/
]

function validarPlanilla2(input, mensaje,num) {
    if ($("#"+input).val()=="") {

        swal("Por Favor!", mensaje, "warning")
        //swal(mensaje, "info");
        //alert(mensaje)
        $("#"+input).focus();
        key=1;
        $('#ok'+num).remove()
        $("#"+input).css('border', '1px solid red').after('<span id="ok'+num+'" style="color:red" class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span>');
        return 1;
    }else{
        $('#ok'+num).remove()
        $("#"+input).css('border', '1px solid green').after('<span id="ok'+num+'" style="color:green" class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span>');
        return 0;
    }
}
 

 var key=0;
    $("#formplanillad02").submit(function() {
    key = validarPlanilla2('', mens[0],0)

    if ($("#inver1").is(':checked')){
    key = validarPlanilla2('emp_relacionadas_internacional', mens[1],1)
    if(key == 1){return false}
      key = validarPlanilla2('casa_matriz', mens[2],2)
    if(key == 1){return false}
      key = validarPlanilla2('emp_repre_casa_matriz', mens[3],3)
    if(key == 1){return false}
      key = validarPlanilla2('afiliados_internacional', mens[4],4)
    if(key == 1){return false}
       key = validarPlanilla2('sub_emp_extranjero', mens[5],5)
    if(key == 1){return false}
    
    }

    if ($("#inver2").is(':checked')){

    if (key==0) {
        return true;
    }

}


if ($("#nacional1").is(':checked')){
    key = validarPlanilla2('emp_haldig_coorp', mens[7],7)
    if(key == 1){return false}
      key = validarPlanilla2('afiliados_nacional', mens[8],8)
    if(key == 1){return false}
      key = validarPlanilla2('sub_locales', mens[9],9)
    if(key == 1){return false}


 }


 if ($("#nacional2").is(':checked')){

      if (key==0) {
        return true;
    }

  }


if ($("#reprelegal1").is(':checked')){

    key = validarPlanilla2('num_apostilla', mens[11],11)
    if(key == 1){return false}
      key = validarPlanilla2('fecha_apostilla', mens[12],12)
    if(key == 1){return false}
      key = validarPlanilla2('pais', mens[13],13)
    if(key == 1){return false}
      key = validarPlanilla2('estado', mens[14],14)
    if(key == 1){return false}
       key = validarPlanilla2('autoridad_apostilla', mens[15],15)
    if(key == 1){return false}
      key = validarPlanilla2('datos_adicionales', mens[16],16)
    if(key == 1){return false}

}

if ($("#reprelegal2").is(':checked')){

if (key==0) {
        return true;
    }

}

});
    
 }





























/*///////////////////////////////////////validacion planilla 3///////////////////////////////////////////*/

function enviarplanilla3() {

var mens=[
    //'Estimado usuario. Debe seleccionar en el paso 1 el Sector Productivo o Económico para completar el registro',
    //'Estimado usuario. Debe seleccionar en el paso 1 el Destino de la Inversión para completar el registro',
    //'Estimado usuario. Debe seleccionar si tiene o no perspectivas de realizar inversiones con recursos externos en la República Bolivariana de Venezuela para completar el registro',
    //'Estimado usuario. Debe Detalle de los recursos externos para completar el registro',
    //'Estimado usuario. Debe seleccionar si es un Proyecto nuevo o una Ampliación de su actual empresa para completar el registro',
    //'Estimado usuario. Debe ingresar el Nombre del Proyecto para completar el registro',
    //'Estimado usuario. Debe ingresar el Ubicación (Estado) para completar el registro',
    //'Estimado usuario. Debe ingresar el Origen de los Recursos (País de Origen) para completar el registro',
    //'Estimado usuario. Debe seleccionar si es una Inversión Extranjera Directa o una Inversión de Cartera para completar el registro',
    //'Estimado usuario. Debe seleccionar al menos al menos una opcion de Inversión Extranjera Directa para completar el registro',
    //'Estimado usuario. Debe seleccionar al menos al menos una opcion de Inversión de Cartera para completar el registro',
    //'Estimado usuario. Debe ingresar la fecha del Periodo previsto para efectuar su inversión para completar el registro',
    'Estimado usuario. Debe ACEPTAR LA DECLARACIÓN ANTES EXPUESTAS para completar el registro',
]

function validarPlanilla3(input, mensaje,num) {
    if ($("#"+input).val()=="") {

        swal("Por Favor!", mensaje, "warning")
        //swal(mensaje, "info");
        //alert(mensaje)
        $("#"+input).focus();
        key=1;
        $('#ok'+num).remove()
        $("#"+input).css('border', '1px solid red').after('<span id="ok'+num+'" style="color:red" class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span>');
        return 1;
    }else{
        $('#ok'+num).remove()
        $("#"+input).css('border', '1px solid green').after('<span id="ok'+num+'" style="color:green" class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span>');
        return 0;
    }
}


function validarCondicion(input,mensaje,num){

  if ( $("#"+input).is(':checked') ) {


        $('#ok'+num).remove()
        $("#"+input).css('border', '1px solid green').after('<span id="ok'+num+'" style="color:green" class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span>');
        return 0;

    }else{

        swal("Por Favor!", mensaje, "warning")
        $("#"+input).focus();
        key=1;
        $('#ok'+num).remove()
        $("#"+input).css('border', '1px solid red').after('<span id="ok'+num+'" style="color:red" class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span>');
        return 1;
    }

}

   

    k=2;

    if(k==2){

      var v=document.getElementById('guardar_personal');
       v.setAttribute('onclick','');
       v.setAttribute('data-toggle','modal');
       v.setAttribute('data-target','#planilla3');

      }


     key = validarCondicion('condicion', mens[0],0)
     if(key == 1){return false}



    if (key==0) {


      document.forms["formplanilla3"].action="/savePlanilla3Inversion";
      document.forms["formplanilla3"].submit();
    }


}


/********************validar declracion planilla 2****************/



function enviarplanilla2() {

var mens=[
    //'Estimado usuario. Debe seleccionar en el paso 1 el Sector Productivo o Económico para completar el registro',
    //'Estimado usuario. Debe seleccionar en el paso 1 el Destino de la Inversión para completar el registro',
    //'Estimado usuario. Debe seleccionar si tiene o no perspectivas de realizar inversiones con recursos externos en la República Bolivariana de Venezuela para completar el registro',
    //'Estimado usuario. Debe Detalle de los recursos externos para completar el registro',
    //'Estimado usuario. Debe seleccionar si es un Proyecto nuevo o una Ampliación de su actual empresa para completar el registro',
    //'Estimado usuario. Debe ingresar el Nombre del Proyecto para completar el registro',
    //'Estimado usuario. Debe ingresar el Ubicación (Estado) para completar el registro',
    //'Estimado usuario. Debe ingresar el Origen de los Recursos (País de Origen) para completar el registro',
    //'Estimado usuario. Debe seleccionar si es una Inversión Extranjera Directa o una Inversión de Cartera para completar el registro',
    //'Estimado usuario. Debe seleccionar al menos al menos una opcion de Inversión Extranjera Directa para completar el registro',
    //'Estimado usuario. Debe seleccionar al menos al menos una opcion de Inversión de Cartera para completar el registro',
    //'Estimado usuario. Debe ingresar la fecha del Periodo previsto para efectuar su inversión para completar el registro',
    'Estimado usuario. Debe ACEPTAR LA DECLARACIÓN ANTES EXPUESTAS para completar el registro',
]

function validarPlanilla2(input, mensaje,num) {
    if ($("#"+input).val()=="") {

        swal("Por Favor!", mensaje, "warning")
        //swal(mensaje, "info");
        //alert(mensaje)
        $("#"+input).focus();
        key=1;
        $('#ok'+num).remove()
        $("#"+input).css('border', '1px solid red').after('<span id="ok'+num+'" style="color:red" class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span>');
        return 1;
    }else{
        $('#ok'+num).remove()
        $("#"+input).css('border', '1px solid green').after('<span id="ok'+num+'" style="color:green" class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span>');
        return 0;
    }
}


function validarCondicion(input,mensaje,num){

  if ( $("#"+input).is(':checked') ) {


        $('#ok'+num).remove()
        $("#"+input).css('border', '1px solid green').after('<span id="ok'+num+'" style="color:green" class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span>');
        return 0;

    }else{

        swal("Por Favor!", mensaje, "warning")
        $("#"+input).focus();
        key=1;
        $('#ok'+num).remove()
        $("#"+input).css('border', '1px solid red').after('<span id="ok'+num+'" style="color:red" class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span>');
        return 1;
    }

}

   

    k=2;

    if(k==2){

      var v=document.getElementById('guardar_personal');
       v.setAttribute('onclick','');
       v.setAttribute('data-toggle','modal');
       v.setAttribute('data-target','#planilla2');

      }


     key = validarCondicion('condicion', mens[0],0)
     if(key == 1){return false}



    if (key==0) {


      document.forms["formplanilla2"].action="/savePlanilla2Inversion";
      document.forms["formplanilla2"].submit();
    }


}



/**********************validar declracion de planilla 1***************/


function enviarplanilla1() {

var mens=[
    //'Estimado usuario. Debe seleccionar en el paso 1 el Sector Productivo o Económico para completar el registro',
    //'Estimado usuario. Debe seleccionar en el paso 1 el Destino de la Inversión para completar el registro',
    //'Estimado usuario. Debe seleccionar si tiene o no perspectivas de realizar inversiones con recursos externos en la República Bolivariana de Venezuela para completar el registro',
    //'Estimado usuario. Debe Detalle de los recursos externos para completar el registro',
    //'Estimado usuario. Debe seleccionar si es un Proyecto nuevo o una Ampliación de su actual empresa para completar el registro',
    //'Estimado usuario. Debe ingresar el Nombre del Proyecto para completar el registro',
    //'Estimado usuario. Debe ingresar el Ubicación (Estado) para completar el registro',
    //'Estimado usuario. Debe ingresar el Origen de los Recursos (País de Origen) para completar el registro',
    //'Estimado usuario. Debe seleccionar si es una Inversión Extranjera Directa o una Inversión de Cartera para completar el registro',
    //'Estimado usuario. Debe seleccionar al menos al menos una opcion de Inversión Extranjera Directa para completar el registro',
    //'Estimado usuario. Debe seleccionar al menos al menos una opcion de Inversión de Cartera para completar el registro',
    //'Estimado usuario. Debe ingresar la fecha del Periodo previsto para efectuar su inversión para completar el registro',
    'Estimado usuario. Debe ACEPTAR LA DECLARACIÓN ANTES EXPUESTAS para completar el registro',
]

function validarPlanilla1(input, mensaje,num) {
    if ($("#"+input).val()=="") {

        swal("Por Favor!", mensaje, "warning")
        //swal(mensaje, "info");
        //alert(mensaje)
        $("#"+input).focus();
        key=1;
        $('#ok'+num).remove()
        $("#"+input).css('border', '1px solid red').after('<span id="ok'+num+'" style="color:red" class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span>');
        return 1;
    }else{
        $('#ok'+num).remove()
        $("#"+input).css('border', '1px solid green').after('<span id="ok'+num+'" style="color:green" class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span>');
        return 0;
    }
}


function validarCondicion(input,mensaje,num){

  if ( $("#"+input).is(':checked') ) {


        $('#ok'+num).remove()
        $("#"+input).css('border', '1px solid green').after('<span id="ok'+num+'" style="color:green" class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span>');
        return 0;

    }else{

        swal("Por Favor!", mensaje, "warning")
        $("#"+input).focus();
        key=1;
        $('#ok'+num).remove()
        $("#"+input).css('border', '1px solid red').after('<span id="ok'+num+'" style="color:red" class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span>');
        return 1;
    }

}

   

    k=2;

    if(k==2){

      var v=document.getElementById('guardar_personal');
       v.setAttribute('onclick','');
       v.setAttribute('data-toggle','modal');
       v.setAttribute('data-target','#planilla1');

      }


     key = validarCondicion('condicion', mens[0],0)
     if(key == 1){return false}



    if (key==0) {


      document.forms["formregisinversion"].action="/savePlanilla1Inversion";
      document.forms["formregisinversion"].submit();
    }


}



