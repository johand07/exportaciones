<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNumSolToCertExportaFacil extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cert_exporta_facil', function (Blueprint $table) {
            
            $table->string('num_sol_cert', 255)->nullable()->after('gen_usuario_id');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cert_exporta_facil', function (Blueprint $table) {
            //
            $table->dropColumn('num_sol_cert');
            
        });
    }
}
