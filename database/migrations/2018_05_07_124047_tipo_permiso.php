<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TipoPermiso extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tipo_permiso', function (Blueprint $table) {
           $table->engine = 'InnoDB';
           $table->increments('id');
           $table->string('nombre_permiso', 50)->nullable();
           $table->string('descrip_permiso', 100)->nullable();
           $table->boolean('bactivo')->default(1);
           $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::dropIfExists('tipo_permiso');
    }
}
