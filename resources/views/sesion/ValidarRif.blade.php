@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2"><br><br>
            <div class="panel panel-primary">
                <div class="panel-heading"><span><i class="glyphicon glyphicon-user"></i></span> <bold>Registro</bold></div>
                @php
                    @$eventbrowser= strpos($GLOBALS["HTTP_USER_AGENT"],"MSIE")==0 ?"onkeypress":"onkeydown";
                @endphp

                <div class="panel-body">
                    @include("flash::message")
                    <form class="form-horizontal" method="POST" action="/RifEmp" id="formLogin">
                        {{ csrf_field() }}

                        <div class="form-group" >

                       <label for="rif" class="col-md-4 control-label">Tipo de Usuario </label>

                       {!! Form::radio('tipoUsu', '1',false,['class'=>'radio-inline','id'=>'tipoUsu']) !!}
                       {!! Form::label('Natural', 'Natural',['class'=>'radio-inline']) !!}
                       {!! Form::radio('tipoUsu', '2',false,['class'=>'radio-inline','id'=>'tipoUsu']) !!}
                       {!! Form::label('Jurídico', 'Jurídico',['class'=>'radio-inline']) !!}
                        </div>

                        <div class="form-group{{ $errors->has('rif') ? ' has-error' : '' }}">

                          <label for="rif" class="col-md-4 control-label">Ingrese el RIF </label>

                        <div class="col-md-8">

                            <table>
                              <tr>
                                <td>{{--<select id="select_tipoPersona" name="select_tipoPersona" class="form-control">
                                    <option value=""></option>
                                    <option value="V">V</option>
                                    <option value="E">E</option>
                                    <option value="J">J</option>
                                    <option value="P">P</option>
                                    <option value="G">G</option>
                                  </select>--}}
                               {!!Form::select('select_tipoPersona',array('V'=>'V', 'E' => 'E','J'=>'J','P'=>'P','G'=>'G'),null,['class'=>'form-control','id'=>'select_tipoPersona','placeholder'=>'', 'onchange="validarPasaporte()"'])!!}
                              </td>

                                      <td>
                                       {!!Form::text('rif',null,['class'=>'form-control','placeholder'=>'J-00000001, V-00000001','onkeypress="return soloNumeros(event)"','maxlength'=>'9','id'=>'rif'])!!}
                                    </td></tr>

                            </table>


                                <span class="help-block">Ejemplo: V-00000001 , J-00000001, G-00000001</span>
                                @if ($errors->has('rif'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('rif') }}</strong>
                                    </span>
                                @endif
                              </div>
                          </div>

                        <div class="form-group{{ $errors->has('captcha') ? ' has-error' : '' }}">
                            <label for="captcha" class="col-md-4 control-label">Captcha</label>

                            <div class="col-md-6">
                                <div class="captcha">
                                    <span>{!! captcha_img('flat')!!}</span>
                                    <button type="button" class="btn btn-primary btn-refresh">Cambiar</button>
                                </div>
                                <br>
                                <div class="input-group">
                                    <input id="captcha" type="text" class="form-control" name="captcha" placeholder="Ingrese Caracateres de la Imagen">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
                                </div>

                               <!--<div class="g-recaptcha" data-sitekey="6LfdaFcUAAAAAIk5MtzzcVAQiUoYL7pEC7JByecA"></div>-->

                               
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary btn-lg btn-block" onclick="validacionForm()">
                                    Enviar
                                </button>

                                <!--<a class="btn btn-link" href="{{ url('/passwordReset') }}">
                                    Olvidaste tu contraseña?
                                </a>-->
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@include('templates/footer')


<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
  
  <script>
    //Validación de formulario, se puede llamar y pasar la sección que contenga los inputs a validar
    $(document).ready(function ()
    {
                var error = '<div class="msg_alert"><span style="color:red" class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span><strong id="cod_aran-error" class="added"> El campo es obligatorio.</strong></div>';
                var valido = '<div class="msg_alert"><span style="color:green" class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span></div>';
                var mens=['Estimado Usuario. Debe completar los campos solicitados']
    
                  validacionForm = function() {
    
                  $('#formLogin').submit(function(event) {
                  
                  var campos = $('#formLogin').find('input:text, select, input:password');
                  var n = campos.length;
                  var err = 0;
    
                  $("div").remove(".msg_alert");
                  //bucle que recorre todos los elementos del formulario
                  for (var i = 0; i < n; i++) {
                      var cod_input = $('#formLogin').find('input:text, select, input:password').eq(i);
                      if (!cod_input.attr('noreq')) {
                        if (cod_input.val() == '' || cod_input.val() == null)
                        {
                          err = 1;
                          cod_input.css('border', '1px solid red').after(error);
                        }
                        else{
                          if (err == 1) {err = 1;}else{err = 0;}
                          cod_input.css('border', '1px solid green').after(valido);
                        }
                        
                      }
                  }
  
                    //Si hay errores se detendrá el submit del formulario y devolverá una alerta
                    if(err==1){
                        event.preventDefault();
                        swal("Por Favor!", mens[0], "warning")
                    }

                  });
                }
              });
  </script>

@endsection
