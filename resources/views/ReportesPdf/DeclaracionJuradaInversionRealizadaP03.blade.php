<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Planilla DJIR03</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
        <style type="text/css">
        .centrar{
            text-align: center;
        }

        .titulo{
            
            font-weight: bold;
        }

        .nro-correlativo{
            color: red;
            font-size: 16px;
        }

        .derecha{
            text-align: left;

        }

        .text-normal{
            font-size: 14px;
        }

        .text-medio{
            font-size: 12px;
        }
        .text-11{
            font-size: 11px;
        }
        .text-small{
            font-size: 10px;
        }

        .text-9{
            font-size: 9px;
        }

        .text-8{
            font-size: 8px;
        }

        .espacio-superior{
            margin-top: 5px;
        }

        .espacio-superior-small{
            margin-top: 5px;
        }

        .ancho-medio{
            width: 50%;
        }

        .ancho-full{
            width: 100%;
        }

        .ancho-medio-alto{
            width: 75%;
        }

        .ancho-90{
            width: 90%;
        }

        .table{
            border-collapse: collapse;
        }
        
        .borde{
            border: 1px solid #fff;
        }

        .sin-borde{
            border: none;
        }

        .top{
            vertical-align: top;
        }

        .under-dotted{    
            border-bottom: 1px dotted #000;
            text-decoration: none;
        }

        .content-1{
            width: 5%;
        }

        .content-9{
            width: 20%;
        }

        .sangria-1{
            margin-left: 28px;
        }

        .sangria-2{
            margin-left: 36px;
        }

        .sangria-3{
            margin-left: 54px;
        }

        .sangria-sub{
            margin-left: 14px;
            
        }

        
        .page-break { page-break-before: always; }

        .table-center{
            margin: 0 auto; 
        }

        .justify{
            text-align: justify;
        }

        td.justify {
            padding-left: 8px;
            padding-right: 8px; 
        }
        .lineal{
            
        }
        .lineal>div{
            max-width:91%;

        }
        .lineal>*{
           display: inline-block;
           vertical-align:top;
        }

        .sangria-lista{
            list-style-type: none;
            margin: 0;
            padding: 0;
            margin-bottom: 4px;
        }

        .sangria-lista>li{
            margin-top: 5px;
            padding-left: 10px;
            padding-right: 10px;
        }
    </style>
</head>
<body>
      <div class="centrar espacio-superior">
        <table style="width:100%;" class="table2">
          <tbody>
            <tr>
              <td class="td3">
                <img class="img-responsive" style="width:200px; height: 40px;" src='img/MIPPCOEXIN.png'>
              </td>
           <td class="nro-correlativo" style="width: 30%; font-size:80%; text-align: left;">
            <td class="nro-correlativo" style="width: 30%; font-size:80%; text-align: left;">
               <td>
                  <img class="img-responsive borde" style="width: 300px; height: 40px; text-align: right;" src='img/cintillo.png'>
              </td>
            </tr>
          </tbody>
        </table>
    </div><br><br><br>

    <div class="centrar espacio-superior">
        <table class="ancho-full">
            <tr>
                <td class="center" style="width: 70%; font-size:107%; text-align: center;">
                    PLANILLA (DJIR 03)
                </td>
            </tr>
            <tr>
                <td class="center" style="width: 70%; font-size:16px; text-align: center;">
                     <b>PLANILLA DE DECLARACIÓN JURADA DE INVERSION REALIZADA (03)<br> INFORMACIÓN COMPLEMENTARIA</b>
                </td>
            </tr>
        </table>
    </div>

    <div class="centrar espacio-superior">
        <table class="ancho-full">
            <tr>
            	<td class="nro-correlativo" style="width: 30%; font-size:80%; text-align: left;">
                     <b>Tipo de Persona:</b><b style="color: black">{{ $solicitudDeclaracion->CatTipoUsuario->nombre}}</b>
                </td>
                
                <td class="nro-correlativo" style="width: 30%; font-size:80%; text-align: right;"></td>
                <td class="nro-correlativo" style="width: 30%; font-size:80%; text-align: right;">
                     <b>Fecha Solicitud:</b><b style="color: black">{{$solicitudDeclaracion->created_at}}
                </td>

            </tr>
            <tr>
            	<td class="nro-correlativo" style="width: 30%; font-size:80%; text-align: right;"></td>
                <td class="nro-correlativo" style="width: 30%; font-size:80%; text-align: right;"></td>
                <td class="nro-correlativo" style="width: 30%; font-size:80%; text-align: right;">
                     <b>Nº Solicitud:</b><b style="color: black">{{$solicitudDeclaracion->num_declaracion}}</b>
                </td>

            </tr>
        </table>
    </div>

	<div class="centrar espacio-superior">
	    <table class="ancho-full" border="1">
	        <tr>
	            <td class="top" style="text-align: left; font-size: 13px" colspan="50"><b>Sector Productivo o Económico</b><br><br>

		            @foreach($gen_sector_productivo_djir03 as $sector)
		                <ul>
		                  <li>{{$sector->sectorProdEco ? $sector->sectorProdEco->nombre_sector:''}}</li>
		                </ul>        
		            @endforeach
	            </td>

	            <td class="top" style="text-align: left; font-size: 13px" colspan="50"><b>Destino de la Inversión</b><br>
		          @foreach($gen_destino_inversion_djir03 as $destino)
		            <ul>
		              <li>{{$destino->catDestInversion ? $destino->catDestInversion->nombre_destino:''}}</li>
		            </ul>        
		          @endforeach
	            <br></td>
	        </tr>
	    </table>
	</div>

	<div class="centrar espacio-superior">
	    <table class="ancho-full" border="1">
	        <tr>
	            <td class="top" style="text-align: left; font-size: 13px" colspan="50">
	            	Tiene perspectivas de realizar inversiones con recursos externos en la República Bolivariana de Venezuela: <b>{{$planillaDjir03->inversion_recursos_externos ? $planillaDjir03->inversion_recursos_externos:''}}</b><br>

           @if(!empty($planillaDjir03->inversion_recursos_externos == 'Si'))

              @if($planillaDjir03->inversion_recursos_externos == 'Si' && $planillaDjir03->proyecto_nuevo == 'Si' )
                Aspectos: <b>Proyecto nuevo (Otra empresa)</b><br>
                @elseif($planillaDjir03->inversion_recursos_externos == 'Si' && $planillaDjir03->proyecto_nuevo == 'No' )
                 Aspectos: <b>Ampliación de su actual empresa</b><br> 
              @endif

              @if($planillaDjir03->proyecto_nuevo == 'Si')
                Nombre del Proyecto: <b>{{$planillaDjir03->nombre_proyecto ? $planillaDjir03->nombre_proyecto:''}}</b><br>
                Ubicación (Estado): <b>{{$planillaDjir03->ubicacion ? $planillaDjir03->ubicacion:''}}</b><br>
                Origen de los Recursos (País de Origen): <b>{{$planillaDjir03->pais ? $planillaDjir03->pais->dpais:''}}</b>
              <br>
              @elseif($planillaDjir03->proyecto_nuevo == 'No')
                Bajo que Modalidad Prevé sus Inversiones en el Futuro: <b>{{$planillaDjir03->ampliacion_actual_emp ? $planillaDjir03->ampliacion_actual_emp:''}}</b><br>

                @if($planillaDjir03->ampliacion_actual_emp == 'Inversión Extranjera Directa')
                  <ul>
                    @if(!empty($planillaDjir03->ampliacion_accionaria_dir))
                    <li>{{$planillaDjir03->ampliacion_accionaria_dir ? $planillaDjir03->ampliacion_accionaria_dir:''}}</li>
                    @endif

                    @if(!empty($planillaDjir03->utilidad_reinvertida))
                    <li>{{$planillaDjir03->utilidad_reinvertida ? $planillaDjir03->utilidad_reinvertida:''}}</li>
                    @endif

                    @if(!empty($planillaDjir03->credito_casa_matriz))
                    <li>{{$planillaDjir03->credito_casa_matriz ? $planillaDjir03->credito_casa_matriz:''}}</li>
                    @endif

                    @if(!empty($planillaDjir03->creditos_terceros))
                    <li>{{$planillaDjir03->creditos_terceros ? $planillaDjir03->creditos_terceros:''}}</li>
                    @endif

                    @if(!empty($planillaDjir03->otra_dir))
                    <li>{{$planillaDjir03->otra_dir ? $planillaDjir03->otra_dir:''}}</li>
                    @endif
                  </ul>  

                @elseif($planillaDjir03->ampliacion_actual_emp == 'Inversión de Cartera')
                  <ul>
                    @if(!empty($planillaDjir03->participacion_accionaria_cart))
                    <li>{{$planillaDjir03->participacion_accionaria_cart ? $planillaDjir03->participacion_accionaria_cart:''}}</li>
                    @endif

                    @if(!empty($planillaDjir03->bonos_pagare))
                    <li>{{$planillaDjir03->bonos_pagare ? $planillaDjir03->bonos_pagare:''}}</li>
                    @endif

                    @if(!empty($planillaDjir03->otra_cart))
                    <li>{{$planillaDjir03->otra_cart ? $planillaDjir03->otra_cart:''}}</li>
                    @endif
                  </ul> 
                @endif
              @endif
          @elseif(!empty($planillaDjir03->inversion_recursos_externos == 'No'))
           {{$planillaDjir03->det_invert_recursos ? $planillaDjir03->det_invert_recursos:''}}
           @endif
	            </td>

	            
	        </tr>
	    </table>
	</div>

	<div class="centrar espacio-superior">
	    <table class="ancho-full" border="1">
	        <tr>
	            <td class="top" style="text-align: left; font-size: 13px" colspan="50"><b>Periodo previsto para efectuar su inversión: </b>
	            	{{$planillaDjir03->periodo_inversion ? $planillaDjir03->periodo_inversion:''}}
	            </td>
	        </tr>
	    </table>
	</div><br><br>

	<div class="centrar espacio-superior">
	    <table class="ancho-full">
	        <tr>
	            <td class="top" style="text-align: justify; font-size: 13px; text-transform: uppercase" colspan="100"><b>Declaro que los datos contenidos en esta declaración ha sido determinados a base en las disposiciones legales y reglamentarias correspondientes. <br> La información aquí declarada es entregada con carácter confidencial en los días del mes.</b><br><br><br><br>
	        </td>
	        </tr>
	    </table>
	</div>

<br>
        <div class="centrar espacio-superior">
        <table class="ancho-full">
            <tr>
                <td class="top" style="text-align: center; font-size: 13px; text-transform: uppercase" colspan="100">
                Representante legal n° de rif: <b>{{ $solicitudDeclaracion->rPlanillaDjir01 ? $solicitudDeclaracion->rPlanillaDjir01->numero_doc_identidad:''}}</b><br>
                Firma del contribuyente o Representante Legal <br><br><br>



                _______________________________________________________
            </td>
            </tr>
        </table>
    </div>















</body>
</html>