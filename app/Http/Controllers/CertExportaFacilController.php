<?php

namespace App\Http\Controllers;

use App\Models\GenUsuario;
use App\Models\CertExportaFacil;
use App\Models\DescripcionMercancia;
use App\Models\DocExtra;
use App\Models\Pais;
use Illuminate\Http\Request;
use Auth;
use Session;
use Laracasth\Flash\Flash;
use Alert;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class CertExportaFacilController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $titulo= 'Exporta Facil';
        $descripcion= 'Perfil del Exportador';

        $solicitudes = CertExportaFacil::where('bactivo',1)->where('gen_usuario_id', Auth::user()->id)->get();


       return view('CertExportaFacil.index',compact('titulo','descripcion','solicitudes'));
    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        $titulo= 'Certificado Exporta Fácil';
        $descripcion= 'Peril del Exportador';/* nsantos*/
        $tipo_meracncia =[
         'Perecedero'=>'Perecedero',
         'No perecedero'=>'No perecedero'
        ];
        $pais=Pais::where('bactivo',1)->pluck('dpais','id');
         return view('CertExportaFacil.create',compact('titulo','descripcion','pais','tipo_meracncia'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       /*Validaciones tipo request "es donde llega la petición" lado servidor*/
        $this->validate($request,[
    
         'codigo'=>'required',
         'regimen_one'=>'required',
         'fecha_reg'=>'required',
         'nombre_repre_export'=>'required',
         'telefono_exportador'=>'required',
         'correo'=>'required',
         'direccion_emisor'=>'required',
         'rud'=>'required',
         'pais_origen'=>'required',
         'cod_postal'=>'required',
         'ciudad'=>'required',
         'fecha_embarque'=>'required',
         'destinario'=>'required',
         'telefono_destinario'=>'required',
         'pais_id'=>'required',
         'cod_postal_destino'=>'required',
         'ciudad_destino'=>'required',
         'tipo_meracncia'=>'required',
         'total_fob'=>'required',
         'peso_estimado'=>'required',
         'alto'=>'required',
         'ancho'=>'required',
         'total_bultos'=>'required',
         'tipo_servicio'=>'required',
         'valor_total_flete_sinIva'=>'required',
         'valor_total_flete_conIva'=>'required',
         'total_seguro'=>'required'

 
         
       ]);

        /*Insert para Exporta facil*/

       // dd($this->generarRamdom());

       $sol = new CertExportaFacil;
       $sol->gen_usuario_id = Auth::user()->id;
       $sol->num_sol_cert = $this->generarRamdom();
       $sol->codigo                             = $request->codigo;
       $sol->regimen                            = $request->regimen_one;
       $sol->fecha                              = $request->fecha_reg;
       $sol->nombre_repre_export                = $request->nombre_repre_export;
       $sol->telefono_exportador                = $request->telefono_exportador;
       $sol->correo                             = $request->correo;
       $sol->direccion_emisor                   = $request->direccion_emisor;
       $sol->rud                                = $request->rud;
       $sol->pais_origen                        = $request->pais_origen;
       $sol->cod_postal                         = $request->cod_postal;
       $sol->ciudad                             = $request->ciudad;
       $sol->fecha_embarque                     = $request->fecha_embarque;
       $sol->destinario                         = $request->destinario;
       $sol->telefono_destinario                = $request->telefono_destinario;
       $sol->direccion_receptor                 = $request->direccion_receptor;
       $sol->pais_id                            = $request->pais_id;
       $sol->cod_postal_destino                 = $request->cod_postal_destino;
       $sol->ciudad_destino                     = $request->ciudad_destino;
       $sol->tipo_meracncia                     = $request->tipo_meracncia;
       $sol->total_fob                          = $request->total_fob;
       $sol->peso_estimado                      = $request->peso_estimado;
       $sol->alto                               = $request->alto;
       $sol->ancho                              = $request->ancho;
       $sol->tratamiento_especial               = $request->tratamiento_especial;
       $sol->total_bultos                       = $request->total_bultos;
       $sol->tipo_servicio                      = $request->tipo_servicio;
       $sol->valor_total_flete_sinIva           = $request->valor_total_flete_sinIva;
       $sol->valor_total_flete_conIva           = $request->valor_total_flete_conIva;
       $sol->total_seguro           = $request->total_seguro;
       $sol->save();
       

       for($i=0; $i <= count($request->serie)-1; $i++){
            $descrip = DescripcionMercancia::create([
                'cert_exporta_facil_id'     => $sol->id,
                'serie'                     => $request->serie[$i],
                'sub_partida_arancel'       => $request->sub_partida_arancel[$i],
                'descripcion'               => $request->descripcion[$i],
                'valor_fob'                 => $request->valor_fob[$i],
                'cantidad'                  => $request->cantidad[$i],
                'fecha'                     => $request->fecha[$i],
                'regimen'                   => $request->regimen[$i],
            ]);
       }

       for($i=0; $i <= 3; $i++){
            DocExtra::create([
                'cert_exporta_facil_id' => $sol->id,
                'cat_descrip_doc_id'    => $request->cat_descrip_doc_id[$i],
                'numero_documento'      => $request->numero_documento[$i],
                'entidad_emisora'       => $request->entidad_emisora[$i],
                'fecha'                 => $request->fecha_doc[$i],
            ]);
        } 

       Alert::success('REGISTRO EXITOSO','Solicitud registrada')->persistent("Ok");
        return redirect()->action('CertExportaFacilController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\GenUsuario  $genUsuario
     * @return \Illuminate\Http\Response
     */
    public function show(GenUsuario $genUsuario)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\GenUsuario  $genUsuario
     * @return \Illuminate\Http\Response
     */
    public function edit(GenUsuario $genUsuario, $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\GenUsuario  $genUsuario
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, GenUsuario $genUsuario)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\GenUsuario  $genUsuario
     * @return \Illuminate\Http\Response
     */
    public function destroy(GenUsuario $genUsuario)
    {
        //
    }
    private function generarRamdom()
    {
        $user = Auth::user();
        $random = Str::random(5);
        $id  = $user->id;
        $rif = substr($user->detUsuario->rif, -2);
        $fecha = date('Ymd');
        return $id.'-'.'EPF'.$random.$fecha.$rif;
    }
}
