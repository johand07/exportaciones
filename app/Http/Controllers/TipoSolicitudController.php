<?php

namespace App\Http\Controllers;
use App\Models\GenSolicitud;
use App\Models\TipoSolicitud;
use App\Http\Requests\GenSolicitudRequest;
use App\Models\GenConsignatario;
use App\Models\ConsigTecno;
use App\Models\DetUsuario;
use App\Models\GenFactura;
use Illuminate\Support\Facades\Session;
use Auth;
use Laracasts\Flash\Flash;
use Alert;
use Illuminate\Http\Request;

class TipoSolicitudController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        
        $titulo="Solicitudes de Exportación Realizada";
        $descripcion="Solicitudes Registradas";
        $solicitudes=GenSolicitud::where('gen_usuario_id',Auth::user()->id)->where('bactivo',1)->get();

               
        return view('SolicitudER.index',compact('solicitudes','titulo','descripcion'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

     
       $arx=[];
       $asoli=[];
       $data=GenFactura::where('gen_usuario_id',Auth::user()->id)->get();
       $soli=GenSolicitud::where('gen_usuario_id',Auth::user()->id)->get();
        foreach ($data as $key => $value) {

           if($value->bactivo!==0){
             $arx[$key]=0;
             break;
            }
    
         }

       foreach ($soli as $key => $value) {
         if($value->monto_solicitud==null){

            $asoli[$key]=1;
            break;
           }

       }

      

         if($arx==null){

         Alert::warning('Debe tener facturas registradas para poder realizar una solicitud de tipo ER.','Acción Denegada!')->persistent("OK");

         return redirect()->action('TipoSolicitudController@index');

           }else{

 
             if($asoli==null){

             $titulo="Nueva Solicitud Exportación Realizada";
                     $descripcion="Registro de Solicitud";
                     $fecha=date('Y-m-d');
                     $consig=GenConsignatario::where('gen_usuario_id',Auth::user()->id)->pluck('nombre_consignatario','id');
                     $tipoExpor=DetUsuario::where('gen_usuario_id',Auth::user()->id)->first();
                     ($tipoExpor->produc_tecno==1) ? $num=2 : $num=1;
                     $tiposolic=TipoSolicitud::whereIn('id',[$num])->pluck('solicitud','id');
                     return view('SolicitudER.create',compact('tiposolic','titulo','descripcion','fecha','consig','num'));
            }else{
    
         Alert::warning('Existe una solicitud incompleta. 
         Finalice ese proceso para poder realizar una nueva solicitud de tipo ER.','Acción Denegada!')->persistent("OK");

         
       

         return redirect()->action('TipoSolicitudController@index');

           
           }
           
       }


    }





    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(GenSolicitudRequest $request)
    {

        /*ramdom*/

    function generarRandom($longitud) {
        $key = '';
        $pattern = '1234567890abcdefghijklmnopqrstuvwxyz';
        $max = strlen($pattern)-1;
        for($i=0;$i < $longitud;$i++) $key .= $pattern{mt_rand(0,$max)};
        return $key;
    }
       $user=Auth::user()->id;
       $estatus=3;
       $fecha=date('Y-m-d');
       $random=generarRandom(4);
       $firma=$fecha.$user.$random;
       $firma_seguridad=md5($firma);

       $solicitud = new GenSolicitud;
       $solicitud->tipo_solicitud_id=$request->tipo_solicitud_id;
       $solicitud->gen_usuario_id=$user;
       $solicitud->gen_status_id=$estatus;
       $solicitud->fstatus=$fecha;
       $solicitud->fsolicitud=$fecha;
       $solicitud->opcion_tecnologia=$request->opcion_tecnologia;
       $solicitud->cfirma_seguridad=$firma_seguridad;
       $solicitud->save();
       //consultamos mi ultima solictud registrada
       $ult_solicitud=GenSolicitud::where('gen_usuario_id',Auth::user()->id)->where('gen_status_id',3)->orderby('created_at','DESC')->take(1)->first();

       //dd($request->all());

       $consi_tecno=new ConsigTecno;
       $consi_tecno->gen_tipo_solicitud=$ult_solicitud->id;
       $consi_tecno->gen_id_consignatario=$request->consig_tecno;
       $consi_tecno->bactivo=1;
       $consi_tecno->save();


       //dd($ult_solicitud->id);
       //creamos variable de sesion con mi ultima solicitud registrada
       Session::put('gen_solictud_id',$ult_solicitud->id);


       //Creamos variables de session con el tipo de solicitud de mi ultima solicitud registrada///
       Session::put('tipo_solicitud_id',$ult_solicitud->tipo_solicitud_id);




        return redirect()->action('FacturaSolicitudController@create');
        Alert::success('Registro Exitoso!', 'Solicitud Agregada.')->persistent("OK");

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\GenSolicitud  $genSolicitud
     * @return \Illuminate\Http\Response
     */
    public function show(GenSolicitud $genSolicitud)
    {

    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\GenSolicitud  $genSolicitud
     * @return \Illuminate\Http\Response
     */
    public function edit(GenSolicitud $genSolicitud, $id)
    {
       
       
       //dd(session()->all());
        $titulo="Editar Solicitud Exportación Realizada";
        $descripcion="Editar Solicitud";
        $solicitud=GenSolicitud::find($id);
        $fecha=date('Y-m-d');
    //    dd($id);
        $consig_id=ConsigTecno::where('gen_tipo_solicitud',$id)->first();
        $consig=GenConsignatario::where('id',$consig_id->gen_id_consignatario)->pluck('nombre_consignatario','id');
        $tipoExpor=DetUsuario::where('gen_usuario_id',Auth::user()->id)->first();

        ($tipoExpor->produc_tecno==1) ? $num=2 : $num=1;
                   
        $tiposolic=TipoSolicitud::whereIn('id',[$num])->pluck('solicitud','id');

        Session::put('tipo_solicitud_id',$num);
        

        return view('SolicitudER.edit', compact('titulo','descripcion','solicitud','fecha','tiposolic','consig','num'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\GenSolicitud  $genSolicitud
     * @return \Illuminate\Http\Response
     */
    public function update(GenSolicitudRequest $request, GenSolicitud $genSolicitud, $id)
    {
      
        GenSolicitud::find($id)->update($request->all());
        $solicitud_er=GenSolicitud::find($id);
        $ses=Session::put('gen_solictud_id',$solicitud_er->id);
        $obtener_sol=Session::get('gen_solictud_id');
        
        //Alert::success('Actualización Exitosa!', 'Actualización Exitosa.')->persistent("OK");
        return redirect("exportador/FacturaSolicitud/$obtener_sol/edit");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\GenSolicitud  $genSolicitud
     * @return \Illuminate\Http\Response
     */
    public function destroy(GenSolicitud $genSolicitud)
    {
        //
    }

    public function verificarFac(Request $request){

       if($request->ajax()){

         $arx=[];
         $data=GenFactura::where('gen_usuario_id',Auth::user()->id)->get();
        foreach ($data as $key => $value) {

             if($value->bactivo!==0){
               $arx[$key]=0;
               break;
             }
         }

        if($arx==null){
            return 1;
        }else{
            return 0;
        }

       }
    }
}
