<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CatDocumentos extends Model
{
    protected $table = 'cat_documentos';

    	protected $fillable = [

           'gen_ente_id',
           'nombre_documento',
           'bactivo',
    ];

    protected $date =['created_at','updated_at'];

    public function ente() {

     return $this->hasOne('App\Models\GenEnte','id','gen_ente_id');

    }
}
