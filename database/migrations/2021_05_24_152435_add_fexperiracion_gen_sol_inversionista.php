<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFexperiracionGenSolInversionista extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('gen_sol_inversionista', function (Blueprint $table) {
            //
            $table->date('fecha_expiracion')->index()->nullable()->after('apellido_repre_legal');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('gen_sol_inversionista', function (Blueprint $table) {
            //
            $table->dropColumn('fecha_expiracion');
            
        });
    }
}
