<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AltertablePlanillaDjir02BaseEstimacion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::table('planilla_djir02', function (Blueprint $table) {
            //
            $table->string('base_estimacion')->nullable()->after('especifique_otro'); 
          
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::table('planilla_djir02', function (Blueprint $table) {
            //
            $table->dropColumn('base_estimacion');
        });
    }
}
