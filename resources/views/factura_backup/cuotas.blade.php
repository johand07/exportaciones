
<table class="table table-bordered table-hover" id="cuotas">

  <tr>
    <th> N° Cuotas </th>
    <th> Monto </th>
    <th> Fecha Estimada de Pago </th>
  </tr>

  <tr id="filas">

    <td>{!!Form::text('num_cuotas[]',null,['class'=>'form-control','id'=>'num_cuotas'])!!}
    </td>

    <td>{!!Form::text('monto[]',null,['class'=>'form-control','id'=>'monto'])!!}
    </td>

    <td><div class="input-group date">
         {!! Form::text('fecha_pago[]', null, ['class' => 'form-control','id'=>'fecha_pago']) !!}
           <span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
       </div>
    </td>

    <td><button  name="agregar" type="button" id="agregar" value="add more" class="btn btn-success">Agregar</button></td>

  </tr>


</table>





@push('readyscripts')

var i = 0;
      $('#agregar').click(function(){
       i++;

$('#cuotas tr:last').after('<tr id="row'+i+'" display="block" class="show_div"><td>{!!Form::text('num_cuotas[]',null,['class'=>'form-control','id'=>'num_cuotas'])!!}</td><td>{!!Form::text('monto[]',null,['class'=>'form-control','id'=>'monto'])!!}</td><td class="input-group date">{!!Form::text('fecha_pago[]', null, ['class' => 'form-control','id'=>'fecha_pago'])!!}<span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span></td><td><button  name="remove" type="button" id="'+i+'" value="" class="btn btn-danger btn-remove">x</button></td></tr>');


   $('.input-group.date').datepicker({ format: 'yyyy-mm-dd',autoclose: true});

    });

      $(document).on('click','.btn-remove',function(){
        var id_boton= $(this).attr("id");
        $("#row"+id_boton+"").remove();

      });


@endpush
