@extends('templates/layoutlte')
@section('content')

  <div class="row">
  <div class="col-md-12">
    <div class="row">
      <div class="col-md-7"></div>
      <div class="col-md-3">
          <a class="btn btn-primary" href="{{action('PdfController@PlanillaConsigDoc')}}">
          <span class="glyphicon glyphicon-download" title="Agregar" aria-hidden="true"></span>
           Planilla Consignación de Documentos
        </a>
      </div>
      <div class="col-md-2">
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal" title="Agregar" aria-hidden="true">Registar Nueva Venta
        </button>
      </div>
    </div>
    @if(isset($listaventa)) 

    
      <table id="listaventa" class="table table-striped table-bordered" style="width:100%">
        <thead>
         <tr>
             <th>Número de Factura</th>
             <th>Fecha Creación Dvd</th>
             <th>Monto Fob</th>
             <th>Monto Percibido de la Venta</th>
             <th>Monto Vendido al BCV</th>
             <th>Monto Pendiente por Vender</th>
             <th>Divisa</th>
             <th>Observaciones</th>
             <th>Acciones</th>
         </tr>
        </thead>
        <tbody>
          @foreach($listaventa as  $lista)
            <tr>
                <td>@if(!is_null($lista->gen_factura_id)){{$lista->factura->numero_factura}}@endif</td>
                <td>{{$lista->fventa_bcv}}</td>
                <td>@if(!is_null($lista->mfob)){{$lista->mfob}}@endif</td>
                <td>@if(!is_null($lista->mpercibido)){{$lista->mpercibido}}@endif</td>
                <td>@if(!is_null($lista->mvendido)){{$lista->mvendido}}@endif</td>
                <td>@if(!is_null($lista->mpendiente)){{$lista->mpendiente}}@endif</td>
                <td>@if(!is_null($lista->gen_factura_id)){{$lista->factura->divisa->ddivisa_abr}}@endif</td>
                <td>{{$lista->descripcion}}</td>
                <td>
                    @if($lista->realizo_venta==1)
                    <a href="{{action('PdfController@planillaNotaD',array('id'=>$lista->id))}}" class="glyphicon glyphicon-file btn btn-default btn-sm" title="Planilla ER" aria-hidden="true">DVD</a>
                @else
                    <a href="{{action('PdfController@planillaDvdDesND',array('id'=>$lista->id))}}" class="glyphicon glyphicon-file btn btn-default btn-sm" title="Planilla ER" aria-hidden="true">DVD</a>
                @endif

                <!--Congelando boton de los doc para usuarios declarados que quieran hacer la carga de los doc-->
                {{--  @if($lista->gen_operador_cambiario_id==2)
                    @if(empty($lista->docNd))
                        <a href="{{route('DocumentosDvdND.create',array('id'=>$lista->id))}}" class="glyphicon glyphicon-file btn btn-primary btn-sm" aria-hidden="">Documentos <br>DVD ND</a>
                    @endif
                @endif--}}
                </td>
            </tr>
          @endforeach
        </tbody>
      </table>
    @else
    @endif
  </div>
</div>
@stop
<!-- Modal -->
    <div class="modal fade" id="myModal" role="dialog"  data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <!--<button type="button" class="close" data-dismiss="modal">&times;</button>-->
                    <h4 class="modal-title">ATENCIÓN EXPORTADORES</h4>
                </div>
                <div class="modal-body">
                   <img class="img-responsive" src={{ asset('img/dvd.jpg') }}>

                </div>
                <div class="modal-footer">
                    <a href="{{route('VentanND.edit',$idsolicitud)}}" type="button" class="btn btn-success btn-block"><b>ENTENDIDO</b></a>
                </div>
            </div>
        </div>
    </div>