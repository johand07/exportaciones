<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class GenUsuario extends Authenticatable
{
	use Notifiable;

    protected $table = 'gen_usuario';

		protected $fillable = [

        		
				'email',
				'email_seg',
				'gen_tipo_usuario_id',
				'gen_status_id',
				'cat_preguntas_seg_id',
				'pregunta_alter',
				'respuesta_seg',
				'bactivo'		
    ];

		protected $hidden = [
        'password', 'remember_token',
    ];



		public function tipoUsuario()
	 {

			 return $this->hasOne('App\Models\GenTipoUsuario','id','gen_tipo_usuario_id');

	 }


	public function estatus()
 {

		 return $this->hasOne('App\Models\GenStatus','id','gen_status_id');

 }

	public function pregunta()
 {

		 return $this->hasOne('App\Models\CatPreguntaSeg','id','cat_preguntas_seg_id');

 }

 public function detUsuario()
 {

		 return $this->hasOne('App\Models\DetUsuario','gen_usuario_id','id');

 }

 public function rDetUsersInversiones() {

     return $this->hasOne('App\Models\detUsersInversiones','gen_usuario_id','id');

    
    }

 public function rDetUsersResguardo() {

     return $this->hasOne('App\Models\DetUsersResguardo','gen_usuario_id','id');

    
    }

 }
