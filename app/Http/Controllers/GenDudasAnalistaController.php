<?php

namespace App\Http\Controllers;

use App\Models\{GenAsistenciaUsuario, DetUsuario};
//use App\Http\Requests\GenDuaRequest;
use Session;
use Auth;
use Laracasts\Flash\Flash;
use Alert;
use Illuminate\Http\Request;

class GenDudasAnalistaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $titulo="Dudas Registradas";
        $descripcion="Duda Realizada";
        $solicitudes = GenAsistenciaUsuario::select('gen_asistencia_usuario.id', 'gen_asistencia_usuario.gen_usuario_id','gen_asistencia_usuario.motivo_de_contacto',
            'gen_asistencia_usuario.mensaje','gen_asistencia_usuario.gen_status_id as status',
            'gen_asistencia_usuario.gen_usuario_atencion_id as usuario_atencion_id', 'gen_asistencia_usuario.created_at',
            'gen_status.nombre_status', 'gen_usuario.email as email')
        ->join('gen_status', 'gen_asistencia_usuario.gen_status_id','=','gen_status.id')
        ->join('gen_usuario', 'gen_asistencia_usuario.gen_usuario_id','=','gen_usuario.id')
        ->get();
        return view('DudasAnalistaSolicitud.index',compact('solicitudes','titulo','descripcion'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request);
        $agregar_respuesta = GenAsistenciaUsuario::find($request->gen_asistencia_usuario_id);
        $gen_usuario_atencion_id = $request->gen_usuario_atencion_id;
        $respuesta = $request->respuesta_analista;
        //dd($agregar_respuesta);
        /*
        $email = $request->correo;
        $nombreape = $request->razon_social;
        $correo = $request->correo;
        $telefono = $request->telefono_movil;
        $tipo = $request->tipo;
        $texto = $request->mensaje;
        $motivo_de_contacto = $request->tipo;
        $mensaje = $request->mensaje;
        */
        $agregar_respuesta->gen_usuario_atencion_id = $gen_usuario_atencion_id;
        $agregar_respuesta->respuesta = $respuesta;
        $agregar_respuesta->gen_status_id = 10;
        $agregar_respuesta->save();

        //enviamos email con datos para acceso
        //Mail::to($email)->send(new DudasEmail($nombreape,$correo,$telefono,$tipo,$texto));
        //Alert::success('Duda enviada!','Gracias por consultar en nuestro sistema.')->persistent("Ok");

        return redirect()->action('GenDudasAnalistaController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\GenDVDSolicitud  $genDVDSolicitud
     * @return \Illuminate\Http\Response
     */
    public function show(GenAsistenciaUsuario $genAsistenciaUsuario, $id)
    {
        //
        $solicitud = GenAsistenciaUsuario::where('gen_asistencia_usuario.id', $id)
        ->select(
            'gen_asistencia_usuario.id', 'gen_asistencia_usuario.gen_usuario_id',
            'gen_asistencia_usuario.motivo_de_contacto', 'gen_asistencia_usuario.gen_status_id as status',
            'gen_asistencia_usuario.mensaje','gen_asistencia_usuario.respuesta',
            'gen_asistencia_usuario.gen_usuario_atencion_id as usuario_atencion_id',
            'gen_asistencia_usuario.created_at',
            'gen_status.nombre_status', 'gen_usuario.email as email',
            'det_usuario.razon_social', 'det_usuario.telefono_movil', 'det_usuario.correo as correo'
            )
        ->join('gen_status', 'gen_asistencia_usuario.gen_status_id','=','gen_status.id')
        ->join('gen_usuario', 'gen_asistencia_usuario.gen_usuario_id','=','gen_usuario.id')
        ->join('det_usuario', 'gen_asistencia_usuario.gen_usuario_id', '=', 'det_usuario.gen_usuario_id')
        ->first();
        return view('DudasAnalistaSolicitud.show',compact('solicitud'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\GenDVDSolicitud  $genDVDSolicitud
     * @return \Illuminate\Http\Response
     */
    public function edit(GenAsistenciaUsuario $genIdSolicitud, $id)
    {
        //
        $solicitud = GenAsistenciaUsuario::where('gen_asistencia_usuario.id', $id)
        ->select(
            'gen_asistencia_usuario.id', 'gen_asistencia_usuario.gen_usuario_id',
            'gen_asistencia_usuario.motivo_de_contacto', 'gen_asistencia_usuario.mensaje','gen_asistencia_usuario.gen_status_id as status',
            'gen_asistencia_usuario.gen_usuario_atencion_id as usuario_atencion_id', 'gen_asistencia_usuario.respuesta',
            'gen_asistencia_usuario.created_at',
            'gen_status.nombre_status', 'gen_usuario.email as email',
            'det_usuario.razon_social', 'det_usuario.telefono_movil', 'det_usuario.correo as correo'
            )
        ->join('gen_status', 'gen_asistencia_usuario.gen_status_id','=','gen_status.id')
        ->join('gen_usuario', 'gen_asistencia_usuario.gen_usuario_id','=','gen_usuario.id')
        ->join('det_usuario', 'gen_asistencia_usuario.gen_usuario_id', '=', 'det_usuario.gen_usuario_id')
        ->first();
        
        return view('DudasAnalistaSolicitud.edit',compact('solicitud'));
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\GenAsistenciaUsuario  $gen_asistencia_usuario_id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $agregar_respuesta = GenAsistenciaUsuario::find($id);
        $respuesta = $request->respuesta_analista;
        $agregar_respuesta->respuesta = $respuesta;
        $agregar_respuesta->save();
        
        //enviamos email con datos para acceso
        //Mail::to($email)->send(new DudasEmail($nombreape,$correo,$telefono,$tipo,$texto));
        //Alert::success('Duda enviada!','Gracias por consultar en nuestro sistema.')->persistent("Ok");

        return response()->json(['success'=>'Respuesta actualizado exitosamente.']);
        return redirect()->action('GenDudasAnalistaController@show', $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\GenDVDSolicitud  $genDVDSolicitud
     * @return \Illuminate\Http\Response
     */
    public function destroy(GenDVDSolicitud $genDVDSolicitud)
    {
        //
    }
}
