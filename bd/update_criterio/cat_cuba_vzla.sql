-- phpMyAdmin SQL Dump
-- version 4.6.6deb5ubuntu0.5
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 04-04-2022 a las 11:52:23
-- Versión del servidor: 5.7.33-0ubuntu0.18.04.1
-- Versión de PHP: 7.3.27-9+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `exportaciones`
--

-- --------------------------------------------------------

-- Volcado de datos para la tabla `cat_cuba_vzla`
--

INSERT INTO `cat_cuba_vzla` (`id`, `codigo`, `descripcion`, `bactivo`, `created_at`, `updated_at`) VALUES
(1, '3-1A', 'Anexo I,Artículo 3 párrafo 1, literales a) al h),(según sea el caso)', 1, NULL, NULL),
(2, '3-1B', 'Anexo I,Artículo 3 párrafo 1, literales a) al h),(según sea el caso)', 1, NULL, NULL),
(3, '3-1C', 'Anexo I,Artículo 3 párrafo 1, literales a) al h),(según sea el caso)', 1, NULL, NULL),
(4, '3-1D', 'Anexo I,Artículo 3 párrafo 1, literales a) al h),(según sea el caso)', 1, NULL, NULL),
(5, '3-1E', 'Anexo I,Artículo 3 párrafo 1, literales a) al h),(según sea el caso)', 1, NULL, NULL),
(6, '3-1F', 'Anexo I,Artículo 3 párrafo 1, literales a) al h),(según sea el caso)', 1, NULL, NULL),
(7, '3-1G', 'Anexo I,Artículo 3 párrafo 1, literales a) al h),(según sea el caso)', 1, NULL, NULL),
(8, '3-1H', 'Anexo I,Artículo 3 párrafo 1, literales a) al h),(según sea el caso)', 1, NULL, NULL),
(9, '3-2', 'Anexo I,Artículo 3 párrafo 2', 1, NULL, NULL),
(10,'3-3', 'Anexo I,Artículo 3 párrafo 3', 1, NULL, NULL),
(11, '3-4A','Anexo I,Artículo 3 párrafo 4,literal a)', 1, NULL, NULL),
(12, '3-4B','Anexo I,Artículo 3 párrafo 4 literal b)', 1, NULL, NULL),
(13, '3-4C','Anexo I,Artículo 3, párrafo 4, literal c)', 1, NULL, NULL),
(14, '4-1', 'Anexo I,Artículo 4, párrafo 1', 1, NULL, NULL),
(15, '8', 'Artículo 8°', 1, NULL, NULL),
(16, '*', 'Información insuficiente para la determinación del Origen', 1, NULL, NULL),
(17, '(1)', 'no', 1, NULL, NULL),
(18, '(2)', 'no', 1, NULL, NULL),
(19, '(3)', 'no', 1, NULL, NULL);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `cat_cuba_vzla`
--
ALTER TABLE `cat_cuba_vzla`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `cat_cuba_vzla`
--
ALTER TABLE `cat_cuba_vzla`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
