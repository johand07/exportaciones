<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GenAnticipoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        'nro_anticipo'=>'required',
        'fecha_anticipo'=>'required',
        'gen_consignatario_id'=>'required',
        'nro_doc_comercial'=>'required',    
        'monto_anticipo'=>'required',
        'monto_total_ant'=>'required',
        'observaciones'=>'required',

            
        ];
    }
}
