@extends('templates/layoutlte')
@section('content')

<script src="{{asset('plugins/fastselect/build.min.js')}}"></script>
<link rel="stylesheet" href="{{asset('plugins/fastselect/fastselect.min.css')}}">
<script src="{{asset('plugins/fastselect/fastselect.standalone.js')}}"></script>

{{Form::model($planillaDjir03,['route'=>['savePlanilla3Inversion.update',$planillaDjir03->id],'method'=>'PATCH','id'=>'formPlanilla1Inversion'])}}
@if(isset($planillaDjir03->descrip_observacion) && $planillaDjir03->estado_observacion == 1)
<div class="alert alert-dismissible alert-danger">
  <button type="button" class="close" data-dismiss="alert">&times;</button>
  <h4 class="alert-heading">Observación!</h4>
  <p class="mb-0">{{$planillaDjir03->descrip_observacion}}</p>
</div>
@endif
{!! Form::hidden('estado_observacion',null,['class'=>'form-control','id'=>'estado_observacion','onkeypress'=>'return soloNumeros(event)'])!!}
{{Form::hidden('gen_declaracion_inversion_id',$planillaDjir03->gen_declaracion_inversion_id)}}

<div id="section1">
	<ol class="breadcrumb">
	  <li  class="active" id="paso1" style="color:#1E3165;">Paso 1</li>
	  <li  id="paso2">Paso 2</li>
	</ol>
	<div class="panel panel-primary">
     <div class="panel-heading"><h3>Tiene perspectivas de realizar inversiones con recursos externos en la República Bolivariana de Venezuela</h3></div>
 <div class="panel-body">
 	        <div class="row">
              	<div class="col-md-12">
	              	<div class="col-md-2"><br>
	              		<div class="form-group">
		              		<div class="form-check form-check-inline" id="rvalorsi">
		  						{!! Form::radio('inversion_recursos_externos','Si',false,['class'=>'form-check-input','id'=>'inversion_recursos_externos'])!!}
		  						{!! Form::label('','Si',['class'=>'form-check-label'])!!}

		  						{!! Form::hidden('',$planillaDjir03->inversion_recursos_externos,['class'=>'form-control','id'=>'inversion_recursos_externos_edit'])!!}
							</div>
						</div>
					</div>
	              	<div class="col-md-2"><br>
	              		<div class="form-group">
		              		<div class="form-check form-check-inline" id="rvalorno">
		 						{!! Form::radio('inversion_recursos_externos','No',false,['class'=>'form-check-input','id'=>'inversion_recursos_externos'])!!}
		  						{!! Form::label('','No',['class'=>'form-check-label'])!!}
							</div>
						</div>
					</div>
	              	<div class="col-md-8"><br>
	              		<div class="form-group" id="detailinver">
		                 	{!! Form::label('','Detalle:')!!}
		                  	{!! Form::text('det_invert_recursos',null,['class'=>'form-control','id'=>'det_invert_recursos','onkeypress'=>'return soloLetras(event)'])!!}
		            	</div>
	              	</div>
              	</div>
            </div>

           	<div class="row" style="display: none;" id="emp">
              <div class="col-md-12">
              	<h4 style="color:red;"><b>Indicar los siguientes aspectos:</b></h4>
              	<div class="form-group" id="ocultarproyecnew">
					<div class="form-check form-check-inline">
	  					{!! Form::radio('proyecto_nuevo','Si',false,['class'=>'form-check-input','id'=>'proyecnew'])!!}
		  				{!! Form::label('','Proyecto nuevo (Otra empresa)',['class'=>'form-check-label'])!!}

		  				{!! Form::hidden('',$planillaDjir03->proyecto_nuevo,['class'=>'form-control','id'=>'proyecto_nuevo_edit'])!!}
					</div>
				</div>
				<div class="form-group" id="ocultarampliacion">
					<div class="form-check form-check-inline">
						{!! Form::radio('proyecto_nuevo','No',false,['class'=>'form-check-input','id'=>'ampliacion'])!!}
		  				{!! Form::label('','Ampliación de su actual empresa',['class'=>'form-check-label'])!!}
	 				</div>
				</div>
              </div>
            </div><br><br>


  			<div class="row" style="display: none;" id="emp1">
              <div class="col-md-12">
              	<div class="col-md-4">
              		<div class="form-group">
	                 	{!! Form::label('','Nombre del Proyecto:')!!}
	                  	{!! Form::text('nombre_proyecto',null,['class'=>'form-control','id'=>'nompre_proyecto','onkeypress'=>'return soloLetras(event)'])!!}
	            	</div>
              	</div>
              	<div class="col-md-4">
              		<div class="form-group">
	                 	{!! Form::label('','Ubicación (Estado):')!!}
	                  	{!! Form::text('ubicacion',null,['class'=>'form-control','id'=>'ubicacion','onkeypress'=>'return soloNumerosyLetras(event)'])!!}
	            	</div>
              	</div>
              	<div class="col-md-4">
              		<div class="form-group">
	                 	{!! Form::label('','Origen de los Recursos (País de Origen):')!!}

	                  	{!!Form::select('pais_id',$estado,null,['class'=>'form-control', 'id'=>'pais_id','placeholder'=>'Seleccione el País de Origen']) !!}
	            	</div>
              	</div>
				
              </div>
            </div><br>

 			<div class="row" style="display: none;" id="emp2">
              	<div class="col-md-12">
					<h4 style="color:red;"><b>Bajo que Modalidad Prevé sus Inversiones en el Futuro:</b></h4>
		      		<div class="form-group">
			      		<div class="form-check form-check-inline" id="ried">
			      			{!! Form::radio('ampliacion_actual_emp','Inversión Extranjera Directa',false,['class'=>'form-check-input','id'=>'ied'])!!}
		  					{!! Form::label('','Inversión Extranjera Directa (IED)',['class'=>'form-check-label'])!!}
						</div>
					</div>
					<div class="form-group">
						<div class="form-check form-check-inline" id="ric">
							{!! Form::radio('ampliacion_actual_emp','Inversión de Cartera',false,['class'=>'form-check-input','id'=>'ic'])!!}
		  					{!! Form::label('','Inversión de Cartera',['class'=>'form-check-label'])!!}
						</div>
					</div>
		  				{!! Form::hidden('',$planillaDjir03->ampliacion_actual_emp,['class'=>'form-control','id'=>'modalidad_edit'])!!}
            	</div>
        	</div>

           	<div class="row" style="display: none;" id="directa">
              	<div class="col-md-12">
					<div class="col-md-6">
						<div class="form-group">
							<div class="form-check form-check-inline">
								{!! Form::checkbox('ampliacion_accionaria_dir','Participacion Accionaria',false,['class'=>'form-check-input','id'=>'particiarancel'])!!}
			  					{!! Form::label('','Participación Accionaria',['class'=>'form-check-label'])!!}

			  					{!! Form::hidden('',$planillaDjir03->ampliacion_accionaria_dir,['class'=>'form-control','id'=>'participacion_edit'])!!}
							</div>
						</div>
						<div class="form-group">
							<div class="form-check form-check-inline">
								{!! Form::checkbox('utilidad_reinvertida','Utilidades Reinvertidas',false,['class'=>'form-check-input','id'=>'utilidades'])!!}
			  					{!! Form::label('','Utilidades Reinvertidas',['class'=>'form-check-label'])!!}

			  					{!! Form::hidden('',$planillaDjir03->utilidad_reinvertida,['class'=>'form-control','id'=>'utilidades_edit'])!!}
							</div>
						</div>
						<div class="form-group">
							<div class="form-check form-check-inline">
								{!! Form::checkbox('credito_casa_matriz','Creditos con Casa Matriz o Afiliadas',false,['class'=>'form-check-input','id'=>'csamatriz'])!!}
			  					{!! Form::label('','Créditos con Casa Matriz o Afiliadas',['class'=>'form-check-label'])!!}

			  					{!! Form::hidden('',$planillaDjir03->credito_casa_matriz,['class'=>'form-control','id'=>'casamatriz_edit'])!!}
							</div>
						</div>
						<div class="form-group">
							<div class="form-check form-check-inline">
								{!! Form::checkbox('creditos_terceros','Creditos con Terceros',false,['class'=>'form-check-input','id'=>'terceros'])!!}
			  					{!! Form::label('','Créditos con Terceros',['class'=>'form-check-label'])!!}

			  					{!! Form::hidden('',$planillaDjir03->creditos_terceros,['class'=>'form-control','id'=>'creditos_edit'])!!}
							</div>
						</div>
						<div class="form-group">
							<div class="form-check form-check-inline">
								{!! Form::checkbox('otra_dir','Otras',false,['class'=>'form-check-input','id'=>'otras'])!!}
			  					{!! Form::label('','Otras',['class'=>'form-check-label'])!!} 

			  					{!! Form::hidden('',$planillaDjir03->otra_dir,['class'=>'form-control','id'=>'otros_edit'])!!}
							</div>
						</div>
					</div>
              	</div>
        	</div>

			<div class="row" style="display: none;" id="cartera">
	            <div class="col-md-12">
					<div class="col-md-6">
						<div class="form-group">
							<div class="form-check form-check-inline">
								{!! Form::checkbox('participacion_accionaria_cart','Participacion Accionaria',false,['class'=>'form-check-input','id'=>'partiarancel'])!!}
			  					{!! Form::label('','Participación Accionaria',['class'=>'form-check-label'])!!}

			  					{!! Form::hidden('',$planillaDjir03->participacion_accionaria_cart,['class'=>'form-control','id'=>'car_edit'])!!}
							</div>
						</div>
						<div class="form-group">
							<div class="form-check form-check-inline">
								{!! Form::checkbox('bonos_pagare','Bonos y Pagares',false,['class'=>'form-check-input','id'=>'bonospagares'])!!}
			  					{!! Form::label('','Bonos y Pagares',['class'=>'form-check-label'])!!}

			  					{!! Form::hidden('',$planillaDjir03->bonos_pagare,['class'=>'form-control','id'=>'bonospagare_edit'])!!}
							</div>
						</div>
						<div class="form-group">
							<div class="form-check form-check-inline">
								{!! Form::checkbox('otra_cart','Otras',false,['class'=>'form-check-input','id'=>'otro'])!!}
			  					{!! Form::label('','Otras',['class'=>'form-check-label'])!!}

			  					{!! Form::hidden('',$planillaDjir03->otra_cart,['class'=>'form-control','id'=>'otro_cartera_edit'])!!}
							</div>
						</div>
					</div>
	            </div>
            </div>


            <div class="panel-primary" style="#fff">
<div class="panel-heading"><h3>Periodo previsto para efectuar su inversión</h3></div>
<div class="panel-body"><br>
    <div class="row">
    	 <div class="col-md-12">
          	<div class="col-md-4">
          		<h5><b>Fecha:</b></h5>
          		<div class="input-group date">
                {!! Form::text('periodo_inversion',null,['class'=>'form-control','readonly'=>'readonly','id'=>'periodo_inversion']) !!}
                <span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
            </div>
          	</div>
          	<div class="col-md-4"></div>
          	<div class="col-md-4"></div>
           
          </div>
    </div>
</div>
</div>
 </div>
   
		<div class="row">
	      <div class="col-md-12">
	        <div class="col-md-4"></div>
	        <div class="col-md-4 text-center">
	          <div class="form-group">
	            <a class="btn btn-primary" href="#" id="seguir1">Siguiente</a>
	          </div>
	        </div>
	      </div>
	    </div>
    </div>
</div>

<!------------------------------------------------------------------------------------------------------------->
<div id="section2" style="display: none;">
	  <ol class="breadcrumb">
	      <li  class="active" id="paso1" style="color:#1E3165;">Paso 1</li>
		  <li  class="active" id="paso2" style="color:#1E3165;">Paso 2</li>
      </ol>
	<div class="panel panel-primary">
	    <div class="panel-primary" style="#fff">
<div class="panel-heading"><h3>Sector Productivo o Económico</h3></div>
<div class="panel-body">
            <div class="row">
              <div class="col-md-12">

            	@php

				// obtener el Sector Productivo o Económico sociado a ese certificado
				      $listadesectorproducecono=[];
				      $valueIdSector="";
				        foreach($gen_sector_productivo_djir03 as $sector)
				        {   

				        $listadesectorproducecono[]=['text'=>''.$sector->sectorProdEco->nombre_sector.'','value'=>''.$sector->sectorProdEco->id.''];

				         // para colocar el atribute value del input multiple de la siguiente manera value="id1pais,idpais2,idpais3"
				         $valueIdSector.=''.$sector->sectorProdEco->id.',';
				        }
				      $sector=json_encode($listadesectorproducecono);

     			 @endphp
            		<div class="col-md-6"><b></b><br>
						<div class="form-group">	
							<input
		                    type="text"
		                    multiple
		                    class="tagsInput"
		                    value="{{ $valueIdSector or '' }}"
		                    placeholder="Seleccionar Sector Productivo o Económico"
		                    data-initial-value='{!! $sector or '' !!}'
		                    data-user-option-allowed="false"
		                    data-url="{{url('exportador/ajax/sectoreconomico')}}"
		                    data-load-once="false"
		                    name="cat_sector_productivo_eco_id[]"
		                    id="cat_sector_productivo_eco_id">
		                    <script type="text/javascript">
		                        $('.tagsInput').fastselect();
		                    </script>
		                </div>
            		</div>
            	</div>
            </div><br>
        </div>
</div> 

<div class="panel-primary" style="#fff">
<div class="panel-heading"><h3>Destino de la Inversión</h3></div>
<div class="panel-body"><br>
            <div class="row">
            	<div class="col-md-12">
            		@php

				// obtener el Sector Productivo o Económico sociado a ese certificado
				      $listaDestinoInversion=[];
				      $valueIdDestino="";
				        foreach($gen_destino_inversion_djir03 as $destino)
				        {   

				        $listaDestinoInversion[]=['text'=>''.$destino->catDestInversion->nombre_destino.'','value'=>''.$destino->catDestInversion->id.''];

				         // para colocar el atribute value del input multiple de la siguiente manera value="id1pais,idpais2,idpais3"
				         $valueIdDestino.=''.$destino->catDestInversion->id.',';
				        }
				      $destinoInversion=json_encode($listaDestinoInversion);

     			 @endphp
            		<div class="col-md-6"><b></b><br>
						<div class="form-group">	
							<input
		                    type="text"
		                    multiple
		                    class="tagsInput"
		                    value="{{ $valueIdDestino or '' }}"
		                    placeholder="Seleccionar Destino de la Inversión"
		                    data-initial-value='{!! $destinoInversion or '' !!}'
		                    data-user-option-allowed="false"
		                    data-url="{{url('exportador/ajax/destinoinversion')}}"
		                    data-load-once="false"
		                    name="cat_destino_inversion_id[]"
		                    id="cat_destino_inversion_id">
		                    <script type="text/javascript">
		                        $('.tagsInput').fastselect();
		                    </script>
		                </div>
            		</div>
            	</div>
            </div>
        </div>
</div>

            
<br><br>
        	<div class="row">
              <div class="col-md-12">
                <div class="col-md-4"></div>
                <div class="col-md-4 text-center">
                  <div class="form-group">
                    <a class="btn btn-primary" href="#" id="atras1">Atras</a>
                   <a class="btn btn-primary" href="#" id="guardar_personal" onclick="enviarplanilla3()">Registrar</a>
                  </div>
                </div>
              </div>
            </div>
        </div>		
    </div>
</div>
<!------------------------------------------------------------------------------------------------------------->



<!-------Modal--->
<div id="planilla3" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close btn-md" data-dismiss="modal" style="height:5px;">&times;</button>
        <h4 class="modal-title"><b style="color:#337AB7">DECLARACIÓN JURADA DE INVERSIÓN REALIZADA</b></h4>
      </div>
      <div class="modal-body">
      <div class="col-md-12">
        <p class="text-justify">DECLARO QUE LOS DATOS CONTENIDOS EN ESTA DECLARACIÓN HAN SIDO DETERMNADOS EN BASE A LAS DISPOSICIONES LEGALES Y REGLAMENTARIAS CORRESPONDIENTES.
        </p>
        <div class="row">
          <div class="colo-md-1"></div>
          <div class="col-md-10">
             <input type="checkbox" name="condicion" id="condicion" value="si" class="checkbox-inline"><label class="text-justify">LA INFORMACIÓN AQUÍ DECLARADA ES ENTREGADA CON CARÁCTER CONFIDENCIAL.</label>
          </div>
          <div class="colo-md-1"></div>
        </div>
      </div>
      </div>
      <div class="modal-footer">
        <div class="row">
          <div class="col-md-12">
            <div class="col-md-4"></div>
            <div class="col-md-4 text-center">
              <div class="form-group">
                 <button type="button" class="btn btn-default" style="background-color:#ADABC9; color: #FFFFFF;" data-dismiss="modal"><b>Cerrar</b></button>
                <input type="submit" class="btn btn-primary" value="Aceptar" name="Aceptar"> 
              </div>
            </div>
            <div class="col-md-4"></div>
          </div>
        </div>
      </div>
    </div>

  </div>
</div>

{{Form::close()}}


	<script type="text/javascript">
			$('.tagsInput').fastselect();


$( document ).ready(function() {
	//alert($('#ampliacion_actual_emp_edit').val());
    var recursos_externos = $("#inversion_recursos_externos_edit").val(); 
		if(recursos_externos == "Si"){
		 	$('#emp').show();
		}



	var modalidad = $("#modalidad_edit").val(); 
		if(modalidad == "Inversión Extranjera Directa"){
			$("#ied").prop("checked", true);
		 	$('#directa').show();
		}else{
			$("#ic").prop("checked", true);
			$('#directa').hide();
			$('#cartera').show();
		}

		if($("#ampliacion").prop("checked", true)){
			$('#emp2').show();
		}


 var proyecto_nuevo = $("#proyecto_nuevo_edit").val(); 
		if(proyecto_nuevo == "Si"){
			$("#proyecnew").prop("checked", true);
		 	$('#emp1').show();
		 	$('#emp2,#cartera').hide();
		}


	var participacion = $("#participacion_edit").val(); 
		if(participacion == "Participacion Accionaria"){
			$("#particiarancel").prop("checked", true);
		}else{
		}

	var utilidades = $("#utilidades_edit").val(); 
		if(utilidades == "Utilidades Reinvertidas"){
			$("#utilidades").prop("checked", true);
		}else{
		}

	var casamatriz = $("#casamatriz_edit").val(); 
		if(casamatriz == "Creditos con Casa Matriz o Afiliadas"){
			$("#csamatriz").prop("checked", true);
		}else{
		}

	var creditos = $("#creditos_edit").val(); 
		if(creditos == "Creditos con Terceros"){
			$("#terceros").prop("checked", true);
		}else{
		}

	var otros = $("#otros_edit").val(); 
		if(otros == "Otras"){
			$("#otras").prop("checked", true);
		}else{
		}

	var car = $("#car_edit").val(); 
		if(car == "Participacion Accionaria"){
			$("#partiarancel").prop("checked", true);
		}else{
		}

	var bonospagare = $("#bonospagare_edit").val(); 
		if(bonospagare == "Bonos y Pagares"){
			$("#bonospagares").prop("checked", true);
		}else{
		}

	var otro_cartera = $("#otro_cartera_edit").val(); 
		if(otro_cartera == "Otras"){
			$("#otro").prop("checked", true);
		}else{
		}

});
	</script>
@stop
