<?php

namespace App\Http\Controllers;
use App\Models\GenUsuario;
use App\Models\Accionistas;
use App\Models\Productos;
use App\Models\DetUsuario;
use Illuminate\Http\Request;
use Session;
use Auth;
use Laracasts\Flash\Flash;
use Alert;
use PDF;
use Illuminate\Routing\Route;
use Illuminate\Http\Response;
use Maatwebsite\Excel\Facades\Excel;
//use App\Exports\UsersExport;
//use Illuminate\Http\Response;
//use Illuminate\Support\Facades\Response;

class ReporteEmpresasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $titulo="Detalle de Empresas";
        $descripcion="Empresas Registradas"; 
        //dd($request->desde);
        $fecha=date("Y-m-d");
        $desde=$request->desde;
        $hasta=$request->hasta;
        $empresas=DetUsuario::where('bactivo',1)->whereDate('created_at',$fecha)->whereNotIn('gen_usuario_id',[1,2,3,4,5,11,12])->get();
        
        
      //dd($empresas);

      return view('DetalleEmpresas.index',compact('empresas','titulo','descripcion','desde','hasta'));

        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $titulo="Detalle de Empresas";
        $descripcion="Empresas Registradas"; 
        //dd($request->all());
        $fecha=date("Y-m-d H");
        $fecha_desde=$request->desde.' 00:00:00';
        //dd($fecha_desde);
        $fecha_hasta=$request->hasta.' 23:59:59';
        $desde=$request->desde;
        $hasta=$request->hasta;
        
        if (!empty($request->desde) and ($request->hasta)) {
          
            //si es ! a vacio osea si esta lleno !empty muestrame tal cosa""""
            $empresas=DetUsuario::where('bactivo',1)->whereBetween('created_at',[$fecha_desde,$fecha_hasta])->whereNotIn('gen_usuario_id',[1,2,3,4,5,11,12])->get();
            //dd($empresas);
          /*  Excel::create('Reportes Detalle de Empresas',function($excel)use($empresas){ // Nombre princial del archivo
            $excel->sheet('Empresas',function($sheet)use ($empresas){ // se crea la hoja con el nombre de datos
               // dd($empresas);
                //header
                $sheet->setStyle(array(
                'font' => array(
                    'name' =>  'itálico',
                    'size' =>  8,
                    'bold' =>  true
                )
                ));

                $sheet->mergeCells('A1:I1'); // union de celdas desde A a F
                $sheet->row(1,['Reportes Detalle de Empresas']);
                $sheet->row(2,[]);
                $sheet->row(3,['Rif','Nombre empresa','Email','Teléfono Local','Teléfono Movil']);

                //$sheet->fromArray($empresas, null, 'A4', null, false);
                // consulta

                
               //dd($empresas);
                //recorrido
                foreach ($empresas as $emp) {


                   $row=[]; // un arreglo vacio que se va a llenar con las posicion 
                   $row[0]=$emp->rif;
                   $row[1]=$emp->razon_social;
                   $row[2]=$emp->correo;
                   $row[3]=$emp->telefono_local;
                   $row[4]=$emp->telefono_movil;

                   $sheet->appendRow($row); // agrega una fila vacia al final del reporte

                }


            });
           // dd($empresas);
           
        })->download('xlsx'); */
            
        // exportar el archivo con extension xlsx
            return view('DetalleEmpresas.index',compact('empresas','titulo','descripcion','fecha_desde','fecha_hasta','desde','hasta'));
            //return redirect()->back()->with('empresas', [$empresas]);
            //return redirect()->back(compact('empresas','titulo','descripcion','fecha_desde','fecha_hasta','desde','hasta'));
           
        } elseif(empty($empresas)) {
            $empresas=DetUsuario::where('bactivo',1)->whereDate('created_at',$fecha)->whereNotIn('gen_usuario_id',[1,2,3,4,5,11,12])->get();
        }

         

        //$vista="DetalleEmpresas.pdf_detalle";

        //$pdf=PDF::loadView($vista,compact('empresas'));

        //return $pdf->download('Detalle_Empresa.pdf');
        //return $pdf->download('Detalle_Empresa.pdf')->redirect()->route('DetalleEmpresas.index',compact('empresas','titulo','descripcion'));
        //return redirect()->response()->download('Detalle_Empresa.pdf')->route('DetalleEmpresas.index',compact('empresas','titulo','descripcion'));
        return view('DetalleEmpresas.index',compact('empresas','titulo','descripcion','fecha_desde','fecha_hasta','desde','hasta'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\DetUsuario  $detUsuario
     * @return \Illuminate\Http\Response
     */
    public function show(DetUsuario $detUsuario,$gen_usuario_id)
    {
        $titulo="Detalle de Empresas";
        $descripcion="Empresas Registradas"; 
        //dd($gen_usuario_id);
        $detusers=DetUsuario::where('bactivo',1)->where('gen_usuario_id',$gen_usuario_id)->first();

        //$users=GenUsuario::where('id',$detusers->gen_usuario_id)->first();

        $accionistas=Accionistas::where('gen_usuario_id', $gen_usuario_id)->get();
        $productos=Productos::where('gen_usuario_id', $gen_usuario_id)->get();
        return view('DetalleEmpresas.show',compact('detusers','accionistas','productos','titulo','descripcion'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\DetUsuario  $detUsuario
     * @return \Illuminate\Http\Response
     */
    public function edit(DetUsuario $detUsuario)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\DetUsuario  $detUsuario
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DetUsuario $detUsuario)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\DetUsuario  $detUsuario
     * @return \Illuminate\Http\Response
     */
    public function destroy(DetUsuario $detUsuario)
    {
        //
    }

    public function ExcelDetalleEmpresas(Request $request, DetUsuario $detUsuario)
    {
        
        //dd($request->all());
        $empresas=DetUsuario::where('bactivo',1)->whereBetween('created_at',[$request->desde,$request->hasta])->whereNotIn('gen_usuario_id',[1,2,3,4,5,11,12])->get();
        $name="Reportes Detalle de Empresas";
        //dd($empresas);
        $myFile   = Excel::create('Reportes Detalle de Empresas',function($excel)use($empresas){ // Nombre princial del archivo
            $excel->sheet('Empresas',function($sheet)use ($empresas){ // se crea la hoja con el nombre de datos
               // dd($empresas);
                //header
                $sheet->setStyle(array(
                'font' => array(
                    'name' =>  'itálico',
                    'size' =>  8,
                    'bold' =>  true
                )
                ));

                $sheet->mergeCells('A1:I1'); // union de celdas desde A a F
                $sheet->row(1,['Reportes Detalle de Empresas']);
                $sheet->row(2,[]);
                $sheet->row(3,['Rif','Nombre empresa','Email','Teléfono Local','Estado']);

                //$sheet->fromArray($empresas, null, 'A4', null, false);
                // consulta

                
               //dd($empresas);
                //recorrido
                foreach ($empresas as $emp) {


                   $row=[]; // un arreglo vacio que se va a llenar con las posicion 
                   $row[0]=$emp->rif;
                   $row[1]=$emp->razon_social;
                   $row[2]=$emp->correo;
                   $row[3]=$emp->telefono_local;
                   $row[4]=$emp->estado->estado;

                   $sheet->appendRow($row); // agrega una fila vacia al final del reporte

                }


            });
           // dd($empresas);
           
        });
        $myFile   = $myFile->string('xls'); 
        $response = array(
            'name' => $name, 
            'file' => "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64," . base64_encode($myFile), 
        );

        return response()->json($response);
        //->export('xlsx');
    }


}
