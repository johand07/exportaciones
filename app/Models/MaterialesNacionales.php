<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MaterialesNacionales extends Model
{
     protected $table = 'materiales_nacionales';
    protected $fillable = [

           'planilla_2_id',
           'codigo',
           'descripcion',
           'nombre_produc_nac',    
           'nombre_provedor',
           'incidencia_costo',
           'rif_productor',    
           'bactivo'
    ];

    public function planilla2() {

     return $this->hasOne('App\Models\Planilla2','id','planilla_2_id');

    }

    public function getMaterialNombreAttribute(){
        $mercosur = GenArancelMercosur::where('codigo',$this->codigo)->get()->last();
        $nandina = GenArancelNandina::where('codigo',$this->codigo)->get()->last();

        if(!empty($mercosur)){
            return $mercosur->descripcion;
        }

        if(!empty($nandina)){
            return $nandina->descripcion;
        }

        return "";
    }

}
