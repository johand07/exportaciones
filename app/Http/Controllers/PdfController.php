<?php

namespace App\Http\Controllers;

use App\Models\GenUsuario;
use App\Models\NotaCreditoSolicitud;
use App\Models\CatPreguntaSeg;
use App\Models\GenTipoUsuario;
use App\Models\DetUsuario;
use App\Models\Accionistas;
use App\Models\Productos;
use App\Models\GenStatus;
use App\Models\GenAgenteAduanal;
use App\Models\GenConsignatario;
use App\Models\GenSolicitud;
use App\Models\TipoSolicitud;
use App\Models\CatRegimenExport;
use App\Models\FinanciamientoSolicitud;
use App\Models\TipoInstrumento;
use App\Models\CatTipoConvenio;
use App\Models\FacturaSolicitud;
use App\Models\GenFactura;
use App\Models\GenDua;
use App\Models\GenNotaCredito;
use App\Models\FormaPago;
use App\Models\DetProdFactura;
use App\Models\DetProdTecnologia;
use App\Models\GenDVDSolicitud;
use App\Models\GenDvdNd;
use App\Models\ConsigTecno;
use App\Models\CatDocumentos;
use App\Models\GenCertificado;
use App\Models\CatTipoCafe;
use App\Models\DetProductosCertificado;
use App\Models\DetDeclaracionCertificado;
use App\Models\ImportadorCertificado;
use App\Models\BandejaAnalisisCertificado;
use App\Models\GenSolResguardoAduanero;
use App\Models\CatCargados;
use App\Models\GenSolInversionista;
use App\Models\AnalisisCalificacion;
use App\Models\FirmantesCertificado;
use App\Models\PlanillaDjir_01;
use App\Models\Estado;
use App\Models\Municipio;
use App\Models\GenAccionistasDeclaracionDjir_01;
use App\Models\PlanillaDjir_02;
use App\Models\PlanillaDjir_03;
use App\Models\GenDestinoInversionDjir_03;
use App\Models\GenSectorProductivoDjir_03;
use App\Models\Pais;
use App\Models\GenDeclaracionInversion;
use App\Models\PermisoEspecialBovino;
use App\Models\GenCertZooEu;
use App\Models\CatMediotransporte;
use App\Models\CatCondtransporte;
use App\Models\CatCertificadoEfectosDe;
use App\Models\DescripPartidaZoo;
use App\Models\GenSolCvc;
use App\Models\CatMetodoElab;
use App\Models\GenDivisa;
use App\Models\GenUnidadMedida;
use App\Models\CertExportaFacil;
use App\Models\DescripcionMercancia;
use App\Models\DocExtra;

use Session;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use PDF;
use Alert;
use Illuminate\Support\Facades\Storage;
use setasign\Fpdi\Tcpdf\Fpdi;

class PdfController extends Controller
{
    public function CrearPdf(Request $request)
    {

      $users=DetUsuario::where('bactivo',1)->get();
      $vista="ReportesPdf.ReporteUsuarios";
      $reporte=\View::make($vista,compact('users'))->render();
      $pdf=\App::make('dompdf.wrapper');
      $pdf->loadHTML($reporte)->setPaper('a4', 'landscape');
      return $pdf->stream('Reporte_Usuarios.pdf');

    }

     public function Creargrafic(Request $request)
    {   //$MES=date("m");
      //dd($request->fecha_grafico);
    $anio_actual=date('Y');
    if (empty($request->fecha_grafico)) {
      $anio=$anio_actual;
    } else {
      $anio=$request->fecha_grafico;
    }
    
        $titulo= 'Reporte de Usuarios';
        $descripcion= 'Usuarios del Sistema';
      $usr_natural=count(GenUsuario::where('cat_tipo_usuario_id',1)->get());
      $usr_juridico=count(GenUsuario::where('cat_tipo_usuario_id',2)->get());


      $Ene=count(GenUsuario::where('created_at','>=',$anio.'-01-01')->where('created_at','<=',$anio.'-01-31')->get());

        $Feb=count(GenUsuario::where('created_at','>=',$anio.'-02-01')->where('created_at','<=',$anio.'-02-31')->get());

        $Mar=count(GenUsuario::where('created_at','>=',$anio.'-03-01')->where('created_at','<=',$anio.'-03-31')->get());

        $Abr=count(GenUsuario::where('created_at','>=',$anio.'-04-01')->where('created_at','<=',$anio.'-04-31')->get());

        $May=count(GenUsuario::where('created_at','>=',$anio.'-05-01')->where('created_at','<=',$anio.'-05-31')->get());

        $Jun=count(GenUsuario::where('created_at','>=',$anio.'-06-01')->where('created_at','<=',$anio.'-06-31')->get());

        $Jul=count(GenUsuario::where('created_at','>=',$anio.'-07-01')->where('created_at','<=',$anio.'-07-31')->get());

        $Ago=count(GenUsuario::where('created_at','>=',$anio.'-08-01')->where('created_at','<=',$anio.'-08-31')->get());

        $Sep=count(GenUsuario::where('created_at','>=',$anio.'-09-01')->where('created_at','<=',$anio.'-09-31')->get());

        $Oct=count(GenUsuario::where('created_at','>=',$anio.'-10-01')->where('created_at','<=',$anio.'-10-31')->get());

        $Nov=count(GenUsuario::where('created_at','>=',$anio.'-11-01')->where('created_at','<=',$anio.'-11-31')->get());

        $Dic=count(GenUsuario::where('created_at','>=',$anio.'-12-01')->where('created_at','<=',$anio.'-12-31')->get());


      //dd($Enero);
      return view('ReportesPdf.Reportegrafic',compact('titulo','descripcion','usr_juridico','usr_natural','Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic','anio'));

    }


    public function planillaEr(Request $request){

      // dd($request->id);
      $id=$request->id;
      $gen_solicitud=GenSolicitud::find($id);
      // dd($gen_solicitud);
      Session::put('tipo_solicitud_id',$gen_solicitud->tipo_solicitud_id);
      
      $users=DetUsuario::where('gen_usuario_id',$gen_solicitud->gen_usuario_id)->where('bactivo',1)->first();

      $finanSoli=FinanciamientoSolicitud::where('gen_solicitud_id',$id)->first();
      $tipoSoli=TipoSolicitud::all();
      $catRegimen=CatRegimenExport::all();
      $tipoInst=TipoInstrumento::all();
      $tipoConv=CatTipoConvenio::all();
      $forma_pago=FormaPago::all();
      

      $fac_soli=FacturaSolicitud::where('gen_solicitud_id',$id)->get();
  
   // $factura_solicitud=array_unique($fac_soli);?

      $tipo_inst=[];
      $num_fac=[];
      $fecha_emision=[];
      $divisa=[];
      $formaPago=[];
      $facturas=[];
      $montoTotal=0;
      $montoFlete=0;
      $montoSeguro=0;
      $montoOtro=0;

      //$facturas=[];//Declaro arreglo facturas

        foreach ($fac_soli as $key=>$value) { 
          //Validar que $value->gen_factura_id NO este dentro del arreglo $facturas[]
          //empty=vacio
          
          //  Y Todo el bloque va dentro de la condicion
         
          //if ( $facturas) {

           
         // }

            $data=GenFactura::find($value->gen_factura_id);
            $tipo_inst[$key]=$data->tipo_instrumento_id;
            $num_fac[$key]=$data->numero_factura;
            $fecha_emision[$key]=$data->fecha_factura;
            $divisa[$key]=$data->divisa->ddivisa_abr;
            $formaPago[$key]=$data->forma_pago_id;
            $facturas[]=$value->gen_factura_id;
            $montoFlete=$montoFlete+$data->monto_flete;
            $montoSeguro=$montoSeguro+$data->monto_seguro;
            $montoOtro=$montoOtro+$data->otro_monto;



        }

          $tipo_inst=array_unique($tipo_inst);


   $products=DetProdFactura::whereIn('gen_factura_id',$facturas)->get();

    foreach ($products as $value) {

         $montoTotal=$montoTotal+$value->monto_total;
    }


    $factura=GenFactura::find($fac_soli[0]->gen_factura_id);
    $dua=GenDua::find($factura->gen_dua_id);
    $agenteAdua=GenAgenteAduanal::find($dua->gen_agente_aduana_id);
    $consig=GenConsignatario::find($dua->gen_consignatario_id);

    $array_notas=array();//creamos un array vacio
    //consultamos las notas de credito asociadas a la solicitud


    $notas=NotaCreditoSolicitud::where('gen_solicitud_id',$id)->get();


    //iteramos para llenar arreglo con los id de las notas de credito
    foreach($notas as $key=>$value) {
      //validar si value->gen_nota_credito_id; No esta dentro $array_notas[] es que va a llenar el arreglo; linea 216 dentro del arreglo
      $array_notas[]=$value->gen_nota_credito_id;

    }

    //consultamos notas de credito
    $notaCre=GenNotaCredito::whereIn('id',$array_notas)->get();
    //dd($notaCre);
    //hacemos suma de los montos de las notas de credito
    $monto_notaCre=GenNotaCredito::whereIn('id',$array_notas)->sum('monto_nota_credito');

    //dd($notaCre);

    (!is_null($notaCre)) ? $notaCre_valor=$monto_notaCre : $notaCre_valor=0;

    $total=$montoTotal-$notaCre_valor+$montoFlete+$montoSeguro+$montoOtro;


    $vista="ReportesPdf.PlanillaEr";

    $pdf=PDF::loadView($vista, compact('users','agenteAdua','consig','tipoSoli','catRegimen','finanSoli','tipoInst','tipoConv','id','gen_solicitud','dua','factura','tipo_inst','num_fac','fecha_emision','notaCre','divisa','forma_pago','formaPago','products','montoTotal','montoFlete','montoSeguro','montoOtro','notaCre_valor','total','fac_soli') );

     return $pdf->download('Planilla_ER_BIENES.pdf');


     }


     public function planillaTecno(Request $request){
         $id=$_GET['id'];
         $gen_solicitud=GenSolicitud::find($id);  
            
         $consi_tecno=ConsigTecno::where('gen_tipo_solicitud',$gen_solicitud->id)->first();
         $users=DetUsuario::where('gen_usuario_id',Auth::user()->id)->where('bactivo',1)->first();
         $finanSoli=FinanciamientoSolicitud::where('gen_solicitud_id',$id)->first();
         $tipoSoli=TipoSolicitud::all()->take(2);
         $catRegimen=CatRegimenExport::all();
         $tipoInst=TipoInstrumento::all();
         $tipoConv=CatTipoConvenio::all();
         $forma_pago=FormaPago::all();       
         $fac_soli=FacturaSolicitud::where('gen_solicitud_id',$id)->get();
         $tipo_inst=[];
         $num_fac=[];
         $fecha_emision=[];
         $divisa=[];
         $formaPago=[];
         $facturas=[];
         $montoTotal=0;
         $montoFlete=0;
         $montoSeguro=0;
         $montoOtro=0;
         foreach ($fac_soli as $key=>$value) {
            $data=GenFactura::find($value->gen_factura_id);
            $tipo_inst[$key]=$data->tipo_instrumento_id;
            $num_fac[$key]=$data->numero_factura;
            $fecha_emision[$key]=$data->fecha_factura;
            $divisa[$key]=$data->divisa->ddivisa_abr;
            $formaPago[$key]=$data->forma_pago_id;
            $facturas[]=$value->gen_factura_id;
            $montoFlete=$montoFlete+$data->monto_flete;
            $montoSeguro=$montoSeguro+$data->monto_seguro;
            $montoOtro=$montoOtro+$data->otro_monto;
           }
             $tipo_inst=array_unique($tipo_inst);
            if(Session::get('tipo_solicitud_id')==1)
            {
            $products=DetProdFactura::whereIn('gen_factura_id',$facturas)->get();
             foreach ($products as $value) {
            $montoTotal=$montoTotal+$value->monto_total;
            }
     }else{
       $products=DetProdTecnologia::whereIn('gen_factura_id',$facturas)->get();
       foreach ($products as $value) {
       $montoTotal=$montoTotal+$value->valor_fob;
       }
     }
       $factura=GenFactura::find($fac_soli[0]->gen_factura_id);
       $consig=GenConsignatario::find($consi_tecno->gen_id_consignatario);
     
     
////////////////////////////////////////////////////////////////////////////////////////////////
        $array_notas=array();//creamos un array vacio
        //consultamos las notas de credito asociadas a la solicitud
        $notas=NotaCreditoSolicitud::where('gen_solicitud_id',$id)->get();
        //iteramos para llenar arreglo con los id de las notas de credito
        foreach($notas as $key=>$value) {
          $array_notas[]=$value->gen_nota_credito_id;
        }
        //consultamos notas de credito
        $notaCre=GenNotaCredito::whereIn('id',$array_notas)->get();
        //dd($notaCre);
        //hacemos suma de los montos de las notas de credito
        $monto_notaCre=GenNotaCredito::whereIn('id',$array_notas)->sum('monto_nota_credito');
        //dd($notaCre);
        (!is_null($notaCre)) ? $notaCre_valor=$monto_notaCre : $notaCre_valor=0;
//////////////////////////////////////////////////////////////////////////////////////////////////
       $total=$montoTotal+$montoFlete+$montoSeguro+$montoOtro-$monto_notaCre;
       @$agenteAdua=$agenteAdua;
         $vista="ReportesPdf.PlanillaEr_tecno";
         $pdf=PDF::loadView($vista,compact('users','agenteAdua','consig','tipoSoli','catRegimen','finanSoli','tipoInst','tipoConv','id','gen_solicitud','factura','tipo_inst','num_fac','fecha_emision','notaCre','divisa','forma_pago','formaPago','products','montoTotal','montoFlete','montoSeguro','montoOtro','notaCre_valor','total','gen_solicitud') );
          return $pdf->download('Planilla_ER_TECNO.pdf');
        }




    public function planillaDvd(Request $request){

//dd($request->id);
      $idsol=Session::get('solicitud_dvd');
      $gen_solicitud=GenDVDSolicitud::where('id',$request->id)->first();
//dd($gen_solicitud->gen_factura_id);



$num_nota_credito=GenNotaCredito::where('gen_factura_id',$gen_solicitud->gen_factura_id)->get();

//dd($num_nota_credito);


      $solicitud=GenSolicitud::find($gen_solicitud->gen_solicitud_id);
      $users=DetUsuario::where('gen_usuario_id',$solicitud->gen_usuario_id)->where('bactivo',1)->first();
     // $descrip_export=GenDVDSolicitud::where('gen_usuario_id',Auth::user()->id)where('bactivo',1)->first();
      $vista="ReportesPdf.PlanillaDVD";
      $reporte=\View::make($vista,compact('users','gen_solicitud','num_nota_credito'))->render();
      $pdf=\App::make('dompdf.wrapper');
      $pdf->loadHTML($reporte)->setPaper('a4');
      return $pdf->download('Planilla_DVD.pdf');

     // return view('ReportesPdf.PlanillaDVD');
    }


    public function planillaNotaD(Request $request){


        $idsol=Session::get('dvd_nd');
//dd($request->id);

      $gen_solicitud=GenDvdNd::where('id',$request->id)->first();
      //dd($gen_solicitud);
      $genfacturaSolictud=FacturaSolicitud::where('gen_factura_id',$gen_solicitud->gen_factura_id)->first();
     // dd($genfacturaSolictud);
     $solicitud=GenSolicitud::find($genfacturaSolictud->gen_solicitud_id);
      $users=DetUsuario::where('gen_usuario_id',$solicitud->gen_usuario_id)->where('bactivo',1)->first();
     // $descrip_export=GenDVDSolicitud::where('gen_usuario_id',Auth::user()->id)where('bactivo',1)->first();


      $vista="ReportesPdf.PlanillaDVDND";
      $reporte=\View::make($vista,compact('users','gen_solicitud','genfacturaSolictud'))->render();
      $pdf=\App::make('dompdf.wrapper');
      $pdf->loadHTML($reporte)->setPaper('a4');
      return $pdf->download('Planilla_DVDND');

     // return view('ReportesPdf.PlanillaDVD');
    }


    public function planillaDvdDes(Request $request){

      $idsol=Session::get('solicitud_dvd');


      $gen_solicitud=GenDVDSolicitud::where('id',$request->id)->first();
      $users=DetUsuario::where('gen_usuario_id',Auth::user()->id)->where('bactivo',1)->first();

      $vista="ReportesPdf.PlanillaDVDDesestimiento";
      $reporte=\View::make($vista,compact('users','gen_solicitud'))->render();
      $pdf=\App::make('dompdf.wrapper');
      $pdf->loadHTML($reporte)->setPaper('a4');
      return $pdf->download('PlanillaDVDDesestimiento');
      //return view ('ReportesPdf.PlanillaDVDDesestimiento');
    }


    public function planillaDvdDesND(Request $request){

      $idsol=Session::get('solicitud_dvd');


      $gen_solicitud=GenDvdNd::where('id',$request->id)->first();
      //dd($gen_solicitud);
      $users=DetUsuario::where('gen_usuario_id',Auth::user()->id)->where('bactivo',1)->first();

      $vista="ReportesPdf.PlanillaDvdDestimientoND";
      $reporte=\View::make($vista,compact('users','gen_solicitud'))->render();
      $pdf=\App::make('dompdf.wrapper');
      $pdf->loadHTML($reporte)->setPaper('a4');
      return $pdf->download('PlanillaDVDDesestimiento');
      //return view ('ReportesPdf.PlanillaDVDDesestimiento');
    }

     public function PlanillaConsigDoc(Request $request){

      $documentos=CatDocumentos::where('bactivo',1)->get();
      $users=DetUsuario::where('gen_usuario_id',Auth::user()->id)->where('bactivo',1)->first();
      $vista="ReportesPdf.PlanillaConsigDoc";
      $reporte=\View::make($vista,compact('users','documentos'))->render();
      $pdf=\App::make('dompdf.wrapper');
      $pdf->loadHTML($reporte)->setPaper('a4');
      return $pdf->download('PlanillaDVDDesestimiento');
      //return view ('ReportesPdf.PlanillaConsigDoc');
    }




     public function planillaSolInversionista(Request $request){


         $id=$_GET['id'];
         //Me traigo todo de la solicitud
         $solicitud_de_inversionista=GenSolInversionista::find($id);

        //dd($solicitud_de_inversionista);

        //$solicitud_de_inversionista=GenSolInversionista::where('gen_usuario_id',$solicitud_de_inversionista->gen_usuario_id)->first();
       
         //Me traigo los datos de Det Usuario
         $users=DetUsuario::where('gen_usuario_id',$solicitud_de_inversionista->gen_usuario_id)->where('bactivo',1)->first();

         $datosfirmante=FirmantesCertificado::where('bactivo',1)->where('tipo_certificado','Inversiones')->first();

        if (Auth::user()->cat_tipo_usuario_id ==2) {

          $accionistas=Accionistas::where('gen_usuario_id',$solicitud_de_inversionista->gen_usuario_id)->where('bactivo',1)->get();

        }else{
          $accionistas=null;
        }
         
         $vista="ReportesPdf.planillaSolInversionista";

         $pdf=PDF::loadView($vista,compact('id','solicitud_de_inversionista','users','accionistas','datosfirmante') );

         return $pdf->stream('Planilla_Registro_Inversionista.pdf');

        }


    public function planillaRecaudosInversionista(Request $request){

      $documentosNat=CatDocumentos::where('gen_ente_id',2)->where('bactivo',1)->whereIn('id',[16,17,18])->get();
      $documentosJuri=CatDocumentos::where('gen_ente_id',2)->where('bactivo',1)->whereIn('id',[19,20,21,18,22])->get();
      $documentosSport=CatDocumentos::where('gen_ente_id',2)->where('bactivo',1)->whereIn('id',[10,11,12,13,14,15])->get();
      
      //dd($documentos);
         $vista="ReportesPdf.planillaRecaudosInversionista";
         $pdf=PDF::loadView($vista,compact('documentosNat','documentosJuri','documentosSport') );

         return $pdf->download('Planilla_Recaudos_Inversionista.pdf');

    }

    public function firmaDigital($namefile, $datosfirmante){
        $pdf = new Fpdi();
        // $pdf = new \FPDI(); //FPDI extends TCPDF
        $path = 'storage/Certificado_Inversionista/'.$namefile;
        //$path = Storage::url($namefile);
        //dd($path);
        $pages = $pdf->setSourceFile($path);
        // dd($pages);
        // ubicacion del archivo .pem
        //$certificate = 'file://'.realpath('/var/www/html/exportaciones/firma_inversiones/inversiones.pem');
        // $crt =  'file://'.realpath('/var/www/html/exportaciones/firma_inversiones/inversiones.crt');
        $certificate = 'file://'.realpath('storage/certificates/certificate.pem');
        $crt =  'file://'.realpath('storage/certificates/certificate.crt');
        // $certificate =Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix().'certificates/certificate.crt';
        // $crt = Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix().'certificates/certificate.crt';
        //dd($certificate);
        // set additional information
        $info = array(
            'Name' => 'CERTIFICADO DE POTENCIAL INVERSIONISTA EXTRANJERO VUCE ',
            'Location' => 'Caracas - Venezuela',
            'Reason' => 'CERTIFICADO DE POTENCIAL INVERSIONISTA EXTRANJERO',
            'ContactInfo' => 'http://www.vuce.gob.ve',
            );
        $author = $datosfirmante->nombre_firmante." ".$datosfirmante->apellido_firmante;
        for ($i = 1; $i <= $pages; $i++){
                $pdf->AddPage();
                $page = $pdf->importPage($i);
                $pdf->useTemplate($page, 0, 0);

                // set document signature
                $pdf->setSignature($crt, $certificate, 'vuce123456', '', 2, $info); 
                $pdf->SetAuthor($author);
                $pdf->SetTitle('CERTIFICADO DE POTENCIAL INVERSIONISTA EXTRANJERO');
                $pdf->SetSubject('CERTIFICADO DE POTENCIAL INVERSIONISTA EXTRANJERO VUCE');
                $pdf->SetKeywords('POTENCIAL INVERSIONISTA EXTRANJERO, INVERSIONISTA, INVERSIÓN'); 
                // $pdf->Image('img/firma_vice_inversiones.png', 130, 200, 25, 25, 'PNG'); 
                // $pdf->setSignatureAppearance(160, 200, 15, 15);

                // $pdf->addEmptySignatureAppearance(160, 200, 15, 15);   
        }
        // Storage::put('public/Certificado_Inversionista/'.$namefile, $pdf->output($namefile, 'I'));
        return $pdf->output($namefile, 'I');
    }


     
      public function CertificadoInversionista(Request $request){

        $id=$_GET['id'];
         //Me traigo todo de la solicitud
         $solicitud_de_inversionista=GenSolInversionista::find($id);

          //$solicitud_de_inversionista=GenSolInversionista::where('gen_usuario_id',$solicitud_de_inversionista->gen_usuario_id)->first();
         //dd($solicitud_de_inversionista);

        $users=DetUsuario::where('gen_usuario_id',$solicitud_de_inversionista->gen_usuario_id)->where('bactivo',1)->first();

        $datosfirmante=FirmantesCertificado::where('bactivo',1)->where('tipo_certificado','Inversiones')->first();

        

        $Fecha =$this->actual_date();
      //dd($documentos);
         $vista="ReportesPdf.CertificadoInversionista";
         $pdf=PDF::loadView($vista,compact('users','solicitud_de_inversionista','datosfirmante', 'Fecha') );
         $namefile = "Certificado_Inversionista_".$solicitud_de_inversionista->num_sol_inversionista.".pdf";
         Storage::put('public/Certificado_Inversionista/'.$namefile, $pdf->output());
         //$this->firmaDigital($namefile, $datosfirmante);

         return $pdf->download('Certificado_Inversionista_.pdf');




          
         // $path = storage_path('public/Certificado_Inversionista/'.$namefile);

         // return $pdf->download($path);

        }

      public function actual_date ()  
      {  
          $week_days = array ("Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado");  
          $months = array ("", "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
          setlocale(LC_TIME, 'es_ES.UTF-8');  
          $year_now = date ("Y");  
          $month_now = date ("n");  
          $day_now = date ("j");  
          $week_day_now = date ("w");  
          $date = $week_days[$week_day_now] . ", " . $day_now . " de " . $months[$month_now] . " de " . $year_now;   
          return $date;    
      } 

      public function VerificacionCertificadoInversionista(Request $request){
    
      
        $nro_solicitud= decrypt($request->nro_solicitud);

                          
        $solicitud_de_inversionista=GenSolInversionista::where('num_sol_inversionista',$nro_solicitud)->with('DetUsuario')->first();
        if(!empty($solicitud_de_inversionista)){
          $solicitud=$solicitud_de_inversionista;
        }else{
          Alert::success('Datos Invalidos!', 'No se puede generar la Verificación, la solicitud que intenta realizar es invalida.')->persistent("OK");
        }

      
     
      return view('BandejaInversionista.VerificarCertificado',compact('solicitud'));
  }
















   public function CertificadoRegistroVuce(Request $request){

        if(!empty($request->cod_empresa)){
          $cod_empresa=$request->cod_empresa;
          $users=DetUsuario::where('cod_empresa',$cod_empresa)->where('bactivo',1)->first();
        }else{
          $users=DetUsuario::where('gen_usuario_id',Auth::user()->id)->where('bactivo',1)->first();
        }

        if(empty($users)){
          $mensaje = "No se puede generar el PDF, la solicitud que intenta realizar es invalida o El numero de registro no cumple con los criterios establecidos.";
          return view('DatosJuridicos.verificarRegistro',compact('mensaje'));
        }
         
         $datosfirmante=FirmantesCertificado::where('bactivo',1)->where('tipo_certificado','Inversiones')->first();
         
         $vista="ReportesPdf.CertificadoVuce";

         $pdf=PDF::loadView($vista,compact('users','datosfirmante') );

         if(!empty($request->cod_empresa)){
          return $pdf->stream('RegistroCertificadoVuce.pdf');
        }else{
          return $pdf->download('RegistroCertificadoVuce.pdf');
        }
         

  }


  public function VerificacionRegistroInversionista(Request $request)
  {
//dd($request->num_sol_inversionista);

        if(!empty($request->num_sol_inversionista)){
          $num_sol=$request->num_sol_inversionista;

          $solicitud_de_inversionista=GenSolInversionista::where('num_sol_inversionista',$num_sol)->where('bactivo',1)->first();

        }else{
          $mensaje = "No se puede generar el PDF, la solicitud que intenta realizar es invalida o El numero de registro no cumple con los criterios establecidos.";
          return view('DatosJuridicos.verificarRegistro',compact('mensaje'));
        }

        /*if(empty($users)){
          $mensaje = "No se puede generar el PDF, la solicitud que intenta realizar es invalida o El numero de registro no cumple con los criterios establecidos.";
          return view('DatosJuridicos.verificarRegistro',compact('mensaje'));
        }*/
         
         $vista="ReportesPdf.planillaSolInversionista";

         $pdf=PDF::loadView($vista,compact('solicitud_de_inversionista') );

         if(!empty($request->num_sol_inversionista)){
          return $pdf->stream('CertificadoRegistroPotencialInversionista.pdf');
        }else{
          return $pdf->download('CertificadoRegistroPotencialInversionista.pdf');
        }
         

  }
































































     public function CertificadoReguardoAduanero(Request $request){
//dd($request->cerId);
        $gen_certificado_id = $request->cerId? $request->cerId : 0;
        $certificado= GenSolResguardoAduanero::where('id',$gen_certificado_id)->first(); 
       if(!empty($certificado)){
          $gen_usuario_id=$certificado->gen_usuario_id;
          $users=DetUsuario::where('gen_usuario_id',$gen_usuario_id)->where('bactivo',1)->first();
        }else{
          $users=DetUsuario::where('gen_usuario_id',Auth::user()->id)->where('bactivo',1)->first();
        }

        if(empty($users)){
          $mensaje = "No se puede generar el PDF, la solicitud que intenta realizar es invalida o El numero de registro no cumple con los criterios establecidos.";
          return view('DatosJuridicos.verificarRegistro',compact('mensaje'));
        }

         
       // dd($gen_certificado_id);
        
        // consulta de Certificado de origen y todos sus datos asociados

        
     // dd($certificado);
      
        $det_usuario=DetUsuario::where('gen_usuario_id',$certificado->gen_usuario_id)->first();
       // dd($det_usuario);

         $datosfirmante=FirmantesCertificado::where('bactivo',1)->where('tipo_certificado','Inversiones')->first();
         
         $vista="ReportesPdf.CertificadoReguardoAduanero";

         $pdf=PDF::loadView($vista,compact('det_usuario','datosfirmante','certificado') );

      if(!empty($request->cod_empresa)){
          return $pdf->stream('CertificadoReguardoAduanero.pdf');
        }else{
          return $pdf->download('CertificadoReguardoAduanero.pdf');
        }
         

  }



    public function AnalisisDeclaracionJO(Request $request){
    
      try{
        $nro_solicitud= decrypt($request->nro_solicitud)? decrypt($request->nro_solicitud) : 0;

      }catch(\Exception $e){
        if(!empty($request->json)){
          echo json_encode(array('error'=>'Solicitud invalida. El numero de solicitud no cumple con los criterios establecidos.'));
        }else{
          echo "No se puede generar el PDF, la solicitud que intenta realizar es invalida. El numero de solicitud no cumple con los criterios establecidos.";
          
        }exit();
      }
                        
      // consulta de la declaracion jurada de origen y todos sus datos asociados
      $modelDeclaracion= new  AnalisisCalificacion;
      $declaracion= $modelDeclaracion
                          ->where('num_solicitud',$nro_solicitud)       
                          ->with('rCriterioAnalisis.declaracionProduc.rProducto.rArancelNan')
                          ->with('rCriterioAnalisis.declaracionProduc.rProducto.rArancelMer') 
                          ->with('rCriterioAnalisis.declaracionProduc.rDeclaracionPaises')                      
                          ->first();
     if(empty($declaracion))
     {
      if(!empty($request->json)){
        echo json_encode(array('error'=>'Solicitud no encontrada'));
      }else{
        echo "No se puede generar el PDF, la solicitud que intenta acceder no se encuentra registrada.";
        
      }exit();
     }
    $vista="ReportesPdf.AnalisisDeclaracionJOP";
    $reporte=\View::make($vista,compact('declaracion')); 
    $pdf=\App::make('dompdf.wrapper');
    $pdf->loadHTML($reporte)->setPaper('letter','landscape');
    return $pdf->stream('AnalisisDeclaracionJOP');
      return view('ReportesPdf.AnalisisDeclaracionJOP',compact('declaracion'));
  }


  public function CertificadoOrigenColombiapdf(Request $request){

        $gen_certificado_id = decrypt($request->cerId)? decrypt($request->cerId) : 0;
        //dd($gen_certificado_id);
        
        // consulta de Certificado de origen y todos sus datos asociados

        $certificado= GenCertificado::where('id',$gen_certificado_id)->first(); 
        //dd($certificado);
        //
//consulta para seleccionar el tipo del certificado
           $tipocertificado=$certificado->gen_tipo_certificado_id;
          //dd($tipocertificado);


         $det_usuario=DetUsuario::where('gen_usuario_id',$certificado->gen_usuario_id)->first();
         //dd($det_usuario);

           //dd($det_usuario)
          /*if($tipocertificado == 9) {
             $detalleProduct=DB::table('det_productos_certificado as det')
                ->select('det.num_orden',//
                        'det.cantidad_segun_factura',//
                        'det.descripcion_comercial',//
                        'det.unidad_fisica',//
                        'cri.normas_criterio',//
                        'cri.numero_factura',//
                        'cri.f_fac_certificado'
                        
                        )
                ->join('det_declaracion_certificado as cri', 'det.gen_certificado_id', '=', 'cri.gen_certificado_id')//
                ->where('det.bactivo',1)
                ->where('det.gen_certificado_id',$gen_certificado_id)
                ->get();
                dd($detalleProduct);
          }*/


        $detalleProduct=DetProductosCertificado::where('gen_certificado_id',$gen_certificado_id)->get();
        //dd($detalleProduct);


          /************Consulta del detalle de Declaracion certificado para traerme todas***/

          $detdeclaracionCert=DetDeclaracionCertificado::where('gen_certificado_id',$gen_certificado_id)->get();


        //$detdeclaracionTurquia=DetDeclaracionCertificado::where('gen_certificado_id',$gen_certificado_id)->first();
          //dd($detdeclaracionCert);

        /************Consulta para traerme los datos del importador certificado para traerme todas***/

        $importadorCertificado=ImportadorCertificado::where('gen_certificado_id',$gen_certificado_id)->first();

        //dd($importadorCertificado);

        $datosfirmante=FirmantesCertificado::where('bactivo',1)->where('tipo_certificado','Inversiones','ente_firmante')->first();



      

//reenderizacion de pdf segun el tipo del certificado

        switch ($tipocertificado) {
          case 1:
            $vista="ReportesPdf.CertificadoOrigenP";
            $reporte=\View::make($vista,compact('certificado','detalleProduct','detdeclaracionCert','importadorCertificado','det_usuario','gen_certificado_id'));
            $pdf=\App::make('dompdf.wrapper');
            $pdf->loadHTML($reporte)->setPaper('a4');

            //dd($pdf);
            //Ruta de certificado de Origen//
            return $pdf->stream('CertificadoOrigenP');
            //Retorna a la vista 
            return view('ReportesPdf.CertificadoOrigenP',compact('certificado','detalleProduct','detdeclaracionCert','importadorCertificado','det_usuario','gen_certificado_id'));
          break;
          case 2:
            //$certificado='Certificado de Origen';
            $vista="ReportesPdf.CertificadoOrigenBolivia";
            $reporte=\View::make($vista,compact('certificado','detalleProduct','detdeclaracionCert','importadorCertificado','det_usuario','gen_certificado_id'));
            $pdf=\App::make('dompdf.wrapper');
            $pdf->loadHTML($reporte)->setPaper('a4');

            //dd($pdf);
            //Ruta de certificado de Origen//
            return $pdf->stream('CertificadoOrigenBolivia');
            //Retorna a la vista 
            return view('ReportesPdf.CertificadoOrigenBolivia',compact('certificado','detalleProduct','detdeclaracionCert','importadorCertificado','det_usuario','gen_certificado_id'));
          break;
          case 3:
            $vista="ReportesPdf.CertificadoOrigenChile";
            $reporte=\View::make($vista,compact('certificado','detalleProduct','detdeclaracionCert','importadorCertificado','det_usuario','gen_certificado_id'));
            $pdf=\App::make('dompdf.wrapper');
            $pdf->loadHTML($reporte)->setPaper('a4');

            //dd($pdf);
            //Ruta de certificado de Origen//
            return $pdf->stream('CertificadoOrigenChile');
            //Retorna a la vista 
            return view('ReportesPdf.CertificadoOrigenChile',compact('certificado','detalleProduct','detdeclaracionCert','importadorCertificado','det_usuario','gen_certificado_id'));
          break;

          case 4:
            $vista="ReportesPdf.CertificadoOrigenAladi";
            $reporte=\View::make($vista,compact('certificado','detalleProduct','detdeclaracionCert','det_usuario','gen_certificado_id'));
            $pdf=\App::make('dompdf.wrapper');
            $pdf->loadHTML($reporte)->setPaper('a4');

            //dd($pdf);
            //Ruta de certificado de Origen//
            return $pdf->stream('CertificadoOrigenAladi');
            //Retorna a la vista 
            return view('ReportesPdf.CertificadoOrigenAladi',compact('certificado','detalleProduct','detdeclaracionCert','det_usuario','gen_certificado_id'));
          break;

          case 5:
            $vista="ReportesPdf.CertificadoOrigMercosur";
            $reporte=\View::make($vista,compact('certificado','detalleProduct','detdeclaracionCert','det_usuario','gen_certificado_id','importadorCertificado'));
            $pdf=\App::make('dompdf.wrapper');
            $pdf->loadHTML($reporte)->setPaper('a4');

            //dd($pdf);
            //Ruta de certificado de Origen Mercosur//
            return $pdf->stream('CertificadoOrigMercosur');
            //Retorna a la vista 
            return view('ReportesPdf.CertificadoOrigMercosur',compact('certificado','detalleProduct','detdeclaracionCert','det_usuario','gen_certificado_id','importadorCertificado'));
          break;

           case 6:
            $vista="ReportesPdf.CertificadoOrigenPeru";
            $reporte=\View::make($vista,compact('certificado','detalleProduct','detdeclaracionCert','det_usuario','gen_certificado_id','importadorCertificado'));
            $pdf=\App::make('dompdf.wrapper');
            $pdf->loadHTML($reporte)->setPaper('a4');

            //dd($pdf);
            //Ruta de certificado de Origen Peru//
            return $pdf->stream('CertificadoOrigenPeru');
            //Retorna a la vista 
            return view('ReportesPdf.CertificadoOrigenPeru',compact('certificado','detalleProduct','detdeclaracionCert','det_usuario','gen_certificado_id','importadorCertificado'));
          break;


          case 7:
            $vista="ReportesPdf.CertificadoOrigenCuba";
            $reporte=\View::make($vista,compact('certificado','detalleProduct','detdeclaracionCert','det_usuario','gen_certificado_id','importadorCertificado'));
            $pdf=\App::make('dompdf.wrapper');
            $pdf->loadHTML($reporte)->setPaper('a4');

            //dd($pdf);
            //Ruta de certificado de Origen Cuba//
            return $pdf->stream('CertificadoOrigenCuba');
            //Retorna a la vista 
            return view('ReportesPdf.CertificadoOrigenCuba',compact('certificado','detalleProduct','detdeclaracionCert','det_usuario','gen_certificado_id','importadorCertificado'));
          break;

          case 8:
            $vista="ReportesPdf.CertificadoOrigenTurquia";
            $reporte=\View::make($vista,compact('certificado','detalleProduct','detdeclaracionCert','det_usuario','gen_certificado_id','importadorCertificado','datosfirmante'));
            $pdf=\App::make('dompdf.wrapper');
            $pdf->loadHTML($reporte)->setPaper('a4');

            //dd($pdf);
            //Ruta de certificado de Origen Cuba//
            return $pdf->stream('CertificadoOrigenTurquia');
            //Retorna a la vista 
            return view('ReportesPdf.CertificadoOrigenTurquia',compact('certificado','detalleProduct','detdeclaracionCert','det_usuario','gen_certificado_id','importadorCertificado','datosfirmante'));
          break;

           case 9:
            $vista="ReportesPdf.CertificadoOrigenUE";
            $reporte=\View::make($vista,compact('certificado','detalleProduct','detdeclaracionCert','det_usuario','gen_certificado_id','importadorCertificado'));
            $pdf=\App::make('dompdf.wrapper');
            $pdf->loadHTML($reporte)->setPaper('a4');

            //dd($pdf);
            //Ruta de certificado de Origen Cuba//
            return $pdf->stream('CertificadoOrigenUE');
            //Retorna a la vista 
            return view('ReportesPdf.CertificadoOrigenUE',compact('certificado','detalleProduct','detdeclaracionCert','det_usuario','gen_certificado_id','importadorCertificado'));
          break;

        }
      
    }

     public function DeclaracionJuradaInversionRealizadaP01(Request $request){

        $id=$_GET['id'];
      //dd($id);
        $solicitudDeclaracion=GenDeclaracionInversion::find($id);

        $planillaDjir01=PlanillaDjir_01::where('gen_declaracion_inversion_id',$id)->first();
       
        $estados=Estado::where('id',"<>",null)->orderBy('estado','asc')->pluck('estado','id');

        $municipios=Municipio::all()->pluck('municipio','id');
        //dd($municipios);

        $accionistas=GenAccionistasDeclaracionDjir_01::where('gen_declaracion_inversion_id',$id)->where('bactivo',1)->get();
         
        $vista="ReportesPdf.DeclaracionJuradaInversionRealizadaP01";
        $pdf=PDF::loadView($vista,compact('planillaDjir01','estados','municipios','accionistas','solicitudDeclaracion'));
        return $pdf->stream('ReportesPdf.DeclaracionJuradaInversionRealizadaP01'); 

     }





   public function DeclaracionJuradaInversionRealizadaP02(Request $request){

        $id=$_GET['id'];
      //dd($id);

        $solicitudDeclaracion=GenDeclaracionInversion::find($id);

        $planillaDjir02=PlanillaDjir_02::where('gen_declaracion_inversion_id',$id)->first();
        //dd($planillaDjir02);
        
         
        $vista="ReportesPdf.DeclaracionJuradaInversionRealizadaP02";
        $pdf=PDF::loadView($vista,compact('planillaDjir02','solicitudDeclaracion'));
        return $pdf->stream('ReportesPdf.DeclaracionJuradaInversionRealizadaP02'); 

  }







      public function DeclaracionJuradaInversionRealizadaP03(Request $request){

        $id=$_GET['id'];
      //dd($id);
        $solicitudDeclaracion=GenDeclaracionInversion::find($id);
        $planillaDjir03 = PlanillaDjir_03::where('gen_declaracion_inversion_id', $id)->first();
       
        $gen_destino_inversion_djir03=GenDestinoInversionDjir_03::where('gen_declaracion_inversion_id',$id)->get();

        $gen_sector_productivo_djir03=GenSectorProductivoDjir_03::where('gen_declaracion_inversion_id',$id)->get(); 

        $estado=Pais::all()->pluck('dpais','id');
         
         $vista="ReportesPdf.DeclaracionJuradaInversionRealizadaP03";
        $pdf=PDF::loadView($vista,compact('planillaDjir03','gen_destino_inversion_djir03','gen_sector_productivo_djir03','estado','solicitudDeclaracion'));
        return $pdf->stream('ReportesPdf.DeclaracionJuradaInversionRealizadaP03');

  }



  /**********************************CERTIFICADO*******************************************************/


  public function CertificadoInversionRealizada(Request $request){

       // $id=$_GET['id'];
    // dd($request->id);
        
        //$solicitud_de_inversionista=GenDeclaracionInversion::find($id);

          //if(!empty($request->id)){
          $id=$request->id;

          $solicitud_de_inversionista=GenDeclaracionInversion::where('id',$id)->where('bactivo',1)->first();
        /*}else{
          $solicitud_de_inversionista=GenDeclaracionInversion::where('gen_usuario_id',Auth::user()->id)->where('bactivo',1)->first();
        }*/

       // dd($solicitud_de_inversionista);

        if(empty($solicitud_de_inversionista)){
          $mensaje = "No se puede generar el PDF, la solicitud que intenta realizar es invalida o El numero de registro no cumple con los criterios establecidos.";
          return view('DatosJuridicos.verificarRegistro',compact('mensaje'));
        }

        $users=DetUsuario::where('gen_usuario_id',$solicitud_de_inversionista->gen_usuario_id)->where('bactivo',1)->first();

        $datosfirmante=FirmantesCertificado::where('bactivo',1)->where('tipo_certificado','Inversiones')->first();

         $Fecha =$this->actual_date();

         $sectorproducteconomico=GenSectorProductivoDjir_03::where('gen_declaracion_inversion_id',$id)->get();

        $vista="ReportesPdf.CertificadoDJIR";
        $pdf=PDF::loadView($vista,compact('solicitud_de_inversionista','users','datosfirmante','Fecha','sectorproducteconomico'));
        /* return $pdf->download('ReportesPdf.CertificadoDJIR');

         if(!empty($request->num_declaracion)){
          return $pdf->stream('CertificadoDJIR.pdf');
        }else{
          return $pdf->download('CertificadoDJIR.pdf');
        } */


        //$vista="ReportesPdf.CertificadoInversionista";
         //$pdf=PDF::loadView($vista,compact('users','solicitud_de_inversionista','datosfirmante', 'Fecha') );
         $namefile = "Certificado_Declaracion_Inversion_".$solicitud_de_inversionista->num_declaracion.".pdf";
         Storage::put('public/Certificado_Inversionista/'.$namefile, $pdf->output());
         //$this->firmaDigital($namefile, $datosfirmante);

         return $pdf->download('CertificadoDJIR.pdf');



 
       
  }
  /*Autorizacion de exportacion especial Bovino*/
  

  public function AutorizacionEspecialBovino(Request $request){
        // dd($request->id);
        $id=$request->id;
        
        if (isset($id)) {
          Session::put('id_autorizacion',$id);

        } else {
          $id=Session::get('id_autorizacion');
        }
        
        
        $autorizacion=PermisoEspecialBovino::find($id);
        $det_usuario=DetUsuario::where('gen_usuario_id',$autorizacion->gen_usuario_id)->first();
        $datosfirmante=FirmantesCertificado::where('bactivo',1)->where('tipo_certificado','Inversiones')->first();
        $vista="ReportesPdf.autorizacionEspecialBovino";
        $pdf=PDF::loadView($vista,compact('autorizacion','det_usuario','datosfirmante'));
        return $pdf->stream('ReportesPdf.autorizacionEspecialBovino');

  }


  public function certZooEu(Request $request){
          // dd($request->id);
          $id=$request->id;
          
          if (isset($id)) {
            Session::put('id_certZooEu',$id);

          } else {
            $id=Session::get('certZooEu');
          }
          
          
          $certZooEu=GenCertZooEu::find($id);
          //dd($certZooEu);
          $det_usuario=DetUsuario::where('gen_usuario_id',$certZooEu->gen_usuario_id)->first();
          $medioTransporte=CatMediotransporte::all();
          $CondiTransporte=CatCondtransporte::all();
          $catCertEfectosDe=CatCertificadoEfectosDe::all();
          $descriptPartidaZoo=DescripPartidaZoo::where('gen_cert_zoo_eu_id',$id)->where('bactivo',1)->get();
          //dd($descriptPartidaZoo);

          $Eu='http://registro-empresa.vuce.gob.ve/exportador/certZooEu/'.$id;
          //dd($Eu);

          $cantDescripPartida = count($descriptPartidaZoo);
          
          $vista="ReportesPdf.certZooEu";
          $pdf=PDF::loadView($vista,compact('certZooEu','det_usuario','medioTransporte','id','CondiTransporte','catCertEfectosDe','descriptPartidaZoo','Eu','cantDescripPartida'));
          return $pdf->stream('ReportesPdf.certZooEu');

    }




/************CertificadoCna*******/

    public function CertificadoVuceCna(Request $request){
//dd($request->all());
        if(!empty($request->id)){
          $solicitud_cna=GenSolResguardoAduanero::find($request->id);
          if(!empty($solicitud_cna)){
            $users=DetUsuario::where('gen_usuario_id',$solicitud_cna->gen_usuario_id)->where('bactivo',1)->first();
          }else{
            $mensaje = "No se puede generar el PDF, la solicitud que intenta realizar es invalida o El numero de registro no cumple con los criterios establecidos.";
            return view('DatosJuridicos.verificarRegistro',compact('mensaje'));
          }
          
        }else{
          $users=DetUsuario::where('gen_usuario_id',Auth::user()->id)->where('bactivo',1)->first();
        }
         
         $datosfirmante=FirmantesCertificado::where('bactivo',1)->where('tipo_certificado','Inversiones')->first();
         
         $vista="ReportesPdf.CertificadoVuceCna";

         $pdf=PDF::loadView($vista,compact('users','datosfirmante','solicitud_cna') );

         if(!empty($request->cod_empresa)){
          return $pdf->stream('CertificadoVuceCna.pdf');
        }else{
          return $pdf->download('CertificadoVuceCna.pdf');
        }
         

  }


  public function CertificadoDemandaInterna(Request $request){

        //dd(1);
//dd($request->all());
//
     $solcvc=GenSolCvc::find($request->id);
     $users=DetUsuario::where('gen_usuario_id',$solcvc->gen_usuario_id)->where('bactivo',1)->first();

       $datosfirmante=FirmantesCertificado::where('bactivo',1)->where('tipo_certificado','Corporación Venezolana de Café')->first();
         
         $vista="ReportesPdf.CertificadoDemandaInternaSatisfecha";

         $pdf=PDF::loadView($vista,compact('datosfirmante','solcvc','users') );

        
          return $pdf->stream('CertificadoDemandaInternaSatisfecha.pdf');
       
          return $pdf->download('CertificadoDemandaInternaSatisfecha.pdf');
       
         

  }



  public function CertificadoDemandaInternaSol(Request $request){

       // dd(1);

//
//$id=$_GET['id'];
     
      $solcvc=GenSolCvc::find($request->id);

      Session::put('cat_cargados_id',$solcvc->cat_cargados_id);
      Session::put('cat_tipo_cafe_id',$solcvc->cat_tipo_cafe_id);
      Session::put('cat_metodo_elab_id',$solcvc->cat_cargados_id);
      Session::put('gen_divisa_id',$solcvc->gen_divisa_id);
      Session::put('gen_unidad_medida_id',$solcvc->gen_unidad_medida_id);

        $users=DetUsuario::where('gen_usuario_id',$solcvc->gen_usuario_id)->where('bactivo',1)->first();

      

      $datosfirmante=FirmantesCertificado::where('bactivo',1)->where('tipo_certificado','Corporación Venezolana de Café')->first();
         
      
      $tipoSoli=CatCargados::all();
      $tipoSoliCafe=CatTipoCafe::all();
      $tipoSoliElab=CatMetodoElab::all();
      $tipoSoliDivisa=GenDivisa::whereIn('id',[32,18,26])->get();
      $tipoSoliUniPeso=GenUnidadMedida::whereIn('id',[10,13,32,45])->get();

    

         $vista="ReportesPdf.CertificadoDemandaInternaSol";

         $pdf=PDF::loadView($vista,compact('datosfirmante','solcvc','users','tipoSoli','tipoSoliCafe','tipoSoliElab','tipoSoliDivisa','tipoSoliUniPeso') );

        
          return $pdf->stream('CertificadoDemandaInternaSol.pdf');
       
          return $pdf->download('CertificadoDemandaInternaSol.pdf');
       
         

  }


  public function CertificadoExportaFacil(Request $request){

       // dd(1);
//dd($request->id);
//
    

        $gen_certificado_id = $request->id;


         $certificado=CertExportaFacil::where('id',$gen_certificado_id)->first(); 
       if(!empty($certificado)){
          $gen_usuario_id=$certificado->gen_usuario_id;
          $users=DetUsuario::where('gen_usuario_id',$gen_usuario_id)->where('bactivo',1)->first();
        }else{
          $users=DetUsuario::where('gen_usuario_id',Auth::user()->id)->where('bactivo',1)->first();
        }

        if(empty($users)){
          $mensaje = "No se puede generar el PDF, la solicitud que intenta realizar es invalida o El numero de registro no cumple con los criterios establecidos.";
          return view('DatosJuridicos.verificarRegistro',compact('mensaje'));
        }



   $descrip_mercancia=DescripcionMercancia::where('cert_exporta_facil_id',$certificado->id)->get(); 

   $catdocumentos=DocExtra::where('cert_exporta_facil_id',$certificado->id)->get();

//dd($descrip_mercancia);
       $datosfirmante=FirmantesCertificado::where('bactivo',1)->where('tipo_certificado','Inversiones')->first();
         
         $vista="ReportesPdf.CertificadaExportaFacil";

         $pdf=PDF::loadView($vista,compact('datosfirmante','certificado','users','descrip_mercancia','catdocumentos') );

        
          return $pdf->stream('CertificadoExportaFacil.pdf');
       
          return $pdf->download('CertificadoExportaFacil.pdf');
       
         

  }

}