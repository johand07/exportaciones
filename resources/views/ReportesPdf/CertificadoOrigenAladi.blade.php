<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>CERTIFICADO DE ORIGEN ALADI</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style type="text/css">
        .centrar{
            text-align: center;
        }

        .titulo{
            
            font-weight: bold;
        }

        .nro-correlativo{
            color: red;
            font-size: 16px;
        }

        .derecha{
            text-align: right;

        }

        .text-normal{
            font-size: 12px;
        }

        .text-titulo{
            font-size: 18px;
        }

        .text-medio{
            font-size: 11px;
        }
        .text-11{
            font-size: 11px;
        }
        .text-small{
            font-size: 10px;
        }

        .text-9{
            font-size: 8px;
        }

        .text-8{
            font-size: 8px;
        }

        .espacio-superior{
            margin-top: 10px;
        }

        .espacio-superior-small{
            margin-top: 5px;
        }

        .ancho-medio{
            width: 50%;
        }

        .ancho-full{
            width: 100%;
        }

        .ancho-medio-alto{
            width: 75%;
        }

        .ancho-90{
            width: 90%;
        }

        .table{
            border-collapse: collapse;
        }
        
        .borde{
            border: 1px solid #000;
            border-radius: 7px 7px 7px 7px;
            border-top: 1px solid black;
        }

         
        .sin-borde{
            border: none;
        }

        .top{
            vertical-align: top;
        }

        .under-dotted{    
            border-bottom: 1px dotted #000;
            text-decoration: none;
        }

        .content-1{
            width: 5%;
        }

        .content-9{
            width: 20%;
        }

        .sangria-1{
            margin-left: 28px;
        }

        .sangria-2{
            margin-left: 36px;
        }

        .sangria-3{
            margin-left: 54px;
        }

        .sangria-sub{
            margin-left: 14px;
            
        }

        
        .page-break { page-break-before: always; }

        .table-center{
            margin: 0 auto; 
        }

        .justify{
            text-align: justify;
        }

        td.justify {
            padding-left: 8px;
            padding-right: 8px; 
        }
        .lineal{
            
        }
        .lineal>div{
            max-width:91%;

        }
        .lineal>*{
           display: inline-block;
           vertical-align:top;
        }

        .sangria-lista{
            list-style-type: none;
            margin: 0;
            padding: 0;
            margin-bottom: 4px;
        }

        .sangria-lista>li{
            margin-top: 5px;
            padding-left: 10px;
            padding-right: 10px;
        }

      
    </style>
</head>
<body>
    
    <!--div class="izquierda titulo text-medio"><b style="color:red">N°</b><span class="">{{ $certificado->num_certificado}}</span></div-->
    <div class="centrar titulo text-titulo espacio-superior"><b>CERTIFICADO DE ORIGEN</b> <br></div><br>
    <div class="centrar espacio-superior">
        <table border="0" class="ancho-full">
            <tr>
                <td class="ancho-medio">
                    <div class="izquierdo text-normal titulo">(1) PAÍS EXPORTADOR: <u>{{$certificado->pais_exportador}}</u></div>
                </td>
                <td class="ancho-medio centrar">
                    <div class="derecha text-normal titulo">(2) PAÍS IMPORTADOR: <u>{{$certificado->pais_importador}}</u></div>
                </td>
            </tr>
        </table>
    </div>
    <!--Productos-->
    <div class="centrar espacio-superior borde">
        <table class="ancho-full table" style="width:100%">
            <tr class="text-normal">
                <td class="centrar top titulo borde" style="width: 12%;">Nº de <br> Orden (3)*</td>
                <td class="centrar top titulo borde" style="width: 12%;">(4) Código Arancelario</td>
                <td class="centrar top titulo borde" style="width: 76%;">(5) DENOMINACIóN DE LAS MERCADERíAS</td>
            </tr>
           
            @foreach($detalleProduct as $value)
            
            <tr class="text-normal">
                <td class="centrar top">{{$value->num_orden}}</td>
                <td class="centrar top">{{$value->codigo_arancel}}</td>
                <td class="centrar top">{{$value->denominacion_mercancia}}</td>
            </tr>
            @endforeach

            @if(count($detalleProduct)<=1)
                @for($auxConut = 0; $auxConut<=3-count($detalleProduct); $auxConut++)
                <tr class="text-normal">
                    <td class="centrar top">&nbsp;</td>
                    <td class="centrar top">&nbsp;</td>
                    <td class="centrar top">&nbsp;</td> 
                </tr>
                @endfor
            @endif
        
        </table>
    </div>
    <!--Productos-->
<!--Declaracion--><br>
    <div class="centrar titulo text-medio espacio-superior">DECLARACION DE ORIGEN</div>
    <div class="text-normal espacio-superior">DECLARAMOS que las mercaderías indicadas en el presente formulario, correspondientes a la Factura Comercial Nº <u>{{ $detdeclaracionCert[0]->numero_factura }}</u>
    cumplen con lo establecido en las Normas de Origen <u>{{ $detdeclaracionCert[0]->nombre_norma_origen }}</u> de conformidad con el siguiente desglose.</div>
<!--Detalle declaracion-->
<br>
    <div class="centrar espacio-superior borde">
        <table class="ancho-full table" style="width:100%">
            <tr class="text-medio">
                <td class="centrar top titulo borde" style="width: 15%;">Nº de <br>Orden (6)*</td><br>
                <td class="centrar top titulo borde" style="width: 80%;">(7) NORMAS **</td>
            </tr>
            @foreach($detdeclaracionCert as $value1)
            
                <tr class="text-center">
                    <td class="centrar top">{{$value1->num_orden_declaracion}}</td>
                    <td class="centrar top">{{$value1->normas_criterio}}
                </tr>
            @endforeach

            @if(count($detdeclaracionCert)<=1)
                @for($auxConut = 0; $auxConut<=3-count($detdeclaracionCert); $auxConut++)
                <tr class="text-medio">
                    <td class="centrar top">&nbsp;</td>
                    <td class="centrar top">&nbsp;</td>
                </tr>
                @endfor
            @endif
        </table>
    </div>
    <div>
        <table class="ancho-full borde">
            <tr class="text-normal">
                <td class="top titulo"><br>(8) Fecha,  <u><u>{{$certificado->fecha_emision_exportador}}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u></u><br><br>
                (9) Razón social, sello y firma del exportador o productor<br>{{ $certificado->GenUsuario->DetUsuario->razon_social}}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="derecha"><u></u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<u></u>&nbsp;&nbsp;(9) FIRMA AUTORIZADA</span></td><br>
            </tr>
        </table>
    </div>
    <div class="izquierda text-normal"><b>(10) OBSERVACIONES: ----------------------------------------------------------------------------------------------------------------------------</b><br>{{$certificado->observaciones}}<br><br></div>
    <div class="text-normal borde table justify"><b><br>&nbsp;&nbsp;(11) Certifico la veracidad de la presente declaración, que sello y firmo en la ciudad de_____________________________________</b><br><br>&nbsp;&nbsp;a los __________________________________________________________________________________________________________<br><br><br></div>
    <div class="text-normal justify">NOTAS: (*) Esta columna indica el orden en que se individualiza las mercaderías comprendidas en el presente certificado. En caso de ser insuficiente se continuara la individualización de las mercaderias en ejemplares suplementarios de este certificado numerados correlativamente.<br></div><br>
    <div class="text-normal justify">(**) En este recuadro especificar la NORMA DE ORIGEN, con que cumplen cada mercadería individualizada por su numero de orden.<br></div>
   <div class="centrar espacio-superior">
   <table class="ancho-full">
        <tr class="text-medio izquierda">
            <td class="justify text-normal"><b class="text-medio">ORIGINAL:ADUANA DE DESTINO</b><p><b>DUPLICADO: EXPORTADOR</p></b><p><b>TRIPLICADO: MINISTERIO </b></p></td>
            <td class="centrar"><b>CUADRUPLICADO:MINISTERIO</b></td>
            <td class="centrar"><b>EL FORMULARIO NO PODRA PRESENTAR RASPADURAS, TACHADURAS O ENMIENDAS.</b></td>
        </tr>
    </table>
   </div>
</body>
</html>