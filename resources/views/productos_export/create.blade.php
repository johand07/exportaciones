@extends('templates/layoutlte')
@section('content')

@component('modal_dialogo',['arancel'=>$arancel])

@slot('header') <span>Código Arancelario</span> @endslot

@slot('body')  <div id="arancel"></div>

@endslot

@slot('footer')@endslot

@endcomponent

{{Form::open(['route' =>'productos_export.store' ,'method'=>'POST','id'=>'formProductos'])}}

{{Form::hidden('metodo','Crear',['readonly'=>'true'])}}

<div class="container">


  <div class="row" id="codigo_arancelarios" >
      <div class="col-md-3"></div>
       <div class="col-md-6 text-center">

         <h3><span></span></h3>
      
         
       </div>
     <div class="col-md-3"></div>
   </div><br><br>


<table class="table table-bordered text-center" id="prod">

  <tr>
    <th>Código Arancelario </th>
    <th>Descripción Arancelaria</th>
    <th>Descripción Comercial</th>
    <th>Unidad de Medida</th>
    <th>Capacidad Operativa Anual</th>
    <th>Capacidad Instalada Anual</th>
    <th>Capacidad de Almacenamiento Anual</th>
    <th>Capacidad de Exportación Anual</th>
    <th></th>

  </tr>
  <tr id="fila">

    <td>{!!Form::text('codigo_arancel[]',null,['class'=>'form-control','data-toggle'=>"modal",'data-target'=>"#modal",'id'=>'cod_arancel','readonly'=>'true'])!!}
    </td>
    <td>{!!Form::text('descrip_arancel[]',null,['class'=>'form-control','id'=>'descrip_arancel','onkeypress'=>'return soloLetras(event)'])!!}</td>
    <td>{!!Form::text('descrip_comercial[]',null,['class'=>'form-control','id'=>'descrip_comercial','onkeypress'=>'return soloLetras(event)'])!!}</td>
    <td>{!!Form::select('gen_unidad_medida_id[]',$unidad_medida,null,['placeholder'=>'--Seleccione una Unidad de Medida--', 'class'=>'form-control','id'=>'gen_unidad_medida_id']) !!}</td>
    <td>{!!Form::text('cap_opera_anual[]',null,['class'=>'form-control','id'=>'cap_opera_anual','onkeypress'=>'return soloNumeros(event)'])!!}</td>
    <td>{!!Form::text('cap_insta_anual[]',null,['class'=>'form-control','id'=>'cap_insta_anual','onkeypress'=>'return soloNumerosletrasyPorcentaje(event)'])!!}</td>
    <td>{!!Form::text('cap_alamcenamiento[]',null,['class'=>'form-control','id'=>'cap_alamcenamiento','onkeypress'=>'return soloNumeros(event)'])!!}</td>

    <td>{!!Form::text('cap_exportacion[]',null,['class'=>'form-control','id'=>'cap_exportacion','onkeypress'=>'return soloNumeros(event)'])!!}</td>

    <td><button  name="add2" type="button" id="add-prod" value="add more" class="btn btn-success">Agregar</button></td>
  </tr>

</table>


<div class="col-md-12" align="center">

 <a href="{{url('exportador/DatosEmpresa')}}" class="btn btn-primary">Atrás</a>
 <a class="btn btn-success" href="#" onclick="validarProductos()">Guardar</a>

</div>

{{Form::close()}}

</div>


<script src="{{asset('js/ajax_v2.1.1.js')}}"></script>

<script>


$(document).ready(function (){


var i = 0;
     $('#add-prod').click(function(e){
       i++;
      var row = 'row'+i;


$('#prod tr:last').after('<tr id="row'+i+'" display="block" class="show_div"><td>{!!Form::text('codigo_arancel[]',null,['class'=>'form-control','readOnly'=>'readOnly','data-toggle'=>"modal",'data-target'=>"#modal",'id'=>'cod_arancel'])!!}</td><td>{!!Form::text('descrip_arancel[]',null,['class'=>'form-control','onkeypress'=>'return soloNumerosDouble(event)'])!!}</td><td>{!!Form::text('descrip_comercial[]',null,['class'=>'form-control','id'=>'descrip_comercial','onkeypress'=>'return soloLetras(event)'])!!}</td><td>{!!Form::select('gen_unidad_medida_id[]',$unidad_medida,null,['placeholder'=>'--Seleccione una Unidad de Medida--', 'class'=>'form-control','id'=>'gen_unidad_medida_id'])!!}</td><td>{!!Form::text('cap_opera_anual[]',null,['class'=>'form-control','id'=>'cap_opera_anual','onkeypress'=>'return soloNumeros(event)'])!!}</td><td>{!!Form::text('cap_insta_anual[]',null,['class'=>'form-control','id'=>'cap_insta_anual','onkeypress'=>'return soloNumerosletrasyPorcentaje(event)'])!!}</td><td>{!!Form::text('cap_alamcenamiento[]',null,['class'=>'form-control','id'=>'cap_alamcenamiento','onkeypress'=>'return soloNumeros(event)'])!!}</td><td>{!!Form::text('cap_exportacion[]',null,['class'=>'form-control','id'=>'cap_exportacion','onkeypress'=>'return soloNumeros(event)'])!!}</td><td><button  name="remove" type="button" id='+i+' value="" class="btn btn-danger btn-remove">x</button></td></tr>');

});

   $(document).on('click','.btn-remove',function(){
     var id_boton= $(this).attr("id");
     $("#row"+id_boton+"").remove();

});

  });

</script>






@stop
