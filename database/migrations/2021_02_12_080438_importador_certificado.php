<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ImportadorCertificado extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('importador_certificado', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('gen_certificado_id')->default(1)->index()->nullable();
            $table->foreign('gen_certificado_id')->references('id')->on('gen_certificado');
            $table->string('razon_social_importador', 100)->nullable();
            $table->string('direccion_importador', 100)->nullable();
            $table->string('pais_ciudad_importador', 100)->nullable();
            $table->string('email_importador', 150)->nullable();
            $table->string('telefono', 20)->nullable();
            $table->string('medio_transporte_importador', 100)->nullable();
            $table->string('lugar_embarque_importador', 150)->nullable();
            $table->boolean('bactivo')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('importador_certificado');
    }
}
