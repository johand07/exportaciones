<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\DetDeclaracionProduc;
use App\Models\Planilla4;
use App\Models\Productos;
use App\Http\Requests\DJOPlanilla4Request;
use Auth;
use Alert;
use Session;
use DB;
use View;
use Validator;
use Illuminate\Support\Facades\Input;
use Response;

class DJOPlanilla4Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return $this->create($request);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        
    	$productos_id= $request['producto_id'];
        $gen_declaracion_jo_id =Session::get('gen_declaracion_jo_id');

       
        // validamos que exista la declaracion 
        if(empty($gen_declaracion_jo_id)){

          Alert::warning('La solicitud que intenta realizar requiere que este asociada a una Declaracion Jurada de Origen. Seleccione o creela.','Acción Denegada1!')->persistent("OK");

          return redirect()->action('ListaDeclaracionJOController@index'); 
        }

         $det_declaracion_product=DetDeclaracionProduc::with('rProducto')->with('rPlanilla4')->where('gen_declaracion_jo_id',$gen_declaracion_jo_id)->where('id',$productos_id)->get()->last();

        // validamos que existe el producto asociado a la declaracion
        if(empty($det_declaracion_product->id)){

          Alert::warning('La solicitud que intenta realizar requiere que el producto este asociado a la Declaracion Jurada de Origen. Intente nuevamente.','Acción Denegada!')->persistent("OK");

          return redirect()->action('DeclaracionJOController@index');

        }else{
        
            Session::put('det_declaracion_product_id',$det_declaracion_product->id);
        
        }

        // Verificamos si existe datos en la planilla asociada al producto 
        if(!empty($det_declaracion_product->rPlanilla4->id)){

           return redirect()->action('DJOPlanilla4Controller@edit', ['id' => $det_declaracion_product->rPlanilla4->id]);
        }

        $producto = $det_declaracion_product->rProducto;
        $titulo= 'Declaración Jurada de Origen';
        $descripcion= 'Planilla 4';
        return view('djoplanilla4.create',compact('producto','titulo','descripcion'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DJOPlanilla4Request $request)
    {
         Alert::warning('No es posible realizar su solicitud. Intente nuevamente.','Acción Denegada!')->persistent("OK");

        DB::beginTransaction();

        $det_declaracion_product_id = Session::get('det_declaracion_product_id');
        $gen_declaracion_jo_id = Session::get('gen_declaracion_jo_id');

        if(empty($det_declaracion_product_id)){
            Alert::warning('No es posible realizar su solicitud. Intente nuevamente.','Acción Denegada!')->persistent("OK");

            return redirect()->back()->withInput();
        }

        $proceso = $request->proceso;
        if(empty($proceso)){
        	Alert::warning('Debe indicar el proceso productivo literal de producto a exportar. Intente nuevamente.','Acción Denegada!')->persistent("OK");

            return redirect()->back()->withInput();
        }

        $planilla4 = Planilla4::with('declaracionProduc')->where('det_declaracion_produc_id',$det_declaracion_product_id)->get()->last();

        if(!empty($planilla4)){

             Alert::error('Usted ya registro datos para la planilla 4 asociada al producto " '.$planilla4->declaracionProduc->rProducto->descrip_comercial.' ". Intente nuevamente.','Acción Denegada!')->persistent("OK");

            return redirect()->action('DeclaracionJOController@index');
        }

        $planilla4 = new Planilla4;
        $planilla4->det_declaracion_produc_id=$det_declaracion_product_id;
        $planilla4->proceso_produccion=$proceso;
        $planilla4->bactivo=1;

        if($planilla4->save()){

            /*****  Cambiando el estado de la planilla 4 en det declaracion product ******/
            $detDeclaracionProducModel = $planilla4->declaracionProduc;
            $detDeclaracionProducModel->estado_p4=1;
            $detDeclaracionProducModel->save();
            
            DB::commit();

        	 Alert::success('Se han guardado los datos correspondientes a la Planilla 4!','Registrado Correctamente!')->persistent("Ok");

            return redirect()->action('DeclaracionJOController@index',['djo'=>encrypt($gen_declaracion_jo_id)]);
        }else{

            DB::rollback();

        	Alert::warning('No se pudo procesar la información. Intente nuevamente.','Acción Denegada!')->persistent("OK");

            
            return redirect()->back()->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $layout= $request->layout==="false"? false: true;
          //dd($request->det_declaracion_id);

        if(!empty($request->det_declaracion_id))
        {
            
            $det_declaracion= DetDeclaracionProduc::with('rPlanilla4')->find($request->det_declaracion_id);
        
            if(empty($det_declaracion)){
                Alert::error('La planilla que intenta visualizar no esta registrada en el sistema. D1','Acción Denegada!')->persistent("OK");
                return $layout? redirect()->action('DeclaracionJOController@index') : '<div class="alert alert-danger">La planilla que intenta visualizar no esta registrada en el sistema. D1</div>';
            }

            $planilla4 = $det_declaracion->rPlanilla4;


        }else{
            $id= $request->id ;
            $planilla4 = Planilla4::with('declaracionProduc')->find($id);
        }
    
  

        /***** Validar que haya un registro de la planilla ****/
        if(empty($planilla4)){

             Alert::error('La planilla que intenta visualizar no esta registrada en el sistema.','Acción Denegada!')->persistent("OK");

            return $layout? redirect()->action('DeclaracionJOController@index') : '<div class="alert alert-danger">La planilla que intenta visualizar no esta registrada en el sistema.</div>';
        }

        /***** Validar que la planilla sea de un usuario en especifico ****/
        if($planilla4->declaracionProduc->declaracionJO->gen_usuario_id!=Auth::user()->id && Auth::user()->gen_tipo_usuario_id!=5 && Auth::user()->gen_tipo_usuario_id!=6 && Auth::user()->gen_tipo_usuario_id!=1 && Auth::user()->gen_tipo_usuario_id!=8){

            Alert::error('No tiene permisos para visualizar esta planilla.','Acción Denegada!')->persistent("OK");

           return $layout? redirect()->action('DeclaracionJOController@index') : '<div class="alert alert-danger">No tiene permisos para visualizar esta planilla.</div>';
       }

       $producto = $planilla4->declaracionProduc->rProducto;

        if($layout)
            return view('djoplanilla4.show',compact('layout','planilla4','producto'));
        else
            return View::make('djoplanilla4.show',compact('layout','planilla4','producto'))->renderSections()['content'];
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // get the planilla4
        $planilla4 = Planilla4::find($id);

        $producto = $planilla4->declaracionProduc->rProducto;
        
        // show the edit form and pass the planilla4
        return View::make('djoplanilla4.edit',compact('planilla4','producto'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(DJOPlanilla4Request $request, $id)
    {
        // validate
        // read more on validation at http://laravel.com/docs/validation
        $rules = array(
            'proceso'       => 'required',
        );

        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            Alert::error('Debe indicar el proceso productivo del producto a exportar','Acción Denegada!')->persistent("OK");
            return redirect()->back()->withInput();
        } else {
            // store
            $planilla4 = Planilla4::find($id);
            $planilla4->proceso_produccion = Input::get('proceso');
            $planilla4->bactivo = 1;
            $planilla4->estado_observacion=0;
            $planilla4->touch();

             /*****  Cambiando el estado de la planilla 4 en det declaracion product ******/
             $detDeclaracionProducModel = $planilla4->declaracionProduc;
             $detDeclaracionProducModel->estado_p4=1;
             $detDeclaracionProducModel->touch();
            // redirect
            Alert::success('Se han actualizado los datos correspondientes a la Planilla 4!','Actualizado Correctamente!')->persistent("Ok");

            return redirect()->action('DeclaracionJOController@index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

      /** Para consultar las observaciones mediante ajax **/
    /**
     * Se recibe como parametro el $id de la planilla
     * 
     */
    public function obtenerObservacion(Request $request,$id=null){

        if(!empty($request->det_declaracion_id)){
            $detDeclaracion= DetDeclaracionProduc::with('rPlanilla4')->find($request->det_declaracion_id);
            $planilla4= $detDeclaracion->rPlanilla4;
        }else{
        $planilla4 = Planilla4::find($id);
        }
        return Response::json(array('observacion'=>$planilla4->descrip_observacion));
        
    }

      /** Para guardar las observaciones mediante ajax **/
    /**
     * Se recibe como parametro el $id de la planilla
     * 
     */
    public function storeObservacion(Request $request,$id=null){

        
        if(!empty($request->det_declaracion_id)){
            $detDeclaracion= DetDeclaracionProduc::with('rPlanilla4')->find($request->det_declaracion_id);
            $planilla4= $detDeclaracion->rPlanilla4;
        }else{
            $planilla4 = Planilla4::find($id);
        }
        if(!empty($request->observacion)){
            $planilla4->descrip_observacion=$request->observacion;
            $planilla4->estado_observacion=1;
        }else{
            $planilla4->descrip_observacion=null;
            $planilla4->estado_observacion=0;
        }

        if($planilla4->touch())
            return Response::json(array('respuesta'=>'Solicitud procesada con exito','estado'=>1));
        else
            return Response::json(array('respuesta'=>'La solicitud no pudo ser procesada','estado'=>0));
        
    }
}
