<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DocAdicionalDjo extends Model
{
    protected $table = 'doc_adicional_djo';



    protected $fillable = [

           'gen_declaracion_jo_id',
           'file_1',
           'file_2',
           'file_3',
           'file_4',
           'file_5',
           'file_6',
           'estado_observacion',
           'descrip_observacion',
           'bactivo',
    ];

    protected $date =['created_at','updated_at'];

    public function GenDeclaracionJO() {

     return $this->hasOne('App\Models\GenDeclaracionJO','id','gen_declaracion_jo_id');

    }
}
