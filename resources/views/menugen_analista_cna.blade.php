<!-- Sidebar Menu -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header"><a href="#"><span class="glyphicon glyphicon-home"></span>Analista CNA</a></li>

       <li class="treeview">
          <a href="#"><i class="glyphicon glyphicon-paste"></i> <span>Bandeja de Solicitudes</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{url('/AnalistaCna/ListaAnalistCna')}}">Solicitud</a></li>
          </ul>
      </li>
      <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->