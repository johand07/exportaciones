<?php

namespace App\Http\Controllers;
use App\Models\GenUsuario;
use App\Models\DetUsuario;
use Illuminate\Http\Request;
use Alert;

class DatosAccesoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\DetUsuario  $detUsuario
     * @return \Illuminate\Http\Response
     */
    public function show(DetUsuario $detUsuario)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\DetUsuario  $detUsuario
     * @return \Illuminate\Http\Response
     */
    public function edit(DetUsuario $detUsuario, $id)
    {
        $titulo= 'Datos de Acceso';
        $descripcion= 'Actualiza tu correo de acceso';
        $DatosAcceso=DetUsuario::where('gen_usuario_id',$id)->first();

        return view('DatosJuridicos.edit_datos_acceso', compact('DatosAcceso', 'descripcion', 'titulo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\DetUsuario  $detUsuario
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DetUsuario $detUsuario, $id)
    {
        
       
        $DatosAcceso=DetUsuario::find($id);

        //dd($DatosAcceso->correo);

        $user=GenUsuario::where('email',$DatosAcceso->correo)->first();
        //dd($user->id);
        $DatosAcceso2=GenUsuario::find($user->id);
        $DatosAcceso2->email=$request->correo;
        $DatosAcceso2->email_seg=$request->correo_sec;
        $DatosAcceso2->save();

        $DatosAcceso=DetUsuario::find($id);
        $DatosAcceso->correo=$request->correo;
        $DatosAcceso->correo_sec=$request->correo_sec;
        $DatosAcceso->save();
        /*
        

        dd($user);
        // 
        */
        Alert::success('Registro Actualizado!', 'ACTUALIZACIÓN EXITOSA.')->persistent("Ok");
        return view('DatosJuridicos.botones_juridicos');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\DetUsuario  $detUsuario
     * @return \Illuminate\Http\Response
     */
    public function destroy(DetUsuario $detUsuario)
    {
        //
    }
}
