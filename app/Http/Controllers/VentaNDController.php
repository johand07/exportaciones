<?php

namespace App\Http\Controllers;


use App\Models\GenDvdNd;
use App\Models\GenNotaDebito;
use App\Models\GenFactura;
use App\Models\GenOperadorCamb;
use App\Models\GenFacturaND;
use App\Models\DocumentosDvdNdBdv;
use App\Http\Requests\GenDvdNDRequest;
use Illuminate\Http\Request;
use File;
use Session;
use Auth;
use Laracasts\Flash\Flash;
use Alert;
use Illuminate\Support\Facades\DB;

class VentaNDController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $dvdsol=GenDvdNd::where('gen_factura_id',$request->gen_factura_id)->where('gen_status_id',3)->where('tipo_solicitud_id',4)->orderby('created_at','DESC')->take(1)->first();
            $montoventa=20;
            $montoretencion=80;
        if (!empty($dvdsol)) {

            $monto_pendiente=$dvdsol->mpendiente;
            $gen_factura=GenNotaDebito::select('monto_nota_debito')->where('gen_factura_id',$request->gen_factura_id)->first();
            $monto_fob=$gen_factura->monto_nota_debito;

            $monto=array(
                  'monto_fob'  => $monto_fob,
                  'montoventa'  => $montoventa,
                  'montoretencion'  => $montoretencion,
                  'montopendiente'  => $monto_pendiente
                );
        } else {
            $gen_factura=GenNotaDebito::select('monto_nota_debito')->where('gen_factura_id',$request->gen_factura_id)->first();
            $monto_fob=$gen_factura->monto_nota_debito;

            $monto=array(
                  'monto_fob'  => $monto_fob,
                  'montoventa'  => $montoventa,
                  'montoretencion'  => $montoretencion,
                  'montopendiente'  => $monto_fob
                );

        }

        return response()->json($monto);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(GenDvdNDRequest $request)
    {
      
//dd($request->all());
// dd(count($request->ruta_doc_dvd));
        /*ramdom que te permite generar el cifrado del codigo de seguridad*/
    function generarRandom($longitud) {
        $key = '';
        $pattern = '1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $max = strlen($pattern)-1;
        for($i=0;$i < $longitud;$i++) $key .= $pattern{mt_rand(0,$max)};
        return $key;
    }
       $user=Auth::user()->id;
       $estatus=3;
       $fecha=date('Y-m-d');
       $random=generarRandom(5);
       $firma=$fecha.$user.$random;
       $cod_seguridad=md5($firma);

       if ($request->realizo_venta == 2 && empty($request->descripcion)) {
          $descripcion=$request->descripciondes;
        }else{
          $descripcion=$request->descripcion;
        } 

        if (!empty($request->gen_operador_cambiario_id)) {
          $operadorcambiario=$request->gen_operador_cambiario_id;
        }else {
          $operadorcambiario=31;
        }
        

       $monto_pendiente=$request->pendienteventa-$request->mpercibido;

       $insertdvd = new GenDvdNd;
       $insertdvd->nota_debito_id=$request->gen_solicitud_id;
       $insertdvd->tipo_solicitud_id=4;
       $insertdvd->gen_status_id=$estatus;
       $insertdvd->gen_operador_cambiario_id=$operadorcambiario;
       $insertdvd->cod_seguridad=$cod_seguridad;
       $insertdvd->fdisponibilidad=$request->fdisponibilidad;
       $insertdvd->gen_factura_id=$request->gen_factura_id;
       $insertdvd->mfob=$request->mfob;
       $insertdvd->mpercibido=$request->mpercibido;
       $insertdvd->mvendido=$request->mvendido;
       $insertdvd->mretencion=$request->mretencion;
       $insertdvd->mpendiente=$monto_pendiente;
       $insertdvd->descripcion=$descripcion;
       $insertdvd->fstatus=$fecha;
       $insertdvd->fventa_bcv=$fecha;
       $insertdvd->realizo_venta=$request->realizo_venta;
       $insertdvd->save();


      if ($monto_pendiente==0 && $request->realizo_venta == 1) {
        $dua=GenFacturaND::where('gen_factura_id',$request->gen_factura_id)->where('nota_debito_id',$request->gen_solicitud_id)->update(['bactivo' => 0, 'updated_at' => date("Y-m-d H:i:s") ]);
        //dd($dua);
      }

        $dvdsol=GenDvdNd::where('gen_factura_id',$request->gen_factura_id)->where('gen_status_id',3)->where('tipo_solicitud_id',4)->orderby('created_at','DESC')->take(1)->first();

        Session::put('dvd_nd',$dvdsol->id);


             //Guardar las imagenes

/******Congelando los doc para bdv y tesoro mientras se termina********/

/*if(isset($request->ruta_doc_dvd) && ($request->gen_operador_cambiario_id == 2 || $request->gen_operador_cambiario_id == 18)){
        $destino = '/img/docndbdv/'.Auth::user()->detUsuario->cod_empresa;
        $destinoPrivado = public_path($destino);
        
        if (!file_exists($destinoPrivado)) {
            $filesystem = new Filesystem();
$filesystem->makeDirectory($destinoPrivado, $mode = 0777, true, true); 
        }

        for( $i=0; $i <=count($request->ruta_doc_dvd)+2; $i++ ){

        if (array_key_exists($i, $request->ruta_doc_dvd)) {

            $imagen = $request->ruta_doc_dvd[$i];
            if (!empty($imagen)) {
               
               $nombre_imagen = $imagen->getClientOriginalName();
              
                $imagen->move($destinoPrivado, $nombre_imagen);
                
               
                $file = new DocumentosDvdNdBdv;

                $file->gen_dvd_nd_id      =  $insertdvd->id;
                $file->cat_documentos_id  = $request->cat_documentos_id[$i];
                $file->ruta_doc_dvd       = $destino.'/'.$nombre_imagen;
                $file->save();
                
            }
          }
      }
  }*/
    







        Alert::success('REGISTRO EXITOSO','DVD Nota Debito Agregada')->persistent("Ok");         
        return redirect('exportador/ListaVentaND/'.$request->gen_solicitud_id.'/edit');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\GenDvdNd  $genDvdNd
     * @return \Illuminate\Http\Response
     */
    public function show(GenDvdNd $genDvdNd)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\GenDvdNd  $genDvdNd
     * @return \Illuminate\Http\Response
     */
    public function edit(GenDvdNd $genDvdNd,$id)

    {
        $facturanota=GenFacturaND::where('nota_debito_id',$id )->where('bactivo',1)->first();
        //dd($facturanota);
        if (!empty($facturanota)) {
          $facturas=GenFactura::whereIn('id',[$facturanota->gen_factura_id])->pluck('numero_factura','id');
        } 

        //dd($facturas);   
          if (empty($facturas)) {
             Alert::success('Nota de Debito Vendida','Ya realizo la venta total de esta Nota de Debito')->persistent("Ok");         
            return redirect('exportador/DvdSolicitudND');
          }
          
           //preguntamos por los usuarios banco de produccion para que les muestre sus oca de prueba sino para los usuarios normales muestra los oca reales
       if (Auth::user()->id == 4|| Auth::user()->id == 5 || Auth::user()->id == 1 || Auth::user()->id == 283 || Auth::user()->id == 476) {
         $oca=GenOperadorCamb::all()->pluck('nombre_oca','id');
       } else {
         $oca=GenOperadorCamb::whereNotIn('id',[31,32,33])->pluck('nombre_oca','id');
       }
           //dd($facturasolicitud);
          $solicitud=GenNotaDebito::find($id);
          $genDvd=GenDvdNd::where('nota_debito_id',$id)->sum('mpercibido');
          //dd($solicitud);

          $mvendido=0;
          //dd($solicitud->monto_solicitud);
          
           $mvendido=$mvendido+$genDvd;
           $idsolicitud=$id;
           $pendiente=$solicitud->monto_nota_debito-$mvendido;
       return view('VentaND.create',compact('facturas','oca','solicitud','mvendido','pendiente','idsolicitud'));
          

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\GenDvdNd  $genDvdNd
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, GenDvdNd $genDvdNd)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\GenDvdNd  $genDvdNd
     * @return \Illuminate\Http\Response
     */
    public function destroy(GenDvdNd $genDvdNd)
    {
        //
    }
}
