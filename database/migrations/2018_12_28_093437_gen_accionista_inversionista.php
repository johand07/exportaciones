<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GenAccionistaInversionista extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gen_accionista_inversionista', function (Blueprint $table) {
          
          $table->engine = 'InnoDB';

          $table->increments('id');
        
          $table->unsignedInteger('gen_sol_inversionista_id')->index()->nullable();
          $table->foreign('gen_sol_inversionista_id')->references('id')->on('gen_sol_inversionista');

          $table->unsignedInteger('accionistas_id')->index()->nullable();
          $table->foreign('accionistas_id')->references('id')->on('accionistas');

          
          $table->boolean('bactivo')->default(1);
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::dropIfExists('gen_accionista_inversionista');
    }
}
