@extends('templates/layoutlte_seniat')

@section('content')
<div class="row"> 
    <div class="col-md-12">
        <div id="datos_persona1">
            <div class="panel panel-primary">
            <div class="panel-heading">CERTIFICADO DE ORIGEN</div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-6">
                            <label>Código Arancelario</label><br>
                            <strong>{{ $certificado->codigo_arancelario }}</strong><br><br><br>

                            <label>Archivo</label><br>
                            <a href="{{ asset($file->file) }}" download="Certificado" class="btn btn-primary btn-lg">Certificado de Origen</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            
                        </div>
                    </div>
                   
                </div>
            </div>
        </div>
    </div>
</div>
@stop