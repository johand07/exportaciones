<?php

namespace App\Http\Controllers;
use Illuminate\Filesystem\Filesystem;
use App\Models\PlanillaDjir_01;
use App\Models\PlanillaDjir_02;
use App\Models\PlanillaDjir_03;
use App\Models\Estado;
use App\Models\Municipio;
use App\Models\genDocDecInversion;
use App\Models\GenAccionistasDeclaracionDjir_01;
use App\Models\GenDeclaracionInversion;
use App\Models\DocumentsVariosInversiones;

use Illuminate\Http\Request;
use Auth;
use File;
use Session;
use Laracasts\Flash\Flash;
use Alert;

class Planilla1DecInversionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        if(!isset($request->file)){
            Alert::warning('Debe cargar los documentos para poder continuar con el registro','Documentos no cargados!')->persistent("Ok");
            return redirect()->back()->withInput();
        }
        switch($request->gen_tipo_empresa){
            case 1:
                $publica = 1;
                $privada = 0;
                $cap_mix = 0;
                $otro    = 0;
                break;
            case 2:
                $publica = 0;
                $privada = 1;
                $cap_mix = 0;
                $otro    = 0;
                break;
            case 4:
                $publica = 0;
                $privada = 0;
                $cap_mix = 1;
                $otro    = 0;
                break;
            case 5:
                $publica = 0;
                $privada = 0;
                $cap_mix = 0;
                $otro    = 1;
                break;
            default:
                $publica = 0;
                $privada = 0;
                $cap_mix = 0;
                $otro    = 1;
        }

        $planilla1 = new PlanillaDjir_01;
        $planilla1->gen_declaracion_inversion_id            = $request->gen_declaracion_inversion_id;
        $planilla1->gen_status_id                           = 16;
        $planilla1->razon_social_solicitante                = $request->razon_social_solicitante;
        $planilla1->rif_solicitante                         = $request->rif_solicitante;
        $planilla1->finicio_op                              = $request->finicio_op;
        $planilla1->fingreso_inversion                      = $request->fingreso_inversion;
        $planilla1->emp_privada                             = $privada;
        $planilla1->emp_publica                             = $publica;
        $planilla1->emp_mixto                               = $cap_mix;
        $planilla1->emp_otro                                = $otro;
        $planilla1->especifique_otro                        = (isset($request->especifique_otro))?$request->especifique_otro:'';
        $planilla1->registro_publico                        = $request->registro_publico;
        $planilla1->estado_emp                              = (isset($request->estado_emp))?$request->estado_emp:'';
        $planilla1->municipio_emp                           = (isset($request->municipio_emp))?$request->municipio_emp:'';
        $planilla1->numero_r_mercantil                      = $request->numero_r_mercantil;
        $planilla1->folio                                   = $request->folio;
        $planilla1->tomo                                    = $request->tomo;
        $planilla1->finspeccion                             = (isset($request->finspeccion))?$request->finspeccion:'';
        $planilla1->direccion_emp                           = $request->direccion_emp;
        $planilla1->tlf_emp                                 = $request->tlf_emp;
        $planilla1->pagina_web_emp                          = $request->pagina_web_emp;
        $planilla1->correo_emp                              = $request->correo_emp;
        $planilla1->rrss                                    = $request->rrss;
        $planilla1->emple_actual_directo                    = $request->emple_actual_directo;
        $planilla1->emple_actual_indirecto                  = $request->emple_actual_indirecto;
        $planilla1->emple_generar_directo                   = $request->emple_generar_directo;
        $planilla1->emple_generar_indirecto                 = $request->emple_generar_indirecto;
        $planilla1->capital_suscrito                        = $request->capital_suscrito;
        $planilla1->capital_apagado                         = $request->capital_apagado;
        $planilla1->num_acciones_emitidas                   = $request->num_acciones_emitidas;
        $planilla1->valor_nominal_accion                    = $request->valor_nominal_accion;
        $planilla1->fmodificacion_accionaria                = $request->fmodificacion_accionaria;
        $planilla1->finscripcion                            = $request->finscripcion;
        $planilla1->nombre_repre                            = $request->nombre_repre;
        $planilla1->apellido_repre                          = $request->apellido_repre;
        $planilla1->numero_doc_identidad                    = $request->numero_doc_identidad;
        $planilla1->numero_pasaporte_repre                  = $request->numero_pasaporte_repre;
        $planilla1->cod_postal_repre                        = $request->cod_postal_repre;
        $planilla1->rif_repre                               = $request->rif_repre;
        $planilla1->telefono_1                              = $request->telefono_1;
        $planilla1->telefono_2                              = $request->telefono_2;
        $planilla1->correo_repre                            = $request->correo_repre;
        $planilla1->direccion_repre                         = $request->direccion_repre;
        $planilla1->estado_observacion                      = (isset($request->estado_observacion))?$request->estado_observacion:0;
        $planilla1->descripcion_observacion                 = (isset($request->descripcion_observacion))?$request->estado_observacion:null;
        $planilla1->save();
        //Accionistas
        $contar = count($request->razon_social_accionista);
        for($i=0; $i<=$contar-1; $i++){
            $accionistas = new GenAccionistasDeclaracionDjir_01;
            $accionistas->gen_declaracion_inversion_id  = $request->gen_declaracion_inversion_id;
            $accionistas->razon_social_accionista   = $request->razon_social_accionista[$i];
            $accionistas->identificacion_accionista = $request->identificacion_accionista[$i];
            $accionistas->nacionalidad_accionista   = $request->nacionalidad_accionista[$i];
            $accionistas->capital_social_suscrito   = $request->capital_social_suscrito[$i];
            $accionistas->capital_social_pagado = $request->capital_social_pagado[$i];
            $accionistas->porcentaje_participacion  = $request->porcentaje_participacion[$i];
            $accionistas->save();
        }
        
        //files
        $destino = '/img/declaracioninversion/'.Auth::user()->detUsuario->cod_empresa;
        $destinoPrivado = public_path($destino);
        
        if (!file_exists($destinoPrivado)) {
            $filesystem = new Filesystem();
            $filesystem->makeDirectory($destinoPrivado, $mode = 0777, true, true); 
        }


        for( $i=1; $i <=10 ; $i++ ){

            $imagen = $request->file('file_'.$i);
            //dd($imagen);
            if (!empty($imagen)) {
                //dd($imagen);
               $nombre_imagen = $imagen->getClientOriginalName();
               // dd($destinoPrivado);
                $imagen->move($destinoPrivado, $nombre_imagen);
                $auxStr='file_'.$i;
                $$auxStr = $nombre_imagen;
               //dd($$auxStr);
            }
            

        }
            
            $docadicional = new genDocDecInversion;
            $docadicional->gen_declaracion_inversion_id = $request->gen_declaracion_inversion_id;

            
            if (!empty($file_1)) {
                $docadicional->file_1 = $destino.'/'.$file_1;
            }

            if (!empty($file_2)) {
                $docadicional->file_2 = $destino.'/'.$file_2;
            }
            if (!empty($file_3)) {
                $docadicional->file_3 = $destino.'/'.$file_3;
            }
            if (!empty($file_4)) {
                $docadicional->file_4 = $destino.'/'.$file_4;
            }
            if (!empty($file_5)) {
                $docadicional->file_5 = $destino.'/'.$file_5;
            }
            if (!empty($file_6)) {
                $docadicional->file_6 = $destino.'/'.$file_6;
            }
            if (!empty($file_7)) {
                $docadicional->file_7 = $destino.'/'.$file_7;
            }
            if (!empty($file_8)) {
                $docadicional->file_8 = $destino.'/'.$file_8;
            }
            if (!empty($file_9)) {
                $docadicional->file_9 = $destino.'/'.$file_9;
            }
            if (!empty($file_10)) {
                $docadicional->file_10 = $destino.'/'.$file_10;
            }
            
                $docadicional->bactivo=1;

                $docadicional->save();
                
            //return redirect()->action('DeclaracionJOController@create');
        // realizar validate creo falta definir quien es obligatorio y quien no
        // 
        $gen_declaracion_inversion_id=$request->gen_declaracion_inversion_id;
        // realizar insert
        // 
        if(isset($request->file)){
            for($i=0; $i <= count($request->file)-1; $i++){

                $imagen = $request->file[$i];
                $doc = new DocumentsVariosInversiones;
                $doc->cat_documentos_id = $request->cat_documentos_id[$i];
                $doc->gen_declaracion_inversion_id=$gen_declaracion_inversion_id;
                    if (!empty($imagen)) {
                            
                        $nombre_imagen = $imagen->getClientOriginalName();
                    
                        $imagen->move($destinoPrivado, $nombre_imagen);
                        //$auxStr='file_'.$i;
                        //$$auxStr = $nombre_imagen;
                    
                    }
                $doc->file =$destino.'/'.$nombre_imagen;
                $doc->save();
            }
        }else{
            Alert::warning('Debe cargar los documentos para poder continuar con el registro','Documentos no cargados!')->persistent("Ok");
            return redirect()->back()->withInput();
        }
        
        // 
        $planillaDjir01=PlanillaDjir_01::where('gen_declaracion_inversion_id',$gen_declaracion_inversion_id)->first();
        $planillaDjir02=PlanillaDjir_02::where('gen_declaracion_inversion_id',$gen_declaracion_inversion_id)->first();
        $planillaDjir03=PlanillaDjir_03::where('gen_declaracion_inversion_id',$gen_declaracion_inversion_id)->first();
            //dd($request->all());
        Alert::success('Por favor proceda a completar la siguiente Planilla (DJIR 02)!','Registrado Correctamente!')->persistent("Ok");
        return view('BandejaInversion.datos_inversion',compact('gen_declaracion_inversion_id','planillaDjir01','planillaDjir02','planillaDjir03'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PlanillaDjir_01  $planillaDjir_01
     * @return \Illuminate\Http\Response
     */
    public function show(PlanillaDjir_01 $planillaDjir_01)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\PlanillaDjir_01  $planillaDjir_01
     * @return \Illuminate\Http\Response
     */
    public function edit(PlanillaDjir_01 $planillaDjir_01, $id)
    {
        $planillaDjir01 = PlanillaDjir_01::where('gen_declaracion_inversion_id',$id)->first();
       
        //dd($planillaDjir01);

        $documentos=genDocDecInversion::where('gen_declaracion_inversion_id',$id)->first();// Uno el que pido el primero que consiga
        // dd($documentos);
        $estados=Estado::where('id',"<>",null)->orderBy('estado','asc')->pluck('estado','id');

        $municipios=Municipio::all()->pluck('municipio','id');

        $accionistas=GenAccionistasDeclaracionDjir_01::where('gen_declaracion_inversion_id',$id)->where('bactivo',1)->get();

        
        $docListDinamica1=DocumentsVariosInversiones::where('gen_declaracion_inversion_id',$id)->where('cat_documentos_id',23)->get();

        $docListDinamica2=DocumentsVariosInversiones::where('gen_declaracion_inversion_id',$id)->where('cat_documentos_id',24)->get();

         //$docListDinamica1=[];

         //$docListDinamica2=[];


        return view('BandejaInversion.editPlanilla1Inversion', compact('planillaDjir01','estados','municipios','accionistas','documentos','docListDinamica1','docListDinamica2','id'));
    }


     //Metodo para eliminar Lista Dinamica
     public function eliminarSoporte(Request $request)
    {

         //dd($_GET['id']);
       $delete=DocumentsVariosInversiones::find($_GET['id'])->delete();
       if($delete)
       {
        //dd(1);
          return "Documento Eliminado";
       }else
       {
         return "Documento No existe";
       }


    }


    public function eliminarAccionista(Request $request,$id)
    {
        //dd($_GET['id']);
       $eli_accionista=GenAccionistasDeclaracionDjir_01::find($_GET['id'])->delete();

       if($eli_accionista)
       {
        //dd(1);
          return "Accionista Eliminado";
       }else
       {
         return "Accionista No existe";
       }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\PlanillaDjir_01  $planillaDjir_01
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PlanillaDjir_01 $planillaDjir_01)
    {
        // dd($request->all());
        if(isset($request->gen_tipo_empresa)){
            switch($request->gen_tipo_empresa){
                case 1:
                    $publica = 1;
                    $privada = 0;
                    $cap_mix = 0;
                    $otro    = 0;
                    break;
                case 2:
                    $publica = 0;
                    $privada = 1;
                    $cap_mix = 0;
                    $otro    = 0;
                    break;
                case 4:
                    $publica = 0;
                    $privada = 0;
                    $cap_mix = 1;
                    $otro    = 0;
                    break;
                case 5:
                    $publica = 0;
                    $privada = 0;
                    $cap_mix = 0;
                    $otro    = 1;
                    break;
                default:
                    $publica = 0;
                    $privada = 0;
                    $cap_mix = 0;
                    $otro    = 1;
            }
        } else {
            $publica = $request->emp_privada_edit;
            $privada = $request->emp_publica_edit;
            $cap_mix = $request->emp_mixto_edit;
            $otro    =$request->emp_otro_edit;
            
        }
        if(isset($request->estado_observacion) && $request->estado_observacion == 0){
            $status=9;
            $estado_observacion=0;
        } else {
            $status=18;
            $estado_observacion=0;
        }

        /*$solDecla=GenDeclaracionInversion::where('id',$request->gen_declaracion_inversion_id)->update(['gen_status_id'=>$status,'updated_at' => date("Y-m-d H:i:s"), 'fstatus'=>date('Y-m-d') ]);*/

        $planilla1 = PlanillaDjir_01::where('gen_declaracion_inversion_id', $request->gen_declaracion_inversion_id)->first();        
        $planilla1->gen_status_id                       = $status;
        $planilla1->razon_social_solicitante            = $request->razon_social_solicitante;
        $planilla1->rif_solicitante                     = $request->rif_solicitante;
        $planilla1->finicio_op                          = $request->finicio_op;
        $planilla1->fingreso_inversion                  = $request->fingreso_inversion;
        $planilla1->emp_privada                         = $privada;
        $planilla1->emp_publica                         = $publica;
        $planilla1->emp_mixto                           = $cap_mix;
        $planilla1->emp_otro                            = $otro;
        $planilla1->especifique_otro                    = (isset($request->especifique_otro))?$request->especifique_otro:'';
        $planilla1->registro_publico                    = $request->registro_publico;
        $planilla1->estado_emp                          = (isset($request->estado_emp))?$request->estado_emp:null;
        $planilla1->municipio_emp                       = (isset($request->municipio_emp))?$request->municipio_emp:$planilla1->municipio_emp;
        $planilla1->numero_r_mercantil                  = $request->numero_r_mercantil;
        $planilla1->folio                               = $request->folio;
        $planilla1->tomo                                = $request->tomo;
        $planilla1->finspeccion                         = (isset($request->finspeccion))?$request->finspeccion:null;
        $planilla1->direccion_emp                       = $request->direccion_emp;
        $planilla1->tlf_emp                             = $request->tlf_emp;
        $planilla1->pagina_web_emp                      = $request->pagina_web_emp;
        $planilla1->correo_emp                          = $request->correo_emp;
        $planilla1->rrss                                = $request->rrss;
        $planilla1->emple_actual_directo                = $request->emple_actual_directo;
        $planilla1->emple_actual_indirecto              = $request->emple_actual_indirecto;
        $planilla1->emple_generar_directo               = $request->emple_generar_directo;
        $planilla1->emple_generar_indirecto             = $request->emple_generar_indirecto;
        $planilla1->capital_suscrito                    = $request->capital_suscrito;
        $planilla1->capital_apagado                     = $request->capital_apagado;
        $planilla1->num_acciones_emitidas               = $request->num_acciones_emitidas;
        $planilla1->valor_nominal_accion                = $request->valor_nominal_accion;
        $planilla1->fmodificacion_accionaria            = $request->fmodificacion_accionaria;
        $planilla1->finscripcion                        = $request->finscripcion;
        $planilla1->nombre_repre                        = $request->nombre_repre;
        $planilla1->apellido_repre                      = $request->apellido_repre;
        $planilla1->numero_doc_identidad                = $request->numero_doc_identidad;
        $planilla1->numero_pasaporte_repre              = $request->numero_pasaporte_repre;
        $planilla1->cod_postal_repre                    = $request->cod_postal_repre;
        $planilla1->rif_repre                           = $request->rif_repre;
        $planilla1->telefono_1                          = $request->telefono_1;
        $planilla1->telefono_2                          = $request->telefono_2;
        $planilla1->correo_repre                        = $request->correo_repre;
        $planilla1->direccion_repre                     = $request->direccion_repre;
        $planilla1->estado_observacion                  = $estado_observacion;
        $planilla1->descripcion_observacion             = (isset($request->descripcion_observacion))?$request->estado_observacion:null;
        $planilla1->save();

        //dd($request->id);
        //Accionistas
        $contar = count($request->razon_social_accionista);
        for($i=0; $i<=$contar-1; $i++){
            
            GenAccionistasDeclaracionDjir_01::updateOrCreate(
            ['id'                               => $request->id[$i]],
            ['gen_declaracion_inversion_id'     => $request->gen_declaracion_inversion_id,
            'razon_social_accionista'           => $request->razon_social_accionista[$i],
            'identificacion_accionista'         => $request->identificacion_accionista[$i],
            'nacionalidad_accionista'           => $request->nacionalidad_accionista[$i],
            'capital_social_suscrito'           => $request->capital_social_suscrito[$i],
            'capital_social_pagado'             => $request->capital_social_pagado[$i],
            'porcentaje_participacion'          => $request->porcentaje_participacion[$i]]
            
            );
        }

        //files
        $destino = '/img/declaracioninversion/'.Auth::user()->detUsuario->cod_empresa;
        $destinoPrivado = public_path($destino);
        
        if (!file_exists($destinoPrivado)) {
            $filesystem = new Filesystem();
            $filesystem->makeDirectory($destinoPrivado, $mode = 0777, true, true); 
        }


        for( $i=1; $i <=10 ; $i++ ){

            $imagen = $request->file('file_'.$i);
            //dd($imagen);
            if (!empty($imagen)) {
                //dd($imagen);
               $nombre_imagen = $imagen->getClientOriginalName();
               // dd($destinoPrivado);
                $imagen->move($destinoPrivado, $nombre_imagen);
                $auxStr='file_'.$i;
                $$auxStr = $nombre_imagen;
               //dd($$auxStr);
            }
            

        }

        //dd($request->all());

        //files

        $docadicional = genDocDecInversion::find($request->file_id);
        $docadicional->gen_declaracion_inversion_id = $request->gen_declaracion_inversion_id;

        
        if (!empty($file_1)) {
            //$auxFile1= $docadicional->file_1;
            $docadicional->file_1 = $destino.'/'.$file_1;
        }

        if (!empty($file_2)) {
            //$auxFile2= $docadicional->file_2;
            $docadicional->file_2 = $destino.'/'.$file_2;
        }
        if (!empty($file_3)) {
            //$auxFile3= $docadicional->file_3;
            $docadicional->file_3 = $destino.'/'.$file_3;
        }
        if (!empty($file_4)) {
            //$auxFile4= $docadicional->file_4;
            $docadicional->file_4 = $destino.'/'.$file_4;
        }
        if (!empty($file_5)) {
            //$auxFile5= $docadicional->file_5;
            $docadicional->file_5 = $destino.'/'.$file_5;
        }
        if (!empty($file_6)) {
            //$auxFile6= $docadicional->file_6;
            $docadicional->file_6 = $destino.'/'.$file_6;
        }
        if (!empty($file_7)) {
            //$auxFile7= $docadicional->file_7;
            $docadicional->file_7 = $destino.'/'.$file_7;
        }
        if (!empty($file_8)) {
            //$auxFile8= $docadicional->file_8;
            $docadicional->file_8 = $destino.'/'.$file_8;
        }
        if (!empty($file_9)) {
            //$auxFile9= $docadicional->file_9;
            $docadicional->file_9 = $destino.'/'.$file_9;
        }
        if (!empty($file_10)) {
            //$auxFile10= $docadicional->file_10;
            $docadicional->file_10 = $destino.'/'.$file_10;
        }
        
        if($docadicional->touch()){
            if(!empty($auxFile1)){
                if(is_file('.'.$auxFile1))
                unlink('.'.$auxFile1); //elimino el fichero
            }
            if(!empty($auxFile2)){
                if(is_file('.'.$auxFile2))
                unlink('.'.$auxFile2); //elimino el fichero
            }
            if(!empty($auxFile3)){
                if(is_file('.'.$auxFile3))
                unlink('.'.$auxFile3); //elimino el fichero
            }
            if(!empty($auxFile4)){
                if(is_file('.'.$auxFile4))
                unlink('.'.$auxFile4); //elimino el fichero
            }
            if(!empty($auxFile5)){
                if(is_file('.'.$auxFile5))
                unlink('.'.$auxFile5); //elimino el fichero
            }
            if(!empty($auxFile6)){
                if(is_file('.'.$auxFile6))
                unlink('.'.$auxFile6); //elimino el fichero
            }
            if(!empty($auxFile7)){
                if(is_file('.'.$auxFile7))
                unlink('.'.$auxFile7); //elimino el fichero
            }
            if(!empty($auxFile8)){
                if(is_file('.'.$auxFile8))
                unlink('.'.$auxFile8); //elimino el fichero
            }
            if(!empty($auxFile9)){
                if(is_file('.'.$auxFile9))
                unlink('.'.$auxFile9); //elimino el fichero
            }
            if(!empty($auxFile10)){
                if(is_file('.'.$auxFile10))
                unlink('.'.$auxFile10); //elimino el fichero
            }

        }else{
            if (!empty($file_1)) {
                if(is_file('.'.$destino.'/'.$file_1))
                unlink('.'.$destino.'/'.$file_1);
            }
            
            if (!empty($file_2)) {
                if(is_file('.'.$destino.'/'.$file_2))
                unlink('.'.$destino.'/'.$file_2);
            }
            
            if (!empty($file_3)) {
                if(is_file('.'.$destino.'/'.$file_3))
                unlink('.'.$destino.'/'.$file_3);
            }

            if (!empty($file_4)) {
                if(is_file('.'.$destino.'/'.$file_4))
                unlink('.'.$destino.'/'.$file_4);
            }

            if (!empty($file_5)) {
                if(is_file('.'.$destino.'/'.$file_5))
                unlink('.'.$destino.'/'.$file_5);
            }

            if (!empty($file_6)) {
                if(is_file('.'.$destino.'/'.$file_6))
                unlink('.'.$destino.'/'.$file_6);
            }

            if (!empty($file_7)) {
                if(is_file('.'.$destino.'/'.$file_7))
                unlink('.'.$destino.'/'.$file_7);
            }

            if (!empty($file_8)) {
                if(is_file('.'.$destino.'/'.$file_8))
                unlink('.'.$destino.'/'.$file_8);
            }

            if (!empty($file_9)) {
                if(is_file('.'.$destino.'/'.$file_9))
                unlink('.'.$destino.'/'.$file_9);
            }

            if (!empty($file_10)) {
                if(is_file('.'.$destino.'/'.$file_10))
                unlink('.'.$destino.'/'.$file_10);
            }
            

            Alert::warning('No es posible realizar su solicitud. Intente nuevamente.','Acción Denegada!')->persistent("OK");
            return redirect()->back()->withInput();
        }

        // redirect
        Alert::success('Se han actualizado los datos!','Actualizado Correctamente!')->persistent("Ok");
        $gen_declaracion_inversion_id=$request->gen_declaracion_inversion_id;
        // realizar insert

        if(isset($request->file)){
            for($i=0; $i <= count($request->file)-1; $i++){

                $imagen = $request->file[$i];
                $doc = new DocumentsVariosInversiones;
                $doc->cat_documentos_id = $request->cat_documentos_id[$i];
                $doc->gen_declaracion_inversion_id=$gen_declaracion_inversion_id;
                    if (!empty($imagen)) {
                            
                        $nombre_imagen = $imagen->getClientOriginalName();
                    
                        $imagen->move($destinoPrivado, $nombre_imagen);
                        //$auxStr='file_'.$i;
                        //$$auxStr = $nombre_imagen;
                    
                    }
                $doc->file =$destino.'/'.$nombre_imagen;
                $doc->save();
            }
        }
        // 
        // 
        $planillaDjir01=PlanillaDjir_01::where('gen_declaracion_inversion_id',$gen_declaracion_inversion_id)->first();
        $planillaDjir02=PlanillaDjir_02::where('gen_declaracion_inversion_id',$gen_declaracion_inversion_id)->first();
        $planillaDjir03=PlanillaDjir_03::where('gen_declaracion_inversion_id',$gen_declaracion_inversion_id)->first();
            //dd($request->all());
        
        return view('BandejaInversion.datos_inversion',compact('gen_declaracion_inversion_id','planillaDjir01','planillaDjir02','planillaDjir03'));
    
    }

    public function eliminarArchivo(Request $request,$id)
    {
        //dd($_GET['id']);
       $eli_archivo=DocumentsVariosInversiones::find($_GET['id'])->delete();

       if($eli_archivo)
       {
        //dd(1);
          return "Accionista Eliminado";
       }else
       {
         return "Accionista No existe";
       }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PlanillaDjir_01  $planillaDjir_01
     * @return \Illuminate\Http\Response
     */
    public function destroy(PlanillaDjir_01 $planillaDjir_01)
    {
        //
    }
}
