<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableCriterioAnalisis extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
          Schema::table('criterio_analisis', function (Blueprint $table) {
            $table->string('tr',100)->nullable()->after('tp');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('criterio_analisis', function (Blueprint $table) {
            //
            $table->dropColumn('tr');
        });
    }
}
