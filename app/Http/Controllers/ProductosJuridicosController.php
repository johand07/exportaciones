<?php

namespace App\Http\Controllers;

use \App\Models\Productos;
use \App\Models\GenArancelNandina;
use \App\Models\GenArancelMercosur;
use \App\Models\GenUnidadMedida;
use Illuminate\Http\Request;

use Auth;
use Session;
use Laracasth\Flash\Flash;
use Alert;

class ProductosJuridicosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Productos  $productos
     * @return \Illuminate\Http\Response
     */
    public function show(Productos $productos)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Productos  $productos
     * @return \Illuminate\Http\Response
     */
    public function edit(Productos $productos,$id)
    { 
        /*if($request->ajax()){

                //dd($request['arancel']);
            if($request['arancel']==1){

               // $arancel=GenArancelNandina::take(50)->get();
                return "si";

            }elseif($request['arancel']==2){

                 $arancel=GenArancelMercosur::take(50)->get();
                 return $arancel;

            }elseif($request['valor']!="") {

                if( ($request['tipoAran']==1) && ($request['valor']!="")){

                    $data=GenArancelNandina::where('codigo','like','%'.$request['valor'].'%')->orWhere('descripcion','like','%'.$request['valor'].'%')->take(50)->get();

                return $data;

                }elseif(($request['tipoAran']==2) && ($request['valor']!="")){

                    $data=GenArancelMercosur::where('codigo','like','%'.$request['valor'].'%')->orWhere('descripcion','like','%'.$request['valor'].'%')->take(50)->get();

                    return $data;

               }

            }else{
                if($request['tipoAran']==1){

                $arancel=GenArancelNandina::take(50)->get();
                 return $arancel;

                }else{

                $arancel=GenArancelMercosur::take(50)->get();
                return $arancel;

               }
            }

        }else{*/

    /************************Consulto a la tabla Gen Arancel Mercosur*************/

           $arancel=$arancel=GenArancelMercosur::all();

    /****************************Consulto la tabla de Productos**********************/   

        $productosjur=Productos::where('gen_usuario_id',Auth::user()->id)->where('bactivo',1)->get();
       
         //dd($productosjur); 
       /***********Consulta la tabla gen_unidad_medida para traerme todas las unidad de medida**********************/
         $unidad_medida=GenUnidadMedida::all()->pluck('dunidad','id');

        //dd($productos);
        
        $titulo="Registro del Exportador";
        $descripcion="Actualización de Datos";


        return view('DatosJuridicos.edit_prod_juridico',compact('titulo','descripcion','productosjur','id','arancel','unidad_medida'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Productos  $productos
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Productos $productos,$id)
    {
        
       

        $cont_productos=count($request->descrip_arancel);

            if (!empty($request->descrip_arancel[0])) {
                
                //dd($request->descrip_arancel[0]);

                for($i=0;$i<=($cont_productos-1);$i++){


                Productos::firstOrCreate(
                ['id'=>$request->id[$i]],
                ['gen_usuario_id'=>Auth::user()->id,
                 'cod_arancel'=>$request->cod_arancel[$i],
                 'descrip_arancel'=>$request->descrip_arancel[$i],
                 'descrip_comercial'=>$request->descrip_comercial[$i],
                 'gen_unidad_medida_id'=>$request->gen_unidad_medida_id[$i],
                 'cap_opera_anual'=>$request->cap_opera_anual[$i],
                 'cap_insta_anual'=>$request->cap_insta_anual[$i],
                 'cap_alamcenamiento'=>$request->cap_alamcenamiento[$i],
                 'bactivo'=>1]

                   );

                }

            }
  
       Alert::success('ACTUALIZACIÓN EXISTOSA','Productos Actualizados')->persistent("Ok");
       return redirect()->action('ProductosJuridicosController@index');

    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Productos  $productos
     * @return \Illuminate\Http\Response
     */
    public function destroy(Productos $productos)
    {
        //
    }
}
