<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GenAnticipo extends Model

{
	protected $table = 'gen_anticipo';

	protected $fillable = [

           'gen_usuario_id',
           'gen_consignatario_id',
           'gen_divisa_id',
           'nro_anticipo',
           'fecha_anticipo',
           'nro_doc_comercial',
           'monto_anticipo',
           'monto_total_ant',
           'observaciones',
           'bactivo',
    ];

     protected $date =['created_at','updated_at'];

     public function Consignatario() {

      return $this->hasOne('App\Models\GenConsignatario','id','gen_consignatario_id');

    
    }

    public function divisa() {

      return $this->hasOne('App\Models\GenDivisa','id','gen_divisa_id');

    
    }

    public function usuario() {

      return $this->hasOne('App\Models\GenUsuario','id','gen_usuario_id');

    
    }


    
}
