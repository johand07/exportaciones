<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DocCertRegSeniat extends Model
{
    protected $table = 'doc_cert_reg_seniat';
    protected $fillable = [

           'id',
           'gen_cert_reg_seniat_id',
           'file',
           'bactivo',
    ];

    protected $date =['created_at','updated_at'];
   
        public function GenCertRegSeniat() {
   
        return $this->hasOne('App\Models\GenCertRegSeniat','id','gen_cert_reg_seniat_id');
   
       }
}
