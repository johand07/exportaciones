<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BandejaAnalistaResguardo extends Model
{
    protected $table = 'bandeja_analista_resguardo';
    
    protected $fillable = [
        'gen_sol_resguardo_aduanero_id',
        'gen_usuario_id',
        'gen_status_id',
        'fstatus',
        'bactivo',
    ];

    public function rGenSolResguardo()
    {
        return $this->hasOne('App\Models\GenSolResguardoAduanero','id','gen_sol_resguardo_aduanero_id');
    }

    public function rGenUsuario()
    {
        return $this->hasOne('App\Models\GenUsuario','id','gen_usuario_id');
    }

    public function rGenStatus()
    {
        return $this->hasOne('App\Models\GenStatus','id','gen_status_id');
    }
}
