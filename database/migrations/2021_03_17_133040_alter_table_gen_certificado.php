<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableGenCertificado extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('gen_certificado', function (Blueprint $table) {
            $table->string('rif_productor')->nullable()->after('gen_tipo_certificado_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('gen_certificado', function (Blueprint $table) {
            $table->dropColumn('rif_productor');
        });
    }
}
