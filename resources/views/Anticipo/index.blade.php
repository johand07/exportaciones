@extends('templates/layoutlte')
@section('content')

<div class="row">
  <div class="col-md-12">
    <div class="row">
      <div class="col-md-10"></div>
      <div class="col-md-2">
        <a class="btn btn-primary" href="{{url('exportador/Anticipo/create')}}">
          <span class="glyphicon glyphicon-file" title="Agregar" aria-hidden="true"></span>
           Registro de Anticipo
        </a>
        
      </div>
    </div>
    <table id="listaAnticipo" class="table table-striped table-bordered" style="width:100%">
            <thead>
                <tr>
                    <th>Número de Anticipo</th>
                    <th>Fecha Anticipo</th>
                    <th>Consignatario</th>
                    <th>Divisa</th>
                    <th>Número Documento Comercial</th>
                    <th>Monto anticipo</th>
                    <th>Monto total anticipo</th>
                    <th>Observaciones </th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody>
            	@foreach($anticipo as $ant)               	
                <tr>
                    <td>{{$ant->nro_anticipo}}</td>
                    <td>{{$ant->fecha_anticipo}}</td>
                    <td>{{$ant->Consignatario->nombre_consignatario}}</td>
                    <td>{{$ant->divisa->ddivisa_abr}}</td>
                    <td>{{$ant->nro_doc_comercial}}</td>
                    <td>{{$ant->monto_anticipo}}</td>
                    <td>{{$ant->monto_total_ant}}</td>
                    <td>{{$ant->observaciones}}</td>


                 <td>
                    <!--Ruta y metodo-->
                    <a href="{{route('Anticipo.edit',$ant->id)}}" class="btn btn-primary btn-sm"><i class="glyphicon glyphicon-edit"></i></a>
                    <!--Boton eliminar-->
                    <input id="token" type="hidden" name="_token" value="{{ csrf_token() }}">

                       <a href="#" class="btn btn-danger btn-sm" title="Eliminar" onclick="return eliminar('Anticipo/',{{$ant->id}},'listaAnticipo')"> <i class="glyphicon glyphicon-trash"></i> </a> 

                    </td>                                        
                </tr>
                @endforeach
            </tbody>
        </table>
  </div>
</div>

@stop
