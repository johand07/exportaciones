<?php

namespace App\Http\Controllers;
use App\Models\Productos;
use App\Models\GenUsuario;
use App\Models\GenArancelMercosur;
use Session;
use Auth;
use Laracasts\Flash\Flash;
use Alert;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class ExportadorController extends Controller
{
    protected function index(Request $request){
        $exportaGanado = 0;
        $prod_users = Productos::where('gen_usuario_id', Auth::user()->id)->whereIn('codigo',['0102.29.90.90', '0102.29.11.00'])->get();
        $cont = count($prod_users);
    	if (!empty($prod_users) && $cont > 0) {
            $exportaGanado = 1;
        }
        // dd($prod_users);
        return view('usuario.index', compact('exportaGanado'));       
    }
}
