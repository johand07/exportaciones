@if(isset($planillaDjir01))
<div class="row">
	<div class="col-md-12">
		<div class="col-md-6">
		@if($planillaDjir01->gen_status_id == 13)
			<div class="row">
                <div class="col-md-12">
                    <div class="alert alert-dismissible alert-warning">
                      <button type="button" class="close" data-dismiss="alert">&times;</button>
                      <h4 class="alert-heading">Solicitud de anulacion.!</h4>
                      <p class="mb-0"><h4>{{$planillaDjir01->descripcion_observacion}}</h4></p>
                    </div>
                </div>
            </div>
		@endif
		</div>
		<div class="col-md-6 text-right">
			<h3>Estatus: <span class="text-success">{{$planillaDjir01->genStatus->nombre_status}}</span></h3>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="col-md-6">
			<button type="button" class="btn btn-warning btn-lg text-right" data-toggle="modal" data-target="#doc_solicitantePDIR01">Requisitos del solicitante</button>
		</div>

		<div class="col-md-6">
			<button type="button" class="btn btn-primary btn-lg text-right" data-toggle="modal" data-target="#doc_emp_receptoraPDIR01">Requisitos de la empresa receptora</button>
		</div>
	</div>
</div>
<div class="panel panel primary">
	<div class="panel-heading">
		<div class="panel-body">
		<div class="row">
			<div class="col-md-6">
         		<h4><b style="color:#337AB7">Tipo de Persona:</b></h4>
          		<b>{{ $solicitudDeclaracion->CatTipoUsuario->nombre}}</b>
        	</div>
			
			<div class="col-md-3"><!-- Traerme la consulta con el numero de solicitud-->
				<h4><b style="color:#337AB7">Nº Solicitud:</b></h4>
				<b>{{$solicitudDeclaracion->num_declaracion}}</b>
			</div>
			<div class="col-md-3"><!-- Traerme la consulta con la fecha de solicitud-->
				<h4><b style="color:#337AB7">Fecha Solicitud:</b></h4>
				<b>{{$solicitudDeclaracion->created_at}}</b>
			</div>
		</div>
		<br><br>
		<div class="row">
			<div class="panel panel-primary">
				<div class="panel-heading"><h4>Información General</h4></div>
			</div>
			<div class="row">
				<div  class="col-md-12">
					<div class="col-md-6">
						<div class="form-group">
		                  {!! Form::label('','Nombre o razón social del solicitante')!!}<br>
		                  {{$planillaDjir01->razon_social_solicitante ? $planillaDjir01->razon_social_solicitante : ''}}
		                 
		                 
		                </div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
		                  {!! Form::label('','N° Registro de Información Fiscal (R.I.F)')!!}<br>
		                  {{$planillaDjir01->rif_solicitante ? $planillaDjir01->rif_solicitante : ''}}
		                 
		                </div>
					</div>
				</div>
			</div>
		</div>

		<div class="row">		
			<div class="panel panel-primary">
			    <div class="panel-heading"><h4>Datos de Ingreso de Inversión Extranjera</h4></div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-6">
						<div class="form-group">
							{!! Form::label('','Fecha de inicio de operaciones') !!}<br>
							{{$planillaDjir01->finicio_op ? $planillaDjir01->finicio_op : ''}}
							
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							{!! Form::label('','Fecha de ingreso de la Inversión') !!}<br>
							{{$planillaDjir01->fingreso_inversion ? $planillaDjir01->fingreso_inversion : ''}}
							
							
						</div>
					</div>
				</div>
			</div><br>

			<div class="row">
				<div class="col-md-12">
					<center>{!! Form::label('', 'Tipo de Empresa Receptora de IED') !!}</center>
					<div class="form-group">
						@if(isset($planillaDjir01->emp_privada) && $planillaDjir01->emp_privada == 1)
							<div>Empresa Privada</div>
						@elseif(isset($planillaDjir01->emp_publica) && $planillaDjir01->emp_publica == 1 )
							<div>Empresa Pública</div>

						@elseif(isset($planillaDjir01->emp_mixto) && $planillaDjir01->emp_mixto == 1 )
							<div>Empresa Capital mixto</div>
						@else
							<div>Otro: {{$planillaDjir01->especifique_otro}}</div>
						@endif
						<br>
					</div>
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-3">
						<div class="form-group">
	                 	 {!! Form::label('','Dirección de la empresa:')!!}<br>
	                 	{{$planillaDjir01->direccion_emp ? $planillaDjir01->direccion_emp : ''}}
	                 	
	                	</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
	                 	 {!! Form::label('','Teléfono')!!}<br>
	                 	 {{$planillaDjir01->tlf_emp ? $planillaDjir01->tlf_emp : ''}}
	                 	
	                	</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
	                 	 {!! Form::label('','Página Web')!!}<br>
	                 	 {{$planillaDjir01->pagina_web_emp ? $planillaDjir01->pagina_web_emp : ''}}
	                 	</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
	                 	 {!! Form::label('','Correo electrónico ')!!}<br>
	                 	 {{$planillaDjir01->correo_emp ? $planillaDjir01->correo_emp : ''}}
	                	</div>
					</div>
				</div>
			</div><br><br>

			<div class="row">
				<div class="col-md-12">
					<div class="col-md-3">
						<div class="form-group">
	                 	 {!! Form::label('','RR.SS')!!}<br>
	                 	 {{$planillaDjir01->rrss ? $planillaDjir01->rrss : ''}}
	                 	
	                	</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
	                 	 {!! Form::label('','Registro Público')!!}<br>
	                 	 {{$planillaDjir01->registro_publico ? $planillaDjir01->registro_publico : ''}}
	                	</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
	                 	 <label>Estado</label><br>
	                 	{{$planillaDjir01->estado ? $planillaDjir01->estado->estado: ''}} 

	               	</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
	                 	 <label>Municipio</label><br>
	                 	{{$planillaDjir01->municipio ? $planillaDjir01->municipio->municipio : ''}}
	                 	
	                 
	                	</div>
					</div>
				</div>
			</div><br><br>
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-3">
						<div class="form-group">
	                 	 {!! Form::label('','Inscrito bajo el Número')!!}<br>
	                 	 {{$planillaDjir01->numero_r_mercantil ? $planillaDjir01->numero_r_mercantil : ''}}
	                 	 
	                	</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
	                 	 {!! Form::label('','Folio')!!}<br>
	                 	 {{$planillaDjir01->folio ? $planillaDjir01->folio : ''}}
	                 	
	                	</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
	                 	 {!! Form::label('','Tomo')!!}<br>
	                 	 {{$planillaDjir01->tomo ? $planillaDjir01->tomo : ''}}
	                 	
	                	</div>
					</div>
					<div class="col-md-3">
	                	<div class="form-group">
						{!! Form::label('','Fecha de Inscripción') !!}<br>	
						{{$planillaDjir01->finspeccion ? $planillaDjir01->finspeccion : ''}}
							
						</div>
					</div>
				</div>
			</div><br><br>
			<div class="row">
				<div class="col-md-12">
					<center>
	    				{!! Form::label('','Número de empleos actuales')!!}
	   				</center><br>
					<div class="col-md-6">
						<div class="form-group">
	                 	 {!! Form::label('','Directos ')!!}<br>
	                 	  {{$planillaDjir01->emple_actual_directo ? $planillaDjir01->emple_actual_directo : ''}}

	                 	 
	                	</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
	                 	 {!! Form::label('','Indirectos')!!}<br>
	                 	 {{$planillaDjir01->emple_actual_indirecto ? $planillaDjir01->emple_actual_indirecto : ''}}
	                 	
	                	</div>
					</div>
				</div>
			</div><br>
			<div class="row">
				<div class="col-md-12">
					<center>
						{!! Form::label('','Empleos  por generar')!!}
					</center><br>	
					<div class="col-md-6">
						<div class="form-group">
		             	 {!! Form::label('','Directos ')!!}<br>
		             	{{$planillaDjir01->emple_generar_directo ? $planillaDjir01->emple_generar_directo : ''}}
		             	
		            	</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
		             	 {!! Form::label('','Indirectos')!!}<br>
		             	 {{$planillaDjir01->emple_generar_indirecto ? $planillaDjir01->emple_generar_indirecto : ''}}
		             	
		            	</div>
					</div>
				</div>
			</div><br><br>
		</div>
		<div class="row">
			<div class="panel panel-primary">
				<div class="panel-heading"><h4>Capital Social de la Empresa</h4></div>
			</div>
			
		</div>

		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
		        	{!! Form::label('','Suscrito Bs')!!}<br>
		        	{{$planillaDjir01->capital_suscrito ? $planillaDjir01->capital_suscrito : ''}}

		           
		        </div>	
			</div>
			<div class="col-md-6">
				<div class="form-group">
		        	{!! Form::label('','Pagado Bs')!!}<br>
		        	{{$planillaDjir01->capital_apagado ? $planillaDjir01->capital_apagado : ''}}
		            
		        </div>	
			</div>
		
		</div><br>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
		        	{!! Form::label('','Número de acciones emitidas')!!}<br>
		        	{{$planillaDjir01->num_acciones_emitidas ? $planillaDjir01->num_acciones_emitidas : ''}}
		        	
		            
		        </div>	
			</div>
			<div class="col-md-6">
				<div class="form-group">
		        	{!! Form::label('','Valor nominal de cada acción Bs')!!}<br>
		        	{{$planillaDjir01->valor_nominal_accion ? $planillaDjir01->valor_nominal_accion : ''}}
		           
		        </div>	
			</div>
		</div><br>
			
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					{!! Form::label('','Fecha de la última modificación de la composición accionaria y capital social') !!}<br>
					{{$planillaDjir01->fmodificacion_accionaria ? $planillaDjir01->fmodificacion_accionaria : ''}}
					
				</div>	
			</div>
			<div class="col-md-6">
				<div class="form-group">
					{!! Form::label('','Fecha de Inscripción') !!}<br>
					{{$planillaDjir01->finscripcion ? $planillaDjir01->finscripcion : ''}}
					
				</div>
			</div>
		</div><br>
		<div class="row">
			<div class="panel panel-primary">
				<div class="panel-heading"><h4>Accionistas de la Empresa Receptora de Inversión</h4></div>
			</div>
		</div>
		<div class="row">
			<table class="table table-bordered text-center" id="accionistas">

                     <thead>
	                      <th>Nombre o Razon Social</th>
	                      <th>Identificación (C.I., Pasaporte, ID)</th>
	                      <th>Nacionalidad</th>
	                      <th>Capital Social suscrito</th>
	                      <th>Capital Social Pagado</th>
	                      <th>% de participación dentro del capital social </th>
                   
                      </thead>
                    <tbody>
                    @if(isset($accionistas))
                   	 	@foreach ($accionistas as $key => $accionistasEmp)
	                    <tr class="text-center">
	                    	 
	                    	<td>{{$accionistasEmp->razon_social_accionista}}</td>

	                       	<td>{{$accionistasEmp->identificacion_accionista}}</td>

	                      	<td>{{$accionistasEmp->nacionalidad_accionista}}</td>

	                      	<td>{{$accionistasEmp->capital_social_suscrito}}</td>

	                       	<td>{{$accionistasEmp->capital_social_pagado}}</td>

	                        <td>{{$accionistasEmp->porcentaje_participacion}}</td>
	   
	                    </tr> 
	                	@endforeach
	                @endif
                    </tbody>
        	</table>
        </div>
        <div class="row">
			<div class="panel panel-primary">
				<div class="panel-heading"><h4>Datos del Representante Legal o Apoderado</h4></div>
			</div>
		</div>
		<div class="row">
          		<div class="col-md-4">
	      			<div class="form-group">
	             	 {!! Form::label('','Nombres')!!}<br>
	             	 {{$planillaDjir01->nombre_repre}}
	             	
	            	</div>
          		</div>
          		<div class="col-md-4">
          			<div class="form-group">
                 	 {!! Form::label('','Apellidos')!!}<br>
                 	 {{$planillaDjir01->apellido_repre}}
                 	 
                	</div>
          		</div>
          		<div class="col-md-4">
          			<div class="form-group">
                 	 {!! Form::label('','N° de Documento de Identidad')!!}<br>
                 	 {{$planillaDjir01->numero_doc_identidad}}
                 	
                	</div>
          		</div>
          	</div><br>
          	<div class="row">
          		<div class="col-md-4">
	      			<div class="form-group">
	             	 {!! Form::label('','N° de Pasaporte')!!}<br>
	             	 {{$planillaDjir01->numero_pasaporte_repre ? $planillaDjir01->numero_pasaporte_repre : ''}}
	             	 
	            	</div>
          		</div>
          		<div class="col-md-4">
          			<div class="form-group">
                 	 {!! Form::label('','Código postal ')!!}<br>
                 	 {{$planillaDjir01->cod_postal_repre ? $planillaDjir01->cod_postal_repre : ''}}
                	</div>
          		</div>
          		<div class="col-md-4">
          			<div class="form-group">
                 	 {!! Form::label('','N° Registro de Información Fiscal (R.I.F) del Apoderado')!!}<br>
                 	  {{$planillaDjir01->rif_repre ? $planillaDjir01->rif_repre : ''}}
                 	 
                	</div>
          		</div>
          	</div>
          	<div class="row">
          		<div class="col-md-3">
	      			<br><div class="form-group">
	             	 {!! Form::label('','Teléfono 1')!!}<br>
	             	 {{$planillaDjir01->telefono_1 ? $planillaDjir01->telefono_1 : ''}}
	             
	            	</div>
          		</div>
          		<div class="col-md-3">
	      			<br><div class="form-group">
	             	 {!! Form::label('','Teléfono 2')!!}<br>
	             	 {{$planillaDjir01->telefono_2 ? $planillaDjir01->telefono_2 : ''}}
	             	
	            	</div>
          		</div>
          		<div class="col-md-3">
          			<div class="form-group">
                 	 {!! Form::label('','Correo electrónico donde desea recibir las notificaciones')!!}<br>
                 	  {{$planillaDjir01->correo_repre}}
                
                	</div>
          		</div>
          		<div class="col-md-3">
          			<div class="form-group">
                 	 {!! Form::label('','Dirección donde recibirá las notificaciones')!!}<br>
                 	   {{$planillaDjir01->correo_repre ? $planillaDjir01->correo_repre : ''}}
                 	 
                	</div>
          		</div>
          	</div><br><br><br>
{{Form::model($solicitudDeclaracion,['route'=>['ListaCoordInvExtjera.update',$solicitudDeclaracion->id],'method'=>'PATCH','id'=>'formanalistaplanilla1'])}}
        <div class="row">
      	<div class="col-md-12">
      	{{Form::hidden('planillaDec',1)}}
      		<div class="form-group">
      			<label>Estatus de Planilla</label><br>
      			{!!Form::select('gen_status_id',$estado_observacion_planilla1,null,['placeholder'=>'Seleccione','class'=>'form-control','id'=>'gen_status_id_PDIJ01','onchange'=>'validarObsPDIJ01()'])!!}
      		</div>
      	</div>
      </div>
	<br>
	<div class="row" id="observacionplanilla1" style="display:none;">
		<b><u>OBSERVACIONES:</u></b>
		<textarea rows="4"  class="form-control text-justify" name="descrip_observacion" id="descrip_observacionPDIJ01" style="font-weight:bold; text-transform: capitalize;" ></textarea>
	</div>
	  <div class="row text-center"><br><br>
            <a class="btn btn-primary" href="{{url('CoordinadorInversion/ListaCoordInvExtjera')}}">Cancelar</a>
		     <input type="submit" name="Guardar Observacion" class="btn btn-danger center" value="Guardar Observacion" onclick="return analisisinversion ()">	
		</div>


{{Form::close()}}


		</div>
	</div>
</div>

<!-- Modal para documentos solicitante-->
<div id="doc_solicitantePDIR01" class="modal fade" role="dialog">
  	<div class="modal-dialog modal-lg">
	    <div class="modal-content">
		    <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal">&times;</button>
		        <h3 class="modal-title text-primary"><b>Requisitos del solicitante</b></h3>
				<div id="mostrar_info">
			        <a href="#"><span class="glyphicon glyphicon-info-sign text-success"> Ver info.</span> </a>
			    </div>
			    <div id="ocultar_info" style="display: none;">
			        <a href="#"><span class="glyphicon glyphicon-resize-small text-warning"> Ocultar info.</span></a>
		        </div>
		        <div class="col-md-12" id="det_info" style="display: none;">
		        	<ul>
		        		<li>Registro de Información Fiscal (RIF).</li>
		        		<li>Contrato de Inversión.</li>
		        		<li>Poder de representación debidamente autenticado.</li>
		        	</ul>
		        </div>
		    </div>
		    <div class="modal-body">
		        <div class="modal-body">
		            <div role="tabpanel">
		                <ul class="nav nav-tabs" role="tablist">
		                    <li role="presentation" class="active"><a href="#doc1" class="cargador-planilla" aria-controls="doc1" role="tab" data-toggle="tab" id="tab-inicial">Documento 1</a>
		                    </li>
		                    <li role="presentation"><a href="#doc2" class="cargador-planilla" aria-controls="doc2" role="tab" data-toggle="tab">Documento 2</a>
		                    </li>
		                    <li role="presentation"><a href="#doc3" class="cargador-planilla" aria-controls="doc3" role="tab" data-toggle="tab">Documento 3</a>
		                    </li>
		                    {{--<li role="presentation"><a href="#doc4" class="cargador-planilla" aria-controls="doc4" role="tab" data-toggle="tab">Documento 4</a>
		                    </li>
		                    <li role="presentation"><a href="#doc5" class="cargador-planilla" aria-controls="doc5" role="tab" data-toggle="tab">Documento 5</a>
		                    </li>
		                    <li role="presentation"><a href="#doc6" class="cargador-planilla" aria-controls="doc6" role="tab" data-toggle="tab">Documento 6</a>
		                    </li>--}}
		                </ul>
		            </div>
                    <div class="tab-content">
					<div role="tabpanel" class="tab-pane active text-center" id="doc1">
					@if(isset($docDecInversion))
			            @if(isset($extencion_adi_file_1) && $extencion_adi_file_1 == 'pdf')
                            <span class="text-success"></span>
                            <br>
                            <embed src="{{asset($docDecInversion['file_1'])}}" width= 800px height=680px></embed>
                            @elseif(isset($extencion_adi_file_1) && ($extencion_adi_file_1 == 'docx' || $extencion_adi_file_1 == 'doc'))
                              <br><br>
                              <a href="{{asset($docDecInversion['file_1'])}}" class="btn btn-lg btn-primary"><span><i class="glyphicon glyphicon-cloud-download"></i> </span>Descargar documento 1</a>
                              <br><br>
                            @else
                              .
                        @endif
                     @endif
                    </div>
                    <div role="tabpanel" class="tab-pane text-center" id="doc2">
                    @if(isset($docDecInversion))
                        @if(isset($extencion_adi_file_2) && $extencion_adi_file_2 == 'pdf')
                            <span class="text-success"></span>
                            <br>
                            <embed src="{{asset($docDecInversion['file_2'])}}" width= 800px height=680px></embed>
                            @elseif(isset($extencion_adi_file_2) && ($extencion_adi_file_2 == 'docx' || $extencion_adi_file_2 == 'doc'))
                              <br><br>
                              <a href="{{asset($docDecInversion['file_2'])}}" class="btn btn-lg btn-primary"><span><i class="glyphicon glyphicon-cloud-download"></i> </span>Descargar documento 2</a>
                              <br><br>
                            @else
                              .
                        @endif
                    @endif
                    </div>
                    <div role="tabpanel" class="tab-pane text-center" id="doc3">
                    @if(isset($docDecInversion))
                       @if(isset($extencion_adi_file_3) && $extencion_adi_file_3 == 'pdf')
                            <span class="text-success"></span>
                            <br>
                            <embed src="{{asset($docDecInversion['file_3'])}}" width= 800px height=680px></embed>
                            @elseif(isset($extencion_adi_file_3) && ($extencion_adi_file_3 == 'docx' || $extencion_adi_file_3 == 'doc'))
                              <br><br>
                              <a href="{{asset($docDecInversion['file_3'])}}" class="btn btn-lg btn-primary"><span><i class="glyphicon glyphicon-cloud-download"></i> </span>Descargar documento 3</a>
                              <br><br>
                            @else
                              .
                        @endif
                    @endif
                    </div>
                    {{--  <div role="tabpanel" class="tab-pane text-center" id="doc4">
                    @if(isset($docDecInversion))
                        @if(isset($extencion_adi_file_4) && $extencion_adi_file_4 == 'pdf')
                            <span class="text-success"></span>
                            <br>
                            <embed src="{{asset($docDecInversion['file_4'])}}" width= 800px height=680px></embed>
                            @elseif(isset($extencion_adi_file_4) && ($extencion_adi_file_4 == 'docx' || $extencion_adi_file_4 == 'doc'))
                              <br><br>
                              <a href="{{asset($docDecInversion['file_4'])}}" class="btn btn-lg btn-primary"><span><i class="glyphicon glyphicon-cloud-download"></i> </span>Descargar documento 4</a>
                              <br><br>
                            @else
                              .
                        @endif
                    @endif
                    </div>
                    <div role="tabpanel" class="tab-pane text-center" id="doc5">
                    @if(isset($docDecInversion))
                        @if(isset($extencion_adi_file_5) && $extencion_adi_file_5 == 'pdf')
                            <span class="text-success"></span>
                            <br>
                            <embed src="{{asset($docDecInversion['file_5'])}}" width= 800px height=680px></embed>
                            @elseif(isset($extencion_adi_file_5) && ($extencion_adi_file_5 == 'docx' || $extencion_adi_file_5 == 'doc'))
                              <br><br>
                              <a href="{{asset($docDecInversion['file_5'])}}" class="btn btn-lg btn-primary"><span><i class="glyphicon glyphicon-cloud-download"></i> </span>Descargar documento 5</a>
                              <br><br>
                            @else
                              .
                        @endif
                    @endif
                    </div>
                    <div role="tabpanel" class="tab-pane text-center" id="doc6">
                    @if(isset($docDecInversion))
                        @if(isset($extencion_adi_file_6) && $extencion_adi_file_6 == 'pdf')
                            <span class="text-success"></span>
                            <br>
                            <embed src="{{asset($docDecInversion['file_6'])}}" width= 800px height=680px></embed>
                            @elseif(isset($extencion_adi_file_6) && ($extencion_adi_file_6 == 'docx' || $extencion_adi_file_6 == 'doc'))
                              <br><br>
                              <a href="{{asset($docDecInversion['file_6'])}}" class="btn btn-lg btn-primary"><span><i class="glyphicon glyphicon-cloud-download"></i> </span>Descargar documento 6</a>
                              <br><br>
                            @else
                              .
                        @endif
                    @endif
                    </div>--}}
                    </div>
		        </div>
		    </div>
	    </div>
	    <div class="modal-footer"></div>
	</div>
</div>

<!-- Modal para documentos empresa receptora-->
<div id="doc_emp_receptoraPDIR01" class="modal fade" role="dialog">
  	<div class="modal-dialog modal-lg">
	    <div class="modal-content">
		    <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal">&times;</button>
		        <h3 class="modal-title text-primary"><b>Requisitos de la empresa receptora</b></h3>
				<div id="mostrar_info_soport">
		        	<a href="#"><span class="glyphicon glyphicon-info-sign text-success"> Ver info.</span> </a>
		        </div>
		        <div id="ocultar_info_soport" style="display: none;">
		        	<a href="#"><span class="glyphicon glyphicon-resize-small text-warning"> Ocultar info.</span></a>
		        </div>
		         
		        <div class="col-md-12" id="det_info_soport" style="display: none;">
		        	<ul>
		        		<li>Documento constitutivo de la empresa receptora de inversión, con sus modificaciones.</li>
		        		<li>Registro de Información Fiscal, del representante legal de la empresa receptora de inversión.</li>
		        		<li>Comprobantes de las Inversiones Extranjeras Realizadas, de la adquisición de bienes, o acciones.</li>
		        		<li>Resumen de Balances contables correspondientes a los últimos dos años de ejercicio económico, acompañados de los informes de preparación suscritos por un Contador o Auditor Publico Colegiado, con sus respectivas notas y soportes sobre su elaboración, en el que se refleje el monto de inversión extranjera de entrada o de salida.</li>
		        	</ul>
		        </div>
		    </div>
		    <div class="modal-body">
		        <div class="modal-body">
		            <div role="tabpanel">
		                <ul class="nav nav-tabs" role="tablist">
		                    <li role="presentation" class="active"><a href="#doc1_emp" class="cargador-planilla" aria-controls="doc1_emp" role="tab" data-toggle="tab" id="tab-inicial">Documento 1</a>
		                    </li>
		                    <li role="presentation"><a href="#doc2_emp" class="cargador-planilla" aria-controls="doc2_emp" role="tab" data-toggle="tab">Documento 2</a>
		                    </li>
		                    <li role="presentation"><a href="#doc3_emp" class="cargador-planilla" aria-controls="doc3_emp" role="tab" data-toggle="tab">Documento 3</a>
		                    </li>
		                    <li role="presentation"><a href="#doc4_emp" class="cargador-planilla" aria-controls="doc4_emp" role="tab" data-toggle="tab">Documento 4</a>
		                    </li>
		                </ul>
		            </div>
                    <div class="tab-content">
					<div role="tabpanel" class="tab-pane active text-center" id="doc1_emp">
					@if(isset($docDecInversion))
			            @if(isset($extencion_adi_file_4) && $extencion_adi_file_4 == 'pdf')
                            <span class="text-success"></span>
                            <br>
                            <embed src="{{asset($docDecInversion['file_4'])}}" width= 800px height=680px></embed>
                            @elseif(isset($extencion_adi_file_4) && ($extencion_adi_file_4 == 'docx' || $extencion_adi_file_4 == 'doc'))
                              <br><br>
                              <a href="{{asset($docDecInversion['file_4'])}}" class="btn btn-lg btn-primary"><span><i class="glyphicon glyphicon-cloud-download"></i> </span>Descargar documento 1</a>
                              <br><br>
                            @else
                              .
                        @endif
                     @endif
                    </div>
                    <div role="tabpanel" class="tab-pane text-center" id="doc2_emp">
                    @if(isset($docDecInversion))
                        @if(isset($extencion_adi_file_5) && $extencion_adi_file_5 == 'pdf')
                            <span class="text-success"></span>
                            <br>
                            <embed src="{{asset($docDecInversion['file_5'])}}" width= 800px height=680px></embed>
                            @elseif(isset($extencion_adi_file_5) && ($extencion_adi_file_5 == 'docx' || $extencion_adi_file_5 == 'doc'))
                              <br><br>
                              <a href="{{asset($docDecInversion['file_5'])}}" class="btn btn-lg btn-primary"><span><i class="glyphicon glyphicon-cloud-download"></i> </span>Descargar documento 2</a>
                              <br><br>
                            @else
                              .
                        @endif
                    @endif
                    </div>
                    <div role="tabpanel" class="tab-pane text-center" id="doc3_emp">
                    @if(isset($docListDinamica1))
						@foreach($docListDinamica1 as $documentos) 
			                @if(extnamefile($documentos->file) == 'pdf')
			                    <span class="text-success"></span>
			                    <br>
			                    <embed src="{{asset($documentos->file)}}" style="width: 100%; height: 100%;"></embed>
			                @elseif(extnamefile($documentos->file) == 'docx' || extnamefile($documentos->file) == 'doc')
			                      <br><br>
			                      <a href="{{asset($documentos->file)}}" class="btn btn-lg btn-primary"><span><i class="glyphicon glyphicon-cloud-download"></i> </span>Descargar documento 1</a>
			                      <br><br>
			                @else
			                      .
			                @endif
			        	@endforeach
                    @endif
                    </div>
                    <div role="tabpanel" class="tab-pane text-center" id="doc4_emp">
                    @if(isset($docListDinamica2))
                        @foreach($docListDinamica2 as $documentos2) 
			                @if(extnamefile($documentos2->file) == 'pdf')
			                    <span class="text-success"></span>
			                    <br>
			                    <embed src="{{asset($documentos2->file)}}" style="width: 100%; height: 100%;"></embed>
			                @elseif(extnamefile($documentos2->file) == 'docx' || extnamefile($documentos2->file) == 'doc')
			                      <br><br>
			                      <a href="{{asset($documentos2->file)}}" class="btn btn-lg btn-primary"><span><i class="glyphicon glyphicon-cloud-download"></i> </span>Descargar documento 1</a>
			                      <br><br>
			                @else
			                      .
			                @endif
			        	@endforeach
                    @endif
                   </div>
		        </div>
		    </div>
	    </div>
	    <div class="modal-footer"></div>
	</div>
</div>
@else
<div class="text-center text-warning"> <h4>No se han cargado datos para la panilla Djir01</h4> </div>
@endif
<script src="{{asset('js/ajax_v2.1.1.js')}}"></script>
<script>
  

function validarObsPDIJ01 (){
	var estatus=$('#gen_status_id_PDIJ01').val();
    // alert(estatus);
  	if(estatus == 11 || estatus== 17 || estatus==15) {
    
    	$('#observacionplanilla1').show('show');
    	$('#descrip_observacionPDIJ01').prop("required", true);
  	}else{
    	$('#observacionplanilla1').hide('slow');
    	$('#descrip_observacionPDIJ01').prop('required',false);
        $('#descrip_observacionPDIJ01').removeAttr("required");
    
  	}
}
$(document).ready(function() {
	$('#mostrar_info').click(function(event) {
          $('#mostrar_info').hide(1000).animate({height: "hide"}, 1000,"linear");
          $('#det_info, #ocultar_info').show(1000).animate({width: "show"}, 1000,"linear");  

	});
	$('#ocultar_info').click(function(event) {
          $('#det_info,#ocultar_info').hide(1000).animate({height: "hide"}, 1000,"linear");
          $('#mostrar_info').show(1000).animate({width: "show"}, 1000,"linear");
	});
	$('#mostrar_info_soport').click(function(event) {
          $('#mostrar_info_soport').hide(1000).animate({height: "hide"}, 1000,"linear");
          $('#det_info_soport, #ocultar_info_soport').show(1000).animate({width: "show"}, 1000,"linear");  

	});
	$('#ocultar_info_soport').click(function(event) {
          $('#det_info_soport,#ocultar_info_soport').hide(1000).animate({height: "hide"}, 1000,"linear");
          $('#mostrar_info_soport').show(1000).animate({width: "show"}, 1000,"linear");
	});
	
});
</script>
