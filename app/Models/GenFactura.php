<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GenFactura extends Model
{
    protected $table = 'gen_factura';

 protected $fillable = [

        'gen_usuario_id',
				'gen_consignatario_id',
				'forma_pago_id',
        'fecha_estimada_pago',
        'plazo_pago',
        'tipo_plazo',
				'tipo_instrumento_id',
        'tipo_solicitud_id',
				'gen_dua_id',
				'gen_divisa_id',
				'incoterm_id',
				'numero_factura',
        'fecha_factura',
        'plazo_pago',
        'especif_instrumento',
        'monto_fob',
        'monto_flete',
        'monto_seguro',
        'otro_monto',
        'especif_pago',
        'monto_total',
        'bactivo'
    ];

    public function usuario(){


    }

    public function consignatario(){

       return $this->hasOne('App\Models\GenConsignatario','id','gen_consignatario_id');
    }

    public function formaPago(){

      return $this->hasOne('App\Models\FormaPago','id','forma_pago_id');
    }

    public function tipoInstrumento(){


    }

    public function dua(){

      return $this->hasOne('App\Models\GenDua','id','gen_dua_id');


    }

    public function tipoSolicitud(){

      return $this->hasOne('App\Models\TipoSolicitud','id','tipo_solicitud_id');


    }



    public function incoterm(){


    }

    public function solicitud(){
        return $this->belongsTo('App\Models\GenSolicitud');

    }

    public function divisa(){
        return $this->hasOne('App\Models\GenDivisa','id','gen_divisa_id');

    }


}
