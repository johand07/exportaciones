@extends('templates/layoutlte')
@section('content')

{{Form::open(['method'=>'POST','id'=>'formEr'])}} 


<table class="table table-bordered" id="fields_tec">


   <tr>
   <th>Número de Factura </th>
   <th>Código Arancelario</th>
   <th>Descripción Arancelaria</th>
   <th>Descripción Comercial </th>
   <th>Cantidad </th>
   <th>Disponible</th>
   <th>Cantidad a exportar</th>
   <th>Precio Unitario </th>
   <th>Monto Total</th>

 </tr>

  @foreach($productos as $key=>$valor)
  <tr id="row{{$key}}">

   <td>{!!Form::text('numero_factura',$valor->factura->numero_factura,['class'=>'form-control','readOnly'=>'true'])!!}
  </td>

  <td>{!!Form::text('codigo_arancel[]',$valor->codigo_arancel,['class'=>'form-control','readOnly'=>'true'])!!}
  </td>

 <td>
   {!!Form::text('descripcion_arancelaria[]',$valor->descripcion_arancelaria,['class'=>'form-control','onkeypress'=>'return soloLetras(event)'])!!}</td>

   <td>
   {!!Form::text('descripcion_comercial[]',$valor->descripcion_comercial,['class'=>'form-control','onkeypress'=>'return soloLetras(event)'])!!}</td>


  <td>{!!Form::text('cantidad[]',$valor->cantidad_producto,['class'=>'form-control','id'=>'cantidadFob','onInput'=>"calcularMonto('row$key')",'onkeypress'=>'return soloNumeros(event)'])!!}</td>

   <td>
   {!!Form::text('disponible[]',$valor->disponibles,['class'=>'form-control','readOnly'=>'readOnly'])!!}</td>

    <td>
   {!!Form::text('cantidad_exportar[]',null,['class'=>'form-control'])!!}</td>   

  <td>{!!Form::text('precio_producto[]',$valor->precio_producto,['class'=>'form-control','id'=>'precioFob','onInput'=>"calcularMonto('row$key')",'onkeypress'=>'return soloNumerosDouble(event)'])!!}</td>

   <td>
   {!!Form::text('monto[]',null,['class'=>'form-control','readOnly'=>'readOnly','id'=>'montoFob'])!!}</td>

    
   </tr><br>

  @endforeach
 


</table>

{{form::close()}}

  <div class="col-md-12" align="center">
 <input type="submit" onclick="validarER()" id="submit" class="btn btn-info" value="Enviar"/>
</div>




@endsection


<script type="text/javascript">
	

 function validarER(){

    document.forms["formEr"].action="/guardarEr";
    document.forms["formEr"].submit();
    
 }



</script>
	