<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GenConsignatariosRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

      'cat_tipo_cartera_id'=>'required',
      'rif_empresa'=>'required_if:cat_tipo_cartera_id,2',
      'nombre_consignatario'=>'required',
      'perso_contac'=>'required',
      'cargo_contac'=>'required',
      'pais_id'=>'required_if:cat_tipo_cartera_id,1',
      'cat_tipo_convenio_id'=>'required_if:cat_tipo_cartera_id,1',
      'ciudad_consignatario'=>'required',
      'direccion'=>'required',
      'telefono_consignatario'=>'required|numeric',
      'correo'=>'required',
    ];
    }
}
