<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DocumentosDvdNdBdv extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
          Schema::create('documentos_dvd_nd_bdv', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('gen_dvd_nd_id')->index()->nullable();
            $table->foreign('gen_dvd_nd_id')->references('id')->on('gen_dvd_nd');
            $table->unsignedInteger('cat_documentos_id')->index()->nullable();
            $table->foreign('cat_documentos_id')->references('id')->on('cat_documentos');
            $table->string('ruta_doc_dvd', 200)->nullable();
            $table->boolean('bactivo')->default(1);
            $table->timestamps();
        });    
      }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('documentos_dvd_nd_bdv');    }
}
