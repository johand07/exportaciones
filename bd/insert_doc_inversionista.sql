/**---------------------------------------------------------*/
INSERT INTO `gen_ente` (`id`, `descripcion_ente`, `bactivo`, `created_at`, `updated_at`) VALUES
(2, 'MIPPCOEXIN Inversiones', 1, '2019-02-07 04:00:00', '2019-02-07 04:00:00');


/**---------------------------------------------------------*/
INSERT INTO `cat_documentos` (`id`, `gen_ente_id`, `nombre_documento`, `bactivo`, `created_at`, `updated_at`) VALUES
(10, 2, 'Forma RIE01 completa.', 1, '2019-02-07 04:00:00', '2019-02-07 04:00:00'),
(11, 2, 'Lista de cotejo de recaudos del trámite impresa.', 1, '2019-02-07 04:00:00', '2019-02-07 04:00:00'),
(12, 2, 'Carta de solicitud firmada por el potencial inversionista, en idioma español, exponiendo su interés en establecer su inversión en la República Bolivariana de Venezuela, detallando área o sector productivo de acuerdo a su interés. ', 1, '2019-02-07 04:00:00', '2019-02-07 04:00:00'),
(13, 2, 'Original y copia del pasaporte del representante legal de la empresa o potencial inversionista, centrado en una hoja tamaño carta, con el anexo de la visa solicitada ante el Consulado venezolano acreditado en el país de origen del potencial inversionista, según corresponda:          A. Transeúnte de Negocios (TR-N) Todo potencial inversionista o su representante legal, deberá solicitar esta visa para iniciar sus trámites en el territorio nacional.          B. Transeúnte Inversionista (TR-I) Únicamente para negocios con el Gobierno venezolano, una vez sea aprobada su inversión con un ente del estado.           C. Transeúnte Empresario /Industrial (TR-E-I) Cualquier negocio cuyo capital sea extranjero, una vez sea constituida la empresa en territorio venezolano. ', 1, '2019-02-07 04:00:00', '2019-02-07 04:00:00'),
(14, 2, 'Registro Mercantil de la empresa o industria que representa, en idioma original y traducido al español, apostillado y legalizado ante el Consulado Venezolano acreditado en el país de origen del potencial inversionista. ', 1, '2019-02-07 04:00:00', '2019-02-07 04:00:00'),
(15, 2, 'Original y una (1) copia de la propuesta de la inversión a realizar (resumen de 3 páginas máximo).', 1, '2019-02-07 04:00:00', '2019-02-07 04:00:00'),
(16, 2, 'Poder notariado ante notaria venezolana o de ser extranjero, apostillado o legalizado ante el Consulado Venezolano acreditado en el país de origen del potencial inversionista. (En caso de que la solicitud de registro la presente un tercero, autorizado por el representante legal de la empresa).', 1, '2019-02-07 04:00:00', '2019-02-07 04:00:00'),
(17, 2, '5 U.T en timbres fiscales del Distrito Capital o transferencia electrónica.', 1, '2019-02-07 04:00:00', '2019-02-07 04:00:00');


/**---------------------------------------------------------*/

INSERT INTO `gen_status` (`id`, `nombre_status`, `descripcion_status`, `bactivo`, `created_at`, `updated_at`) VALUES
(13, ' Solicitud de Anulacion', 'Solicitud de Anulacion por el Analista de Inversiones', 1, '2019-02-04 08:00:00', '2019-02-04 08:00:00'),
(14, 'Atendida por el Coordinador', 'Indica que la solicitud fue Atendida por el Coordinador', 1, '2019-02-05 08:00:00', '2019-02-05 08:00:00'),
(15, 'Documentos Incompletos', 'Documentos de solicitud incompleto', 1, '2019-02-08 08:00:00', '2019-02-08 08:00:00'),
(16, 'Aprobado por el Coordinador', 'Indica que fue aprobada por el Coordinador de Inverversion', 1, '2019-02-08 08:00:00', '2019-02-08 08:00:00'),
(17, 'Anulada por Coordinador', 'La solicitud fue anulada por el Coordinador de Inversiones', 1, '2019-02-08 08:00:00', '2019-02-08 08:00:00');

/**---------------------------------------------------------*/

INSERT INTO `tipo_solicitud` (`id`, `solicitud`, `bactivo`) VALUES
(5, 'Inversionista Extranjero', 1),
(6, 'Inversion Extranjera', 1),
(7, 'Transferencia Tecnologica', 1);

/**---------------------------------------------------------*/

INSERT INTO `gen_tipo_usuario` (`id`, `nombre_tipo_usu`, `descripcion_tipo_usu`, `bactivo`, `created_at`, `updated_at`) VALUES
(9, 'Analista Inversionista', 'Analista de solicitudes de inversiones', 1, '2018-12-10 08:00:00', '2018-12-10 08:00:00'),
(10, 'Coordinador Inversionista', 'Coordinador de las solicitudes de inversión', 1, '2018-12-10 08:00:00', '2018-12-10 08:00:00');
