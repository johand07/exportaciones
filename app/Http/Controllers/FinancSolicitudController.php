<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use App\Models\FinanciamientoSolicitud;
use App\Models\GenAnticipo;
use App\Models\CatRegimenExport;
use App\Models\GenNotaCredito;
use App\Models\TipoInstrumento;
use App\Models\GenOperadorCamb;
use App\Models\CatRegimenEspecial;
use Illuminate\Http\Request;
use App\Http\Requests\FinanSoliciRequest;
use App\Models\FacturaSolicitud;
use App\Models\NotaCreditoSolicitud;
use App\Models\GenFactura;
use App\Models\GenSolicitud;
use App\Models\GenUnidadMedida;
use App\Models\GenDua;
use App\Models\TipoSolicitud;
use App\Http\Requests\GenSolicitudRequest;
use App\Models\GenConsignatario;
use App\Models\ConsigTecno;
use App\Models\DetUsuario;
use App\Http\Requests\FacturaSolicitudRequest;
use Session;
use Auth;
use Laracasts\Flash\Flash;
use Alert;
class FinancSolicitudController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
         $num_credito=GenNotaCredito::where('gen_usuario_id',Auth::user()->id)->where('bactivo',1)->first();
         //dd($num_credito);
         if (!empty($num_credito) ) {
            return 1;
         }else{
            return 2;
         }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $titulo="Financiamiento de Solicitud";
        $descripcion="Registra tu financiamiento";
        $num_credito=GenNotaCredito::where('gen_usuario_id',Auth::user()->id)->where('bactivo',1)->pluck('numero_nota_credito','id');
        $num_anticipo=GenAnticipo::where('gen_usuario_id',Auth::user()->id)->where('bactivo',1)->pluck('nro_anticipo','id');
        $regimen_exp=CatRegimenExport::whereIn('id', [1, 2])->pluck('nombre_regimen','id');
        $regimen_especial=CatRegimenEspecial::whereIn('id', [1, 2, 3])->pluck('nombre_especial','id');
        $entidadFina=GenOperadorCamb::whereIn('id', [13, 31])->pluck('nombre_oca','id');
        $tipoInstru=TipoInstrumento::all()->pluck('descrip_tipo_instrumento','id');
        $id=Session::get('gen_solictud_id');

        
        return view('FinancimientoSolicitud.create',compact('num_credito','num_anticipo','regimen_exp','regimen_especial','tipoInstru','entidadFina','titulo','descripcion', 'id'));
        
    }
    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(FinanSoliciRequest $request)
    {
        if($request->nota_credito==1) {

         $NumNotaCredito=$request['gen_nota_credito_id'];

      }else{

        $NumNotaCredito=NULL;

      }
        /*************************************************************/
        if($request->anticipo==1) {

         $Anticipo=$request['gen_anticipo_id'];

      }else{

        $Anticipo=NULL;

      }

        /***************************************************************/

        if($request->cat_regimen_especial_id==3) {

         $RegExport=$request['especifique_regimen'];

      }else{

        $RegExport=NULL;

      }
      /***************************************************************/

      if($request->financiamiento==1) {

        $EntiFinan=$request['entidad_financiera'];
        $TipoInstru=$request['tipo_instrumento_id'];
        $NroContac=$request['nro_contrato'];
        $FechaApro=$request['fapro_contrato'];
        $FechaVenci=$request['fvenci_contrato'];
        $MontoApro=$request['monto_apro_contrato'];
        $NumCuota=$request['num_cuota_contrato'];
        $MontoAmortiza=$request['monto_amort_contrato'];
        $NumCuoAmorti=$request['num_cuota_amortizado'];


      }else{

       
        $EntiFinan=NULL;
        $TipoInstru=NULL;
        $NroContac=NULL;
        $FechaApro=NULL;
        $FechaVenci=NULL;
        $MontoApro=NULL;
        $NumCuota=NULL;
        $MontoAmortiza=NULL;
        $NumCuoAmorti=NULL;
      }

        $id_solicitud=Session::get('gen_solictud_id');

        $FinancSolicitud = new FinanciamientoSolicitud;
        $FinancSolicitud->gen_solicitud_id=$id_solicitud;
        $FinancSolicitud->nota_credito=$request->nota_credito;
        $FinancSolicitud->gen_nota_credito_id=$NumNotaCredito;

        //$FinancSolicitud->anticipo=$request->anticipo;
        //$FinancSolicitud->gen_anticipo_id=$Anticipo;

        $FinancSolicitud->cat_regimen_export_id=$request->cat_regimen_export_id;
        $FinancSolicitud->cat_regimen_especial_id=$request->cat_regimen_especial_id;
        $FinancSolicitud->especifique_regimen=$RegExport;

        $FinancSolicitud->financiamiento=$request->financiamiento;

        $FinancSolicitud->entidad_financiera=$EntiFinan;
        $FinancSolicitud->tipo_instrumento_id=$TipoInstru;
        $FinancSolicitud->nro_contrato=$NroContac;
        $FinancSolicitud->fapro_contrato=$FechaApro;
        $FinancSolicitud->fvenci_contrato=$FechaVenci;
        $FinancSolicitud->monto_apro_contrato=$MontoApro;
        $FinancSolicitud->num_cuota_contrato=$NumCuota;
        $FinancSolicitud->monto_amort_contrato=$MontoAmortiza;
        $FinancSolicitud->num_cuota_amortizado=$NumCuoAmorti;
        $FinancSolicitud->envio_muestra=$request->envio_muestra;
        //$FinancSolicitud->=$request->;
        $FinancSolicitud->save();

        if (isset($NumNotaCredito)) {
            $dua=GenNotaCredito::find($NumNotaCredito)->update(['bactivo' => 0, 'updated_at' => date("Y-m-d H:i:s") ]);
        }
        

         Alert::success('Detalle de la solicitud agregado!','Registro Exitoso')->persistent("OK");
       // return redirect()->action('DetProdSolicitudController@create');
     //  return redirect()->action('SolicitudER');

     $titulo="Solicitudes de Exportación Realizada";
     $descripcion="Solicitudes Registradas";
     $solicitudes=GenSolicitud::where('gen_usuario_id',Auth::user()->id)->where('bactivo',1)->get();
    return view('SolicitudER.index',compact('solicitudes','titulo','descripcion'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\FinancSolicitud  $financSolicitud
     * @return \Illuminate\Http\Response
     */
    public function show(FinanciamientoSolicitud $FinanciamientoSolicitud)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\FinancSolicitud  $financSolicitud
     * @return \Illuminate\Http\Response
     */
    //public function edit(FinanciamientoSolicitud $FinSol, $id)
    public function edit (Request $request)
    {
       
            $id=$request->id;
             ////////////////////////////////////
             $solicitud=GenSolicitud::find($id); 

             $consig_id=ConsigTecno::where('gen_tipo_solicitud',$id)->first();
             $consig=GenConsignatario::where('id',$consig_id->gen_id_consignatario)->pluck('nombre_consignatario','id');
             $tipoExpor=DetUsuario::where('gen_usuario_id',Auth::user()->id)->first();
             ($tipoExpor->produc_tecno==1) ? $num=2 : $num=1;   
             $tiposolic=TipoSolicitud::whereIn('id',[$num])->pluck('solicitud','id');
         /////////////////////Se calcula el monto fob para la solicitud//////////////////////////////
         $soli=Session::get('tipo_solicitud_id');
         $id_solicitud=Session::get('gen_solictud_id');
         $cont=count($request->gen_factura_id);
         for($i=0;$i<=($cont-1);$i++){
            $Fact_solicitud=new FacturaSolicitud;
            $Fact_solicitud->gen_factura_id=$request->gen_factura_id[$i];
            $Fact_solicitud->gen_solicitud_id=$id_solicitud;
            $Fact_solicitud->numero_nota_credito=$request->numero_nota_credito[$i];
            $Fact_solicitud->save();     
       }
      $totalfob=0;
      $cont=count($request->gen_factura_id);
          for($i=0;$i<=($cont-1);$i++){
            $id_factura=$request->gen_factura_id[$i];
            $gen_factura = DB::select('select * from gen_factura where id = ?', [$request->gen_factura_id[$i]]); 
            GenFactura::where('id',$request->gen_factura_id[$i])->update(['bactivo'=>'0']); 
            if (!empty($gen_factura)) {
              $monto_fob = $gen_factura[0]->monto_fob;
              $totalfob=$totalfob+$monto_fob ;
          }
        }
  GenSolicitud::where('id',Session::get('gen_solictud_id'))->update(['monto_solicitud'=>$totalfob,'monto_cif'=>$totalfob]);
  ////////////////////////////////////////////////////

         $array_elem=[];
         $id_fac_solic=[];
         $facturas=array();
         Session::forget('facturas_er_id');
         // Id del tipo de exportacion.
         $tipo_solicitud=Session::get('tipo_solicitud_id');
         // Id de la solicitud er
         $solictud_er=Session::get('gen_solictud_id');
         $id_sol=FacturaSolicitud::where('gen_solicitud_id',$solictud_er)->first();      
        if(is_null($id_sol)){ 
            $status=0;
            $solicitudes=GenFactura::where('gen_usuario_id',Auth::user()->id)->where('bactivo',1)->get(); 
            $facturas=$solicitudes->pluck('numero_factura','id');
         }else{
             $status=1;
             $x=FacturaSolicitud::where('gen_solicitud_id',$solictud_er)->where('bactivo',1)->get();           
           foreach ($x as $key => $value) {
             $array_elem[$key]=$value->gen_factura_id;
             $id_fac_solic[$key]=$value->id;
             Session::put('idFacSoli',$id_fac_solic);
          }
         /**************    Consulta para saber si esa er ha sido finalizada   **************/ 
             $idSoli=GenSolicitud::find($solictud_er);
             (is_null($idSoli->monto_cif)) ? $activo=1 :$activo=0;
              $solicitudes=GenFactura::where('gen_usuario_id',Auth::user()->id)->where('bactivo',$activo)->whereIn('id',$array_elem)->get();
              $facturas=GenFactura::where('gen_usuario_id',Auth::user()->id)->where('bactivo',1)->pluck('numero_factura','id');
                if($activo==0) {
                    foreach ($solicitudes as $key => $value) {
                      $facturas[$value->id]=$value->numero_factura;
                    }         
                }
        /*******************************************************************************/
       }
       $duas=array();
       $id_duas=GenFactura::where('gen_usuario_id',Auth::user()->id)->where('tipo_solicitud_id',$tipo_solicitud)->where('bactivo',1)->get();
       if($tipo_solicitud==1) { 
          if($id_duas!=null) { 
                      foreach ($id_duas as $value) {
                         $duas[$value->id]=$value->gen_dua_id;     
                         } 
                      }else{
                      $duas=[];
                    } 
                    if(!is_null($id_sol)) {
                         foreach ($solicitudes as $value) {
                           $duas[$value->id]=$value->gen_dua_id;
                         } 
                       }
                }else{
                $duas=[];
             }
      ///////////////////////////////////
    
      ////////////////////////////////////
        $solictud_er=$id;        
        $entidadFina=GenOperadorCamb::whereIn('id', [13, 31])->pluck('nombre_oca','id');
        $tipoInstru=TipoInstrumento::all()->pluck('descrip_tipo_instrumento','id');
        $num_anticipo=GenAnticipo::where('gen_usuario_id',Auth::user()->id)->where('bactivo',1)->pluck('nro_anticipo','id');
        $regimen_especial=CatRegimenEspecial::whereIn('id', [1, 2, 3])->pluck('nombre_especial','id');
        $regimen_exp=CatRegimenExport::whereIn('id', [1, 2])->pluck('nombre_regimen','id');
        $num_credito=GenNotaCredito::where('gen_usuario_id',Auth::user()->id)->where('bactivo',1)->pluck('numero_nota_credito','id');
        $titulo="Financiamiento de Solicitud";
        $descripcion="Edita tu financiamiento";
        $edit_FinacSolici=FinanciamientoSolicitud::where('gen_solicitud_id',$solictud_er)->where('bactivo',1)->first();
       ///////////////////////////////////
        if($edit_FinacSolici==null){
            return redirect()->action('FinancSolicitudController@create');
         }else{
        return view('FinancimientoSolicitud.edit',compact('tipoInstru','entidadFina','num_anticipo','regimen_especial','regimen_exp','num_credito','edit_FinacSolici','titulo','descripcion', 'facturas','id','solicitudes','status','duas','solicitud','tiposolic','consig','num'));
         }
      
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\FinancSolicitud  $financSolicitud
     * @return \Illuminate\Http\Response
     */
    public function update(FinanSoliciRequest $request, FinanciamientoSolicitud $FinSol, $id)
    {
        if($request->nota_credito==1) {

         $NumNotaCredito=$request['gen_nota_credito_id'];

      }else{

        $NumNotaCredito=NULL;

      }
        /*************************************************************/
        if($request->anticipo==1) {

         $Anticipo=$request['gen_anticipo_id'];

      }else{

        $Anticipo=NULL;

      }

        /***************************************************************/

        if($request->cat_regimen_especial_id==3) {

         $RegExport=$request['especifique_regimen'];

      }else{

        $RegExport=NULL;

      }
      /***************************************************************/

      if($request->financiamiento==1) {

        $EntiFinan=$request['entidad_financiera'];
        $TipoInstru=$request['tipo_instrumento_id'];
        $NroContac=$request['nro_contrato'];
        $FechaApro=$request['fapro_contrato'];
        $FechaVenci=$request['fvenci_contrato'];
        $MontoApro=$request['monto_apro_contrato'];
        $NumCuota=$request['num_cuota_contrato'];
        $MontoAmortiza=$request['monto_amort_contrato'];
        $NumCuoAmorti=$request['num_cuota_amortizado'];


      }else{

       
        $EntiFinan=NULL;
        $TipoInstru=NULL;
        $NroContac=NULL;
        $FechaApro=NULL;
        $FechaVenci=NULL;
        $MontoApro=NULL;
        $NumCuota=NULL;
        $MontoAmortiza=NULL;
        $NumCuoAmorti=NULL;
      }

        //$id_solicitud=Session::get('gen_solictud_id');

        $FinancSolicitud=FinanciamientoSolicitud::find($id);
        //$FinancSolicitud->gen_solicitud_id=$id_solicitud;
        $FinancSolicitud->nota_credito=$request->nota_credito;
        $FinancSolicitud->gen_nota_credito_id=$NumNotaCredito;

        //$FinancSolicitud->anticipo=$request->anticipo;
        //$FinancSolicitud->gen_anticipo_id=$Anticipo;

        $FinancSolicitud->cat_regimen_export_id=$request->cat_regimen_export_id;
        $FinancSolicitud->cat_regimen_especial_id=$request->cat_regimen_especial_id;
        $FinancSolicitud->especifique_regimen=$RegExport;

        $FinancSolicitud->financiamiento=$request->financiamiento;

        $FinancSolicitud->entidad_financiera=$EntiFinan;
        $FinancSolicitud->tipo_instrumento_id=$TipoInstru;
        $FinancSolicitud->nro_contrato=$NroContac;
        $FinancSolicitud->fapro_contrato=$FechaApro;
        $FinancSolicitud->fvenci_contrato=$FechaVenci;
        $FinancSolicitud->monto_apro_contrato=$MontoApro;
        $FinancSolicitud->num_cuota_contrato=$NumCuota;
        $FinancSolicitud->monto_amort_contrato=$MontoAmortiza;
        $FinancSolicitud->num_cuota_amortizado=$NumCuoAmorti;
        $FinancSolicitud->envio_muestra=$request->envio_muestra;
        //$FinancSolicitud->=$request->;
        $FinancSolicitud->save();

        if (isset($NumNotaCredito)) {
            $dua=GenNotaCredito::find($NumNotaCredito)->update(['bactivo' => 0, 'updated_at' => date("Y-m-d H:i:s") ]);
        }
        

         //Alert::success('Actualización Exitosa!')->persistent("OK");
       // return redirect()->action('DetProdSolicitudController@create');
       // return redirect()->action('TipoSolicitudController');
       $titulo="Solicitudes de Exportación Realizada";
       $descripcion="Solicitudes Registradas";
       $solicitudes=GenSolicitud::where('gen_usuario_id',Auth::user()->id)->where('bactivo',1)->get();

              
       return view('SolicitudER.index',compact('solicitudes','titulo','descripcion'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\FinancSolicitud  $financSolicitud
     * @return \Illuminate\Http\Response
     */
    public function destroy(FinancSolicitud $financSolicitud)
    {
        //
    }
}
