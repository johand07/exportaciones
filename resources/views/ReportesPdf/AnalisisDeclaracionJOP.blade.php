<html>
  <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <title>Analisis declaracion DJO</title>
      <style>
          body{
              text-align:center;
             font-family: Arial;
                   color: #58585A;
          }
          th{
              font-size:10px;
              background-color:#818286;
              color: #fff;
          }
          td{
              font-size:12px;
              border: 1px solid #818286;
          }

          .p{
              border: 0px solid #fff;
          }

          .td2{
              font-size:10px;
              border: 0px solid #818286;

              text-align:center;
              font-size:14px;   
          }
          .td3{
              
              border: 0px solid #818286;
              width: 15%;
              text-align:center;
              
          }
          .td4{
              
              border: 0px solid #818286;
              
              
          }
          table{
              border: 1px solid #818286;
              border-spacing: 0px;
              border-collapse: collapse;
              margin: 0;
              table-layout: fixed;
          }
          .table2{
              border: 0px solid #818286;
          }

         table > tr > td {

            text-align: center;
          }
        .tabla > h4,h6{

          text-align: left;
          margin-bottom: 0;
          font-size:10px;

        }
       
        .tabla > table > .tipo > tr:nth-child(1){

          border-top: 0;
        }


        .centrar{
            text-align: center;
            word-wrap:break-word;
        }

        .izq{
          margin: 2px;
          text-align: left;
        }

        .der{
          margin: auto;
          text-align: right;
        }

        .no-padding{
            padding: 0;
        }

        .table-inner{
            border-style: hidden;
        }

       .table-inner td, .table-inner th {
            border: 1px solid #818286;
        }

        .table-inner2{
            border-style: hidden;
        }

       .table-inner2 td, .table-inner2 th {
            border: 0px solid #818286;
        }

        .page-break {
    page-break-after: always;
}

    .texto-8{
      font-size: 9px;
    }

    .texto-8>p{
      padding-left: 10px;
      padding-right: 10px;
    }

    .page-number:after { content: page  "/" counter(page); }
    #pageCounter { counter-reset: pageTotal; } #pageCounter span { counter-increment: pageTotal; } #pageNumbers { counter-reset: currentPage; } #pageNumbers div:before { counter-increment: currentPage; content: "Page " counter(currentPage) " of "; } #pageNumbers div:after { content: counter(pageTotal); }
      </style>
  </head>
      <body>
    
        <div class="">

@php($NUMITEMS=COUNT($declaracion->rCriterioAnalisis))
@php($ITEMMAX=8)
{{-- $MAXCARACTERLINEADENOM representa la cantidad maxima de caracteres por linea
 en la celda denominacion comercial del producto, esta cantidad es empleada
 para calcular el tamaño que ocupa la celda y definir la cantidad de items a mostrar por hoja  --}}
@php($MAXCARACTERLINEADENOM=70) 
@php($MAXLINEASPLANILLA=17) 
@php($lineaOcupada=0)
@foreach($declaracion->rCriterioAnalisis as $key => $analisis) 

  @if($key%$ITEMMAX==0 || ($lineaOcupada+2>$MAXLINEASPLANILLA))       
  @php($lineaOcupada=0)

          <table style="width:100%;">
            <tr>
                <td colspan="17" class="">
                <table border="0" class="table-inner2" style="width:100%;">
                <tr>
                    <td width="50%"><img class="img-responsive" width="600px" height="50px" src='img/cintillo.png'></td>
                    <td width="50%" class="der">
                      <img src="data:image/png;base64,{!! base64_encode(QrCode::format('png')->size(300)->generate(URL::to('/api/analisisDeclaracion/'.encrypt($declaracion->num_solicitud)))) !!}" width="120px" height="120px">
                    </td>
                </tr>     
                </table>
                </td>
            </tr>
            <tr>
                <td class="td2" colspan="9">
                  <h3>Analisis de Declaración Jurada de Origen</h3>
                </td>
                <td class=" no-padding" colspan="8">
                  <table class="table-inner" style="width:100%">
                    <thead>
                      <tr>
                        <th class="centrar">No. de Solicitud</th>
                        
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td class="centrar">{{$declaracion->num_solicitud}}</td>
                        
                      </tr>
                    </tbody>
                  </table>
                  <table class="table-inner" style="width:100%">
                    <thead>
                      <tr>
                        <th class="centrar">Fecha de Solicitud</th>
                        
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td class="centrar">{{date('d/m/Y',strtotime($declaracion->created_at))}}</td>
                        
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
              <tr>
                    <td style="width: 10%;" class="centrar"><b>RIF</b></td>
                    <td style="width: 30%;" class="centrar"><b>Empresa</b></td>
                    <td colspan="7" class="centrar"><b>Fecha de Emisión</b></td>
                    <td colspan="8" class="centrar"><b>Fecha de Vencimiento</b></td>
              </tr>
              <tr>
                    <td  class="centrar">{{$declaracion->rDeclaracion->rDetUsuario->rif}}</td>
                    <td  class="centrar">{{$declaracion->rDeclaracion->rDetUsuario->razon_social}}</td>
                    <td colspan="7" class="centrar">{{$declaracion->rDeclaracion->fecha_emision}}</td>
                    <td colspan="8" class="centrar">{{$declaracion->rDeclaracion->fecha_vencimiento}}</td>
              </tr>
              <tr>
                    <td  class="centrar"><b>Código Arancelario</b></td>
                    <td  rowspan="2" class="centrar"><b>Denominación Comercial del Producto</b></td>
                    <td colspan="15" class="centrar"><b> *Criterios de Origen </b></td>
              </tr>
              <tr>
                    <td class="centrar">
                    <table class="table-inner" style="width:100%">
                      <tr>
                        <td class="izq"><b>NACIONAL NCM</b></td>
                      </tr>
                      <tr>
                        <td class="der"><b>NALADISA 96</b></td>
                      </tr>
                    </table>
                    </td>
                    <td class="centrar" style="width: 3%;">BOL</td>
                    <td class="centrar" style="width: 3%;">COL</td>
                    <td class="centrar" style="width: 3%;">ECU</td>
                    <td class="centrar" style="width: 3%;">PER</td>
                    <td class="centrar" style="width: 3%;">CUB</td>
                    <td class="centrar" style="width: 3%;">ALD</td>
                    <td class="centrar" style="width: 3%;">ARG</td>
                    <td class="centrar" style="width: 3%;">BRA</td>
                    <td class="centrar" style="width: 3%;">PAR</td>
                    <td class="centrar" style="width: 3%;">URU</td>
                    <td class="centrar" style="width: 3%;">USA</td>
                    <td class="centrar" style="width: 3%;">UE</td>
                    <td class="centrar" style="width: 3%;">CND</td>
                    <td class="centrar" style="width: 3%;">TP</td>
                    <td class="centrar" style="width: 3%;">TR</td>
              </tr>
  @endif

               <tr>
                    <td  class="centrar">
                    <table class="table-inner" style="width:100%">
                      <tr>
                      <td class="der"><b>{{empty($analisis->arancel_mer)? '-': $analisis->arancel_mer}}</b></td>
                      </tr>
                      <tr>
                        
                          <td class="izq"><b>{{empty($analisis->arancel_nan)? '-': $analisis->arancel_nan }}</b></td>
                      </tr>
                    </table>
                    </td>
                    @php 
                        $auxLineaOcupar = ceil(strlen($analisis->declaracionProduc->rProducto->descrip_comercial.$analisis->arancel_descrip_adicional)/$MAXCARACTERLINEADENOM);
                        $lineaOcupada += ($auxLineaOcupar < 2)?$auxLineaOcupar+1:$auxLineaOcupar;
                    @endphp
                    <td class="centrar" >{{$analisis->declaracionProduc->rProducto->descripcion}},
                      {{$analisis->declaracionProduc->rProducto->descrip_comercial}}<br>{{$analisis->descrip_comercial}}</td>
                    <td class="centrar">{{empty($analisis->bol)?'-':$analisis->bol}}</td>
                    <td class="centrar">{{empty($analisis->col)?'-':$analisis->col}}</td>
                    <td class="centrar">{{empty($analisis->ecu)?'-':$analisis->ecu}}</td>
                    <td class="centrar">{{empty($analisis->per)?'-':$analisis->per}}</td>
                    <td class="centrar">{{empty($analisis->cub)?'-': $analisis->cub}}</td>
                    <td class="centrar">{{empty($analisis->ald)?'-':$analisis->ald}}</td>
                    <td class="centrar">{{empty($analisis->arg)?'-':$analisis->arg}}</td>
                    <td class="centrar">{{empty($analisis->bra)?'-':$analisis->bra}}</td>
                    <td class="centrar">{{empty($analisis->par)?'-':$analisis->par}}</td>
                    <td class="centrar">{{empty($analisis->uru)?'-':$analisis->uru}}</td>
                    <td class="centrar">{{empty($analisis->usa)?'-':$analisis->usa}}</td>
                    <td class="centrar">{{empty($analisis->ue)?'-': $analisis->ue}}</td>
                    <td class="centrar">{{empty($analisis->cnd)?'-':$analisis->cnd}}</td>
                    <td class="centrar">{{empty($analisis->tp)?'-': $analisis->tp}}</td>
                    <td class="centrar">{{empty($analisis->tr)?'-': $analisis->tr}}</td>
              </tr>

  @if(($key== $NUMITEMS-1 || $key%$ITEMMAX==($ITEMMAX-1)) && $lineaOcupada < $MAXLINEASPLANILLA)
  @php($auxLineaRestante= $MAXLINEASPLANILLA - $lineaOcupada)
  @php($auxUtil=floor($auxLineaRestante/2))
  @for($j=0;$j<$auxUtil;$j++)
            <tr>
                  <td  class="centrar">
                    <table class="table-inner" style="width:100%">
                      <tr>
                        <td class="centrar"><b>---</b></td>
                      </tr>
                      <tr>
                        <td class="centrar"><b>---</b></td>
                      </tr>
                    </table>
                    </td>
                    <td class="centrar" ><b>INUTILIZADO</b></td>
                    <td class="centrar">---</td>
                    <td class="centrar">---</td>
                    <td class="centrar">---</td>
                    <td class="centrar">---</td>
                    <td class="centrar">---</td>
                    <td class="centrar">---</td>
                    <td class="centrar">---</td>
                    <td class="centrar">---</td>
                    <td class="centrar">---</td>
                    <td class="centrar">---</td>
                    <td class="centrar">---</td>
                    <td class="centrar">---</td>
                    <td class="centrar">---</td>
                    <td class="centrar">---</td>
                    <td class="centrar">---</td>
            </tr>
  @endfor
  @endif

  @if( $key%$ITEMMAX==($ITEMMAX-1) || $key== $NUMITEMS-1 || ($lineaOcupada+2>$MAXLINEASPLANILLA))  
              <tr>
                  <td colspan="17">
                    <p><b><u>OBSERVACIONES: </u></b></p>
                    <p><b>Nota: {{$declaracion->observacion_analisis}}</b></p>
                  </td>
              </tr>
              <tr>
                  <td colspan="17">
                    <p class="">&nbsp;</p>
                  </td>
              </tr>
          </table>
          <div class="page-break"></div>
          @if(($lineaOcupada+2>$MAXLINEASPLANILLA))
            @php($ITEMMAX=$key+1)
          @endif
  @endif
  
@endforeach           

          <table width="100%">
              <tr><td class="centrar" colspan="2"><u><h3>IDENTIFICACIÓN DE LOS CRITERIOS DE ORIGEN A FIN DE SU COLOCACIÓN EN EL CERTIFICADO DE ORIGEN:</h3></u></td></tr>
              <tr><td style="width:50%;" valign="top" class="texto-8">
                  <p>
                    <b><u>- (BOL) BOLIVIA - VENEZUELA:</u></b><br>
                    Califica según "Acuerdo de Comercio de los Pueblos para la Complementariedad Económica, Productiva".<br>
                    (criterio indicado en el campo correspondiente a la mercancia)<br>
                    <b>3-A: Anexo II, Articulo 3, literal a)<br>
                    3-B: Anexo II, Articulo 3, literal b)<br>
                    3-C: Anexo II, Articulo 3, literal c)<br>
                    3-D: Anexo II, Articulo 3, literal d)<br>
                    3-E: Anexo II, Articulo 3, literal e)<br>
                    3-F: Anexo II, Articulo 3, literal f)<br>
                    10:&nbsp;&nbsp; Anexo II, Articulo 10°<br></b>
                  </p>

                  <p>
                    <b><u>- (COL) COLOMBIA - VENEZUELA:</u></b><br>
                    Califica según "Acuerdo de Alcance Parcial 28"<br>
                    (criterio indicado en el campo correspondiente a la mercancia)<br>
                    <b>3-1A: Anexo II, Articulo 3, párrafo 1 literal a)<br>
                    3-1B: Anexo II, Articulo 3, párrafo 1 literal b)<br>
                    3-1C: Anexo II, Articulo 3, párrafo 1 literal c)<br>
                    3-1Di: Anexo II, Articulo 3, párrafo 1 literal d), inciso i<br>
                    3-1Dii: Anexo II, Articulo 3, párrafo 1 literal d), inciso ii<br>
                    3-1Diii: Anexo II, Articulo 3, párrafo 1 literal d), inciso iii<br>
                    7:&nbsp;&nbsp; Anexo II, Articulo 7°<br></b>
                  </p>

                  <p>
                    <b><u>- (ECU) ECUADOR / COMUNIDAD ANDINA (CAN):</u></b><br>
                    Califica según Decisión 416 de la Comisión del Acuerdo de Cartagena, Articulo 2, literal (idicando en el campo correspondiente a la mercancia)<br>
                    En caso que el literal sea la letra C, deberá indicar adicionalmente a la normativa, el número de la Resolución indicado en el campo correspondiente a "OBSERVACIONES" del presente oficio.</b>
                  </p>

                  <p>
                    <b><u>- (PER) PERU - VENEZUELA</u></b><br>
                    Califica según "Acuerdo de Alcance Parcial de Naturaleza Comercial", Anexo II, Artículo Primero, literal (criterio indicado en el campo correspondiente a la mercancia).<br>
                    <b>3-1: Anexo II, Articulo 3, párrafo 1 literales a) al h), (según sea el caso)<br>
                    3-2: Anexo II, Articulo 3, párrafo 2<br>
                    3-3: Anexo II, Articulo 3, párrafo 3<br>
                    3-4: Anexo II, Articulo 3, párrafo 4<br>
                    3-5: Anexo II, Articulo 3, párrafo 5<br>
                    3-6: Anexo II, Articulo 3, párrafo 6<br>
                    8:&nbsp;&nbsp;  Articulo 8°<br></b>

                   
                     <p>
                    <b><u>- (TK) TURQUÍA – VENEZUELA </u></b><br>
                    Califica según ¨Acuerdo de Desarrollo Comercial entre la República Bolivariana de Venezuela y la República de Turquía Según Gaceta Nº 6.418, de fecha 27/12/2018.<br>
                   
                    <b>4-1a: Anexo III, Artículo 4, párrafo 1, literal a) al literal k), (según el caso).<br>
                      4-1b: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      4-1c: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      4-1d: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>
                      4-1e: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      4-1f: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      4-1g: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>
                      4-1h: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      4-1i: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      4-1j: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>
                      4-1k: <br>
                      5-1-2:  Anexo III, Artículo 5, párrafo 1, Apéndice 2<br>
                      5-2-2:  Anexo III, Artículo 5, párrafo 2, Apéndice 2<br>
                      5-3-2:  Anexo III, Artículo 5, párrafo 3, Apéndice 2<br>
                      9:      Anexo III, Artículo 9 <br></b>
                  </p>
                  </p>
                  </td>
                  <td style="width:50%;" valign="top" class="texto-8">
                  <p>
                    <b><u>- (CUB) CUBA - VENEZUELA:</u></b><br>
                    Califica según "Cuarto Protocolo Adicional ACE 40", Cuba - Venezuela<br>
                    (criterio indicado en el campo correspondiente a la mercancía)<br>
                    <b>3-1A: Anexo I, Articulo 3, párrafo 1 literales a) al h), (según sea el caso)<br>
                    3-2: Anexo I, Articulo 3, párrafo 2<br>
                    3-3: Anexo I, Articulo 3, párrafo 3<br>
                    3-4A: Anexo I, Articulo 3, párrafo 4 literal a)<br>
                    3-4B: Anexo I, Articulo 3, párrafo 4 literal b)<br>
                    3-4C: Anexo I, Articulo 3, párrafo 4 literal c)<br>
                    4-1: Anexo I, Articulo 4, párrafo 1<br>
                  
                  </p>

                   <p>
                    <b><u>- (ALD) ALADI y (TP) TERCEROS PAÍSES:</u></b><br>
                    Califica según "Resolución 252 del Comité de Representantes", Capítulo I, Artículo Primero, literal (criterio indicado en el campo correspondiente a la mercancia)<br>
                    <b>A: Literal a)<br>
                    B: Literal b)<br>
                    C: Literal c)<br>
                    D: Literal d)<br>
                    E: Literal e)<br>
                    2: Articulo 2°<br>
                    </b>
                  </p>

                  <p>
                    <b><u>- (ARG-BRA-PAR-URU) CAN - MERCOSUR:</u></b><br>
                    Califica según "Acuerdo de Complementación Económica N° .59", Anexo IV,<br>
                    <b>2A: Artículo 2°, Literal a)<br>
                    2C: Artículo 2°, Literal c)<br>
                    4A: Artículo 4°, Literal a)<br>
                    4B: Artículo 4°, Literal b)<br>
                    4C: Artículo 4°, Literal c)<br>
                    5: Artículo 8°<br>
                    </b>
                    En caso que el criterio indicado sea N-5, deberá indiocar el Apéndice correspondiente al Requisito Específico de Origen en el campo correspondiente a <b>"OBSERVACIONES"</b> del presente oficio.
                  </p>

                  <p>
                    <b><u>- (USA) SGP ESTADOS UNIDOS Y PUERTO RICO:</u></b><br>
                    Califica en tanto el porcentaje indicado sea superior o igual al 35%, o en aquellos casos que la mercancia sea Íntegramente Producida en el País <b>"P"</b><br>
                  </p>

                  <p>
                    <b><u>- (UE) SGP UNION EUROPEA:</u></b><br>
                    Califica en los casos que aparezca la letra <b>"W"</b> o <b>"P"</b>, según el Reglamento (CEE) N° 2454/93 MODIFICADOS POR LOS REGLAMENTOS (CE) N° 19/97 Y 46/1999.<br>
                  </p>

                  <p>
                    <b><u>- (CND) SGP CANADA:</u></b><br>
                    Califica en los casos que aparezca la letra <b>"F"</b> o <b>"P"</b><br>
                  </p>

                  <p>
                    <b>- (N):</b> No califica Origen Venezolano para ese Acuerdo o País.<br>
                    <b>&nbsp;&nbsp;(*):</b> Información insuficiente para la determinación del Origen.
                  </p>
                  
                  </td>
              </tr>
              <tr><td  colspan="2" class="texto-8">
              <p><b><u>Nota:</u></b> Esta Calificación de Origen se expide con base a la Declaración de Origen presentada por la empresa; en consecuencia sólo será válida si dicha información es verdadera y exacta. La empresa deberá comunicar a este Ministerio cualquier variación a los fines del ajuste que corresponda. Se hace de su conocimiento, actuando conforme a lo pautado en el Artículo 73 de la Ley Orgánica de Procedimientos Administrativos, que en caso de disentir de esta Calificación de Origen podrá ejercer el Recurso de Reconsideración ante este Ministerio dentro de los quince (15) días siguientes a su notificación a tenor de lo dispuesto en el Artículo 94 ejusdem.</p>
              <p><b><u>Nota:</u></b> En caso de suscripción de nuevos Acuerdos Comerciales o modificación de los suscritos la empresa debe solicitar nuevamente la Calificación de Origen..</p>
              </td></tr>
          </table>
        </div>
      </body>
</html>