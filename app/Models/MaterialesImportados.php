<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MaterialesImportados extends Model
{
    protected $table = 'materiales_importados';
    protected $fillable = [

           'planilla_2_id',
           'pais_id',
           'codigo',
           'descripcion',
           'incidencia_costo',    
           'bactivo'
    ];

    public function planilla2() {

     return $this->hasOne('App\Models\Planilla2','id','planilla_2_id');

    }

     public function pais() {

     return $this->hasOne('App\Models\Pais','id','pais_id');

    }

    public function getMaterialNombreAttribute(){
        $mercosur = GenArancelMercosur::where('codigo',$this->codigo)->get()->last();
        $nandina = GenArancelNandina::where('codigo',$this->codigo)->get()->last();

        if(!empty($mercosur)){
            return $mercosur->descripcion;
        }

        if(!empty($nandina)){
            return $nandina->descripcion;
        }

        return "";
    }

}
