@extends('templates/layoutlte_admin')

@section('content')

@component('modal_dialogo',['arancel'=>$arancel])

@slot('header') <span>Código Arancelario</span> @endslot

@slot('body')  <div id="arancel"></div>

@endslot

@slot('footer')@endslot

@endcomponent
<style type="text/css">
@media print{@page { width: 50%;
 height: 50%;
 margin: 0% 0% 0% 0%;
 filter: progid:DXImageTransform.Microsoft.BasicImage(Rotation=3);
 size: landscape;
    }}
</style>

<div id="printf">
  <div class="row">
  	<div class="col-md-12">
  		{!! Form::open(['route'=>'DetalleProductos.store','method'=>'POST','id'=>'detalleproducto']) !!}
  			<div class="row" id="codigo_arancelarios">
  	           <div class="col-md-3"></div>
  	            <div class="col-md-6 text-center">
  	              {!!Form::radio('tipo_arancel','1',false,['class'=>'radio-inline','id'=>'cod_andina'])!!}
  	              {!! Form::label('','Nandina',['class'=>'radio-inline'])!!}
  	              {!! Form::radio('tipo_arancel','2',false,['class'=>'radio-inline','id'=>'cod_mercosur'])!!}
  	              {!! Form::label('','Mercosur (NCM)',['class'=>'radio-inline'])!!}
  	            </div>
  	          <div class="col-md-3"></div>
  	        </div>
  	        <div class="row">
  	        	<table class="table table-bordered text-center" id="fields2" >

                    	<tr>
  	                    <th>Código Arancelario </th>
  	                    <th>Descripción Arancelaria</th>
  	                    <th></th>
                        <th></th>
                    	</tr>
                    	<tr id="fila">

  	                    <td>{!!Form::text('codigo',null,['class'=>'form-control','data-toggle'=>"modal",'data-target'=>"#modal",'id'=>'codigo','readonly'=>'true'])!!}
  	                    </td>
  	                    <td>{!!Form::text('descripcion',null,['class'=>'form-control','id'=>'descripcion','onkeypress'=>'return soloLetras(event)'])!!}</td>
  	                    <td>
  	                    	<a class="btn btn-primary" href="{{url('admin/AdminUsuario')}}">Cancelar</a>
    							<input type="submit" name="Consultar" class="btn btn-success center" value="Consultar" onclick="">
  	                    </td>
                        <td>
                          @if(!empty($productos[0]))
                             <button type="button" onClick="javascript:printfun({{count($productos)}})" class="btn btn-primary"><span class="glyphicon glyphicon-print" aria-hidden="true"></span>Imprimir</button>
                          @endif
                        </td>
                        <td>
                          <a href="#" id="btnExport" class="btn btn-primary" onclick="exportarexel()"><span class="glyphicon glyphicon-print" aria-hidden="true" ></span>Exportar Excel</a>
                        </td>
              </table> 
            </div>
      {{Form::close()}} 
  	        
         <div class="row">
          <div class="col-md-4"> 
          </div>
          <div class="col-md-4"> 
          </div>
          <div class="col-md-4"> 
            <div class="input-group">
            <span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
            <input id="system_search" name="busqueda" type="text" class="form-control" placeholder="Buscar">
            </div>
            </div>  
         </div>
      </div>
  </div>
</div>
<br>

 
@if(!empty($productos[0]))

<div class="row" style="display:none;" id="cintillo"><br>
	<table style="width: 100%">
		<tr>
      <td></td>
			<td align="center"><img class="img-responsive" style="width: 800px;" src={{ asset('img/cintillo_reporte.png') }}></td>
		</tr>
		<tr>
			<td></td>
			<td align="center">Fecha: {{$fecha}}</td>
		</tr>
	</table>
</div>
<div class="row">
	<div class="col-md-6">
    	<b>Cantidad de Productos Resgitrados:</b><span class="label label-warning">{{count($productos)}}</span> 
        
    </div>
      <div class="col-md-6"></div>
</div>
<br><br>
<div class="row">
	<div class="col-md-12">
	
		<table id="listaProductos" class="table-list-search tbody table table-bordered table-hover" 		style="width:100%">
			<thead>
                <tr>
                    <th>Rif</th>
                    <th>Razon Social</th>
                    <th>Correo</th>
                    <th>Código Arancelario</th>
                    <th>Descripción Arancelaria</th>
                    <th>Descripción Comercial</th>
                    <th>Capacidad Instalada Anual</th>
                    <th>Capacidad Operativa Anual</th>
                    <th>Capacidad de Almacenamiento Anual</th>
                    <th>Capacidad de Exportación</th>

                </tr>
            </thead>
            <tbody>
	            @foreach($productos as $key=>$producto)
	                <tr>
	                	<td>{{$producto->rif}}</td>
	                    <td>{{$producto->razon_social}}</td>
	                    <td>{{$producto->correo}}</td>
	                    <td>{{$producto->codigo}}</td>
	                    <td>{{$producto->descripcion}}</td>
	                    <td>{{$producto->descrip_comercial}}</td>
	                    <td>{{$producto->cap_insta_anual}}</td>
	                    <td>{{$producto->cap_opera_anual}}</td>
	                    <td>{{$producto->cap_alamcenamiento}}</td>
	                    <td>{{$producto->cap_exportacion}}</td>
	                </tr>
	            @endforeach
            </tbody>
        </table>
        <br><br><br>
    </div>
	</div>
</div>
@endif
@stop

<script src="{{asset('js/ajax_v2.1.1.js')}}"></script>
<script type="text/javascript">
/*--------------ajax reporte exel---------------------------------------*/
function exportarexel(){
    $.ajax({    //AgenteAduanal.destroy O exportador/AgenteAduanal/{AgenteAduanal}
            url: '/admin/DetalleProductosExcel',
            type: 'GET',

            data: {
                    
                    'codigo': $('#codigo').val(),
                    'descripcion': $('#descripcion').val()

                },
        })
        .done(function(data) {
            //alert(data.file);
              var a = document.createElement("a");
              a.href = data.file;
              a.download = data.name;
              document.body.appendChild(a);
              a.click();
              a.remove();
              console.log(data);
       



        })
        .fail(function(jqXHR, textStatus, thrownError) {
            errorAjax(jqXHR,textStatus)

        })
}

	  $('input:radio[name="tipo_arancel"]').change( function(){

     if($(this).is(':checked')) {

       var parametros = {
               'id': $(this).val()

       };
       var token= $('input[name="_token"]').val();
       console.log(token);
       $('#id_arancel').val(parametros.id);
      $('#codigo').prop('disabled',false);
       $.ajax({
           type: 'GET',
           url: '/admin/reporteArancel',
           data: {'arancel': $(this).val(),'_token': token},
           success: function(data){

              $table='<table class="table table-bordered table-hover" id="tabla"><tr><th>Código Arancelario</th><th>Descripción</th></tr>';

              $('#arancel').html($table);
              $.each(data,function(index,value){

             $('#tabla tr:last').after('<tr class="data"><td>'+value.codigo+'</td><td>'+value.descripcion+'</td></tr></table>');

              });

           }

       }); 
    }
});

$('#codigo').show('modal');

/**------------------------------*/
 var global_id=localStorage.setItem('arancel'," ");


 $(document).on('click','#codigo',function(){

   var id=$(this).parents("tr").attr('id');
   localStorage.setItem('arancel',id);

});

 $(document).on('click','tr.data',function(){

  var id=localStorage.getItem('arancel');
  var cod=$(this).find("td").eq(0).text();
  var ara=$(this).find("td").eq(1).text();

  $("tr#"+id).find("td").eq(0).children("input").val(cod);
  $("tr#"+id).find("td").eq(1).children("input").val(ara);

  $('#modal button').click();
  $('#buscador_arancel').val("");



  });

 $(document).on('keyup','#buscador_arancel',function(){

 var tipo=$('input[name="tipo_arancel"]').val();

   $.ajax({

      type:'get',
      url: '/admin/reporteArancel',
      data: {'valor': $(this).val(), 'tipoAran':tipo},
      success: function(data){

        $table='<table class="table table-bordered table-hover" id="tabla"><tr><th>Código Arancelario</th><th>Descripción</th></tr>';

        $('#arancel').html($table);
        if(data.length==0){

         $('#tabla tr:last').after('<tr><td colspan="2" style="color:red;text-align:center" >Códigos no existen</td></tr>');

       }else{
        $.each(data,function(index,value){

       $('#tabla tr:last').after('<tr class="data"><td>'+value.codigo+'</td><td>'+value.descripcion+'</td><tr></table');

        });

      }
      }

   });

});



 function printfun(cantidad){
    IE = window.navigator.appName.toLowerCase().indexOf("micro") != -1; //se determina el tipo de  navegador
    (IE)?sColl = "all.":sColl = "getElementById('";
    (IE)?sStyle = ".style":sStyle = "').style";
    eval("document." + sColl + "printf" + sStyle + ".display = 'none';"); //se ocultan los botones
    eval("document." + sColl + "cintillo" + sStyle + ".display = 'block';");
    
    
    $('#listaProductos').css('font-size', 9);
    $('.main-footer').hide();
    window.print();
    
    
    //print();
    /*if (i == cantidad) {
       print(); //se llama el dialogo de impresiÃ³n
    }*/
    //$('#ver').hide();//Casa 
    
    eval("document." + sColl + "printf" + sStyle + ".display = '';"); // se muestran los botones nuevamente
     eval("document." + sColl + "cintillo" + sStyle + ".display = 'none';");
   
    $('#listaProductos').css('font-size', 16);
    $('.main-footer').show();
  }




</script>