<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableGenDeclaracionJo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('gen_declaracion_jo', function (Blueprint $table) {
            
            $table->date('fecha_vencimiento')->nullable()->after('num_paises');
            $table->date('fecha_emision')->nullable()->after('num_paises');
            $table->unsignedInteger('observacion_corregida')->nullable()->after('fecha_vencimiento');
            

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('gen_declaracion_jo', function (Blueprint $table) {
            //
            $table->dropColumn('fecha_emision');
            $table->dropColumn('fecha_vencimiento');
            $table->dropColumn('observacion_corregida');
            
        });
    }
}
