<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DJOPlanilla2Request extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'descripcion_comercial'=>'required',
            'descripcion_arancel'=>'required',
            'codigo_arancel'=>'required',
            'unidad_medida'=>'required',
            'uso_aplicacion'=>'required'
           
        ];
    }
    /*
            'descripcion_arancelaria_importado.*'=>'required',
            'codigo_arancel_importado.*'=>'required',
            'pais_importado.*'=>'required',
            'monto_incidencia_importado.*'=>'required',
            'descripcion_arancelaria_nacional.*'=>'required',
            'codigo_arancel_nacional.*'=>'required',
            'nombre_producto_nacional.*'=>'required',
            'nombre_proveedor_nacional.*'=>'required',
            'monto_incidencia_nacional.*'=>'required',
    */
}
