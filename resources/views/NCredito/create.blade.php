@extends('templates/layoutlte')
@section('content')
{{Form::open(['route'=>'NCredito.store','method'=>'POST','id'=>'formncredito'])}}
<br><div class="row"> 
<div class="col-md-12">
	<div class="col-md-3"></div>
	<div class="col-md-6">
		<div class="form-group">
			{!! Form::hidden('gen_usuario_id', Auth::user()->id) !!}
			{!! Form::label('Consignatario','Consignatario') !!}
			{!! Form::select('gen_consignatario_id',$consig,null,['class'=>'form-control','id'=>'gen_consignatario_id',
			'placeholder'=>'Seleccione un Consignatario']) !!}
		</div>
			<div class="form-group">
			{!! Form::label('Numero de Nota de Crédito','Numero de Nota de Crédito')!!}
			{!! Form::text('numero_nota_credito',null,['class'=>'form-control','id'=>'numero_nota_credito','onkeypress'=>'return soloNumeros(event)'])!!}
		</div>
		<div class="form-group">
	    	{!! Form::label('','Fecha de Emisión') !!} 
	    	<div class="input-group date">
	    		{!! Form::text('femision',null,['class'=>'form-control','readonly'=>'readonly','id'=>'femision']) !!}
	    		<span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
	    	</div>	              
	   </div>
		<div class="form-group">
			{!! Form::label('Concepto de la Nota de Crédito','Concepto de la Nota de Crédito')!!}
			{!! Form::text('concepto_nota_credito',null,['class'=>'form-control','id'=>'concepto_nota_credito','maxlength'=>'100','onkeypress'=>'return soloNumerosyLetras(event)'])!!}
		</div>
		<div class="form-group">
			{!! Form::label('Monto de Nota Crédito','Monto de Nota Crédito')!!}
			{!! Form::text('monto_nota_credito',null,['class'=>'form-control format','id'=>'monto_nota_credito',])!!}
		</div>
		<div class="form-group">
			{!! Form::label('Numero de Factura Asociada','Número de Factura Asociada') !!}
			{!! Form::select('gen_factura_id',$factura,null,['class'=>'form-control','id'=>'gen_factura_id',
			'placeholder'=>'Seleccione una Factura','onchange'=>'montoNotaCredi();']) !!}
		</div>

		<!--Inicio-->
			{!! Form::text('monto_fob',null,['class'=>'form-control','id'=>'monto_fob','readonly'=>'true'])!!}
		<!--Fin-->

		<div class="form-group">
			{!! Form::label('Justificación de la Nota de Crédito','Justificación de la Nota de Crédito')!!}
			{!! Form::text('justificacion',null,['class'=>'form-control','id'=>'justificacion','maxlength'=>'100','onkeypress'=>'return soloNumerosyLetras(event)'])!!}
		</div>
	</div>
	<div class="col-md-3"></div>
</div>
</div>
<br><div class="row">
	<div class="col-md-12">
		<div class="col-md-4"></div>
		<div class="col-md-4 text-center">
		<a href="{{ route('NCredito.index') }}" class="btn btn-primary">Cancelar</a>
		<input type="submit" class="btn btn-success" value="Enviar" name="" onclick="enviarcredito()">
		</div>
		<div class="col-md-4"></div>
	</div>
</div>
{{Form::close()}}
@stop

<script type="text/javascript">


  function montoNotaCredi(){
  $.ajax({    //AgenteAduanal.destroy O exportador/AgenteAduanal/{AgenteAduanal}
            url: '/exportador/FacturaSolicitud',
            type: 'GET',

            data: {
                    '_token'  : $('#token').val(),
                    
                    'gen_factura_id':$('#gen_factura_id').val(),
                    
                },
        })
        .done(function(data) {
            console.log('data', data);
            var montototal     = data.monto_fob;
            let montonota = data.monto_nota_credito;
            //alert(montototal);
            let nota=$('#monto_nota_credito').val();
            let total=parseInt(montonota)+parseInt(nota);
            console.log('montototal', montototal);
            console.log('montonota', montonota);
            console.log('total', total);
            
            if (total >= montototal) {
            	swal('Operación denegada','El total de las notas de crédito asociadas a la factura no puede ser mayor al monto de la factura','warning');
            	$("#monto_nota_credito").css('border', '1px solid red').after('<span id="ok" style="color:red" class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span>');
            	$("#monto_nota_credito").focus();

            	$('#monto_nota_credito').val('');
            } else {
            	$("#monto_nota_credito").css('border', '1px solid green').after('<span id="ok" style="color:green" class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span>');
            	$("#monto_nota_credito").focus();
            }

			$('#monto_fob').val(montototal);
      
        })
        .fail(function(jqXHR, textStatus, thrownError) {
            errorAjax(jqXHR,textStatus)
        })
}
function facturaNC(){
  $.ajax({    //AgenteAduanal.destroy O exportador/AgenteAduanal/{AgenteAduanal}
            url: '/exportador/facturaNC',
            type: 'GET',

            data: {
                    '_token'  : $('#token').val(),
                    
                    'gen_factura_id':$('#gen_factura_id').val()
                },
        })
        .done(function(data) {
  			if (data == 1) {

	  			$("#gen_factura_id").focus();
	        
	        	$('#ok5').remove()
	        	$("#gen_factura_id").css('border', '1px solid red').after('<span id="ok5" style="color:red" class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span>');
	        
				swal('Error!', 'La factura seleccionada ya esta asociada a una nota de credito', 'warning',{ button: "Ok!",});
				$('#gen_factura_id').val('');
				$('#monto_fob').val('');
  			}
  			if (data == 2) {

	  			$("#gen_factura_id").focus();
	        
	        	$('#ok5').remove()
	        	$("#gen_factura_id").css('border', '1px solid green').after('<span id="ok5" style="color:green" class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span>');
	
  			}	

        })
        .fail(function(jqXHR, textStatus, thrownError) {
            errorAjax(jqXHR,textStatus)
        })
}
</script>