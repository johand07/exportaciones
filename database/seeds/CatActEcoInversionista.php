<?php

use Illuminate\Database\Seeder;
use App\Models\CatActividadEcoInversionista;

class CatActEcoInversionista extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sectorInv = [
            [
               
                'cat_sector_inversionista_id'   => 1,
                'nombre'       => 'Carne y preparados de carne',
                'created_at' => '2021-06-15 13:59:08',
                'updated_at' => '2021-06-15 13:59:08',
            ],
            [
               
                'cat_sector_inversionista_id'   => 1,
                'nombre'       => 'Productos lácteos y huevos de aves',
                'created_at' => '2021-06-15 13:59:08',
                'updated_at' => '2021-06-15 13:59:08',
            ],
            [
                
                'cat_sector_inversionista_id'   => 1,
                'nombre'       => 'Pescado (no incluidos los mamíferos marinos), crustáceos, moluscos e invertebrados
                acuáticos y sus preparados',
                'created_at' => '2021-06-15 13:59:08',
                'updated_at' => '2021-06-15 13:59:08',
            ],
            [
               
                'cat_sector_inversionista_id'   => 1,
                'nombre'       => 'Cereales y preparados de cereales',
                'created_at' => '2021-06-15 13:59:08',
                'updated_at' => '2021-06-15 13:59:08',
            ],
            [
                
                'cat_sector_inversionista_id'   => 1,
                'nombre'       => 'Legumbres y frutas',
                'created_at' => '2021-06-15 13:59:08',
                'updated_at' => '2021-06-15 13:59:08',
            ],
            [
               
                'cat_sector_inversionista_id'   => 1,
                'nombre'       => 'Azúcares, preparados de azúcar y miel',
                'created_at' => '2021-06-15 13:59:08',
                'updated_at' => '2021-06-15 13:59:08',
            ],
            [
               
                'cat_sector_inversionista_id'   => 1,
                'nombre'       => 'Café, té, cacao, especias y sus preparados',
                'created_at' => '2021-06-15 13:59:08',
                'updated_at' => '2021-06-15 13:59:08',
            ],
            [
               
                'cat_sector_inversionista_id'   => 1,
                'nombre'       => 'Pienso para animales (excepto cereales sin moler)',
                'created_at' => '2021-06-15 13:59:08',
                'updated_at' => '2021-06-15 13:59:08',
            ],
            [
                
                'cat_sector_inversionista_id'   => 1,
                'nombre'       => 'Productos y preparados comestibles diversos',
                'created_at' => '2021-06-15 13:59:08',
                'updated_at' => '2021-06-15 13:59:08',
            ],
            [
                
                'cat_sector_inversionista_id'   => 2,
                'nombre'       => 'Bebidas',
                'created_at' => '2021-06-15 13:59:08',
                'updated_at' => '2021-06-15 13:59:08',
            ],
            [
                
                'cat_sector_inversionista_id'   => 2,
                'nombre'       => 'Tabaco y sus productos',
                'created_at' => '2021-06-15 13:59:08',
                'updated_at' => '2021-06-15 13:59:08',
            ],
            [
                
                'cat_sector_inversionista_id'   => 3,
                'nombre'       => 'Cueros, pieles y pieles finas, sin curtir',
                'created_at' => '2021-06-15 13:59:08',
                'updated_at' => '2021-06-15 13:59:08',
            ],
            [
                
                'cat_sector_inversionista_id'   => 3,
                'nombre'       => 'Semillas y frutos oleaginosos',
                'created_at' => '2021-06-15 13:59:08',
                'updated_at' => '2021-06-15 13:59:08',
            ],
            [
                
                'cat_sector_inversionista_id'   => 3,
                'nombre'       => 'Caucho en bruto (incluso el caucho sintético y regenerado)',
                'created_at' => '2021-06-15 13:59:08',
                'updated_at' => '2021-06-15 13:59:08',
            ],
            [
               
                'cat_sector_inversionista_id'   => 3,
                'nombre'       => 'Corcho y madera',
                'created_at' => '2021-06-15 13:59:08',
                'updated_at' => '2021-06-15 13:59:08',
            ],
            [
              
                'cat_sector_inversionista_id'   => 3,
                'nombre'       => 'Pasta y desperdicios de papel',
                'created_at' => '2021-06-15 13:59:08',
                'updated_at' => '2021-06-15 13:59:08',
            ],
            [
              
                'cat_sector_inversionista_id'   => 3,
                'nombre'       => 'Fibras textiles (excepto las mechas (tops) y otras formas de lana peinada) y sus
                desperdicios (no manufacturadas en hilados, hilos o tejidos)',
                'created_at' => '2021-06-15 13:59:08',
                'updated_at' => '2021-06-15 13:59:08',
            ],
            [
               
                'cat_sector_inversionista_id'   => 3,
                'nombre'       => 'Abonos en bruto y minerales en bruto (excepto carbón, petróleo y piedras preciosas)',
                'created_at' => '2021-06-15 13:59:08',
                'updated_at' => '2021-06-15 13:59:08',
            ],
            [
                
                'cat_sector_inversionista_id'   => 3,
                'nombre'       => 'Menas y desechos de metales',
                'created_at' => '2021-06-15 13:59:08',
                'updated_at' => '2021-06-15 13:59:08',
            ],
            [
               
                'cat_sector_inversionista_id'   => 3,
                'nombre'       => 'Productos animales y vegetales en bruto, n.e.p.',
                'created_at' => '2021-06-15 13:59:08',
                'updated_at' => '2021-06-15 13:59:08',
            ],
            [
               
                'cat_sector_inversionista_id'   => 4,
                'nombre'       => 'Hulla, coque y briquetas',
                'created_at' => '2021-06-15 13:59:08',
                'updated_at' => '2021-06-15 13:59:08',
            ],
            [
               
                'cat_sector_inversionista_id'   => 4,
                'nombre'       => 'Petróleo, productos derivados del petróleo y productos conexos',
                'created_at' => '2021-06-15 13:59:08',
                'updated_at' => '2021-06-15 13:59:08',
            ],
            [
              
                'cat_sector_inversionista_id'   => 4,
                'nombre'       => 'Gas natural y manufacturado',
                'created_at' => '2021-06-15 13:59:08',
                'updated_at' => '2021-06-15 13:59:08',
            ],
            [
              
                'cat_sector_inversionista_id'   => 4,
                'nombre'       => 'Corriente eléctrica',
                'created_at' => '2021-06-15 13:59:08',
                'updated_at' => '2021-06-15 13:59:08',
            ],
            [
                
                'cat_sector_inversionista_id'   => 5,
                'nombre'       => 'Aceites y grasas de origen animal',
                'created_at' => '2021-06-15 13:59:08',
                'updated_at' => '2021-06-15 13:59:08',
            ],
            [
               
                'cat_sector_inversionista_id'   => 5,
                'nombre'       => 'Aceites y grasas fijos de origen vegetal, en bruto, refinados o fraccionados',
                'created_at' => '2021-06-15 13:59:08',
                'updated_at' => '2021-06-15 13:59:08',
            ],
            [
                
                'cat_sector_inversionista_id'   => 5,
                'nombre'       => 'Aceites y grasas de origen animal o vegetal, elaborados; ceras de origen animal o
                vegetal; mezclas o preparados no comestibles de grasas o aceites de origen animal o
                vegetal, n.e.p.',
                'created_at' => '2021-06-15 13:59:08',
                'updated_at' => '2021-06-15 13:59:08',
            ],
            [
               
                'cat_sector_inversionista_id'   => 6,
                'nombre'       => 'Productos químicos orgánicos',
                'created_at' => '2021-06-15 13:59:08',
                'updated_at' => '2021-06-15 13:59:08',
            ],
            [
               
                'cat_sector_inversionista_id'   => 6,
                'nombre'       => 'Productos químicos inorgánicos',
                'created_at' => '2021-06-15 13:59:08',
                'updated_at' => '2021-06-15 13:59:08',
            ],
            [
               
                'cat_sector_inversionista_id'   => 6,
                'nombre'       => 'Materias tintóreas, curtientes y colorantes',
                'created_at' => '2021-06-15 13:59:08',
                'updated_at' => '2021-06-15 13:59:08',
            ],
            [
              
                'cat_sector_inversionista_id'   => 6,
                'nombre'       => 'Productos medicinales y farmacéuticos',
                'created_at' => '2021-06-15 13:59:08',
                'updated_at' => '2021-06-15 13:59:08',
            ],
            [
                
                'cat_sector_inversionista_id'   => 6,
                'nombre'       => 'Aceites esenciales y resinoides y productos de perfumería; preparados de tocador y
                para pulir y limpiar',
                'created_at' => '2021-06-15 13:59:08',
                'updated_at' => '2021-06-15 13:59:08',
            ],
            [
               
                'cat_sector_inversionista_id'   => 6,
                'nombre'       => 'Abonos',
                'created_at' => '2021-06-15 13:59:08',
                'updated_at' => '2021-06-15 13:59:08',
            ],
            [
               
                'cat_sector_inversionista_id'   => 6,
                'nombre'       => 'Plásticos en formas primarias',
                'created_at' => '2021-06-15 13:59:08',
                'updated_at' => '2021-06-15 13:59:08',
            ],
            [
                
                'cat_sector_inversionista_id'   => 6,
                'nombre'       => 'Plásticos en formas no primarias',
                'created_at' => '2021-06-15 13:59:08',
                'updated_at' => '2021-06-15 13:59:08',
            ],
            [
               
                'cat_sector_inversionista_id'   => 6,
                'nombre'       => 'Materias y productos químicos, n.e.p.',
                'created_at' => '2021-06-15 13:59:08',
                'updated_at' => '2021-06-15 13:59:08',
            ],
            [
                
                'cat_sector_inversionista_id'   => 7,
                'nombre'       => 'Cuero y manufacturas de cuero, n.e.p., y pieles finas curtidas',
                'created_at' => '2021-06-15 13:59:08',
                'updated_at' => '2021-06-15 13:59:08',
            ],
            [
                
                'cat_sector_inversionista_id'   => 7,
                'nombre'       => 'Manufacturas de caucho, n.e.p.',
                'created_at' => '2021-06-15 13:59:08',
                'updated_at' => '2021-06-15 13:59:08',
            ],
            [
                
                'cat_sector_inversionista_id'   => 7,
                'nombre'       => 'Manufacturas de corcho y de madera (excepto muebles)',
                'created_at' => '2021-06-15 13:59:08',
                'updated_at' => '2021-06-15 13:59:08',
            ],
            [
                
                'cat_sector_inversionista_id'   => 7,
                'nombre'       => 'Papel, cartón y artículos de pasta de papel, de papel o de cartón',
                'created_at' => '2021-06-15 13:59:08',
                'updated_at' => '2021-06-15 13:59:08',
            ],
            [
                
                'cat_sector_inversionista_id'   => 7,
                'nombre'       => 'Hilados, tejidos, artículos confeccionados de fibras textiles, n.e.p., y productos
                conexos',
                'created_at' => '2021-06-15 13:59:08',
                'updated_at' => '2021-06-15 13:59:08',
            ],
            [
                
                'cat_sector_inversionista_id'   => 7,
                'nombre'       => 'Manufacturas de minerales no metálicos, n.e.p.',
                'created_at' => '2021-06-15 13:59:08',
                'updated_at' => '2021-06-15 13:59:08',
            ],
            [
               
                'cat_sector_inversionista_id'   => 7,
                'nombre'       => 'Hierro y acero 67 9 35 133 Metales no ferrosos',
                'created_at' => '2021-06-15 13:59:08',
                'updated_at' => '2021-06-15 13:59:08',
            ],
            [
                
                'cat_sector_inversionista_id'   => 7,
                'nombre'       => 'Manufacturas de metales, n.e.p.',
                'created_at' => '2021-06-15 13:59:08',
                'updated_at' => '2021-06-15 13:59:08',
            ],
            [
                
                'cat_sector_inversionista_id'   => 8,
                'nombre'       => 'Maquinaria y equipo generadores de fuerza',
                'created_at' => '2021-06-15 13:59:08',
                'updated_at' => '2021-06-15 13:59:08',
            ],
            [
                
                'cat_sector_inversionista_id'   => 8,
                'nombre'       => 'Maquinarias especiales para determinadas industrias',
                'created_at' => '2021-06-15 13:59:08',
                'updated_at' => '2021-06-15 13:59:08',
            ],
            [
                
                'cat_sector_inversionista_id'   => 8,
                'nombre'       => 'Máquinas para trabajar metales',
                'created_at' => '2021-06-15 13:59:08',
                'updated_at' => '2021-06-15 13:59:08',
            ],
            [
                
                'cat_sector_inversionista_id'   => 8,
                'nombre'       => 'Maquinaria y equipo industrial en general, n.e.p., y partes y piezas de máquinas, n.e.p.',
                'created_at' => '2021-06-15 13:59:08',
                'updated_at' => '2021-06-15 13:59:08',
            ],
            [
                
                'cat_sector_inversionista_id'   => 8,
                'nombre'       => 'Máquinas de oficina y máquinas de procesamiento automático de datos',
                'created_at' => '2021-06-15 13:59:08',
                'updated_at' => '2021-06-15 13:59:08',
            ],
            [
                
                'cat_sector_inversionista_id'   => 8,
                'nombre'       => 'Aparatos y equipo para telecomunicaciones y para grabación y reproducción de sonido',
                'created_at' => '2021-06-15 13:59:08',
                'updated_at' => '2021-06-15 13:59:08',
            ],
            [
                
                'cat_sector_inversionista_id'   => 8,
                'nombre'       => 'Maquinaria, aparatos y artefactos eléctricos, n.e.p., y sus partes y piezas eléctricas
                (incluso las contrapartes no eléctricas, n.e.p., del equipo eléctrico de uso doméstico)',
                'created_at' => '2021-06-15 13:59:08',
                'updated_at' => '2021-06-15 13:59:08',
            ],
            [
                
                'cat_sector_inversionista_id'   => 8,
                'nombre'       => 'Vehículos de carretera (incluso aerodeslizadores)',
                'created_at' => '2021-06-15 13:59:08',
                'updated_at' => '2021-06-15 13:59:08',
            ],
            [
                
                'cat_sector_inversionista_id'   => 8,
                'nombre'       => 'Otro equipo de transporte',
                'created_at' => '2021-06-15 13:59:08',
                'updated_at' => '2021-06-15 13:59:08',
            ],
            [
                
                'cat_sector_inversionista_id'   => 9,
                'nombre'       => 'Edificios prefabricados; artefactos y accesorios sanitarios y para sistemas de
                conducción de aguas, calefacción y alumbrado, n.e.p.',
                'created_at' => '2021-06-15 13:59:08',
                'updated_at' => '2021-06-15 13:59:08',
            ],
            [
                
                'cat_sector_inversionista_id'   => 9,
                'nombre'       => 'Muebles y sus partes; camas, colchones, somieres, cojines y artículos rellenos
                similares',
                'created_at' => '2021-06-15 13:59:08',
                'updated_at' => '2021-06-15 13:59:08',
            ],
            [
                
                'cat_sector_inversionista_id'   => 9,
                'nombre'       => 'Artículos de viajes, bolsos de mano y otros artículos análogos para contener objetos',
                'created_at' => '2021-06-15 13:59:08',
                'updated_at' => '2021-06-15 13:59:08',
            ],
            [
                
                'cat_sector_inversionista_id'   => 9,
                'nombre'       => 'Prendas y accesorios de vestir',
                'created_at' => '2021-06-15 13:59:08',
                'updated_at' => '2021-06-15 13:59:08',
            ],
            [
                
                'cat_sector_inversionista_id'   => 9,
                'nombre'       => 'Calzado',
                'created_at' => '2021-06-15 13:59:08',
                'updated_at' => '2021-06-15 13:59:08',
            ],
            [
                
                'cat_sector_inversionista_id'   => 9,
                'nombre'       => 'Instrumentos y aparatos profesionales, científicos y de control, n.e.p.',
                'created_at' => '2021-06-15 13:59:08',
                'updated_at' => '2021-06-15 13:59:08',
            ],
            [
                
                'cat_sector_inversionista_id'   => 9,
                'nombre'       => 'Aparatos, equipos y materiales fotográficos y artículos de óptica, n.e.p., relojes',
                'created_at' => '2021-06-15 13:59:08',
                'updated_at' => '2021-06-15 13:59:08',
            ],
            [
                
                'cat_sector_inversionista_id'   => 9,
                'nombre'       => 'Artículos manufacturados diversos, n.e.p.',
                'created_at' => '2021-06-15 13:59:08',
                'updated_at' => '2021-06-15 13:59:08',
            ],
            [
                
                'cat_sector_inversionista_id'   => 10,
                'nombre'       => 'Paquetes postales no clasificados según su naturaleza',
                'created_at' => '2021-06-15 13:59:08',
                'updated_at' => '2021-06-15 13:59:08',
            ],
            [
                
                'cat_sector_inversionista_id'   => 10,
                'nombre'       => 'Operaciones y mercancías especiales no clasificadas según su naturaleza',
                'created_at' => '2021-06-15 13:59:08',
                'updated_at' => '2021-06-15 13:59:08',
            ],
            [
               
                'cat_sector_inversionista_id'   => 10,
                'nombre'       => 'Monedas (excepto de oro), que no tengan curso legal',
                'created_at' => '2021-06-15 13:59:08',
                'updated_at' => '2021-06-15 13:59:08',
            ],
            [
                
                'cat_sector_inversionista_id'   => 10,
                'nombre'       => 'Oro no monetario (excepto minerales y concentrados de oro)
                n.e.p.No especificado en otra parte',
                'created_at' => '2021-06-15 13:59:08',
                'updated_at' => '2021-06-15 13:59:08',
            ],

            
        ];

        CatActividadEcoInversionista::insert($sectorInv);
    }
}
