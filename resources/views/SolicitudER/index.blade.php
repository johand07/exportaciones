@extends('templates/layoutlte')
@section('content')
<div class="panels">
<div class="row">
      <div class="col-md-1">
      <a href="{{url('exportador/create')}}"class="btn btn-primary">
          <span class="glyphicon glyphicon-file" title="Agregar" aria-hidden="true"></span>
           Nueva Solicitud ER
        </a>
        
      </div>
      <div class="col-md-1">
      </div>
      <div class="col-md-3"></div>
   </div>
   <hr>

    <table id="listaAgenteAduanal" class="table table-striped table-bordered" style="width:100%">
            <thead>
                <tr>
                   
                    <th>Número de Solicitud </th>
                    <th>Tipo de Solicitud</th>
                    <th>Estatus Solicitud</th>
                    <th>Fecha Solicitud</th>
                    <th>Monto Solicitud</th>
                    <th>Descarga</th>
                    <th>Acciones</th>
                    
                </tr>
            </thead>
            <tbody>
              @foreach($solicitudes as $key=> $solicitud)

                <tr>

                    <td>{{$solicitud->id}}</td>
                    <td>{{$solicitud->tiposolictud->solicitud}}</td>
                    <td>@if(!is_null($solicitud->gen_status_id)){{$solicitud->status->nombre_status}}@endif</td>
                    <td>{{$solicitud->fsolicitud}}</td>
                    <td>{{$solicitud->monto_solicitud}}</td>
                    
                    <td>@if(!is_null($solicitud->monto_solicitud) && ($solicitud->monto_solicitud > 0))
                     <!--span style="color:green">Finalizada</!--span -->
                     @if( !is_null($solicitud->monto_solicitud)) 
                      @if(Helper::consulta($solicitud->id,1)) 
                         @if($solicitud->tipo_solicitud_id==1)
                            <a href="{{action('PdfController@planillaEr',array('id'=>$solicitud->id))}}" class="glyphicon glyphicon-file btn btn-default btn-sm" title="Planilla ER" aria-hidden="true"></a>
                             @else
                            <a href="{{action('PdfController@planillaTecno',array('id'=>$solicitud->id))}}" class="glyphicon glyphicon-file btn btn-default btn-sm" title="Planilla ER" aria-hidden="true"></a>
                          @endif
                       @endif
                   @endif
                     @else
                     <span style="color:red">Incompleto</span>
                    @endif
                </td>
                    <td>

                   @if(!Helper::consulta($solicitud->id,null))

                   <a href="{{route('FacturaSolicitud.edit',$solicitud->id)}}" class="glyphicon glyphicon-edit btn btn-primary btn-sm" title="Modificar" aria-hidden="true"></a>
                   
                   @endif
                 

  
                   
            <!-- a href="{{ route('factura-solicitud.delete-er', ['id' =>$solicitud->id]) }}" class="glyphicon glyphicon-trash btn  btn btn-danger  btn-sm" aria-hidden=""></!-- -->
            <a href="#" class="glyphicon glyphicon-trash btn  btn btn-danger  btn-sm" aria-hidden="" data-remove="true" id="{{$solicitud->id}}" ></a>
           
                    </td>
                 
                </tr>
               @endforeach
            </tbody>
        </table>
  </div>
</div>
</div>
<script>
$(document).on('click','[data-remove]',function(){
               var id= $(this).attr("id");
             
                 swal({
                     title: "¿Eliminar?",
                     text: "¿Está seguro de eliminar esta Solicitud?",
                     type: "warning",
                     showCancelButton: true,
                     confirmButtonColor: "#DD6B55",
                     confirmButtonText: "Si!, Borrarlo!",
                     cancelButtonText: "No, Cancelar!",
                     closeOnConfirm: false,
                     closeOnCancel: false,
                     showLoaderOnConfirm: true
                     },

    function(isConfirm){
         if (isConfirm) {
    
              $.ajax({
               'type':'get',
               'url': '{{ route('factura-solicitud.delete-er', ['id' =>0]) }}',
               'data':{'id':id},
               success: function(data){
                //$('#row'+id_boton).remove();
                if(data>0){
                   swal("Eliminación exitosa","Solicitud DVD Eliminado ", "success");
                   location.reload();
                  }
                 }
             });
         }else{
             swal("Cancelado", "No se ha procesado la eliminación", "warning");
        }
        });
       
 });

 </script>
@stop
