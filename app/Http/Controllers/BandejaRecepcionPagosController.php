<?php

namespace App\Http\Controllers;

use App\Models\ReportePago;
use Illuminate\Http\Request;

class BandejaRecepcionPagosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $titulo="Validación de Pagos";
        $descripcion="Recepción de Pagos";
        return view('bandejaRecepcionPagos.index', compact('titulo','descripcion'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Reporte_Pago  $reporte_Pago
     * @return \Illuminate\Http\Response
     */
    public function show(Reporte_Pago $reporte_Pago)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Reporte_Pago  $reporte_Pago
     * @return \Illuminate\Http\Response
     */
    public function edit(Reporte_Pago $reporte_Pago)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Reporte_Pago  $reporte_Pago
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Reporte_Pago $reporte_Pago)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Reporte_Pago  $reporte_Pago
     * @return \Illuminate\Http\Response
     */
    public function destroy(Reporte_Pago $reporte_Pago)
    {
        //
    }
}
