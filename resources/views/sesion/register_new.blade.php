@extends('layouts.app')
@section('content')
@component('modal_dialogo',['arancel'=>$arancel])

@slot('header') <span>Código Arancelario</span> @endslot

@slot('body')  <div id="arancel"></div>

@endslot

@slot('footer')@endslot

@endcomponent
<div class="container">

  <form id="form" name="inscripcion" method="POST">
        {{ csrf_field() }}
         <!--***************Parte 1 Registro****************************-->

      <div id="datos_persona1">
        <ol class="breadcrumb">
          <li class="active">Paso 1</li>
        </ol>
        <div class="panel panel-primary">
          <div class="panel-heading">DATOS DE LA EMPRESA</div>

          <div class="panel-body">

            <div class="row">
              <div class="col-md-3">
                <div class="form-group">
                  <label>Nombre o Razón Social</label><br>
                
                  <input class="form-control" type="text" id="razon_social" name="razon_social" onkeypress="return soloNumerosyLetras(event)" maxlength="100">
                </div>
              </div>
              <div class="col-md-1">
                <div class="form-group">
                  <label>Siglas</label><br>
                  <input class="form-control" type="text" id="siglas" name="siglas" onkeypress="return soloLetras(event)" maxlength="50">
                </div>
              </div>
              <div class="col-md-2">
                <div class="form-group">
                  <label>Rif</label><br>
                  <input class="form-control" type="text" id="rif" name="rif" value="{{$rif}}" readonly="true" maxlength="11">
                </div>
              </div>
              <div class="col-md-2">
                <div class="form-group">
                  <label>Sector</label><br>
                  <select class="form-control" id="id_gen_sector" name="id_gen_sector">
                    <option value="">--Seleccione una Opción--</option>
                    @if (isset($sectores))
                        @foreach ($sectores as $sector)
                           <option value="{{ $sector->id }}">{{ $sector->descripcion_sector }}</option>
                        @endforeach
                    @endif
                  </select>
                </div>
              </div>
              <div class="col-md-2">
                <div class="form-group">
                  <label>Actividad Económica</label><br>
                  <select class="form-control" id="id_gen_actividad_eco" name="id_gen_actividad_eco">
                    <option value="">--Seleccione una Opción--</option>
                  </select>
                </div>
              </div>
              <div class="col-md-2">
                <div class="form-group">
                  <label>Tipo de Empresa</label><br>
                  <select class="form-control" id="id_tipo_empresa" name="id_tipo_empresa">
                    <option value="">--Seleccione una Opción--</option>
                    @if (isset($tipos_empresas))
                        @foreach ($tipos_empresas as $tipo_empresa)
                           <option value="{{ $tipo_empresa->id }}">{{ $tipo_empresa->d_tipo_empresa }}</option>
                        @endforeach
                    @endif
                  </select>
                </div>
              </div>
            </div>
            <div class="panel panel-primary">
                <div class="panel-heading">TIPO DE ACTIVIDAD ECONÓMICA</div>
            </div>
            <div class="row">
              <div class="col-md-3">
                 <input type="checkbox" name="export" id="export" value="1" class="checkbox-inline"><label>Exportador</label>
              </div>
              <div class="col-md-3">
                <input type="checkbox" name="invers" id="invers" value="1" class="checkbox-inline"><label>Inversionista</label>
              </div>
              <div class="col-md-3">
                 <input type="checkbox" name="produc" id="produc" value="1" class="checkbox-inline"><label>Productor</label>
              </div>
              <div class="col-md-3">
                 <input type="checkbox" name="comerc" id="comerc" value="1" class="checkbox-inline"><label>Comercializadora</label>
              </div>
            </div>
            <br>
              <div class="panel panel-primary">
                <div class="panel-heading">DIRECCIÓN</div>
              </div>
            <div class="row">
              <div class="col-md-3">
                <div class="form-group">
                  <label>País</label>
                  <select class="form-control" id="id_pais" name="id_pais">
                    <option value="">--Seleccione una Opción--</option>
                    @if (isset($paises))
                        @foreach ($paises as $pais)
                           <option value="{{ $pais->id }}">{{ $pais->dpais }}</option>
                        @endforeach
                    @endif
                  </select>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label>Estado</label><br>
                  <select class="form-control" id="id_estado" name="id_estado">
                    <option value="">--Seleccione una Opción--</option>
                    @if (isset($estados))
                        @foreach ($estados as $estado)
                           <option value="{{ $estado->id }}">{{ $estado->estado }}</option>
                        @endforeach
                    @endif
                  </select>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label>Municipio</label><br>
                  <select class="form-control" id="id_municipio" name="id_municipio">
                    <option>--Seleccione una Opción--</option>
                  </select>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label>Parroquia</label><br>
                  <select class="form-control" id="id_parroquia" name="id_parroquia">
                    <option>--Seleccione una Opción--</option>
                  </select>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label>Dirección</label><br>
                  <input class="form-control" type="text" id="direccion" name="direccion" onkeypress="return soloNumerosyLetras(event)" maxlength="200">
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label>Teléfono Local</label><br>
                  <input class="form-control" type="text" id="telefono_local" name="telefono_local" onkeypress="return soloNumeros(event)" maxlength="11"><small>Ejemplo:02125554555</small>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label>Teléfono Celular</label><br>
                  <input class="form-control" type="text" id="telefono_movil" name="telefono_movil" onkeypress="return soloNumeros(event)" maxlength="11"><small>Ejemplo:04165554555</small>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="col-md-4"></div>
                <div class="col-md-4 text-center">
                  <div class="form-group">

                    <a class="btn btn-primary" href="#" id="seguir1">Siguiente</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
             <!--***************Parte 2 Registro****************************-->
          <div id="datos_persona2" style="display: none;">
            <ol class="breadcrumb">
              <li><a href="#" id="paso1">Paso 1</a></li>
              <li class="active" id="paso2">Paso 2</li>
            </ol>
            <div class="panel panel-primary">
              <div class="panel-heading">DATOS DE LA PERSONA JURIDICA O FIRMA PERSONAL</div>

              <div class="panel-body">
                <div class="row">
                  <div class="col-md-4">
                    <div class="form-group">
                      <label>Registro de Circunscripción Judicial</label>
                      <select class="form-control" id="id_circuns_judicial" name="id_circuns_judicial">
                        <option value="">--Seleccione una Opción--</option>
                        @if (isset($circuns_judiciales))
                            @foreach ($circuns_judiciales as $circuns_judicial)
                               <option value="{{ $circuns_judicial->id }}">{{ $circuns_judicial->registro }}</option>
                            @endforeach
                        @endif
                      </select>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <label>Número</label><br>
                      <input class="form-control" type="text" id="num_registro" name="num_registro" onkeypress="return soloNumerosyLetras(event)" maxlength="60">
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <label>Tomo</label><br>
                      <input class="form-control" type="text" id="tomo" name="tomo" onkeypress="return soloNumerosyLetras(event)" maxlength="30">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-4">
                    <div class="form-group">
                      <label>Folio</label><br>
                      <input class="form-control" type="text" id="folio" name="folio" onkeypress="return soloNumerosyLetras(event)" maxlength="30">
                    </div>
                  </div>
                  <div class="col-md-4">
                      <div class="form-group">
                        <label>Capital Social</label><br>
                        <input class="form-control" type="text" id="capital_social" name="capital_social" value=""onkeypress="return soloNumeros(event)" maxlength="200">
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <label>Fecha Registro</label>
                      <div class="input-group date">
                         <input id="f_registro_mer" name="f_registro_mer" type="text" class="form-control" onchange="validarfechaRM(this.value,'f_registro_mer');" readonly="readonly"><span class="input-group-addon"><i class="glyphicon glyphicon-th" ></i></span>
                     </div>
                    </div>
                  </div>
                </div>
                <div class="panel panel-primary">
                  <div class="panel-heading">DATOS DEL PRESIDENTE</div>
                </div>
                <div class="row">
                  <div class="col-md-3">
                    <div class="form-group">
                      <label>Nombre</label><br>
                      <input class="form-control" type="text" id="nombre_presid" name="nombre_presid" onkeypress="return soloLetras(event)" maxlength="50">
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="form-group">
                      <label>Apellido</label><br>
                      <input class="form-control" type="text" id="apellido_presid" name="apellido_presid" onkeypress="return soloLetras(event)" maxlength="50">
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="form-group">
                      <label>Cédula</label><br>
                      <input class="form-control" type="text" id="ci_presid" name="ci_presid" onkeypress="return CedulaFormatrep(this,'Cedula de Indentidad Invalida',-1,true,event)" maxlength="11"><small>Ejemplo:V-00000000</small>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="form-group">
                      <label>Cargo</label><br>
                      <input class="form-control" type="text" id="cargo_presid" name="cargo_presid" onkeypress="return soloLetras(event)" maxlength="80" value="Presidente">
                    </div>
                  </div>
                </div>
                <div class="panel panel-primary">
                  <div class="panel-heading">DATOS DEL REPRESENTANTE LEGAL</div>
                </div>
                <div class="row">
                  <div class="col-md-3">
                    <div class="form-group">
                      <label>Nombre</label><br>
                      <input class="form-control" type="text" id="nombre_repre" name="nombre_repre" onkeypress="return soloLetras(event)" maxlength="50">
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="form-group">
                      <label>Apellido</label><br>
                      <input class="form-control" type="text" id="apellido_repre" name="apellido_repre" onkeypress="return soloLetras(event)" maxlength="50">
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="form-group">
                      <label>Cédula</label><br>
                      <input class="form-control" type="text" id="ci_repre" name="ci_repre" onkeypress="return CedulaFormatrep(this,'Cedula de Indentidad Invalida',-1,true,event)" maxlength="11"><small>Ejemplo:V-00000000</small>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="form-group">
                      <label>Cargo</label><br>
                      <input class="form-control" type="text" id="cargo_repre" name="cargo_repre" onkeypress="return soloLetras(event)" maxlength="80">
                    </div>
                  </div>
                </div>
                <div class="panel panel-primary">
                  <div class="panel-heading">DATOS DE LOS ACCIONISTAS</div>
                </div>
                <div class="row">
                  <div class="col-md-3"></div>
                  <div class="col-md-6 text-center">
                    <div class="form-group">
                      {!! Form::label('','¿Posée Accionista?')!!}
                      {!! Form::label('','Si')!!}
                      {!! Form::radio('accionista','Si',false,['class'=>'radio-inline','id'=>'accion_si'])!!}
                      {!! Form::label('','No')!!}
                      {!! Form::radio('accionista','No',true,['class'=>'radio-inline','id'=>'accion_no'])!!}
                    </div>
                  </div>
                  <div class="col-md-3"></div>
                </div>

                  <table class="table table-bordered" id="fields" style="display:none;">
                    <tr>
                      <th>Nombre </th>
                      <th>Apellido</th>
                      <th>Cédula</th>
                      <th>Cargo</th>
                    </tr>
                    <tr id="row">
                      <td>{!!Form::text('nombre_accionista[]',null,['class'=>'form-control','id'=>'nombre_accionista','onkeypress'=>'return soloLetras(event)','maxlength'=>'50'])!!}</td>
                      <td>{!!Form::text('apellido_accionista[]',null,['class'=>'form-control','id'=>'apellido_accionista','onkeypress'=>'return soloLetras(event)','maxlength'=>'50'])!!}</td>

                      <td>{!!Form::text('cedula_accionista[]',null,['class'=>'form-control','id'=>'cedula_accionista','onkeypress'=>'return CedulaFormatrep(this,"Cedula de Indentidad Invalida",-1,true,event)','maxlength'=>'11'])!!}<small>Ejemplo:V-00000000</small></td>

                      <td>{!!Form::text('cargo_accionista[]',null,['class'=>'form-control','id'=>'cargo_accionista','onkeypress'=>'return soloLetras(event)','maxlength'=>'80'])!!}</td>

                      <td><button  name="add" type="button" id="add" value="add more" class="btn btn-success">Agregar</button></td>
                    </tr>
                  </table>

                <div class="row">
                  <div class="col-md-12">
                    <div class="col-md-4"></div>
                    <div class="col-md-4 text-center">
                      <div class="form-group">
                        <a class="btn btn-primary" href="#" id="atras1">Atrás</a>
                        <a class="btn btn-primary" href="#" id="seguir2">Siguiente</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

  <!--***************Parte 3 Registro****************************-->
          <div id="datos_persona3" style="display: none;">
            <ol class="breadcrumb">
              <li><a href="#" id="paso11">Paso 1</a></li>
              <li><a href="#" id="paso22">Paso 2</a></li>
              <li class="active" id="paso3">Paso 3</li>
            </ol>


            <div class="panel panel-primary">
              <div class="panel-heading">DATOS DE LOS PRODUCTOS QUE PRODUCE Y/O COMERCIALIZA</div>
             <div class="panel-body">
                <div class="row">
                   <div class="col-md-1"></div>
                   <div class="col-md-10 text-center">
                      {!! Form::label('','¿Exporta Productos de Servicios y/o Tecnología?',['class'=>'form-control'])!!}
                      {!! Form::label('','Si',['class'=>'radio-inline'])!!}
                      {!! Form::radio('produc_tecno','1',false,['class'=>'radio-inline','id'=>'prod_tecnologia_si'])!!}
                      {!! Form::label('','No',['class'=>'radio-inline'])!!}
                      {!! Form::radio('produc_tecno','2',false,['class'=>'radio-inline','id'=>'prod_tecnologia_no'])!!}
                    </div>
                  <div class="col-md-1"></div>
                </div><br>
                <div class="row" id="tecno_especifique" style="display: none;">
                  <div class="col-md-1"></div>
                  <div class="col-md-10">
                    {!! Form::label('','Especifique')!!}
                    {!!Form::text('especifique_tecno',null,['class'=>'form-control','id'=>'especifique_tecno','onkeypress'=>'return soloLetras(event)','maxlength'=>'100'])!!}
                  </div>
                  <div class="col-md-1"></div>
                </div>
               <div class="row" id="codigo_arancelarios" style="display: none;">
                   <div class="col-md-3"></div>
                   <div class="col-md-6 text-center">
                      {!!Form::radio('tipo_arancel','1',false,['class'=>'radio-inline','id'=>'cod_andina'])!!}
                      {!! Form::label('','Nandina',['class'=>'radio-inline'])!!}
                      {!! Form::radio('tipo_arancel','2',false,['class'=>'radio-inline','id'=>'cod_mercosur'])!!}
                      {!! Form::label('','Mercosur',['class'=>'radio-inline'])!!}
                    </div>
                  <div class="col-md-3"></div>
                </div><br>
                <table class="table table-bordered text-center" id="fields2" style="display: none;">

                 <tr>
                    <th>Código Arancelario </th>
                    <th>Descripción Arancelaria</th>
                    <th>Descripción Comercial</th>
                    <th>Unidad de Medida</th>
                    <th>Capacidad Operativa Anual</th>
                    <th>Capacidad Instalada Anual</th>
                    <th>Capacidad de Almacenamiento Anual</th>
                    <th></th>

                  </tr>
                  <tr id="fila">

                    <td>{!!Form::text('cod_arancel[]',null,['class'=>'form-control','data-toggle'=>"modal",'data-target'=>"#modal",'id'=>'cod_arancel','readonly'=>'true'])!!}
                    </td>
                    <td>{!!Form::text('descrip_arancel[]',null,['class'=>'form-control','id'=>'descrip_arancel','onkeypress'=>'return soloLetras(event)'])!!}</td>
                    <td>{!!Form::text('descrip_comercial[]',null,['class'=>'form-control','id'=>'descrip_comercial','onkeypress'=>'return soloLetras(event)'])!!}</td>
                    <td>{!!Form::select('gen_unidad_medida_id[]',$unidad_medida,null,['placeholder'=>'--Seleccione una Unidad de Medida--', 'class'=>'form-control','id'=>'gen_unidad_medida_id']) !!}</td>
                    <td>{!!Form::text('cap_opera_anual[]',null,['class'=>'form-control','id'=>'cap_opera_anual','onkeypress'=>'return soloNumeros(event)'])!!}</td>
                    <td>{!!Form::text('cap_insta_anual[]',null,['class'=>'form-control','id'=>'cap_insta_anual','onkeypress'=>'return soloNumerosletrasyPorcentaje(event)'])!!}</td>
                    <td>{!!Form::text('cap_alamcenamiento[]',null,['class'=>'form-control','id'=>'cap_alamcenamiento','onkeypress'=>'return soloNumeros(event)'])!!}</td>

                    <td><button  name="add2" type="button" id="add2" value="add more" class="btn btn-success">Agregar</button></td>
                  </tr>

                </table>
                <div class="row">
                  <div class="col-md-12">
                    <div class="col-md-4"></div>
                    <div class="col-md-4 text-center">
                      <div class="form-group">
                        <a class="btn btn-primary" href="#" id="atras2">Atrás</a>
                        <a class="btn btn-primary" href="#" id="seguir3">Siguiente</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>



<!--***************Parte 4 Registro****************************-->


          <div id="datos_persona4" style="display: none;">
            <ol class="breadcrumb">
              <li><a href="#" id="paso111">Paso 1</a></li>
              <li><a href="#" id="paso222">Paso 2</a></li>
              <li><a href="#" id="paso333">Paso 3</a></li>
              <li class="active" id="paso4">Paso 4</li>
            </ol>
            <div class="panel panel-primary">
              <div class="panel-heading">DATOS DE ACCESO</div>
            <div class="panel-body">
              <div class="row">
                <div class="col-md-3">
                  <div class="form-group">
                    <label>Correo Electrónico</label><br>
                    <input class="form-control" type="email" id="email" name="email" onkeyup="minuscula(this.value,'#email');" maxlength="150">
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group">
                    <label>Correo Alternativo</label><br>
                    <input class="form-control" type="email" id="email_seg" name="email_seg" onkeyup="minuscula(this.value,'#email_seg');" maxlength="150">
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group">
                    <label>Contraseña</label><br>
                    <input class="form-control" type="password" id="password" name="password" maxlength="100">
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group">
                    <label>Confirme Contraseña</label><br>
                    <input class="form-control" type="password" id="password_confirm" name="password_confirm" maxlength="100">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group" id="lista_pregunta">
                    <label>Pregunta de Seguridad</label>
                      <select class="form-control" id="id_cat_preguntas_seg" name="id_cat_preguntas_seg" onchange="preguntaSeg ();">
                        <option value="">--Seleccione una Opción--</option>
                        @if (isset($preguntas_seg))
                            @foreach ($preguntas_seg as $pregunta_seg)
                               <option value="{{ $pregunta_seg->id }}">{{ $pregunta_seg->descripcion_preg }}</option>
                            @endforeach
                        @endif
                      </select>
                  </div>
                  <div class="form-group" id="preg_alternativa" style="display: none;">
                    <label>Pregunta Alternativa</label><br>
                    <input class="form-control" type="text" id="pregunta_alter" name="pregunta_alter" onkeypress="return soloNumerosyLetras(event)" maxlength="200">
                    <span style="color:#3097D1;cursor:pointer;" id="lista_preg">lista de preguntas</span>
                  </div>


                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Respuesta de Seguridad</label><br>
                    <input class="form-control" type="text" id="respuesta_seg" name="respuesta_seg" onkeypress="return soloNumerosyLetras(event)" maxlength="100">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="col-md-4"></div>
                  <div class="col-md-4 text-center">
                    <div class="form-group">

                      <a class="btn btn-primary" href="#" id="atras3">Atras</a>

                      <a class="btn btn-primary" href="#" id="seguir4" data-toggle="modal" data-target="#myModal">Completar inscripción</a>

                    </div>
                  </div>
                  <div class="col-md-4"></div>
                </div>
              </div>
            </div>
          </div>
        </div>
<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Declaración Jurada del Exportador</h4>
      </div>
      <div class="modal-body">
      <div class="col-md-12">
        <p class="text-justify">Usted en su carácter de (Presidente, representante legal, apoderado), de la empresa, declara conocer las condiciones y términos establecidos en el basamento legal aplicable para esta solicitud, así como las consecuencias que de ellas se deslinden; a tales efectos, manifiesta que tanto la información como la documentación suministrada, son ciertas, fidedignas e irrefutables, ya que reflejan con total precisión, los elementos de comprobación necesarios para su validez y eficacia; en este sentido autorizo al Ministerio del Poder Popular de Economia, Finanzas y Comercio Exterior, o a quien este designe, a encauzar todos los medios de confirmación que considere necesarios, sin más limitaciones que las establecidas en la Constitución y las Leyes de la República, sin perjuicio de las acciones civiles, penales, administrativas que como resultado de dicha comprobación tuvieren lugar en razón de una omisión o suministro de información falsa a la Administración Pública.
 
        De igual modo declara que, la recepción de toda notificación, mensaje de datos o incidencias relacionadas con esta solicitud, será efectiva desde el momento que sea emitida por este Sistema, de conformidad a lo estipulado en los artículos  9 y 10 el Decreto con Rango, Valor y Fuerza de Ley Nº 1204, de fecha 10 de febrero de 2001, que regula la materia de Mensaje de Datos y Firmas Electrónicas, publicado en la Gaceta Oficial de la República Bolivariana de Venezuela bajo el número 37.148, de fecha 28 de febrero de 2001.
        </p>
        <div class="row">
          <div class="colo-md-1"></div>
          <div class="col-md-10">

             <input type="checkbox" name="condicion" id="condicion" value="si" class="checkbox-inline"><label>Declaro haber leído y aceptado las condiciones antes expuestas</label>
          </div>
          <div class="colo-md-1"></div>
        </div>
      </div>

      </div>

      <div class="modal-footer">
        <div class="row">
          <div class="col-md-12">
            <div class="col-md-4"></div>
            <div class="col-md-4 text-center">
              <div class="form-group">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <a class="btn btn-primary" style="cursor: pointer;" onclick="enviar()"  id="seguir5" >Guardar</a>
              </div>
            </div>
            <div class="col-md-4"></div>
          </div>
        </div>
      </div>
    </div>

  </div>
</div>
<!-- fin Modal -->


  </form>

</div>



<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>



<script type="text/javascript">

$(document).ready(function (){
var i = 0;
      $('#add').click(function(){
       i++;


    $('#fields tr:last').after('<tr id="row'+i+'" display="block" class="show_div"><td>{!!Form::text('nombre_accionista[]',null,['class'=>'form-control ejem','onkeypress'=>'return soloLetras(event)','maxlength'=>'50'])!!}</td><td>{!!Form::text('apellido_accionista[]',null,['class'=>'form-control','onkeypress'=>'return soloLetras(event)','maxlength'=>'50'])!!}</td><td>{!!Form::text('cedula_accionista[]',null,['class'=>'form-control','onkeypress'=>'return CedulaFormatrep(this,"Cedula de Indentidad Invalida",-1,true,event)','maxlength'=>'11'])!!}<small>Ejemplo:V-00000000</small></td><td>{!!Form::text('cargo_accionista[]',null,['class'=>'form-control','onkeypress'=>'return soloLetras(event)','maxlength'=>'80'])!!}</td><td><button  name="remove" type="button" id="'+i+'" value="" class="btn btn-danger btn-remove">x</button></td></tr>');


       });

  var j = 0;
      $('#add2').click(function(){
       j++;

/*****************************************Campos dinamicos de codigos arancelarios********************************************************************/

    $('#fields2 tr:last').after('<tr id="fila'+j+'" display="block" class="show_div"><td>{!!Form::text('cod_arancel[]',null,['class'=>'form-control ejem','data-toggle'=>"modal",'data-target'=>"#modal",'id'=>"cod_arancel",'readonly'=>'true'])!!}</td><td>{!!Form::text('descrip_arancel[]',null,['class'=>'form-control','onkeypress'=>'return soloLetras(event)'])!!}</td><td>{!!Form::text('descrip_comercial[]',null,['class'=>'form-control','onkeypress'=>'return soloLetras(event)'])!!}</td><td>{!!Form::select('gen_unidad_medida_id[]',$unidad_medida,null,['placeholder'=>'--Seleccione una Unidad de Medida--', 'class'=>'form-control']) !!}</td><td>{!!Form::text('cap_opera_anual[]',null,['class'=>'form-control','onkeypress'=>'return soloNumeros(event)'])!!}</td><td>{!!Form::text('cap_insta_anual[]',null,['class'=>'form-control','onkeypress'=>'return soloNumerosletrasyPorcentaje(event)'])!!}</td><td>{!!Form::text('cap_alamcenamiento[]',null,['class'=>'form-control','onkeypress'=>'return soloNumeros(event)'])!!}</td><td><button  name="remove" type="button" id="'+j+'" value="" class="btn btn-danger btn-remove2">x</button></td></tr>');


       });



     $(document).on('click','.btn-remove',function(){
        var id_boton= $(this).attr("id");
        $("#row"+id_boton+"").remove();

     });
     $(document).on('click','.btn-remove2',function(){
        var id_boton= $(this).attr("id");
        $("#fila"+id_boton+"").remove();

     });

/*function tipoArancel() {

    $('input:radio[name="tipo_arancel"]').change( function(){

      if($(this).is(':checked')) {

        if($(this).val()==1){

           return "nandina";

        }else{

           return "mercosur";
        }

      }

    });

}*/

//////////Accionista pregunta//////////////////////////////////////////////////////////////////////////////////////////////////////
  $('#accion_si').click(function(event) {

    $('#fields').show(1000).animate({width: "show"}, 1000,"linear");

  });

  $('#accion_no').click(function(event) {

    $('#fields').hide(1000).animate({height: "hide"}, 1000,"linear");

  });


//////////////////////////////Parte_1//////////////////////////////////////

      $('#seguir1').click(function(event) {

          $('#datos_persona2').show(1000).animate({width: "show"}, 1000,"linear");
          $('#datos_persona1,#datos_persona3,#datos_persona4').hide(1000).animate({height: "hide"}, 1000,"linear");

      });
//////////////////////////////Parte_2//////////////////////////////////////

      $('#seguir2').click(function(event) {
        $('#datos_persona1,#datos_persona2,#datos_persona4').hide(1000).animate({height: "hide"}, 1000,"linear");
        $('#datos_persona3').show(1000).animate({width: "show"}, 1000,"linear");


      });
      $('#atras1').click(function(event) {
        $('#datos_persona2,#datos_persona3,#datos_persona4').hide(1000).animate({height: "hide"}, 1000,"linear");
        $('#datos_persona1').show(1000).animate({width: "show"}, 1000,"linear");


      });
      $('#paso1').click(function(event) {
        $('#datos_persona2,#datos_persona3,#datos_persona4').hide(1000).animate({height: "hide"}, 1000,"linear");
        $('#datos_persona1').show(1000).animate({width: "show"}, 1000,"linear");


      });

//////////////////////////////Parte_3//////////////////////////////////////

      $('#seguir3').click(function(event) {
        $('#datos_persona3,#datos_persona2,#datos_persona1').hide(1000).animate({height: "hide"}, 1000,"linear");
        $('#datos_persona4').show(1000).animate({width: "show"}, 1000,"linear");


      });
      $('#atras2').click(function(event) {
        $('#datos_persona3,#datos_persona4,#datos_persona1').hide(1000).animate({height: "hide"}, 1000,"linear");
        $('#datos_persona2').show(1000).animate({width: "show"}, 1000,"linear");


      });
      $('#paso22').click(function(event) {
        $('#datos_persona3,#datos_persona4,#datos_persona1').hide(1000).animate({height: "hide"}, 1000,"linear");
        $('#datos_persona2').show(1000).animate({width: "show"}, 1000,"linear");


      });
      $('#paso11').click(function(event) {
        $('#datos_persona2,#datos_persona3,#datos_persona4').hide(1000).animate({height: "hide"}, 1000,"linear");
        $('#datos_persona1').show(1000).animate({width: "show"}, 1000,"linear");


      });
//////////////////////////////Parte_4//////////////////////////////////////
      $('#atras3').click(function(event) {
        $('#datos_persona4,#datos_persona2,#datos_persona1').hide(1000).animate({height: "hide"}, 1000,"linear");
        $('#datos_persona3').show(1000).animate({width: "show"}, 1000,"linear");


      });
      $('#paso333').click(function(event) {
        $('#datos_persona4,#datos_persona2,#datos_persona1').hide(1000).animate({height: "hide"}, 1000,"linear");
        $('#datos_persona3').show(1000).animate({width: "show"}, 1000,"linear");


      });
      $('#paso222').click(function(event) {
        $('#datos_persona3,#datos_persona4,#datos_persona1').hide(1000).animate({height: "hide"}, 1000,"linear");
        $('#datos_persona2').show(1000).animate({width: "show"}, 1000,"linear");


      });
      $('#paso111').click(function(event) {
        $('#datos_persona2,#datos_persona3,#datos_persona4').hide(1000).animate({height: "hide"}, 1000,"linear");
        $('#datos_persona1').show(1000).animate({width: "show"}, 1000,"linear");


      });
      /****************************Validacion de radio tecnologia********************************************/

      $('#prod_tecnologia_si').click(function(event) {
        $('#fields2,#codigo_arancelarios').hide(1000).animate({height: "hide"}, 1000,"linear");
        $('#tecno_especifique').show(1000).animate({width: "show"}, 1000,"linear");

      });

       $('#prod_tecnologia_no').click(function(event) {
        $('#tecno_especifique').hide(1000).animate({height: "hide"}, 1000,"linear");
        $('#fields2,#codigo_arancelarios').show(1000).animate({width: "show"}, 1000,"linear");
      });

});


  $('input:radio[name="tipo_arancel"]').change( function(){




     if($(this).is(':checked')) {

       var parametros = {
               'id': $(this).val()

       };
       var token= $('input[name="_token"]').val();
       console.log(token);
       $('#id_arancel').val(parametros.id);
      $('#cod_arancel').prop('disabled',false);
       $.ajax({
           type: 'GET',
           url: '/Arancel',
           data: {'arancel': $(this).val(),'_token': token},
           success: function(data){

              $table='<table class="table table-bordered table-hover" id="tabla"><tr><th>Código Arancelario</th><th>Descripción</th></tr>';

              $('#arancel').html($table);
              $.each(data,function(index,value){

             $('#tabla tr:last').after('<tr class="data"><td>'+value.codigo+'</td><td>'+value.descripcion+'</td></tr></table>');

              });

           }

       });

    }
});

$('#cod_arancel').show('modal');

/**------------------------------*/
 var global_id=localStorage.setItem('arancel'," ");


 $(document).on('click','#cod_arancel',function(){

   var id=$(this).parents("tr").attr('id');
   localStorage.setItem('arancel',id);

});

 $(document).on('click','tr.data',function(){

  var id=localStorage.getItem('arancel');
  var cod=$(this).find("td").eq(0).text();
  var ara=$(this).find("td").eq(1).text();

  $("tr#"+id).find("td").eq(0).children("input").val(cod);
  $("tr#"+id).find("td").eq(1).children("input").val(ara);

  $('#modal button').click();
  $('#buscador_arancel').val("");



  });

 $(document).on('keyup','#buscador_arancel',function(){

 var tipo=$('input[name="tipo_arancel"]').val();

   $.ajax({

      type:'get',
      url: '/Arancel',
      data: {'valor': $(this).val(), 'tipoAran':tipo},
      success: function(data){

        $table='<table class="table table-bordered table-hover" id="tabla"><tr><th>Código Arancelario</th><th>Descripción</th></tr>';

        $('#arancel').html($table);
        if(data.length==0){

         $('#tabla tr:last').after('<tr><td colspan="2" style="color:red;text-align:center" >Códigos no existen</td></tr>');

       }else{
        $.each(data,function(index,value){

       $('#tabla tr:last').after('<tr class="data"><td>'+value.codigo+'</td><td>'+value.descripcion+'</td><tr></table');

        });

      }
      }

   });

});


</script>
@endsection
