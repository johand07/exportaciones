/*
-- Query: SELECT * FROM exportaciones.cat_peru_vzla
LIMIT 0, 500

-- Date: 2019-08-23 01:53
*/
--
-- Table structure for table `cat_ue_vzla`
--

CREATE TABLE `cat_ue_vzla` (
  `id` int(10) UNSIGNED NOT NULL,
  `codigo` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `descripcion` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bactivo` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cat_ue_vzla`
--
ALTER TABLE `cat_ue_vzla`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cat_ue_vzla`
--
ALTER TABLE `cat_ue_vzla`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

INSERT INTO `cat_ue_vzla` (`id`,`codigo`,`descripcion`,`bactivo`,`created_at`,`updated_at`) VALUES (1,'w','no',1,NULL,NULL);
INSERT INTO `cat_ue_vzla` (`id`,`codigo`,`descripcion`,`bactivo`,`created_at`,`updated_at`) VALUES (2,'p','no',1,NULL,NULL);
INSERT INTO `cat_ue_vzla` (`id`,`codigo`,`descripcion`,`bactivo`,`created_at`,`updated_at`) VALUES (3,'n','no',1,NULL,NULL);
INSERT INTO `cat_ue_vzla` (`id`,`codigo`,`descripcion`,`bactivo`,`created_at`,`updated_at`) VALUES (4,'*','no',1,NULL,NULL);
INSERT INTO `cat_ue_vzla` (`id`,`codigo`,`descripcion`,`bactivo`,`created_at`,`updated_at`) VALUES (5,'(1)','no',1,NULL,NULL);
INSERT INTO `cat_ue_vzla` (`id`,`codigo`,`descripcion`,`bactivo`,`created_at`,`updated_at`) VALUES (6,'(2)','no',1,NULL,NULL);
INSERT INTO `cat_ue_vzla` (`id`,`codigo`,`descripcion`,`bactivo`,`created_at`,`updated_at`) VALUES (7,'(3)','no',1,NULL,NULL);

