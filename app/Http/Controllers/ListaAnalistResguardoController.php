<?php

namespace App\Http\Controllers;

use App\Models\GenSolResguardoAduanero;
use App\Models\BandejaAnalistaResguardo;
use App\Models\CatDocumentos;
use App\Models\GenDocResguardo;
use App\Models\GenStatus;
use App\Models\DetUsuario;
use App\Models\DetUsersResguardo;
use Session;
use Auth;
use Alert;
use Illuminate\Http\Request;

class ListaAnalistResguardoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $detUser= DetUsersResguardo::where('gen_usuario_id',Auth::user()->id )->first();
        //dd($detUser);
        if(!empty($detUser)){
            $sol_resguardo=GenSolResguardoAduanero::where('bactivo',1)->where('gen_aduana_salida_id', $detUser->gen_aduana_salida_id)->with('rAnalistaCna')->get();
        } else {
            Alert::warning('Este usuario no tiene aduana asociada')->persistent("OK");
            
            return redirect()->action('AnalistaResguardoController@index'); 
    
        }
        return view('ListaAnalistResguardo.index',compact('sol_resguardo'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\GenSolResguardoAduanero  $genSolResguardoAduanero
     * @return \Illuminate\Http\Response
     */
    public function show(GenSolResguardoAduanero $genSolResguardoAduanero)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\GenSolResguardoAduanero  $genSolResguardoAduanero
     * @return \Illuminate\Http\Response
     */
    public function edit(GenSolResguardoAduanero $genSolResguardoAduanero, $id)
    {
//dd($id);

        $titulo= 'Verificación de Planilla Resguardo';
        $descripcion= 'Perfil del Analista Resguardo';


     $solResguardos=GenSolResguardoAduanero::find($id);

    // dd($solResguardos);

    /************ Valido si la solicitud existe, Si existe no hago nada en caso contrario inserto la solicitud******************/

       $validaratencion=BandejaAnalistaResguardo::where('gen_sol_resguardo_aduanero_id',$solResguardos->id)->where('gen_usuario_id',Auth::user()->id)->first();
         //dd($validaratencion);


          if(empty($validaratencion)){
            /*Consulta las solicitudes mis solicitudes con el status id 10 atendido*/
            $solResguardos=GenSolResguardoAduanero::find($id);
            $solResguardos->status_resguardo=20;
            $solResguardos->fstatus_resguardo=date('Y-m-d');
            $solResguardos->save();
             /***Inserto en la bandeja solinversionista me traigo el id de esa solicitud con el usuario   analista le actualizo el status a 10 que es en Observacion la actualizo a la fecha de hoy****/
            $resguardoanalista= new BandejaAnalistaResguardo;
            $resguardoanalista->gen_sol_resguardo_aduanero_id=$solResguardos->id;
            $resguardoanalista->gen_usuario_id=Auth::user()->id;
            $resguardoanalista->gen_status_id=20;
            $resguardoanalista->fstatus=date('Y-m-d');
            $resguardoanalista->save();

          }
     

    /*************Consulta para traerme el estado de observacion de las solicitudes**************/
        //$estado_observacion=GenSolInversionista::all()->pluck('estado_observacion','id');
         //dd($estado_observacion);

        /*****Consulta para obtener el estado de la observacion de la solicitud 11 observacion, 12 Aprobada y 13 Anulada****/

        $documentos=CatDocumentos::where('gen_ente_id',3)->where('bactivo',1)->get();
        //dd($documentos);


        $estado_observacion=GenStatus::whereIn('id', [22,24,15,11])->pluck('nombre_status','id');

       // dd($estado_observacion);
 
        /**/
        $det_usuario=DetUsuario::where('gen_usuario_id',$solResguardos->gen_usuario_id)->first();
        
//  dd($det_usuario);
         $docCertificados=GenDocResguardo::where('gen_sol_resguardo_aduanero_id',$solResguardos->id)->first();

         if(!empty($docCertificados['file_1'])){
            $exten_file_1=explode(".", $docCertificados['file_1']);
            $extencion_file_1 = $exten_file_1[1];
//dd($extencion_file_1);
        } else {
            $extencion_file_1 = '';
        }

        if (!empty($docCertificados['file_2'])) {
            $exten_file_2=explode(".", $docCertificados['file_2']);
            $extencion_file_2 = $exten_file_2[1];
        }else {
            $extencion_file_2 = '';
        }
        if (!empty($docCertificados['file_3'])) {
            $exten_file_3=explode(".", $docCertificados['file_3']);
            $extencion_file_3 = $exten_file_3[1];
        }else {
            $extencion_file_3 = '';
        }
        if (!empty($docCertificados['file_4'])) {
            $exten_file_4=explode(".", $docCertificados['file_4']);
            $extencion_file_4 = $exten_file_4[1];
        }else {
            $extencion_file_4 = '';
        }


        

        if (!empty($docCertificados['file_5'])) {
            $exten_file_5=explode(".", $docCertificados['file_5']);
            $extencion_file_5 = $exten_file_5[1];
        }else {
            $extencion_file_5 = '';
        }


        if (!empty($docCertificados['file_6'])) {
            $exten_file_6=explode(".", $docCertificados['file_6']);
            $extencion_file_6 = $exten_file_6[1];
        }else {
            $extencion_file_6 = '';
        }
        


        if (!empty($docCertificados['file_7'])) {
            $exten_file_7=explode(".", $docCertificados['file_7']);
            $extencion_file_7 = $exten_file_7[1];
        }else {
            $extencion_file_7 = '';
        }


         if (!empty($docCertificados['file_8'])) {
            $exten_file_8=explode(".", $docCertificados['file_8']);
            $extencion_file_8 = $exten_file_8[1];
        }else {
            $extencion_file_8 = '';
        }

        if (!empty($docCertificados['file_9'])) {
            $exten_file_9=explode(".", $docCertificados['file_9']);
            $extencion_file_9 = $exten_file_9[1];
        }else {
            $extencion_file_9 = '';
        }

        if (!empty($docCertificados['file_10'])) {
            $exten_file_10=explode(".", $docCertificados['file_10']);
            $extencion_file_10 = $exten_file_10[1];
        }else {
            $extencion_file_10 = '';
        }


        if (!empty($docCertificados['file_11'])) {
            $exten_file_11=explode(".", $docCertificados['file_11']);
            $extencion_file_11 = $exten_file_11[1];
        }else {
            $extencion_file_11 = '';
        }


        if (!empty($docCertificados['file_12'])) {
            $exten_file_12=explode(".", $docCertificados['file_12']);
            $extencion_file_12 = $exten_file_12[1];
        }else {
            $extencion_file_12 = '';
        }


        if (!empty($docCertificados['file_13'])) {
            $exten_file_13=explode(".", $docCertificados['file_13']);
            $extencion_file_13 = $exten_file_13[1];
        }else {
            $extencion_file_13 = '';
        }


        if (!empty($docCertificados['file_14'])) {
            $exten_file_14=explode(".", $docCertificados['file_14']);
             $extencion_file_14 = $exten_file_14[1];
        }else {
            $extencion_file_14 = '';
        }


        if (!empty($docCertificados['file_15'])) {
            $exten_file_15=explode(".", $docCertificados['file_15']);
            $extencion_file_15 = $exten_file_15[1];
        }else {
            $extencion_file_15 = '';
        }

        if (!empty($docCertificados['file_16'])) {
            $exten_file_16=explode(".", $docCertificados['file_16']);
            $extencion_file_16 = $exten_file_16[1];
        }else {
            $extencion_file_16 = '';
        }



        if (!empty($docCertificados['file_17'])) {
            $exten_file_17=explode(".", $docCertificados['file_17']);
            $extencion_file_17 = $exten_file_17[1];
        }else {
            $extencion_file_17 = '';
        }


        if (!empty($docCertificados['file_18'])) {
            $exten_file_18=explode(".", $docCertificados['file_18']);
            $extencion_file_18 = $exten_file_18[1];
        }else {
            $extencion_file_18 = '';
        }


        if (!empty($docCertificados['file_19'])) {
            $exten_file_19=explode(".", $docCertificados['file_19']);
            $extencion_file_19 = $exten_file_19[1];
        }else {
            $extencion_file_19 = '';
        }


        if (!empty($docCertificados['file_20'])) {
            $exten_file_20=explode(".", $docCertificados['file_20']);
            $extencion_file_20 = $exten_file_20[1];
        }else {
            $extencion_file_20 = '';
        }

          if (!empty($docCertificados['file_21'])) {
            $exten_file_21=explode(".", $docCertificados['file_21']);
            $extencion_file_21 = $exten_file_21[1];
        }else {
            $extencion_file_21 = '';
        }

          if (!empty($docCertificados['file_22'])) {
            $exten_file_22=explode(".", $docCertificados['file_22']);
            $extencion_file_22 = $exten_file_22[1];
        }else {
            $extencion_file_22 = '';
        }


          if (!empty($docCertificados['file_23'])) {
            $exten_file_23=explode(".", $docCertificados['file_23']);
            $extencion_file_23 = $exten_file_23[1];
        }else {
            $extencion_file_23 = '';
        }


          if (!empty($docCertificados['file_24'])) {
            $exten_file_24=explode(".", $docCertificados['file_24']);
            $extencion_file_24 = $exten_file_24[1];
        }else {
            $extencion_file_24 = '';
        }



       


        return view('ListaAnalistResguardo.edit',compact('solResguardos','det_usuario','estado_observacion','estado_observacion','documentos','titulo','descripcion','extencion_file_1','extencion_file_2','extencion_file_3','extencion_file_4','extencion_file_5','extencion_file_6','extencion_file_7','extencion_file_8','extencion_file_9','extencion_file_10','docCertificados','extencion_file_11','extencion_file_12','extencion_file_13','extencion_file_14','extencion_file_15','extencion_file_16','extencion_file_17','extencion_file_18','extencion_file_19','extencion_file_20','extencion_file_21','extencion_file_22','extencion_file_23','extencion_file_24'));
           
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\GenSolResguardoAduanero  $genSolResguardoAduanero
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, GenSolResguardoAduanero $genSolResguardoAduanero, $id)
    {
        //dd($request->gen_status_id);

        /* Validamos si viene lleno la observacion y hago esta cosa que es actualizar el status de la solicitud si esta en Observacion.    */
        //dd($request->observacion_inversionista);
            if(!empty($request->observacion_resguardo ) && $request->gen_status_id==11){
         
                 $resguardo=GenSolResguardoAduanero::find($id)->update(['status_resguardo'=>11,'edo_observacion_resguardo'=>1,'observacion_resguardo'=>$request->observacion_resguardo,'updated_at' => date("Y-m-d H:i:s") ]);

                 $bandejaresguardo=BandejaAnalistaResguardo::where('gen_sol_resguardo_aduanero_id',$id)->update(['gen_status_id'=>11,'updated_at' => date("Y-m-d H:i:s") ]);

               
            }elseif(!empty($request->observacion_resguardo ) && $request->gen_status_id==15){
              
              $resguardo=GenSolResguardoAduanero::find($id)->update(['status_resguardo'=>15,'edo_observacion_resguardo'=>1,'observacion_resguardo'=>$request->observacion_resguardo,'updated_at' => date("Y-m-d H:i:s") ]);

               $bandejaresguardo=BandejaAnalistaResguardo::where('gen_sol_resguardo_aduanero_id',$id)->update(['gen_status_id'=>15,'updated_at' => date("Y-m-d H:i:s") ]);


            }elseif(empty($request->observacion_resguardo ) && $request->gen_status_id == 20){
              
              $resguardo=GenSolResguardoAduanero::find($id)->update(['status_resguardo'=>20,'edo_observacion_resguardo'=>0,'updated_at' => date("Y-m-d H:i:s") ]);

               $bandejaresguardo=BandejaAnalistaResguardo::where('gen_sol_resguardo_aduanero_id',$id)->update(['gen_status_id'=>20,'updated_at' => date("Y-m-d H:i:s") ]);

            }elseif(empty($request->observacion_resguardo ) && $request->gen_status_id == 22){
              
              $resguardo=GenSolResguardoAduanero::find($id)->update(['status_resguardo'=>22,'edo_observacion_resguardo'=>0,'aprobada_resguardo'=>1,'updated_at' => date("Y-m-d H:i:s") ]);

              $bandejaresguardo=BandejaAnalistaResguardo::where('gen_sol_resguardo_aduanero_id',$id)->update(['gen_status_id'=>12,'updated_at' => date("Y-m-d H:i:s") ]);

            }elseif(!empty($request->observacion_resguardo ) && $request->gen_status_id == 24){
              
              $resguardo=GenSolResguardoAduanero::find($id)->update(['status_resguardo'=>24,'edo_observacion_resguardo'=>1,'observacion_resguardo'=>$request->observacion_resguardo,'updated_at' => date("Y-m-d H:i:s") ]);

              $bandejaresguardo=BandejaAnalistaResguardo::where('gen_sol_resguardo_aduanero_id',$id)->update(['gen_status_id'=>24,'updated_at' => date("Y-m-d H:i:s") ]);

          }


            Alert::warning('Su Solicitud ha sido Atendida ')->persistent("OK");
            return redirect()->action('ListaAnalistResguardoController@index'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\GenSolResguardoAduanero  $genSolResguardoAduanero
     * @return \Illuminate\Http\Response
     */
    public function destroy(GenSolResguardoAduanero $genSolResguardoAduanero)
    {
        //
    }
}
