<?php

use Illuminate\Database\Seeder;
use App\Models\CatCargados;
use App\Models\CatTipoCafe;
use App\Models\CatMetodoElab;

class CatCargadosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data1 = [
            [
                'id'         => 1,
                'nombre_cargados'       => 'En sacos',
                'created_at' => '2022-03-07 13:59:08',
                'updated_at' => '2022-03-07 13:59:08',
            ],

            [
                'id'         => 2,
                'nombre_cargados'       => 'A granel',
                'created_at' => '2022-03-07 13:59:08',
                'updated_at' => '2022-03-07 13:59:08',
            ],

            [
                'id'         => 3,
                'nombre_cargados'       => 'En contenedores',
                'created_at' => '2022-03-07 13:59:08',
                'updated_at' => '2022-03-07 13:59:08',
            ]
        ];

        $data2 = [
            [
                'id'         => 1,
                'nombre_cafe'       => 'Arábica Verde',
                'created_at' => '2022-03-07 13:59:08',
                'updated_at' => '2022-03-07 13:59:08',
            ],

            [
                'id'         => 2,
                'nombre_cafe'       => 'Robusta Verde',
                'created_at' => '2022-03-07 13:59:08',
                'updated_at' => '2022-03-07 13:59:08',
            ],

            [
                'id'         => 3,
                'nombre_cafe'       => 'Tostado',
                'created_at' => '2022-03-07 13:59:08',
                'updated_at' => '2022-03-07 13:59:08',
            ],

            [
                'id'         => 4,
                'nombre_cafe'       => 'Soluble',
                'created_at' => '2022-03-07 13:59:08',
                'updated_at' => '2022-03-07 13:59:08',
            ],

            [
                'id'         => 5,
                'nombre_cafe'       => 'Líquido',
                'created_at' => '2022-03-07 13:59:08',
                'updated_at' => '2022-03-07 13:59:08',
            ],

            [
                'id'         => 6,
                'nombre_cafe'       => 'Otros',
                'created_at' => '2022-03-07 13:59:08',
                'updated_at' => '2022-03-07 13:59:08',
            ]
        ];

        $data3 = [
            [
                'id'         => 1,
                'nombre_metodo'       => 'Descafeinado',
                'created_at' => '2022-03-07 13:59:08',
                'updated_at' => '2022-03-07 13:59:08',
            ],

            [
                'id'         => 2,
                'nombre_metodo'       => 'Café Verde',
                'created_at' => '2022-03-07 13:59:08',
                'updated_at' => '2022-03-07 13:59:08',
            ],

            [
                'id'         => 3,
                'nombre_metodo'       => 'Via Seca',
                'created_at' => '2022-03-07 13:59:08',
                'updated_at' => '2022-03-07 13:59:08',
            ],

            [
                'id'         => 4,
                'nombre_metodo'       => 'Orgánico',
                'created_at' => '2022-03-07 13:59:08',
                'updated_at' => '2022-03-07 13:59:08',
            ],

            [
                'id'         => 5,
                'nombre_metodo'       => 'Vía Húmeda',
                'created_at' => '2022-03-07 13:59:08',
                'updated_at' => '2022-03-07 13:59:08',
            ],

            [
                'id'         => 6,
                'nombre_metodo'       => 'Café Soluble',
                'created_at' => '2022-03-07 13:59:08',
                'updated_at' => '2022-03-07 13:59:08',
            ],

            [
                'id'         => 7,
                'nombre_metodo'       => 'Certificado',
                'created_at' => '2022-03-07 13:59:08',
                'updated_at' => '2022-03-07 13:59:08',
            ],

            [
                'id'         => 8,
                'nombre_metodo'       => 'Centrifugado',
                'created_at' => '2022-03-07 13:59:08',
                'updated_at' => '2022-03-07 13:59:08',
            ],

            [
                'id'         => 9,
                'nombre_metodo'       => 'N° Certificado',
                'created_at' => '2022-03-07 13:59:08',
                'updated_at' => '2022-03-07 13:59:08',
            ],

            [
                'id'         => 10,
                'nombre_metodo'       => 'Liofilizado',
                'created_at' => '2022-03-07 13:59:08',
                'updated_at' => '2022-03-07 13:59:08',
            ]
        ];

        CatCargados::insert($data1);
        CatTipoCafe::insert($data2);
        CatMetodoElab::insert($data3);
    }
}
