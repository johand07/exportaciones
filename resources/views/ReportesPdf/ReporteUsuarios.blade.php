<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Lista de Usuarios</title>
    <style>
        body{
            text-align:center;
           font-family: Arial;
                 color: #58585A; 
        }
        th{
            font-size:16px;
            background-color:#818286;
            color: #fff;
        }
        td{
            font-size:14px;
            border: 1px solid #818286;
        }
        table{
            border: 1px solid #818286;
        }
    </style>
</head>
    <body>
        <img class="img-responsive" style="width: 565px;" src='img/cintillo.png'>
        <div class="pagina alineacion">
         
            <h2>Reporte de Usuarios</h2>

            
                <table id="listaUsuarios" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                        <tr>
                            <th>Tipo de Usuario</th>
                            <th>Nombre o Razon Social</th>
                            <th>Rif</th>
                            <th>Estatus</th>
                            <th>Fecha Modificación</th>
                            <th>Email</th>
                            <th>Nombre Representate Legal</th>
                            <th>Cédula Representate Legal</th>
                            
                        </tr>
                    </thead>
                    <tbody>
                      @foreach($users as $user)
                        <tr>           
                            <td>{{$user->usuario->tipoUsuario->nombre_tipo_usu}}</td>
                            <td>{{$user->razon_social}}</td> 
                            <td>{{$user->rif}}</td> 
                            <td>{{$user->usuario->estatus->nombre_status}}</td> 
                            <td>{{$user->usuario->updated_at}}</td> 
                            <td>{{$user->usuario->email}}</td>
                            <td>{{$user->nombre_repre}}</td>
                            <td class="col-md-2">{{$user->ci_repre}}</td>
                            

                            </tr>
                      @endforeach

                    </tbody>
                </table>
            
        </div>
    </body>
</html>