<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GenDivisa extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('gen_divisa', function (Blueprint $table) {
          $table->engine = 'InnoDB';
          $table->increments('id');
          $table->string('cdivisa', 60)->nullable();
          $table->string('ddivisa_abr', 60)->nullable();
          $table->string('ddivisa', 60)->nullable();
          $table->string('mfactor_conv', 60)->nullable();
          $table->string('dsimbolo', 60)->nullable();
          $table->string('mfactor_conv2', 60)->nullable();
          $table->boolean('bactivo')->default(1);
          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::dropIfExists('gen_divisa');
    }
}
