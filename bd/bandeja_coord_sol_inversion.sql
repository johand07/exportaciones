-- phpMyAdmin SQL Dump
-- version 4.2.12deb2+deb8u2
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 12-02-2019 a las 15:36:25
-- Versión del servidor: 10.0.30-MariaDB-0+deb8u2
-- Versión de PHP: 7.1.23-2+0~20181017082658.9+jessie~1.gbpab65a0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `exportaciones`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bandeja_coord_sol_inversion`
--

CREATE TABLE IF NOT EXISTS `bandeja_coord_sol_inversion` (
`id` int(10) unsigned NOT NULL,
  `bandeja_sol_inversionista_id` int(10) unsigned DEFAULT '1',
  `gen_usuario_id` int(10) unsigned DEFAULT '1',
  `gen_status_id` int(10) unsigned DEFAULT NULL,
  `fstatus` date DEFAULT NULL,
  `bactivo` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `bandeja_coord_sol_inversion`
--
ALTER TABLE `bandeja_coord_sol_inversion`
 ADD PRIMARY KEY (`id`), ADD KEY `bandeja_coord_sol_inversion_bandeja_sol_inversionista_id_index` (`bandeja_sol_inversionista_id`), ADD KEY `bandeja_coord_sol_inversion_gen_usuario_id_index` (`gen_usuario_id`), ADD KEY `bandeja_coord_sol_inversion_gen_status_id_index` (`gen_status_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `bandeja_coord_sol_inversion`
--
ALTER TABLE `bandeja_coord_sol_inversion`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=58;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `bandeja_coord_sol_inversion`
--
ALTER TABLE `bandeja_coord_sol_inversion`
ADD CONSTRAINT `bandeja_coord_sol_inversion_bandeja_sol_inversionista_id_foreign` FOREIGN KEY (`bandeja_sol_inversionista_id`) REFERENCES `bandeja_sol_inversionista` (`id`),
ADD CONSTRAINT `bandeja_coord_sol_inversion_gen_status_id_foreign` FOREIGN KEY (`gen_status_id`) REFERENCES `gen_status` (`id`),
ADD CONSTRAINT `bandeja_coord_sol_inversion_gen_usuario_id_foreign` FOREIGN KEY (`gen_usuario_id`) REFERENCES `gen_usuario` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
