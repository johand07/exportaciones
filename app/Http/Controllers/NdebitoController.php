<?php

namespace App\Http\Controllers;

use App\Models\NotaDebito;
use App\Models\GenFactura;
use App\Models\GenConsignatario;
use App\Models\GenFacturaND;
use App\Http\Requests\NotaDebitoRequest;
use Session;
use Auth;
use Alert;
use Laracasts\Flash\Flash;
use Illuminate\Http\Request;

class NdebitoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $titulo="Nota de Débito";
        $descripcion="Nota de Débito Registradas";
        $NDebito=NotaDebito::where('gen_usuario_id',Auth::user()->id)->where('bactivo',1)->get();
        return view('NDebito.index', compact('NDebito','titulo','descripcion'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $titulo="Nota de Débito";
        $descripcion="Registro de Nota de Débito";
        $factura=GenFactura::where('gen_usuario_id',Auth::user()->id)->pluck('numero_factura','id');
        $consignatario=GenConsignatario::where('gen_usuario_id',Auth::user()->id)->where('bactivo',1)->pluck('nombre_consignatario','id');
        return view('NDebito.create', compact('factura','titulo','descripcion','consignatario'));
    }


    public function facturaND(Request $request){
        //dd($request->gen_factura_id);

        $facturand = GenFacturaND::where('gen_factura_id',$request->gen_factura_id)->first();

        if (!empty($facturand)) {
          return 1;
        } else {
          return 2;
        }
        

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(NotaDebitoRequest $request)
    {
       $fecha_actual = date("d-m-Y");
        if ((strtotime($request->fecha_emision)) > (strtotime($fecha_actual))) {
           Alert::warning('No puede ser superior a la Fecha Actual', 'Error en la Fecha de Emisión !')->persistent("OK");
        return redirect()->action('NdebitoController@create');
        } else {
       
           //NotaDebito::create($request->all());
           //dd($request->all());
           $debito = new NotaDebito;
           $debito->gen_usuario_id=Auth::user()->id;
           $debito->gen_consignatario_id=$request->gen_consignatario_id;
           $debito->num_nota_debito=$request->num_nota_debito;
           $debito->gen_factura_id=$request->gen_factura_id;
           $debito->fecha_emision=$request->fecha_emision;
           $debito->concepto_nota_debito=$request->concepto_nota_debito;
           $debito->monto_nota_debito=$request->monto_nota_debito;
           $debito->justificacion=$request->justificacion;
           $debito->save();


           $ult_notadebito=NotaDebito::where('gen_usuario_id',Auth::user()->id)->orderby('created_at','DESC')->take(1)->first();

           $insertfacnd = new GenFacturaND;
           $insertfacnd->gen_factura_id=$request->gen_factura_id;
           $insertfacnd->nota_debito_id=$ult_notadebito->id;
           $insertfacnd->save();

           
           Alert::success('REGISTRO EXITOSO','Nota de Débito agregada')->persistent("Ok");
           return redirect()->action('NdebitoController@index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\models\NotaDebito  $notaDebito
     * @return \Illuminate\Http\Response
     */
    public function show(NotaDebito $notaDebito)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\models\NotaDebito  $notaDebito
     * @return \Illuminate\Http\Response
     */
    public function edit(NotaDebito $notaDebito, $id)
    {
        $titulo="Editar Nota de Débito";
        $descripcion="Editar Datos";
        $NDebito=NotaDebito::find($id);
        $factura=GenFactura::where('gen_usuario_id',Auth::user()->id)->pluck('numero_factura','id');
        $consignatario=GenConsignatario::where('gen_usuario_id',Auth::user()->id)->where('bactivo',1)->pluck('nombre_consignatario','id');
        return view('NDebito.edit', compact('titulo','descripcion','NDebito','factura','consignatario'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\models\NotaDebito  $notaDebito
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, NotaDebito $notaDebito, $id)
    {
        $fecha_actual = date("d-m-Y");
         if ((strtotime($request->fecha_emision)) > (strtotime($fecha_actual))) {
           Alert::warning('No puede ser superior a la Fecha Actual', 'Error en la Fecha de Emisión !')->persistent("OK");
        return redirect('exportador/Ndedito/'.$id.'/edit');
        } else {
           NotaDebito::find($id)->update($request->all());
           //dd($request->all());
           Alert::success('Actualización Exitosa!', 'Actulización Exitosa.')->persistent("OK");
           return redirect()->action('NdebitoController@index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\models\NotaDebito  $notaDebito
     * @return \Illuminate\Http\Response
     */
    public function destroy(NotaDebito $notaDebito,$id)
    {
        $NotaDebito=NotaDebito::find($id)->update(['bactivo' => 0, 'updated_at' => date("Y-m-d H:i:s") ]);
            if ($NotaDebito) {
               return 1;
            }else{
                return 2;
            }

    }
    
}
