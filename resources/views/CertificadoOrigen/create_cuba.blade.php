@extends('templates/layoutlte')
@section('content')

<!--Aqui Form::open-->
{!! Form::open(['route' => 'CertificadoOrigen.store' ,'method'=>'POST', 'id'=>'FormProd', 'enctype' => 'multipart/form-data']) !!}

<!-------------------Item carga de documentos------------------- -->
<input type="hidden" name="gen_tipo_certificado_id" id="gen_tipo_certificado_id" value="{{$gen_tipo_certificado_id ? $gen_tipo_certificado_id : 7}}" />
<div class="content">
	<div class="panel-group">

		<div class="panel-primary" style="background-color: #fff">
			<div class="panel-heading"><h3>Carga de Documentos </h3></div>
			<div class="panel-body">
				<h5 style="color: red">Los campos marcados con (*) son Obligatorios.  (Formatos permitidos .doc, .docx, .ppt, .pptx, .pdf)</h5>
				<div class="row"><br>
					<div class="col-md-12">
						<div class="col-md-6">
							<tr>
								<td class="text-center" valign="top" > 
									<span><b><p style="font-size:14px;">1.) Cargar el formato de Solicitud de Certificado de Origen</p></b></span>
									<strong style="color: red"> *</strong>
									<span class="btn btn-default btn-file" style="margin: 3px;">
										Cargar Archivo <span class="glyphicon glyphicon-file"></span>
										{!! Form::file('file_1', array('class' => 'file-input','style'=>'margin: 10px;','id'=>'file_1','title'=>'formatos permitidos .doc, .docx, .ppt, .pptx, .pdf')) !!}
									</span>
								</td>
								<td class="text-center" height="40px"><img id="" src="" alt="" width="300px" style="margin: 3px;"><img id="imgdocumento_file_1" src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px; display: none;">
									<div id="nombre_file_1"></div>
								</td>
							</tr>
						</div>
						<div class="col-md-6">
							<tr>
								<td class="text-center" valign="top" > 
									<span><b><p style="font-size:14px;">4.) Pago por concepto de solicitud de certificado de origen (0,25 petros)</p></b></span>
									<strong style="color: red"> *</strong>
									<span class="btn btn-default btn-file" style="margin: 3px;">
										Cargar Archivo <span class="glyphicon glyphicon-file"></span>
										{!! Form::file('file_4', array('class' => 'file-input','style'=>'margin: 10px;','id'=>'file_4','title'=>'formatos permitidos .doc, .docx, .ppt, .pptx, .pdf')) !!}
									</span>
								</td>
								<td class="text-center" height="40px"><img id="" src="" alt="" width="300px" style="margin: 3px;"><img id="imgdocumento_file_4" src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px; display: none;"><div id="nombre_file_4"></div><i></i>

								</td>
							</tr>
						</div>
					</div>
					

					<div class="col-md-12"><br><br><br>
						<div class="col-md-6">
							<tr>
								<td class="text-center" valign="top" > 
									<span><b><p style="font-size:14px;">2.) Declaración jurada de Origen</p></b></span>
									<strong style="color: red"> *</strong>
									<span class="btn btn-default btn-file" style="margin: 3px;">
										Cargar Archivo <span class="glyphicon glyphicon-file"></span>
										{!! Form::file('file_2', array('class' => 'file-input','style'=>'margin: 10px;','id'=>'file_2','title'=>'formatos permitidos .doc, .docx, .ppt, .pptx, .pdf')) !!}
									</span>
								</td>
								<td class="text-center" height="40px"><img id="" src="" alt="" width="300px" style="margin: 3px;"><img id="imgdocumento_file_2" src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px; display: none;"><div id="nombre_file_2"></div>
								</td>
							</tr>
						</div>
						<div class="col-md-6">
							<tr>
								<td class="text-center" valign="top" > 
									<span><b><p style="font-size:14px;">5.) Pago de 0,02 U.T Gobierno del Distrito Capital</p></b></span>
									<strong style="color: red"> *</strong>
									<span class="btn btn-default btn-file" style="margin: 3px;">
										Cargar Archivo <span class="glyphicon glyphicon-file"></span>
										{!! Form::file('file_5', array('class' => 'file-input','style'=>'margin: 10px;','id'=>'file_5','title'=>'formatos permitidos .doc, .docx, .ppt, .pptx, .pdf')) !!}
									</span>
								</td>
								<td class="text-center" height="40px"><img id="" src="" alt="" width="300px" style="margin: 3px;"><img id="imgdocumento_file_5" src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px; display: none;"><div id="nombre_file_5"></div>
								</td>
							</tr>
						</div>
					</div>
					<div class="col-md-12"><br><br><br>
						<div class="col-md-6">
							<tr>
								<td class="text-center" valign="top" > 
									<span><b><p>3.) Factura comercial de exportador</p></b></span>
									<strong style="color: red"> *</strong>
									<span class="btn btn-default btn-file" style="margin: 3px;">
										Cargar Archivo <span class="glyphicon glyphicon-file"></span>
										{!! Form::file('file_3', array('class' => 'file-input','style'=>'margin: 10px;','id'=>'file_3','title'=>'formatos permitidos .doc, .docx, .ppt, .pptx, .pdf')) !!}
									</span>
								</td>
								<td class="text-center" height="40px"><img id="" src="" alt="" width="300px" style="margin: 3px;"><img id="imgdocumento_file_3" src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px; display: none;"><div id="nombre_file_3"></div>
								</td>
							</tr>
						</div>
						<div class="col-md-6">
							<tr>
								<td class="text-center" valign="top" > 
									<span><b><p style="font-size:14px;">6.) Factura de productor (Si es <b>Comercializadora</b>)</p></b></span>
									<!--strong style="color: red"> *</strong-->
									<span class="btn btn-default btn-file" style="margin: 3px;">
										Cargar Archivo <span class="glyphicon glyphicon-file"></span>
										{!! Form::file('file_6', array('class' => 'file-input','style'=>'margin: 10px;','id'=>'file_6','title'=>'formatos permitidos .doc, .docx, .ppt, .pptx, .pdf', "noreq"=>"noreq")) !!}
									</span>
								</td>
								<td class="text-center" height="40px"><img id="" src="" alt="" width="300px" style="margin: 3px;"><img id="imgdocumento_file_6" src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px; display: none;">	<div id="nombre_file_6"></div>
								</td>
							</tr>
						</div>
					</div>

				</div>
			</div>
		<br><br>
		<!-----------------Item Certificado de Origen Colombia------------ -->
		<div class="panel-primary" style="background-color: #fff">
			<div class="panel-heading">
			<h3>Certificado de origen acuerdo de complementación económica Cuba - Venezuela</h3>
			</div>
			<div class="panel-body">
				<div class="row"><br>
					<div class="col-md-6">
						<div class="form-group">
							{!! Form::label('','País Exportador') !!}
							{!! Form::text('pais_exportador',null,['class'=>'form-control','id'=>'pais_exportador']) !!}
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							{!! Form::label('','País Importador') !!}
							{!! Form::text('pais_importador',null,['class'=>'form-control','id'=>'pais_importador']) !!}
						</div>
					</div>
				</div>

				<!--Primera tabla para prodcutos completos en el caso Colombia que tiene un campo demas Cantidadsegún facturapara cuposCapítulo 72-->
				
				<div class="row"><br><!--Agregar productos-->
					
					<table class="table table-bordered text-left" id="productos">
						<tr> <td><button  name="add" type="button" id="add-productos" value="add more" class="btn btn-success" >Agregar</button></td></tr>

						<tr>
							<th>Nº. De Orden </th>
							
							<th>Código arancelario</th>

							<th>Descripción de las
							mercancías
							</th>

							<th>Unidad Física según factura
							</th>

							<th>Valor de cada<br>
								mercancía según<br>
								factura en US$
							</th>
				
						
						</tr>
						<tr id="row1"><!--row1 es para la lista dinamica de agregar productos nsantos-->
								<!--Lista dinamica Prodcutos-->
						
							<td>{!!Form::text('num_orden[]',null,['class'=>'form-control',''=>"modal",'data-target'=>"",'id'=>'num_orden',''=>'true', "noreq"=>"noreq"])!!}
							</td>

							<td>{!!Form::text('codigo_arancel[]',null,['class'=>'form-control','id'=>'codigo_arancel', "noreq"=>"noreq"])!!}
							</td>

							<td>{!!Form::text('denominacion_mercancia[]',null,['class'=>'form-control','id'=>'denominacion_mercancia', "noreq"=>"noreq"])!!}
							</td>

							<td>{!!Form::text('unidad_fisica[]',null,['class'=>'form-control','id'=>'unidad_fisica', "noreq"=>"noreq"])!!}
							</td>

							<td>{!!Form::text('valor_fob[]',null,['class'=>'form-control','id'=>'valor_fob', "noreq"=>"noreq"])!!}
							</td>
						
							
						</tr>
					</table>
				</div>
			
				<div class="row">
					<center><h3>Declaración de origen</h3></center>
					<div class="col-md-6">
						<div class="form-group">
							{!! Form::label('','Factura Comercial No.') !!}
							{!! Form::text('numero_factura',null,['class'=>'form-control','id'=>'numero_factura', "noreq"=>"noreq"]) !!}
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							{!! Form::label('','Fecha') !!}
							<div class="input-group date">
								{!! Form::text('f_fac_certificado',null,['class'=>'form-control','readonly'=>'readonly','id'=>'f_fac_certificado', "noreq"=>"noreq"]) !!}
								<span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
							</div>
						</div>
					</div>
				</div><br><br>
			
				<div class="row"><br>
					<table class="table table-bordered text-left" id="criterios">
						<tr> <td><button  name="add" type="button" id="add-criterios" value="add more" class="btn btn-success" >Agregar</button></td></tr>

						<tr>
							<th>Nº. De Orden </th>
						
							<th>Criterio para la Calificación de Origen
							</th>
						</tr>
						<tr id="row2">

							<td>{!!Form::text('num_orden_declaracion[]',null,['class'=>'form-control','id'=>'num_orden_declaracion', "noreq"=>"noreq"])!!}
							</td>

							<td>{!!Form::text('normas_criterio[]',null,['class'=>'form-control','id'=>'normas_criterio', "noreq"=>"noreq"])!!}
							</td>
						</tr>

					</table>
				</div>
			
				<div class="row"><br>
					<div class="col-md-6">
						<h3>Productor o exportador</h3>
						<div class="form-group">					
							{!! Form::label('','Nombre o Razón Social') !!}
							{!! Form::text('razon_social',$det_usuario->razon_social,['class'=>'form-control','id'=>'razon_social','readonly'=>'readonly']) !!}
						</div>
						<div class="form-group">
							{!! Form::label('','Número de identificacion Tributario/Fiscal') !!}
							{!! Form::text('rif',$det_usuario->rif,['class'=>'form-control','id'=>'rif','readonly'=>'readonly']) !!}
						</div>
						
						<div class="form-group">
							{!! Form::label('','Dirección o Domicilio') !!}
							{!! Form::text('direccion',$det_usuario->direccion,['class'=>'form-control','id'=>'direccion','readonly'=>'readonly']) !!}
						</div>
						<div class="form-group">
							{!! Form::label('','E-mail') !!}
							{!! Form::text('correo',$det_usuario->correo,['class'=>'form-control','id'=>'correo','readonly'=>'readonly']) !!}
						</div>
						<div class="form-group">
							{!! Form::label('','Teléfono') !!}
							{!! Form::text('telefono_movil',$det_usuario->telefono_movil,['class'=>'form-control','id'=>'telefono_movil','readonly'=>'readonly']) !!}
						</div>
						
						<div class="form-group">
							{!! Form::label('','Fecha de Emision') !!}
							<div class="input-group date">		
								{!! Form::text('fecha_emision_exportador',null,['class'=>'form-control','readonly'=>'readonly','id'=>'fecha_emision_exportador'])!!}<span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
							</div>
						</div>
						
					</div>

					<div class="col-md-6">
						<h3>Importador</h3>
				
						<div class="form-group">
							
							{!! Form::label('','Nombre o Razón Social') !!}
							{!! Form::text('razon_social_importador',null,['class'=>'form-control','id'=>'razon_social_importador', "noreq"=>"noreq"]) !!}
						</div>
						<div class="form-group">
							
							{!! Form::label('','Dirección') !!}
							{!! Form::text('direccion_importador',null,['class'=>'form-control','id'=>'direccion_importador', "noreq"=>"noreq"]) !!}
						</div>
						<div class="form-group">
							{!! Form::label('','Teléfono') !!}
							{!! Form::text('telefono',null,['class'=>'form-control','id'=>'telefono', "noreq"=>"noreq"]) !!}
						</div>
						<div class="form-group">
							{!! Form::label('','E-mail') !!}
							{!! Form::text('email_importador',null,['class'=>'form-control','id'=>'email_importador', "noreq"=>"noreq"]) !!}
						</div>
						<div class="form-group">
							{!! Form::label('','Medio de Transporte') !!}
							{!! Form::text('medio_transporte_importador',null,['class'=>'form-control','id'=>'medio_transporte_importador', "noreq"=>"noreq"]) !!}
						</div>

						<div class="form-group">
							{!! Form::label('','Puerto o Lugar Embarque') !!}
							{!! Form::text('lugar_embarque_importador',null,['class'=>'form-control','id'=>'lugar_embarque_importador', "noreq"=>"noreq"]) !!}
						</div>
					</div>
				</div>
				
				<div id="observaciones">
					<div class="row">
						<div  class="col-md-12">
							<div class="form-group">
								{!! Form::label('','Observaciones') !!}
								{!! Form::textarea('observaciones',null,['class'=>'form-control','id'=>'observaciones', "noreq"=>"noreq"]) !!}
							</div>
						</div>
					</div>
				</div>

				<div class="row text-center">
					<a href="{{ route('CertificadoOrigen.create')}}" class="btn btn-primary">Cancelar</a>
					<input type="submit" class="btn btn-success" value="Enviar" name="submit" id="submit" onclick="validacionForm()"> 
				</div>

			</div>
		</div>
		

	</div><!--div cierre panel-group general-->
</div><!--div cierre content general-->

{{Form::close()}}
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="{{asset('js/ajax_v2.1.1.js')}}"></script>
<script src="{{asset('js/CertificadoOrigenJS/COCreateCuba.js')}}"></script>


@stop