<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NumeroCuotas extends Model
{
    protected $table = 'numero_cuotas';

    protected $fillable = [

   'gen_factura_id',
   'num_cuotas',
   'monto',
   'fecha_pago'

    ];
}
