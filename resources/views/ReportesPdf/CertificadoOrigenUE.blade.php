<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>CERTIFICADO DE ORIGEN UNIÓN EUROPEA</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style type="text/css">
        .centrar{
            text-align: center;
        }

        .titulo{
            
            font-weight: bold;
        }

        .nro-correlativo{
            color: red;
            font-size: 16px;
        }

        .derecha{
            text-align: left;

        }

        .text-normal{
            font-size: 14px;
        }

        .text-medio{
            font-size: 12px;
        }
        .text-11{
            font-size: 11px;
        }
        .text-small{
            font-size: 10px;
        }

        .text-9{
            font-size: 9px;
        }

        .text-8{
            font-size: 8px;
        }

        .espacio-superior{
            margin-top: 10px;
        }

        .espacio-superior-small{
            margin-top: 5px;
        }

        .ancho-medio{
            width: 50%;
        }

        .ancho-full{
            width: 100%;
        }

        .ancho-medio-alto{
            width: 75%;
        }

        .ancho-90{
            width: 90%;
        }

        .table{
            border-collapse: collapse;
        }
        
        .borde{
            border: 1px solid #000;
        }

        .sin-borde{
            border: none;
        }

        .top{
            vertical-align: top;
        }

        .under-dotted{    
            border-bottom: 1px dotted #000;
            text-decoration: none;
        }

        .content-1{
            width: 5%;
        }

        .content-9{
            width: 20%;
        }

        .sangria-1{
            margin-left: 28px;
        }

        .sangria-2{
            margin-left: 36px;
        }

        .sangria-3{
            margin-left: 54px;
        }

        .sangria-sub{
            margin-left: 14px;
            
        }

        
        .page-break { page-break-before: always; }

        .table-center{
            margin: 0 auto; 
        }

        .justify{
            text-align: justify;
        }

        td.justify {
            padding-left: 8px;
            padding-right: 8px; 
        }
        .lineal{
            
        }
        .lineal>div{
            max-width:91%;

        }
        .lineal>*{
           display: inline-block;
           vertical-align:top;
        }

        .sangria-lista{
            list-style-type: none;
            margin: 0;
            padding: 0;
            margin-bottom: 4px;
        }

        .sangria-lista>li{
            margin-top: 5px;
            padding-left: 10px;
            padding-right: 10px;
        }
    </style>
</head>
<body>
<!-- primera parte del certificado-->
<div class="centrar espacio-superior">
        <table class="ancho-full table borde">
            <tr class="text-small">
                <td class="top borde" colspan="50">1 Goods consigned from (exporter's business name, address, country)<br><br>
                <b>{{$certificado->GenUsuario->DetUsuario->razon_social}}</b>
                <br> {{$certificado->GenUsuario->DetUsuario->direccion}} {{$certificado->pais_exportador ? $certificado->pais_exportador : ''}}
                <br></td>
                <td class="top borde" colspan="50">Referencia N°. <br>
                <h3 class="text-left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>A</b><b style="text-align:left; color:red;">29308</b></h3>
                <h4 class="centrar">GENERALIZED SYSTEM OF PREFERENCES <br>CERTIFICATE OF ORIGIN <br>(Combined declaration and certificate) <br><b>FORMA A</b></h4><br><br>
                <p>Issued in &nbsp;&nbsp;&nbsp;&nbsp;<b>República Bolivariana de Venezuela</b><br>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;............................................................................................</p>
                <div class="centrar">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(country)</div>
                </td>
            </tr>
            <tr class="text-small">
                <td class="top borde" colspan="50">2. Goods consigned to (consignee's name, address, country)<br><br>
                {{$importadorCertificado->razon_social_importador ? $importadorCertificado->razon_social_importador : ''}}  {{$importadorCertificado->direccion_importador ? $importadorCertificado->direccion_importador : ''}} {{$importadorCertificado->pais_ciudad_importador ? $importadorCertificado->pais_ciudad_importador : ''}}
                <br><br><br></td>
                <td class="top" colspan="50"><div><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;See notes overlaf</b></div>
                </td>
            </tr>
            <tr class="text-small">
                <td class="top borde" colspan="50">3. Means of transport and route (as far as known)<br><br>
                <h4>{{$certificado->direccion_productor ? $certificado->direccion_productor : ''}}   {{$importadorCertificado->lugar_embarque_importador ? $importadorCertificado->lugar_embarque_importador : ''}}</h4>
                <br><br>
                </td>
                <td class="top borde" colspan="50">4.For official use <br><br><br><br><h4 class="centrar">ISSUED RETROSPECTIVELY</h4><br><br><br><br></td>

            </tr>
            <!--parte 5 hasta la 10-->
        </table>
    </div>
    <div>
        <table class="ancho-full borde table table2">
            <tr class="text-small">
                <td class="top borde" colspan="1" style="width:3%;">5.Item Num <br>ber</td>
                <td class="top borde" colspan="1" style="width:10%;">6.Marks and number of <br>packages</td>
                <td class="top borde" colspan="3" style="width:60%;">7.Numbers and kind of packages descripcion of goods</td>
                <td class="top borde" colspan="1" style="width:10%;">8.Origin criterion <br>(see notes overlaf)</td>
                <td class="top borde" colspan="1" style="width:10%;">9.Gross weight or ther quantity</td>
                <td class="top borde" colspan="1" style="width:10%;">10.Number and <br>date of invoices</td>
            </tr>
            <tr>
                <td class="top borde" colspan="1" style="width:3%;">
                    <table>
                        @foreach ($detalleProduct as $key => $detalleP)
                            <tr class="text-small">
                                <td>
                                {{$detalleP->num_orden ? $detalleP->num_orden : ''}} <br><br><br>
                                </td><!--num orden-->
                            </tr>
                        @endforeach
                    </table>
                </td>
                <td class="top borde" colspan="1" style="width:10%;">
                    <table>
                        @foreach ($detalleProduct as $key => $detalleP)
                            <tr class="text-small">
                                <td >
                                {{$detalleP->cantidad_segun_factura ? $detalleP->cantidad_segun_factura : ''}} <br><br><br>
                                </td><!--cantidad segun factura-->
                            </tr>
                        @endforeach
                    </table>
                </td>
                <td class="top borde" colspan="3" style="width:60%;">
                    <table>
                        @foreach ($detalleProduct as $key => $detalleP)
                            <tr class="text-small">
                                <td>
                                {{$detalleP->descripcion_comercial ? $detalleP->descripcion_comercial : ''}} <br><br><br>
                                </td><!--Descripcion comercial-->
                            </tr>
                        @endforeach
                        <tr>
                            <td class="text-small">
                                {{$certificado->observaciones ? $certificado->observaciones : ''}}
                            </td>
                        </tr>
                    </table>
                </td>
                <td class="top borde" colspan="1" style="width:10%;">
                    <table><!-- normas -->
                        @foreach ($detdeclaracionCert as $key => $detalleCert)
                        <tr class="text-small">
                          <td >{{$detalleCert->normas_criterio ? $detalleCert->normas_criterio : ''}} <br> <br></td><!--normas criterios-->
                          
                        </tr>
                        @endforeach
                    </table>
                </td>
                <td class="top borde" colspan="1" style="width:10%;">
                    <table >
                        @foreach ($detalleProduct as $key => $detalleP2)
                        <tr class="text-center text-small">
                          <td >{{$detalleP2->unidad_fisica ? $detalleP2->unidad_fisica : ''}} <br> <br>
                          </td>
                        </tr>
                        @endforeach
                    </table>
                </td>
                <td class="top borde text-small" colspan="1" style="width:10%;">
                    {{$detdeclaracionCert[0]->numero_factura ? $detdeclaracionCert[0]->numero_factura : ''}} <br><br> {{$detdeclaracionCert[0]->f_fac_certificado ? $detdeclaracionCert[0]->f_fac_certificado : ''}}<!--Numero y fecha de factura-->
                </td>
            </tr>
        </table>
        <table class="ancho-full borde table table2">
            <tr class="text-small">
                <td class="top borde" colspan="50"><b>11.Certification</b><br>
                <p>It is hereby certified, on the basis of control carried out, that the declaration by the exporter is correct</p><br><br><br><br><br><br><br><br><br><br><br><br>
                .......................................................................................................................................<br><br>
                Place and date, signature and stamp of certifying authority</td>
                <td class="top borde justify" colspan="50"><b>12.Declaration by the exporter </b><br><p>The undersigned hereby declares that the above details and statements are correct; that all the goods were</p><br>
                <p>produced in   República Bolivariana de Venezuela</p>
                ........................................................................................................
                <div class="centrar">(country)</div><br>
                <div class="justify">and that they comply with the origin requirements specified for those goods in the generalized system of preferences for goods exported to</div><br>
                .........................................................................................................
                <div class="centrar">(importing country)</div><br>
                ..................................................................................................<br>
                Place and date, signature and stamp of certifying authority
                <br></td>
            </tr>
        </table>
<div style="page-break-after:always;"></div>
<div class="justify"><h5 class="centrar">APPLICATION FOR CERTIFICATE OF ORIGIN</h5><p class="centrar"><b>Form B</b></p></div>
<div class="justify text-13">the undersigned, being the exporter of the goods described overleaf. DECLARES that these goods were produced in..................<b>VENEZUELA</b>................................................................................. (country)<br>SPECIFIES as follows the grounds on which the goods are claimed to comply with GPS origin requeriments</div>
_______________________________________________________________________________________
 @foreach ($detalleProduct as $key => $detalleP)
<div class="justify text-normal">{{$detalleP->codigo_arancel ? $detalleP->codigo_arancel : ''}} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; {{$detalleP->denominacion_mercancia ? $detalleP->denominacion_mercancia : ''}}</div>
@endforeach
________________________________________________________________________________________
________________________________________________________________________________________
________________________________________________________________________________________
<div class="text-medio">SUBMITS the following supporting documents</div>
<br><br>
<div class="justify text-normal">UNDERTAKES to submit, at the request of the appropiate authorities of the exporting country any additional supporting evidence which these authorities may require for the purpose of issuing a certificate of origin, and undertakes, if require to agree to any inspection of his accounts and any check on the processes of manufacture of the above goods, carried out by the said authorities REQUESTS the issue of a certificate of origin for these goods.</div>
<br><br>
<div class="text-normal">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Place and date  Caracas _______________________________________________________</div>
<br><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;__________________________________________________________
<div class="centrar text-normal">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(signature of authorized signatory)</div>
<br>
_____________________________________________________________________________________
<br><br>
<div class="justify text-normal">(1) to be completed if materials or components originating in another country have been used in the manufacture of the goods in question. Indicate the materials or components used, their Brussels Nomenclatura tariff heading, their country of origin and, where appropiate, the manufacturing processes qualifying the goods as originating in the country of manufacture (aplication of List B or of the special conditions laid down in List A) the goods produced and their Brussels Nomenclatura tariff heading. <br>Where the origin criteria involve a percentage value, give information enabling percentage to be verified for example the value of imported materials and components and those of undertermined origin and the exfactory price of the exported goods, where applicable.</div>
<br>
<div class="centrar"><b><h5 class="text-normal">NOTES</h5></b></div>
<br>
<div class="justify text-normal">A. Procedure for claiming preference. A declaration on the certificate of origin form must be prepared by the exporter of the goods and submitted In duplicate, together with a GSP aplication form to certifying authority of the country of exportation which will, if satisfied, certify the top copy of the certificate, of origin and return in to the importer for transmission to the importer in the country of destination, the certifying authority will at the same time return to the exporter for his retention the duplicate copy of the certificate of origin, but will itself retain the GSP application, form duly completed and signed by the exporter
</div><br>
<div class="justify text-normal">B. Sanctions. Person who furnish or cause to be furnished, information which relates to origin or consignments and which is untrue in a material particular are liable to legal penalties and to the suspension of facilities for their goods to obtain preference. </div>
</div>
</body>
</html>