<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GenAsistenciaUsuario extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('gen_asistencia_usuario', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('gen_usuario_id');
            $table->string('motivo_de_contacto', 100);
            $table->text('mensaje');
            $table->text('respuesta');
            $table->unsignedInteger('gen_status_id')->nullable();
            $table->unsignedInteger('gen_usuario_atencion_id')->nullable();
            $table->timestamps();
            $table->foreign('gen_usuario_id')->references('id')->on('gen_usuario');
            $table->foreign('gen_usuario_atencion_id')->references('id')->on('gen_usuario');
            $table->foreign('gen_status_id')->references('id')->on('gen_status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gen_asistencia_usuario');
    }
}
