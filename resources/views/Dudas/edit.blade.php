@extends('templates/layoutlte')

@section('content')
{!!Form::open(['route'=>['Dudas.update', $asistencia_usuario['id'] ],'method'=>'PUT','id'=>'dudas']) !!}
@php
  $motivo_contacto = ['Atención al Exportador', 'Soporte al Usuario'];
@endphp

<div class="panels">
    <div class="row"><br>
      <div class="col-md-12">
        <div class="col-md-2"></div>
        <div class="col-md-4">
          <div class="form-group">
            {!! Form::label('','Nombre o Razón Social') !!}
            {!! Form::text('razon_social_h',$datosempresa->razon_social,['class'=>'form-control','id'=>'razon_social','onkeypress'=>'return soloLetras(event)','maxlength'=>'40', 'disabled']) !!}
            {!! Form::hidden('razon_social',$datosempresa->razon_social)!!}
          </div> 
          <div class="form-group">
            {!! Form::label('', 'Correo Electrónico') !!}
            {!! Form::email('correo_h', $datosempresa->correo, ['class' => 'form-control','id'=>'correo', 'disabled']) !!}
            {!! Form::hidden('correo',$datosempresa->correo)!!}
          </div> 
        </div><!--1era columna de 4-->
        <div class="col-md-4">
          <div class="form-group">
          {!! Form::label('','Teléfono') !!}
          {!! Form::text('telefono_movil_h',$datosempresa->telefono_movil,['class'=>'form-control','id'=>'telefono_movil','onkeypress'=>'return soloNumeros(event)','maxlength'=>'12', 'disabled']) !!}
          {!! Form::hidden('telefono_movil',$datosempresa->telefono_movil)!!}
          </div> 
          <div class="form-group">
            <label>Motivo de Contacto</label>
            {!! Form::text('tipo_h',$asistencia_usuario['motivo_de_contacto'],['class'=>'form-control','id'=>'tipo','onkeypress'=>'return soloNumeros(event)','maxlength'=>'12', 'disabled']) !!} 
            {!! Form::hidden('tipo',$datosempresa->telefono_movil)!!}
          </div>  
        </div><!-- 2da columna de 4-->
        <div class="col-md-2"></div>
      </div>    
    </div>

    <div class="row">
      <div class="col-md-12 text-center">
      <div class="col-md-2"></div>
        <div class="col-md-8">
          <div class="form-group">
            {!! Form::label('','Mensaje') !!}
            {!! Form::textarea('mensaje_registrado',$asistencia_usuario['mensaje'],['class'=>'form-control','id'=>'mensaje_registrado','onkeypress'=>'return soloNumerosyLetras(event)','maxlength'=>'500', 'required'=>'true']) !!}
          </div>  
        </div>
        <div class="col-md-2"></div>
      </div> 
    </div>
    {!! Form::hidden('gen_usuario_id', $datosempresa->gen_usuario_id)!!}
    {!! Form::hidden('asistencia_usuario_id', $asistencia_usuario['id'])!!}
    
    <div class="row">
      <div class="col-md-12 text-center">
      <div class="col-md-2"></div>
        <div class="col-md-8">
          <div class="form-group">
            @if(!empty($asistencia_usuario['respuesta']))
              {!! Form::label('','Respuesta') !!}
              {!! Form::textarea('respuesta',$asistencia_usuario['respuesta'],['class'=>'form-control','id'=>'respuesta_registrada','onkeypress'=>'return soloNumerosyLetras(event)','maxlength'=>'500', 'required'=>'true', 'disabled']) !!}
            @else
              {!! Form::label('','Sin Respuesta') !!}
            @endif
          </div>  
        </div>
        <div class="col-md-2"></div>
      </div> 
    </div>
    

    <div class="row">
      <div class="col-md-12">
      <div class="col-md-4"></div>
          <div class="col-md-4 text-center">
            <div class="form-group">
              <a href="{{url('/exportador/Dudas')}}" class="btn btn-primary">Volver</a>
              @if($asistencia_usuario['status'] == 9)
              <input class="btn btn-success" type="button" name="actualizar_mensaje" id="actualizar_mensaje" value="Actualizar", {{$asistencia_usuario['id']}})">
              @endif
              <a href="#" class="btn btn-danger btn-sm" title="Eliminar" onclick="return elimDudas('/exportador/Dudas/delete/{{$asistencia_usuario['id']}}', {{$asistencia_usuario['id']}})">Eliminar <i class="glyphicon glyphicon-trash"></i> </a>
            </div>
      </div>
      <div class="col-md-4"></div>
      </div>
    </div>
</div>

{!! Form::hidden('gen_usuario_id', $datosempresa->gen_usuario_id)!!}
{!! Form::hidden('gen_asistencia_usuario_id', $asistencia_usuario['id'], ['id' => 'gen_asistencia_usuario_id'])!!}

{{Form::close()}}

<script>
  $('#respuesta_registrada').prop('disabled', true);

</script>
@if($asistencia_usuario['status'] == 10)
<script>
    $('#mensaje_registrado').prop('disabled', true);
</script>
@endif
<script>
  //funcion para eliminar Duda****************************** ******
function elimDudas(route, id){
  
  //route = "/exportador/Dudas/delete/{{$asistencia_usuario['id']}}";
  console.log(route);
  
  swal({
      title: "¿Eliminar?",
      text: "¿Está seguro de eliminar este Registro?",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Si!, Borrarlo!",
      cancelButtonText: "No, Cancelar!",
      closeOnConfirm: false,
      closeOnCancel: false,
      showLoaderOnConfirm: true
    },
    function(isConfirm){
      if (isConfirm) {

        $.ajax({
          url: route,
          type: 'POST',
          dataType: "JSON",
          data: {
              "id": id,
              _token: "{{ csrf_token() }}"
          },
          
          success : function(response){
            console.log(response);
            swal("¡Eliminado!", "¡Duda eliminada exitosamente!", "success");
            window.location.href = "/exportador/Dudas";
          },
          error: function(err){
            console.log(err);
            swal("¡Error eliminando!", "Por favor, intente de nuevo", "error");
          }
        });
      } else {
        swal("Cancelado", "No se ha procesado la eliminación", "warning");
      }
    }
  );
  
}


$('#actualizar_mensaje').click(function (e) {
    let validar = enviarDuda();
    let id = $('#gen_asistencia_usuario_id').val();
    let route = '/exportador/Dudas/update/'+id;
    e.preventDefault();
    $(this).html('Enviando..');
    if(validar == 0){
      $.ajax({
        data: $('#dudas').serialize(),
        url: route,
        type: "PUT",
        dataType: 'json',
        success: function (data) {
            console.log(data);
            swal("¡Actualizado!", "¡Duda actualizada exitosamente!", "success");
        },
        error: function (data) {
            console.log('Error:', data);
            $('#actualizar_mensaje').html('Actualizar');
            swal("Cancelado", "No se ha procesado la actualización", "warning");
        }  
      });
    }      
  });


/* Validación Registro DUDA*/
function enviarDuda() {

  var mens=[
      'Estimado usuario. Debe ingresar el Número de DUA para completar el registro',
      'Estimado usuario. Debe ingresar el Número de Referencia para completar el registro',
      'Estimado usuario. Debe seleccionar el Agente Aduanal para completar el registro',
      'Estimado usuario. Debe seleccionar la Modalidad de Transporte para completar el registro',
      'Estimado usuario. Debe seleccionar la Aduana Salida para completar el registro',
      'Estimado usuario. Debe ingresar el Lugar de Salida para completar el registro',
      'Estimado usuario. Debe ingresar la Aduana de LLegada para completar el registro',
      'Estimado usuario. Debe ingresar el Lugar de LLegada para completar el registro',
      'Estimado usuario. Debe ingresar la Fecha de Registro de DUA en Aduanas para completar el registro',
      'Estimado usuario. Debe ingresar la Fecha de Embarque para completar el registro',
      'Estimado usuario. Debe ingresar la Fecha de Estimada de Arribo para completar el registro',
      'Estimado usuario. Debe seleccionar el consignatario para completar el registro',
      'Estimado usuario. Debe ingresar el mensaje de duda para completar el registro',
      'Estimado usuario. Debe ingresar la respuesta a la duda para completar el registro'
  ];

  $("div").remove(".msg_alert");

  let err_duda = 0;
  let error_dudas = '<div class="msg_alert"><span style="color:red" class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span><strong id="cod_aran-error" class="added"> El campo es obligatorio.</strong></div>';
  let valido_dudas = '<div class="msg_alert"><span style="color:green" class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span></div>';
  let campos = ["correo", "razon_social", "telefono_movil", "mensaje", "mensaje_registrado", "repuesta", "respuesta_analista", "tipo"];
  let nombre_campos = ["correo", "razon social", "telefono móvil", "mensaje", "mensaje", "respuesta", "respuesta", "motivo de contacto"];
  let n = campos.length;
  let input_duda = '';
  
  for (var i = 0; i < n; i++) {
    input_duda = $('#'+campos[i]);
    
    if (input_duda.val() == '')
    {
      err_duda = 1;
      input_duda.css('border', '1px solid red').after(error_dudas);
      if(err_duda==1){
        event.preventDefault(); 
        swal("Por Favor!", 'Estimado Usuario. Debe Ingresar '+nombre_campos[i].toUpperCase()+' para Completar el Registro', "warning")
        setTimeout(function(){
          $("div").remove(".msg_alert");
        }, 5000);
      }
    }
    else{
      if (err_duda == 1) {err_duda = 1;}else{err_duda = 0;}
      input_duda.css('border', '1px solid green').after(valido_dudas);
    }
  }

  if(err_duda == 1){
    setTimeout(function(){
      $("div").remove(".msg_alert");
    }, 5000);
    setTimeout(function(){
      for (var i = 0; i < n; i++) {
        input_duda = $('#'+campos[i]);
        input_duda.css('border', '1px solid #ccc');
      }
    }, 5000);
  }
  
  return err_duda;
}
</script>

@stop
