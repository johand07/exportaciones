    @extends('templates/layoutlte')

    @section('content')

    <div class="content" style="background-color: #fff">
      <div class="panel-group" style="background-color: #fff;">
        <div class="panel-primary">
          <div class="panel-heading"><h3>(P-1)-1. Datos Generales de Certificado de Origen</h3> </div>
          <div class="panel-body">
            <div class="row">
              <div class="col-md-12">
                <div class="row">
              
                  <div class="col-md-3 col-sm-3 col-xs-3">
                    <a class="btn btn-success form-control" href="{{route('CertificadoOrigen.create')}}">
                      <span class="glyphicon glyphicon-file" title="Agregar" aria-hidden="true"></span>
                      Crear Nueva Solicitud
                    </a>
                  </div>
                  <div class="col-md-2 col-sm-2 col-xs-2">

                    <a class="btn btn-primary form-control" href="{{route('CertificadoOrigen.index')}}">
                      <span class="glyphicon glyphicon-refresh" title="Agregar" aria-hidden="true"></span>
                      Actualizar
                    </a>
                  </div>

                </div> <hr>
                <table id="listaCertificados" class="table table-striped table-bordered" style="width:100%">
                  <thead>
                    <tr>
                      <th class="text-center">Nº</th>
                      <th class="text-center">Nº Certificado</th>
                      <th class="text-center">Nombre Certificado</th>
                      <th class="text-center">Rif de Exportador</th>
                      <th class="text-center">Fecha de Emisión</th>
                      <th class="text-center">Estatus</th>
                      <th class="text-center">Acciones</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($CertificadoOrigen as $key => $certificado)
                    <tr class="text-center">
                      <td width="4%">{{ $key+1 }}</td>
                      <td width="15%"><b class="text-primary">{{$certificado->num_certificado}}</b></td>
                      <td>{{$certificado->GenTipoCertificado->nombre_certificado}}</td><!--Acceder a la relacion de usuario y det para obtener la raz`on social y rif del exportador-->
                      <td>{{$certificado->GenUsuario->DetUsuario->rif}}</td>
                      <td class="col-md-2 text-center">{{ date ('d-m-Y',strtotime($certificado->created_at))}}</td>

                      <td class="col-md-1" style="font-size:12px;"><!--Estatus de certificado-->
                        <b>
                          @if($certificado->gen_status_id==11)
                          @if($certificado->observaciones_corregida==1)
                          ¡Corrección Enviada!
                          @else
                          ¡Tiene Observaciones!
                          @endif
                          @elseif($certificado->gen_status_id==10)
                          ¡Atendido por un Analista!
                          @elseif($certificado->gen_status_id==12)
                          ¡Verificado por un Analista!
                          @elseif($certificado->gen_status_id==14)
                          ¡Atendido por un Coordinador!
                          @elseif($certificado->gen_status_id==18)
                          ¡Atendido por el usuario!
                          @elseif($certificado->gen_status_id==16)
                          ¡Aprobada por el coordinador!
                          @else
                          {{ $certificado->GenStatus->nombre_status }}   
                          @endif
                        </b>
                      </td>
                      <!--Acciones de los status-->
                      <td class="col-md-2" id="estatus">
                        <input id="token" type="hidden" name="_token" value="{{ csrf_token() }}">
                        @if ($certificado->gen_status_id==7)

                            <a href="{{ route('CertificadoOrigen.index',['Co'=>encrypt($certificado->id)])}}" class="text-center btn btn-sm btn-danger list-inline "><i class="glyphicon glyphicon-exclamation-sign"></i><b>Completar</b></a>

                        @elseif($certificado->gen_status_id==8)

                            <a class="text-center btn btn-sm btn-danger list-inline"><i class="glyphicon glyphicon-remove "></i> {{ $certificado->nombre_status }}</a>

                        @elseif($certificado->gen_status_id==9)

                            <a class="text-center btn btn-sm btn-default list-inline"><i class="glyphicon glyphicon-refresh"></i><b> En Proceso</b> {{ $certificado->nombre_status }}</a>

                        @elseif($certificado->gen_status_id==10)

                            <a class="text-center btn btn-sm btn-primary list-inline"><i class="glyphicon glyphicon-user"></i> <b>Atendido</b> </a>

                        @elseif($certificado->gen_status_id==11)
                        @if(@$certificado->observacion_corregida==1)
                            <a class="text-center btn btn-sm btn-info list-inline"><i class="glyphicon glyphicon-time"></i> <b>En Observacion </b></a>
                        @else
                            <a href="{{ route('CertificadoOrigen.edit',$certificado->id) }}" class="text-center btn btn-sm btn-warning list-inline"><i class="glyphicon glyphicon-pencil text-black"></i> <b>Corregir</b></a>
                        @endif           
                        @elseif($certificado->gen_status_id==12)
                        <a class="text-center btn btn-sm btn-primary list-inline"><i class="glyphicon glyphicon-user"></i> <b>Verificado</b> </a>
                        @elseif($certificado->gen_status_id==14)
                        <a class="text-center btn btn-sm btn-primary list-inline"><i class="glyphicon glyphicon-user"></i> <b>Atendido</b> </a>
                        @elseif($certificado->gen_status_id==18)
                          Corregido por el usuario 
                        @elseif($certificado->gen_status_id==16)
                          <i class="glyphicon glyphicon-ok"></i> <b>Puede ir a Retirar </b></a>
                        @endif
                        
                      </td>
                    </tr>

                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div><!-- fin content-->
    @stop
