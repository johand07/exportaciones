<!-- Sidebar Menu -->
<ul class="sidebar-menu" data-widget="tree">
    <li class="header"><a href="#"><span class="glyphicon glyphicon-home"></span>
      {{ Auth::user()->tipoUsuario->nombre_tipo_usu}}
    </a></li>
    <!-- Optionally, you can add icons to the links
    <li class="active"><a href="#"><i class="fa fa-link"></i> <span>Lingeneral</span></a></li>  
    <li><a href="#"><i class="fa fa-link"></i> <span>Another Link</span></a></li>-->
    
   <li class="treeview">
      <a href="#"><i class="glyphicon glyphicon-paste"></i> <span>Solicitudes de Exportación</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
      </a>
      <ul class="treeview-menu">
          <li><a href="{{route('AnalistaIntranet.index')}}">Recepción de Solicitudes</a></li>
          <li><a href="{{route('solEvaluadasAnalista')}}">Mis Solicitudes Evaluadas</a></li>
      </ul>
  </li>
   <!--Items de Reportes-->
  <!--li class="treeview">
      <a href="#"><i class="glyphicon glyphicon-tags"></i> <span>Módulo de Reportes</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
      </a>
      <ul class="treeview-menu">
          <li><a href="{{url('/IntranetAnalista/ReportesErIntra')}}">Reportes Er</a></li>
          <li><a href="{{url('/IntranetAnalista/ReportesDVDIntra')}}">Reportes Dvd</a></li>
          <li><a href="{{url('/IntranetAnalista/ReportesDvdNotaCredIntra')}}">Reportes Nota de Creditos</a></li>
      </ul>
  </li-->
  <!--Items de Reportes--> 
     
    <!--<li class="treeview">
      <a href="#"><i class="glyphicon glyphicon-screenshot"></i> <span>Registro de Operación</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
      </a>
      <ul class="treeview-menu">
        <li><a href="#">Anticipo de Exportación</a></li>
        <li><a href="#">Registro de Exportación</a></li>
        <li><a href="#">Consulta de Exportación</a></li>
      </ul>
    </li>
    <li class="treeview">
      <a href="#"><i class="glyphicon glyphicon-random"></i> <span>Registro de Venta de <br>Divisa</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
      </a>
      <ul class="treeview-menu">
        <li><a href="#">Registro de DVD</a></li>
        <li><a href="#">Carga en Lote</a></li>
        <li><a href="#">Historial de Lotes</a></li>
      </ul>
    </li>
    <li class="treeview">
      <a href="#"><i class="glyphicon glyphicon-transfer"></i> <span>Asistencia al Usuario</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
      </a>
      <ul class="treeview-menu">
        <li><a href="#">Preguntas Frecuentes</a></li>
        <li><a href="#">Reportar Duda o Problema <br> de una Solicitud</a></li>
        <li><a href="#">Consulta de Dudas o Problemas</a></li>
      </ul>
    </li>
    <li class="treeview">
      <a href="#"><i class="glyphicon glyphicon-cog"></i> <span>Perfil del Exportador</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
      </a>
      <ul class="treeview-menu">
        <li><a href="#">Datos de la Empresa</a></li>
        <li><a href="#">Cambio de Contraseña</a></li>
        <li><a href="#">Cambio de Pregunta de Seguridad</a></li>
      </ul>
    </li>
  </ul>-->
  <!-- /.sidebar-menu -->
</section>
<!-- /.sidebar -->
