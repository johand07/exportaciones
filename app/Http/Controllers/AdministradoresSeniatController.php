<?php

namespace App\Http\Controllers;

use App\Models\GenUsuario;
use App\Models\CatPreguntaSeg;
use App\Models\GenTipoUsuario;
use App\Models\DetUsuario;
use App\Models\Accionistas;
use App\Models\Productos;
use App\Models\GenOperadorCamb;
use App\Models\GenStatus;
use App\Models\GenUsuarioOca;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Alert;

use Auth;

class AdministradoresSeniatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $titulo= 'Usuarios Seniat';
      $descripcion= 'Gestión de Usuarios';
      $users=GenUsuario::where('bactivo',1)->whereIn('gen_tipo_usuario_id',[15,16,17])->get();
      
     
      return view('AdministradoresSeniat.index',compact('users','titulo','descripcion'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //dd(1);
        $titulo= 'Creación de Usuarios';
        $descripcion= 'Usuarios del sistema';
        
        $preguntas=CatPreguntaSeg::all()->pluck('descripcion_preg','id');
        $tipousu=GenTipoUsuario::whereIn('id',[15,16,17])->pluck('nombre_tipo_usu','id');
        $statususu=GenStatus::whereIn('id', [1, 2])->pluck('nombre_status','id');
        //dd($statususu);
        return view('AdministradoresSeniat.create',compact('preguntas','tipousu','statususu', 'titulo', 'descripcion'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd(1);
        if (isset($request->cat_preguntas_seg_id)) {
            if (isset($request->pregunta_alter)) {
    
               $pregunta=$request['pregunta_alter'];
            }else{
    
               $pregunta='N/A';
            }
          }
          $usu= new GenUsuario;
          $usu->email=$request->email;
          $usu->email_seg=$request->email_seg;
          $usu->gen_tipo_usuario_id=$request->gen_tipo_usuario_id;
          $usu->gen_status_id=$request->gen_status_id;
          $usu->password=bcrypt($request->password);
          $usu->cat_preguntas_seg_id=$request->cat_preguntas_seg_id;
          $usu->pregunta_alter=$pregunta;
          $usu->respuesta_seg=$request->respuesta_seg;
          $usu->remember_token=str_random(10);
          $usu->save();
          Alert::success('Registro Exitoso!', 'Usuario agregado.')->persistent("Ok");
          return redirect()->action('AdministradoresSeniatController@index');
    }

    /**e
     * Display the specified resource.
     *
     * @param  \App\Models\GenUsuario  $genUsuario
     * @return \Illuminate\Http\Response
     */
    public function show(GenUsuario $genUsuario)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\GenUsuario  $genUsuario
     * @return \Illuminate\Http\Response
     */
    public function edit(GenUsuario $genUsuario ,$id)
    {
        //dd(1);
        $titulo= 'Editar Usuarios Seniat';
        $descripcion= 'Editar Usuarios';
        $usu=GenUsuario::find($id);
        $usu_edit=$usu;
        $tipousu=GenTipoUsuario::whereIn('id',[15,16,17])->pluck('nombre_tipo_usu','id');
        $statususu=GenStatus::whereIn('id', [1, 2])->pluck('nombre_status','id');
        $preguntas=CatPreguntaSeg::all()->pluck('descripcion_preg','id');
        return view('AdministradoresSeniat.edit', compact('usu_edit','preguntas','titulo','descripcion','tipousu','statususu'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\GenUsuario  $genUsuario
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, GenUsuario $genUsuario, $id)
    {
        if (isset($request->cat_preguntas_seg_id)) {
            if (isset($request->pregunta_alter)) {
    
               $pregunta=$request['pregunta_alter'];
            }else{
    
               $pregunta='N/A';
            }
          }
          
          //dd($pregunta);
          $usu= GenUsuario::find($id);
          $usu->email=$request->email;
          $usu->email_seg=$request->email_seg;
          $usu->gen_tipo_usuario_id=$request->gen_tipo_usuario_id;
          $usu->gen_status_id=$request->gen_status_id;
          $usu->password=bcrypt($request->password);
          $usu->cat_preguntas_seg_id=$request->cat_preguntas_seg_id;
          $usu->pregunta_alter=$pregunta;
          $usu->respuesta_seg=$request->respuesta_seg;
          $usu->updated_at=date("Y-m-d H:i:s");
          $usu->save();
    
          Alert::success('Registro Actualizado!', 'Usuario Actualizado.')->persistent("Ok");
          return redirect()->action('AdministradoresSeniatController@index');
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\GenUsuario  $genUsuario
     * @return \Illuminate\Http\Response
     */
    public function destroy(GenUsuario $genUsuario)
    {
        //
    }
}
