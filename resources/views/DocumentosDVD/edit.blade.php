@extends('templates/layoutlte')
@section('content')
<!--Funcion para lista dinamica-->
@php
function namefile($name){
  $explode=explode("/",$name);
  return $explode[4];

}
 $contador1=count($NotaDebitos); 
 $contador2=count($NotaCreditos); 
 $contador3=count($docEmbarques); 
 $contador4=count($CopiaSwifts); 
@endphp

<div class="content">
  {{Form::model($solicitud_Dvd,['route'=>['DocumentosDVD.update',$solicitud_Dvd->id],'method'=>'PATCH','id'=>'formDocDvd','enctype' => 'multipart/form-data'])}}
  {!!Form::hidden('gen_dvd_solicitud_id',$solicitud_Dvd->id) !!}<!--Campo oculto para recibir el id de la solicitud-->
  {{Form::hidden('contador1',$contador1)}}
  {{Form::hidden('contador2',$contador2)}}
  {{Form::hidden('contador3',$contador3)}}
  {{Form::hidden('contador4',$contador4)}}
  <!--Campos ocultos para la lista dinamica-->

   <div class="panel panel-primary">
    <div class="panel-primary" style=" #fff">
      <div class="panel-heading"><h4>Actualizar Documentos DVD</h4></div>
      <div class="panel-body">
        <div class="row" id="documentos_dvd">
        <h4>&nbsp;&nbsp;&nbsp;<b>Carga de Documentos</b></h4><br><br>
        <div class="row">
          <div class="col-md-12">
            <div class="col-md-6">
              <table class="table table-bordered text-left" id="factura">
                <span><b>Factura Comercial</b></span><br><br> 

                <td>
                  <div id="facturas_dvd" style="display:none;">
                    <strong style="color: red"> *</strong>
                    <input type="hidden" name="cat_documentos_id[]" id="cat_documentos_id" value="8">
                    <input type="file" name='ruta_doc_dvd[]' id='factura' class="file-input">
                  </div>
                  
                  <table>
                    <tr id="row5{{$Facturas->id}}">
                      <td>
                        @if(isset($Facturas))
                          <div id="imgdocumentoEdit_factura">
                            <img src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px;">
                              {{namefile($Facturas->ruta_doc_dvd)}}
                          </div>
                        @endif
                      </td>
                      <td>
                        {!!Form::hidden('cat_documentos_id[]',$Facturas->id)!!}<button  name="remove" type="button" id="{{$Facturas->id}}" value="" class="btn btn-danger btn-remove5">x</button>
                      </td>
                    </tr>
                  </table>
                  
                </td>

              </table>

            </div>
            <div class="col-md-6">
              <table class="table table-bordered text-left" id="notadebito">
                <span><b>Nota débito</b></span><br><br> 
                <tr> <td><button  name="add" type="button" id="add-notadebito" value="add more" class="btn btn-success" >Agregar</button></td></tr>
                @if (isset($NotaDebitos))

                  @foreach ($NotaDebitos as $key=>$nota)

                    <tr id="row1{{$key+1}}">

                      <td>
                      <strong style="color: red"> *</strong>
                     <div id='nombreCargado1{{$key+1}}'>
                         {{namefile($nota->ruta_doc_dvd)}}
                      </div>

                      </td>
                      <td> {!!Form::hidden('cat_documentos_id[]',$nota->id)!!}<button  name="remove" type="button" id="{{$key+1}}" value="" class="btn btn-danger btn-remove1">x</button>
                      </td>
                    </tr>
                  @endforeach
                @endif
                
              </table>
            </div>
          </div>
        </div><br><br>

        <div class="row">
          <div class="col-md-12">
            <div class="col-md-6">
              <table class="table table-bordered text-left" id="notacredito">

                <span><b>Nota crédito</b></span><br><br>
                <tr> <td><button  name="add" type="button" id="add-notacredito" value="add more" class="btn btn-success" >Agregar</button></td></tr>
                 @if (isset($NotaCreditos))

                  @foreach ($NotaCreditos as $key=>$creditos)

                    <tr id="row2{{$key+1}}">

                      <td>
                      <strong style="color: red"> *</strong>
                     <div id='nombreCargado2{{$key+1}}'>
                         {{namefile($creditos->ruta_doc_dvd)}}
                      </div>

                      </td>
                      <td> {!!Form::hidden('cat_documentos_id[]',$creditos->id)!!}<button  name="remove" type="button" id="{{$key+1}}" value="" class="btn btn-danger btn-remove2">x</button>
                      </td>
                    </tr>
                  @endforeach
                @endif
                
              </table>

            </div>
            <div class="col-md-6">
              <table class="table table-bordered text-left" id="docembarque">

                <span><b>Documento(s) de embarque (Bill Of lading,<br>guía aérea, CPIC, guía de entrega o despacho).</b></span><br><br>


                <tr> <td><button  name="add" type="button" id="add-docembarque" value="add more" class="btn btn-success" >Agregar</button></td></tr>
                @if (isset($docEmbarques))

                  @foreach ($docEmbarques as $key=>$embarque)

                    <tr id="row3{{$key+1}}">

                      <td>
                      <strong style="color: red"> *</strong>
                     <div id='nombreCargado3{{$key+1}}'>
                         {{namefile($embarque->ruta_doc_dvd)}}
                      </div>

                      </td>
                      <td> {!!Form::hidden('cat_documentos_id[]',$embarque->id)!!}<button  name="remove" type="button" id="{{$key+1}}" value="" class="btn btn-danger btn-remove3">x</button>
                      </td>
                    </tr>
                  @endforeach
                @endif
                
              </table>

            </div>

          </div>
        </div><br><br><br>

        <div class="row">
          <div class="col-md-12">
            <div class="col-md-6">
              <table class="table table-bordered text-left" id="swift">

                <span><b>Copia de mensaje(s) Swift correspondiente al ingreso <br>o pago de las divisas producto de la exportación realizada.</b></span><br><br>


                <tr> <td><button  name="add" type="button" id="add-swift" value="add more" class="btn btn-success" >Agregar</button></td></tr>
                @if (isset($CopiaSwifts))

                  @foreach ($CopiaSwifts as $key=>$mensaje)

                    <tr id="row4{{$key+1}}">

                      <td>
                      <strong style="color: red"> *</strong>
                     <div id='nombreCargado4{{$key+1}}'>
                         {{namefile($mensaje->ruta_doc_dvd)}}
                      </div>

                      </td>
                      <td> {!!Form::hidden('cat_documentos_id[]',$mensaje->id)!!}<button  name="remove" type="button" id="{{$key+1}}" value="" class="btn btn-danger btn-remove4">x</button>
                      </td>
                    </tr>
                  @endforeach
                @endif
                
              </table>

            </div>
            <div class="col-md-6">
              <table class="table table-bordered text-left" id="declaracioniva">
                <span><b>Planilla de declaración y pago del IVA.</b></span><br><br> 

                <td>
                  <div id="ivas_dvd" style="display:none;">
                    <strong style="color: red"> *</strong>
                    <input type="hidden" name="cat_documentos_id[]" id="cat_documentos_id" value="3">
                    <input type="file" name='ruta_doc_dvd[]' id='iva' class="file-input">
                  </div>
                  
                  <table>
                    <tr id="row6{{$Ivas->id}}">
                      <td>
                        @if(isset($Ivas))
                          <div id="imgdocumentoEdit_iva">
                            <img src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px;">
                            {{namefile($Ivas->ruta_doc_dvd)}}
                          </div>
                        @endif
                      </td>
                      <td>
                        {!!Form::hidden('cat_documentos_id[]',$Ivas->id)!!}<button  name="remove" type="button" id="{{$Ivas->id}}" value="" class="btn btn-danger btn-remove6">x</button>
                      </td>
                    </tr>
                  </table>
                </td>

              </table>

            </div>

          </div>
        </div><br><br>
      </div>

        <!--Botones-->
        <div class="row">
          <div class="col-md-12">
            <div class="col-md-4"></div>
            <div class="col-md-4 text-center">
              <div class="form-group">
                <a href="{{route('DvdSolicitud.index')}}" class="btn btn-primary">Cancelar</a>
                <input class="btn btn-success" type="submit" name="" value="Enviar" onclick="enviardvd()">
              </div>
            </div>
            <div class="col-md-4"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
{{Form::close()}}

<!--Scripts para lista dinamica-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="{{asset('js/ajax_v2.1.1.js')}}"></script>
<!--Script para Tooltips guia de campos con stylos bootstrap-->
<script src="https://unpkg.com/@popperjs/core@2"></script>
<script src="https://unpkg.com/tippy.js@6"></script>

<script type="text/javascript">


///Funcion para cambiar imagen//
function cambiarImagen(event) {
    var id= event.target.getAttribute('id');
    var reader = new FileReader();
    reader.onload = function(){
      var output = document.getElementById('vista_previa_'+id);
      output.src = reader.result;
    };
    let archivo = $(".file-input").val();
    if (!archivo) {
      archivo=$('#'+id).val();
    }
    var extensiones = archivo.substring(archivo.lastIndexOf(".")).toLowerCase();
    
    //var imgfile="/exportaciones/public/img/file.png";

//alert(imgfile);
    if(extensiones != ".doc" && extensiones != ".docx" && extensiones != ".pdf" && extensiones != ".ppt" && extensiones != ".pptx" && extensiones != ".xls" && extensiones != "xlsx"){
      swal("Formato invalido", "Formato de archivo invalido debe ser .doc, .docx o pdf", "warning");
        
        
    }else{
      $('#imgdocumento_'+id).show();
      $('#imgdocumentoEdit_'+id).hide();
       let file=$('#'+id).val();
     // alert(file);
     //Y pintame ruta y nombre del archivo acargado con nombre_
      $('#nombre_'+id).html('<strong>'+file+'</strong>');

      reader.readAsDataURL(event.target.files[0]);
    }

  }

  function cambiarImagenDoc(event) {

    
    //console.log(event);

    var id= event.target.getAttribute('id');
    //alert(id);
    
  
      archivo=$('#'+id).val();
    //}
    var extensiones = archivo.substring(archivo.lastIndexOf(".")).toLowerCase();
    
    //var imgfile="/exportaciones/public/img/file.png";

    
     if(extensiones != ".doc" && extensiones != ".docx" && extensiones != ".pdf" && extensiones != ".ppt" && extensiones != ".pptx" && extensiones != ".xls" && extensiones != "xlsx"){
      swal("Formato invalido", "Formato de archivo invalido debe ser .doc, .docx o pdf", "warning"); 
        $('#'+id).val('');
        
    }else{
      $('#imgdocumento_'+id).show();
      let file=$('#'+id).val();
     // alert(file);
      $('#nombre_'+id).html('<strong>'+file+'</strong>');
     
      reader.readAsDataURL(event.target.files[0]);

    }

  }

  $(document).on('change', '.file-input', function(evt) {
  
    cambiarImagen(evt);
  });

  $(document).on('change', '.file_multiple', function(evt) {
      
    cambiarImagenDoc(evt);
  });

$(document).ready(function() {


  //////////////////////Para Documentos BDV listas dinamicas////////////////////////
///////////////////////lista1 Nota debito///////////////////////lista1 Nota debito
  var i = 0;
    $('#add-notadebito').click(function(){
    i++;

   
     $('#notadebito tr:last').after('<tr id="row1'+i+'"display="block" class="show_div"><td><strong style="color: red"> *</strong><input type="hidden" name="cat_documentos_id[]" id="cat_documentos_id" value="9"><input type="file" name="ruta_doc_dvd[]" id="ruta_doc_dvd'+i+'" class="file_multiple"></td><td>{!!Form::hidden('cat_documentos_id[]',0)!!}<button  name="remove" type="button" id="'+i+'"value="" class="btn btn-danger btn-remove1">x</button></td></tr>');


    });

    $(document).on('click','.btn-remove1',function(){
      var id_boton= $(this).attr("id");
      var id=$(this).prev().val();
      if(id!=0){

        if (confirm('Deseas eliminar este Archivo?')) {

          $.ajax({

           'type':'get',
           'url':'eliminarDocDvd',
           'data':{ 'id':id},
           success: function(data){

            $('#row1'+id_boton).remove();

             }

           });

        }
      }else{

        $('#row1'+id_boton).remove();
      }

    });


  ////////////////////////lista2
  var j = 0;
    $('#add-notacredito').click(function(){
    j++;

   
    $('#notacredito tr:last').after('<tr id="row2'+j+'"display="block" class="show_div"><td><strong style="color: red"> *</strong><input type="hidden" name="cat_documentos_id[]" id="cat_documentos_id" value="43"><input type="file" name="ruta_doc_dvd[]" id="ruta_doc_dvd'+j+'" class="file_multiple"></td><td>{!!Form::hidden('cat_documentos_id[]',0)!!}<button  name="remove" type="button" id="'+j+'" value="" class="btn btn-danger btn-remove2">x</button></td></tr>');


    });

    $(document).on('click','.btn-remove2',function(){
      var id_boton= $(this).attr("id");
      var id=$(this).prev().val();
      if(id!=0){

        if (confirm('Deseas eliminar este Archivo?')) {

          $.ajax({

           'type':'get',
           'url':'eliminarDocDvd',
           'data':{ 'id':id},
           success: function(data){

            $('#row2'+id_boton).remove();

             }

           });

        }
      }else{

        $('#row2'+id_boton).remove();
      }
    });

    ////////////////////////lista3
    var k = 0;
    $('#add-docembarque').click(function(){
    k++;

    $('#docembarque tr:last').after('<tr id="row3'+k+'"display="block" class="show_div"><td><strong style="color: red"> *</strong><input type="hidden" name="cat_documentos_id[]" id="cat_documentos_id" value="42"><input type="file" name="ruta_doc_dvd[]" id="ruta_doc_dvd'+k+'" class="file_multiple"></td><td>{!!Form::hidden('cat_documentos_id[]',0)!!}<button  name="remove" type="button" id="'+k+'" value="" class="btn btn-danger btn-remove3">x</button></td></tr>');


    });

    $(document).on('click','.btn-remove3',function(){
      var id_boton= $(this).attr("id");
      var id=$(this).prev().val();
      if(id!=0){

        if (confirm('Deseas eliminar este Archivo?')) {

          $.ajax({

           'type':'get',
           'url':'eliminarDocDvd',
           'data':{ 'id':id},
           success: function(data){

            $('#row3'+id_boton).remove();

             }

           });

        }
      }else{

        $('#row3'+id_boton).remove();
      }
    });
  ////////////////////////lista4

    var l = 0;
    $('#add-swift').click(function(){
    j++;

   
     $('#swift tr:last').after('<tr id="row4'+l+'"display="block" class="show_div"><td><strong style="color: red"> *</strong><input type="hidden" name="cat_documentos_id[]" id="cat_documentos_id" value="6"><input type="file" name="ruta_doc_dvd[]" id="ruta_doc_dvd'+l+'" class="file_multiple"></td><td>{!!Form::hidden('cat_documentos_id[]',0)!!}<button  name="remove" type="button" id="'+l+'" value="" class="btn btn-danger btn-remove4">x</button></td></tr>');


    });

    $(document).on('click','.btn-remove4',function(){
      var id_boton= $(this).attr("id");
      var id=$(this).prev().val();
      if(id!=0){

        if (confirm('Deseas eliminar este Archivo?')) {

          $.ajax({

           'type':'get',
           'url':'eliminarDocDvd',
           'data':{ 'id':id},
           success: function(data){

            $('#row4'+id_boton).remove();

             }

           });

        }
      }else{

        $('#row4'+id_boton).remove();
      }
    });


     $(document).on('click','.btn-remove5',function(){
      //alert('lalalala');
      var id_boton= $(this).attr("id");
      var id=$(this).prev().val();
      if(id!=0){

        if (confirm('Deseas eliminar este Archivo?')) {

          $.ajax({

           'type':'get',
           'url':'eliminarDocDvd',
           'data':{ 'id':id},
           success: function(data){

            $('#row5'+id_boton).remove();
            $('#facturas_dvd').show();
             }

           });

        }
      }else{

        $('#row5'+id_boton).remove();
      }
    });

  $("#mvendido").prop("readonly", true);
  $("#mretencion").prop("readonly", true);

  $(document).on('click','.btn-remove6',function(){
    //alert('lelelele');
      var id_boton= $(this).attr("id");
      var id=$(this).prev().val();
      if(id!=0){

        if (confirm('Deseas eliminar este Archivo?')) {

          $.ajax({

           'type':'get',
           'url':'eliminarDocDvd',
           'data':{ 'id':id},
           success: function(data){

            $('#row6'+id_boton).remove();
            $('#ivas_dvd').show();
             }
             
           });

        }
      }else{

        $('#row6'+id_boton).remove();
      }
    });

});
</script>

@stop

