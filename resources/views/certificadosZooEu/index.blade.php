@extends('templates/layoutlte')
@section('content')
<div class="content" style="background-color: #fff">
	<div class="panel-group" style="background-color: #fff;">
		<div class="panel-primary">
			<div class="panel-heading"><h3>Datos Generales de Certificados Fitosanitario</h3> </div>
			<div class="panel-body">
				<div class="row">
					<div class="col-md-12">
						<div class="row">
							<div class="col-md-7 col-sm-7 col-xs-7">
								<h4 class="text-info"> Mis Certificados:</h4>
							</div>
							<div class="col-md-3 col-sm-3 col-xs-3">
			                    <a class="btn btn-success form-control" href="{{route('certificadosZooEu.create')}}">
			                      <span class="glyphicon glyphicon-file" title="Agregar" aria-hidden="true"></span>
			                      Registrar Certificado
			                    </a>
			                </div>   
						</div> <hr>
						<table id="listaInsai" class="table table-striped table-bordered" style="width:100%">
							<thead>
								<tr>
									<th>Número de Certificado</th>
									<th>Rif</th>
									<th>Razón Social</th>
									<th>País Exportador</th>
									<th>País Importador</th>
									<th>Lugar de Carga</th>
									<th>Medio de Transporte</th>
									<th>Identificación Medio de Transporte</th>
									<th>Acciones</th>
								</tr>
							</thead>
							<tbody id="tbodyInsai">
								@if(isset($certificadoZoo))
								 @foreach($certificadoZoo as  $certZoo)
								<tr>
									<td>{{$certZoo->num_ref_cert ? $certZoo->num_ref_cert : ''}}</td>
									<td>{{$certZoo->detUsuarios ? $certZoo->detUsuarios->rif : ''}}</td>
									<td>{{$certZoo->detUsuarios ? $certZoo->detUsuarios->razon_social : ''}}</td>
									<td>{{$certZoo->pais_export ? $certZoo->pais_export : ''}}</td>
									<td>{{$certZoo->pais_import ? $certZoo->pais_import : ''}}</td>
									<td>{{$certZoo->lug_carga ? $certZoo->lug_carga : ''}}</td>
									<td>{{$certZoo->medioTransporte ? $certZoo->medioTransporte->nombre : ''}}</td>
									<td>{{$certZoo->ident_medio_transport ? $certZoo->ident_medio_transport :''}}</td>
									<td>
										<a href="{{route('certificadosZooEu.edit',$certZoo->id)}}" class="glyphicon glyphicon-edit btn btn-primary btn-sm" title="Modificar" aria-hidden="true"></a>
										<a href="{{action('PdfController@certZooEu',array('id'=>$certZoo->id))}}" target="_blank" class="btn btn-success btn-sm glyphicon glyphicon-file">Cerificado</a>

									</td>
								</tr>
								@endforeach
								@endif
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@stop