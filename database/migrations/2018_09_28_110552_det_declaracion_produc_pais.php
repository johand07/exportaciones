<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DetDeclaracionProducPais extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('det_declaracion_produc_pais', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('det_declaracion_produc_id')->index()->nullable();
            $table->foreign('det_declaracion_produc_id')->references('id')->on('det_declaracion_produc');
            $table->unsignedInteger('pais_id')->index()->nullable();
            $table->foreign('pais_id')->references('id')->on('pais');
            $table->boolean('bactivo')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('det_declaracion_produc_pais');
    }
}
