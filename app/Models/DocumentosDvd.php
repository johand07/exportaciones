<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DocumentosDvd extends Model
{
    protected $table = 'documentos_dvd';

    	protected $fillable = [

           'gen_factura_id',
           'gen_dvd_solicitud_id',
           'cat_documentos_id',
           'gen_status_id',
           'fstatus',
           'firma_seguridad',
           'bactivo',
    ];

    protected $date =['created_at','updated_at'];

    public function factura() {

     return $this->hasOne('App\Models\GenFactura','id','gen_factura_id');

    }

    public function dvdsolicitud() {

     return $this->hasOne('App\Models\GenDVDSolicitud','id','gen_dvd_solicitud_id');

    }

    public function documentos() {

     return $this->hasOne('App\Models\CatDocumentos','id','cat_documentos_id');

    }

    public function estatus() {

     return $this->hasOne('App\Models\GenStatus','id','gen_status_id');

    }

    public function solicitud() {

     return $this->hasOne('App\Models\GenSolicitud','id','gen_solicitud_id');

    }
}
