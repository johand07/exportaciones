<?php

namespace App\Http\Controllers;

use App\Models\GenDVDSolicitud;
use App\Models\GenSolicitud;
use App\Models\GenStatus;
use App\Models\GenOperadorCamb;
use App\Models\GenFactura;
//use App\Http\Requests\GenDuaRequest;
use Session;
use Auth;
use Laracasts\Flash\Flash;
use Alert;
use Illuminate\Http\Request;

class GenDVDSolicitudController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $titulo="Declaración de Ventas de Divisas";
        $descripcion="Exportación Realizada";
        $solicitudes=GenSolicitud::where('gen_usuario_id',Auth::user()->id)->where('bactivo',1)->whereNotNull('monto_solicitud')->get();
        return view('DvdSolicitud.index',compact('solicitudes','titulo','descripcion'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\GenDVDSolicitud  $genDVDSolicitud
     * @return \Illuminate\Http\Response
     */
    public function show(GenDVDSolicitud $genDVDSolicitud)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\GenDVDSolicitud  $genDVDSolicitud
     * @return \Illuminate\Http\Response
     */
    public function edit(GenDVDSolicitud $genDVDSolicitud)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\GenDVDSolicitud  $genDVDSolicitud
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, GenDVDSolicitud $genDVDSolicitud)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\GenDVDSolicitud  $genDVDSolicitud
     * @return \Illuminate\Http\Response
     */
    public function destroy(GenDVDSolicitud $genDVDSolicitud)
    {
        //
    }
}
