<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\CatSectorProductivoEco;
use Auth;

class AjaxGenSectorEconomico extends Controller
{
      public function listar(){

         /*$paises=Pais::select('text','dpais','cpais')->where('bactivo',1)->get();*/

        $sect_produc_eco=CatSectorProductivoEco::select('nombre_sector','id')->where('bactivo',1)->get();

        $listasector=[]; 

        foreach ($sect_produc_eco as $key => $value) {

            $listasector[]=['text'=>''.$value->nombre_sector.'','value'=>''.$value->id.''];
        }
         //dd($listasector);
        return response()->json($listasector);    }
}
