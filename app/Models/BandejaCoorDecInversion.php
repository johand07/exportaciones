<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BandejaCoorDecInversion extends Model
{
    protected $table = 'bandeja_coord_dec_inversion';
    
    protected $fillable = [
        'gen_declaracion_inversion_id',
        'gen_usuario_id',
        'gen_status_id',
        'fstatus',
    ];

    public function genDecInversion()
	{
        return $this->hasOne('App\Models\GenDeclaracionInversion','id','gen_declaracion_inversion_id');
    }

    public function genUsuario()
	{
        return $this->hasOne('App\Models\GenUsuario','id','gen_usuario_id');
    }

    public function genStatus()
	{
        return $this->hasOne('App\Models\GenStatus','id','gen_status_id');
    }
}
