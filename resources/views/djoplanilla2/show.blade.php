@extends('templates/layoutlte')
@section('content')
<div class="content">
<div class="panel-group" style="background-color: #fff;">
      <div class="panel-primary">
        <div class="panel-heading"><h3>(P-2) - 4. Datos Generales del Producto a Exportar</h3></div>
        <div class="panel-body">



<div class="row" style="background-color: #fff">
    <div class="col-md-12">
 
        <div class="row">
            <!--div class="col-md-12">
                <h3>4. Datos generales del producto a exportar</h3>
            </div-->
            <div class="col-md-6">
                <div class="form-group">
                    <label>4.1 Denominación Comercial</label>
                    <label class="form-control">{{$producto->descrip_comercial}}</label>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>4.2 Descripción Arancelaria</label>
                    <label class="form-control">{{$producto->descripcion}}</label>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>4.3 Código Arancelario</label>
                    <label class="form-control">{{$producto->codigo}}</label>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>4.4 Unidad de medida</label>
                    <label class="form-control">{{$producto->unidadMedida->dunidad}}</label>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label>4.5 Uso y aplicación</label>
                    <label class="form-control">{{$planilla2->uso_aplicacion}}</label>
                </div>
            </div>
        </div>
  
        <div class="row" style="background-color: #fff">
            <div class="col-md-12">
                <p><b>4.6 Materiales que intervienen en la elaboración del producto</b></p>
            </div>
        <div class="col-md-12">

                    <!-- ############################################################### -->
             
              <div class="row">
                  <div class="col-md-12">

                      <table class="table table-striped table-info" id="fieldse">
                            <tr>
                              <th>Materiales Importados </th>
                              <th>Código Arancelario</th>
                              <th>País de Origen</th>
                              <th>Incidencia sobre costo total de Materiales importados</th>
                            </tr>

                            @foreach($materiales_importados as $materiales)
                              <tr id="row">
                                <td>                
                                    {{$materiales->descripcion or ''}}
                                </td>
                                <td>
                                    {{$materiales->codigo or '' }}
                                </td>
                                <td>
                                    {{$materiales->pais->dpais or '' }}
                                </td>
                                <td>
                                    {{$materiales->incidencia_costo or '' }}
                                </td>
                              </tr>
                            @endforeach

                            <tr>
                              <td colspan="3" align="right" style="vertical-align:middle">
                                  <b>Total:</b>
                              </td>
                              <td>
                                 <label class="form-control"  id="total_incidencia_importado">{{$planilla2->total_mate_import}}</label>
                              </td>
                              <td>
                              </td> 
                            </tr>
                      </table>
         
                  </div>
              </div>
        </div>

</div>
<hr>  
        <!-- ################################################################ -->
<div class="row" style="background-color: #fff">
        <div class="col-md-12">

              <div class="row">
                  <div class="col-md-12">

                      <table class="table table-striped table-info" id="fieldse2">
                            <tr>
                              <th>Materiales Nacionales</th>
                              <th>Código Arancelario</th>
                              <th>Nombre del Productor Nacional</th>
                              <th>Nombre del Proveedor</th>
                              <th>Incidencia sobre costo total De Materiales Nacionales</th>
                              <th>Rif del Productor</th>
                              
                            </tr>

                            @foreach($materiales_nacionales as $materiales)
                              <tr id="row">
                                 <td>                
                                 {{$materiales->descripcion or '' }}
                                </td>
                                <td>
                                   {{$materiales->codigo or '' }}
                                </td>
                                <td>
                                   {{$materiales->nombre_produc_nac or '' }}
                                </td>
                                 <td>
                                   {{$materiales->nombre_provedor or '' }}
                                </td>

                                <td>
                                   {{$materiales->incidencia_costo or '' }}
                                </td>
                                <td>
                                   {{$materiales->rif_productor or '' }}
                                </td>
                                
                              </tr>
                            @endforeach

                            <tr>
                              <td colspan="4" align="right" style="vertical-align:middle">
                                  <b>Total:</b>
                              </td>
                              <td>
                                 <label class="form-control"  id="total_incidencia_nacional">{{$planilla2->total_mate_nac}}</label>
                              </td>
                              <td>
                              </td> 
                            </tr>
                      </table>
         
                  </div>
              </div>
        </div>

           <!--  ###############################################################  -->
        </div>




</div>

        </div>
      </div>
    </div>

  </div><!-- Fin content -->



<!--script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script-->

@endsection
