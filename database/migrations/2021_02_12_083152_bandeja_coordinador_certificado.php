<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BandejaCoordinadorCertificado extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('bandeja_coordinador_certificado', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('gen_certificado_id')->default(1)->index()->nullable();
            $table->foreign('gen_certificado_id')->references('id')->on('gen_certificado');
            $table->unsignedInteger('gen_usuario_id')->default(1)->index()->nullable();
            $table->foreign('gen_usuario_id')->references('id')->on('gen_usuario');
            $table->unsignedInteger('gen_status_id')->default(1)->index()->nullable();
            $table->foreign('gen_status_id')->references('id')->on('gen_status');
            $table->boolean('bactivo')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bandeja_coordinador_certificado');
    }
}
