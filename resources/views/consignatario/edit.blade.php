@extends('templates/layoutlte')
@section('content')
{{Form::model($consig,['route'=>['consignatario.update', $consig->id],'method'=>'PATCH','id'=>'formconsig'])}}
<br><div class="row"> 
<div class="col-md-12">
	<div class="col-md-6">
	<div class="form-group">
			{!! Form::hidden('gen_usuario_id', Auth::user()->id) !!}
			{!! Form::label('Cartera','Cartera Comprador / Proveedor')!!}
			{!! Form::select('cat_tipo_cartera_id',$tipocartera,null,['class'=>'form-control','id'=>'cat_tipo_cartera_id','placeholder'=>'Seleccione Comprador / Proveedor','onchange'=>'MostrarRif()']) !!}
		</div>
		<div class="form-group" id="rif" style="display:none;">
			{!! Form::label('consignatario','Rif Empresa')!!}
			{!! Form::text('rif_empresa',null,['class'=>'form-control','id'=>'rif_empresa','maxlength'=>'11','pattern'=>'^([VEJPG]{1})([-]{1})([0-9]{9})$'])!!}
					
		</div>
		<div class="form-group">
			{!! Form::label('consignatario','Nombre o Razón Social')!!}
			{!! Form::text('nombre_consignatario',null,['class'=>'form-control','id'=>'nombre_consignatario','maxlength'=>'100','onkeypress'=>'return soloNumerosyLetras(event)'])!!}
		</div>
		<div class="form-group">
			{!! Form::label('Contacto','Persona de Contacto')!!}
			{!! Form::text('perso_contac',null,['class'=>'form-control','id'=>'perso_contac','maxlength'=>'80','onkeypress'=>'return soloLetras(event)'])!!}
		</div>
		<div class="form-group">
			{!! Form::label('Cargo','Cargo de la Persona de Contacto')!!}
			{!! Form::text('cargo_contac',null,['class'=>'form-control','id'=>'cargo_contac','maxlength'=>'60','onkeypress'=>'return soloLetras(event)'])!!}
		</div>		
		<div class="form-group" id="pais" style="display:none;">
			{!! Form::label('paises','País') !!}
			{!! Form::select('pais_id',$paises,null,['class'=>'form-control','id'=>'pais_id',
			'placeholder'=>'Seleccione un pais']) !!}
		</div>
	</div>
	<div class="col-md-6">
			<table style="width: 100%; display:none;" id="convenio">
			<tr>
				<td>
					<div class="form-group">
						{!! Form::label('conevio','Tipo de Convenio') !!}
						{!! Form::select('cat_tipo_convenio_id',$convenio,null,['class'=>'form-control','id'=>'cat_tipo_convenio_id',
						'placeholder'=>'Seleccione un Convenio']) !!}
					</div>
				</td>
				<td align="center">
					<a id="infoCOnvenios" style="cursor:pointer;" data-toggle="modal" data-target="#myModal"><span class="glyphicon glyphicon-info-sign text-info" aria-hidden="true" ></span></a>
				</td>
			</tr>
		</table>
		<div class="form-group">
			{!! Form::label('ciudad','Ciudad')!!}
			{!! Form::text('ciudad_consignatario',null,['class'=>'form-control','id'=>'ciudad_consignatario','onkeypress'=>'return soloLetras(event)','maxlength'=>'100'])!!}
		</div>
		<div class="form-group">
			{!! Form::label('direccion','Dirección')!!}
			{!! Form::text('direccion',null,['class'=>'form-control','id'=>'direccion','maxlength'=>'200'])!!}
		</div>
		<div class="form-group">
			{!! Form::label('telefono','Teléfono de Contacto')!!}
			{!! Form::text('telefono_consignatario',null,['class'=>'form-control','id'=>'telefono_consignatario','onkeypress'=>'return soloNumeros(event)','maxlength'=>'17'])!!}
		</div>
		<div class="form-group">
			{!! Form::label('email','Correo Electrónico')!!}
			{!! Form::text('correo',null,['class'=>'form-control','id'=>'correo','maxlength'=>'80'])!!}
		</div>
	</div>
</div>
</div>
<br><div class="row">
	<div class="col-md-12">
		<div class="col-md-4"></div>
		<div class="col-md-4 text-center">
		<a href="{{ route('consignatario.index') }}" class="btn btn-primary">Cancelar</a>
		<input type="submit" class="btn btn-success" value="Enviar" name="" onclick="validacionForm()">
		</div>
		<div class="col-md-4"></div>
	</div>
</div>

{{Form::close()}}
<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Información de Convenios</h4>
      </div>
      <div class="modal-body">
            <table class="table table-bordered">
                <tbody>
                <tr class="bg-primary">
                    <th>ALADI</th>
                    <th>CAN</th>
                    <th>G3</th>
                    <th>SUCRE</th>
                    <th>MERCOSUR</th>
                </tr>
                <tr>
                    <td>
                    	Argentina
						Bolivia
						Brasil
						Chile
						Colombia
						Ecuador
						Mexicanos
						Paraguay
						Perú
						Uruguay
						Venezuela
                    </td>
                    <td>
	                    Bolivia
						Colombia
						Ecuador
						Perú
						Venezuela
                    </td>
                    <td>
	                    México
						Colombia
						Venezuela
                    </td>
                    <td>
	                    Bolivia
						Cuba
						Ecuador
						Nicaragua
						Venezuela
                    </td>
                    <td>
	                    Argentina
	                    Brasil
						Paraguay
						Venezuela
                    </td>
                </tr>
            </tbody>
        </table>     
        </div>
      <div class="modal-footer">
        <div class="row">
          <div class="col-md-12">
            <div class="col-md-4"></div>
            <div class="col-md-4 text-center">
              <div class="form-group">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
              </div>
            </div>
            <div class="col-md-4"></div>
          </div>
        </div>
      </div>
    </div>

  </div>
</div>
<!-- fin Modal -->


<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="{{asset('js/ajax_v2.1.1.js')}}"></script>

<script>
	$(document).ready(function(){
		MostrarRif();
	});
</script>

<script type="text/javascript">
  	
	//Validación de formulario, se puede llamar y pasar la sección que contenga los inputs a validar

    	var error = '<div class="msg_alert"><span style="color:red" class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span><strong id="cod_aran-error" class="added"> El campo es obligatorio.</strong></div>';
		var valido = '<div class="msg_alert"><span style="color:green" class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span></div>';
		var mens = ['Estimado Usuario. Debe completar los campos solicitados', 'El campo RIF debe respetar el formato J-000000000, asegúrese de colocar el guíon y todos los números de su rif']

		validacionForm = function() {

		$('#formconsig').submit(function(event) {
		
		var campos = $('#formconsig').find('input:text, select');
		var n = campos.length;
		var err = 0;

		$("div").remove(".msg_alert");
		//bucle que recorre todos los elementos del formulario
		for (var i = 0; i < n; i++) {
				var cod_input = $('#formconsig').find('input:text, select').eq(i);
				if (!cod_input.attr('noreq')) {
					if (cod_input.val() == '' || cod_input.val() == null)
					{
						err = 1;
						cod_input.css('border', '1px solid red').after(error);
					}
					else{
						if (err == 1) {err = 1;}else{err = 0;}
						cod_input.css('border', '1px solid green').after(valido);
					}
					
				}
		}
		if (!$("#rif_empresa").attr('noreq')) {
			if ($("#rif_empresa:contains('-')").val() || $("#rif_empresa").val().length < 11) {
				
				err = 2;

			}
		}

		//Si hay errores se detendrá el submit del formulario y devolverá una alerta
		if(err==1){
				event.preventDefault();
				swal("Por Favor!", mens[0], "warning")
			}

		//Si hay errores se detendrá el submit del formulario y devolverá una alerta
		if(err==2){
				event.preventDefault();
				swal("Por Favor!", mens[1], "warning")
			}

			});
		}
</script>

@stop