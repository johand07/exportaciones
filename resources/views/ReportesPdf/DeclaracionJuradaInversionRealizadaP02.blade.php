<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>PLANILLA DJIR02</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style type="text/css">
        .centrar{
            text-align: center;
        }

        .titulo{
            
            font-weight: bold;
        }

        .nro-correlativo{
            color: red;
            font-size: 16px;
        }

        .derecha{
            text-align: left;

        }

        .text-normal{
            font-size: 14px;
        }

        .text-medio{
            font-size: 12px;
        }
        .text-11{
            font-size: 11px;
        }
        .text-small{
            font-size: 10px;
        }

        .text-9{
            font-size: 9px;
        }

        .text-8{
            font-size: 8px;
        }

        .espacio-superior{
            margin-top: 5px;
        }

        .espacio-superior-small{
            margin-top: 5px;
        }

        .ancho-medio{
            width: 50%;
        }

        .ancho-full{
            width: 100%;
        }

        .ancho-medio-alto{
            width: 75%;
        }

        .ancho-90{
            width: 90%;
        }

        .table{
            border-collapse: collapse;
        }
        
        .borde{
            border: 1px solid #fff;
        }

        .sin-borde{
            border: none;
        }

        .top{
            vertical-align: top;
        }

        .under-dotted{    
            border-bottom: 1px dotted #000;
            text-decoration: none;
        }

        .content-1{
            width: 5%;
        }

        .content-9{
            width: 20%;
        }

        .sangria-1{
            margin-left: 28px;
        }

        .sangria-2{
            margin-left: 36px;
        }

        .sangria-3{
            margin-left: 54px;
        }

        .sangria-sub{
            margin-left: 14px;
            
        }

        
        .page-break { page-break-before: always; }

        .table-center{
            margin: 0 auto; 
        }

        .justify{
            text-align: justify;
        }

        td.justify {
            padding-left: 8px;
            padding-right: 8px; 
        }
        .lineal{
            
        }
        .lineal>div{
            max-width:91%;

        }
        .lineal>*{
           display: inline-block;
           vertical-align:top;
        }

        .sangria-lista{
            list-style-type: none;
            margin: 0;
            padding: 0;
            margin-bottom: 4px;
        }

        .sangria-lista>li{
            margin-top: 5px;
            padding-left: 10px;
            padding-right: 10px;
        }
    </style>
</head>
<body>
    <div class="centrar espacio-superior">
        <table style="width:100%;" class="table2">
          <tbody>
            <tr>
              <td class="td3">
                <img class="img-responsive" style="width:200px; height: 40px;" src='img/MIPPCOEXIN.png'>
              </td>
           <td class="nro-correlativo" style="width: 30%; font-size:80%; text-align: left;">
            <td class="nro-correlativo" style="width: 30%; font-size:80%; text-align: left;">
               <td>
                  <img class="img-responsive borde" style="width: 300px; height: 40px; text-align: right;" src='img/cintillo.png'>
              </td>
            </tr>
          </tbody>
        </table>
    </div><br>

    <div class="centrar espacio-superior">
        <table class="ancho-full">
            <tr>
                <td class="center" style="width: 70%; font-size:107%; text-align: center;">
                    PLANILLA (DJIR 02)
                </td>
            </tr>
            <tr>
                <td class="center" style="width: 70%; font-size:14px; text-align: center;">
                     <b>PLANILLA DE DECLARACIÓN JURADA DE INVERSIÓN REALIZADA 02
                        (INFORMACIÓN FINANCIERA)</b>
                </td>
            </tr>
        </table>
    </div>

    <div class="centrar espacio-superior">
        <table class="ancho-full">
            <tr>
            	<td class="nro-correlativo" style="width: 30%; font-size:80%; text-align: left;">
                     <b>Tipo de Persona:</b><b style="color: black">{{ $solicitudDeclaracion->CatTipoUsuario->nombre}}</b>
                </td>
                
                <td class="nro-correlativo" style="width: 30%; font-size:80%; text-align: right;"></td>
                <td class="nro-correlativo" style="width: 30%; font-size:80%; text-align: right;">
                     <b>Fecha Solicitud:</b><b style="color: black">{{$solicitudDeclaracion->created_at}}
                </td>

            </tr>
            <tr>
            	<td class="nro-correlativo" style="width: 30%; font-size:80%; text-align: right;"></td>
                <td class="nro-correlativo" style="width: 30%; font-size:80%; text-align: right;"></td>
                <td class="nro-correlativo" style="width: 30%; font-size:80%; text-align: right;">
                     <b>Nº Solicitud:</b><b style="color: black">{{$solicitudDeclaracion->num_declaracion}}</b>
                </td>
            </tr>
        </table>
    </div>
    <br><br>
    <!--Grupo Internacional-->
    <div class="centrar espacio-superior">
        <table class="ancho-full" border="1">
              <tr>
                <td class="top" style="background-color:#818286; color:#fff; text-align: left; font-size: 13px" colspan="50"><b>Pertenece a un Grupo Empresarial Internacional?</b></td>
            </tr> 
            @if(isset($planillaDjir02->grupo_emp_internacional) && $planillaDjir02->grupo_emp_internacional == 1)   
            <tr>
                <td class="top" style="text-align: left; font-size: 10px" colspan="100"><b>Si</b></td>
               
            </tr>
            @else
            <tr>
                <td class="top" style="text-align: left; font-size: 10px" colspan="100"><b>No</b></td>
            </tr> 
            @endif
        </table>
    </div>
    @if(isset($planillaDjir02->grupo_emp_internacional) && $planillaDjir02->grupo_emp_internacional == 1)
        <div class="centrar espacio-superior" id="datos_repre">
            <table class="ancho-full" border="1">
                <tr>
                    <td class="top" style="text-align: left; font-size: 10px" colspan="50"><h5><b>Si respondió si, por favor mencione las empresas relacionadas:</b></h5></td>
                    <td>{{$planillaDjir02->emp_relacionadas_internacional ? $planillaDjir02->emp_relacionadas_internacional:''}}</td>
                </tr>
                <tr>
                    <td class="top" style="text-align: left; font-size: 10px" colspan="50"><b>Casa Matriz:</b></td>
                    <td>{{$planillaDjir02->casa_matriz ? $planillaDjir02->casa_matriz:''}}</td>
                </tr>
                <tr>
                    <td class="top" style="text-align: left; font-size: 10px" colspan="50"><b>Empresa Representante de la Casa Matriz para <br>América Latina:</b></td>
                    <td>{{$planillaDjir02->emp_repre_casa_matriz ? $planillaDjir02->emp_repre_casa_matriz:''}}</td>
                </tr>
                <tr>
                    <td class="top" style="text-align: left; font-size: 10px" colspan="50"><b>Afiliadas:</b></td>
                    <td>{{$planillaDjir02->afiliados_internacional ? $planillaDjir02->afiliados_internacional:''}}</td>
                </tr>
                <tr>
                    <td class="top" style="text-align: left; font-size: 10px" colspan="50"><b>Subsidiarias de su empresa en el Extranjero:</b></td>
                    <td>{{$planillaDjir02->sub_emp_extranjero ? $planillaDjir02->sub_emp_extranjero:''}}</td>
                </tr>
            </table>
        </div>
    @endif
<!--Grupo Nacional-->
<div class="centrar espacio-superior">
        <table class="ancho-full" border="1">
             <tr>
                <td class="top" style=" background-color:#818286; color:#fff;text-align: left; font-size: 13px" colspan="50"><b>Pertenece a un Grupo Empresarial Nacional?</b></td>
            </tr> 
     @if(isset($planillaDjir02->grupo_emp_nacional) && $planillaDjir02->grupo_emp_nacional == 1)
            <tr>
                <td class="top" style="text-align: left; font-size: 10px" colspan="100"><b>Si</b></td>
            </tr>
        @else
            <tr>
                <td class="top" style="text-align: left; font-size: 10px" colspan="100"><b>No</b></td>
            </tr>
        @endif
        </table>
    </div>
  @if(isset($planillaDjir02->grupo_emp_nacional) && $planillaDjir02->grupo_emp_nacional == 1)
    <div class="centrar espacio-superior" id="datos_emp">
        <table class="ancho-full" border="1">
            <tr>
                <td class="top" style="text-align: left; font-size: 10px" colspan="50"><b>Empresa Holding, Corporación o Consorcio:</b></td>
                <td>{{$planillaDjir02->emp_haldig_coorp ? $planillaDjir02->emp_haldig_coorp:''}}</td>
            </tr>
            <tr>
                <td class="top" style="text-align: left; font-size: 10px" colspan="50"><b>Afiliadas:</b></td>
                <td>{{$planillaDjir02->afiliados_nacional ? $planillaDjir02->afiliados_nacional:''}}</td>
            </tr>
            <tr>
                <td class="top" style="text-align: left; font-size: 10px" colspan="50"><b>Subsidiarias locales:</b></td>
                <td>{{$planillaDjir02->sub_locales ? $planillaDjir02->sub_locales:''}}</td>
            </tr>
        </table>
    </div>
@endif
<!--Representacion Legal-->
    <div class="centrar espacio-superior">
        <table class="ancho-full" border="1">
             <tr>
                <td class="top" style=" background-color:#818286; color:#fff; text-align: left; font-size: 13px" colspan="50"><b>Ha presentado ante el Despacho de Viceministro para el Comercio Exterior y Promoción de Inversiones los documentos que acreditan la representación legal o poder?</b></td>
            </tr> 
     @if(isset($planillaDjir02->present_doc_repre_legal) && $planillaDjir02->present_doc_repre_legal == 1)
           
            <tr>
                <td class="top" style="text-align: left; font-size: 10px" colspan="100"><b>Si</b></td>
            </tr>
        @else
            <tr>
                <td class="top" style="text-align: left; font-size: 10px" colspan="100"><b>No</b></td>
            </tr>
        @endif  
        </table>
    </div>
 @if(isset($planillaDjir02->present_doc_repre_legal) && $planillaDjir02->present_doc_repre_legal ==1) 
    <div class="centrar espacio-superior" id="representante">
        <table class="ancho-full" border="1">
            <tr>
                <td class="top" style="text-align: left; font-size: 13px" colspan="100"><b>EN CASO AFIRMATIVO, PROPORCIONE LOS SIGUIENTES DATOS:</b></td>
            </tr>
            <tr>
                <td class="top" style="text-align: left; font-size: 10px" colspan="50"><b>N° de Apostilla:</b></td>
                <td colspan="50">{{$planillaDjir02->num_apostilla ? $planillaDjir02->num_apostilla:''}}</td>
            </tr>
            <tr>
                <td class="top" style="text-align: left; font-size: 10px" colspan="50"><b>Fecha:</b></td>
                <td colspan="50">{{$planillaDjir02->fecha_apostilla ? $planillaDjir02->fecha_apostilla:''}}</td>
            </tr>
            <tr>
                <td class="top" style="text-align: left; font-size: 10px" colspan="50"><b>País:</b></td>
                <td colspan="50">{{$planillaDjir02->pais ? $planillaDjir02->pais:''}}</td>
            </tr>
            <tr>
                <td class="top" style="text-align: left; font-size: 10px" colspan="50"><b>Estado o Ciudad:</b></td>
                <td colspan="50">{{$planillaDjir02->estado ? $planillaDjir02->estado:''}}</td>
            </tr>
            <tr>
                <td class="top" style="text-align: left; font-size: 10px" colspan="50"><b>Autoridad competente para apostillar:</b></td>
                <td colspan="50">{{$planillaDjir02->autoridad_apostilla ? $planillaDjir02->autoridad_apostilla:''}}</td>
            </tr>
            <tr>
                <td class="top" style="text-align: left; font-size: 10px" colspan="50"><b>Cargo</b></td>
                <td colspan="50">{{$planillaDjir02->cargo ? $planillaDjir02->cargo:''}}</td>
            </tr>
            <tr>
                <td class="top" style="text-align: left; font-size: 10px" colspan="50"><b>Traductor:</b></td>
                <td colspan="50">{{$planillaDjir02->traductor ? $planillaDjir02->traductor:''}}</td>
            </tr>
            <tr>
                <td class="top" style="text-align: left; font-size: 10px" colspan="50"><b>Datos Adicionales:</b></td>
                <td colspan="50">{{$planillaDjir02->datos_adicionales ? $planillaDjir02->datos_adicionales:''}}</td>
            </tr> 
            </table>
        </div>
     @endif
<!--iNGRESOS & EGRESOS-->
<div class="centrar espacio-superior">
    <table class="ancho-full" border="1">
        <tr>
            <td class="top" style="text-align: left; font-size: 10px" colspan="50"><b>Ingresos anuales promedio del último ejercicio:</b></td>
            <td colspan="50">{{$planillaDjir02->ingresos_anual_ult_ejer ? $planillaDjir02->ingresos_anual_ult_ejer:''}}</td>
        </tr>
        <tr>
            <td class="top" style="text-align: left; font-size: 10px" colspan="50"><b>Egresos anuales promedio del último ejercicio:</b></td>
            <td colspan="50">{{$planillaDjir02->egresos_anual_ult_ejer ? $planillaDjir02->egresos_anual_ult_ejer:''}}</td>
        </tr>
        <tr>
            <td class="top" style="text-align: left; font-size: 10px" colspan="50"><b>Total balance del último ejercicio:</b></td>
            <td colspan="50">{{$planillaDjir02->total_balance_ult_ejer ? $planillaDjir02->total_balance_ult_ejer:''}}</td>
        </tr> 
    </table>
</div>

<!--Tipo de Inversion-->
<div class="centrar espacio-superior">
    <table class="ancho-full" border="1">
        <tr>
            <td class="top" style="background-color:#818286; color:#fff; text-align: left; font-size: 13px" colspan="100"><b>Detalle de la Declaración Jurada de la Inversión Realizada</b></td>
        </tr>
        <tr>
            <td class="top" style="text-align: left; font-size: 10px" colspan="40"><b>Año de Declaración</b></td>
            <td colspan="60">{{$planillaDjir02->anio_informacion_financiera ? $planillaDjir02->anio_informacion_financiera:''}}</td>
        </tr>
    </table>
</div>
<!--Inversion Financiera-->
    <div class="centrar espacio-superior">
        <table class="ancho-full" border="1">
            <tr>
                <td class="top" style="background-color:#818286; color:#fff; text-align: left; font-size: 13px" colspan="100"><b>Inversión Financiera en divisas y/o cualquier otro medio de cambio</b></td>
            </tr>
            <tr>
                <td class="top" style="text-align: left; font-size: 10px" colspan="60"><b>Tipo de Moneda</b></td>
                 <td colspan="40">{{$planillaDjir02->infoFinanciera ? $planillaDjir02->infoFinanciera->ddivisa_abr : ''}}</td>
            </tr>
            <tr>
                <td class="top" style="text-align: left; font-size: 10px" colspan="60"><b>Moneda extranjera</b></td>
                <td colspan="40">{{$planillaDjir02->moneda_extranjera ? $planillaDjir02->moneda_extranjera:''}}</td>
            </tr>
            <tr>
                <td class="top" style="text-align: left; font-size: 10px" colspan="60"><b>Utilidades reinvertidas</b></td>
                <td colspan="40">{{$planillaDjir02->utilidades_reinvertidas ? $planillaDjir02->utilidades_reinvertidas:''}}</td>
            </tr>
            <tr>
                <td class="top" style="text-align: left; font-size: 10px" colspan="60"><b>Crédito con casa matriz y/o filial extranjera </b></td>
                <td colspan="40">{{$planillaDjir02->credito_casa_matriz ? $planillaDjir02->credito_casa_matriz:''}}</td>
            </tr>
        </table>
    </div>

<!--Bienes Tangibles-->
<br>
<div class="centrar espacio-superior">
    <table class="ancho-full" border="1">
        <tr>
            <td class="top" style="background-color:#818286; color:#fff; text-align: left; font-size: 13px" colspan="100"><b>Bienes de capital fisico o tangibles (Maquinarias, equipos,  herramientas o cualquier otro tipo de activo tangible). </b></td>
        </tr>
         <tr>
            <td class="top" style="text-align: left; font-size: 10px" colspan="50"><b>Tipo de Moneda</b></td>
             <td colspan="50">{{$planillaDjir02->bienesTangibles ? $planillaDjir02->bienesTangibles->ddivisa_abr : ''}}</td>
        </tr>
        <tr>
            <td class="top" style="text-align: left; font-size: 10px" colspan="50"><b>Tierras y terrenos</b></td>
            <td colspan="50">{{$planillaDjir02->tierras_terrenos ? $planillaDjir02->tierras_terrenos:''}}</td>
        </tr>
        <tr>
            <td class="top" style="text-align: left; font-size: 10px" colspan="50"><b>Edificios y otras construcciones</b></td>
            <td colspan="50">{{$planillaDjir02->edificios_construcciones ? $planillaDjir02->edificios_construcciones:''}}</td>
        </tr>
        <tr>
            <td class="top" style="text-align: left; font-size: 10px" colspan="50"><b>Maquinarias, equipos y herramientas</b></td>
            <td colspan="50">{{$planillaDjir02->maquinarias_eqp_herra ? $planillaDjir02->maquinarias_eqp_herra:''}}</td>
        </tr>
        <tr>
            <td class="top" style="text-align: left; font-size: 10px" colspan="50"><b>Equipos de transporte</b></td>
            <td colspan="50">{{$planillaDjir02->eqp_transporte ? $planillaDjir02->eqp_transporte:''}}</td>
        </tr>
        <tr>
            <td class="top" style="text-align: left; font-size: 10px" colspan="50"><b>Otros activos fijos tangibles</b></td>
            <td colspan="50">{{$planillaDjir02->otros_activos_tangibles ? $planillaDjir02->otros_activos_tangibles:''}}</td>
        </tr>
        <tr>
            <td class="top" style="text-align: left; font-size: 10px" colspan="50"><b>Muebles, enseres y equipos de oficina</b></td>
            <td colspan="50">{{$planillaDjir02->muebles_enceres ? $planillaDjir02->muebles_enceres:''}}
            </tr>
        </table>
    </div>
<br>
    <!--Bienes intangibles-->
    <div class="centrar espacio-superior">
    <table class="ancho-full" border="1">
        <tr>
            <td class="top" style="background-color:#818286; color:#fff; text-align: left; font-size: 13px" colspan="100"><b>Bienes inmateriales o intangible (Software, patentes, derechos de marca u otros activos intangibles).</b></td>
        </tr>
         <tr>
            <td class="top" style="text-align: left; font-size: 10px" colspan="50"><b>Tipo de Moneda</b></td>
            <td colspan="50">{{$planillaDjir02->bienesIntangibles ? $planillaDjir02->bienesIntangibles->ddivisa_abr : ''}}</td>
        </tr>
        <tr>
            <td class="top" style="text-align: left; font-size: 10px" colspan="50"><b>Software</b></td>
            <td colspan="50">{{$planillaDjir02->software ? $planillaDjir02->software:''}}</td>
        </tr>
        <tr>
            <td class="top" style="text-align: left; font-size: 10px" colspan="50"><b>Derechos de propiedad intelectual</b></td>
            <td colspan="50">{{$planillaDjir02->derecho_prop_intelectual ? $planillaDjir02->derecho_prop_intelectual:''}}</td>
        </tr>
        <tr>
            <td class="top" style="text-align: left; font-size: 10px" colspan="50"><b>Contribuciones tecnológicas intangibles</b></td>
            <td colspan="50">{{$planillaDjir02->contribuciones_tecno ? $planillaDjir02->contribuciones_tecno:''}}</td>
        </tr>
        <tr>
            <td class="top" style="text-align: left; font-size: 10px" colspan="50"><b>Otros activos fijos intangibles</b></td>
            <td colspan="50">{{$planillaDjir02->otros_activos_intangibles ? $planillaDjir02->otros_activos_intangibles:''}}</td>
        </tr>
        <tr>
            <td class="top" style="text-align: right; font-size: 10px" colspan="50"><b>TOTAL</b></td>
            <td colspan="50">{{$planillaDjir02->total_costos_declaracion ? $planillaDjir02->total_costos_declaracion:''}}</td>
        </tr>
        </table>
    </div>
<!--Tipo de Inversion-->
<div class="centrar espacio-superior">
    <table class="ancho-full" border="1">
        <tr>
            <td class="top" style="background-color:#818286; color:#fff; text-align: left; font-size: 13px" colspan="100"><b>Tipo de Inversión</b></td>
        </tr>
        @if(isset($planillaDjir02->tipo_inversion) && $planillaDjir02->tipo_inversion == 1)
        <tr>
            <td class="top" style="text-align: left; font-size: 10px" colspan="100"><b>Inversión Extranjera Directa</b> se entiende la inversión productiva efectuada a través de los aportes realizados por los inversionistas extranjeros conformados por recursos tangibles o financieros, destinados a formar parte del patrimonio de los sujetos receptores de inversión extranjera en el territorio nacional, con la finalidad de generar valor agregado al proceso productivo en el que se inserta. Estos aportes deben representar una participación igual o superior al 10% del capital societario)</td>
        </tr>
        @else
        <tr>
            <td class="top" style="text-align: left; font-size: 10px" colspan="100"><b>Inversión de Cartera </b> (Se refiere a la adquisición de acciones o participaciones societarias en todo tipo de empresas que representen un nivel de participación en el patrimonio societario inferior al diez por ciento (10%)</td>
        </tr>
        @endif
    </table>
</div>
<!--Resumen de estimacion-->
<div class="centrar espacio-superior">
    <table class="ancho-full" border="1">
        <tr>
            <td class="top" style="background-color:#818286; color:#fff; text-align: left; font-size: 13px" colspan="100"><b>RESUMEN - Estimación de la Inversión Realizada (DJIR -02)</b></td>
        </tr>
        <tr>
            <td class="top" style="text-align: left; font-size: 10px" colspan="50"><b>Modalidad de la Inversión</b></td>
            <td class="top" style="text-align: left; font-size: 10px" colspan="50"><b>Monto</b></td>
        </tr>
        <tr>
            <td class="top" style="text-align: left; font-size: 10px" colspan="50"><b>Inversión Financiera en divisas y/o cualquier otro medio de cambio</b></td>
            <td colspan="50">{{$planillaDjir02->invert_divisa_cambio ? $planillaDjir02->invert_divisa_cambio:''}}</td>
        </tr>
        <tr>
            <td class="top" style="text-align: left; font-size: 10px" colspan="50"><b>Bienes de capital fisico o tangibles (Maquinarias, equipos, herramientas o cualquier otro tipo de activo tangible).</b></td>
            <td colspan="50">{{$planillaDjir02->bienes_cap_fisico_tangibles ? $planillaDjir02->bienes_cap_fisico_tangibles:''}}</td>
        </tr>
        <tr>
            <td class="top" style="text-align: left; font-size: 10px" colspan="50"><b>Bienes inmateriales o intangible (Software, patentes, derechos de marca u otros activos intangibles.</b></td>
            <td colspan="50">{{$planillaDjir02->bienes_inmateriales_intangibles ? $planillaDjir02->bienes_inmateriales_intangibles:''}}</td>
        </tr>
        <tr>
            <td class="top" style="text-align: left; font-size: 10px" colspan="50"><b>Reinversiones de utilidades ( SOLO PARA PROCESO DE ACTUALIZACIÓN)</b></td>
            <td colspan="50">{{$planillaDjir02->reinversiones_utilidades ? $planillaDjir02->reinversiones_utilidades:''}}</td>
        </tr>
        <tr>
            <td class="top" style="text-align: left; font-size: 10px" colspan="50"><b>Otra (especifique)</b></td>
            <td colspan="50">{{$planillaDjir02->especifique_otro ? $planillaDjir02->especifique_otro:''}}</td>
        </tr>
        <tr>
            <td class="top" style="text-align: right; font-size: 10px" colspan="50"><b>TOTAL</b></td>
            <td colspan="50">{{$planillaDjir02->total_modalidad_inv ? $planillaDjir02->total_modalidad_inv:''}}</td>
        </tr>
    </table>
</div>

<!--valor de Patrimonio-->
<div class="centrar espacio-superior">
    <table class="ancho-full" border="1">
        <tr>
            <td class="top" style="background-color:#818286; color:#fff; text-align: left; font-size: 13px" colspan="100"><b>Cuál fue la base de estimación?</b></td>
        </tr>
        <tr>
            <td colspan="100">{{$planillaDjir02->base_estimacion ? $planillaDjir02->base_estimacion:''}}</td>
        </tr>
    @if(isset($planillaDjir02->otro_especifique))
        <tr>
            <td class="top" style="text-align: left; font-size: 10px" colspan="50"><b>Otros (especificar)</b></td>
            <td colspan="50">{{$planillaDjir02->otro_especifique ? $planillaDjir02->otro_especifique:''}}</td>
        </tr>
    @endif
    </table>
</div>

<br><br>
	 <div class="centrar espacio-superior">
        <table class="ancho-full">
            <tr>
                <td class="top" style="text-align: center; font-size: 13px; text-transform: uppercase" colspan="100">
                Representante legal n° de rif: <b>{{ $solicitudDeclaracion->rPlanillaDjir01 ? $solicitudDeclaracion->rPlanillaDjir01->numero_doc_identidad:''}}</b><br>
                Firma del contribuyente o Representante Legal <br><br><br>



                _______________________________________________________
            </td>
            </tr>
        </table>
    </div>

</body>
</html>

