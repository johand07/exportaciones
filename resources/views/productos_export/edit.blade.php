@extends('templates/layoutlte')
@section('content')
@component('modal_dialogo',['arancel'=>$arancel])
@slot('header') <span>Código Arancelario</span> @endslot
@slot('body')  <div id="arancel"></div>
@endslot
@slot('footer')@endslot
@endcomponent
@php $contador=count($productos); @endphp
{{Form::hidden('metodo','Edit')}}
{{Form::hidden('contador',$contador)}}

<div class="content" >
  <div class="row" id="codigo_arancelarios" >
      <div class="col-md-3"></div>
       <div class="col-md-6 text-center">

       </div>
     <div class="col-md-3"></div>
   </div>
   
<div class="" id="steep_1">
  <div class="panel-heading">LISTA DE PRODUCTOS REGISTRADOS</div>
    <div class="panel-body">
      <table class="table table-bordered text-center table-responsive " id="prod">
        <thead >
          <tr><td colspan="9"><div class="text-right"><button name="add2" type="button" id="add-prod" value="add more" class="btn btn-success agregar"><i class="glyphicon glyphicon-plus"></i> Agregar Nuevo Producto</button></div></td></tr>

          <tr>
            <th>Código Arancelario </th>
            <th>Descripción Arancelaria</th>
            <th>Descripción Comercial</th>
            <th>Unidad de Medida</th>
            <th>Capacidad Operativa Anual</th>
            <th>Capacidad Instalada Anual</th>
            <th>Capacidad de Almacenamiento Anual</th>
            <th>Capacidad de Exportación Anual</th>
            <th></th>

          </tr>
        </thead>
        <tbody >
<tr><td>hola</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
          @foreach($productos as $key=>$valor)
            <tr id="row{{$key+1}}" >

              <td>{!!Form::text('codigo[]',$valor->codigo,['class'=>'form-control','id'=>'codigo','readonly'=>'true'])!!}
              </td>
              <td>{!!Form::text('descripcion[]',$valor->descripcion,['class'=>'form-control','id'=>'descripcion','onkeypress'=>'return soloLetras(event)','readonly'=>'true'])!!}</td>

              <td>{!!Form::text('descrip_comercial[]',$valor->descrip_comercial,['class'=>'form-control','id'=>'descrip_comercial','onkeypress'=>'return soloLetras(event)','readonly'=>'true'])!!}</td>

              <td>{!!Form::select('gen_unidad_medida_id[]',$unidad_medida,$valor->gen_unidad_medida_id,['placeholder'=>'--Seleccione una Unidad de Medida--', 'class'=>'form-control','id'=>'gen_unidad_medida_id','readonly'=>'true']) !!}</td>

              <td>{!!Form::text('cap_opera_anual[]',$valor->cap_opera_anual,['class'=>'form-control','id'=>'cap_opera_anual','onkeypress'=>'return soloNumeros(event)','readonly'=>'true'])!!}</td>
              
              <td>{!!Form::text('cap_insta_anual[]',$valor->cap_insta_anual,['class'=>'form-control','id'=>'cap_insta_anual','onkeypress'=>'return soloNumerosletrasyPorcentaje(event)','readonly'=>'true'])!!}</td>
              <td>{!!Form::text('cap_almacenamiento[]',$valor->cap_alamcenamiento,['class'=>'form-control','id'=>'cap_alamcenamiento','onkeypress'=>'return soloNumeros(event)','readonly'=>'true'])!!}</td>

              <td>{!!Form::text('cap_exportacion[]',$valor->cap_exportacion,['class'=>'form-control','id'=>'cap_exportacion','onkeypress'=>'return soloNumeros(event)','readonly'=>'true'])!!}</td>
              <td>
                {!!Form::hidden('id[]',$valor->id)!!}
        <!--Validacion para el boton eliminar-->
            @if(!isset($valor->rDeclaracionProduct) && !isset($valor->rDeclaracionProduct->productos_id) )
                <button  name="remove" type="button" id="{{$key+1}}" value="" class="btn btn-danger btn-remove">x</button>
            @endif
                <a href="#" onclick="editProduct({{$valor->id}})" class="glyphicon glyphicon-edit btn btn-success" title="Editar" aria-hidden="true"></a> 
              </td>
            </tr>
          @endforeach  
        </tbody>
  <!--Validacion para el boton eliminar--> 

      </table>
     
{{Form::model($productos,['route' =>['productos_export.update',$id] ,'method'=>'PATCH','id'=>'formProductos'])}}
      <table class="table table-bordered text-center table-responsive " id="prod1">
        <thead >
         

          <tr>
           
            <th></th>

          </tr>
        </thead>
        <tbody >
          
        </tbody>
       

      </table>
    
  </div>
  <div class="panel-footer text-center">

      <a href="{{url('exportador/DatosEmpresa')}}" class="btn btn-primary">Atrás</a>
     <!--a class="btn btn-success" href="#" onclick="validarProductos()">Guardar</a--> 
     <input type="submit" class="btn btn-success guardar" value="Guardar" style="display:none">

  </div>



{{Form::close()}}

</div>

<div class="panel panel-primary" id="steep_2" style="display:none;">
  <div class="panel-heading">LISTA DE PRODUCTOS REGISTRADOS</div>
    <div class="panel-body">
    {{Form::open(['route' =>'productos_export_edit.actualizar' ,'method'=>'POST','id'=>'formProductosEdit'])}}
          <table class="table table-bordered text-center" id="prod">
            <thead >
              <tr>
                <th>Código Arancelario </th>
                <th>Descripción Arancelaria</th>
                <th>Descripción Comercial</th>
                <th>Unidad de Medida</th>
                <th>Capacidad Operativa Anual</th>
                <th>Capacidad Instalada Anual</th>
                <th>Capacidad de Almacenamiento Anual</th>
                <th>Capacidad de Exportación Anual</th>
                <th></th>

              </tr>
            </thead >
            <tbody>
              <tr id="fila">

                <td>{!!Form::text('codigo_arancel',null,['class'=>'form-control','data-toggle'=>"modal",'data-target'=>"#modal",'id'=>'cod_arancel1','readonly'=>'true'])!!}
                </td>
                <td>{!!Form::text('descrip_arancel',null,['class'=>'form-control','id'=>'descrip_arancel1','onkeypress'=>'return soloLetras(event)'])!!}</td>
                <td>{!!Form::text('descrip_comercial',null,['class'=>'form-control','id'=>'descrip_comercial1','onkeypress'=>'return soloLetras(event)'])!!}</td>
                <td>{!!Form::select('gen_unidad_medida_id',$unidad_medida,null,['placeholder'=>'--Seleccione una Unidad de Medida--', 'class'=>'form-control','id'=>'gen_unidad_medida_id1']) !!}</td>
                <td>{!!Form::text('cap_opera_anual',null,['class'=>'form-control','id'=>'cap_opera_anual1','onkeypress'=>'return soloNumeros(event)'])!!}</td>
                <td>{!!Form::text('cap_insta_anual',null,['class'=>'form-control','id'=>'cap_insta_anual1','onkeypress'=>'return soloNumerosletrasyPorcentaje(event)'])!!}</td>
                <td>{!!Form::text('cap_alamcenamiento',null,['class'=>'form-control','id'=>'cap_alamcenamiento1','onkeypress'=>'return soloNumeros(event)'])!!}</td>

                <td>{!!Form::text('cap_exportacion',null,['class'=>'form-control','id'=>'cap_exportacion1','onkeypress'=>'return soloNumeros(event)'])!!}</td>

                <input type="hidden" name="id" id="id1">
                <input type="hidden" id="valid" value="">
              </tr>
            </tbody>
          </table>

          <a href="#" onclick="regresar()" class="btn btn-primary">Atrás</a>
          <input class="btn btn-success" type="submit" value='Actualizar'>
          {{Form::close()}}
    </div>

  </div>
</div>



<script src="{{asset('js/ajax_v2.1.1.js')}}"></script>

<script>


$(document).ready(function (){


  var x = $('input[name="contador"]').val();
  var i=parseInt(x);


  $('#add-prod').click(function(){
  i++;


$('#prod1 tr:last').after('<tr id="row'+i+'"><td>{!!Form::text('codigo[]',null,['class'=>'form-control','readOnly'=>'readOnly','data-toggle'=>"modal",'data-target'=>"#modal",'id'=>'cod_arancel'])!!}</td><td>{!!Form::text('descripcion[]',null,['class'=>'form-control','onkeypress'=>'return soloNumerosDouble(event)'])!!}</td><td>{!!Form::text('descrip_comercial[]',null,['class'=>'form-control','id'=>'descrip_comercial','onkeypress'=>'return soloLetras(event)'])!!}</td><td>{!!Form::select('gen_unidad_medida_id[]',$unidad_medida,null,['placeholder'=>'--Seleccione una Unidad de Medida--', 'class'=>'form-control','id'=>'gen_unidad_medida_id'])!!}</td><td>{!!Form::text('cap_opera_anual[]',null,['class'=>'form-control','id'=>'cap_opera_anual','onkeypress'=>'return soloNumeros(event)'])!!}</td><td>{!!Form::text('cap_insta_anual[]',null,['class'=>'form-control','id'=>'cap_insta_anual','onkeypress'=>'return soloNumerosletrasyPorcentaje(event)'])!!}</td><td>{!!Form::text('cap_almacenamiento[]',null,['class'=>'form-control','id'=>'cap_alamcenamiento','onkeypress'=>'return soloNumeros(event)'])!!}</td><td>{!!Form::text('cap_exportacion[]',null,['class'=>'form-control','id'=>'cap_exportacion','onkeypress'=>'return soloNumeros(event)'])!!}</td><td>{!!Form::hidden('id[]',0)!!}<button  name="remove" type="button" id='+i+' value="" class="btn btn-danger btn-remove">x</button></td></tr>');

});



   $(document).on('click','.btn-remove',function(){

            var id_boton= $(this).attr("id");
            var id=$(this).prev().val();

            if(id!=0){

              swal({
              /*Cargo el alert para confirmar o declinar*/
                  title: "¿Eliminar?",
                  text: "¿Está seguro de eliminar este Registro?",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonColor: "#DD6B55",
                  confirmButtonText: "Si!, Borrarlo!",
                  cancelButtonText: "No, Cancelar!",
                  closeOnConfirm: false,
                  closeOnCancel: false,
                  showLoaderOnConfirm: true
                  },

 function(isConfirm){
      if (isConfirm) {

           $.ajax({

            'type':'get',
            'url':'eliminar',
            'data':{ 'id':id},
            success: function(data){

             $('#row'+id_boton).remove();
             if(data==1){

       swal("Eliminación exitosa","Producto Eliminado ", "success");

             }

          }

            });


        }else{

    swal("Cancelado", "No se ha procesado la eliminación", "warning");

        }

      });


    }else{

           $('#row'+id_boton).remove();
          }

});

  $(".agregar").click(function(){
    $(".guardar").show();
  });

  });

  function editProduct(id){
    
    $.ajax({

      'type':'GET',
      'url':"editar",
      'data':{ 'id':id},
      success: function(data){

        $('#cod_arancel1').val(data.codigo);
        $('#descrip_arancel1').val(data.descripcion);
        $('#descrip_comercial1').val(data.descrip_comercial);
        $('#gen_unidad_medida_id1').val(data.gen_unidad_medida_id);
        $('#cap_opera_anual1').val(data.cap_opera_anual);
        $('#cap_insta_anual1').val(data.cap_insta_anual);
        $('#cap_alamcenamiento1').val(data.cap_alamcenamiento);
        $('#cap_exportacion1').val(data.cap_exportacion);
        $('#id1').val(data.id);
        
        $('#steep_1').hide();
        $('#steep_2').show();
        $('#valid').val(1);

        
      }
    });
  }

  function regresar()
  {
    $('#formProductosEdit')[0].reset();

        $('#steep_2').hide();
        $('#steep_1').show();
        $('#valid').val(0);
  }

</script>





@endsection
