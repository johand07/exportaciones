->nullable()<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GenArancelNandina extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('gen_arancel_nandina', function (Blueprint $table) {
           $table->engine = 'InnoDB';
           $table->increments('id');
           $table->string('codigo', 100)->nullable();
           $table->string('descripcion', 100)->nullable();
           $table->string('capitulo', 100)->nullable();
           $table->boolean('bactivo')->default(1);
           $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gen_arancel_nandina');
    }
}
