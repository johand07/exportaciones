<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GenDeclaracionInversion extends Model
{
    protected $table = 'gen_declaracion_inversion';

    protected $filleable = [
        'gen_usuario_id',
        'cat_tipo_usuario_id',
        'gen_status_id',
        'num_declaracion',
        'num_declaracion_actualizada',
        'actualizado',
        'fstatus', 
    ];

    public function genUsuario()
    {
        return $this->hasOne('App\Models\GenUsuario','id','gen_usuario_id');
    }

    public function catTipoUsuario()
	{
        return $this->hasOne('App\Models\CatTipoUsuario','id','cat_tipo_usuario_id');
    }

    public function genStatus()
	{
        return $this->hasOne('App\Models\GenStatus','id','gen_status_id');
    }

    public function rPlanillaDjir01()
    {
        return $this->belongsTo('App\Models\PlanillaDjir_01', 'id', 'gen_declaracion_inversion_id');
    }

    public function rPlanillaDjir02()
    {
        return $this->belongsTo('App\Models\PlanillaDjir_02', 'id', 'gen_declaracion_inversion_id');
    }

    public function rPlanillaDjir03()
    {
        return $this->belongsTo('App\Models\PlanillaDjir_03', 'id', 'gen_declaracion_inversion_id');
    }

    public function rBandejaAlistaDec()
    {
        return $this->belongsTo('App\Models\BandejAnalistaDecInversion', 'id', 'gen_declaracion_inversion_id');
    }

    public function rBandejaCoordDec()
    {
        return $this->belongsTo('App\Models\BandejaCoorDecInversion', 'id', 'gen_declaracion_inversion_id');
    }
   
}
