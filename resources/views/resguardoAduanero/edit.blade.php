@extends('templates/layoutlte')

@section('content')

@php
function namefile($name){
  $explode=explode("/",$name);
  return $explode[3];

}

@endphp


{{ Form::model($resguardo, array('route' => array('resguardoAduanero.update', $resguardo->id), 'method' => 'PUT','enctype' => 'multipart/form-data')) }} 

@if(isset($resguardo->observacion_resguardo) && $resguardo->edo_observacion_resguardo == 1)
	<div class="alert alert-dismissible alert-danger">
	  <button type="button" class="close" data-dismiss="alert">&times;</button>
	  <h4 class="alert-heading">Observación!</h4>
	  <p class="mb-0">{{$resguardo->observacion_resguardo}}</p>
	</div>
@endif
@if(isset($resguardo->observacion_cna) && $resguardo->edo_observacion_cna == 1)
	<div class="alert alert-dismissible alert-danger">
	  <button type="button" class="close" data-dismiss="alert">&times;</button>
	  <h4 class="alert-heading">Observación!</h4>
	  <p class="mb-0">{{$resguardo->observacion_cna}}</p>
	</div>
@endif
<div class="content">
	<div class="panel panel-primary">
		<div class="panel-primary" style=" #fff">
			<div class="panel-heading"><h4>REQUISITOS DOCUMENTALES EXIGIDOS POR LA UNIDAD ESPECIAL ANTIDROGAS (UEA-45) LA GUAIRA PARA LAS EMPRESAS EXPORTADORAS</h4></div>
			<div class="panel-body">
				<div class="row">
					<div class="col-md-12">
						
						<div class="panel-body">
							<div class="form-group">
								{!! Form::label('Ubicación aduana','Ubicación') !!}

								{!! Form::select('gen_aduana_salida_id',$AduanaSalida,null,['class'=>'form-control','id'=>'gen_aduana_salida_id',
								'placeholder'=>'Seleccione aduana','onchange'=>'valorSelect()', 'required'=>'true']) !!}
							</div>
						</div>
					
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-4">
							<tr>
								<td class="text-center" valign="top">
									<span><b>1.) Carta de inscripción ante la Aduana Marítima de la Guaira (SENIAT) - Solo para Agentes Aduanales.</b></span><br><br>
									<span class="btn btn-default btn-file" style="margin: 3px;">
	                       				 Cargar Archivo <span class="glyphicon glyphicon-file"></span>
				                        {!! Form::file('file_1', array('class' => 'file-input','style'=>'margin:10px;','id'=>'file_1','title'=>'formatos permitidos .doc, .docx, .pdf')) !!}
			                      	</span>
								</td>
								<!--Para pintar la vista previa el sobre azulito al cargar un archivo con imgdocumento_ y nombre del archivo cargado con nombre_-->
                 				<td class="text-center" height="40px"><img id="" src="" alt="" width="300px" style="margin: 3px;"><img id="imgdocumento_file_1" src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px; display: none;"><div id="nombre_file_1"></div>
                 					 @if(isset($documentos->file_1))
					                      <div id="imgdocumentoEdit_file_1">
					                      <img src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px;">
					                      {{namefile($documentos->file_1)}}
					                      </div>
                    				 @endif


                 				</td>
							</tr>
							<input type="hidden" name="file_id" value="{{$documentos->id}}">
						</div>
						<div class="col-md-4">
							<tr>
								<td class="text-center" valign="top">
									<span><b>2.) Registro Mercantil: Primera y última asamblea y/o asambleas extraordinarias donde se haya modificado el objeto de la empresa, su domicilio, accionistas o representantes legales.</b></span><br><br>
									<span class="btn btn-default btn-file" style="margin: 3px;">
	                       				 Cargar Archivo <span class="glyphicon glyphicon-file"></span>
				                        {!! Form::file('file_2', array('class' => 'file-input','style'=>'margin:10px;','id'=>'file_2','title'=>'formatos permitidos .doc, .docx, .pdf')) !!}
			                      	</span>
								</td>
								<!--Para pintar la vista previa el sobre azulito al cargar un archivo con imgdocumento_ y nombre del archivo cargado con nombre_-->
                 				<td class="text-center" height="40px"><img id="" src="" alt="" width="300px" style="margin: 3px;"><img id="imgdocumento_file_2" src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px; display: none;"><div id="nombre_file_2"></div>
                 					 @if(isset($documentos->file_2))
					                      <div id="imgdocumentoEdit_file_2">
					                      <img src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px;">
					                      {{namefile($documentos->file_2)}}
					                      </div>
                    				 @endif

                 				</td>
							</tr>
						</div>
						<div class="col-md-4">
							<tr>
								<td class="text-center" valign="top">
									<span><b>3.) Registro de información Fiscal (Rif) de la empresa y del representante legal.</b></span><br><br>
									<span class="btn btn-default btn-file" style="margin: 3px;">
	                       				 Cargar Archivo <span class="glyphicon glyphicon-file"></span>
				                        {!! Form::file('file_3', array('class' => 'file-input','style'=>'margin:10px;','id'=>'file_3','title'=>'formatos permitidos .doc, .docx, .pdf')) !!}
			                      	</span>
								</td>
								<!--Para pintar la vista previa el sobre azulito al cargar un archivo con imgdocumento_ y nombre del archivo cargado con nombre_-->
                 				<td class="text-center" height="40px"><img id="vista_previa_file_3" src="" alt="" width="300px" style="margin: 3px;"><img id="imgdocumento_file_3" src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px; display: none;"><div id="nombre_file_3"></div>
                 					 @if(isset($documentos->file_3))
					                      <div id="imgdocumentoEdit_file_3">
					                      <img src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px;">
					                      {{namefile($documentos->file_3)}}
					                      </div>
                    				 @endif

                 				</td>
							</tr>
						</div>
					</div>
				</div><br><br><br><!-- 1er row-->
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-4">
							<tr>
								<td class="text-center" valign="top">
									<span><b>4.) Cedula de identidad del representante legal de la empresa.</b></span><br><br>
									<span class="btn btn-default btn-file" style="margin: 3px;">
	                       				 Cargar Archivo <span class="glyphicon glyphicon-file"></span>
				                        {!! Form::file('file_4', array('class' => 'file-input','style'=>'margin:10px;','id'=>'file_4','title'=>'formatos permitidos .doc, .docx, .pdf')) !!}
			                      	</span>
								</td>
								<!--Para pintar la vista previa el sobre azulito al cargar un archivo con imgdocumento_ y nombre del archivo cargado con nombre_-->
                 				<td class="text-center" height="40px"><img id="" src="" alt="" width="300px" style="margin: 3px;"><img id="imgdocumento_file_4" src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px; display: none;"><div id="nombre_file_4"></div>
                 					 @if(isset($documentos->file_4))
					                      <div id="imgdocumentoEdit_file_4">
					                      <img src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px;">
					                      {{namefile($documentos->file_4)}}
					                      </div>
                    				 @endif

                 				</td>
							</tr>
						</div>
						<div class="col-md-4">
							<tr>
								<td class="text-center" valign="top">
									<span><b>5.) Fotografía tamaño postal del representante legal de la empresa (Debe estar impresa en papel fotográfico).</b></span><br><br>
									<span class="btn btn-default btn-file" style="margin: 3px;">
	                       				 Cargar Archivo <span class="glyphicon glyphicon-picture"></span>
				                        {!! Form::file('file_5', array('class' => 'file-input-foto','style'=>'margin:10px;','id'=>'file_5','title'=>'formatos permitidos .jpg, .jpeg, .png')) !!}
			                      	</span>
								</td>
								<!--Para pintar la vista previa el sobre azulito al cargar un archivo con imgdocumento_ y nombre del archivo cargado con nombre_-->
                 				<td class="text-center" height="40px"><img id="" src="" alt="" width="300px" style="margin: 3px;"><img id="imgdocumento_file_5" src="{{ asset('img/picture.png') }}" alt="" width="100px" style="margin: 3px; display: none;"><div id="nombre_file_5"></div>
                 					 @if(isset($documentos->file_5))
					                      <div id="imgdocumentoEdit_file_5">
					                      <img src="{{ asset('img/picture.png') }}" alt="" width="100px" style="margin: 3px;">
					                      {{namefile($documentos->file_5)}}
					                      </div>
                    				 @endif

                 				</td>
							</tr>
						</div>
						<div class="col-md-4">
							<tr>
								<td class="text-center" valign="top">
									<span><b>6.) Constancia de residencia Original del representante legal de la empresa (CNE, Consejo Comunal O Junta de Condominio).</b></span><br><br>
									<span class="btn btn-default btn-file" style="margin: 3px;">
	                       				 Cargar Archivo <span class="glyphicon glyphicon-file"></span>
				                        {!! Form::file('file_6', array('class' => 'file-input','style'=>'margin:10px;','id'=>'file_6','title'=>'formatos permitidos .doc, .docx, .pdf')) !!}
			                      	</span>
								</td>
								<!--Para pintar la vista previa el sobre azulito al cargar un archivo con imgdocumento_ y nombre del archivo cargado con nombre_-->
                 				<td class="text-center" height="40px"><img id="" src="" alt="" width="300px" style="margin: 3px;"><img id="imgdocumento_file_6" src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px; display: none;"><div id="nombre_file_6"></div>
                 					 @if(isset($documentos->file_6))
					                      <div id="imgdocumentoEdit_file_6">
					                      <img src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px;">
					                      {{namefile($documentos->file_6)}}
					                      </div>
                    				 @endif

                 				</td>
							</tr>
							
						</div>
					</div>
				</div><br><br><br>
				<div class="row"><!--2do row-->
					<div class="col-md-12">
						<span><b>7.) Fotografías de la fachada de la empresa, desde un plano general hasta un plano especifico (Mínimo 06 fotografías y deben estar impresas en papel fotográfico).</b></span><br><br>
						<div class="col-md-4">
							<tr>
								<td class="text-center" valign="top">
									<span class="btn btn-default btn-file" style="margin: 3px;">
	                       				 Cargar Archivo <span class="glyphicon glyphicon-picture"></span>
				                        {!! Form::file('file_7', array('class' => 'file-input-foto','style'=>'margin:10px;','id'=>'file_7','title'=>'formatos permitidos .jpg, .jpeg, .png')) !!}
			                      	</span>
								</td>
								<!--Para pintar la vista previa el sobre azulito al cargar un archivo con imgdocumento_ y nombre del archivo cargado con nombre_-->
                 				<td class="text-center" height="40px"><img id="" src="" alt="" width="300px" style="margin: 3px;"><img id="imgdocumento_file_7" src="{{ asset('img/picture.png') }}" alt="" width="100px" style="margin: 3px; display: none;"><div id="nombre_file_7"></div>
                 					 @if(isset($documentos->file_7))
					                      <div id="imgdocumentoEdit_file_7">
					                      <img src="{{ asset('img/picture.png') }}" alt="" width="100px" style="margin: 3px;">
					                      {{namefile($documentos->file_7)}}
					                      </div>
                    				 @endif

                 				</td>
							</tr>
						</div>
						<div class="col-md-4">
							<tr>
								<td class="text-center" valign="top">
							
									<span class="btn btn-default btn-file" style="margin: 3px;">
	                       				 Cargar Archivo <span class="glyphicon glyphicon-picture"></span>
				                        {!! Form::file('file_8', array('class' => 'file-input-foto','style'=>'margin:10px;','id'=>'file_8','title'=>'formatos permitidos .jpg, .jpeg, .png')) !!}
			                      	</span>
								</td>
								<!--Para pintar la vista previa el sobre azulito al cargar un archivo con imgdocumento_ y nombre del archivo cargado con nombre_-->
                 				<td class="text-center" height="40px"><img id="" src="" alt="" width="300px" style="margin: 3px;"><img id="imgdocumento_file_8" src="{{ asset('img/picture.png') }}" alt="" width="100px" style="margin: 3px; display: none;"><div id="nombre_file_8"></div>
                 					 @if(isset($documentos->file_8))
					                      <div id="imgdocumentoEdit_file_8">
					                      <img src="{{ asset('img/picture.png') }}" alt="" width="100px" style="margin: 3px;">
					                      {{namefile($documentos->file_8)}}
					                      </div>
                    				 @endif

                 				</td>
							</tr>
						</div>
						<div class="col-md-4">
							<tr>
								<td class="text-center" valign="top">
							
									<span class="btn btn-default btn-file" style="margin: 3px;">
	                       				 Cargar Archivo <span class="glyphicon glyphicon-picture"></span>
				                        {!! Form::file('file_9', array('class' => 'file-input-foto','style'=>'margin:10px;','id'=>'file_9','title'=>'formatos permitidos .jpg, .jpeg, .png')) !!}
			                      	</span>
								</td>
								<!--Para pintar la vista previa el sobre azulito al cargar un archivo con imgdocumento_ y nombre del archivo cargado con nombre_-->
                 				<td class="text-center" height="40px"><img id="" src="" alt="" width="300px" style="margin: 3px;"><img id="imgdocumento_file_9" src="{{ asset('img/picture.png') }}" alt="" width="100px" style="margin: 3px; display: none;"><div id="nombre_file_9"></div>
                 					 @if(isset($documentos->file_9))
					                      <div id="imgdocumentoEdit_file_9">
					                      <img src="{{ asset('img/picture.png') }}" alt="" width="100px" style="margin: 3px;">
					                      {{namefile($documentos->file_9)}}
					                      </div>
                    				 @endif

                 				</td>
							</tr>
						</div>
					</div>
				</div><br>
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-4">
							<tr>
								<td class="text-center" valign="top">
							
									<span class="btn btn-default btn-file" style="margin: 3px;">
	                       				 Cargar Archivo <span class="glyphicon glyphicon-picture"></span>
				                        {!! Form::file('file_10', array('class' => 'file-input-foto','style'=>'margin:10px;','id'=>'file_10','title'=>'formatos permitidos .jpg, .jpeg, .png')) !!}
			                      	</span>
								</td>
								<!--Para pintar la vista previa el sobre azulito al cargar un archivo con imgdocumento_ y nombre del archivo cargado con nombre_-->
                 				<td class="text-center" height="40px"><img id="" src="" alt="" width="300px" style="margin: 3px;"><img id="imgdocumento_file_10" src="{{ asset('img/picture.png') }}" alt="" width="100px" style="margin: 3px; display: none;"><div id="nombre_file_10"></div>
                 					 @if(isset($documentos->file_10))
					                      <div id="imgdocumentoEdit_file_10">
					                      <img src="{{ asset('img/picture.png') }}" alt="" width="100px" style="margin: 3px;">
					                      {{namefile($documentos->file_10)}}
					                      </div>
                    				 @endif

                 				</td>
							</tr>
						</div>
						<div class="col-md-4">
							<tr>
								<td class="text-center" valign="top">
							
									<span class="btn btn-default btn-file" style="margin: 3px;">
	                       				 Cargar Archivo <span class="glyphicon glyphicon-picture"></span>
				                        {!! Form::file('file_11', array('class' => 'file-input-foto','style'=>'margin:10px;','id'=>'file_11','title'=>'formatos permitidos .jpg, .jpeg, .png')) !!}
			                      	</span>
								</td>
								<!--Para pintar la vista previa el sobre azulito al cargar un archivo con imgdocumento_ y nombre del archivo cargado con nombre_-->
                 				<td class="text-center" height="40px"><img id="" src="" alt="" width="300px" style="margin: 3px;"><img id="imgdocumento_file_11" src="{{ asset('img/picture.png') }}" alt="" width="100px" style="margin: 3px; display: none;"><div id="nombre_file_11"></div>
                 					 @if(isset($documentos->file_11))
					                      <div id="imgdocumentoEdit_file_11">
					                      <img src="{{ asset('img/picture.png') }}" alt="" width="100px" style="margin: 3px;">
					                      {{namefile($documentos->file_11)}}
					                      </div>
                    				 @endif

                 				</td>
							</tr>
						</div>
						<div class="col-md-4">
							<tr>
								<td class="text-center" valign="top">
							
									<span class="btn btn-default btn-file" style="margin: 3px;">
	                       				 Cargar Archivo <span class="glyphicon glyphicon-picture"></span>
				                        {!! Form::file('file_12', array('class' => 'file-input-foto','style'=>'margin:10px;','id'=>'file_12','title'=>'formatos permitidos .jpg, .jpeg, .png')) !!}
			                      	</span>
								</td>
								<!--Para pintar la vista previa el sobre azulito al cargar un archivo con imgdocumento_ y nombre del archivo cargado con nombre_-->
                 				<td class="text-center" height="40px"><img id="" src="" alt="" width="300px" style="margin: 3px;"><img id="imgdocumento_file_12" src="{{ asset('img/picture.png') }}" alt="" width="100px" style="margin: 3px; display: none;"><div id="nombre_file_12"></div>
                 					 @if(isset($documentos->file_12))
					                      <div id="imgdocumentoEdit_file_12">
					                      <img src="{{ asset('img/picture.png') }}" alt="" width="100px" style="margin: 3px;">
					                      {{namefile($documentos->file_12)}}
					                      </div>
                    				 @endif

                 				</td>
							</tr>
						</div>
					</div>
				</div><br><br><br>

				<div class="row">
					<div class="col-md-12">
						<div class="col-md-4">
							<tr>
								<td class="text-center" valign="top">
									<span><b>8.) Original del recibo de un servicio público de la empresa (Agua, Electricidad, Teléfono, otros).</b></span><br><br>
									<span class="btn btn-default btn-file" style="margin: 3px;">
	                       				 Cargar Archivo <span class="glyphicon glyphicon-file"></span>
				                        {!! Form::file('file_13', array('class' => 'file-input','style'=>'margin:10px;','id'=>'file_13','title'=>'formatos permitidos .doc, .docx, .pdf')) !!}
			                      	</span>
								</td>
								<!--Para pintar la vista previa el sobre azulito al cargar un archivo con imgdocumento_ y nombre del archivo cargado con nombre_-->
                 				<td class="text-center" height="40px"><img id="" src="" alt="" width="300px" style="margin: 3px;"><img id="imgdocumento_file_13" src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px; display: none;"><div id="nombre_file_13"></div>
                 					 @if(isset($documentos->file_13))
					                      <div id="imgdocumentoEdit_file_13">
					                      <img src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px;">
					                      {{namefile($documentos->file_13)}}
					                      </div>
                    				 @endif

                 				</td>
							</tr>
						</div>
						<div class="col-md-4">
							<tr>
								<td class="text-center" valign="top">
									<span><b>9.) Contrato de arrendamiento del local o Documento de Propiedad.</b></span><br><br>
									<span class="btn btn-default btn-file" style="margin: 3px;">
	                       				 Cargar Archivo <span class="glyphicon glyphicon-file"></span>
				                        {!! Form::file('file_14', array('class' => 'file-input','style'=>'margin:10px;','id'=>'file_14','title'=>'formatos permitidos .doc, .docx, .pdf')) !!}
			                      	</span>
								</td>
								<!--Para pintar la vista previa el sobre azulito al cargar un archivo con imgdocumento_ y nombre del archivo cargado con nombre_-->
                 				<td class="text-center" height="40px"><img id="" src="" alt="" width="300px" style="margin: 3px;"><img id="imgdocumento_file_14" src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px; display: none;"><div id="nombre_file_14"></div>
                 					 @if(isset($documentos->file_14))
					                      <div id="imgdocumentoEdit_file_14">
					                      <img src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px;">
					                      {{namefile($documentos->file_14)}}
					                      </div>
                    				 @endif

                 				</td>
							</tr>
						</div>
						<div class="col-md-4">
							<tr>
								<td class="text-center" valign="top">
									<span><b>10.) Ultima declaración del ISLR.</b></span><br><br>
									<span class="btn btn-default btn-file" style="margin: 3px;">
	                       				 Cargar Archivo <span class="glyphicon glyphicon-file"></span>
				                        {!! Form::file('file_15', array('class' => 'file-input','style'=>'margin:10px;','id'=>'file_15','title'=>'formatos permitidos .doc, .docx, .pdf')) !!}
			                      	</span>
								</td>
								<!--Para pintar la vista previa el sobre azulito al cargar un archivo con imgdocumento_ y nombre del archivo cargado con nombre_-->
                 				<td class="text-center" height="40px"><img id="" src="" alt="" width="300px" style="margin: 3px;"><img id="imgdocumento_file_15" src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px; display: none;"><div id="nombre_file_15"></div>
                 					 @if(isset($documentos->file_15))
					                      <div id="imgdocumentoEdit_file_15">
					                      <img src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px;">
					                      {{namefile($documentos->file_15)}}
					                      </div>
                    				 @endif

                 				</td>
							</tr>
						</div>
					</div>
				</div><br><br><br><!--cierre row doc 10-->

				<div class="row">
					<div class="col-md-12">
						<div class="col-md-4">
							<tr>
								<td class="text-center" valign="top">
									<span><b>11.) Patente Municipal.</b></span><br><br>
									<span class="btn btn-default btn-file" style="margin: 3px;">
	                       				 Cargar Archivo <span class="glyphicon glyphicon-file"></span>
				                        {!! Form::file('file_16', array('class' => 'file-input','style'=>'margin:10px;','id'=>'file_16','title'=>'formatos permitidos .doc, .docx, .pdf')) !!}
			                      	</span>
								</td>
								<!--Para pintar la vista previa el sobre azulito al cargar un archivo con imgdocumento_ y nombre del archivo cargado con nombre_-->
                 				<td class="text-center" height="40px"><img id="" src="" alt="" width="300px" style="margin: 3px;"><img id="imgdocumento_file_16" src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px; display: none;"><div id="nombre_file_16"></div>
                 					 @if(isset($documentos->file_16))
					                      <div id="imgdocumentoEdit_file_16">
					                      <img src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px;">
					                      {{namefile($documentos->file_16)}}
					                      </div>
                    				 @endif

                 				</td>
							</tr>
						</div>
						<div class="col-md-4">
							<tr>
								<td class="text-center" valign="top">
									<span><b>12.) Permiso de Bomberos vigente.</b></span><br><br>
									<span class="btn btn-default btn-file" style="margin: 3px;">
	                       				 Cargar Archivo <span class="glyphicon glyphicon-file"></span>
				                        {!! Form::file('file_17', array('class' => 'file-input','style'=>'margin:10px;','id'=>'file_17','title'=>'formatos permitidos .doc, .docx, .pdf')) !!}
			                      	</span>
								</td>
								<!--Para pintar la vista previa el sobre azulito al cargar un archivo con imgdocumento_ y nombre del archivo cargado con nombre_-->
                 				<td class="text-center" height="40px"><img id="" src="" alt="" width="300px" style="margin: 3px;"><img id="imgdocumento_file_17" src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px; display: none;"><div id="nombre_file_17"></div>
                 					 @if(isset($documentos->file_17))
					                      <div id="imgdocumentoEdit_file_17">
					                      <img src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px;">
					                      {{namefile($documentos->file_17)}}
					                      </div>
                    				 @endif

                 				</td>
							</tr>
						</div>
						<div class="col-md-4">
							<tr>
								<td class="text-center" valign="top">
									<span><b>13.) Relación completa de la nómina de Trabajadores, indicando nombres, apellidos, cedula de identidad y cargo desempeñado.</b></span><br><br>
									<span class="btn btn-default btn-file" style="margin: 3px;">
	                       				 Cargar Archivo <span class="glyphicon glyphicon-file"></span>
				                        {!! Form::file('file_18', array('class' => 'file-input','style'=>'margin:10px;','id'=>'file_18','title'=>'formatos permitidos .doc, .docx, .pdf')) !!}
			                      	</span>
								</td>
								<!--Para pintar la vista previa el sobre azulito al cargar un archivo con imgdocumento_ y nombre del archivo cargado con nombre_-->
                 				<td class="text-center" height="40px"><img id="" src="" alt="" width="300px" style="margin: 3px;"><img id="imgdocumento_file_18" src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px; display: none;"><div id="nombre_file_18"></div>
                 					 @if(isset($documentos->file_18))
					                      <div id="imgdocumentoEdit_file_18">
					                      <img src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px;">
					                      {{namefile($documentos->file_18)}}
					                      </div>
                    				 @endif

                 				</td>
							</tr>
						</div>
					</div>
				</div><br><br>
				<div class="row"><!--2do row-->
					<div class="col-md-12">
						<span><b>14.) Tres (03) referencias personales del representante legal (Debe incluir, copia de la cedula de identidad, número telefónico, dirección domiciliaria y estampa de las huellas dactilares (ambos pulgares) de quien refiere). Deben ser originales.</b></span><br><br>
						<div class="col-md-4">
							<tr>
								<td class="text-center" valign="top">
									<span>1. Referencia Personal </span><br>
									<span class="btn btn-default btn-file" style="margin: 3px;">
	                       				 Cargar Archivo <span class="glyphicon glyphicon-file"></span>
				                        {!! Form::file('file_19', array('class' => 'file-input','style'=>'margin:10px;','id'=>'file_19','title'=>'')) !!}
			                      	</span>
								</td>
								<!--Para pintar la vista previa el sobre azulito al cargar un archivo con imgdocumento_ y nombre del archivo cargado con nombre_-->
                 				<td class="text-center" height="40px"><img id="" src="" alt="" width="300px" style="margin: 3px;"><img id="imgdocumento_file_19" src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px; display: none;"><div id="nombre_file_19"></div>
                 					 @if(isset($documentos->file_19))
					                      <div id="imgdocumentoEdit_file_19">
					                      <img src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px;">
					                      {{namefile($documentos->file_19)}}
					                      </div>
                    				 @endif

                 				</td>
							</tr>
						</div>
						<div class="col-md-4">
							<tr>
								<td class="text-center" valign="top">
									<span>2. Referencia Personal</span><br>
									<span class="btn btn-default btn-file" style="margin: 3px;">
	                       				 Cargar Archivo <span class="glyphicon glyphicon-file"></span>
				                        {!! Form::file('file_20', array('class' => 'file-input','style'=>'margin:10px;','id'=>'file_20','title'=>'')) !!}
			                      	</span>
								</td>
								<!--Para pintar la vista previa el sobre azulito al cargar un archivo con imgdocumento_ y nombre del archivo cargado con nombre_-->
                 				<td class="text-center" height="40px"><img id="" src="" alt="" width="300px" style="margin: 3px;"><img id="imgdocumento_file_20" src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px; display: none;"><div id="nombre_file_20"></div>
                 					 @if(isset($documentos->file_20))
					                      <div id="imgdocumentoEdit_file_20">
					                      <img src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px;">
					                      {{namefile($documentos->file_20)}}
					                      </div>
                    				 @endif

                 				</td>
							</tr>
						</div>
						<div class="col-md-4">
							<tr>
								<td class="text-center" valign="top">
									<span>3. Referencia Personal</span><br>
									<span class="btn btn-default btn-file" style="margin: 3px;">
	                       				 Cargar Archivo <span class="glyphicon glyphicon-file"></span>
				                        {!! Form::file('file_21', array('class' => 'file-input','style'=>'margin:10px;','id'=>'file_21','title'=>'')) !!}
			                      	</span>
								</td>
								<!--Para pintar la vista previa el sobre azulito al cargar un archivo con imgdocumento_ y nombre del archivo cargado con nombre_-->
                 				<td class="text-center" height="40px"><img id="" src="" alt="" width="300px" style="margin: 3px;"><img id="imgdocumento_file_21" src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px; display: none;"><div id="nombre_file_21"></div>
                 					 @if(isset($documentos->file_21))
					                      <div id="imgdocumentoEdit_file_21">
					                      <img src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px;">
					                      {{namefile($documentos->file_21)}}
					                      </div>
                    				 @endif

                 				</td>
							</tr>
						</div>
					</div>
				</div><br><br><br><br>
				<div class="row"><!--2do row-->
					<div class="col-md-12">
						<span><b>15.) Tres (03) Números de teléfonos alternos o de localización del representante legal, local y celular.</b></span><br><br>
						<div class="col-md-4">
							<div class="form-group">
							<span>1. Teléfono </span><br>
							
							{!! Form::text('tlf_1',null,['class'=>'form-control','id'=>'tlf_1','onkeypress'=>'return soloNumeros(event)']) !!}
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
							<span>2. Teléfono </span><br>
							{!! Form::text('tlf_2',null,['class'=>'form-control','id'=>'tlf_2','onkeypress'=>'return soloNumeros(event)']) !!}
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
							<span>3. Teléfono </span><br>
							{!! Form::text('tlf_3',null,['class'=>'form-control','id'=>'tlf_3','onkeypress'=>'return soloNumeros(event)']) !!}
							</div>
						
						</div>
					</div>
				</div>
				</div><br><br>
				<div class="row">
				<div class="col-md-12">
					<div class="col-md-4">
						<div class="form-group">
							<span><b>16.) Ubicación Geo referencial de la Empresa, donde se refleje la ubicación exacta y la indicación de las Coordenadas Geográficas.</b></span><br>
							
							{!! Form::text('ubicacion_georeferencial',null,['class'=>'form-control','id'=>'ubicacion_georeferencial','onkeypress'=>'']) !!}
						</div>
					</div>
					<div class="col-md-4">
						<tr>
							<td class="text-center" valign="top">
								<span><b>17.) Oficio de remisión de todos los requisitos (dos originales).</b></span><br><br>
								<span class="btn btn-default btn-file" style="margin: 3px;">
                       				 Cargar Archivo <span class="glyphicon glyphicon-file"></span>
			                        {!! Form::file('file_22', array('class' => 'file-input','style'=>'margin:10px;','id'=>'file_22','title'=>'formatos permitidos .doc, .docx, .pdf')) !!}
		                      	</span>
							</td>
							<!--Para pintar la vista previa el sobre azulito al cargar un archivo con imgdocumento_ y nombre del archivo cargado con nombre_-->
             				<td class="text-center" height="40px"><img id="" src="" alt="" width="300px" style="margin: 3px;"><img id="imgdocumento_file_22" src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px; display: none;"><div id="nombre_file_22"></div>
             					 
             					 @if(isset($documentos->file_22))
					                      <div id="imgdocumentoEdit_file_22">
					                      <img src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px;">
					                      {{namefile($documentos->file_22)}}
					                      </div>
                    				 @endif

             				</td>
						</tr>
					</div>
					<div class="col-md-4">
						<tr>
							<td class="text-center" valign="top">
								<span><b>18.)Oficio de declaración de autenticidad de todos los requisitos consignados (Firmado, sellado y con huelas dactilares). Original..</b></span><br><br>
								<span class="btn btn-default btn-file" style="margin: 3px;">
                       				 Cargar Archivo <span class="glyphicon glyphicon-file"></span>
			                        {!! Form::file('file_23', array('class' => 'file-input','style'=>'margin:10px;','id'=>'file_23','title'=>'formatos permitidos .doc, .docx, .pdf')) !!}
		                      	</span>
							</td>
							<!--Para pintar la vista previa el sobre azulito al cargar un archivo con imgdocumento_ y nombre del archivo cargado con nombre_-->
             				<td class="text-center" height="40px"><img id="" src="" alt="" width="300px" style="margin: 3px;"><img id="imgdocumento_file_23" src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px; display: none;"><div id="nombre_file_23"></div>
             					 @if(isset($documentos->file_23))
				                      <div id="imgdocumentoEdit_file_23">
				                      <img src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px;">
				                      {{namefile($documentos->file_23)}}
				                      </div>
                    			 @endif

             				</td>
						</tr>
					</div>
				</div>
			</div><br><br><br><br><br><!-- 1er row-->
			<div class="row"><!--2do row-->
				<div class="col-md-12">
					<span><b>&nbsp;&nbsp;&nbsp;&nbsp;19.) Reseña fotográfica de la mercancía a Exportador (NOTA: En caso que las agencias navieras, agencias aduanales o empresas exportadoras, interrumpan las operaciones de &nbsp;&nbsp;&nbsp;&nbsp;exportación que constantemente realizan, este registro tendrá una vigencia de Seis (06) meses a partir de la última exportación realizada).</b></span><br><br>
					<div class="col-md-4">
						<tr>
							<td class="text-center" valign="top">
								
								<span class="btn btn-default btn-file" style="margin: 3px;">
                       				 Cargar Archivo <span class="glyphicon glyphicon-picture"></span>
			                        {!! Form::file('file_24', array('class' => 'file-input','style'=>'margin:10px;','id'=>'file_24','title'=>'')) !!}
		                      	</span>
							</td>
							<!--Para pintar la vista previa el sobre azulito al cargar un archivo con imgdocumento_ y nombre del archivo cargado con nombre_-->
             				<td class="text-center" height="40px"><img id="" src="" alt="" width="300px" style="margin: 3px;"><img id="imgdocumento_file_24" src="{{ asset('img/picture.png') }}" alt="" width="100px" style="margin: 3px; display: none;"><div id="nombre_file_24"></div>
             					 @if(isset($documentos->file_24))
					                      <div id="imgdocumentoEdit_file_24">
					                      <img src="{{ asset('img/picture.png') }}" alt="" width="100px" style="margin: 3px;">
					                      {{namefile($documentos->file_24)}}
					                      </div>
                    				 @endif

             				</td>
						</tr>
					</div>
					<div class="col-md-4">
						
					</div>
					<div class="col-md-4">
						
					</div>
				</div>
			</div><br><br><br><br>

			<div class="row text-center">
         	 	<a href="{{ url('/exportador/resguardoAduanero')}}" class="btn btn-primary">Cancelar</a>
          		<input type="submit" class="btn btn-success" value="Enviar" name="Enviar" onclick="enviarInversionista()"> 
        	</div><br>
			


			</div>
		</div>
	</div>
</div>
	
{{Form::close()}}


<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="{{asset('js/ajax_v2.1.1.js')}}"></script>
<!--Script para Tooltips guia de campos con stylos bootstrap-->
<script src="https://unpkg.com/@popperjs/core@2"></script>
<script src="https://unpkg.com/tippy.js@6"></script>



<script type="text/javascript">
	/********************Scrip para carga de archivos**************** */
 function cambiarImagen(event) {
    var id= event.target.getAttribute('name');
    var reader = new FileReader();
    reader.onload = function(){
      var output = document.getElementById('vista_previa_'+id);
      output.src = reader.result;
    };
    let archivo = $(".file-input").val();
    if (!archivo) {
      archivo=$('#'+id).val();
    }
    var extensiones = archivo.substring(archivo.lastIndexOf(".")).toLowerCase();
    
    //var imgfile="/exportaciones/public/img/file.png";

//alert(imgfile);
   if((extensiones != ".doc" && extensiones != ".docx" && extensiones != ".pdf")){
      swal("Formato invalido", "Formato de archivo invalido debe ser .doc, .docx, .pdf", "warning");      
    }else{
      $('#imgdocumento_'+id).show();
      $('#imgdocumentoEdit_'+id).hide();
       let file=$('#'+id).val();
     // alert(file);
      $('#nombre_'+id).html('<strong>'+file+'</strong>');
      reader.readAsDataURL(event.target.files[0]);
    }   
  
  }


  /////////////**Scrip para cargar archivos formatos validos para Documentos png,jpg y jpeg****/
  
function cambiarImagenFoto(event) {
    var id= event.target.getAttribute('name');
    var reader = new FileReader();
    reader.onload = function(){
      var output = document.getElementById('vista_previa_'+id);
      output.src = reader.result;
    };
    //let archivo = $(".file-input").val();
    //if (!archivo) {

      let archivo=$('#'+id).val();
      //console.log(reader);
    //}
    var extensiones = archivo.substring(archivo.lastIndexOf(".")).toLowerCase();
    
  
   if((extensiones != ".png" && extensiones != ".jpg" && extensiones != ".jpeg")){
    swal("Formato invalido", "Formato de archivo invalido debe ser una imagen .png, .jpg, .jpeg", "warning");    
            
    }else{
      //Pintame la vista previa el Documento azulito cuando se carga un archivo con imgdocumento_
      $('#imgdocumento_'+id).show();
      let file=$('#'+id).val();
     // alert(file);
     //Y pintame ruta y nombre del archivo acargado con nombre_
      $('#nombre_'+id).html('<strong>'+file+'</strong>');
     
      reader.readAsDataURL(event.target.files[0]);

    }

  }

////////////////////////////////////////////////////////////////////////////////////////////////////
   /* function cambiarImagenDoc(event) {

    
    //console.log(event);

    var id= event.target.getAttribute('id');
    //alert(id);
    
  
      archivo=$('#'+id).val();
    //}
    var extensiones = archivo.substring(archivo.lastIndexOf(".")).toLowerCase();
    
    //var imgfile="/exportaciones/public/img/file.png";

    
    if(extensiones != ".doc" && extensiones != ".docx" && extensiones != ".pdf"){
      swal("Formato invalido", "Formato de archivo invalido debe ser .doc, .docx o pdf", "warning"); 
        $('#'+id).val('');
        
    }else{
      $('#imgdocumento_'+id).show();
      let file=$('#'+id).val();
     // alert(file);
      $('#nombre_'+id).html('<strong>'+file+'</strong>');
     
      reader.readAsDataURL(event.target.files[0]);

    }

  }*/

    $(document).on('change', '.file-input', function(evt) {
      
      let size=this.files[0].size;
      if (size>8500000) {

         swal("Tamaño de archivo invalido", "El tamaño del archivo No puede ser mayor a 8.5 megabytes","warning"); 
          this.value='';
         //this.files[0].value='';
      }else{
        cambiarImagen(evt);
      }
      
           
    });
/////////////////Par lista dinamica////////////////
  $(document).on('change', '.file_multiple', function(evt) {
           

          let size=this.files[0].size;
      if (size>2500000) {

         swal("Tamaño de archivo invalido", "El tamaño del archivo No puede ser mayor a 8.5 megabytes","warning"); 
         this.value='';
      }else{
        cambiarImagenDoc(evt);
      }

    });
  //////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////
 
   $(document).on('change', '.file-input-foto', function(evt) {
      
      let size=this.files[0].size;
      if (size>8500000) {

         swal("Tamaño de archivo invalido", "El tamaño del archivo No puede ser mayor a 8.5 megabytes","warning"); 
          this.value='';
         //this.files[0].value='';
      }else{

        cambiarImagenFoto(evt);
      }
      
           
    });




  /********************************************************************** */


</script>
@stop