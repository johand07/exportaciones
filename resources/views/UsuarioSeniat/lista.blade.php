@extends('templates/layoutlte_seniat')

@section('content')
<div class="row">
  <div class="col-md-12">
  <br><br>
    <div class="row">

        <div class="col-md-2">
        <a class="btn btn-primary" href="{{url('/Seniat/UsuarioSeniat/create')}}">
          <span class="glyphicon glyphicon-file" title="Agregar" aria-hidden="true"></span>
           Nuevo
        </a>
      </div>
      <div class="col-md-10"></div>
      
    </div><br><br>
    <table id="listaDebito" class="table table-striped table-bordered" style="width:100%">
            <thead>
                <tr>
                   
                    <th>Codigo Arancelario</th>
                    <th>Fecha Creado</th>
                    <th>Estatus</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
              @foreach($certificados as $row)
                <tr>                
                    <td>{{ $row->codigo_arancelario }}</td>   
                    <td>{{ $row->fecha_certificado }}</td>
                    <td>
                      @if($row->gen_status_id == 27)
                        En espera de aprobación de la información
                      @elseif($row->gen_status_id == 28) 
                        Rechazada
                      @elseif($row->gen_status_id == 12)
                        Aprobada por Analista
                      @else
                        Aprobada por Viceministro
                      @endif
                    </td>
                    
                    <td>
                       <a href="{{ route('UsuarioSeniat.show', $row->id) }}" class="glyphicon glyphicon-eye-open btn btn-primary btn-sm" title="Modificar" aria-hidden="true"></a>    
                    </td>


                </tr>
              @endforeach
            </tbody>
        </table>
  </div>
</div>
@stop
