@extends('templates/layoutlte_coord_inversionista')

@section('content')

<div class="content" style="background-color: #fff">
    <div class="panel-group" style="background-color: #fff;">
      <div class="panel-primary">
        <div class="panel-heading"><h3>Solicitudes de Inversión Extranjera</h3> </div>
        <div class="panel-body">
            <div class="row">
              
              <div class="col-md-12">
                <div class="row">
                  <div class="col-md-8">

                           <h4 class="text-info">Bandeja de Solicitudes de Inversión Extranjera:</h4><hr>
                          
                  </div>
                  <div class="col-md-2">
                   
                  </div>
                  <div class="col-md-2">
                    <a class="btn btn-primary" href="{{url('/CoordinadorInversion/ListaCoordInvExtjera')}}"><span class="glyphicon glyphicon-refresh" title="Agregar" aria-hidden="true"></span>
                        Actualizar</a>
                    
                  </div>
                </div>
                <div style="overflow-y: auto;">
                  <table id="listaCdInvExtranjera" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                        <tr>

                            <th>Nro. Solicitud</th>
                            <th>Rif</th>
                            <th>Razon Social</th>
                            <th>Estatus</th>
                            <th>Detalle estatus</th>
                            <th>Fecha etatus</th>
                            <th>Fecha Solicitud</th>
                            <!--th>Analista</th-->
                            <th>Coordinador</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>

                      @if(isset($declaracionesInversion))
                        @foreach($declaracionesInversion as $key =>  $decInversion)

                          <tr>
                            
                            <td>
                              {{$decInversion->num_declaracion}}
                            </td>
                            <td>
                              {{$decInversion->genUsuario->detUsuario->rif}}
                            </td>
                            <td>
                              {{$decInversion->genUsuario->detUsuario->razon_social}}
                            </td>
                            <td>
                              {{$decInversion->genStatus->nombre_status}}
                            </td>
                            <td>
                              @if((isset($decInversion->rPlanillaDjir01) && $decInversion->rPlanillaDjir01->gen_status_id == 9)&& (isset($decInversion->rPlanillaDjir02) && $decInversion->rPlanillaDjir02->gen_status_id == 9 )&& (isset($decInversion->rPlanillaDjir03) && $decInversion->rPlanillaDjir03->gen_status_id == 9))
                                  Solicitud Completada y enviada, en espera de analisis!
                              @elseif(isset($decInversion->rPlanillaDjir01) && !isset($decInversion->rPlanillaDjir02) && !isset($decInversion->rPlanillaDjir03))
                               Declaración completada. Puede descargar las Planillas.  
                              @elseif(isset($decInversion->rPlanillaDjir02) && isset($decInversion->rPlanillaDjir01) && !isset($decInversion->rPlanillaDjir03))
                               Declaración completada. Puede descargar las Planillas. 
                              @elseif(!isset($decInversion->rPlanillaDjir01) && !isset($decInversion->rPlanillaDjir02) && !isset($decInversion->rPlanillaDjir03))
                               Declaración completada. Puede descargar las Planillas. 
                              @elseif((isset($decInversion->rPlanillaDjir01) && $decInversion->rPlanillaDjir01->estado_observacion == 1 && ($decInversion->rPlanillaDjir01->gen_status_id == 11 || $decInversion->rPlanillaDjir01->gen_status_id == 13 || $decInversion->rPlanillaDjir01->gen_status_id == 15)) || (isset($decInversion->rPlanillaDjir02) && $decInversion->rPlanillaDjir02->estado_observacion == 1 && ($decInversion->rPlanillaDjir02->gen_status_id == 11 || $decInversion->rPlanillaDjir02->gen_status_id == 13 || $decInversion->rPlanillaDjir02->gen_status_id == 15) || (isset($decInversion->rPlanillaDjir03) && $decInversion->rPlanillaDjir03->estado_observacion == 1 && ($decInversion->rPlanillaDjir03->gen_status_id == 11 || $decInversion->rPlanillaDjir03->gen_status_id == 13 || $decInversion->rPlanillaDjir03->gen_status_id == 15))))
                                  <b>PLanilla Djir01:</b> {{$decInversion->rPlanillaDjir01->descripcion_observacion ? 'Con observaciones' : 'Sin observaciones'}}
                                  <br>
                                  <b>PLanilla Djir02:</b> {{$decInversion->rPlanillaDjir02->descrip_observacion ? 'Con observaciones' : 'Sin observaciones'}}
                                  <br>
                                  <b>PLanilla Djir03:</b> {{$decInversion->rPlanillaDjir03->descrip_observacion ? 'Con observaciones' : 'Sin observaciones'}}
                              @endif
                            </td>
                            <td>
                              {{$decInversion->fstatus}}
                            </td>
                            <td>
                              {{$decInversion->created_at}}
                            </td>
                            <!--td>
                              @if(isset($decInversion->rBandejaAlistaDec))
                              {{$decInversion->rBandejaAlistaDec->genUsuario->email}}
                              @else
                                Sin atender
                              @endif
                            </td-->
                            <td>
                              @if(isset($decInversion->rBandejaCoordDec))
                              {{$decInversion->rBandejaCoordDec->genUsuario->email}}
                              @else
                               Aprobada por el Coordinador.
                              @endif
                            </td>
                            <td>
                             @if(isset($decInversion->rPlanillaDjir01) && isset($decInversion->rPlanillaDjir02) && isset($decInversion->rPlanillaDjir03))
                                @if($decInversion->rPlanillaDjir01->gen_status_id == 9 && $decInversion->rPlanillaDjir02->gen_status_id == 9 && $decInversion->rPlanillaDjir03->gen_status_id == 9)
                                   Lista para atención
                                @endif
                              @endif
                              
                              @if((isset($decInversion->rPlanillaDjir01) && $decInversion->rPlanillaDjir01->gen_status_id == 12 && !isset($decInversion->rBandejaCoordDec)) && (isset($decInversion->rPlanillaDjir02) && $decInversion->rPlanillaDjir02->gen_status_id == 12 && !isset($decInversion->rBandejaCoordDec)) && (isset($decInversion->rPlanillaDjir03) && $decInversion->rPlanillaDjir03->gen_status_id == 12 && !isset($decInversion->rBandejaCoordDec)))

                                <a href="{{ route('ListaCoordInvExtjera.edit',$decInversion->id)}}" class="btn btn-sm btn-danger" id="atender"><i class="glyphicon glyphicon-user"></i><b> Atender</b></a>
                              @endif

                              @if(isset($decInversion->rPlanillaDjir01) && isset($decInversion->rPlanillaDjir02) && isset($decInversion->rPlanillaDjir03))
                                @if($decInversion->rPlanillaDjir01->gen_status_id == 13 && $decInversion->rPlanillaDjir02->gen_status_id == 13 && $decInversion->rPlanillaDjir03->gen_status_id == 13)
                                   <a href="{{ route('ListaCoordInvExtjera.edit',$decInversion->id)}}" class="btn btn-sm btn-default" id="atender"><i class="glyphicon glyphicon-user"></i><b> Anulación</b></a>
                                @endif
                              @endif
                              {{--@if(isset($decInversion->rPlanillaDjir01) && isset($decInversion->rPlanillaDjir02) && isset($decInversion->rPlanillaDjir03))
                                @if($decInversion->rPlanillaDjir01->gen_status_id == 12 && $decInversion->rPlanillaDjir02->gen_status_id == 12 && $decInversion->rPlanillaDjir03->gen_status_id == 12)
                                  <a href="{{ route('ListaCoordInvExtjera.edit',$decInversion->id)}}" class="btn btn-sm btn-danger" id="atender"><i class="glyphicon glyphicon-user"></i><b> Atender</b></a>
                                @endif
                              @endif--}}


                              @if(isset($decInversion->rPlanillaDjir01) && isset($decInversion->rPlanillaDjir02) && isset($decInversion->rPlanillaDjir03))
                                @if($decInversion->rPlanillaDjir01->gen_status_id == 18 || $decInversion->rPlanillaDjir02->gen_status_id == 18 || $decInversion->rPlanillaDjir03->gen_status_id == 18)
                                  Corregida por el usuario
                                @endif
                              @endif
                              @if(isset($decInversion->rPlanillaDjir01) && isset($decInversion->rPlanillaDjir02) && isset($decInversion->rPlanillaDjir03) && $decInversion->gen_status_id != 17)
                                  <a href="{{action('PdfController@DeclaracionJuradaInversionRealizadaP01',array('id'=>$decInversion->rPlanillaDjir01->gen_declaracion_inversion_id))}}" class="glyphicon glyphicon-file btn btn-default btn-sm" target="_blank"  aria-hidden="true" title="Declaracion Jurada Inversion Realizada 01"></a>

                                  <a href="{{action('PdfController@DeclaracionJuradaInversionRealizadaP02',array('id'=>$decInversion->rPlanillaDjir02->gen_declaracion_inversion_id))}}" class="glyphicon glyphicon-file btn btn-default btn-sm" target="_blank" title="Declaracion Jurada Inversion Realizada 02" aria-hidden="true" ></a>

                                   <a href="{{action('PdfController@DeclaracionJuradaInversionRealizadaP03',array('id'=>$decInversion->rPlanillaDjir03->gen_declaracion_inversion_id))}}" class="glyphicon glyphicon-file btn btn-default btn-sm" target="_blank" title="Declaracion Jurada Inversion Realizada 03" aria-hidden="true" ></a>

                                @if($decInversion->rPlanillaDjir01->gen_status_id == 16 && $decInversion->rPlanillaDjir02->gen_status_id == 16 && $decInversion->rPlanillaDjir03->gen_status_id == 16 || $decInversion->rPlanillaDjir01->gen_status_id == 26 && $decInversion->rPlanillaDjir02->gen_status_id == 26 && $decInversion->rPlanillaDjir03->gen_status_id == 26)
                                  <a href="{{action('PdfController@CertificadoInversionRealizada',array('id'=>$decInversion->id))}}" class="glyphicon glyphicon-file btn btn-success btn-sm" target="_blank" title="Certificado Declaracion Jurada Inversion Realizada" aria-hidden="true">Certificado</a>
                                  <a href="{{ route('ListaCoordInvExtjera.show',$decInversion->id)}}" class="btn btn-sm btn-primary" id="atender"><i class="glyphicon glyphicon-eye-open"></i><b> Ver </b></a>
                                @endif
                              @endif

                              @if(isset($decInversion->rPlanillaDjir01) && isset($decInversion->rPlanillaDjir02) && isset($decInversion->rPlanillaDjir03) && $decInversion->gen_status_id == 17)
                                  

                                @if($decInversion->rPlanillaDjir01->gen_status_id == 17 && $decInversion->rPlanillaDjir02->gen_status_id == 17 && $decInversion->rPlanillaDjir03->gen_status_id == 17)
                                  Solicitud Anulada!
                                @endif
                              @endif

                              @if((isset($decInversion->rPlanillaDjir01) && ($decInversion->rPlanillaDjir01->gen_status_id == 11 || $decInversion->rPlanillaDjir01->gen_status_id == 12 ||
                                $decInversion->rPlanillaDjir01->gen_status_id == 14 || $decInversion->rPlanillaDjir01->gen_status_id == 15) && isset($decInversion->rBandejaCoordDec)) || 
                                 (isset($decInversion->rPlanillaDjir02) && ($decInversion->rPlanillaDjir02->gen_status_id == 11|| $decInversion->rPlanillaDjir02->gen_status_id == 12 || $decInversion->rPlanillaDjir02->gen_status_id == 14 ||$decInversion->rPlanillaDjir02->gen_status_id == 15) && isset($decInversion->rBandejaCoordDec))||
                                 (isset($decInversion->rPlanillaDjir03) && ($decInversion->rPlanillaDjir03->gen_status_id == 11|| $decInversion->rPlanillaDjir03->gen_status_id == 12 || $decInversion->rPlanillaDjir03->gen_status_id == 14 || $decInversion->rPlanillaDjir03->gen_status_id == 15) && isset($decInversion->rBandejaCoordDec)))
                                  <a href="{{ route('ListaCoordInvExtjera.edit',$decInversion->id)}}" class="btn btn-sm btn-warning" id="atender"><i class="glyphicon glyphicon-user"></i><b> Continuar</b></a>
                              @endif

                              @if(isset($decInversion->rPlanillaDjir01) && !isset($decInversion->rPlanillaDjir02) && !isset($decInversion->rPlanillaDjir03))
                                Incompleta
                              @endif
                              @if(isset($decInversion->rPlanillaDjir02) && isset($decInversion->rPlanillaDjir01) && !isset($decInversion->rPlanillaDjir03))
                                 Incompleta
                              @endif
                              @if(!isset($decInversion->rPlanillaDjir01) && !isset($decInversion->rPlanillaDjir02) && !isset($decInversion->rPlanillaDjir03))
                                Incompleta
                              @endif
                            </td>
                          </tr>
                        @endforeach
                      @endif
                    
                    </tbody>    
                  </table>
                </div>
              </div>
            </div><!-- Cierre 1° Row-->
        </div>
      </div>
    </div>
</div><!-- fin content-->
@stop