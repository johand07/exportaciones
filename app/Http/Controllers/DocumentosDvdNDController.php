<?php

namespace App\Http\Controllers;
use App\Models\GenDvdNd;
use App\Models\DocumentosDvdNdBdv;
use App\Models\GenOperadorCamb;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Http\Request;
use Session;
use Auth;
use Alert;
use File;

class DocumentosDvdNDController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //dd($request->all());
        $titulo='Documentos Notas de Debito';
        $descripcion='Perfil del Exportador';

        $solicitud_Nd=GenDvdNd::where('id',$request->id)->first();
        $Doc_Nd=DocumentosDvdNdBdv::where('gen_dvd_nd_id',$request->id)->first();
        

        return view('DocumentosDvdND.documentosNd',compact('titulo','descripcion','solicitud_Nd','Doc_Nd'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $destino = '/img/docndbdv/'.Auth::user()->detUsuario->cod_empresa;
        $destinoPrivado = public_path($destino);
        
        if (!file_exists($destinoPrivado)) {
            $filesystem = new Filesystem();
            $filesystem->makeDirectory($destinoPrivado, $mode = 0777, true, true); 
        }
      if(isset($request->ruta_doc_dvd)){
          for( $i=0; $i <=count($request->ruta_doc_dvd)-1 ; $i++ ){

                $imagen = $request->ruta_doc_dvd[$i];
                if (!empty($imagen)) {
                   
                   $nombre_imagen = $imagen->getClientOriginalName();
                  
                    $imagen->move($destinoPrivado, $nombre_imagen);
                    
                   
                    $file = new DocumentosDvdNdBdv;

                    $file->gen_dvd_nd_id      = $request->gen_dvd_nd_id;
                    $file->cat_documentos_id  = $request->cat_documentos_id[$i];
                    $file->ruta_doc_dvd       = $destino.'/'.$nombre_imagen;
                    $file->save();
                    
                }
          }
      }

        Alert::success('Se han guardado los documentos correspondientes!','Cargados Correctamente los Documentos de Nota de Debito!')->persistent("Ok"); 

            //return redirect('exportador/ListaVentaND/'.$request->nota_debito_id.'/edit');

            return redirect('exportador/DvdSolicitudND');  

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\GenDvdNd  $genDvdNd
     * @return \Illuminate\Http\Response
     */
    public function show(GenDvdNd $genDvdNd)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\GenDvdNd  $genDvdNd
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $titulo='Documentos DVD Notas de Debito';
        $descripcion='Perfil del Exportador';


        $solicitud_Nd=GenDvdNd::where('id',$id)->first();
            //dd($solicitud_Nd);

        $Doc_Nd=DocumentosDvdNdBdv::where('gen_dvd_nd_id',$id)->first();

         //dd($Doc_Nd);

        /*********Consultas para lista dinamica de nota de debito****/

        $NotaDebitos=DocumentosDvdNdBdv::where('gen_dvd_nd_id',$id)->where('cat_documentos_id',9)->get();
        //dd($NotaDebitos);
        $NotaCreditos=DocumentosDvdNdBdv::where('gen_dvd_nd_id',$id)->where('cat_documentos_id',43)->get();

        $docEmbarques=DocumentosDvdNdBdv::where('gen_dvd_nd_id',$id)->where('cat_documentos_id',42)->get();

        $CopiaSwifts=DocumentosDvdNdBdv::where('gen_dvd_nd_id',$id)->where('cat_documentos_id',6)->get();

        $Facturas=DocumentosDvdNdBdv::where('gen_dvd_nd_id',$id)->where('cat_documentos_id',8)->first();
        //dd($Facturas);
        $Ivas=DocumentosDvdNdBdv::where('gen_dvd_nd_id',$id)->where('cat_documentos_id',3)->first();
        

        return view('DocumentosDvdND.edit',compact('titulo','descripcion','solicitud_Nd','Doc_Nd','NotaDebitos','NotaCreditos','docEmbarques','CopiaSwifts','Facturas','Ivas'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\GenDvdNd  $genDvdNd
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, GenDvdNd $genDvdNd ,$id)
    {
        //dd($request->ruta_doc_dvd);
        $destino = '/img/docndbdv/'.Auth::user()->detUsuario->cod_empresa;
        $destinoPrivado = public_path($destino);
        
        if (!file_exists($destinoPrivado)) {
            $filesystem = new Filesystem();
            $filesystem->makeDirectory($destinoPrivado, $mode = 0777, true, true); 
        }
      if(isset($request->ruta_doc_dvd)){
          for( $i=0; $i <=count($request->ruta_doc_dvd)-1 ; $i++ ){

            $imagen = $request->ruta_doc_dvd[$i];
                if (!empty($imagen)) {
                //alert('lalalala');
                   
                   $nombre_imagen = $imagen->getClientOriginalName();
                  
                    $imagen->move($destinoPrivado, $nombre_imagen);
                    $file = new DocumentosDvdNdBdv;
                    $file->gen_dvd_nd_id      = $request->gen_dvd_nd_id;
                    $file->cat_documentos_id  = $request->cat_documentos_id[$i];
                    $file->ruta_doc_dvd       = $destino.'/'.$nombre_imagen;
                    $file->save();
                    
                }
          }
      }

      $docNotaD =DocumentosDvdNdBdv::find($request->file_id);
      
       if(!empty($ruta_doc_dvd )){
        $auxFile1= $docNotaD->ruta_doc_dvd;
        $docNotaD->ruta_doc_dvd = $destino.'/'.$file_1;
    }

        Alert::success('Sus Documentos han sido Actualizados!','Actualización Satisfactoria!')->persistent("Ok"); 

            return redirect('exportador/ListaVentaND/'.$request->gen_dvd_nd_id.'/edit');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\GenDvdNd  $genDvdNd
     * @return \Illuminate\Http\Response
     */
    public function eliminarDocND(Request $request)
    {
          //dd($_GET['id']);
       $delete=DocumentosDvdNdBdv::find($_GET['id'])->delete();
       if($delete)
       {
        //dd(1);
          return "Documento Eliminado";
       }else
       {
         return "Documento No existe";
       }

    }



     public function destroy(GenDvdNd $genDvdNd)
    {
        //
    }
}
