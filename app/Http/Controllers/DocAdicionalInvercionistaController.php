<?php

namespace App\Http\Controllers;
use Illuminate\Filesystem\Filesystem;
use App\Models\Doc_Adic_Inversiones;
use App\Models\GenSolInversionista;
use Illuminate\Http\Request;
use Session;
use Auth;
use Alert;
use File;

class DocAdicionalInvercionistaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //dd(1);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $destino = '/img/inversionistaExt/'.Auth::user()->detUsuario->cod_empresa;
        $destinoPrivado = public_path($destino);
        
        if (!file_exists($destinoPrivado)) {
            $filesystem = new Filesystem();
            $filesystem->makeDirectory($destinoPrivado, $mode = 0777, true, true); 
        }


        for( $i=1; $i <=6 ; $i++ ){

            $imagen = $request->file('file_'.$i);
            if (!empty($imagen)) {
                //dd($imagen);
               $nombre_imagen = $imagen->getClientOriginalName();
               // dd($destinoPrivado);
                $imagen->move($destinoPrivado, $nombre_imagen);
                $auxStr='file_'.$i;
                $$auxStr = $nombre_imagen;
               // dd($$auxStr);
            }
            

        }

        $docadicional = new Doc_Adic_Inversiones;
            $docadicional->gen_sol_inversionista_id = $request->gen_sol_inversionista_id;
//dd($destino.'/'.$file_1);
            $docadicional->file_1 = $destino.'/'.$file_1;

            if (!empty($file_2)) {
                $docadicional->file_2 = $destino.'/'.$file_2;
            }
            if (!empty($file_3)) {
                $docadicional->file_3 = $destino.'/'.$file_3;
            }
            if (!empty($file_4)) {
                $docadicional->file_4 = $destino.'/'.$file_4;
            }
            if (!empty($file_5)) {
                $docadicional->file_5 = $destino.'/'.$file_5;
            }
            if (!empty($file_6)) {
                $docadicional->file_6 = $destino.'/'.$file_6;
            }
            
                $docadicional->bactivo=1;

                $docadicional->save();

                $updatesol=GenSolInversionista::where('id',$request->gen_sol_inversionista_id)
        ->update([  'estado_observacion'      => 0,
                    'observacion_inversionista'          => null,
                    'gen_status_id'         => 18,
                    'updated_at'        =>  date("Y-m-d H:i:s") 
        ]);
                

                Alert::success('Se han guardado los documentos correspondientes!','Registrado Correctamente!')->persistent("Ok");

            return redirect()->action('InversionistaExtController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Doc_Adic_Inversiones  $doc_Adic_Inversiones
     * @return \Illuminate\Http\Response
     */
    public function show(Doc_Adic_Inversiones $doc_Adic_Inversiones)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Doc_Adic_Inversiones  $doc_Adic_Inversiones
     * @return \Illuminate\Http\Response
     */
    public function edit(Doc_Adic_Inversiones $doc_Adic_Inversiones,$gen_sol_inversionista_id)
    {
        //dd($gen_declaracion_jo_id);
        $inver=GenSolInversionista::find($gen_sol_inversionista_id);
        $detDocAdicionalInversiones= Doc_Adic_Inversiones::where('gen_sol_inversionista_id',$gen_sol_inversionista_id)->first();
        //dd($detDocAdicionalDeclaracion);
        return view('DocAdicionalInvercionista.edit',compact('detDocAdicionalInversiones','gen_sol_inversionista_id','inver'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Doc_Adic_Inversiones  $doc_Adic_Inversiones
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Doc_Adic_Inversiones $doc_Adic_Inversiones, $id)
    {
        /*************************** Guardar las imagenes  ******************************/
        $destino = '/img/inversionistaExt/'.Auth::user()->detUsuario->cod_empresa;
        $destinoPrivado = public_path($destino);
        
        if (!file_exists($destinoPrivado)) {
            $filesystem = new Filesystem();
$filesystem->makeDirectory($destinoPrivado, $mode = 0777, true, true); 
        }


        for( $i=1; $i <=6 ; $i++ ){

            $imagen = $request->file('file_'.$i);
            if (!empty($imagen)) {
                //dd($imagen);
               $nombre_imagen = $imagen->getClientOriginalName();
               // dd($destinoPrivado);
                $imagen->move($destinoPrivado, $nombre_imagen);
                $auxStr='file_'.$i;
                $$auxStr = $nombre_imagen;
               // dd($$auxStr);
            }
            

        }

    $docAdiInvert = Doc_Adic_Inversiones::find($id);

    
    
        if(!empty($file_1)){
            $auxFile1= $docAdiInvert->file_1;
            $docAdiInvert->file_1 = $destino.'/'.$file_1;
        }
        if(!empty($file_2)){
            $auxFile2= $docAdiInvert->file_2;
            $docAdiInvert->file_2 = $destino.'/'.$file_2;
        }
        if(!empty($file_3)){
            $auxFile3= $docAdiInvert->file_3;
            $docAdiInvert->file_3 = $destino.'/'.$file_3;
        }
        if(!empty($file_4)){
            $auxFile4= $docAdiInvert->file_4;
            $docAdiInvert->file_4 = $destino.'/'.$file_4;
        }
        if(!empty($file_5)){
            $auxFile5= $docAdiInvert->file_5;
            $docAdiInvert->file_5 = $destino.'/'.$file_5;
        }
        if(!empty($file_6)){
            $auxFile6= $docAdiInvert->file_6;
            $docAdiInvert->file_6 = $destino.'/'.$file_6;
        }

        // $docAdiInvert->bactivo=1;
        $updatesol=GenSolInversionista::where('id',$docAdiInvert->gen_sol_inversionista_id)
        ->update([  'estado_observacion'      => 0,
                    'observacion_inversionista'          => null,
                    'gen_status_id'         => 18,
                    'updated_at'        =>  date("Y-m-d H:i:s") 
        ]);


        if($docAdiInvert->touch()){
            if(!empty($auxFile1)){
                if(is_file('.'.$auxFile1))
                unlink('.'.$auxFile1); //elimino el fichero
            }
            if(!empty($auxFile2)){
                if(is_file('.'.$auxFile2))
                unlink('.'.$auxFile2); //elimino el fichero
            }
            if(!empty($auxFile3)){
                if(is_file('.'.$auxFile3))
                unlink('.'.$auxFile3); //elimino el fichero
            }
            if(!empty($auxFile4)){
                if(is_file('.'.$auxFile4))
                unlink('.'.$auxFile4); //elimino el fichero
            }
            if(!empty($auxFile5)){
                if(is_file('.'.$auxFile5))
                unlink('.'.$auxFile5); //elimino el fichero
            }
            if(!empty($auxFile6)){
                if(is_file('.'.$auxFile6))
                unlink('.'.$auxFile6); //elimino el fichero
            }

        }else{
            if (!empty($file_1)) {
                if(is_file('.'.$destino.'/'.$file_1))
                unlink('.'.$destino.'/'.$file_1);
            }
            
            if (!empty($file_2)) {
                if(is_file('.'.$destino.'/'.$file_2))
                unlink('.'.$destino.'/'.$file_2);
            }
            
            if (!empty($file_3)) {
                if(is_file('.'.$destino.'/'.$file_3))
                unlink('.'.$destino.'/'.$file_3);
            }

            if (!empty($file_4)) {
                if(is_file('.'.$destino.'/'.$file_4))
                unlink('.'.$destino.'/'.$file_4);
            }

            if (!empty($file_5)) {
                if(is_file('.'.$destino.'/'.$file_5))
                unlink('.'.$destino.'/'.$file_5);
            }

            if (!empty($file_6)) {
                if(is_file('.'.$destino.'/'.$file_6))
                unlink('.'.$destino.'/'.$file_6);
            }
            

            Alert::warning('No es posible realizar su solicitud. Intente nuevamente.','Acción Denegada!')->persistent("OK");
            return redirect()->back()->withInput();
        }
    
        // redirect
        Alert::success('Se han actualizado los datos correspondientes a los Documentos Adicionales!','Actualizado Correctamente!')->persistent("Ok");
        return redirect()->action('InversionistaExtController@index');

    


}

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Doc_Adic_Inversiones  $doc_Adic_Inversiones
     * @return \Illuminate\Http\Response
     */
    public function destroy(Doc_Adic_Inversiones $doc_Adic_Inversiones)
    {
        //
    }
}
