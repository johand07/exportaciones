/*
-- Query: SELECT * FROM exportaciones.cat_peru_vzla
LIMIT 0, 500

-- Date: 2019-08-23 01:53
*/
INSERT INTO `cat_peru_vzla` (`id`,`codigo`,`descripcion`,`bactivo`,`created_at`,`updated_at`) VALUES (1,'3-1a','Anexo II,Artículo 3 párrafo 1 literales a) al h),(según sea el caso)',1,NULL,NULL);
INSERT INTO `cat_peru_vzla` (`id`,`codigo`,`descripcion`,`bactivo`,`created_at`,`updated_at`) VALUES (2,'3-1b','Anexo II,Artículo 3 párrafo 1 literales a) al h),(según sea el caso)',1,NULL,NULL);
INSERT INTO `cat_peru_vzla` (`id`,`codigo`,`descripcion`,`bactivo`,`created_at`,`updated_at`) VALUES (3,'3-1c','Anexo II,Artículo 3 párrafo 1 literales a) al h),(según sea el caso)',1,NULL,NULL);
INSERT INTO `cat_peru_vzla` (`id`,`codigo`,`descripcion`,`bactivo`,`created_at`,`updated_at`) VALUES (4,'3-1d','Anexo II,Artículo 3 párrafo 1 literales a) al h),(según sea el caso)',1,NULL,NULL);
INSERT INTO `cat_peru_vzla` (`id`,`codigo`,`descripcion`,`bactivo`,`created_at`,`updated_at`) VALUES (5,'3-1e','Anexo II,Artículo 3 párrafo 1 literales a) al h),(según sea el caso)',1,NULL,NULL);
INSERT INTO `cat_peru_vzla` (`id`,`codigo`,`descripcion`,`bactivo`,`created_at`,`updated_at`) VALUES (6,'3-1f','Anexo II,Artículo 3 párrafo 1 literales a) al h),(según sea el caso)',1,NULL,NULL);
INSERT INTO `cat_peru_vzla` (`id`,`codigo`,`descripcion`,`bactivo`,`created_at`,`updated_at`) VALUES (7,'3-1g','Anexo II,Artículo 3 párrafo 1 literales a) al h),(según sea el caso)',1,NULL,NULL);
INSERT INTO `cat_peru_vzla` (`id`,`codigo`,`descripcion`,`bactivo`,`created_at`,`updated_at`) VALUES (8,'3-1h','Anexo II,Artículo 3 párrafo 1 literales a) al h),(según sea el caso)',1,NULL,NULL);
INSERT INTO `cat_peru_vzla` (`id`,`codigo`,`descripcion`,`bactivo`,`created_at`,`updated_at`) VALUES (9,'3-2','Anexo II,Artículo 3 párrafo 2',1,NULL,NULL);
INSERT INTO `cat_peru_vzla` (`id`,`codigo`,`descripcion`,`bactivo`,`created_at`,`updated_at`) VALUES (10,'3-3','Anexo II,Artículo 3 párrafo 3',1,NULL,NULL);
INSERT INTO `cat_peru_vzla` (`id`,`codigo`,`descripcion`,`bactivo`,`created_at`,`updated_at`) VALUES (11,'3-4','Anexo II,Artículo 3 párrafo 4',1,NULL,NULL);
INSERT INTO `cat_peru_vzla` (`id`,`codigo`,`descripcion`,`bactivo`,`created_at`,`updated_at`) VALUES (12,'3-5','Anexo II,Artículo 3 párrafo 5',1,NULL,NULL);
INSERT INTO `cat_peru_vzla` (`id`,`codigo`,`descripcion`,`bactivo`,`created_at`,`updated_at`) VALUES (13,'3-6','Anexo II,Artículo 3 párrafo 6',1,NULL,NULL);
INSERT INTO `cat_peru_vzla` (`id`,`codigo`,`descripcion`,`bactivo`,`created_at`,`updated_at`) VALUES (14,'8','Artículo 8°',1,NULL,NULL);
INSERT INTO `cat_peru_vzla` (`id`,`codigo`,`descripcion`,`bactivo`,`created_at`,`updated_at`) VALUES (15,'n','No califica Origen Venezolano para ese Acuerdo o País',1,NULL,NULL);
INSERT INTO `cat_peru_vzla` (`id`,`codigo`,`descripcion`,`bactivo`,`created_at`,`updated_at`) VALUES (16,'*','Información insuficiente para la determinación del Origen',1,NULL,NULL);
INSERT INTO `cat_peru_vzla` (`id`,`codigo`,`descripcion`,`bactivo`,`created_at`,`updated_at`) VALUES (17,'(1)','no',1,NULL,NULL);
INSERT INTO `cat_peru_vzla` (`id`,`codigo`,`descripcion`,`bactivo`,`created_at`,`updated_at`) VALUES (18,'(2)','no',1,NULL,NULL);
INSERT INTO `cat_peru_vzla` (`id`,`codigo`,`descripcion`,`bactivo`,`created_at`,`updated_at`) VALUES (19,'(3)','no',1,NULL,NULL);
