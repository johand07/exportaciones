@extends('templates/layoutlte')
@section('content')

{{Form::open(['route'=>'DocumentosDvdND.store','method'=>'POST','id'=>'formDocDvdNd','enctype' => 'multipart/form-data']) }}

 {!!Form::hidden('gen_dvd_nd_id',$solicitud_Nd->id) !!}

<div class="content">
  <!-------------------- Aqui Carga de Documentos BDV-------------------->
  <div class="panel panel-primary">
    <div class="panel-primary" style=" #fff">
      <div class="panel-heading"><h4>Documentos Notas de Debito</h4></div>
      <div class="panel-body">
        <div class="row" id="documentos_dvd">
        <h4>&nbsp;&nbsp;&nbsp;<b>Carga de Documentos</b></h4><br><br>
        <div class="row">
          <div class="col-md-12">
            <div class="col-md-6">
              <table class="table table-bordered text-left" id="factura">
                <span><b>Factura Comercial</b></span><br><br> 

                <td>
                  <strong style="color: red"> *</strong>
                  <input type="hidden" name="cat_documentos_id[]" id="cat_documentos_id" value="8">
                  <input type="file" name='ruta_doc_dvd[]' id='ruta_doc_dvd' class="file_multiple">
                </td>

              </table>

            </div>
            <div class="col-md-6">
              <table class="table table-bordered text-left" id="notadebito">
                <span><b>Nota débito</b></span><br><br> 
                <tr> <td><button  name="add" type="button" id="add-notadebito" value="add more" class="btn btn-success" >Agregar</button></td></tr>

                <tr id="row1">

                  <td>
                    <strong style="color: red"> *</strong>
                    <input type="hidden" name="cat_documentos_id[]" id="cat_documentos_id" value="9">
                    <input type="file" name='ruta_doc_dvd[]' id='ruta_doc_dvd' class="file_multiple">
                  </td>

                </tr>
              </table>
            </div>
          </div>
        </div><br><br>

        <div class="row">
          <div class="col-md-12">
            <div class="col-md-6">
              <table class="table table-bordered text-left" id="notacredito">

                <span><b>Nota crédito</b></span><br><br>
                <tr> <td><button  name="add" type="button" id="add-notacredito" value="add more" class="btn btn-success" >Agregar</button></td></tr>

                <tr id="row2">

                  <td>
                    <strong style="color: red"> *</strong>

                    <input type="hidden" name="cat_documentos_id[]" id="cat_documentos_id" value="43">

                    <input type="file" name='ruta_doc_dvd[]' id='ruta_doc_dvd' class="file_multiple">
                  </td>

                </tr>
              </table>

            </div>
            <div class="col-md-6">
              <table class="table table-bordered text-left" id="docembarque">

                <span><b>Documento(s) de embarque (Bill Of lading,<br>guía aérea, CPIC, guía de entrega o despacho).</b></span><br><br>


                <tr> <td><button  name="add" type="button" id="add-docembarque" value="add more" class="btn btn-success" >Agregar</button></td></tr>

                <tr id="row3">

                  <td>
                    <strong style="color: red"> *</strong>

                    <input type="hidden" name="cat_documentos_id[]" id="cat_documentos_id" value="42">

                    <input type="file" name='ruta_doc_dvd[]' id='ruta_doc_dvd' class="file_multiple">

                  </td>

                </tr>
              </table>

            </div>

          </div>
        </div><br><br><br>

        <div class="row">
          <div class="col-md-12">
            <div class="col-md-6">
              <table class="table table-bordered text-left" id="swift">

                <span><b>Copia de mensaje(s) Swift correspondiente al ingreso <br>o pago de las divisas producto de la exportación realizada.</b></span><br><br>


                <tr> <td><button  name="add" type="button" id="add-swift" value="add more" class="btn btn-success" >Agregar</button></td></tr>

                <tr id="row4">

                  <td>
                    <strong style="color: red"> *</strong>

                    <input type="hidden" name="cat_documentos_id[]" id="cat_documentos_id" value="6">

                    <input type="file" name='ruta_doc_dvd[]' id='ruta_doc_dvd' class="file_multiple">

                  </td>

                </tr>
              </table>

            </div>
            <div class="col-md-6">
              <table class="table table-bordered text-left" id="declaracioniva">
                <span><b>Planilla de declaración y pago del IVA.</b></span><br><br> 

                <td>
                  <strong style="color: red"> *</strong>
                  <input type="hidden" name="cat_documentos_id[]" id="cat_documentos_id" value="3">
                  <input type="file" name='ruta_doc_dvd[]' id='ruta_doc_dvd' class="file_multiple">
                </td>

              </table>

            </div>

          </div>
        </div><br><br>
      </div>

        <!--Botones-->
        <div class="row">
          <div class="col-md-12">
            <div class="col-md-4"></div>
            <div class="col-md-4 text-center">
              <div class="form-group">
                <a href="{{route('DvdSolicitudND.index')}}" class="btn btn-primary">Cancelar</a>
                <input class="btn btn-success" type="submit" name="" value="Enviar" onclick="enviardvd()">
              </div>
            </div>
            <div class="col-md-4"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</div>
</div>
{{Form::close()}}


<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="{{asset('js/ajax_v2.1.1.js')}}"></script>
<!--Script para Tooltips guia de campos con stylos bootstrap-->
<script src="https://unpkg.com/@popperjs/core@2"></script>
<script src="https://unpkg.com/tippy.js@6"></script>

<script type="text/javascript">



$(document).ready(function() {


  //////////////////////Para Documentos BDV listas dinamicas////////////////////////
///////////////////////lista1 Nota debito///////////////////////lista1 Nota debito
  var i = 0;
    $('#add-notadebito').click(function(){
    i++;

   
     $('#notadebito tr:last').after('<tr id="row1'+i+'"display="block" class="show_div"><td><strong style="color: red"> *</strong><input type="hidden" name="cat_documentos_id[]" id="cat_documentos_id" value="9"><input type="file" name="ruta_doc_dvd[]" id="ruta_doc_dvd'+i+'" class="file_multiple"></td><td><button  name="remove" type="button" id="'+i+'"value="" class="btn btn-danger btn-remove1">x</button></td></tr>');


    });

    $(document).on('click','.btn-remove1',function(){
      var id_boton= $(this).attr("id");
      $("#row1"+id_boton+"").remove();

    });


  ////////////////////////lista2
  var j = 0;
    $('#add-notacredito').click(function(){
    j++;

   
    $('#notacredito tr:last').after('<tr id="row2'+j+'"display="block" class="show_div"><td><strong style="color: red"> *</strong><input type="hidden" name="cat_documentos_id[]" id="cat_documentos_id" value="43"><input type="file" name="ruta_doc_dvd[]" id="ruta_doc_dvd'+j+'" class="file_multiple"></td><td><button  name="remove" type="button" id="'+j+'" value="" class="btn btn-danger btn-remove2">x</button></td></tr>');


    });

    $(document).on('click','.btn-remove2',function(){
      var id_boton= $(this).attr("id");
      $("#row2"+id_boton+"").remove();

    });

    ////////////////////////lista3
    var k = 0;
    $('#add-docembarque').click(function(){
    k++;

    $('#docembarque tr:last').after('<tr id="row3'+k+'"display="block" class="show_div"><td><strong style="color: red"> *</strong><input type="hidden" name="cat_documentos_id[]" id="cat_documentos_id" value="42"><input type="file" name="ruta_doc_dvd[]" id="ruta_doc_dvd'+k+'" class="file_multiple"></td><td><button  name="remove" type="button" id="'+k+'" value="" class="btn btn-danger btn-remove3">x</button></td></tr>');


    });

    $(document).on('click','.btn-remove3',function(){
      var id_boton= $(this).attr("id");
      $("#row3"+id_boton+"").remove();

    });
  ////////////////////////lista4

    var l = 0;
    $('#add-swift').click(function(){
    j++;

   
     $('#swift tr:last').after('<tr id="row4'+l+'"display="block" class="show_div"><td><strong style="color: red"> *</strong><input type="hidden" name="cat_documentos_id[]" id="cat_documentos_id" value="6"><input type="file" name="ruta_doc_dvd[]" id="ruta_doc_dvd'+l+'" class="file_multiple"></td><td><button  name="remove" type="button" id="'+l+'" value="" class="btn btn-danger btn-remove4">x</button></td></tr>');


    });

    $(document).on('click','.btn-remove4',function(){
      var id_boton= $(this).attr("id");
      $("#row4"+id_boton+"").remove();

    });

  $("#mvendido").prop("readonly", true);
  $("#mretencion").prop("readonly", true);

});

////Funcion para ocultar y mostrar solo mostarr Carga de Documentos cuando sea BDV
function oca (argument) {
  let oca=$("#gen_operador_cambiario_id").val();
  //lert(oca);
  console.log('oca',oca);
  if (oca==2) {
    $("#documentos_dvd").show(1000)//Mostrar 
  }else{
    $('#documentos_dvd').hide(1000);//Si es cualquier otra Ocultar
  }
}
</script>

@stop

