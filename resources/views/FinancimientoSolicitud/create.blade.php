@extends('templates/layoutlte')
@section('content')
{{Form::open(['route'=>'FinanciamientoSolicitud.store','method'=>'POST','id'=>'FinacSolicitud']) }}

<div>
  <!-- Pestañas -->
  <div class="warpper">
    <input class="radio" id="one" name="group" type="radio" >
    <input class="radio" id="two" name="group" type="radio" checked>
    <input class="radio" id="three" name="group" type="radio">
    <div class="tabs">
      @if($id)
      <a href="{{route('FacturaSolicitud.edit',$id)}}" aria-hidden="true"> 
	  <label class="tab">Facturas asociadas</label>
      </a>
      @else
	  <a href="#" aria-hidden="true"> 
	  <label class="tab">Facturas asociadas</label>
      </a>
      @endif
	  <label class="tab" id="two-tab" for="two">Financiamiento</label>
    </div>
    <div class="panels">
      <div class="panel" id="two-panel">
<br>
	<div class="row">
		<div class="col-md-12">
			<div class="col-md-6">
        		<div class="form-group">
	        		{!! Form::label('','Régimen de Exportación')!!}
	          		{!! Form::select('cat_regimen_export_id',$regimen_exp,null,['class'=>'form-control','id'=>'cat_regimen_export_id',
	              'placeholder'=>'Seleccione un Regimen de Exportación','onchange'=>'regimenExport()']) !!}
        		</div>
        		<div class="form-group" id="RegExportacion" style="display: none;">
	        		{!! Form::label('','Régimen Especial')!!}
	          		{!! Form::select('cat_regimen_especial_id',$regimen_especial,null,['class'=>'form-control','id'=>'cat_regimen_especial_id',
	              'placeholder'=>'Seleccione un Regimen de Exportación','onchange'=>'regimenEspecial()']) !!}
        		</div>
        		
	          	<div class="form-group" id='ordinaria' style="display:none;">
                  		
              		{!! Form::label('','Con Financiamiento')!!}
              		{!! Form::radio('financiamiento',1,false,['class'=>'radio-inline','id'=>'financiamiento_si','onchange'=>'financ()'])!!}
              		{!! Form::label('','Sin Financiamiento')!!}
              		{!! Form::radio('financiamiento',2,true,['class'=>'radio-inline','id'=>'financiamiento_no'])!!}
                </div>
	        	
			</div>	


			<div class="col-md-6">
				<!--<div class="form-group">
                  {!! Form::label('','¿Tiene Anticipo?')!!}
                  {!! Form::label('','Si')!!}
                  {!! Form::radio('anticipo',1,false,['class'=>'radio-inline','id'=>'accion_si'])!!}
                  {!! Form::label('','No')!!}
                  {!! Form::radio('anticipo',2,true,['class'=>'radio-inline','id'=>'accion_no'])!!}
                </div>
                <div class="form-group" id="ERanticipo" style="display: none;">
	        		{!! Form::label('','Número de Anticipo')!!}
	          		{!! Form::select('gen_anticipo_id',$num_anticipo,null,['class'=>'form-control','id'=>'gen_anticipo_id',
	              'placeholder'=>'Seleccione un Número de Anticipo']) !!}
        		</div>-->
        		<div class="form-group">
                  	{!! Form::label('','Envio de Muestras')!!}	
              		{!! Form::label('','Si')!!}
              		{!! Form::radio('envio_muestra',1,false,['class'=>'radio-inline','id'=>'muestra_si','onchange'=>'financ()'])!!}
              		{!! Form::label('','No')!!}
              		{!! Form::radio('envio_muestra',2,true,['class'=>'radio-inline','id'=>'muestra_no'])!!}
                </div>
				<div class="form-group " id="OtroRegimenExpo" style="display:none;" >
	          		{!! Form::label('','Especifique Régimen')!!}
	          		{!! Form::text('especifique_regimen',null,['class'=>'form-control','id'=>'especifique_regimen','maxlength'=>'200','onkeypress'=>'return soloNumerosyLetras(event)']) !!}
	          	</div>
	          	
			</div>
		</div>
	</div>
    <div class="row">            <!--55555555555-->
    	<div class="col-md-12">
    		
                <div id="Financiamiento" style="display:none;">
                	<div class="col-md-4">
	                	<div class="form-group">
		        		{!! Form::label('','Entidad Bancaria')!!}
		          		{!! Form::select('entidad_financiera',$entidadFina,null,['class'=>'form-control','id'=>'entidad_financiera',
		              'placeholder'=>'Seleccione una Entidad Financiera',]) !!}
	        			</div>
	        			<div class="form-group">
		        		{!! Form::label('','Tipo Instrumento de Crédito')!!}
		          		{!! Form::select('tipo_instrumento_id',$tipoInstru,null,['class'=>'form-control','id'=>'tipo_instrumento_id',
		              'placeholder'=>'Seleccione una Entidad Financiera',]) !!}
	        			</div>
	                
		                <div class="form-group ">
			          		{!! Form::label('','Número de Contrato')!!}
			          		{!! Form::text('nro_contrato',null,['class'=>'form-control','id'=>'nro_contrato','maxlength'=>'20','onkeypress'=>'return soloNumeros(event)']) !!}
			          	</div>
					</div>
					<div class="col-md-4">
			          	<div class="form-group">
			    			{!! Form::label('','Fecha de Aprobación del Contrato') !!} 
			    			<div class="input-group date">
			    			{!! Form::text('fapro_contrato',null,['class'=>'form-control','readonly'=>'readonly','id'=>'fapro_contrato']) !!}
			    			<span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
			    			</div>	              
			   			</div>
			   			<div class="form-group">
			    			{!! Form::label('','Fecha de Vencimiento del Contrato') !!} 
			    			<div class="input-group date">
			    			{!! Form::text('fvenci_contrato',null,['class'=>'form-control','readonly'=>'readonly','id'=>'fvenci_contrato']) !!}
			    			<span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
			    			</div>	              
			   			</div>
			   			<div class="form-group ">
			          		{!! Form::label('','Monto Aprobado')!!}
			          		{!! Form::text('monto_apro_contrato',null,['class'=>'form-control','id'=>'monto_apro_contrato','maxlength'=>'20','onkeypress'=>'return soloNumeros(event)']) !!}
			          	</div>
			        </div>
			        <div class="col-md-4">
			          	<div class="form-group ">
			          		{!! Form::label('','Número de Cuotas')!!}
			          		{!! Form::text('num_cuota_contrato',null,['class'=>'form-control','id'=>'num_cuota_contrato','maxlength'=>'20','onkeypress'=>'return soloNumeros(event)']) !!}
			          	</div>
			          	<div class="form-group ">
			          		{!! Form::label('','Monto Amortizado')!!}
			          		{!! Form::text('monto_amort_contrato',null,['class'=>'form-control','id'=>'monto_amort_contrato','maxlength'=>'20','onkeypress'=>'return soloNumeros(event)']) !!}
			          	</div>
			          	<div class="form-group ">
			          		{!! Form::label('','Número de Cuota Amortizada')!!}
			          		{!! Form::text('num_cuota_amortizado',null,['class'=>'form-control','id'=>'num_cuota_amortizado','maxlength'=>'200','onkeypress'=>'return soloNumeros(event)']) !!}
			          	</div>
			        </div>
				</div>
			
		</div>
	</div>
	<div class="col-md-12">
		<div class="row text-center"><br><br>
			<!-- a class="btn btn-primary" href="{{ route('FacturaSolicitud.create') }}">Regresar</!-->
			<input type="submit" name="Enviar" class="btn btn-success center" value="Enviar" onclick="validacionForm()">	
		</div>	
	</div>	
{{Form::close()}}
@endsection
</div>
<script src="{{asset('adminlte/bower_components/jquery/dist/jquery.min.js')}}"></script>

<script type="text/javascript">
$(document).ready(function() {

addAttrer();
addAttfin();

/*financiamiento solicitud*/
//scrip tiene nota de credito
$('#credito_si').click(function(){
$('#notac').show(1000);

  });

$('#credito_no').click(function(){
$('#notac').hide(1000);

  });

//scrip tiene anticipo
$('#accion_si').click(function(){
$('#ERanticipo').show(1000);

  });

$('#accion_no').click(function(){
$('#ERanticipo').hide(1000);

  });


$('#financiamiento_si').click(function(){
$('#Financiamiento').show(1000);
remAttrer();
remAttrfin();
});

$('#financiamiento_no').click(function(){
$('#Financiamiento').hide(1000);
addAttrer();
addAttfin();

  });
/*fin financiamiento solicitud*/

});

addAttrer = function() {

$('#especifique_regimen').attr('noreq','noreq');

}

addAttfin = function() {

$('#entidad_financiera').attr('noreq','noreq');
$('#tipo_instrumento_id').attr('noreq','noreq');
$('#nro_contrato').attr('noreq','noreq');
$('#fapro_contrato').attr('noreq','noreq');
$('#fvenci_contrato').attr('noreq','noreq');
$('#monto_apro_contrato').attr('noreq','noreq');
$('#num_cuota_contrato').attr('noreq','noreq');
$('#monto_amort_contrato').attr('noreq','noreq');
$('#num_cuota_amortizado').attr('noreq','noreq');
}

remAttrer = function() {

$('#especifique_regimen').removeAttr('noreq');

}

remAttrfin = function() {

$('#entidad_financiera').removeAttr('noreq');
$('#tipo_instrumento_id').removeAttr('noreq');
$('#nro_contrato').removeAttr('noreq');
$('#fapro_contrato').removeAttr('noreq');
$('#fvenci_contrato').removeAttr('noreq');
$('#monto_apro_contrato').removeAttr('noreq');
$('#num_cuota_contrato').removeAttr('noreq');
$('#monto_amort_contrato').removeAttr('noreq');
$('#num_cuota_amortizado').removeAttr('noreq');

}

function notaCredito(){

	$.ajax({    //AgenteAduanal.destroy O exportador/AgenteAduanal/{AgenteAduanal}
            url: '/exportador/FinanciamientoSolicitud',
            type: 'GET',

            data: {
                    '_token'  : $('#token').val(),
                    

                },
        })
        .done(function(data) {

            //console.log();
           

            //alert(valor);
      

            if (data == 1) {
                /*swal('Eliminado !', 'Eliminado Exitosamente', 'success',{ button: "Ok!",});
                $("#"+idtabla).load(" #"+idtabla);*/
                //location.reload();
                //setTimeout('document.location.reload()',1000);
            }
            if (data == 2) {
                swal('Sin Notas de Credito Disponibles !', 'Debe cargar al menos una nota de credito', 'warning',{ button: "Ok!",});
                        
        		$("#gen_nota_credito_id").css('border', '1px solid red').after('<span id="ok1" style="color:red" class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span>');

                //$("#"+idtabla).load(" #"+idtabla);
                //location.reload();
                //setTimeout('document.location.reload()',1000);
            }

            




        })
        .fail(function(jqXHR, textStatus, thrownError) {
            errorAjax(jqXHR,textStatus)

        })
}


</script>

<script type="text/javascript">
  	
	//Validación de formulario, se puede llamar y pasar la sección que contenga los inputs a validar

    	var error = '<div class="msg_alert"><span style="color:red" class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span><strong id="cod_aran-error" class="added"> El campo es obligatorio.</strong></div>';
		var valido = '<div class="msg_alert"><span style="color:green" class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span></div>';
		var mens = ['Estimado Usuario. Debe completar los campos solicitados', 'La fecha de registro DUA no puede ser mayor a la fecha de embarque']

		validacionForm = function() {

		$('#FinacSolicitud').submit(function(event) {
		
		var campos = $('#FinacSolicitud').find('input:text, select');
		var n = campos.length;
		var err = 0;

		$("div").remove(".msg_alert");
		//bucle que recorre todos los elementos del formulario
		for (var i = 0; i < n; i++) {
				var cod_input = $('#FinacSolicitud').find('input:text, select').eq(i);
				if (!cod_input.attr('noreq')) {
					if (cod_input.val() == '' || cod_input.val() == null)
					{
						err = 1;
						cod_input.css('border', '1px solid red').after(error);
					}
					else{
						if (err == 1) {err = 1;}else{err = 0;}
						cod_input.css('border', '1px solid green').after(valido);
					}
					
				}
		}

		//Si hay errores se detendrá el submit del formulario y devolverá una alerta
		if(err==1){
				event.preventDefault();
				swal("Por Favor!", mens[0], "warning")
			}

			});
		}

</script>