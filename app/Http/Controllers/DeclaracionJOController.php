<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\DeclaracionJO;
use App\Models\Pais;
use Auth;
use App\Models\GenDeclaracionJO;
use App\Models\DetDeclaracionProduc;
use App\Models\DetDeclaracionProducPais;
use App\Models\GenUnidadMedida;
use App\Models\Productos;
use App\Models\AnalisisCalificacion;
use App\Models\DocAdicionalDjo;
use Illuminate\Support\Facades\DB;
use Alert;
use Session;
use Illuminate\Support\Collection;
class DeclaracionJOController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        
        $idDeclaracion=$request->djo ? $request->djo : '';

        // Validando si viene por get el id de la declaracion
        if (!empty($idDeclaracion)) 
        {

            $idDeclaracion=decrypt($idDeclaracion) ? decrypt($idDeclaracion) : 'false';

        }else{

            $idDeclaracion = Session::get('gen_declaracion_jo_id');
        }

        // validamos que  exista la variable id de la declaracion
        if(empty($idDeclaracion) or  $idDeclaracion=='false')
        {
              
              Alert::warning('La secion a expirado intenta nuevamente.','Acción Denegada!')->persistent("OK");

              return redirect()->action('ListaDeclaracionJOController@index'); 

        }

        // Validamos la consulta en Tabla Declaracion
        $model=new GenDeclaracionJO;

            $declaracion=$model->where('id',$idDeclaracion)
                            ->where('gen_usuario_id',Auth::user()->id)
                            ->with('rDeclaracionProduc.rPlanilla2')
                            ->with('rDeclaracionProduc.rPlanilla3')
                            ->with('rDeclaracionProduc.rPlanilla4')
                            ->with('rDeclaracionProduc.rPlanilla5')
                            ->with('rDeclaracionProduc.rPlanilla6')
                            ->with('rDeclaracionProduc.rDeclaracionPaises.rPais')
                            ->with('rDetUsuario')
                            ->get()
                            ->last();
        // 

        // validamos que exista la declaracion para el usuario
        if(empty($declaracion->id))
        {
              
              Alert::warning('La solicitud que intenta realizar no puede ser procesada no se encuentra registrada.','Acción Denegada!')->persistent("OK");

              return redirect()->action('ListaDeclaracionJOController@index'); 

        }else{

            Session::put('gen_declaracion_jo_id', $declaracion->id);
        }
            //Para recordar los mensajes flash y puedan ser empleados en la redireccion 
            $request->session()->reflash();
            // Si tiene estatus  Incompleta enviame para crear 
            if ($declaracion->gen_status_id==7) {
                
                return redirect()->action('DeclaracionJOController@create'); 
            }

             // Si tiene estatus Observacion enviame para edit
            if ($declaracion->gen_status_id==11) {

                return redirect()->action('DeclaracionJOController@edit',['id'=>$declaracion->id]); 
            }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        
        // obtenemos el Id de la declaracion
        $idDeclaracion=Session::get('gen_declaracion_jo_id');

        // Validamos la consulta en Tabla Declaracion 
        $model=new GenDeclaracionJO;
        $declaracion=$model->where('id',$idDeclaracion)
                            ->where('gen_status_id',7)
                            ->where('gen_usuario_id',Auth::user()->id)
                            ->with('rDeclaracionProduc')
                            ->with('rDeclaracionProduc.rPlanilla2')
                            ->with('rDeclaracionProduc.rPlanilla3')
                            ->with('rDeclaracionProduc.rPlanilla4')
                            ->with('rDeclaracionProduc.rPlanilla5')
                            ->with('rDeclaracionProduc.rPlanilla6')
                            ->with('rDeclaracionProduc.rDeclaracionPaises.rPais')
                            ->with('rDetUsuario')
                            ->first();
        //dd($declaracion);

        // Validamos que sea de estatus 7
        if(empty($declaracion->id))
        {
          
          Alert::warning('La solicitud que intenta realizar requiere que este asociada a una Declaracion Jurada de Origen sin completar.','Acción Denegada!')->persistent("OK");

          return redirect()->action('ListaDeclaracionJOController@index'); 
        }            

           $titulo= 'Declaración Jurada de Origen';
           $descripcion= 'Lista de Productos para Declarar';

        return view('DeclaracionJO.index',compact('declaracion','titulo','descripcion','idDeclaracion'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // refactorizar 
        # Registrar los paises asociados a cada producto declarado
        foreach ($request->idPaises as $idTablaProducto => $idPaisesArray) {
            
            $idPaises=explode(',',$idPaisesArray);

            foreach ($idPaises as $key => $idPais) {

                #INSERT
                $modelProductoPaises= new DetDeclaracionProducPais;
                 # id de tabla det_declaracion_produc
                $modelProductoPaises->det_declaracion_produc_id = $idTablaProducto;
                 # id del pais
                $modelProductoPaises->pais_id = $idPais;

                $cantidadPaises[$idPais]="";
                if ($modelProductoPaises->save()) {
                    
                    # Inserto el Registro
                }else{

                     Alert::success('Fallo!')->persistent("Ok");
                     
                     return redirect()->action('ListaDeclaracionJOController@index');
                }
            }
        }
        
        $modelSolicitudDJO=GenDeclaracionJO::where('gen_usuario_id',Auth::user()->id)->find($request->idSolicitud);
        $modelSolicitudDJO->num_paises    = count($cantidadPaises); 
        $modelSolicitudDJO->gen_status_id = 9;
        $modelSolicitudDJO->fstatus       = date('Y-m-d');

        if($modelSolicitudDJO->update()){

            Alert::success('Su Solicitud ha sido Completada Exitosamente!')->persistent("Ok");
            return redirect()->action('ListaDeclaracionJOController@index');
        }
       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $gen_declaracion_jo_id = $id;

        // Validamos la consulta en Tabla Declaracion 
        $model=new GenDeclaracionJO;
        $declaracion=$model->where('id',$gen_declaracion_jo_id)
                            ->where('gen_usuario_id',Auth::user()->id)
                            ->with('rDeclaracionProduc')
                            ->with('rDeclaracionProduc.rPlanilla2')
                            ->with('rDeclaracionProduc.rPlanilla3')
                            ->with('rDeclaracionProduc.rPlanilla4')
                            ->with('rDeclaracionProduc.rPlanilla5')
                            ->with('rDeclaracionProduc.rPlanilla6')
                            ->with('rDeclaracionProduc.rDeclaracionPaises.rPais')
                            ->with('rDetUsuario')
                            ->first();

        if(empty($declaracion->id))
        {
            Alert::warning('No se pudo procesar su solicitud','Acción Denegada!')->persistent("OK");
             DB::rollback();
            return redirect()->back()->withInput();
        }

        $detDocAdicionalDeclaracion= DocAdicionalDjo::where('gen_declaracion_jo_id',$gen_declaracion_jo_id)->first();

        $titulo= 'Declaración Jurada de Origen';
        $descripcion= 'Lista de Productos para Declarar';
        //Se agrego la variable de paises///
        $paises= 'Lista de Paises de Origen';

        // Obtengo el código de status actual - By WETS
        $status_id = GenDeclaracionJO::where('id', $gen_declaracion_jo_id)->first();

        return view('DeclaracionJO.edit',compact('declaracion','titulo','descripcion','detDocAdicionalDeclaracion', 'gen_declaracion_jo_id', 'status_id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

       DB::beginTransaction();
       $productoPaisesFM=[];
       $cantidadPaisesFM=[];
       $inserPaisesProducto=[];
       $deletePaisesProducto=[];
       // por ahora cancelado el editar de los paises no podra agrear paises ni eliminar pais de la solicitud
        // Recorremos los paises que vienen en el formulario
       /* foreach ($request->idPaises as $idTablaProducto => $idPaisesArray) 
        {
            // Consultamos los paises que estan en la base de datos
             $ProductoPaisesBD=DetDeclaracionProducPais::where('det_declaracion_produc_id',$idTablaProducto)->get();
            //
            // tranformamos el tipo de variable recibido 'idproducto'=>'idpais,idpas2' 
            $idPaises=explode(',',$idPaisesArray);

            // recorremos los paises por producto del formulario
            foreach ($idPaises as $key => $idPais) 
            {
                // validamos que el id no sea vacio
               
                if (!empty($idPais)) 
                {
                    // contamos y almacenamos la cantidad de paises hacia donde va exportar los productos                       
                        $cantidadPaisesFM[$idPais]="";                        
                    //
                        $productoPaisesFM[][$idTablaProducto]=$idPais;
                     // Si existe en base de datos el pais 
                        if ($ProductoPaisesBD->contains('pais_id',$idPais)) {

                            // dejamos el registro en bd 

                    // Si el pais No existe en base de datos lo almacenamos para luego insertarlo 
                        }else{ 

                                //////////////////////////////////////////////////////////////////////////
                                //   INSERTAR                                                            ///
                                ///////////////////////////////////////////////////////////////////////////  
                                $modelProductoPaises= new DetDeclaracionProducPais;
                                  $modelProductoPaises->det_declaracion_produc_id = $idTablaProducto;
                                    $modelProductoPaises->pais_id = $idPais;
                                     //dd($modelProductoPaises);
                                        if ($modelProductoPaises->save())
                                        {
                                            

                                        }else{

                                            Alert::success('Esta solicitud no pruede ser procesada, Fallo actualizando los paises ,  Intente Nuevamente!')->persistent("Ok");
                                             DB::rollback();
                                            return redirect()->back()->withInput();
                                        }
                        }                                       
                }              
            }

            // recorremos los paises por producto desde la base de datos
            foreach ($ProductoPaisesBD as $indice => $idPaisesBD) 
            {

               if (collect($productoPaisesFM)->contains($idTablaProducto,$idPaisesBD->pais_id)) 
               {
                    //echo "<br>".$idTablaProducto."->".$idPaisesBD->pais_id."-Existente<br>";

               }else{
                 
                        //////////////////////////////////////////////////////////////////////////
                        //   DELETE                                                            ///
                        //////////////////////////////////////////////////////////////////////////
                        $deleteProductoPaises= DetDeclaracionProducPais::where('det_declaracion_produc_id',$idTablaProducto)->where('pais_id',$idPaisesBD->pais_id)->get()->last();
                        //dd($deleteProductoPaises);
                        if ($deleteProductoPaises->delete())
                        {
                            

                        }else{

                            Alert::success('Esta solicitud no pruede ser procesada, Fallo actualizando los paises ,  Intente Nuevamente!')->persistent("Ok");
                             DB::rollback();
                            return redirect()->back()->withInput();
                        }
                }   
            }

        } */           
            // agregado observacion corregida por el usuario para indicarle que hacer

            // Obtengo el código de status actual - By WETS
            $status_id = GenDeclaracionJO::where('id', $request->idSolicitud)->first();

            if ($status_id->gen_status_id==16) {

                $modelAnalisis=AnalisisCalificacion::where('gen_declaracion_jo_id',$request->idSolicitud)->first();
                $modelAnalisis->observacion_analisis = $request->observacion_analisis;
                $modelAnalisis->gen_status_id = 37;

                $modelSolicitudDJO=GenDeclaracionJO::where('gen_usuario_id',Auth::user()->id)->find($request->idSolicitud);
                $modelSolicitudDJO->gen_status_id = 37;
                $modelSolicitudDJO->fstatus = date('Y-m-d');


            }else{

                $modelSolicitudDJO=GenDeclaracionJO::where('gen_usuario_id',Auth::user()->id)->find($request->idSolicitud);
                //$modelSolicitudDJO->num_paises    = count($cantidadPaisesFM); 
                $modelSolicitudDJO->gen_status_id = 11;
                $modelSolicitudDJO->observacion_corregida = 1; 
                $modelSolicitudDJO->fstatus = date('Y-m-d');

            }
               



    if ($status_id->gen_status_id==16) {
        if ($modelSolicitudDJO->save() && $modelAnalisis->save()) 
        {

            Alert::success('Su Solicitud ha sido Actualizada Exitosamente!')->persistent("Ok");
             DB::commit();
            return redirect()->action('ListaDeclaracionJOController@index');

        }else{

            Alert::warning('No se pudo procesar su solicitud','Acción Denegada!')->persistent("OK");
             DB::rollback();
            return redirect()->back()->withInput();
        }
    }
        else
        {
            if ($modelSolicitudDJO->save())
            {

                Alert::success('Su Solicitud ha sido Actualizada Exitosamente!')->persistent("Ok");
                DB::commit();
                return redirect()->action('ListaDeclaracionJOController@index');

            }else{

                Alert::warning('No se pudo procesar su solicitud','Acción Denegada!')->persistent("OK");
                DB::rollback();
                return redirect()->back()->withInput();
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function pdf(Request $request){

       // $gen_declaracion_jo_id = decrypt($request->djo)? decrypt($request->djo) : 0;
        $gen_declaracion_jo_id = $request->djo;
        //dd($gen_declaracion_jo_id );
        /*$model=new GenDeclaracionJO;
        $declaracion=$model->where('id',$gen_declaracion_jo_id)
                            ->where('gen_usuario_id',Auth::user()->id)
                            ->with('rDetUsuario')
                            ->with('rAnalisisCalificacion.rCriterioAnalisis.declaracionProduc.rProducto')
                            ->first();*/
                          
        // consulta de la declaracion jurada de origen y todos sus datos asociados
        $modelDeclaracion= new  AnalisisCalificacion;
        $declaracion= $modelDeclaracion
                            ->where('gen_declaracion_jo_id',$gen_declaracion_jo_id)       
                            ->with('rCriterioAnalisis.declaracionProduc.rProducto.rArancelNan')
                            ->with('rCriterioAnalisis.declaracionProduc.rProducto.rArancelMer') 
                            ->with('rCriterioAnalisis.declaracionProduc.rDeclaracionPaises')                      
                            ->first();
                            //dd($declaracion);
      $vista="ReportesPdf.AnalisisDeclaracionJOP";
      $reporte=\View::make($vista,compact('declaracion'));
      $pdf=\App::make('dompdf.wrapper');
      $pdf->loadHTML($reporte)->setPaper('letter','landscape');
      
      return $pdf->stream('AnalisisDeclaracionJOP');
        return view('ReportesPdf.AnalisisDeclaracionJOP',compact('declaracion'));
    }
    
}
