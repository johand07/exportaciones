<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('arancelMercosur', function() {

    //return datatables()->eloquent(App\Models\GenArancelMercosur::query())->toJson();
    return datatables()->eloquent(App\Models\GenArancelMercosur::whereRaw("LENGTH(REPLACE(codigo, '.', '')) = 10"))->toJson();
});

Route::get('arancelNandina', function() {

    //return datatables()->eloquent(App\Models\GenArancelNandina::query())->toJson();
    return datatables()->eloquent(App\Models\GenArancelNandina::whereRaw("LENGTH(REPLACE(codigo, '.', '')) = 10"))->toJson();
});

Route::get('analisisDeclaracion/{nro_solicitud}',[
    'uses' => 'PdfController@AnalisisDeclaracionJO'
]);

Route::get('validarInvitacion/{id_invitacion}',[
    'uses' => 'NotificacionesController@validarQR'
]);

Route::get('generarQRE/{id_invitacion}',[
    'uses' => 'NotificacionesController@generarQR'
]);

Route::get('generarInvitacion/{id_invitacion}/{arte}',[
    'uses' => 'NotificacionesController@generarinvitacion'

]);



Route::get('VerificacionCertificadoVuce/{cod_empresa}',[
    'uses' => 'PdfController@CertificadoRegistroVuce'
]);

Route::get('VerificacionRegistroInversionista/{num_sol_inversionista}',[
    'uses' => 'PdfController@VerificacionRegistroInversionista'
]);

Route::get('VerificacionCertificadoReguardoAduanero/{cerId}',[
    'uses' => 'PdfController@CertificadoReguardoAduanero'
]);

Route::get('VerificacionCertificadoExportaFacil/{id}',[
    'uses' => 'PdfController@CertificadoExportaFacil'
]);

Route::get('AutorizacionEspecialExportacion/{id}',[
    'uses' => 'PdfController@AutorizacionEspecialBovino'
]);


Route::get('VerificacionDJIR/{id}',[
    'uses' => 'PdfController@CertificadoInversionRealizada'
]);

/* Ruta para el qr de certificado de ZOOSanitarioEuropeo*/
Route::get('CertificadoZooSanitarioEu/{id}',[
    'uses' => 'PdfController@certZooEu'
]);

Route::get('VerificacionCertificadoCna/{id}',[
    'uses' => 'PdfController@CertificadoVuceCna'
]);

Route::get('services/planillaEr', [
    'uses' => 'PdfController@planillaEr',
]);

Route::get('services/planillaDvd', [
    'uses' => 'PdfController@planillaDvd',
]);

Route::get('services/planillaDvdNotaDebito', [
    'uses' => 'PdfController@planillaNotaD',

]);
