<?php

namespace App\Http\Controllers;

use App\Models\DetProdFactura;
use App\Models\GenArancelNandina;
use App\Models\GenArancelMercosur;
use App\Models\Productos;
use App\Models\Permiso;
use App\Models\GenFactura;
use Illuminate\Http\Request;
use Laracasts\Flash\Flash;
use App\Models\GenUnidadMedida;
use App\Http\Requests\ProdFacturaRequest;
use Illuminate\Support\Facades\Session;
use Auth;
use Alert;
use Illuminate\Support\Facades\DB;

class DetProdFacturaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

         if($request->ajax())
         {


         if($request['valor']==""){

            return $arancel=Productos::where('gen_usuario_id',Auth::user()->id)->take(50)->get();

           }else{

             $arancel=Productos::where('gen_usuario_id',Auth::user()->id)->where(function($q) use ($request){$q->where('codigo','like','%'.$request['valor'].'%')->orWhere('descripcion','like','%'.$request['valor'].'%');
              })
            ->take(50)->get();

            return $arancel;

            }

       }else{

       $arancel=Productos::where('gen_usuario_id',Auth::user()->id)->take(50)->get();

       }

       $titulo="Productos-Factura";
       //$descripcion="Registrar Productos";
       $descripcion="Las sumatorias de los valores FOB no debe ser mayor al monto FOB registrado. Si no se cumple con esta condición no podrá registrar los productos.";
       $medidas=GenUnidadMedida::all()->pluck('dunidad','id');
       //$permisos=Permiso::where('gen_usuario_id',Auth::user()->id)->pluck('numero_permiso','id');

      $monto_fob=GenFactura::where('gen_usuario_id',Auth::user()->id)->orderBy('id','desc')->take(1)->first();
       $monto=$monto_fob->monto_fob;
       $idfact=$monto_fob->id;



       return view('productos.create',compact('medidas','arancel','titulo','descripcion','monto','idfact'));


   }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProdFacturaRequest $request)
    {

    //   dd($request->all());

       $cont=count($request->descripcion_arancelaria);
         for($i=0;$i<=($cont-1);$i++){

            $det=new DetProdFactura;
            $det->gen_factura_id=Session::get('factura_id');
            $det->codigo_arancel=$request->codigo_arancel[$i];
            $det->descripcion_arancelaria=$request->descripcion_arancelaria[$i];
            $det->descripcion_comercial=$request->descripcion_comercial[$i];
            $det->cantidad_producto=$request->cantidad_producto[$i];
            $det->precio_producto=$request->precio_producto[$i];
            $det->disponibles=$request->cantidad_producto[$i];
            $det->monto_total=$request->valor_fob[$i];
            $det->unidad_medida_id=$request->unidad_medida_id[$i];
            /*$det->permiso_id=$request->permiso_id[$i];*/
            $det->bactivo=1;
            $det->save();

       }

        Alert::success('Registro Exitoso!')->persistent("Ok");
        return redirect('exportador/factura');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\DetProdFactura  $detProdFactura
     * @return \Illuminate\Http\Response
     */
    public function show(DetProdFactura $detProdFactura)
    {




    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\DetProdFactura  $detProdFactura
     * @return \Illuminate\Http\Response
     */
    public function edit(ProdFacturaRequest $request,$id)
    {


      if($request->ajax())
      {


      if($request['valor']==""){

         return $arancel=Productos::where('gen_usuario_id',Auth::user()->id)->take(50)->get();

        }else{

         return  $arancel=Productos::where('gen_usuario_id',Auth::user()->id)
              ->where(function($q) use ($request)
              {$q->where('codigo','like','%'.$request['valor'].'%')->orWhere('descripcion','like','%'.$request['valor'].'%');
               })
               ->take(50)->get();
         }

       
    }else{

        $arancel=Productos::where('gen_usuario_id',Auth::user()->id)->take(50)->get();

    }


        $productos=DetProdFactura::where('gen_factura_id',$id)->get();
        $medidas=GenUnidadMedida::all()->pluck('dunidad','id');
        //$permisos=Permiso::where('gen_usuario_id',Auth::user()->id)->pluck('numero_permiso','id');
        $titulo="Editar Productos";
        //$descripcion="Listado de Productos";
        $descripcion="Las sumatorias de los valores FOB no debe ser mayor al monto FOB registrado. Si no se cumple con esta condición no podrá actualizar los productos.";
        $id=$id;
        $monto_fob=GenFactura::where('gen_usuario_id',Auth::user()->id)->where('id',$id)->orderBy('id','desc')->take(1)->first();
        $monto=$monto_fob->monto_fob;
        $idfact=$monto_fob->id;
        $nfact=$monto_fob->numero_factura;



      return view('productos.edit',compact('productos','medidas','arancel','titulo','descripcion','id','monto','idfact','nfact'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\DetProdFactura  $detProdFactura
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DetProdFactura $detProdFactura)
    {

   
    // $cant=self::formateo($request->cantidad_producto);
    // $precio=self::formateo($request->precio_producto);


       $cont=count($request->descripcion_arancelaria);
         for($i=0;$i<=($cont-1);$i++){

            DetProdFactura::updateOrCreate(
            ['id'=>$request->id[$i]],
            ['gen_factura_id'=>$request->gen_factura_id,
             'unidad_medida_id'=>$request->unidad_medida_id[$i],
             'codigo_arancel'=>$request->codigo_arancel[$i],
             'descripcion_arancelaria'=>$request->descripcion_arancelaria[$i],
             'descripcion_comercial'=>$request->descripcion_comercial[$i],
             'cantidad_producto'=>$request->cantidad_producto[$i],
             'precio_producto'=>$request->precio_producto[$i],
             'disponibles'=>$request->cantidad_producto[$i],
             'monto_total'=>$request->monto_total[$i],
             /*'permiso_id'=>$request->permiso_id[$i],*/
             'bactivo'=>1]

            );

       }

        Alert::success('Actualización Exitosa!')->persistent("Ok");
        return redirect('exportador/factura');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\DetProdFactura  $detProdFactura
     * @return \Illuminate\Http\Response
     */
    public function destroy(DetProdFactura $producto)
    {
      //dd($producto);
    }

    public function formateo($data){

       $a=[];

        foreach ($data as $key => $value) {
     
           if(strpos($value,'.') && strpos($value,',')){

              $a[$key]=str_replace(',','.',str_replace('.','',$value));

           }elseif(strpos($value,',')){

               $a[$key]=str_replace(',', '.',$value);
           
           }elseif(strpos($value,'.')){
 
              $a[$key]=str_replace('.','.',$value);

           }else{

             $a[$key]=$value;

           }
       }

       return $a;

    }

    public function eliminar($id)
    {

       $delete=DetProdFactura::find($_GET['id'])->delete();
       if($delete)
       {
          return 1;
       }else
       {
         return 0;
       }



    }

}
