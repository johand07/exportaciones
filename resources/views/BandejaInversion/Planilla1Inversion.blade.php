@extends('templates/layoutlte')
@section('content')

{{Form::open(['route'=>'savePlanilla1Inversion.store','method'=>'POST','id'=>'formregisinversion', 'enctype' => 'multipart/form-data']) }}

{{Form::hidden('gen_declaracion_inversion_id',$gen_declaracion_inversion_id)}}

<div class="panels">
	<div id="section1">
		<ol class="breadcrumb">
		<li  class="active" id="paso1">Paso 1</li>
		<li  class="" id="paso1">Paso 2</li>
		<li  class="" id="paso1">Paso 3</li>
		<li  class="" id="paso1">Paso 4</li>

		</ol>
		<div class="panel panel-primary">
		<div class="panel-heading"><h4>Carga de Documentos Requisitos del solicitante</h4></div>
		<div class="panel-body">
			<h5 style="color: red">(Formatos permitidos .doc, .docx, .pdf) </h5>
			<div class="row">
				<div  class="col-md-12">
					<div class="col-md-4">
						<tr>
							<td  class="text-center" valign="top">
								<span><b>1.) Registro de Información Fiscal (RIF).</b>
								</span><br><br><br>
								<span class="btn btn-default btn-file file_1" style="margin: 3px;">
									Cargar Archivo <span class="glyphicon glyphicon-file"></span>
									{!! Form::file('file_1', array('class' => 'file-input','style'=>'margin:10px;','id'=>'file_1','title'=>'formatos permitidos .doc, .docx, .pdf')) !!}
								</span>
							</td>
							<td class="text-center" height="40px"><img id="imgdocumento_file_1" src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px; display: none;"><div id="nombre_file_1"></div>
							</td>
						</tr>
					</div>
					<div class="col-md-4">
						<tr>
							<td  class="text-center" valign="top">
								<span><b>2.) Contrato de Inversión.</b>
								</span><br><br><br>
								<span class="btn btn-default btn-file file_2" style="margin: 3px;">
									Cargar Archivo <span class="glyphicon glyphicon-file"></span>
									{!! Form::file('file_2', array('class' => 'file-input','style'=>'margin:10px;','id'=>'file_2','title'=>'formatos permitidos .doc, .docx, .pdf')) !!}
								</span>
							</td>
							<td class="text-center" height="40px"><img id="imgdocumento_file_2" src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px; display: none;"><div id="nombre_file_2"></div>
							</td>
						</tr>
					</div>
					<div class="col-md-4">
						<tr>
							<td  class="text-center" valign="top">
								<span><b>3.) Poder de representación debidamente autenticado. </b>
								</span><br><br>
								<span class="btn btn-default btn-file file_3" style="margin: 3px;">
									Cargar Archivo <span class="glyphicon glyphicon-file"></span>
									{!! Form::file('file_3', array('class' => 'file-input','style'=>'margin:10px;','id'=>'file_3','title'=>'formatos permitidos .doc, .docx, .pdf')) !!}
								</span>
							</td>
							<td class="text-center" height="40px"><img id="imgdocumento_file_3" src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px; display: none;"><div id="nombre_file_3"></div>
							</td>
							
						</tr>
					</div>
				</div>
			</div> 
			<br><br>
		<div class="row">
			<div class="panel panel-primary">
				<div class="panel-heading"><h4>Carga de Documentos Requisitos de la Empresa Receptora</h4></div>
		</div>
				<h5 style="color: red">&nbsp;&nbsp;&nbsp;&nbsp;(Formatos permitidos .doc, .docx, .pdf) </h5>
			<div class="row">
				<div  class="col-md-12">
					<div class="col-md-6">
						<tr>
							<td  class="text-center" valign="top">
								<span><b>1.) Documento constitutivo de la empresa receptora de inversión, con sus modificaciones.</b>
								</span><br><br>
								<span class="btn btn-default btn-file file_4" style="margin: 3px;">
									Cargar Archivo <span class="glyphicon glyphicon-file"></span>
									{!! Form::file('file_4', array('class' => 'file-input','style'=>'margin:10px;','id'=>'file_4','title'=>'formatos permitidos .doc, .docx, .pdf')) !!}
								</span>
							</td>
							<td class="text-center" height="40px"><img id="imgdocumento_file_4" src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px; display: none;"><div id="nombre_file_4"></div>
							</td>
						</tr>
					</div>
					<div class="col-md-6">
						<tr>
							<td  class="text-center" valign="top">
								<span><b>2.) Registro de Información Fiscal, del representante legal de la empresa receptora de inversión.</b>
								</span><br><br>
								<span class="btn btn-default btn-file file_5" style="margin: 3px;">
									Cargar Archivo <span class="glyphicon glyphicon-file"></span>
									{!! Form::file('file_5', array('class' => 'file-input','style'=>'margin:10px;','id'=>'file_5','title'=>'formatos permitidos .doc, .docx, .pdf')) !!}
								</span>
							</td>
							<td class="text-center" height="40px"><img id="imgdocumento_file_5" src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px; display: none;"><div id="nombre_file_5"></div>
							</td>
						</tr>
					</div>
				</div>
			</div> <br><br>
			<div class="row">
				<div  class="col-md-12">
					<div class="col-md-6">
						<table class="table table-bordered text-left" id="empresareceptora3">
							<tr>
							<th><span><b><p style="text-align: justify;">3.) Comprobantes de las Inversiones Extranjeras Realizadas, de la adquisición de bienes, o acciones.</p>
							</b></span> </th>
							
							</tr>
							<tr> 
								<td><button  name="add" type="button" id="add-empresareceptora3" value="add more" class="btn btn-success" >Agregar</button></td>
							</tr>

							<tr id="row1">
							
							<td>
							<strong style="color: red"> *</strong>
						
							<input type="hidden" name="cat_documentos_id[]" id="cat_documentos_id" value="23">

							<input type="file" name='file[]' id='file_6' class="file_multiple">

							</td>
							
							</tr>
						</table>
					</div>
					<div class="col-md-6">
						<table class="table table-bordered text-left" id="empresareceptora4">
							<tr>
							<th><span><b><p style="text-align: justify;">4.) Resumen de Balances contables correspondientes a los últimos dos años de ejercicio económico, acompañados de los informes de preparación suscritos por un Contador o Auditor Publico Colegiado, con sus respectivas notas y soportes sobre su elaboración, en el que se refleje el monto de inversión extranjera de entrada o de salida.</p>
							</b></span> </th>
							
							</tr>
							<tr> 
								<td><button  name="add" type="button" id="add-empresareceptora4" value="add more" class="btn btn-success" >Agregar</button></td>
							</tr>

							<tr id="row2">
							
							<td>
							<strong style="color: red"> *</strong>
						
							<input type="hidden" name="cat_documentos_id[]" id="cat_documentos_id" value="24">

							<input type="file" name='file[]' id='file_7' class="file_multiple">

							</td>
							
							</tr>
						</table>
					</div>


				</div>
			</div>
			<br><br>
		
		</div>
			
			<br>
			<div class="row">
			<div class="col-md-12">
				<div class="col-md-4"></div>
				<div class="col-md-4 text-center">
				<div class="form-group">

					<a class="btn btn-primary" href="#" id="seguir1">Siguiente</a>
				</div>
				</div>
			</div>
			</div>
		</div>
		</div>
	</div>

	<!------------------------------------------------------------------- -->

	<div id="section2" style="display: none;">
		<ol class="breadcrumb">
		<li href="#" vid="paso4">Paso 1</li>
		<li class="active"  id="paso3">Paso 2</li>
		<li  class="" id="paso1">Paso 3</li>
			<li  class="" id="paso1">Paso 4</li>
		</ol>
		<div class="panel panel-primary">
			<div class="panel-heading"><h4>Información General</h4></div>
				<div class="panel-body">
				<div class="row">
					<div  class="col-md-12"><br>
						<div class="col-md-6">
							<div class="form-group">
							{!! Form::label('','Nombre o razón social del solicitante')!!}
							{!! Form::text('razon_social_solicitante',null,['class'=>'form-control','id'=>'razon_social_solicitante','onkeypress'=>'return soloLetras(event)'])!!}
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
							{!! Form::label('','N° Registro de Información Fiscal (R.I.F)')!!}
							{!! Form::text('rif_solicitante',null,['class'=>'form-control','id'=>'rif_solicitante','onkeypress'=>'return soloNumerosyLetras(event)'])!!}
							</div>
						</div>
					</div>
				</div><br><br>
				<div class="row">		
					<div class="panel panel-primary">
						<div class="panel-heading"><h4>Datos de Ingreso de Inversión Extranjera</h4></div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="col-md-6">
								<div class="form-group">
									{!! Form::label('','Fecha de inicio de operaciones') !!}
									<div class="input-group date">
										{!! Form::text('finicio_op',null,['class'=>'form-control','readonly'=>'readonly','id'=>'finicio_op']) !!}
										<span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									{!! Form::label('','Fecha de ingreso de la Inversión') !!}
									<div class="input-group date">
										{!! Form::text('fingreso_inversion',null,['class'=>'form-control','readonly'=>'readonly','id'=>'fingreso_inversion']) !!}
										<span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-md-12">
							<div class="col-md-12">
								<center>{!! Form::label('', 'Tipo de Empresa Receptora de IED') !!}</center>
								<br>
								<div class="col-md-3">
									<div class="form-group">
								{!! Form::radio('gen_tipo_empresa',2,false,['class'=>'radio-inline','id'=>'emp_privada'])!!}
								{!! Form::label('Empresa Privada ', 'Empresa Privada ',['class'=>'radio-inline']) !!}
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group">
									{!! Form::radio('gen_tipo_empresa',1,false,['class'=>'radio-inline','id'=>'emp_publica'])!!}
									{!! Form::label('Empresa Pública', 'Empresa Pública',['class'=>'radio-inline']) !!}
								
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group">
									{!! Form::radio('gen_tipo_empresa',4,false,['class'=>'radio-inline','id'=>'emp_mixto'])!!}
									{!! Form::label('Empresa Capital mixto', 'Empresa Capital mixto',['class'=>'radio-inline']) !!}
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group">
									{!! Form::radio('gen_tipo_empresa',5,false,['class'=>'radio-inline','id'=>'emp_otro'])!!}
									{!! Form::label('Otro','Otro',['class'=>'radio-inline']) !!}
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row" style="display:none" id="tipo_emp_otro">
						<div class="col-md-12">
							<div class="col-md-12">
						<div class="form-group">
								{!! Form::label('','(Indicar):')!!}
							{!! Form::text('especifique_otro',null,['class'=>'form-control','id'=>'especifique_otro','onkeypress'=>''])!!}
						</div>
						</div>
					</div>
					</div><br>

					<div class="row">
						<div class="col-md-12">
							<div class="col-md-3">
								<div class="form-group">
								{!! Form::label('','Dirección de la empresa:')!!}
								{!! Form::text('direccion_emp',null,['class'=>'form-control','id'=>'direccion_emp','onkeypress'=>''])!!}
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group">
								{!! Form::label('','Teléfono')!!}
								{!! Form::text('tlf_emp',null,['class'=>'form-control','id'=>'tlf_emp','onkeypress'=>'return soloNumeros(event)'])!!}
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group">
								{!! Form::label('','Página Web')!!}
								{!! Form::text('pagina_web_emp',null,['class'=>'form-control','id'=>'pagina_web_emp','onkeypress'=>''])!!}
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group">
								{!! Form::label('','Correo electrónico ')!!}
								{!! Form::email('correo_emp',null,['class'=>'form-control','id'=>'correo_emp','onkeypress'=>'minuscula(this.value,"#correo_emp")'])!!}
								</div>
							</div>
						</div>
					</div><br><br><br>

					<div class="row">
						<div class="col-md-12">
							<div class="col-md-3">
								<div class="form-group">
								{!! Form::label('','RR.SS')!!}
								{!! Form::text('rrss',null,['class'=>'form-control','id'=>'rrss','onkeypress'=>'return soloNumerosyLetras(event)'])!!}
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group">
								{!! Form::label('','Registro Público')!!}
								{!! Form::text('registro_publico',null,['class'=>'form-control','id'=>'registro_publico','onkeypress'=>'return soloNumerosyLetras(event)'])!!}
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group">
								<label>Estado</label><br>
						{!!Form::select('estado_emp',$estados,null,
						['class'=>'form-control','id'=>'id_estado','placeholder'=>'--Seleccione una Opción--'])!!}
							</div>
							</div>
							<div class="col-md-3">
								<div class="form-group">
								<label>Municipio</label><br>
						<select class="form-control" id="id_municipio" name="municipio_emp">
								<option value="">--Seleccione una Opción--</option>
							</select>
								</div>
							</div>
						</div>
					</div><br><br><br>
					<div class="row">
						<div class="col-md-12">
							<div class="col-md-3">
								<div class="form-group">
								{!! Form::label('','Inscrito bajo el Número')!!}
								{!! Form::text('numero_r_mercantil',null,['class'=>'form-control','id'=>'numero_r_mercantil','onkeypress'=>'return soloNumeros(event)'])!!}
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group">
								{!! Form::label('','Folio')!!}
								{!! Form::text('folio',null,['class'=>'form-control','id'=>'folio','onkeypress'=>'return soloNumerosyLetras(event)'])!!}
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group">
								{!! Form::label('','Tomo')!!}
								{!! Form::text('tomo',null,['class'=>'form-control','id'=>'tomo','onkeypress'=>'return soloNumerosyLetras(event)'])!!}
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group">
									{!! Form::label('','Fecha de Inscripción') !!}
									<div class="input-group date">
										{!! Form::text('finspeccion',null,['class'=>'form-control','readonly'=>'readonly','id'=>'finspeccion']) !!}
										<span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
									</div>
								</div>
							</div>
						</div>
					</div><br><br><br>
					<div class="row">
						<div class="col-md-12">
							<center>
								{!! Form::label('','Número de empleos actuales')!!}
							</center><br>
							<div class="col-md-6">
								<div class="form-group">
								{!! Form::label('','Directos ')!!}
								{!! Form::text('emple_actual_directo',null,['class'=>'form-control','id'=>'emple_actual_directo','onkeypress'=>'return soloNumeros(event)'])!!}
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
								{!! Form::label('','Indirectos')!!}
								{!! Form::text('emple_actual_indirecto',null,['class'=>'form-control','id'=>'emple_actual_indirecto','onkeypress'=>'return soloNumeros(event)'])!!}
								</div>
							</div>
						</div>
					</div><br><br>
					<div class="row">
						<div class="col-md-12">
							<center>
								{!! Form::label('','Empleos  por generar')!!}
							</center><br>	
							<div class="col-md-6">
								<div class="form-group">
								{!! Form::label('','Directos ')!!}
								{!! Form::text('emple_generar_directo',null,['class'=>'form-control','id'=>'emple_generar_directo','onkeypress'=>'return soloNumeros(event)'])!!}
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
								{!! Form::label('','Indirectos')!!}
								{!! Form::text('emple_generar_indirecto',null,['class'=>'form-control','id'=>'emple_generar_indirecto','onkeypress'=>'return soloNumeros(event)'])!!}
								</div>
							</div>
						</div>
					</div><br><br>
				</div>
				
				<div class="row">
				<div class="col-md-12">
					<div class="col-md-4"></div>
					<div class="col-md-4 text-center">
					<div class="form-group">
						<a class="btn btn-primary" href="#" id="atras1">Atras</a>
						<a class="btn btn-primary" href="#" id="seguir2">Siguiente</a>
					</div>
					</div>
				</div>
				</div>
		</div>
		</div>
	</div>
	<!------------------------------------------------------------------- -->

	<div id="section3" style="display: none;">
			<ol class="breadcrumb">
			<li id="paso2">Paso 1</li>
			<li id="paso1">Paso 2</li>
			<li class="active"  id="paso3">Paso 3</li>
					<li  class="" id="paso1">Paso 4</li>
			</ol>
			<div class="panel panel-primary">
			<div class="panel-heading"><h4>Capital Social de la Empresa</h4></div>
			<div class="panel-body">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							{!! Form::label('','Suscrito Bs')!!}
							{!! Form::text('capital_suscrito',null,['class'=>'form-control','id'=>'capital_suscrito','onkeypress'=>'return soloNumeros(event)'])!!}
						</div>	
					</div>
					<div class="col-md-6">
						<div class="form-group">
							{!! Form::label('','Pagado Bs')!!}
							{!! Form::text('capital_apagado',null,['class'=>'form-control','id'=>'capital_apagado','onkeypress'=>'return soloNumeros(event)'])!!}
						</div>	
					</div>
			
				</div><br><br>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							{!! Form::label('','Número de acciones emitidas')!!}
							{!! Form::text('num_acciones_emitidas',null,['class'=>'form-control','id'=>'num_acciones_emitidas','onkeypress'=>'return soloNumeros(event)'])!!}
						</div>	
					</div>
					<div class="col-md-6">
						<div class="form-group">
							{!! Form::label('','Valor nominal de cada acción Bs')!!}
							{!! Form::text('valor_nominal_accion',null,['class'=>'form-control','id'=>'valor_nominal_accion','onkeypress'=>'return soloNumeros(event)'])!!}
						</div>	
					</div>
				</div><br><br>
				
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							{!! Form::label('','Fecha de la última modificación de la composición accionaria y capital social') !!}
							<div class="input-group date">
								{!! Form::text('fmodificacion_accionaria',null,['class'=>'form-control','readonly'=>'readonly','id'=>'fmodificacion_accionaria']) !!}
								<span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
							</div>
						</div>	
					</div>
					<div class="col-md-6">
						<div class="form-group">
							{!! Form::label('','Fecha de Inscripción') !!}
							<div class="input-group date">
								{!! Form::text('finscripcion',null,['class'=>'form-control','readonly'=>'readonly','id'=>'finscripcion']) !!}
								<span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
							</div>
						</div>
					</div>
				</div>
				
						<div class="row">
				<div class="panel panel-primary">
						<div class="panel-heading"><h4>Accionistas de la Empresa Receptora de Inversión</h4></div>
					</div>
				
				<table class="table table-bordered text-center" id="accionistas">

					<tr> <td><button  name="add" type="button" id="add-accionistas" value="add more" class="btn btn-success" >Agregar</button></td></tr>

						<tr>
						<th>Nombre o Razon Social</th>
						<th>Identificación (C.I., Pasaporte, ID)</th>
						<th>Nacionalidad</th>
						<th>Capital Social suscrito</th>
						<th>Capital Social Pagado</th>
						<th>% de participación dentro del capital social </th>
						</tr>

						<tr id="row3">

							<td>{!!Form::text('razon_social_accionista[]',null,['class'=>'form-control','id'=>'razon_social_accionista','onkeypress'=>'return soloLetras(event)','maxlength'=>''])!!}</td>

							<td>{!!Form::text('identificacion_accionista[]',null,['class'=>'form-control','id'=>'identificacion_accionista','onkeypress'=>'return soloNumerosyLetras(event)','maxlength'=>''])!!}</td>

							<td>{!!Form::text('nacionalidad_accionista[]',null,['class'=>'form-control','id'=>'nacionalidad_accionista','onkeypress'=>'return soloLetras(event)','maxlength'=>''])!!}</td>

							<td>{!!Form::text('capital_social_suscrito[]',null,['class'=>'form-control','id'=>'capital_social_suscrito','onkeypress'=>'return soloNumeros(event)','maxlength'=>''])!!}</td>

							<td>{!!Form::text('capital_social_pagado[]',null,['class'=>'form-control','id'=>'capital_social_pagado','onkeypress'=>'return soloNumeros(event)','maxlength'=>''])!!}</td>

							<td>{!!Form::text('porcentaje_participacion[]',null,['class'=>'form-control','id'=>'porcentaje_participacion','onkeypress'=>'return soloNumerosyLetras(event)','maxlength'=>''])!!}</td>
	
							
							</tr> 
						
					</table>
					
				</div>
					<br>
					<div class="row">
					<div class="col-md-12">
						<div class="col-md-4"></div>
						<div class="col-md-4 text-center">
						<div class="form-group">
							<a class="btn btn-primary" href="#" id="atras2">Atras</a>
							<a class="btn btn-primary" href="#" id="seguir3">Siguiente</a>
						</div>
						</div>
					</div>
					</div>
			</div>
			</div>
	</div>

	<div id="section4" style="display: none;">
			<ol class="breadcrumb">
			<li id="paso4">Paso 1</li>
			<li id="paso3">Paso 2</li>
			<li id="paso2">Paso 3</li>
			<li class="active"  id="paso1">Paso 4</li>
			</ol>
			<div class="panel panel-primary">
			<div class="panel-heading"><h4>Datos del Representante Legal o Apoderado</h4></div>
			<div class="panel-body">
				<div class="row">
					<div class="col-md-4">
						<div class="form-group">
						{!! Form::label('','Nombres')!!}
						{!! Form::text('nombre_repre',null,['class'=>'form-control','id'=>'nombre_repre','onkeypress'=>'return soloLetras(event)'])!!}
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
						{!! Form::label('','Apellidos')!!}
						{!! Form::text('apellido_repre',null,['class'=>'form-control','id'=>'apellido_repre','onkeypress'=>'return soloLetras(event)'])!!}
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
						{!! Form::label('','N° de Documento de Identidad')!!}
						{!! Form::text('numero_doc_identidad',null,['class'=>'form-control','id'=>'numero_doc_identidad','onkeypress'=>'return soloNumerosyLetras(event)'])!!}
						</div>
					</div>
				</div><br>
				<div class="row">
					<div class="col-md-4">
						<div class="form-group">
						{!! Form::label('','N° de Pasaporte')!!}
						{!! Form::text('numero_pasaporte_repre',null,['class'=>'form-control','id'=>'numero_pasaporte_repre','onkeypress'=>'return soloNumerosyLetras(event)'])!!}
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
						{!! Form::label('','Código postal ')!!}
						{!! Form::text('cod_postal_repre',null,['class'=>'form-control','id'=>'cod_postal_repre','onkeypress'=>''])!!}
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
						{!! Form::label('','N° Registro de Información Fiscal (R.I.F) del Apoderado')!!}
						{!! Form::text('rif_repre',null,['class'=>'form-control','id'=>'rif_repre','onkeypress'=>'return soloNumerosyLetras(event)'])!!}
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-3">
						<br><div class="form-group">
						{!! Form::label('','Teléfono 1')!!}
						{!! Form::text('telefono_1',null,['class'=>'form-control','id'=>'telefono_1','onkeypress'=>'return soloNumeros(event)'])!!}
						</div>
					</div>
					<div class="col-md-3">
						<br><div class="form-group">
						{!! Form::label('','Teléfono 2')!!}
						{!! Form::text('telefono_2',null,['class'=>'form-control','id'=>'telefono_2','onkeypress'=>'return soloNumeros(event)'])!!}
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
						{!! Form::label('','Correo electrónico donde desea recibir las notificaciones')!!}
						{!! Form::email('correo_repre',null,['class'=>'form-control','id'=>'correo_repre','onkeypress'=>'minuscula(this.value,"#correo_emp")'])!!}
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
						{!! Form::label('','Dirección donde recibirá las notificaciones')!!}
						{!! Form::text('direccion_repre',null,['class'=>'form-control','id'=>'direccion_repre','onkeypress'=>''])!!}
						</div>
					</div>
				</div><br>

				<div class="row">
				<div class="col-md-12">
					<div class="col-md-4"></div>
					<div class="col-md-4 text-center">
					<div class="form-group">
						<a class="btn btn-primary" href="#" id="atras3">Atras</a>
						<!-- <a class="btn btn-primary" href="#" id="guardar_personal" onclick="enviarplanilla1()">Registrar</a> -->
						<a class="btn btn-primary" href="#" id="guardar_personal" onclick="">Registrar</a>
					</div>
					</div>
				</div>
				</div>
			</div>
		</div>
	</div>

	<!-------Modal--->
	<div id="planilla1" class="modal fade" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close btn-md" data-dismiss="modal" style="height:5px;">&times;</button>
			<h4 class="modal-title"><b style="color:#337AB7">DECLARACIÓN JURADA DE INVERSIÓN REALIZADA</b></h4>
		</div>
		<div class="modal-body">
		<div class="col-md-12">
			<p class="text-justify">DECLARO QUE LOS DATOS CONTENIDOS EN ESTA DECLARACIÓN HAN SIDO DETERMNADOS EN BASE A LAS DISPOSICIONES LEGALES Y REGLAMENTARIAS CORRESPONDIENTES.
			</p>
			<div class="row">
			<div class="colo-md-1"></div>
			<div class="col-md-10">
				<input type="checkbox" name="condicion" id="condicion" value="si" class="checkbox-inline"><label class="text-justify">LA INFORMACIÓN AQUÍ DECLARADA ES ENTREGADA CON CARÁCTER CONFIDENCIAL.</label>
			</div>
			<div class="colo-md-1"></div>
			</div>
		</div>
		</div>
		<div class="modal-footer">
			<div class="row">
			<div class="col-md-12">
				<div class="col-md-4"></div>
				<div class="col-md-4 text-center">
				<div class="form-group">
					<button type="button" class="btn btn-default" style="background-color:#ADABC9; color: #FFFFFF;" data-dismiss="modal"><b>Cerrar</b></button>
					<input type="submit" class="btn btn-primary" value="Aceptar" name="Aceptar" onclick="enviarInversionista()"> 
				</div>
				</div>
				<div class="col-md-4"></div>
			</div>
			</div>
		</div>
		</div>

	</div>
	</div>
</div>

{{Form::close()}}

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="{{asset('js/ajax_v2.1.1.js')}}"></script>

<script src="https://unpkg.com/@popperjs/core@2"></script>
<script src="https://unpkg.com/tippy.js@6"></script>



<script type="text/javascript">

/********************Scrip para carga de archivos**************** */
 function cambiarImagen(event) {
    var id= event.target.getAttribute('name');
    var reader = new FileReader();
    reader.onload = function(){
      /*var output = document.getElementById('vista_previa_'+id);
	  console.log(output);
	  console.log(reader);
      output.src = reader.result;*/
    };
    //let archivo = $(".file-input").val();
    //if (!archivo) {
		
      let archivo=$('#'+id).val();
      //console.log(reader);
    //}
    var extensiones = archivo.substring(archivo.lastIndexOf(".")).toLowerCase();
    
    //var imgfile="/exportaciones/public/img/file.png";
	let elem_boton = $('#'+id).parent().parent().find('.btn-file.'+id);
	elem_boton.removeAttr('style');
	//alert(imgfile);
    if(extensiones != ".doc" && extensiones != ".docx" && extensiones != ".pdf"){
		$('#nombre_'+id).html('');
		$('#'+id).val('');
      	swal("Formato invalido", "Formato de archivo invalido debe ser .doc, .docx o pdf", "warning"); 
		elem_boton.css('background-color','#d9534f');
      	elem_boton.css('color','white');
		elem_boton.css('overflow', 'visible');
        
    }else{
		//Pintame la vista previa el Documento azulito cuando se carga un archivo con imgdocumento_
		$('#imgdocumento_'+id).show();
		let file=$('#'+id).val();
		// alert(file);
		//Y pintame ruta y nombre del archivo acargado con nombre_
		$('#nombre_'+id).html('<strong>'+file+'</strong>');
		elem_boton.css('background-color','green');
		elem_boton.css('color','white');
		reader.readAsDataURL(event.target.files[0]);
    }

  }
  ///////////////Para validar documentos//////////////////

  
/////////////////Validacion acondicionada para cuanto la carga de archivo NO puede sercargado un archivo que pee mas de 2.5 megabytes////////////////
    $(document).on('change', '.file-input', function(evt) {
      
      let size=this.files[0].size;
      if (size>8500000) {

         swal("Tamaño de archivo invalido", "El tamaño del archivo No puede ser mayor a 8.5 megabytes","warning"); 
          this.value='';
         //this.files[0].value='';
      }else{
        cambiarImagen(evt);
		
      }
      
           
    });


  /********************************************************************** */


function CedulaFormat1(vCedulaName,mensaje,postab,escribo,evento) {

  //  var tipoPerson=document.getElementsByName('tipo_usuario');
    tecla=getkey(evento);
    vCedulaName.value=vCedulaName.value.toUpperCase();
    vCedulaValue=vCedulaName.value;
    valor=vCedulaValue.substring(2,12);
    var numeros='0123456789/';
    var digit;
    var noerror=true;
    if (shift && tam>1) {
    return false;
    }
    for (var s=0;s<valor.length;s++){
    digit=valor.substr(s,1);
    if (numeros.indexOf(digit)<0) {
    noerror=false;
    break;
    }
    }
    tam=vCedulaValue.length;
    if (escribo) {
    if ( tecla==8 || tecla==37) {
    if (tam>2)
    vCedulaName.value=vCedulaValue.substr(0,tam-1);
    else
    vCedulaName.value='';
    return false;
    }
    if (tam==0 && tecla==69) {
    vCedulaName.value='E-';
    return false;
    }
    if (tam==0 && tecla==69) {
    //vCedulaName.value='E-';
    return false;
    }


    if (tam==0 && tecla==86) {
    vCedulaName.value='V-';
    return false;
    }


        if (tam==0 && tecla==74) {
    vCedulaName.value='J-';
    return false;
    }
    if (tam==0 && tecla==71) {
    vCedulaName.value='G-';
    return false;
    }
    if (tam==0 && tecla==80) {
    vCedulaName.value='P-';
    return false;
    }

    else if ((tam==0 && ! (tecla<14 || tecla==69 || tecla==86 || tecla==46)))
    return false;
    else if ((tam>1) && !(tecla<14 || tecla==16 || tecla==46 || tecla==8 || (tecla >= 48 && tecla <= 57) || (tecla>=96 && tecla<=105)))
    return false;
    }

}


$(document).ready(function (){

	/*************************Lista dinamica para Requisitos de la Empresa Receptora Doc 3 y 4 ********************** */

	var i = 0;
	$('#add-empresareceptora3').click(function(){
	i++;

		$('#empresareceptora3 tr:last').after('<tr id="row1'+i+'"display="block" class="show_div"><td><strong style="color: red"> *</strong> <input type="hidden" name="cat_documentos_id[]" id="cat_documentos_id" value="23"><input type="file" name="file[]" id="file_6'+i+'" class="file_multiple"></td><td><button  name="remove" type="button" id="'+i+'" value="" class="btn btn-danger btn-remove1">x</button></td></tr>');

				
	});

	$(document).on('click','.btn-remove1',function(){
		var id_boton= $(this).attr("id");
		$("#row1"+id_boton+"").remove();

	});

	/*********************Lista Dinamica para soporte 4 **************************/
	var j = 0;
	$('#add-empresareceptora4').click(function(){
	j++;

		$('#empresareceptora4 tr:last').after('<tr id="row2'+j+'"display="block" class="show_div"><td><strong style="color: red"> *</strong> <input type="hidden" name="cat_documentos_id[]" id="cat_documentos_id" value="24"><input type="file" name="file[]" id="file_7'+j+'" class="file_multiple"></td><td><button  name="remove" type="button" id="'+j+'" value="" class="btn btn-danger btn-remove2">x</button></td></tr>');

	});

	$(document).on('click','.btn-remove2',function(){
		var id_boton= $(this).attr("id");
		$("#row2"+id_boton+"").remove();

	});




	
	/*********************Lista Dinamica para soporte 4 **************************/

	//////////////////Lista Dinamica Accionistas//////////////////
	var i = 0;
		$('#add-accionistas').click(function(){
		i++;

			$('#accionistas tr:last').after('<tr id="row3'+i+'"display="block" class="show_div"><td>{!!Form::text("razon_social_accionista[]",null,["class"=>"form-control","id"=>"razon_social_accionista","onkeypress"=>"return soloLetras(event)","maxlength"=>""])!!}</td><td>{!!Form::text("identificacion_accionista[]",null,["class"=>"form-control","id"=>"identificacion_accionista","onkeypress"=>"return soloNumerosyLetras(event)","maxlength"=>""])!!}</td><td>{!!Form::text("nacionalidad_accionista[]",null,["class"=>"form-control","id"=>"nacionalidad_accionista","onkeypress"=>"return soloLetras(event)","maxlength"=>""])!!}</td><td>{!!Form::text("capital_social_suscrito[]",null,["class"=>"form-control","id"=>"capital_social_suscrito","onkeypress"=>"return soloNumeros(event)","maxlength"=>""])!!}</td><td>{!!Form::text("capital_social_pagado[]",null,["class"=>"form-control","id"=>"capital_social_pagado","onkeypress"=>"return soloNumeros(event)","maxlength"=>""])!!}</td><td>{!!Form::text("porcentaje_participacion[]",null,["class"=>"form-control","id"=>"porcentaje_participacion","onkeypress"=>"return soloNumerosyLetras(event)","maxlength"=>""])!!}</td><td><button  name="remove" type="button" id="'+i+'" value="" class="btn btn-danger btn-remove">x</button></td></tr>');

		});

		$(document).on('click','.btn-remove',function(){
			var id_boton= $(this).attr("id");
			$("#row3"+id_boton+"").remove();

		});

	/*****************Ocultar y mostrar Empresa Receptora ***********/
	$('#emp_otro').click(function(event) {
		$('#tipo_emp_otro').show();
	});
	$('#emp_privada,#emp_publica,#emp_mixto').click(function(event) {
		$('#tipo_emp_otro').hide();
	});


	  //////////Tooltip Guia para cada campo///////// 
	tippy('#razon_social_solicitante', {
	content: 'Colocar nombre y apellido de la persona natural según se indique en el documento de identidad o pasaporte o, la denominación comercial de la empresa o persona jurídica que solicita el Certificado de Registro de Inversión, según aparezca en sus estatutos de creación.',
	placement: 'bottom',
	});

	tippy('#rif_solicitante', {
	content: 'Las personas naturales o jurídicas con residencia o no en el territorio nacional que deseen realizar negocios o asentar su empresa productiva en el territorio nacional deben inscribirse en este Registro, ante el SENIAT. (www.seniat.gob.ve)',
	placement: 'bottom',
	});

	tippy('#direccion_emp', {
	content: 'Colocar la dirección fiscal del sujeto receptor de receptora de inversión, desagregada en los campos que se solicitan en la Forma.',
	placement: 'bottom',
	});

		tippy('#tlf_emp', {
	content: 'Colocar lo número telefónico asociados a la ubicación fiscal del sujeto receptor de inversión.',
	placement: 'bottom',  
	}); 

	tippy('#pagina_web_emp', {
	content: 'Colocar la página web del sujeto receptor de inversión.',
	placement: 'bottom',  
	}); 

	tippy('#correo_emp', {
	content: 'Colocar correo electrónico de contacto con el sujeto receptor de inversión',
	placement: 'bottom',  
	}); 




	tippy('#registro_publico', {
	content: 'Ejemplo: REGISTRO MERCANTIL SEGUNDO DEL DISTRITO CAPITAL',
	placement: 'bottom',  
	});
	

	tippy('#id_estado', {
	content: 'Seleccionar la Entidad Federal donde se encuentra establecido el sujeto receptor de inversión, de acuerdo al R.I.F. (para el caso de la dirección fiscal), o en donde se encuentra establecida su principal planta productiva.',
	placement: 'bottom',  
	}); 

	tippy('#id_municipio', {
	content: 'Colocar el municipio donde se encuentra establecido el sujeto receptor de la inversión, de acuerdo al R.I.F. (para el caso de la dirección fiscal), o en donde se encuentra establecida su principal planta productiva.',
	placement: 'bottom',  
	});

	tippy('#numero_r_mercantil', {
	content: 'Colocar el número bajo el cual quedó protocolizado el documento constitutivo de la sociedad mercantil, ante el registro respectivo.',
	placement: 'bottom',  
	}); 
	

	tippy('#tomo', {
	content: 'Colocar el tomo bajo el cual quedó protocolizado el documento constitutivo de la sociedad mercantil, ante el registro respectivo.',
	placement: 'bottom',  
	}); 

	tippy('#emple_actual_directo', {
	content: ' Colocar la cantidad de puestos de trabajo directo, que actualmente genera el sujeto receptor de inversión.',
	placement: 'bottom',  
	}); 

	tippy('#emple_actual_indirecto', {
	content: 'Colocar la cantidad de puestos de trabajo indirecto, que actualmente genera el sujeto receptor de inversión.',
	placement: 'bottom',  
	}); 

	tippy('#emple_generar_directo', {
	content: 'Colocar la cantidad de puestos de trabajo directo, que estima generar el sujeto receptor de inversión.',
	placement: 'bottom',  
	});

	tippy('#emple_generar_indirecto', {
	content: 'Colocar la cantidad de puestos de trabajo indirecto, que estima generar el sujeto receptor de inversión.',
	placement: 'bottom',  
	});

	tippy('#capital_suscrito', {
	content: 'Colocar el capital suscrito actual del sujeto receptor de inversión, en Bolívares Soberanos (Bs.S).',
	placement: 'bottom',  
	});

	tippy('#capital_apagado', {
	content: 'Colocar el capital pagado actual del sujeto receptor de inversión, en Bs.S.',
	placement: 'bottom',  
	})

	tippy('#num_acciones_emitidas', {
	content: 'Colocar el número de acciones actualizado, que constituye la totalidad del capital social del sujeto receptor de inversión.',
	placement: 'bottom',  
	})

	tippy('#valor_nominal_accion', {
	content: 'Colocar el valor nominal de cada acción emitida por el sujeto receptor de inversión, expresado en Bs.S.',
	placement: 'bottom',  
	})  

	tippy('#razon_social_accionista', {
	content: 'Colocar el nombre o razón social del accionista.',
	placement: 'bottom',  
	})

	tippy('#identificacion_accionista', {
	content: 'Colocar el número corresponda.',
	placement: 'bottom',  
	}) 

	tippy('#nacionalidad_accionista', {
	content: 'Colocar Nacionalidad del accionista.',
	placement: 'bottom',  
	}) 

	tippy('#capital_social_suscrito', {
	content: 'Colocar Capital Social suscrito del accionista.',
	placement: 'bottom',  
	}) 

	tippy('#capital_social_pagado', {
	content: 'Colocar Capital Social Pagado del accionista.',
	placement: 'bottom',  
	}) 

		tippy('#porcentaje_participacion', {
	content: 'Colocar % de participación dentro del capital social.',
	placement: 'bottom',  
	}) 
 
});/*llave del document).ready*/

var noseguir = 0;
function enviarPlanillaInversionista(section) {
	
	let input_basicos = $('#formregisinversion '+section).find('input, select, textarea').not(':button,:hidden');
	let campos = [];
	let extraer = ['pagina_web_emp'];
	let excepcion = ['finicio_op','fmodificacion_accionaria', 'finscripcion','fingreso_inversion', 'emp_privada', 'emp_publica', 'finspeccion', 'emp_publica', 'emp_mixto', 'emp_otro'];
	let agregar = ['razon_social_accionista', 'identificacion_accionista', 'nacionalidad_accionista', 'capital_social_suscrito', 'capital_social_pagado','porcentaje_participacion'];
	let k = 0;

	for(let i=0; i < input_basicos.length; i++){
		if(!extraer.includes(input_basicos[i].id)){
			campos.push(input_basicos[i].id);
		}
	}

	$("div").remove(".msg_alert");

	let err_duda = 0;
	let error_dudas = '<div class="msg_alert"><span style="color:red" class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span><strong id="cod_aran-error" class="added"> El campo es obligatorio.</strong></div>';
	let valido_dudas = '<div class="msg_alert"><span style="color:green" class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span></div>';
	let n = campos.length;
	let input_duda = '';
	let m = 0;

	for (var i = 0; i < n; i++) {
		input_duda = $('#'+campos[i]);

		let valor_campo = '';
		switch(campos[i]){
			case 'emp_privada':
			case 'emp_publica':
			case 'emp_mixto':
			case 'emp_otro':
				if($('#emp_privada').is(":checked") || $('#emp_publica').is(":checked") || $('#emp_mixto').is(":checked") || $('emp_otro').is(":checked")){
					valor_campo = 1;
				}
			break;

			default:
				valor_campo = input_duda.val();
			break;
		}
		
		if (valor_campo == '')
		{
			err_duda = 1;
			noseguir = 1;
			if(input_duda[0].type == 'file'){
				if(m <5){
					$('.'+input_duda[0].id).css('border', '1px solid red').after(error_dudas);
				} else {
					input_duda.css('border', '1px solid red').after(error_dudas);
				}
				m++;
			} else {
				input_duda.css('border', '1px solid red').after(error_dudas);
			}
			
			
			if(err_duda==1){
				event.preventDefault();
				swal("¡Por Favor!", 'Estimado usuario. Debe completar los campos solicitados', "warning");
			}
		}else{
			if (err_duda == 1) {
				err_duda = 1;
				noseguir = 1;
			}else{
				err_duda = 0;
				noseguir = 0;
			}

			if(input_duda[0].type == 'file'){
				if(m <5){
					$('.'+input_duda[0].id).css('border', '1px solid green').after(valido_dudas);
				} else {
					input_duda.css('border', '1px solid green').after(valido_dudas);
				}
			} else {
				input_duda.css('border', '1px solid green').after(valido_dudas);
			}
		}
	}


	if(err_duda == 0 && section == "#section4"){
		return 1;
	}
	
}

$('#seguir1').on('click', function(){
	enviarPlanillaInversionista('#section1')
});
$('#seguir2').on('click', function(){
	enviarPlanillaInversionista('#section2')
});

$('#seguir3').on('click', function(){
	enviarPlanillaInversionista('#section3')
});

$('#guardar_personal').on('click', function(){
	let registrar = enviarPlanillaInversionista('#section4');
	if(registrar == 1){
		enviarplanilla1();
	}
});

</script>
@stop