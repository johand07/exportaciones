
@extends('templates/layoutlte')
@section('content')
<div class="panels">
   <div class="row">
      <div class="col-md-1">
         <a class="btn btn-primary" href="{{url('/exportador/factura/create')}}">
            <span class="glyphicon glyphicon-file" title="Agregar" aria-hidden="true"></span>
            Nuevo
         </a>
      </div>
      <div class="col-md-1">
      </div>
      <div class="col-md-3"></div>
   </div>
   <hr>
   <div class="row">
      <div class="col-md-12">
         <table id="tablaFacturas" class="table table-striped table-bordered" style="width:100%">
            <!-- div class="col-md-1">
               Desde:
            </-div>
            <div class="col-md-2">
               <input type="date" name="fechadesde" class="form-control input-sm">
            </div>
            <div class="col-md-1">
               Hasta:
            </div>
            <div class="col-md-2">
               <input type="date" name="fechadesde" class="form-control input-sm">
            </div>
            <div class="col-md-1">
               Buscar:
            </div>
            <div class="col-md-3">
               <input type="text" name="fechadesde" class="form-control input-sm">
            </div -->
            <thead>
               <tr>
                  <th>#</th>
                  <th>Número de Factura</th>
                  @if( !is_null($id_sol) && $id_sol==1)
                  <th>Número de Dua </th>
                  @endif
                  <th>Fecha de la Factura</th>
                  <th>Monto FOB</th>
                  <th>Tipo de Solicitud</th>
                  @if($tipoExpor->productos_tecno==2)
                  <th>Número de la Dua</th>
                  @endif
                  <th>Acciones</th>
               </tr>
            </thead>
            <tbody>
               @foreach($facturas as $key=>$factura)
               @if($factura->bactivo!=2)
               <tr>
                  <td>{{$key+1}}</td>
                  <td>{{$factura->numero_factura}}</td>
                  @if( !is_null($id_sol) && $id_sol==1)
                  <td>{{$factura->dua->numero_dua}}</td>
                  @endif
                  <td>{{$factura->fecha_factura}}</td>
                  <td>{{$factura->monto_fob}}</td>
                  <td>@if(!is_null($factura->tipo_solicitud_id)){{$factura->tipoSolicitud->solicitud}}@endif</td>
                  @if($tipoExpor->productos_tecno==2)
                  <td>@if(!is_null($factura->gen_dua_id)){{ $factura->dua->numero_dua }} @endif</td>
                  @endif
                  
                  <td>
                     <a href="{{route('factura.edit',$factura->id)}}"
                        class="glyphicon glyphicon-edit btn btn-primary btn-sm" title="Modificar"
                        aria-hidden="true"></a>
                        
                    {{-- <a href="{{route('factura.edit',$factura->id)}}"
                           class="btn btn-danger btn-sm" title="Eliminar"
                           aria-hidden="true"><i class="glyphicon glyphicon-trash"></i></a>
                     <input id="token" type="hidden" name="_token" value="{{ csrf_token() }}">--}}
                     
                      <a href="#" class="btn btn-danger btn-sm" title="Eliminar" onclick="eliminarFactura('factura/',{{$factura->id}},'tablaFacturas')">
                     <i class="glyphicon glyphicon-trash"></i> </a> 
                  </td>
               </tr>
               @endif
               @endforeach
            </tbody>
         </table>
      </div>
   </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="{{asset('js/ajax_v2.1.1.js')}}"></script>
<script type="text/javascript">



function eliminarFactura(ruta,eli, idtabla){

/*creo una cookie con la id a eliminar*/
    
swal({
/*Cargo el alert para confirmar o declinar*/
    title: "¿Eliminar?",
    text: "¿Está seguro de eliminar este Registro?",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: "#DD6B55",
    confirmButtonText: "Si!, Borrarlo!",
    cancelButtonText: "No, Cancelar!",
    closeOnConfirm: false,
    closeOnCancel: false,
    showLoaderOnConfirm: true
    },

    function(isConfirm){
    if (isConfirm) {

/*si confirmo ubico la id en las cookies*/
        var lista = document.cookie.split(";");
         for (i in lista) {
             var busca = lista[i].search("elimi");
             if (busca > -1) {micookie=lista[i]}
             }
         

/*Asigno la id a una variable*/
    valorCaja2 = eli;
/*paso la id para su eliminacion via ajax*/

        $.ajax({    //AgenteAduanal.destroy O exportador/AgenteAduanal/{AgenteAduanal}
        url: '/IntranetCoordinador/EliminarRegistroFactura',
        type: 'GET',

            data: {
                   
                    'id': valorCaja2

                },
        })
        .done(function(data) {

            //console.log();
            //var valor= data.valor_caso;

            //alert(valor);
            console.log(data);
       /* for (var i = 0; i < data.length; i++) {

            var valor = data.valor_caso;
            alert(data[i]['valor_caso']);

        }*/

       // var datas=data;
       // alert(datas);


            /*$.each(data, function(index, val) {
                alert(val);
            });*/

            if (data == 1) {
                swal('Eliminado !', 'Eliminado Exitosamente', 'success',{ button: "Ok!",});
                $("#"+idtabla).load(" #"+idtabla);
                //location.reload();
                //setTimeout('document.location.reload()',1000);
            }
         




        })
        .fail(function(jqXHR, textStatus, thrownError) {
            errorAjax(jqXHR,textStatus)

        })

    } else {
    swal("Cancelado", "No se ha procesado la eliminación", "warning");   } });
}

</script>
@stop
