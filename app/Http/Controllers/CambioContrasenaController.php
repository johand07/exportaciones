<?php

namespace App\Http\Controllers;

use App\Models\GenUsuario;
use App\Http\Requests\CambioContrasenaRequest;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Http\Request;
use Session;
use Auth;
use Laracasth\Flash\Flash;
use Alert;


class CambioContrasenaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      
       
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //return view('CambioContrasena.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CambioContrasenaRequest $request)
    {
        //dd($request->all());
        

    
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\GenUsuario  $genUsuario
     * @return \Illuminate\Http\Response
     */
    public function show(GenUsuario $genUsuario)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\GenUsuario  $genUsuario
     * @return \Illuminate\Http\Response
     */
    public function edit(GenUsuario $genUsuario,$id)
    { 

        
        $titulo= 'Cambio de Contraseña';
        $descripcion= 'Actualización de Cambio Contraseña';
        $user=GenUsuario::where('id',$id)->select('email','id')->first();
      
        return view('CambioContrasena.edit',compact('titulo','descripcion','user','id'));
    
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\GenUsuario  $genUsuario
     * @return \Illuminate\Http\Response
     */
    public function update(CambioContrasenaRequest $request,$id)
    {
        
        $user= GenUsuario::find($id);
        $user->id=$id;
        $user->email=$request->email;
        $user->password=bcrypt($request->password);
        $user->save();
        Alert::success('','ACTUALIZACIÓN EXITOSA.')->persistent("Ok");
        return redirect('/home');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\GenUsuario  $genUsuario
     * @return \Illuminate\Http\Response
     */
    public function destroy(GenUsuario $genUsuario)
    {
        //
    }
}
