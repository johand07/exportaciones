@extends('templates/layoutlte_admin_resgu_cna')
@section('content')
  <div class="row">
    <div class="col-md-12">
      <div class="col-md-3"></div>
      <div class="col-md-6">
        {!! Form::open(['route'=>'userManagerResgCna.store', 'method'=>'POST','id'=>'formadminusuarioResguardo']) !!}

        <div class="form-group">
          {!! Form::label('', 'Nombre') !!}
          {!! Form::text('nombre_funcionario', null, ['class' => 'form-control','id'=>'nombre_funcionario']) !!}
        </div>

        <div class="form-group">
          {!! Form::label('', 'Apellido') !!}
          {!! Form::text('apellido_funcionario', null, ['class' => 'form-control','id'=>'apellido_funcionario']) !!}
        </div>

        <div class="form-group">
          {!! Form::label('', 'Cédula') !!}
          {!! Form::text('cedula_funcionario', null, ['class' => 'form-control','id'=>'cedula_funcionario']) !!}
        </div>
        <div class="form-group">
          {!! Form::label('', 'Rango') !!}
          {!! Form::text('rango_funcionario', null, ['class' => 'form-control','id'=>'rango_funcionario']) !!}
        </div>
        <div class="form-group">
          {!! Form::label('', 'Unidad') !!}
          {!! Form::text('unidad', null, ['class' => 'form-control','id'=>'unidad']) !!}
        </div>
        <div class="form-group">
          {!! Form::label('','Ubicación')!!}
          {!! Form::select('gen_aduana_salida_id',$AduanaSalida,null,['class'=>'form-control','id'=>'gen_aduana_salida_id',
          'placeholder'=>'Seleccione aduana','onchange'=>'valorSelect()', 'required'=>'true']) !!}
        </div>
        <div class="form-group">
          {!! Form::label('', 'Teléfono') !!}
          {!! Form::text('tlf_funcionario', null, ['class' => 'form-control','id'=>'tlf_funcionario']) !!}
        </div>

        <div class="form-group">
          {!! Form::label('', 'Email principal') !!}
          {!! Form::text('email', null, ['class' => 'form-control','id'=>'email']) !!}
        </div>
        <div class="form-group">
          {!! Form::label('', 'Email secundario') !!}
          {!! Form::text('email_seg', null, ['class' => 'form-control','id'=>'email_seg']) !!}
        </div>
        <div class="form-group">
          {!! Form::label('','Tipo de Usuario') !!}
          {!! Form::select('gen_tipo_usuario_id',$tipousu,null,['class'=>'form-control',
          'placeholder'=>'Seleccione el Tipo de Usuario','id'=>'gen_tipo_usuario_id']) !!}
        </div>
        
        <div class="form-group">
          {!! Form::label('','Estatus') !!}
          {!! Form::select('gen_status_id',$statususu,null,['class'=>'form-control',

          'placeholder'=>'Seleccione un estatus','id'=>'gen_status_id']) !!}

          <div class="form-group">
            {!! Form::label('', 'Contraseña') !!}<br>
            {!! Form::password('password',['class' => 'form-control','id'=>'password']) !!}
          </div>

          <div class="form-group">
            {!! Form::label('', 'Confirmar Contraseña') !!}<br>
            {!! Form::password('password_confirm',['class' => 'form-control','id'=>'password_confirm']) !!}
          </div>
          <div class="form-group" >

            <div id="lista_preg">
              {!! Form::label('', 'Pregunta de Seguridad') !!}
              {!! Form::select('cat_preguntas_seg_id',$preguntas, null, ['class' => 'form-control','placeholder'=>'Seleccione una pregunta','id'=>'preguntas_seg','onchange'=>'preguntaSeg ()']) !!}
            </div>

            <div class="form-group" id="preg_alternativa" style="display: none;">
              {!! Form::label('', 'Pregunta Alternativa') !!}
              {!! Form::text('pregunta_alter',null,['class'=>'form-control','id'=>'pregunta_alter']) !!}
              <span style="color:#3097D1;cursor:pointer;" id="boton">lista de preguntas</span>
            </div>
          </div>
          <div class="form-group">
            {!! Form::label('respuesta_p1', 'Respuesta') !!}
            {!! Form::text('respuesta_seg', null, ['class' => 'form-control','id'=>'respuesta_seg','onkeypress'=>'return soloNumerosyLetras(event)']) !!}
          </div>

          <div class="form-group text-center">
            <a class="btn btn-primary" href="{{route('userManagerResgCna.index')}}">Cancelar</a>
            <input class="btn btn-success" type="submit" name="" value="Enviar" onclick="enviaradminusuarioResguardo()">
          </div>
          {!! Form::close() !!}
        </div>
        <div class="col-md-3" ></div>
      </div>
    </div>
  </div>
@stop