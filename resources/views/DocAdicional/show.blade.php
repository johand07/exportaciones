@extends('templates/layoutlte')
@section('content')
<div class="content">
<div class="panel-group" style="background-color: #fff;">
      <div class="panel-primary">
        <div class="panel-heading"> <h3>9. Documentos Adicionales</h3></div>
        <div class="panel-body">
        <h5 style="color: red">Formatos permitidos .doc, .docx, .pdf, .pptx, .ppt, .xls, .xlsx, .zip, .png, .jpg, ,jpeg</h5>

                  <div class="row" style="background-color: #fff">
                      <div class="col-md-12">

                          <div class="row">
                             
                              <div class="col-md-12">
                                  <table border="1" class="col-md-12">
                                    <tbody>
                                      <tr>
                                        <td class="text-center" valign="top" >
                                            
                                          @if(file_exists(substr($planilla7->file_1, 1)))
                                            @if(isset($extencion_file_1) && $extencion_file_1 == 'pdf')
                                            <span class="text-success">Documento 1</span>
                                            <br>
                                             <embed id="fred" src="{{asset($planilla7->file_1)}}" width="800" height="600" type="application/pdf">
                                            @elseif(isset($extencion_file_1) && ($extencion_file_1 == 'docx' || $extencion_file_1 == 'doc' || $extencion_file_1 == 'xlsx' || $extencion_file_1 == 'xls' || $extencion_file_1 == 'pptx' || $extencion_file_1 == 'ppt' || $extencion_file_1 == 'zip'))
                                              <br><br>
                                              <a href="{{asset($planilla7->file_1)}}" class="btn btn-lg btn-primary"><span><i class="glyphicon glyphicon-cloud-download"></i> </span>Descargar documento 1</a>
                                              <br><br>
                                            @elseif(isset($extencion_file_1) && ($extencion_file_1 == 'png' || $extencion_file_1 == 'jpg' || $extencion_file_1 == 'jpeg'))
                                              <img src="{{asset($planilla7->file_1)}}" alt="Imagen 1" width="300px" style="border-radius: 5px;">
                                            @else
                                              .
                                            @endif
                                            @else
                                            <br>
                                            <div class="alert alert-warning" style="color:#fff"><strong>Atención!</strong> No es posible ubicar este fichero, por favor comunicarse con el administrador de sistemas.</div>
                                            @endif
                                            
                                            @if(file_exists(substr($planilla7->file_2, 1)))
                                            @if(isset($extencion_file_2) && $extencion_file_2 == 'pdf')
                                            <span class="text-success">Documento 2</span>
                                            <br>
                                           <embed id="fred" src="{{asset($planilla7->file_2)}}" width="800" height="600" type="application/pdf">
                                            @elseif(isset($extencion_file_2) && ($extencion_file_2 == 'docx' || $extencion_file_2 == 'doc' || $extencion_file_2 == 'xlsx' || $extencion_file_2 == 'xls' || $extencion_file_2 == 'pptx' || $extencion_file_2 == 'ppt' || $extencion_file_2 == 'zip'))
                                              <br><br>
                                              <a href="{{asset($planilla7->file_2)}}" class="btn btn-lg btn-primary"><span><i class="glyphicon glyphicon-cloud-download"></i> </span>Descargar documento 2</a>
                                              <br><br>
                                              @elseif(isset($extencion_file_2) && ($extencion_file_2 == 'png' || $extencion_file_2 == 'jpg' || $extencion_file_2 == 'jpeg'))
                                              <img src="{{asset($planilla7->file_2)}}" alt="Imagen 2" width="300px" style="border-radius: 5px;">

                                            @else
                                              .
                                            @endif
                                            @else
                                            <br>
                                            <div class="alert alert-warning" style="color:#fff"><strong>Atención!</strong> No es posible ubicar este fichero, por favor comunicarse con el administrador de sistemas.</div>
                                            @endif

                                            @if(file_exists(substr($planilla7->file_3, 1)))
                                            @if(isset($extencion_file_3) && $extencion_file_3 == 'pdf')
                                            <span class="text-success">Documento 3</span>
                                            <br>
                                            <embed id="fred" src="{{asset($planilla7->file_3)}}" width="800" height="600" type="application/pdf">
                                            @elseif(isset($extencion_file_3) && ($extencion_file_3 == 'docx' || $extencion_file_3 == 'doc' || $extencion_file_3 == 'xlsx' || $extencion_file_3 == 'xls' || $extencion_file_3 == 'pptx' || $extencion_file_3 == 'ppt' || $extencion_file_3 == 'zip'))
                                              <br><br>
                                              <a href="{{asset($planilla7->file_3)}}" class="btn btn-lg btn-primary"><span><i class="glyphicon glyphicon-cloud-download"></i> </span>Descargar documento 3</a>
                                              <br><br>
                                              @elseif(isset($extencion_file_3) && ($extencion_file_3 == 'png' || $extencion_file_3 == 'jpg' || $extencion_file_3 == 'jpeg'))
                                              <img src="{{asset($planilla7->file_3)}}" alt="Imagen 3" width="300px" style="border-radius: 5px;">

                                            @else
                                              .
                                            @endif
                                            @else
                                            <br>
                                            <div class="alert alert-warning" style="color:#fff"><strong>Atención!</strong> No es posible ubicar este fichero, por favor comunicarse con el administrador de sistemas.</div>
                                            @endif

                                            @if(file_exists(substr($planilla7->file_4, 1)))
                                            @if(isset($extencion_file_4) && $extencion_file_4 == 'pdf')
                                            <span class="text-success">Documento 4</span>
                                            <br>
                                           <embed id="fred" src="{{asset($planilla7->file_4)}}" width="800" height="600" type="application/pdf">
                                            @elseif(isset($extencion_file_4) && ($extencion_file_4 == 'docx' || $extencion_file_4 == 'doc' || $extencion_file_4 == 'xlsx' || $extencion_file_4 == 'xls' || $extencion_file_4 == 'pptx' || $extencion_file_4 == 'ppt' || $extencion_file_4 == 'zip'))
                                              <br><br>
                                              <a href="{{asset($planilla7->file_4)}}" class="btn btn-lg btn-primary"><span><i class="glyphicon glyphicon-cloud-download"></i> </span>Descargar documento 4</a>
                                              <br><br>
                                              @elseif(isset($extencion_file_4) && ($extencion_file_4 == 'png' || $extencion_file_4 == 'jpg' || $extencion_file_4 == 'jpeg'))
                                              <img src="{{asset($planilla7->file_4)}}" alt="Imagen 4" width="300px" style="border-radius: 5px;">

                                            @else
                                              .
                                            @endif
                                            @else
                                            <br>
                                            <div class="alert alert-warning" style="color:#fff"><strong>Atención!</strong> No es posible ubicar este fichero, por favor comunicarse con el administrador de sistemas.</div>
                                            @endif

                                            @if(file_exists(substr($planilla7->file_5, 1)))
                                            @if(isset($extencion_file_5) && $extencion_file_5 == 'pdf' )
                                            <span class="text-success">Documento 5</span>
                                            <br>
                                           <embed id="fred" src="{{asset($planilla7->file_5)}}" width="800" height="600" type="application/pdf">
                                            @elseif(isset($extencion_file_5) && ($extencion_file_5 == 'docx' || $extencion_file_5 == 'doc' || $extencion_file_5 == 'xlsx' || $extencion_file_5 == 'xls' || $extencion_file_5 == 'pptx' || $extencion_file_5 == 'ppt' || $extencion_file_5 == 'zip'))
                                              <br><br>
                                              <a href="{{asset($planilla7->file_5)}}" class="btn btn-lg btn-primary"><span><i class="glyphicon glyphicon-cloud-download"></i> </span>Descargar documento 5</a>
                                              <br><br>
                                              @elseif(isset($extencion_file_5) && ($extencion_file_5 == 'png' || $extencion_file_5 == 'jpg' || $extencion_file_5 == 'jpeg'))
                                              <img src="{{asset($planilla7->file_5)}}" alt="Imagen 5" width="300px" style="border-radius: 5px;">

                                            @else
                                              .
                                            @endif
                                            @else
                                            <br>
                                            <div class="alert alert-warning" style="color:#fff"><strong>Atención!</strong> No es posible ubicar este fichero, por favor comunicarse con el administrador de sistemas.</div>
                                            @endif

                                            @if(file_exists(substr($planilla7->file_6, 1)))
                                            @if(isset($extencion_file_6) && $extencion_file_6 == 'pdf')
                                            <span class="text-success">Documento 6</span>
                                            <br>
                                           <embed id="fred" src="{{asset($planilla7->file_6)}}" width="800" height="600" type="application/pdf">
                                            @elseif(isset($extencion_file_6) && ($extencion_file_6 == 'docx' || $extencion_file_6 == 'doc' || $extencion_file_6 == 'xlsx' || $extencion_file_6 == 'xls' || $extencion_file_6 == 'pptx' || $extencion_file_6 == 'ppt' || $extencion_file_6 == 'zip'))
                                              <br><br>
                                              <a href="{{asset($planilla7->file_6)}}" class="btn btn-lg btn-primary"><span><i class="glyphicon glyphicon-cloud-download"></i> </span>Descargar documento 6</a>
                                              <br><br>
                                              @elseif(isset($extencion_file_6) && ($extencion_file_6 == 'png' || $extencion_file_6 == 'jpg' || $extencion_file_6 == 'jpeg'))
                                              <img src="{{asset($planilla7->file_6)}}" alt="Imagen 6" width="300px" style="border-radius: 5px;">

                                            @else
                                              .
                                            @endif
                                            @else
                                            <br>
                                            <div class="alert alert-warning" style="color:#fff"><strong>Atención!</strong> No es posible ubicar este fichero, por favor comunicarse con el administrador de sistemas.</div>
                                            @endif

                                              
                                        </td>
                                      </tr>
                                    </tbody>
                                  </table>
                              </div>
                          </div>             



                      </div>
                  </div>

        </div>
      </div>
    </div>

  </div><!-- Fin content -->

<!--script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script-->


@endsection