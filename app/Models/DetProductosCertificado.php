<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DetProductosCertificado extends Model
{
    protected $table = 'det_productos_certificado';
    protected $fillable = [

           'id',
           'gen_certificado_id',
           'num_orden',
           'codigo_arancel',
           'descripcion_comercial',
           'denominacion_mercancia',
           'unidad_fisica',
           'valor_fob',
           'cantidad_segun_factura',
           'bactivo',
    ];

    protected $date =['created_at','updated_at'];

    public function GenCertificado() {

     return $this->hasOne('App\Models\GenCertificado','id','gen_certificado_id');

    }
    public function DetDeclaracionCertificado() {

        return $this->hasOne('App\Models\DetDeclaracionCertificado','gen_certificado_id','gen_certificado_id');
   
    }
}
