@extends('templates/layoutlte_analista_cvc')
@section('content')
<div class="content"  style="background-color: #fff">
  {{Form::model($solicitud_cvc,['route'=>['ListaAnalistCvc.update',$solicitud_cvc->id],'method'=>'PATCH','id'=>'formanalistaCvc'])}}
  <div class="panel-group" id="datoscvc">
    <div class="panel-primary">
      <div class="panel-heading">
        <div class="row">
          <div class="col-md-8"><h3>ANÁLISIS DE SOLICITUD DE CVC</h3></div>
          <div class="col-md-2"><!-- Traerme la consulta con el numero de solicitud-->
            <h4><b>Nº Solicitud:</b></h4>
            {{ $solicitud_cvc->num_sol_cvc}}
          </div>
          <div class="col-md-2"><!-- Traerme la consulta con la fecha de solicitud-->
            <h4><b>Fecha Solicitud:</b></h4>
            {{ $solicitud_cvc->created_at}}
          </div>
        </div>
      </div>
      <br><br>
  <div class="row">
    <div class="col-md-12">
      <div class="col-md-4">
        <div class="form-group">
           <h4><b>Exportador (Nombre)</b></h4>
              {{$solicitud_cvc->DetUsuario ? $solicitud_cvc->DetUsuario->razon_social : ''}}
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group">
           <h4><b>Dirección para Notificaciones</b></h4>
              {{$solicitud_cvc->direccion_notif}}
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group">
           <h4><b>Clave del país</b></h4>
              {{$solicitud_cvc->clave_pais}}
        </div>
      </div>
    </div>
  </div>
  <br><br>
  <div class="row">
    <div class="col-md-12">
      <div class="col-md-4">
        <div class="form-group">
           <h4><b>Puerto de Embarque</b></h4>
              {{$solicitud_cvc->rAduanaSalida ? $solicitud_cvc->rAduanaSalida->daduana :'' }}
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group">
           <h4><b>Número de Serie</b></h4>
              {{$solicitud_cvc->num_serie}}
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group">
           <h4><b>País Productor</b></h4>
            {{$solicitud_cvc->paisProductor ? $solicitud_cvc->paisProductor->dpais : '' }}
        </div>
      </div>
    </div>
  </div>
  <br><br>
  <div class="row">
    <div class="col-md-12">
      <div class="col-md-4">
        <div class="form-group">
           <h4><b>País de Destino</b></h4>
            {{$solicitud_cvc->paisDestino ? $solicitud_cvc->paisDestino->dpais : ''}}
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group">
           <h4><b>Fecha de Exportación</b></h4>
              {{$solicitud_cvc->fexportacion}}
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group">
           <h4><b>País de Trasbordo</b></h4>
            {{$solicitud_cvc->paisTrasbordo ?  $solicitud_cvc->paisTrasbordo->dpais : ''}}
        </div>
      </div>
    </div>
  </div>
  <br><br>

  <div class="row">
    <div class="col-md-12">
      <div class="col-md-4">
        <div class="form-group">
           <h4><b>Nombre del Medio de Transporte</b></h4>
              {{$solicitud_cvc->nombre_transporte}}
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group">
           <h4><b>Marca de Identificación</b></h4>
              {{$solicitud_cvc->marca_ident}}
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group">
           <h4><b>Cargados</b></h4>
           <table class="table table-responsive table-bordered">
             <tr><br>
               <td>
              @if(isset($solicitud_cvc->cat_cargados_id) && $solicitud_cvc->cat_cargados_id == 1)
                   <div>En sacos</div>
                  @elseif(isset($solicitud_cvc->cat_cargados_id) && $solicitud_cvc->cat_cargados_id == 2)
                   <div>A granel</div>
                   @else
                   <div>En contenedor</div>
              @endif
               </td>
             </tr>
           </table>
        </div>
      </div>
    </div>
  </div>
  <br><br>
  <div class="row">
    <div class="col-md-12">
      <div class="col-md-4">
        <div class="form-group">
           <h4><b>Peso Neto de la Partida</b></h4>
              {{$solicitud_cvc->peso_neto}}
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group">
           <h4><b>Unidad de Peso</b></h4>
          {{$solicitud_cvc->unidadMedida ? $solicitud_cvc->unidadMedida->dunidad :'' }}
        </div>
      </div>
    </div>
  </div>
  <br><br>
  <div class="row">
    <div class="panel-primary">
      <div class="panel-heading">
        <h4><b>Descripción del café (Formato, tipo, si procede)</b></h4>
      </div>
      <table class="table table-responsive table-bordered">
        <tr><br><br>
          <td>
        @if(isset($solicitud_cvc->cat_tipo_cafe_id) && $solicitud_cvc->cat_tipo_cafe_id == 1)
          <div>Arábica verde</div>

        @elseif(isset($solicitud_cvc->cat_tipo_cafe_id) && $solicitud_cvc->cat_tipo_cafe_id == 2)
          <div>Robusta verde</div>

        @elseif(isset($solicitud_cvc->cat_tipo_cafe_id) && $solicitud_cvc->cat_tipo_cafe_id == 3)
          <div>Tostado</div>

        @elseif(isset($solicitud_cvc->cat_tipo_cafe_id) && $solicitud_cvc->cat_tipo_cafe_id == 4)
          <div>Soluble</div>
        @elseif(isset($solicitud_cvc->cat_tipo_cafe_id) && $solicitud_cvc->cat_tipo_cafe_id == 5)
          <div>Líquido</div>
        @else
          <div>Otros</div>
        @endif
          </td>
        </tr>
      </table>
    </div>
  </div>
<br><br>
<!---->
  <div class="row">
    <div class="panel-primary">
      <div class="panel-heading">
        <h4><b>Método de elaboración</b></h4>
      </div>
           <table class="table table-responsive table-bordered">
        <tr><br><br>
          <td>
        @if(isset($solicitud_cvc->cat_metodo_elab_id) && $solicitud_cvc->cat_metodo_elab_id == 1)
            <div>Descafeinado</div>

        @elseif(isset($solicitud_cvc->cat_metodo_elab_id) && $solicitud_cvc->cat_metodo_elab_id == 2)
            <div>Orgánico</div>

        @elseif(isset($solicitud_cvc->cat_metodo_elab_id) && $solicitud_cvc->cat_metodo_elab_id == 3)
            <div>Certificado</div>

       @elseif(isset($solicitud_cvc->cat_metodo_elab_id) && $solicitud_cvc->cat_metodo_elab_id == 4)
      
            <div>Nº Certificado</div>

      @elseif(isset($solicitud_cvc->cat_metodo_elab_id) && $solicitud_cvc->cat_metodo_elab_id == 5)
      
            <div>Café Verde</div>
         
      @elseif(isset($solicitud_cvc->cat_metodo_elab_id) && $solicitud_cvc->cat_metodo_elab_id == 6)

          <div>Vía Seca</div>

      @elseif(isset($solicitud_cvc->cat_metodo_elab_id) && $solicitud_cvc->cat_metodo_elab_id == 7)
         
            <div>Vía Humeda</div>
         
      @elseif(isset($solicitud_cvc->cat_metodo_elab_id) && $solicitud_cvc->cat_metodo_elab_id == 8)
            <div>Café Soluble</div>
         
      @elseif(isset($solicitud_cvc->cat_metodo_elab_id) && $solicitud_cvc->cat_metodo_elab_id == 9)

            <div>Centrifugado</div>
        @else
       
            <div>Liofilizado</div>
        @endif
          </td>
        </tr>
      </table>
    </div>
  </div>
<br><br>
<!--div class="row">
    <div class="panel-default">
      <div class="panel-heading">
        <h4><b>POR EL PRESENTE SE CERTIFICA QUE EL CAFÉ ARRIBA DESCRITO FUE PRODUCIDO/BENEFICIADO EN EL PAÍS QUE SE INDICA EN LA CASILLA (PAIS PRODUCTOR) Y EXPORTADO EN LA FECHA QUE SEGUIDAMENTE SE HACE CONSTAR ESTE CERTIFICDO SE EXPIDE EXCLUSIVAMENTE PARA FINES ESTADISTICOS Y PROCEDENCIA DE ORIGEN</b></h4>
      </div>
    </div>
  </div-->
   <div class="row">
    <div class="panel-primary">
      <div class="panel-heading">
        <h4><b>Normas óptimas de calidad del café verde (ICC Resolución Número 420)</b></h4>
      </div><br>
      <div class="row">
        <div class="col-md-12">
          <table class="table table-responsive table-bordered">
            @if(isset($solicitud_cvc->norma_calidad_cafe) && $solicitud_cvc->norma_calidad_cafe == 'S')
              <tr>
                <td>"S" Plena observancia de las normas óptimas sobre defecto y humedad</td>
              </tr>


            @elseif(isset($solicitud_cvc->norma_calidad_cafe) && $solicitud_cvc->norma_calidad_cafe == 'XD')
             
              <tr>
                 <td>"XD" El café no responde a las normas óptimas sobre defectos</td>
              </tr>

            @elseif(isset($solicitud_cvc->norma_calidad_cafe) && $solicitud_cvc->norma_calidad_cafe == 'XM')
              
              <tr>
                <td>"XM" el café no responde a las normas óptimas sobre humedad</td>
              </tr>
            @else
              <tr>
                <td>"XDM" el café no responde a ninguna de las normas óptimas</td>
              </tr>
           @endif
          </table>
        </div>
      </div>
    </div>
  </div>
<br><br>
  <div class="row">
    <div class="col-md-12">
      <div class="col-md-4">
        <div class="form-group">
           <h4><b>Código Arancelario</b></h4>
          {{$solicitud_cvc->codigo_arancelario}}
        </div>
      </div>
       <div class="col-md-4">
        <div class="form-group">
           <h4><b>Características especiales (especifique)</b></h4>
          {{$solicitud_cvc->caract_especiales}}
        </div>
      </div>
       <div class="col-md-4">
        <div class="form-group">
           <h4><b>Código del Sistema Armonizado (SA)</b></h4>
          {{$solicitud_cvc->codio_sist_sa}}
        </div>
      </div>
    </div>
  </div>
<br><br>
<div class="row">
    <div class="col-md-12">
      <div class="col-md-4">
        <div class="form-group">
           <h4><b>Valor (FOB) del embarque</b></h4>
          {{$solicitud_cvc->valor_fob}}
        </div>
      </div>
       <div class="col-md-4">
        <div class="form-group">
           <h4><b>Divisas</b></h4>
          {{$solicitud_cvc->divisa ? $solicitud_cvc->divisa->ddivisa_abr :'' }}
        </div>
      </div>
       <div class="col-md-4">
        <div class="form-group">
           <h4><b>Información adicional</b></h4>
          {{$solicitud_cvc->info_adicional}}
        </div>
      </div>
    </div>
  </div>
<br><br><br>
      <!---Botonos de observacion--->
      <div class="row">
        <div class="col-md-12">
          <div class="form-group">
            <label>Estado de la Observación</label><br>
            {!!Form::select('gen_status_id',$edo_observacion,null,['placeholder'=>'Seleccione','class'=>'form-control','id'=>'gen_status_id','onchange'=>'validarobservacion ()'])!!}
          </div>
        </div>
      </div>
      <br>
      <div class="row" id="observacion" style="display:none;">
          <b><u>OBSERVACIONES:</u></b>
          <textarea rows="4"  class="form-control text-justify" name="observacion" id="observacion" style="font-weight:bold; text-transform: capitalize;" ></textarea>
      </div>
      <div class="row text-center"><br><br>
          <a class="btn btn-primary" href="{{route('ListaAnalistCvc.index')}}">Cancelar</a>
          <input type="submit" name="Guardar Observacion" class="btn btn-danger center" value="Guardar Observacion" onclick="return analisisinversion ()">  
      </div>
    </div>
    </div>
  </div>
</div>
{{Form::close()}}
@stop

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="{{asset('js/ajax_v2.1.1.js')}}"></script>

<script type="text/javascript">

$(document).ready(function() {
   $("#datoscvc").load(" #datoscvc");
});


</script>




