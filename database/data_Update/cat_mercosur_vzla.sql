/*
-- Query: SELECT * FROM exportaciones.cat_can_mercosur
LIMIT 0, 500

-- Date: 2019-08-23 01:57
*/
INSERT INTO `cat_can_mercosur` (`id`,`codigo`,`descripcion`,`bactivo`,`created_at`,`updated_at`) VALUES (1,'2a','Artículo 2°,literal a)',1,NULL,NULL);
INSERT INTO `cat_can_mercosur` (`id`,`codigo`,`descripcion`,`bactivo`,`created_at`,`updated_at`) VALUES (2,'2c','Artículo 2°,literal c)',1,NULL,NULL);
INSERT INTO `cat_can_mercosur` (`id`,`codigo`,`descripcion`,`bactivo`,`created_at`,`updated_at`) VALUES (3,'4a','Artículo 4°,literal a)',1,NULL,NULL);
INSERT INTO `cat_can_mercosur` (`id`,`codigo`,`descripcion`,`bactivo`,`created_at`,`updated_at`) VALUES (4,'4b','Artículo 4°,literal b)',1,NULL,NULL);
INSERT INTO `cat_can_mercosur` (`id`,`codigo`,`descripcion`,`bactivo`,`created_at`,`updated_at`) VALUES (5,'4c','Artículo 4°,literal c)',1,NULL,NULL);
INSERT INTO `cat_can_mercosur` (`id`,`codigo`,`descripcion`,`bactivo`,`created_at`,`updated_at`) VALUES (6,'5','Artículo 5°',1,NULL,NULL);
INSERT INTO `cat_can_mercosur` (`id`,`codigo`,`descripcion`,`bactivo`,`created_at`,`updated_at`) VALUES (7,'n','No califica Origen Venezolano para ese Acuerdo o País',1,NULL,NULL);
INSERT INTO `cat_can_mercosur` (`id`,`codigo`,`descripcion`,`bactivo`,`created_at`,`updated_at`) VALUES (8,'*','Información insuficiente para la determinación del Origen',1,NULL,NULL);
INSERT INTO `cat_can_mercosur` (`id`,`codigo`,`descripcion`,`bactivo`,`created_at`,`updated_at`) VALUES (9,'(1)','no',1,NULL,NULL);
INSERT INTO `cat_can_mercosur` (`id`,`codigo`,`descripcion`,`bactivo`,`created_at`,`updated_at`) VALUES (10,'(2)','no',1,NULL,NULL);
INSERT INTO `cat_can_mercosur` (`id`,`codigo`,`descripcion`,`bactivo`,`created_at`,`updated_at`) VALUES (11,'(3)','no',1,NULL,NULL);
