<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GenBandejaEntradaDjo extends Model
{
    protected $table = 'gen_bandeja_entrada_djo';

    protected $fillable = [

           'gen_declaracion_jo_id',
           'gen_usuario_id',
           'estado',
           'descripcion',
    ];

    public function rDeclaracionJO() {

     return $this->hasOne('App\Models\GenDeclaracionJO','id','gen_declaracion_jo_id');
     
    }
    public function rUsuario() {

     return $this->hasOne('App\Models\DetUsuario','id','gen_usuario_id');
     
    }
    public function rGenStatus() {

     return $this->hasOne('App\Models\GenStatus','id','estado');
     
    }
    
    public function rGenUsuario() {

     return $this->hasOne('App\Models\GenUsuario','id','gen_usuario_id');
     
    }
}
