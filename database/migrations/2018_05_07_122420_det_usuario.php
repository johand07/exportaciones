<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DetUsuario extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */    
    public function up()
    {
        Schema::create('det_usuario', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('cod_empresa', 120)->nullable();
            $table->unsignedInteger('gen_usuario_id')->index()->nullable();
            $table->foreign('gen_usuario_id')->references('id')->on('gen_usuario');
            $table->unsignedInteger('gen_actividad_eco_id')->index()->nullable();
            $table->foreign('gen_actividad_eco_id')->references('id')->on('gen_actividad_eco');
            $table->unsignedInteger('gen_tipo_empresa_id')->index()->nullable();
            $table->foreign('gen_tipo_empresa_id')->references('id')->on('gen_tipo_empresa');
            $table->unsignedInteger('estado_id')->index()->nullable();
            $table->foreign('estado_id')->references('id')->on('estado');
            $table->unsignedInteger('municipio_id')->index()->nullable();
            $table->foreign('municipio_id')->references('id')->on('municipio');
            $table->unsignedInteger('parroquia_id')->index()->nullable();
            $table->foreign('parroquia_id')->references('id')->on('parroquia');
            $table->unsignedInteger('circuns_judicial_id')->index()->nullable();
            $table->foreign('circuns_judicial_id')->references('id')->on('circuns_judicial');
            $table->string('razon_social', 100)->nullable();
            $table->string('siglas', 50)->nullable();   
            $table->string('rif', 11)->nullable();
            $table->string('pagina_web', 60)->nullable();
            $table->string('telefono_local', 15)->nullable();
            $table->string('telefono_movil', 15)->nullable();
            $table->string('direccion', 200)->nullable();
            $table->string('num_registro', 60)->nullable();
            $table->string('tomo', 30)->nullable();
            $table->string('folio', 30)->nullable();
            $table->string('Oficina', 60)->nullable();
            ///Se agrego campo Capital Social//////
            $table->date('f_registro_mer')->nullable();//Cambiar nombre del campo//
            //$table->string('nombre_presid', 50)->nullable();
            //$table->string('ci_presid', 20)->nullable();
            //$table->string('cargo_presid', 80)->default('Presidente')->nullable();
            $table->string('nombre_repre', 50)->nullable();
            $table->string('apellido_repre', 50)->nullable();
            $table->string('ci_repre', 20)->nullable();
            $table->string('cargo_repre', 80)->nullable();
            $table->string('correo_repre', 80)->nullable();// Se agrego nuevo este campo///
            $table->string('correo', 80)->nullable();
            $table->string('correo_sec', 80)->nullable();
            $table->string('accionista', 20)->nullable();
            $table->string('invers', 20)->nullable();
            $table->string('export', 20)->nullable();
            $table->string('produc', 20)->nullable();
            $table->string('comerc', 20)->nullable();
            //$table->string('comerc', 20)->nullable();
            $table->unsignedInteger('produc_tecno')->index()->nullable();
            $table->foreign('produc_tecno')->references('id')->on('cat_export_prod');
            $table->string('especifique_tecno', 100)->nullable();////Se agrego un campo de especificación de productos de tecnologia////////////
            $table->boolean('bactivo')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('det_usuario');
    }
}
