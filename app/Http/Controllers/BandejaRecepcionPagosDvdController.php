<?php

namespace App\Http\Controllers;

use App\Models\ReportePago;
use App\Models\GenStatus;
use App\Models\GenDVDSolicitud;
use Illuminate\Http\Request;

class BandejaRecepcionPagosDvdController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pagosrecibidos = ReportePago::where('tipo_pago_id',1)
                        ->where('gen_status_id', 20)
                            ->whereNotNull('gen_dvd_solicitud_id')
                            ->get();
        // dd($pagosrecibidos); 
        $pagosvalidados = ReportePago::where('tipo_pago_id',1)
                        ->where('gen_status_id', 21)
                            ->whereNotNull('gen_dvd_solicitud_id')
                            ->get();
        // dd($pagosvalidados);
        $pagosnovalidados = ReportePago::where('tipo_pago_id',1)
                        ->where('gen_status_id', 22)
                            ->whereNotNull('gen_dvd_solicitud_id')
                            ->get();
        // dd($pagosnovalidados);
        $fases = GenStatus::whereIn('id', [21, 22])
                ->select(
                    'id',
                    'nombre_status as nombre_fase'
                )
                ->get();

        // dd($fases);
        return view('bandejaRecepcionPagosDvd.index', compact('pagosrecibidos', 'pagosvalidados', 'pagosnovalidados', 'fases'));
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $changeStatusPago= ReportePago::where('id', $request->reporte_pago_id)->update(['gen_status_id' => $request->gen_status_id, 'updated_at' => date("Y-m-d H:i:s")]);
        if($request->gen_status_id == 21) {
            $changeStatusDvd= GenDVDSolicitud::where('id', $request->gen_dvd_solicitud_id)->update(['gen_status_id' => 3, 'updated_at' => date("Y-m-d H:i:s")]);
        } elseif ($request->gen_status_id == 22) {
            $changeStatusDvd= GenDVDSolicitud::where('id', $request->gen_dvd_solicitud_id)->update(['gen_status_id' => $request->gen_status_id, 'updated_at' => date("Y-m-d H:i:s")]);
        }
        if ($request->gen_status_id == 21) {
            
            return 1;

        } elseif ($request->gen_status_id == 22) {
            
            return 2;
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ReportePago  $reportePago
     * @return \Illuminate\Http\Response
     */
    public function show(ReportePago $reportePago)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ReportePago  $reportePago
     * @return \Illuminate\Http\Response
     */
    public function edit(ReportePago $reportePago)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ReportePago  $reportePago
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ReportePago $reportePago)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ReportePago  $reportePago
     * @return \Illuminate\Http\Response
     */
    public function destroy(ReportePago $reportePago)
    {
        //
    }
}
