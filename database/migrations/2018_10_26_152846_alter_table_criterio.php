<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableCriterio extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::table('criterio_analisis', function (Blueprint $table) {
            //
            $table->string('arancel_nan')->nullable()->after('det_declaracion_produc_id');
            $table->string('arancel_mer')->nullable()->after('det_declaracion_produc_id');
            $table->string('arancel_descrip_adicional')->nullable()->after('arancel_nan');
            

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::table('criterio_analisis', function (Blueprint $table) {
            //
            $table->dropColumn('arancel_nan');
            $table->dropColumn('arancel_mer');
             $table->dropColumn('arancel_descrip_adicional');
        });
    }
}
