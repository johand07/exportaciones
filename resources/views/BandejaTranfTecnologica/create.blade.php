@extends('templates/layoutlte')

@section('content')
	<div class="panel panel-primary">
		<div class="panel-heading">Contrato de Transferencia Tecnológica</div>
		<div class="panel-body">
			{{Form::open(['route'=>'BandejaTranfTecnologica.store','method'=>'POST','id'=>''])}}
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
						    {!! Form::label('','Identificación del Trámite por Realizar')!!}
						    {!! Form::text('',null,['class'=>'form-control','id'=>'']) !!}
						</div>
					</div>

					<div class="col-md-6">
						<div class="form-group">
						 	{!! Form::label('','Número de Certificado')!!}
						    {!! Form::text('',null,['class'=>'form-control','id'=>'']) !!}
						</div>
					</div>
				</div>

				<div class="panel panel-primary">
	                <div class="panel-heading">Datos del Sujeto Proveedor de Tecnológia</div>
	              </div>
	              <div class="row">
	                <div class="col-md-4">
							<div class="form-group">
					    		{!! Form::label('','Nombre')!!}
					      		{!! Form::text('',null,['class'=>'form-control','id'=>'']) !!}
							</div>
						</div>

						<div class="col-md-4">
							<div class="form-group">
					    		{!! Form::label('','Apellido')!!}
					      		{!! Form::text('',null,['class'=>'form-control','id'=>'']) !!}
							</div>
						</div>

						<div class="col-md-4">
							<div class="form-group">
					    		{!! Form::label('','Rif')!!}
					      		{!! Form::text('',null,['class'=>'form-control','id'=>'']) !!}
							</div>
						</div>

						<div class="col-md-4">
							<div class="form-group">
					    		{!! Form::label('','Correo')!!}
					      		{!! Form::text('',null,['class'=>'form-control','id'=>'']) !!}
							</div>
						</div>

						<div class="col-md-4">
							<div class="form-group">
					    		{!! Form::label('','Direccion')!!}
					      		{!! Form::text('',null,['class'=>'form-control','id'=>'']) !!}
							</div>
						</div>

						<div class="col-md-4">
							<div class="form-group">
					    		{!! Form::label('','Otro')!!}
					      		{!! Form::text('',null,['class'=>'form-control','id'=>'']) !!}
							</div>
						</div>
	               </div>


				<div class="panel panel-primary">
	                <div class="panel-heading">Datos del Sujeto Receptor de Tecnológia</div>
	              </div>
	              <div class="row">
	                <div class="col-md-4">
							<div class="form-group">
					    		{!! Form::label('','Tipo de Empresa')!!}
					      		{!! Form::text('',null,['class'=>'form-control','id'=>'']) !!}
							</div>
						</div>

						<div class="col-md-4">
							<div class="form-group">
					    		{!! Form::label('','Razon Social del Sujeto Receptor')!!}
					      		{!! Form::text('',null,['class'=>'form-control','id'=>'']) !!}
							</div>
						</div>

						<div class="col-md-4">
							<div class="form-group">
					    		{!! Form::label('','Rif del Sujeto Receptor de Tecnológia')!!}
					      		{!! Form::text('',null,['class'=>'form-control','id'=>'']) !!}
							</div>
						</div>

						<div class="col-md-6">
							<div class="form-group">
						    	{!! Form::label('','Fecha de Inscripción') !!} 
						    	<div class="input-group date">
						    		{!! Form::text('',null,['class'=>'form-control','readonly'=>'readonly','id'=>'fecha_emision']) !!}
						    		<span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
			    				</div>	              
			   				</div>
						</div>

						<div class="col-md-6">
							<div class="form-group">
						    	{!! Form::label('','Fecha de Vencimiento') !!} 
						    	<div class="input-group date">
						    		{!! Form::text('',null,['class'=>'form-control','readonly'=>'readonly','id'=>'fecha_emision']) !!}
						    		<span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
			    				</div>	              
			   				</div>
						</div>
	               </div>
	               <br>
				<div class="panel panel-primary">
					<div class="panel-heading">Dirección Fiscal del Sujeto Receptor</div>
				</div>
				<div class="row">
					<div class="col-md-4">
					<div class="form-group">
		    			{!! Form::label('','Estado')!!}
		      			{!! Form::text('',null,['class'=>'form-control','id'=>'']) !!}
					</div>
					</div>

					<div class="col-md-4">
						<div class="form-group">
	    					{!! Form::label('','Municipio')!!}
	      					{!! Form::text('',null,['class'=>'form-control','id'=>'']) !!}
						</div>
					</div>

					<div class="col-md-4">
						<div class="form-group">
		    				{!! Form::label('','Parroquia')!!}
		      				{!! Form::text('',null,['class'=>'form-control','id'=>'']) !!}
						</div>
					</div>

					<div class="col-md-4">
						<div class="form-group">
		    				{!! Form::label('','Dirección Fiscal')!!}
		      				{!! Form::text('',null,['class'=>'form-control','id'=>'']) !!}
						</div>
					</div>

					<div class="col-md-4">
						<div class="form-group">
		    				{!! Form::label('','Zona Postal')!!}
		      				{!! Form::text('',null,['class'=>'form-control','id'=>'']) !!}
						</div>
					</div>

					<div class="col-md-4">
						<div class="form-group">
		    				{!! Form::label('','Telefono de Dirección Fiscal')!!}
		      				{!! Form::text('',null,['class'=>'form-control','id'=>'']) !!}
						</div>
					</div>

					<div class="col-md-4">
						<div class="form-group">
		    				{!! Form::label('','Fax')!!}
		      				{!! Form::text('',null,['class'=>'form-control','id'=>'']) !!}
						</div>
					</div>

					<div class="col-md-4">
						<div class="form-group">
		    				{!! Form::label('','Correo Electronico')!!}
		      				{!! Form::text('',null,['class'=>'form-control','id'=>'']) !!}
						</div>
					</div>

					<div class="col-md-4">
						<div class="form-group">
		    				{!! Form::label('','Pagina Web')!!}
		      				{!! Form::text('',null,['class'=>'form-control','id'=>'']) !!}
						</div>
					</div>
				</div>
				<div class="panel panel-primary">
					<div class="panel-heading">Términos del Contrato</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<div class="form-group">
					    	{!! Form::label('','Fecha de Celebración') !!} 
					    	<div class="input-group date">
					    		{!! Form::text('fcelebracion_contrato',null,['class'=>'form-control','readonly'=>'readonly','id'=>'fecha_emision']) !!}
					    		<span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
		    				</div>	              
		   				</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
					    	{!! Form::label('','Fecha de Inicio del Contrato') !!} 
					    	<div class="input-group date">
					    		{!! Form::text('finicio_contrato',null,['class'=>'form-control','readonly'=>'readonly','id'=>'fecha_emision']) !!}
					    		<span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
		    				</div>	              
		   				</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
					    	{!! Form::label('','Fecha Fin Del Contrato') !!} 
					    	<div class="input-group date">
					    		{!! Form::text('fvencimiento_contrato',null,['class'=>'form-control','readonly'=>'readonly','id'=>'fecha_emision']) !!}
					    		<span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
		    				</div>	              
		   				</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
		    				{!! Form::label('','Tipo de Contrato')!!}
		      				{!! Form::text('cat_tipo_contrato_id',null,['class'=>'form-control','id'=>'']) !!}
						</div>
					</div>

					<div class="col-md-4">
						<div class="form-group">
		    				{!! Form::label('','Objeto del Contrato')!!}
		      				{!! Form::text('objeto_contrato',null,['class'=>'form-control','id'=>'']) !!}
						</div>
					</div>

					<div class="col-md-4">
						<div class="form-group">
		    				{!! Form::label('','Monto del Contrato')!!}
		      				{!! Form::text('monto_contrato',null,['class'=>'form-control','id'=>'']) !!}
						</div>
					</div>

					<div class="col-md-4">
						<div class="form-group">
		    				{!! Form::label('','Divisa')!!}
		      				{!! Form::text('gen_divisa_id',null,['class'=>'form-control','id'=>'']) !!}
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
		    				{!! Form::label('','Firma del Representante Legal o Apoderado')!!}
		      				{!! Form::text('',null,['class'=>'form-control','id'=>'']) !!}
						</div>
					</div>

					<div class="col-md-4">
						<div class="form-group">
		    				{!! Form::label('','Lugar y Fecha')!!}
		      				{!! Form::text('',null,['class'=>'form-control','id'=>'']) !!}
						</div>
					</div>

				</div>

				<div class="row">
	              <div class="col-md-12">
	                <div class="col-md-4"></div>
	                <div class="col-md-4 text-center">
	                  <div class="form-group">
						<a href="{{url('exportador/BandejaTranfTecnologica')}}" class="btn btn-success">Cancelar</a>
	                    <button class="btn btn-primary" href="#" id="Transferencia">Registrar</button><!--Boton siguiente del Paso 1 del Formulario-->
	                  </div>
	                </div>
	              </div>

	            </div>
			{{Form::close()}}
		</div>
    </div>
@stop