<?php

namespace App\Http\Controllers;
use Illuminate\Filesystem\Filesystem;
use App\Models\GenUsuario;
use App\Models\GenJuridicoCencoex;
use App\Models\GenJuridicoSeniat;
use App\Models\DetUsuario;
use App\Models\Pais;
use App\Models\Estado;
use App\Models\Municipio;
use App\Models\Parroquia;
use App\Models\GenSector;
use App\Models\GenActividadEco;
use App\Models\GenTipoEmpresa;
use App\Models\CircunsJudicial;
use App\Models\CatPreguntaSeg;
use App\Models\GenArancelNandina;
use App\Models\GenArancelMercosur;
use App\Models\Accionistas;
use App\Models\Productos;
use App\Models\GenUnidadMedida;
use App\Models\CatTipoUsuario;
use App\Models\Permiso;
use App\Models\CasaMatriz;
use App\Models\UltimaExport;
use App\Models\CatExportProd;
use App\Models\HistSesionUser;
use App\Http\Requests\DetUsuarioRequest;
use File;
use Session;
use Auth;
use Laracasts\Flash\Flash;
use Alert;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

use \App\Mail\welcome as welcomeEmail;


//use \App\Mail\welcome as welcomeEmail;
//use Illuminate\Mail\Message;
use \Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class SesionController extends Controller
{
    public function __construct()
    {
        // Filtrar todos los métodos
        //$this->middleware('Sesion');
    }

    protected function index()
    {

        return view('sesion.login');
    }

    protected function create()
    {

        return  view('sesion.register');

    }

    protected function refreshCaptcha()
    {
        return  response()->json(['captcha'=>captcha_img('flat')]);

    }


    protected function RifValido(Request $request)
    {
        //validamos el captcha
        //dd($request->all());
       if(!Session::has('bandera')){

          $this->validate($request,[
          'captcha' => 'required|captcha',
        ]);

          $actividad_eco=array();
          $municipios=array();
          $parroquias=array();

    }else { $request->rif=Session::get('bandera_rif');
            $request->select_tipoPersona=Session::get('bandera_persona');
            $request->tipoUsu=Session::get('bandera_tipo_persona');
            $actividad_eco=GenActividadEco::all()->pluck('dactividad','id');
            $municipios=Municipio::all()->pluck('municipio','id');
            $parroquias=Parroquia::all()->pluck('parroquia','id');

    }


      $rif_seniat=$request->select_tipoPersona.$request->rif;

      $rif_conca=$request->select_tipoPersona.'-'.$request->rif;


      //$rif_accionista=$request->cedula_accionista.$request->rif;

        if($request->select_tipoPersona != 'P'){


            /*validamos que el rif sea valido con el ultimo digito verificador*/
                $retorno = preg_match("/^([VEJPG]{1})([0-9]{9}$)/", $rif_seniat);
                
            if ($retorno) {

                $digitos = str_split($rif_seniat);

                $digitos[8] *= 2;

                $digitos[7] *= 3;

                $digitos[6] *= 4;

                $digitos[5] *= 5;

                $digitos[4] *= 6;

                $digitos[3] *= 7;

                $digitos[2] *= 2;

                $digitos[1] *= 3;

                // Determinar dígito especial según la inicial del RIF

                // Regla introducida por el SENIAT

                switch ($digitos[0]) {

                    case 'V':

                        $digitoEspecial = 1;

                        break;

                    case 'E':

                        $digitoEspecial = 2;

                        break;

                    case 'J':

                        $digitoEspecial = 3;

                        break;

                    case 'P':

                        $digitoEspecial = 4;

                        break;

                    case 'G':

                        $digitoEspecial = 5;

                        break;

                }

                $suma = (array_sum($digitos) - $digitos[9]) + ($digitoEspecial*4);

                $residuo = $suma % 11;

                $resta = 11 - $residuo;

                $digitoVerificador = ($resta >= 10) ? 0 : $resta;

                if ($digitoVerificador != $digitos[9]) {

                    $retorno = false;

                }

            }
        }else{
            $retorno = true;
        }

        //dd($retorno);
        /*fin validacion del rif*/
        //validamos si el rif cumplio con las condiciones
        if ($retorno == false) {
            Alert::warning('Error en el Rif!', 'El rif ingresado es incorrecto.')->persistent("Ok");
            //flash('<b>Email Incorrecto!</b>,  el campo no puede estar vacío.', 'danger');
            return view('sesion.ValidarRif');
        }

        $usuario_existente=DB::table('det_usuario')->where('rif', $rif_conca)->first();
        if (isset($usuario_existente)) {
            Alert::warning('Usuario ya registrado!', 'El rif ingresado ya fue validado en nuestro sistema.')->persistent("Ok");

            //flash('<b>Email Incorrecto!</b>,  el campo no puede estar vacío.', 'danger');
            return view('sesion.ValidarRif');
        }
        //$user = DB::table('users')->where('name', 'John')->first();
        //consulto gen_jurico_cencoex
        $datos_cencoex=GenJuridicoCencoex::where('CRIF', '=', $rif_conca)->first();

            if (empty($datos_cencoex)) {//si no trajo nada de gen_jurico_cencoex que consulte  gen_jurico_seniat

                //dd($rif_seniat);
                $datos_seniat=GenJuridicoSeniat::where('CRIF', '=', $rif_seniat)->first();

                $datos=$datos_seniat;

            }else{
                $datos=$datos_cencoex;
            }


            $rif=$rif_conca;

           //dd($rif);
        /******************************************************************/

        //consulto tabla de pais para mostrar Venezuela
            $paises=Pais::orderBy('dpais','asc')->pluck('dpais','id');
        //consulto tabla estados
            $estados=Estado::where('id',"<>",null)->orderBy('estado','asc')->pluck('estado','id');
        //consulto todos los sectores
            $sectores=GenSector::all()->pluck('descripcion_sector','id');
        //consulto todos los tipos de empresa
            $tipos_empresas=GenTipoEmpresa::whereIn('id', [1, 2])->pluck('d_tipo_empresa','id');
            //dd($tipos_empresas);
        //consulto circunscripcion judicial
            $circuns_judiciales=CircunsJudicial::all()->pluck('registro','id');
        //consulto preguntas de seguridad
            $preguntas_seg=CatPreguntaSeg::all()->pluck('descripcion_preg','id');

        //Consulta la tabla gen_unidad_medida para traerme todas las unid medidas
        $unidad_medida=GenUnidadMedida::all()->pluck('dunidad','id');
        // Consulta el si y el no de la pregunta exporta productos de exportacion
        $radio=CatExportProd::all();

        /*******************************Consulta de los accionista***********************************/



        Session::flash('rif', $request->rif);
        Session::flash('captcha', $request->captcha);


      if($request->ajax()){

                //dd($request['arancel']);
            if($request['arancel']==1){

                $arancel=GenArancelNandina::take(50)->get();
                return $arancel;

            }elseif($request['arancel']==2){

                 //$arancel=GenArancelMercosur::take(50)->get();
                 $arancel=GenArancelMercosur::whereRaw("LENGTH(REPLACE(codigo, '.', '')) = 10")->where('bactivo',1)->take(50)->get();
                 return $arancel;

            }elseif($request['valor']!="") {

                if( ($request['tipoAran']==1) && ($request['valor']!="")){

                    $data=GenArancelNandina::where('codigo','like','%'.$request['valor'].'%')->orWhere('descripcion','like','%'.$request['valor'].'%')->take(50)->get();

                return $data;

                }elseif(($request['tipoAran']==2) && ($request['valor']!="")){

                    //$data=GenArancelMercosur::where('codigo','like','%'.$request['valor'].'%')->orWhere('descripcion','like','%'.$request['valor'].'%')->take(50)->get();
                    $data=GenArancelMercosur::whereRaw("LENGTH(REPLACE(codigo, '.', '')) = 10")->where('bactivo',1)
                                    ->where(function($q) use ($request){

                                                $q->where('codigo','like','%'.$request['valor'].'%')
                                                ->orWhere('descripcion','like','%'.$request['valor'].'%');
                                            })
                                    ->take(50)
                                    ->get();

                    return $data;

               }

            }else{
                if($request['tipoAran']==1){

                $arancel=GenArancelNandina::take(50)->get();
                 return $arancel;

                }else{

                //$arancel=GenArancelMercosur::take(50)->get();
                $arancel=GenArancelMercosur::whereRaw("LENGTH(REPLACE(codigo, '.', '')) = 10")->where('bactivo',1)->take(50)->get();
                return $arancel;

               }
            }

        }else{


           //$arancel=$arancel=GenArancelMercosur::all();
           $arancel=GenArancelMercosur::whereRaw("LENGTH(REPLACE(codigo, '.', '')) = 10")->where('bactivo',1)->take(50)->get();

        }


       if($request->tipoUsu==1){


      return view('sesion.persona_natural', compact('rif', 'datos', 'paises', 'sectores', 'estados', 'preguntas_seg','arancel','unidad_medida','radio','actividad_eco','municipios','parroquias'));

        }else{

        return view('sesion.register', compact('rif', 'datos', 'paises', 'sectores', 'tipos_empresas', 'estados','circuns_judiciales', 'preguntas_seg','arancel','unidad_medida','actividad_eco','municipios','parroquias'));

        }

    }




   public function storeNatural (Request $request){
     //validamos data que viene del formulario de registro
     //dd($request->all());
     Session::put('bandera',1);
     $pos=strpos($request->rif,'-');

     if($pos===false){

      Session::put('bandera_persona',$request->rif[0]);
      Session::put('bandera_rif',substr($request->rif,1));
      Session::put('bandera_tipo_persona',1);


     }else{

     $rif=explode('-',$request->rif);
     Session::put('bandera_persona',$rif[0]);
     Session::put('bandera_rif',$rif[1]);
     Session::put('bandera_tipo_persona',1);

     }

     $this->validate($request,[
         'rif' => 'required|unique:det_usuario,rif',
         'email' => 'required|email|unique:gen_usuario,email',
         'password'=>'required|min:6',
         'password_confirm'=>'required|min:6|same:password',
         'telefono_local'=>'required|min:11',
         'telefono_movil'=>'required|min:11',
         'produc_tecno'=>'required',
         'ruta_doc'=>'required',
        'ruta_rif'=>'required',
        'ruta_reg_merc'=>'required'
       ]);

         //pregutamos si la pregunta del desplegable de preguntas de seguridad viene lleno

                 if (isset($request->id_cat_preguntas_seg)) {
                     if($request->id_cat_preguntas_seg==8) {

                         $pregunta=$request['pregunta_alter'];

                     }else{

                         $pregunta='N/A';

                     }

                 }
       /******************* Si es asi insertamos con el id del desplegable*******************/
                       $user           = new GenUsuario;
                       $user->email    = $request['email'];
                       $user->email_seg = $request['email_seg'];
                       $user->cat_tipo_usuario_id=1;
                       $user->password = bcrypt($request['password']);
                       $user->cat_preguntas_seg_id = $request['id_cat_preguntas_seg'];
                       $user->pregunta_alter=$pregunta;
                       $user->respuesta_seg = $request['respuesta_seg'];
                       $user->remember_token = str_random(10);
                       $user->save();

                       //consulto ususario que acabo de registrar
                       $datos_user=GenUsuario::where('email', '=', $request['email'])->first();

           /***************************evaluamos si  exporta productos tecnologia o no***************************/


                           if (!empty($request->especifique_tecno)) {
                                   $especif_tecno=$request->especifique_tecno;
                                   $auxProducTec=1;
                           }else{
                               $especif_tecno="No exporta tecnología";
                               $auxProducTec=2;
                           }

         /******************************Evaluamos si los checkbox vienen lleno o no si es Si lo colocamos en 1 y no es 0 ***********/
                         if (!empty($request->invers)){

                              $invers=1;
                         }else{
                             $invers=0;

                         }

                         if (!empty($request->export)){

                             $export=1;
                         }else{

                             $export=0;
                         }

                         if (!empty($request->produc)){

                             $produc=1;

                         }else{
                             $produc=0;
                         }

                         if (!empty($request->comerc)) {

                             $comerc=1;
                         }else{
                             $comerc=0;
                         }

   /****************************************************************************/
     // Se creo un random
            function generarRandom($longitud) {
                    $key = '';
                    $pattern = '1234567890abcdefghijklmnopqrstuvwxyz';
                    $max = strlen($pattern)-1;
                    for($i=0;$i < $longitud;$i++) $key .= $pattern{mt_rand(0,$max)};
                    return $key;
            }

                        $ult_rif = substr($request['rif'],  5);  // devuelve "abcde"
                        $random=generarRandom(4);
                        $cod_fecha=date('Ymd');
                        $cod_empresa=$ult_rif.$cod_fecha.$random;

                        $destino = '/img/registerUser/';
                        $destinoPrivado = public_path($destino);
                        
                        if (!file_exists($destinoPrivado)) {
                            $filesystem = new Filesystem();
                            $filesystem->makeDirectory($destinoPrivado, $mode = 0777, true, true); 
                        }

                        $rutaImg = '';
                        $rutaImgRif= '';
                        $rutaImgRegMerc= '';
                        if(!empty($request->file('ruta_doc'))){                     

                            $imagen = $request->file('ruta_doc');
                            if (!empty($imagen)) {
                                
                               $nombre_imagen = $imagen->getClientOriginalName();
                               
                                $imagen->move($destinoPrivado, $nombre_imagen);
                                
                            }
                            
                            $rutaImg = $destino.$nombre_imagen;
                        }
                        if(!empty($request->file('ruta_rif'))){
                            
                            $ruta_rif = $request->file('ruta_rif');
                            if (!empty($ruta_rif)) {
                                
                               $nombre_ruta_rif = $ruta_rif->getClientOriginalName();
                               
                                $ruta_rif->move($destinoPrivado, $nombre_ruta_rif);
                                
                            }
                            
                            $rutaImgRif = $destino.$nombre_ruta_rif;
                        }

                        if(!empty($request->file('ruta_reg_merc'))){
                            
                            $ruta_reg_merc = $request->file('ruta_reg_merc');
                            if (!empty($ruta_reg_merc)) {
                                
                               $nombre_ruta_reg_merc = $ruta_reg_merc->getClientOriginalName();
                               
                                $ruta_reg_merc->move($destinoPrivado, $nombre_ruta_reg_merc);
                                
                            }
                            
                            $rutaImgRegMerc = $destino.$nombre_ruta_reg_merc;
                        }


      /*--------insertamos en det_usuario------------------*/
                         $DetUser= new DetUsuario;
                         $DetUser->gen_usuario_id=$datos_user->id;
                         $DetUser->cod_empresa=$cod_empresa;
                         $DetUser->razon_social=$request['razon_social'];
                         $DetUser->siglas=$request['siglas'];
                         $DetUser->rif=$request['rif'];
                         $DetUser->pagina_web=$request['pagina_web'];
                         //$DetUser->id_gen_sector=$request['gen_sector_id'];
                         $DetUser->gen_actividad_eco_id=$request['id_gen_actividad_eco'];
                         /*$DetUser->gen_tipo_empresa_id=$request['id_tipo_empresa'];*/
                         //$DetUser->pais_id=168;
                         $DetUser->estado_id=$request['id_estado'];
                         $DetUser->municipio_id=$request['id_municipio'];
                         $DetUser->parroquia_id=$request['id_parroquia'];
                         $DetUser->direccion=$request['direccion'];
                         $DetUser->telefono_local=$request['telefono_local'];
                         $DetUser->telefono_movil=$request['telefono_movil'];
                         $DetUser->correo=$request['email'];
                         $DetUser->correo_sec=$request['email_seg'];
                         $DetUser->gen_tipo_empresa_id=3;
                         $DetUser->circuns_judicial_id=49;
                         $DetUser->ruta_doc = $rutaImg;
                         $DetUser->ruta_rif = $rutaImgRif;
                         $DetUser->ruta_reg_merc = $rutaImgRegMerc;
                         //$DetUser->tipo_documento = $request['tipo_documento'];
                         //$DetUser->cargo_presid=NULL;

                         //$DetUser->accionista=$valor_accionista;

                         $DetUser->invers=$invers;
                         $DetUser->export=$export;
                         $DetUser->produc=$produc;
                         $DetUser->comerc=$comerc;

                         $DetUser->produc_tecno=$auxProducTec;
                         $DetUser->especifique_tecno=$especif_tecno;

                         $DetUser->save();

                         /*********************insertamos productos de la empresa******************************/


                         $cont_productos=count($request->descripcion);

                          //dd($cont_productos);

                         if (!empty($request->descripcion[0])) {
                             //dd($request->descrip_arancel[0]);
                             for($i=0;$i<=($cont_productos-1);$i++){
                             $productos= new Productos;
                             $productos->gen_usuario_id=$datos_user->id;
                             $productos->codigo=$request->codigo[$i];
                             $productos->descripcion=$request->descripcion[$i];
                             $productos->descrip_comercial=$request->descrip_comercial[$i];
                             $productos->gen_unidad_medida_id=$request->gen_unidad_medida_id[$i];
                             $productos->cap_opera_anual=$request->cap_opera_anual[$i];
                             $productos->cap_insta_anual=$request->cap_insta_anual[$i];
                             $productos->cap_alamcenamiento=$request->cap_alamcenamiento[$i];
                             $productos->cap_exportacion=$request->cap_exportacion[$i];
                             $productos->save();
                          }
                        }

                        /************insert de ultima exportcion**********************/

                            $ultexport= new UltimaExport;
                            $ultexport->gen_usuario_id=$datos_user->id;
                            $ultexport->pais_id=$request->pais_id;
                            $ultexport->fecha_ult_export=$request->fecha_ult_export;
                            $ultexport->save();


                         /*Insert registro por defecto de permisos*/

                         /*$permiso=new Permiso;
                         $permiso->tipo_permiso_id=5;
                         $permiso->gen_usuario_id=$datos_user->id;
                         $permiso->gen_aduana_salida_id=1;
                         $permiso->pais_id=168;
                         $permiso->numero_permiso='Sin Permiso';
                         $permiso->monto_aprobado=0;
                         $permiso->fecha_permiso=date('Y-m-d');
                         $permiso->fecha_aprobacion=date('Y-m-d');
                         $permiso->save();*/
             //consultamos detalle de usuario ya resgitrado para madar detalle al correo
             //$det_usuario=DB::table('det_usuario')->where('rif', $request->rif)->first();
                         $email=$request['email'];
                         $clave=$request['password'];
                         $razon_social=$request['razon_social'];
                         $rif=$request['rif'];

                        Session::forget('bandera');
                        Session::forget('bandera_rif');
                        Session::forget('bandera_tipo_persona');
                        Session::forget('bandera_persona');

                 //enviamos email con datos para acceso
                  Mail::to($email)->send(new welcomeEmail($razon_social,$rif,$email,$clave,$cod_empresa));
                         Alert::success('Usuario Registrado, se envió un email a su correo registrado!', 'Gracias por registrarse en nuestro sistema, Ya puede iniciar Sesión.')->persistent("Ok");

                    return view('sesion.login');
                    



   }

public function Arancel(Request $request) {


        if($request->ajax()){



            if($request->arancel==1){

                $arancel=GenArancelNandina::take(50)->get();
                return $arancel;

            }elseif($request->arancel==2){

                 //$arancel=GenArancelMercosur::take(50)->get();
                $arancel=GenArancelMercosur::whereRaw("LENGTH(REPLACE(codigo, '.', '')) = 10")->where('bactivo',1)->take(50)->get();
                 return $arancel;

            }elseif($request->valor!="") {

                if( ($request->tipoAran==1) && ($request->valor!="")){

                    $data=GenArancelNandina::where('codigo','like','%'.$request->valor.'%')->orWhere('descripcion','like','%'.$request->valor.'%')->take(50)->get();

                return $data;

                }elseif(($request->tipoAran==2) && ($request->valor!="")){

                    //$data=GenArancelMercosur::where('codigo','like','%'.$request->valor.'%')->orWhere('descripcion','like','%'.$request->valor.'%')->take(50)->get();
                    $data=GenArancelMercosur::whereRaw("LENGTH(REPLACE(codigo, '.', '')) = 10")->where('bactivo',1)
                                    ->where(function($q) use ($request){

                                                $q->where('codigo','like','%'.$request['valor'].'%')
                                                ->orWhere('descripcion','like','%'.$request['valor'].'%');
                                            })
                                    ->take(50)
                                    ->get();

                    return $data;

               }

            }else{
                if($request->tipoAran==1){

                $arancel=GenArancelNandina::take(50)->get();
                 return $arancel;

                }else{

                //$arancel=GenArancelMercosur::take(50)->get();
                $arancel=GenArancelMercosur::whereRaw("LENGTH(REPLACE(codigo, '.', '')) = 10")->where('bactivo',1)->take(50)->get();
                return $arancel;

               }
            }

        }else{

            $arancel=GenArancelMercosur::whereRaw("LENGTH(REPLACE(codigo, '.', '')) = 10")->where('bactivo',1)->take(50)->get();
           //$arancel=$arancel=GenArancelMercosur::all();

        }

        return view('sesion.register',compact('arancel'));
}



    protected function ActividadEcoEmp (Request $request, $id){
        if ($request->ajax()) {
            $actividadeco=GenActividadEco::actividadeco($id);
            return response()->json($actividadeco);
        }
    }

    protected function getMunicipios (Request $request, $id){
        if ($request->ajax()) {
            $municipios=Municipio::municipios($id);
            return response()->json($municipios);
        }
    }

    protected function getParroquias (Request $request, $id){
        if ($request->ajax()) {
            $parroquias=Parroquia::parroquias($id);
            return response()->json($parroquias);
        }
    }


    protected function registroUsr(Request $request){


        //validamos data que viene del formulario de registro

        // dd($request['accionista']);

     Session::put('bandera',1);
     $verificar=strpos($request->rif,'-');
     if($verificar==false)
     {

       Session::put('bandera_persona',$request->rif[0]);
       Session::put('bandera_rif',substr($request->rif,1));
       Session::put('bandera_tipo_persona',2);


     }else{
     $rif=explode('-',$request->rif);
     Session::put('bandera_persona',$rif[0]);
     Session::put('bandera_rif',$rif[1]);
     Session::put('bandera_tipo_persona',2);


   }

       $this->validate($request,[
            'rif' => 'required|unique:det_usuario,rif',
            'email' => 'required|email|unique:gen_usuario,email',
            'password'=>'required|min:6',
            'password_confirm'=>'required|min:6|same:password',
            'produc_tecno'=>'required',
            'telefono_local'=>'required|min:11',
            'ruta_doc'=>'required',
            'ruta_rif'=>'required',
            'ruta_reg_merc'=>'required'
          ]);


    //pregutamos si la pregunta del desplegable de preguntas de seguridad viene lleno

            if (isset($request->id_cat_preguntas_seg)) {
                if($request->id_cat_preguntas_seg==8) {

                    $pregunta=$request['pregunta_alter'];

                }else{

                    $pregunta='N/A';

                }

            }

/******************* Si es asi insertamos con el id del desplegable*******************/
                $user           = new GenUsuario;
                $user->email    = $request['email'];
                $user->email_seg = $request['email_seg'];
                $user->password = bcrypt($request['password']);
                $user->cat_preguntas_seg_id = $request['id_cat_preguntas_seg'];
                $user->pregunta_alter=$pregunta;
                $user->respuesta_seg = $request['respuesta_seg'];
                $user->cat_tipo_usuario_id=2;
                $user->remember_token = str_random(10);
                $user->save();

                //consulto ususario que acabo de registrar
                $datos_user=GenUsuario::where('email', '=', $request['email'])->first();
                //dd($datos_user);

                //insertamos en det_usuario
                if ($request['accionista']=='Si') {
                   $valor_accionista="Si";
                }else{
                    $valor_accionista="No";
                }

/***************************evaluamos si  exporta productos tecnologia o no***************************/


                if (!empty($request->especifique_tecno)) {
                        $especif_tecno=$request->especifique_tecno;
                        $auxProducTec=1;
                }else{
                    $especif_tecno="No exporta tecnología";
                    $auxProducTec=2;
                }

/******************************Evaluamos si los checkbox vienen lleno o no si es Si lo colocamos en 1 y no es 0 ***********/
                if (!empty($request->invers)){

                     $invers=1;
                }else{
                    $invers=0;

                }

                if (!empty($request->export)){

                    $export=1;
                }else{

                    $export=0;
                }

                if (!empty($request->produc)){

                    $produc=1;

                }else{
                    $produc=0;
                }

                if (!empty($request->comerc)) {

                    $comerc=1;
                }else{
                    $comerc=0;
                }


            function generarRandom($longitud) {
                $key = '';
                $pattern = '1234567890abcdefghijklmnopqrstuvwxyz';
                $max = strlen($pattern)-1;
                for($i=0;$i < $longitud;$i++) $key .= $pattern{mt_rand(0,$max)};
                return $key;
            }

                        $ult_rif = substr($request['rif'],  5);  // devuelve "abcde"
                        $random=generarRandom(4);
                        $cod_fecha=date('Ymd');
                        $cod_empresa=$ult_rif.$cod_fecha.$random;


                        $destino = '/img/registerUser/';
                        $destinoPrivado = public_path($destino);
                        
                        if (!file_exists($destinoPrivado)) {
                            $filesystem = new Filesystem();
                            $filesystem->makeDirectory($destinoPrivado, $mode = 0777, true, true); 
                        }

                        $rutaImg = '';
                        $rutaImgRif= '';
                        $rutaImgRegMerc= '';
                        if(!empty($request->file('ruta_doc'))){                        

                            $imagen = $request->file('ruta_doc');
                            if (!empty($imagen)) {
                                
                               $nombre_imagen = $imagen->getClientOriginalName();
                               
                                $imagen->move($destinoPrivado, $nombre_imagen);
                                
                            }
                            
                            $rutaImg = $destino.$nombre_imagen;
                        }
                        if(!empty($request->file('ruta_rif'))){
                            
                            $ruta_rif = $request->file('ruta_rif');
                            if (!empty($ruta_rif)) {
                                
                               $nombre_ruta_rif = $ruta_rif->getClientOriginalName();
                               
                                $ruta_rif->move($destinoPrivado, $nombre_ruta_rif);
                                
                            }
                            
                            $rutaImgRif = $destino.$nombre_ruta_rif;
                        }

                        if(!empty($request->file('ruta_reg_merc'))){
                            
                            $ruta_reg_merc = $request->file('ruta_reg_merc');
                            if (!empty($ruta_reg_merc)) {
                                
                               $nombre_ruta_reg_merc = $ruta_reg_merc->getClientOriginalName();
                               
                                $ruta_reg_merc->move($destinoPrivado, $nombre_ruta_reg_merc);
                                
                            }
                            
                            $rutaImgRegMerc = $destino.$nombre_ruta_reg_merc;
                        }

                

                

                $DetUser= new DetUsuario;
                $DetUser->gen_usuario_id=$datos_user->id;
                $DetUser->cod_empresa=$cod_empresa;
                $DetUser->razon_social=$request['razon_social'];
                $DetUser->siglas=$request['siglas'];
                $DetUser->rif=$request['rif'];
                $DetUser->pagina_web=$request['pagina_web'];
                //$DetUser->id_gen_sector=$request['gen_sector_id'];
                $DetUser->gen_actividad_eco_id=$request['id_gen_actividad_eco'];
                $DetUser->gen_tipo_empresa_id=$request['id_tipo_empresa'];
                //$DetUser->pais_id=168;
                $DetUser->estado_id=$request['id_estado'];
                $DetUser->municipio_id=$request['id_municipio'];
                $DetUser->parroquia_id=$request['id_parroquia'];
                $DetUser->direccion=$request['direccion'];
                $DetUser->telefono_local=$request['telefono_local'];
                $DetUser->telefono_movil=$request['telefono_movil'];
                $DetUser->circuns_judicial_id=$request['id_circuns_judicial'];
                $DetUser->num_registro=$request['num_registro'];
                $DetUser->tomo=$request['tomo'];
                $DetUser->folio=$request['folio'];
                $DetUser->Oficina=$request['Oficina'];
                $DetUser->f_registro_mer=$request['f_registro_mer'];

                //$DetUser->nombre_presid=$request['nombre_presid'];
                //$DetUser->apellido_presid=$request['apellido_presid'];
                //$DetUser->ci_presid=$request['ci_presid'];
                //$DetUser->cargo_presid=$request['cargo_presid'];

                $DetUser->nombre_repre=$request['nombre_repre'];
                $DetUser->apellido_repre=$request['apellido_repre'];
                $DetUser->ci_repre=$request['ci_repre'];
                $DetUser->cargo_repre=$request['cargo_repre'];
                $DetUser->correo_repre=$request['correo_repre'];
                $DetUser->correo=$request['email'];
                $DetUser->correo_sec=$request['email_seg'];

                $DetUser->accionista=$valor_accionista;
                $DetUser->ruta_doc = $rutaImg;
                $DetUser->ruta_rif = $rutaImgRif;
                $DetUser->ruta_reg_merc = $rutaImgRegMerc;
                //$DetUser->tipo_documento = $request['tipo_documento'];

                $DetUser->invers=$invers;
                $DetUser->export=$export;
                $DetUser->produc=$produc;
                $DetUser->comerc=$comerc;

                $DetUser->produc_tecno=$auxProducTec;
                $DetUser->especifique_tecno=$especif_tecno;

                $DetUser->save();

             
              

                if ($request['accionista']=='Si') {

                    $cont_accionista=count($request->nombre_accionista);
                ////Contador para los accionista///
                    for($i=0;$i<=($cont_accionista-1);$i++){
                        $Accionista= new Accionistas;
                        $Accionista->gen_usuario_id=$datos_user->id;
                        $Accionista->nombre_accionista=$request->nombre_accionista[$i];
                        $Accionista->apellido_accionista=$request->apellido_accionista[$i];
                        $Accionista->cedula_accionista=$request->cedula_accionista[$i];
                        $Accionista->cargo_accionista=$request->cargo_accionista[$i];
                        $Accionista->telefono_accionista=$request->telefono_accionista[$i];
                        $Accionista->nacionalidad_accionista=$request->nacionalidad_accionista[$i];
                        $Accionista->participacion_accionista=$request->participacion_accionista[$i];
                        $Accionista->correo_accionista=$request->correo_accionista[$i];
                        $Accionista->save();
                    }
                }

                /*********************insertamos productos de la empresa******************************/


                $cont_productos=count($request->descripcion);

                 //dd($cont_productos);

                if (!empty($request->descripcion[0])) {
                    //dd($request->descrip_arancel[0]);
                    for($i=0;$i<=($cont_productos-1);$i++){
                    $productos= new Productos;
                    $productos->gen_usuario_id=$datos_user->id;
                    $productos->codigo=$request->codigo[$i];
                    $productos->descripcion=$request->descripcion[$i];
                    $productos->descrip_comercial=$request->descrip_comercial[$i];
                    $productos->gen_unidad_medida_id=$request->gen_unidad_medida_id[$i];
                    $productos->cap_opera_anual=$request->cap_opera_anual[$i];
                    $productos->cap_insta_anual=$request->cap_insta_anual[$i];
                    $productos->cap_alamcenamiento=$request->cap_alamcenamiento[$i];
                    $productos->cap_exportacion=$request->cap_exportacion[$i];
                    $productos->save();
                 }
               }


               /***************insert de casa matriz*****************/

                $camatriz= new CasaMatriz;
                $camatriz->gen_usuario_id=$datos_user->id;
                $camatriz->casa_matriz=$request->casa_matriz;
                $camatriz->pais_id=$request->pais_id;
                $camatriz->save();

               /************insert de ultima exportcion**********************/

                $ultexport= new UltimaExport;
                $ultexport->gen_usuario_id=$datos_user->id;
                $ultexport->pais_id=$request->pais_id;
                $ultexport->fecha_ult_export=$request->fecha_ult_export;
                $ultexport->save();


                /*Insert registro por defecto de permisos*/

                /*$permiso=new Permiso;
                $permiso->tipo_permiso_id=5;
                $permiso->gen_usuario_id=$datos_user->id;
                $permiso->gen_aduana_salida_id=1;
                $permiso->pais_id=168;
                $permiso->numero_permiso='Sin Permiso';
                $permiso->monto_aprobado=0;
                $permiso->fecha_permiso=date('Y-m-d');
                $permiso->fecha_aprobacion=date('Y-m-d');
                $permiso->save();*/
    //consultamos detalle de usuario ya resgitrado para madar detalle al correo
    //$det_usuario=DB::table('det_usuario')->where('rif', $request->rif)->first();
                $email=$request['email'];
                $clave=$request['password'];
                $razon_social=$request['razon_social'];
                $rif=$request['rif'];

        //enviamos email con datos para acceso
                Mail::to($email)->send(new welcomeEmail($razon_social,$rif,$email,$clave,$cod_empresa));



                Alert::success('Usuario Registrado, se envió un email a su correo registrado!', 'Gracias por registrarse en nuestro sistema, Ya puede iniciar Sesión.')->persistent("Ok");

           return view('sesion.login');
    }

    protected function passwordReset()
    {
        return view('sesion.email');
    }

    protected function storeLogin(Request $request){
        //dd($request);
        //$var=Auth::user()->id_gen_tipo_usu;
        //dd($var);
        //validaciones para realizar la consulta
        $this->validate($request,[
            'email' => 'required|string|email|max:255',
            'password' => 'required|string|min:6',
            'captcha' => 'required|captcha',
            ]);
        /*Validator::make($request, [

        ]);*/

        if ($request->email =="") {
            Alert::warning('Email Incorrecto!', 'el campo no puede estar vacío.')->persistent("Ok");
            //flash('<b>Email Incorrecto!</b>,  el campo no puede estar vacío.', 'danger');
            return redirect()->action('SesionController@logout');

        }else if($request->password ==""){
            Alert::warning('Password Incorrecto!', 'el campo no puede estar vacío.')->persistent("Ok");
            //flash('<b>Password Incorrecto!</b>,  el campo no puede estar vacío.', 'danger');
            return redirect()->action('SesionController@logout');

            }else{
                    if(Auth::attempt(['email' => $request->email, 'password' => $request->password])){

                        if (Auth::user()->bactivo  != 1 ) {
                            return redirect()->action('SesionController@logout');
                        }
                        //
                        if (Auth::user()->gen_status_id == 2) {
                            //dd(Auth::user()->gen_status_id);
                            Alert::warning('Usuario Suspendido!', 'Usuario ha sido Suspendido.')->persistent("Ok");

                            //return redirect('/loguin');
                            //flash('<b>Usuario Suspendido!</b>,  Su Usuario ha sido Suspendido.', 'danger');
                            return redirect()->action('SesionController@logout');
                        }

                        $sesionuser = new HistSesionUser;
                        $sesionuser->gen_usuario_id=Auth::user()->id;
                        $sesionuser->ip_usuario=\Request::ip();
                        $sesionuser->last_login=@HistSesionUser::where('gen_usuario_id',Auth::user()->id)->latest()->first()->created_at;
                        $sesionuser->sesion=1;
                        $sesionuser->save();

                        switch (Auth::user()->gen_tipo_usuario_id) {
                            case 1:
                                Alert::success('Hola Exportador!', 'Bienvenido al Sistema VUCE.')->persistent("Ok");
                                flash('Hola Exportador <b>'.$request->email.'!</b>,  Bienvenido al Sistema VUCE', 'success');
                                return redirect()->action('ExportadorController@index');
                            break;
                            case 2:
                                Alert::success('Hola Administrador!', 'Bienvenido al Sistema VUCE.')->persistent("Ok");
                                flash('Hola Administrador <b>'.$request->email.'!</b>,  Bienvenido al Sistema VUCE', 'success');
                                return redirect()->action('AdminController@index');
                            break;
                            case 7:
                                Alert::success('Hola !', 'Bienvenido al Sistema VUCE.')->persistent("Ok");
                                flash('Hola <b>'.$request->email.'!</b>,  Bienvenido al Sistema VUCE', 'success');
                                return redirect()->action('PdfController@Creargrafic');
                            break;
                            // Analista DJO
                            case 6:
                                Alert::success('Hola !', 'Bienvenido al Sistema VUCE.')->persistent("Ok");
                                flash('Hola <b>'.$request->email.'!</b>,  Bienvenido al Sistema VUCE', 'success');
                                return redirect()->action('AnalisisCalificacionController@index');
                            break;
                            // Coordinador DJO
                            case 8:
                                Alert::success('Hola !', 'Bienvenido al Sistema VUCE.')->persistent("Ok");
                                flash('Hola <b>'.$request->email.'!</b>,  Bienvenido al Sistema VUCE', 'success');
                                return redirect()->action('CoordinadorVerificacionController@index');
                            break;

                             case 9:
                                Alert::success('Hola !', 'Bienvenido al Sistema VUCE.')->persistent("Ok");
                                flash('Hola <b>'.$request->email.'!</b>,  Bienvenido al Sistema VUCE', 'success');
                                return redirect()->action('BandejaAnalistaController@index');
                            break;


                            case 10:
                                Alert::success('Hola !', 'Bienvenido al Sistema VUCE.')->persistent("Ok");
                                flash('Hola <b>'.$request->email.'!</b>,  Bienvenido al Sistema VUCE', 'success');
                                return redirect()->action('BandejaCoordinadorController@index');
                            break;

                            case 11:
                                Alert::success('Hola !', 'Bienvenido al Sistema VUCE.')->persistent("Ok");
                                flash('Hola <b>'.$request->email.'!</b>,  Bienvenido al Sistema VUCE', 'success');
                                return redirect()->action('AnalistaResguardoController@index');
                            break;

                              case 12:
                                Alert::success('Hola !', 'Bienvenido al Sistema VUCE.')->persistent("Ok");
                                flash('Hola <b>'.$request->email.'!</b>,  Bienvenido al Sistema VUCE', 'success');
                                return redirect()->action('AnalistaCnaController@index');
                            break;

                              case 13:
                                Alert::success('Hola !', 'Bienvenido al Sistema VUCE.')->persistent("Ok");
                                flash('Hola <b>'.$request->email.'!</b>,  Bienvenido al Sistema VUCE', 'success');
                                return redirect()->action('AdministradorResguardoCnaController@index');
                            break;

                            case 14:
                                Alert::success('Hola !', 'Bienvenido al Sistema VUCE.')->persistent("Ok");
                                flash('Hola <b>'.$request->email.'!</b>,  Bienvenido al Sistema VUCE', 'success');
                                return redirect()->action('BandejaAnalistaCvcController@index');
                            break;

                            case 15:
                                Alert::success('Hola !', 'Bienvenido al Sistema VUCE.')->persistent("Ok");
                                flash('Hola <b>'.$request->email.'!</b>,  Bienvenido al Sistema VUCE', 'success');
                                return redirect()->action('UsuarioSeniatController@index');
                            break;

                            case 16:
                                Alert::success('Hola !', 'Bienvenido al Sistema VUCE.')->persistent("Ok");
                                flash('Hola <b>'.$request->email.'!</b>,  Bienvenido al Sistema VUCE', 'success');
                                return redirect()->action('AnalistaSeniatController@index');
                            break;

                            case 17:
                                Alert::success('Hola !', 'Bienvenido al Sistema VUCE.')->persistent("Ok");
                                flash('Hola <b>'.$request->email.'!</b>,  Bienvenido al Sistema VUCE', 'success');
                                return redirect()->action('ViceministroSeniatController@index');
                            break;
                            case 18:
                                Alert::success('Hola !', 'Bienvenido al Sistema VUCE.')->persistent("Ok");
                                flash('Hola <b>'.$request->email.'!</b>,  Bienvenido al Sistema VUCE', 'success');
                                return redirect()->action('AnalistaIntranetController@index');
                            break;
                            case 19:
                                Alert::success('Hola !', 'Bienvenido al Sistema VUCE.')->persistent("Ok");
                                flash('Hola <b>'.$request->email.'!</b>,  Bienvenido al Sistema VUCE', 'success');
                                return redirect()->action('CoordinadorIntranetController@index');
                            break;

                            default:
                                Alert::error('Usuario No encontrado!', 'Los datos no están registrados en el sistema.')->persistent("Ok");


                        return view('sesion.login');
                            break;

                        }
                    }else{
                        //flash('<b>Usuario No encontrado!</b>,  Los datos no están registrados en el sistema.', 'danger');

                        //alert()->error('Usuario No encontrado!', 'Los datos no están registrados en el sistema.');
                        Alert::error('Usuario No encontrado!', 'Los datos no están registrados en el sistema.')->persistent("Ok");

                        return view('sesion.login');
                    }
                }
    }

    protected function logout(){

        $sesionuser = new HistSesionUser;
        $sesionuser->gen_usuario_id=Auth::user()->id;
        $sesionuser->ip_usuario=\Request::ip();
        $sesionuser->last_login=@HistSesionUser::where('gen_usuario_id',Auth::user()->id)->latest()->first()->created_at;
        $sesionuser->sesion=0;
        $sesionuser->save();
        Session::flush();
        Auth::logout();

        Alert::success('Hasta Luego!', 'Gracias por su Visita.')->persistent("Ok");

        return redirect('/');

    }

    public function consultUser(Request $request)
    {
        $datos_user=GenUsuario::where('email', $request['email'])->first();

        if(isset($datos_user)){

            $rif = $request->select_tipoPersona.'-'.$request->rif;

            $userRif = DetUsuario::where('gen_usuario_id', $datos_user->id)->where('rif', $rif)->first();

            if(isset($userRif)){

                $preguntas_seg = CatPreguntaSeg::find($datos_user->cat_preguntas_seg_id);
                $gen_usuario_id= $datos_user->id;

                return view('sesion.reesPassword', compact('preguntas_seg', 'gen_usuario_id'));

            }else{

                Alert::error('Usuario No encontrado!', 'El RIF no coincide con el correo.')->persistent("Ok");
                return redirect('/reestablecer-password');

            }

        }else{

            Alert::error('Usuario No encontrado!', 'Los datos no están registrados en el sistema.')->persistent("Ok");
            return redirect('/reestablecer-password');

        }
    }

    public function resetPassword(Request $request)
    {
        $genUsuario = GenUsuario::find($request->gen_usuario_id);

        if($genUsuario->respuesta_seg == $request->respuesta_seg){
            
            return view('sesion.newPassword', compact('genUsuario'));
        
        }else{

            Alert::error('Error!', 'La respuesta no es correcta.')->persistent("Ok");
            return redirect('/reestablecer-password');

        }
        
    }

    public function storePassword(Request $request)
    {
        $this->validate($request,[
            'password'=>'required|min:6',
            'password_confirm'=>'required|min:6|same:password',
        ]);
        

        $genUsuario = GenUsuario::find($request->id);
        $genUsuario->password = bcrypt($request['password']);
        $genUsuario->save();
        
        Alert::success('Exito!', 'Su contraseña se ha restablecido con exito.')->persistent("Ok");
        
        return redirect('/');

    }

}
