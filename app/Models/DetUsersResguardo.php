<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DetUsersResguardo extends Model
{
    protected $table = 'det_users_resguardo';

    protected $fillable = [

           'gen_usuario_id',
           'nombre_funcionario',
           'apellido_funcionario',
           'cedula_funcionario',
           'rango_funcionario',
           'unidad',
           'gen_aduana_salida_id',
           'ubicacion',
           'tlf_funcionario',
           'bactivo',
    ];

    protected $date =['created_at','updated_at'];

    public function rUsuario() {

        return $this->hasOne('App\Models\GenUsuario','id','gen_usuario_id');

    }
    public function rAduanaSalida() {

        return $this->hasOne('App\Models\GenAduanaSalida','gen_aduana_salida_id','id');

    }
}
