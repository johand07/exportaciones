@extends('templates/layoutlte')
@section('content')
<div class="content">
  @if(!empty($planilla5->estado_observacion))
  <script>

  document.onreadystatechange = function () {
    var state = document.readyState;
    if (state == 'complete') {
      swal("Estimado usuario debera corregír las siguientes observaciones", "{{$planilla5->descrip_observacion}}", "warning");
    }
  }
  </script>
<div id="space_alert1" class="alert alert-danger" role="alert" style="display: block;">
  <strong>
    <span class="glyphicon glyphicon-circle-arrow-right"></span> Observación Indicada por el Analista: {{$planilla5->descrip_observacion}}
  </strong>
</div>
@endif
<div class="panel-group" style="background-color: #fff;">
      <div class="panel-primary">
        <div class="panel-heading"> <h3>(P-5) -  7. Proceso productivo gráfico del producto a exportar</h3></div>
        <div class="panel-body">
                  <div class="row" style="background-color: #fff">
                      <div class="col-md-12">
                          
    {{ Form::model($planilla5, array('route' => array('DJOPlanilla5.update', $planilla5->id), 'method' => 'PUT','enctype' => 'multipart/form-data', 'id'=>'planilla5UpdateForm')) }}
    

                          <div class="row">
                      
                              <div class="col-md-12">
                                  <table border="1" class="col-md-12">
                                    <thead>
                                      <tr>
                                        <th class="text-center" width="50%" height="40px">Sección de carga</th>
                                        <th class="text-center">Vista previa</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      <tr>
                                        <td class="text-center" valign="top" > 
                                          <span class="btn btn-warning btn-file" style="margin: 3px;">
                                          Cargar gráfico productivo 
                                          <span class="glyphicon glyphicon-picture"></span>
                                          {!! Form::file('proceso_grafico_imagen', array('class' => 'file-input','style'=>'margin: 10px;')) !!}
                                          </span>
                                        </td>
                                        <td class="text-center" height="40px">
                                        @if($extencion == 'pdf' || $extencion == 'docx' || $extencion == 'doc')
                                          <img id="vista_previa_proceso_grafico" src="{{ asset('img/file.png') }}" alt="" width="300px" style="margin: 3px;">
                                        @else
                                        <img id="vista_previa_proceso_grafico" src="{{$planilla5->diagrama_proceso_productivo}}" alt="" width="300px" style="margin: 3px;">
                                        @endif

                                        </td>
                                      </tr>
                                    </tbody>
                                  </table>
                              </div>
                          </div>
<hr>
                          <div class="row" style="background-color: #fff">

                                <div class="col-md-12" align="center">

                                    <button  type="submit"  name="submit" id="submit"  class="btn btn-primary" onclick="validacionForm();"><b>Corregir</b></button>
                                </div>
                              
                            </div>


                  {{Form::close()}}

                      </div>
                  </div>

        </div>
      </div>
    </div>

  </div><!-- Fin content -->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script>

   function cambiarImagen(event) {

    var reader = new FileReader();
    reader.onload = function(){
      var output = document.getElementById('vista_previa_proceso_grafico');
      output.src = reader.result;
    };
    var archivo = $(".file-input").val();
    var extensiones = archivo.substring(archivo.lastIndexOf("."))toLowerCase();
    //alert(archivo);
    //var imgfile="/exportaciones/public/img/file.png";

//alert(imgfile);
    if(extensiones != ".jpeg" && extensiones != ".png" && extensiones != ".jpg" && extensiones != ".gif"){
        $('#vista_previa_proceso_grafico').attr("src","{{ asset('img/file.png') }}");
        
    }else{
     
      reader.readAsDataURL(event.target.files[0]);
    }

    
  
  }

    $(document).on('change', '.file-input', function(evt) {
           cambiarImagen(evt);
    });


</script>

<script>
  //Validación de formulario, se puede llamar y pasar la sección que contenga los inputs a validar
  $(document).ready(function ()
  {
              var error = '<div class="msg_alert"><span style="color:red" class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span><strong id="cod_aran-error" class="added"> El campo es obligatorio.</strong></div>';
                var valido = '<div class="msg_alert"><span style="color:green" class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span></div>';
                var mens=['Estimado Usuario. Debe completar los campos solicitados',]
  
                validacionForm = function() {
  
                $('#planilla5UpdateForm').submit(function(event) {
                
                var campos = $('#planilla5UpdateForm').find('input:file');
                var n = campos.length;
                var err = 0;
  
                $("div").remove(".msg_alert");
                //bucle que recorre todos los elementos del formulario
                for (var i = 0; i < n; i++) {
                    var cod_input = $('#planilla5UpdateForm').find('input:file').eq(i);
                    if (!cod_input.attr('noreq')) {
                      if (cod_input.val() == '' || cod_input.val() == null)
                      {
                        err = 1;
                        cod_input.css('border', '1px solid red').after(error);
                      }
                      else{
                        if (err == 1) {err = 1;}else{err = 0;}
                        cod_input.css('border', '1px solid green').after(valido);
                      }
                      
                    }
                }
  
                //Si hay errores se detendrá el submit del formulario y devolverá una alerta
                if(err==1){
                    event.preventDefault();
                    swal("Por Favor!", mens[0], "warning")
                  }
  
                  });
                }
              });
  </script>


@endsection