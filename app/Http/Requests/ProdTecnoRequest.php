<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProdTecnoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
      return [
          'codigo_arancel.*'=>'required',
          'descripcion.*'=>'required',
         // 'cantidad.*'=>'required|numeric',
        //  'precio.*'=>'required|numeric',
           'unidad_medida_id.*'=>'required|numeric'
        ];
    }
}
