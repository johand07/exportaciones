<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DescripPartidaZoo extends Model
{
    protected $table = 'descrip_partida_zoo_eu';
    protected $fillable = [

        'gen_cert_zoo_eu_id',
        'codigo_nc',
        'especie',
        'almacen_frigo',
        'marc_ident',
        'tipos_embalaje',
        'nat_merc',
        'consumidor_final',
        'tipo_tratamiento',
        'fecha_recogida',
        'peso_neto',
        'num_bultos',
        'num_lotes',
        'bactivo',
          
    ];

    public function certificadoEfectoDe() {

   		return $this->hasOne('App\Models\GenCertZooEu','id','gen_cert_zoo_eu_id');

    }
    
}
