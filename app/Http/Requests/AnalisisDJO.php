<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AnalisisDJO extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        
        return [
            //
            'fecha_emision'=>'required',
            'fecha_vencimiento'=>'required',
            'gen_declaracion_jo_id'=>'required',
            'num_solicitud'=>'required',
            'bol.*'=>'required',
            'col.*'=>'required',
            'ecu.*'=>'required',
            'per.*'=>'required',
            'cub.*'=>'required',
            'ald.*'=>'required',
            'arg.*'=>'required',
            'bra.*'=>'required',
            'par.*'=>'required',
            'uru.*'=>'required',
            'usa.*'=>'required',
            'ue.*'=>'required',
            'cnd.*'=>'required',
            'tp.*'=>'required',
        ];
      
    }
}
