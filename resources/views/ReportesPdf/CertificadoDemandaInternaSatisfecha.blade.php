<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>CERTIFICADO DE DEMANDA INTERNA SATISFECHA</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style type="text/css">
        .centrar{
            text-align: center;
        }

        .titulo{
            
            font-weight: bold;
        }

        .nro-correlativo{
            color: red;
            font-size: 16px;
        }

        .derecha{
            text-align: left;

        }

        .text-normal{
            font-size: 14px;
        }

        .text-medio{
            font-size: 12px;
        }
        .text-11{
            font-size: 11px;
        }
        .text-small{
            font-size: 10px;
        }

        .text-9{
            font-size: 9px;
        }

        .text-8{
            font-size: 8px;
        }

        .espacio-superior{
            margin-top: 10px;
        }

        .espacio-superior-small{
            margin-top: 5px;
        }

        .ancho-medio{
            width: 50%;
        }

        .ancho-full{
            width: 100%;
        }

        .ancho-medio-alto{
            width: 75%;
        }

        .ancho-90{
            width: 90%;
        }

        .table{
            border-collapse: collapse;
        }
        
        .borde{
            border: 1px solid #000;
        }

        .sin-borde{
            border: none;
        }

        .top{
            vertical-align: top;
        }

        .under-dotted{    
            border-bottom: 1px dotted #000;
            text-decoration: none;
        }

        .content-1{
            width: 5%;
        }

        .content-9{
            width: 20%;
        }

        .sangria-1{
            margin-left: 28px;
        }

        .sangria-2{
            margin-left: 36px;
        }

        .sangria-3{
            margin-left: 54px;
        }

        .sangria-sub{
            margin-left: 14px;
            
        }

        
        .page-break { page-break-before: always; }

        .table-center{
            margin: 0 auto; 
        }

        .justify{
            text-align: justify;
        }

        td.justify {
            padding-left: 8px;
            padding-right: 8px; 
        }
        .lineal{
            
        }
        .lineal>div{
            max-width:91%;

        }
        .lineal>*{
           display: inline-block;
           vertical-align:top;
        }

        .sangria-lista{
            list-style-type: none;
            margin: 0;
            padding: 0;
            margin-bottom: 4px;
        }

        .sangria-lista>li{
            margin-top: 5px;
            padding-left: 10px;
            padding-right: 10px;
        }
    </style>
</head>
<body>
    <div class="centrar espacio-superior">
        <table class="ancho-full">
            <tr>
                <td class="top center" style="width: 70%; font-size:107%;">
                      <img class="img-responsive" style="width: 565px;" src="img/cintillo.png">
                </td>
            </tr>
        </table>
    </div><br>
    <div class="centrar espacio-superior">
        <table class="ancho-full">
            <tr>
                <td class="nro-correlativo" style="width: 70%; font-size:107%; text-align: left;">
                     <b>N°</b><b style="color: black">{{ $solcvc->id}}</b>
                </td>
                 <td class="nro-correlativo" style="width: 70%; font-size:107%; text-align: right;">
                     <b>MPPAPT-CVC</b><b style="color: black"></b>
                </td>
            </tr>
        </table>
    </div><br>
    <div class="centrar espacio-superior">
        <table class="ancho-full">
            <tr>
                <td class="center" style="width: 70%; font-size:107%; text-align: center;">
                     CERTIFICADO DE DEMANDA INTERNA SATISFECHA <br><br>
                </td>
            </tr>
        </table>
    </div>

<div class="centrar espacio-superior">
    <table class="ancho-full">
        <tr>
            <td class="top" style="text-align: left; font-size: 18px" colspan="50"><b>Sres: </b>{{ $users->razon_social}}</td>
            <td class="top" style="text-align: left; font-size: 18px" colspan="50"><b>Certificado N°: </b>{{ $solcvc->num_sol_cvc}}</td>
        </tr>
         <tr>
            <td class="top" style="text-align: left; font-size: 18px" colspan="50"><b>Rif: </b>{{ $users->rif}}</td>
            <td class="top" style="text-align: left; font-size: 18px" colspan="50"><b>Fecha: </b>{{date('d/m/Y',strtotime($solcvc->created_at))}}</td>
        </tr>
    </table>

</div>

<div>
    <table>
        <tr>
            <td style="text-align: justify; font-size: 15px;" colspan="80"> Presente- <br><br>De conformidad con el articulo 21 de Arancel de Aduanas, establecido en el decreto N° 2.547, de fecha 30 de diciembre d 2016, publicado en la G.O. N° 6.281 extraordinario de la misma fecha, mediante el cual se establece el Régimen Legal aplicable a la exportación de mercancías con la siguiente denominación: "<b>6. Permiso del Ministerio del Poder Popular</b> con competencia en materiales de agricultura".
                <br><br> Se otorga el presente permiso de una vez determinada la suficiente producción nacional de la siguiente mercancía. <br><br></td>
        </tr>
    </table>


    <div class="centrar espacio-superior">
        <table class="ancho-full borde" >
            <tr>
                <td class="top" style="text-align: center; font-size: 14px" colspan="20"><b>CÓDIGO <br> ARANCELARIO</b><br><br>{{$solcvc->codigo_arancelario}}</td>
                <td class="top" style="text-align: center; font-size: 14px" colspan="20"><b>DESCRIPCIÓN <br></b><br><br>{{$solcvc->caract_especiales}}<br><br></td>
                <td class="top" style="text-align: center; font-size: 14px" colspan="20"><b>VOLUMEN <br></b><br><br>{{$solcvc->GenUnidadMedida->dunidad}}</td>
                <td class="top" style="text-align: center; font-size: 14px" colspan="20"><b>ADUANA DE <br>SALIDA</b><br><br>{{$solcvc->GenAduanaSalida->daduana}}<br><br></td>
                <td class="top" style="text-align: center; font-size: 14px" colspan="20"><b>PAIS <br>DESTINO</b><br><br>{{$solcvc->Pais->dpais}}</td>
                

            </tr>
        </table>
    </div>
<br><br>

    <table>
        <tr>
            <td style="text-align: justify; font-size: 15px;" colspan="80">El presente permiso tendrá una vigencia de tres (3) meses a partir de las fechas de su expedición, de conformidad con lo establecido en el articulo 61 del Decreto N° 2.292, de fecha 01 de abril del 2016 publicado en G.O, N° 6.222 extraordinaria de la misma fecha. <br><br> Sirva la presente para la realización de los tramites legales de interés institucional.</td>
        </tr>
    </table>
<br>
    <p align="center">Atentamente</p>
</div>

<br><br>

<div>
     <table>
        <tr>
            <td style="text-align: center; font-size: 17px;" colspan="80"><b>{{ $datosfirmante->nombre_firmante}} {{ $datosfirmante->apellido_firmante}}</b><br><b>{{ $datosfirmante->cargo_firmante}}</b><br>{{ $datosfirmante->gaceta_firmante}}</td>
        </tr>
    </table>
</div>
<br><br>









</body>
</html>