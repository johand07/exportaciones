@extends('templates/layoutlte')
@section('content')
{{Form::open(['route' =>'ProdutPermisos.store' ,'method'=>'POST','id'=>'add_product','name'=>'add_product'])}}
{{Form::hidden('metodo','Crear')}}
    <!--Tabla de los Productos-->
    <table class="table table-bordered" id="fields">    
      <tr>
        <th>Código Arancelario </th>
        <th>Descripcion Arancelaria</th>
        <th>Descripcion Comercial</th>
        <th>Cantidad </th>
        <th>Unidad de Medida</th>
      </tr>
      <tr id="row">
          <td>{!!Form::text('codigo[]',null,['class'=>'form-control','data-toggle'=>"modal",'data-target'=>"#modal",'id'=>'codigo','readonly'=>'true'])!!}
          </td>
          <td>{!!Form::text('descripcion[]',null,['class'=>'form-control','id'=>'descripcion','onkeypress'=>'return soloLetras(event)','readonly'=>'true'])!!}</td>
          <td>{!!Form::text('descrip_comercial[]',null,['class'=>'form-control','onkeypress'=>'return soloLetras(event)'])!!}</td>
          <td>{!!Form::text('cantidad[]',null,['class'=>'form-control','onkeypress'=>'return soloNumeros(event)'])!!}</td>
          <td>{!!Form::select('gen_unidad_medida_id[]',$medidas,null,['class'=>'form-control','placeholder'=>'Seleccione'])!!}</td>      
          <td><button  name="add" type="button" id="add" value="add more" class="btn btn-success">Agregar</button></td>
      </tr>
    </table>
    <!--Espacio para lo Botones-->
    <br><div class="row">
      <div class="col-md-12">
        <div class="col-md-4"></div>
        <div class="col-md-4 text-center">
        <a href="{{ route('Permisos.index') }}" class="btn btn-primary">Cancelar</a>
        <input type="submit" class="btn btn-success" value="Enviar" name="">
        </div>
        <div class="col-md-4"></div>
      </div>
    </div>

    @component('modal_dialogo',['arancel'=>$arancel])

      @slot('header') <span>Código Arancelario</span> @endslot

      @slot('body')  <div id="arancel"></div>

      @endslot

      @slot('footer')@endslot

    @endcomponent

{{Form::close()}}
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script type="text/javascript">

$(document).ready(function (){
var i = 0;
      $('#add').click(function(){
       i++;


    $('#fields tr:last').after('<tr id="row'+i+'" display="block" class="show_div"><td>{!!Form::text('codigo[]',null,['class'=>'form-control','data-toggle'=>"modal",'data-target'=>"#modal",'id'=>"codigo"])!!}</td><td>{!!Form::text('descripcion[]',null,['class'=>'form-control','id'=>'descripcion'])!!}</td><td>{!!Form::text('descrip_comercial[]',null,['class'=>'form-control'])!!}</td><td>{!!Form::text('cantidad[]',null,['class'=>'form-control'])!!}</td><td>{!!Form::select('gen_unidad_medida_id[]',$medidas,null,['class'=>'form-control','placeholder'=>'Seleccione'])!!}</td><td><button  name="remove" type="button" id="'+i+'" value="" class="btn btn-danger btn-remove">x</button></td></tr>');



       });



     $(document).on('click','.btn-remove',function(){
        var id_boton= $(this).attr("id");
        $("#row"+id_boton+"").remove();

     });


     function tipoArancel() {

     $('input:radio[name="tipo_arancel"]').change( function(){

      if($(this).is(':checked')) {

       if($(this).val()==1){

         return "nandina";

       }else{

         return "mercosur";
       }

   }

 });

 }



});



</script>
@stop
