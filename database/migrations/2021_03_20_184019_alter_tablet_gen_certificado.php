<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTabletGenCertificado extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('gen_certificado', function (Blueprint $table) {
            $table->string('razon_social_productor')->nullable()->after('rif_productor');
            $table->string('direccion_productor')->nullable()->after('razon_social_productor');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('gen_certificado', function (Blueprint $table) {
            $table->dropColumn('razon_social_productor');
            $table->dropColumn('direccion_productor');
        });
    }
}
