<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GenAgenteAduanalRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

        'nombre_agente'=>'required|min:3|max:100',
        'letra_rif_agente'=>'required',
        'rif_agente'=>'required|max:100',
        'numero_registro'=>'required|max:60',
        'ciudad_agente'=>'required|max:100',
        'telefono_agente'=>'required|numeric',
           
        ];
    }
}
