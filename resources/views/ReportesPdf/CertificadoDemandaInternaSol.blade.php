<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>CERTIFICADO DE DEMANDA INTERNA SATISFECHA</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style type="text/css">
        .centrar{
            text-align: center;
        }

        .titulo{
            
            font-weight: bold;
        }

       

        .derecha{
            text-align: left;

        }

        .text-normal{
            font-size: 14px;
        }

        .text-medio{
            font-size: 12px;
        }
        .text-11{
            font-size: 11px;
        }
        .text-small{
            font-size: 10px;
        }

        .text-9{
            font-size: 9px;
        }

        .text-8{
            font-size: 8px;
        }

        .espacio-superior{
            margin-top: 10px;
        }

        .espacio-superior-small{
            margin-top: 5px;
        }

        .ancho-medio{
            width: 50%;
        }

        .ancho-full{
            width: 100%;
        }

        .ancho-medio-alto{
            width: 75%;
        }

        .ancho-90{
            width: 90%;
        }

        .table{
            border-collapse: collapse;
        }
        
        .borde{
            border: 1px solid #000;
        }

        .sin-borde{
            border: none;
        }

        .top{
            vertical-align: top;
        }

        .under-dotted{    
            border-bottom: 1px dotted #000;
            text-decoration: none;
        }

        .content-1{
            width: 5%;
        }

        .content-9{
            width: 20%;
        }

        .sangria-1{
            margin-left: 28px;
        }

        .sangria-2{
            margin-left: 36px;
        }

        .sangria-3{
            margin-left: 54px;
        }

        .sangria-sub{
            margin-left: 14px;
            
        }

        
        .page-break { page-break-before: always; }

        .table-center{
            margin: 0 auto; 
        }

        .justify{
            text-align: justify;
        }

        td.justify {
            padding-left: 8px;
            padding-right: 8px; 
        }
        .lineal{
            
        }
        .lineal>div{
            max-width:91%;

        }
        .lineal>*{
           display: inline-block;
           vertical-align:top;
        }

        .sangria-lista{
            list-style-type: none;
            margin: 0;
            padding: 0;
            margin-bottom: 4px;
        }

        .sangria-lista>li{
            margin-top: 5px;
            padding-left: 10px;
            padding-right: 10px;
        }
    </style>
</head>
<body>
    <div class="centrar espacio-superior">
        <table class="ancho-full">
            <tr>
                <td class="top center" style="width: 70%; font-size:80%;">
                      <img class="img-responsive" style="width: 565px;" src="img/cintillo.png">
                </td>
            </tr>
        </table>
    </div>
    <div class="centrar espacio-superior">
        <table class="ancho-full borde">
            <tr>
                <td class="nro-correlativo" style="width: 70%; font-size:80%; text-align: left;">
                     <b>N°</b><b style="color: black">CVC</b>
                </td>
                 <td class="nro-correlativo" style="width: 70%; font-size:80%; text-align: right;">
                     <b>MPPAPT-CVC</b><b style="color: black"></b>
                </td>
            </tr>
        </table>
    </div>

    <table class="ancho-full borde">
        <tr>
            <td style="width: 70%;font-size:80%; text-align: left;">
                 <b>1- Exportador (Nombre:)</b> <br> {{ $users->razon_social}}
            </td>
             <td style="width: 70%;font-size:80%; text-align: left;">
                 <b>Certificado de Origen</b><br><p>CORPORACIÓN VENEZOLANA DEL CAFÉ</p><b style="color: black">
            </td>
             <td>
                <img class="img-responsive" style="width: 50px;" src="img/cafe-logo.png">
            </td>
        </tr>
    </table>

    <table class="ancho-full borde">
        <tr>
            <td style="width: 70%; font-size:80%; text-align: left;">
                 <b>2- Dirección para Notificaciones: </b> <br>{{$solcvc->direccion_notif}}
            </td>
             <td style="width: 70%; font-size:80%; text-align: left;">
                 <b>3- Número de Referencia Interna:</b> {{ $solcvc->num_serie}}<br><br>
                    <table class="ancho-full">
                        <tr>
                            <td style="width: 70%; font-size:90%; text-align: left;">
                                 <b>4a. Clave del <br>País</b><br>
                                    {{ $solcvc->clave_pais}}
                            </td>
                             <td style="width: 70%; font-size:90%; text-align: left;">
                                 <b>4b. Puerto de Embarque</b><br>{{$solcvc->GenAduanaSalida->daduana}}
                                  &nbsp;&nbsp;&nbsp;
                                  &nbsp;&nbsp;&nbsp;
                                 <b style="color: black">
                            </td>
                              <td style="width: 70%; font-size:90%; text-align: left;">
                                 <b>4c. Número de <br>Serie</b><br>
                                  {{ $solcvc->num_serie}}
                            </td>
                        </tr>
                    </table>
                 <b style="color: black">
            </td>
        </tr>
    </table>

    <table class="ancho-full borde">
        <tr>
            <td style="width: 70%; font-size:80%; text-align: left;">
                 <b>6- País de Destino: </b> <br>{{$solcvc->Pais->dpais}}
            </td>
             <td style="width: 70%; font-size:80%; text-align: left;">
               <table class="ancho-full">
                    <tr>
                        <td style="width: 70%; font-size:90%; text-align: left;">
                             <b>5- País Productor: </b><b style="color: black">&nbsp;VENEZUELA</b>
                              &nbsp; &nbsp; &nbsp;&nbsp; <b>168</b>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

    <table class="ancho-full borde">
        <tr>
            <td style="width: 70%; font-size:80%; text-align: left;">
                 <b>8- País de Trasbordo: </b> <br> {{$solcvc->PaisTransbordo->dpais}}
            </td>
             <td style="width: 70%; font-size:80%; text-align: left;">
               <table class="ancho-full">
                    <tr>
                        <td style="width: 70%; font-size:95%; text-align: left;">
                             <b>7- Fecha de Exportación (DD/MM/AA) </b>&nbsp;&nbsp;&nbsp;&nbsp; 
                             {{date('d/m/Y',strtotime($solcvc->fexportacion))}}
                             <br><br>
                            <tr>
                                <td style="width: 70%; font-size:95%; text-align: left;">
                                     <b>9- Nombre del Medio de Transporte:  </b>&nbsp;&nbsp;&nbsp;&nbsp;{{$solcvc->nombre_transporte}}
                                </td>
                            </tr>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

    <table class="ancho-full borde">
        <tr>
            <td style="width: 70%; font-size:80%; text-align: left;">
                 <b>10- Marca de Identificación: </b> <br> {{$solcvc->marca_ident}}
            </td>
             <td style="width: 70%; font-size:80%; text-align: left;">
               <table class="ancho-full">
                    <tr>
                        <td style="width: 100%; font-size:95%;">
                            <b>11- Cargados </b><br>
                        @foreach($tipoSoli as $data)
                            <input type="checkbox" style="display:inline" @if($data->id==Session::get('cat_cargados_id')) checked @endif >&nbsp;
                            @if($data->id == 1)
                            Sacos
                            @elseif($data->id == 2)
                            Granel
                            @elseif($data->id == 3)
                            Contenedores
                            @endif
                        @endforeach
                          </td>
                       
                    </tr>

                            <tr>
                                <td style="width: 70%; font-size:95%; text-align: left;">
                                     <b>12- Peso Neto de la Partida:  </b> <br> {{$solcvc->peso_neto}}
                                </td>
                                 <td style="width: 70%; font-size:85%; text-align: center;">
                                        <b>13- Unidad de Peso:  </b><br>
                                    @foreach($tipoSoliUniPeso as $data)
                                        <input type="checkbox" style="display:inline" @if($data->id==Session::get('gen_unidad_medida_id')) checked @endif >
                                        @if($data->id == 10)
                                        Kg
                                        @elseif($data->id == 13)
                                        Lb
                                        @elseif($data->id == 32)
                                        Tn
                                        @elseif($data->id == 45)
                                        Qn
                                        @endif
                                    @endforeach
                                </td> 
                            </tr>
                </table>
            </td>
        </tr>
    </table>

    <table class="ancho-full borde">
        <tr>
                    <td style="width: 100%; font-size:95%;">
                            <b>14- Descripción del Café (Formato, tipo, si procede):</b><br>
                        @foreach($tipoSoliCafe as $data)
                            <input type="checkbox" style="display:inline" @if($data->id==Session::get('cat_tipo_cafe_id')) checked @endif >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            {{$data->nombre_cafe}}
                        @endforeach
                    </td>
        </tr>
    </table>

    <table class="ancho-full borde">
        <tr>

            <td style="width: 100%; font-size:95%;">
                    <b>15- Metodo de Elaboración: </b><br>
                @foreach($tipoSoliElab as $data)
                    <input type="checkbox" style="display:inline" @if($data->id==Session::get('cat_metodo_elab_id')) checked @endif >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    {{$data->nombre_metodo}}
                @endforeach
            </td>
        </tr>
    </table>

    <table class="ancho-full borde">
        <tr>
            <td style="text-align: justify; font-size:10px;">16- POR EL PRESENTE SE CERTIFICA QUE EL CAFÉ ARRIBA DESCRITO FUE PRODUCIDO/BENEFICIADO EN EL PAÍS QUE SE INDICA EN LA CASILLA 5. EXPORTADO EN LA FECHA QUE SEGUIDAMENTE SE HACE CONSTAR ESTE CERTIFICADO SE EXPIDE EXCLUSIVAMENTE PARA FINES ESTADÍSTICOS Y PROCEDENCIA DE ORIGEN </td>
        </tr>
    </table>

    <table class="ancho-full borde">
        <tr>
            <td style="width: 70%; font-size:70%; text-align: left;">
                 Fecha:<br><br>
                 Localidad:<br>
                 <b>a. Firma del funcionario de Aduanas autorizado y refrendo de Aduana.</b><br>
            </td>
             <td style="width: 70%; font-size:70%; text-align: left;">
                 Fecha:<br><br>
                 Localidad:<br>
                 <b>b. Firma del funcionario del Organismo Certificante y refrendo del Organismo Certificante</b><br>
            </td>
        </tr>
    </table>

    <table class="ancho-full borde">
        <tr>
            <td style="text-align: justify; font-size:12px;">
                 <b>17- Otra informacion pertinente: ICC Resolucion Número 420; Caracteristicas especiales; Código del SA; Valor del embarque (Información voluntaria).</b>
            </td>
        </tr>
    </table>

    <table class="ancho-full borde">
        <tr>
            <td style="font-size:70%; text-align: justify;">
                <b>Normas optimas de calidad del café verde (ICC Resolución Número 420):</b><br>
                <p>"S" Plena observacia de las normas optimas sobre defectos y humedad.</p>
                <p>"XM" EL café no responde a las normas optimas sobre humedad.</p>
            </td>
            <td style="font-size:70%; text-align: left;">
                 <b></b><br>
                 @if($solcvc->norma_calidad_cafe == 'S')
                    <p><img src="img/checktrue.png" width="33" height="32"></p>
                 @else 
                    <p><img src="img/check.png"></p>
                 @endif
                 
                 @if($solcvc->norma_calidad_cafe == 'XM')
                 <p><img src="img/checktrue.png" width="33" height="32"></p>
                 @else 
                    <p><img src="img/check.png"></p>
                 @endif
            </td>
             <td style="font-size:70%; text-align: left;">
                 <b></b><br>
                 <p>"XD" EL café no responde a las normas optimas sobre defectos.</p>
                <p>"XDM" EL café no responde a ninguna de las normas optimas (ni la referente a defectos ni la referente a humedad).</p>
            </td>
            <td style="font-size:70%; text-align: left;">
                 <b></b><br>
                 @if($solcvc->norma_calidad_cafe == 'XD')
                 <p><img src="img/checktrue.png" width="33" height="32"></p>
                 @else 
                    <p><img src="img/check.png"></p>
                 @endif
                 @if($solcvc->norma_calidad_cafe == 'XDM')
                 <p><img src="img/checktrue.png" width="33" height="32"></p>
                 @else 
                    <p><img src="img/check.png"></p>
                 @endif
            </td>
        </tr>
    </table>

    <table class="ancho-full borde">
        <tr>
            <td style="text-align: justify; font-size:12px;">
                 <b>b- Caracteristicas especiales (especifique el nombre o el codigo): </b>
                 {{$solcvc->caract_especiales}} 
            </td>
        </tr>
    </table>

    <table class="ancho-full borde">
        <tr>
            <td style="text-align: justify; font-size:12px;">
                 <b>c- Codigo del Sistema Armonizado (SA):</b> <br> Codigo del SA / HS Code: {{$solcvc->codio_sist_sa}} 
            </td>
            <td style="text-align: justify; font-size:12px;">
                    <b>d- Valor (FOB) del embarque:</b><br>
                @foreach($tipoSoliDivisa as $data)
                    <input type="checkbox" style="display:inline" @if($data->id==Session::get('gen_divisa_id')) checked @endif >&nbsp;&nbsp;&nbsp;
                    @if($data->id == 18)
                        Dolares
                    @elseif($data->id == 26)
                        Euros
                    @elseif($data->id == 32)
                        Moneda Nacional
                    @endif
                @endforeach
            </td>   
        </tr>
    </table>

        <table class="ancho-full borde">
        <tr>
            <td style="text-align: justify; font-size:12px;">
                 <b>e- Informacion adicional: </b>{{$solcvc->info_adicional}} 
            </td>
        </tr>
    </table>






















</body>
</html>