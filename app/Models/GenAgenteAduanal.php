<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GenAgenteAduanal extends Model
{
    protected $table = 'gen_agente_aduanal';

    protected $fillable = [

           'gen_usuario_id',
           'nombre_agente',
           'letra_rif_agente',
           'rif_agente',
           'numero_registro',
           'ciudad_agente',
           'telefono_agente',
           'bactivo',
    ];

    protected $date =['created_at','updated_at'];

    public function usuario() {

     return $this->hasOne('App\Models\GenUsuario','id','gen_usuario_id');

    
    }















}
