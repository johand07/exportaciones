<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GenDocumentosInversiones extends Model
{
    protected $table = 'gen_documentos_inversiones';

    protected $fillable = [

    	   'gen_sol_inversionista_id',
           'file_1',
           'file_2',
           'file_3',
           'file_4',
           'file_5',
           'file_6',
           'file_7',
           'file_8',
           'file_9',
           'file_10',
           'file_11',
           'file_12',
           'file_13',
           'file_14',
           'file_15',
           'file_16',
           'file_17',
           'file_18',
           'file_19',
           'file_20',
           'file_21',
           'bactivo',
    ];

    protected $date =['created_at','updated_at'];

  	
  	public function GenSolInversionista() {

    	return $this->hasOne('App\Models\GenSolInversionista','id','gen_sol_inversionista_id');

	}

}
