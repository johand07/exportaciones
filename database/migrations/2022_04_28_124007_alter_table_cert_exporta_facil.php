<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableCertExportaFacil extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cert_exporta_facil', function (Blueprint $table) {
            
            $table->string('codigo', 200)->nullable()->after('direccion_receptor');
            $table->string('regimen', 200)->nullable()->after('codigo');
            $table->date('fecha')->after('regimen');
            $table->string('nombre_repre_export', 200)->nullable()->after('fecha');
            $table->string('telefono_exportador', 200)->nullable()->after('nombre_repre_export');
            $table->string('rud', 200)->nullable()->after('telefono_exportador');
            $table->unsignedInteger('pais_origen')->index()->nullable()->after('rud');
            $table->foreign('pais_origen')->references('id')->on('pais');
            $table->string('cod_postal', 200)->nullable()->after('pais_origen');
            $table->string('ciudad', 200)->nullable()->after('cod_postal');
            $table->date('fecha_embarque')->after('ciudad');
            $table->string('cod_postal_destino', 200)->nullable()->after('fecha_embarque');
            $table->string('ciudad_destino', 200)->nullable()->after('cod_postal_destino');
            $table->string('total_fob')->nullable()->after('ciudad_destino');
            $table->string('total_bultos')->nullable()->after('total_fob');
            $table->string('tipo_servicio')->nullable()->after('total_bultos');
            $table->string('valor_total_flete_sinIva')->nullable()->after('tipo_servicio');
            $table->string('valor_total_flete_conIva')->nullable()->after('valor_total_flete_sinIva');
            $table->string('total_seguro')->nullable()->after('valor_total_flete_conIva');
            $table->string('telefono_destinario', 200)->nullable()->after('total_seguro');
            $table->string('destinario', 200)->nullable()->after('telefono_destinario');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $table->dropColumn('codigo');
        $table->dropColumn('regimen');
        $table->dropColumn('fecha');
        $table->dropColumn('nombre_repre_export');
        $table->dropColumn('telefono_exportador');
        $table->dropColumn('rud');
        $table->dropColumn('pais_origen');
        $table->dropColumn('cod_postal');
        $table->dropColumn('ciudad');
        $table->dropColumn('fecha_embarque');
        $table->dropColumn('cod_postal_destino');
        $table->dropColumn('ciudad_destino');
        $table->dropColumn('total_fob');
        $table->dropColumn('total_bultos');
        $table->dropColumn('tipo_servicio');
        $table->dropColumn('valor_total_flete_sinIva');
        $table->dropColumn('valor_total_flete_conIva');
        $table->dropColumn('total_seguro');
        $table->dropColumn('telefono_destinario');
        $table->dropColumn('destinario');
    }
}
