<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CatMetodoElab extends Model
{
    protected $table = 'cat_metodo_elab';

    protected $guarded = [];
}
