@extends('templates/layoutlte')
@section('content')

@if(isset($detusuario))
	@if( isset($detusuario->ruta_doc) || isset($detusuario->ruta_rif) || isset($detusuario->ruta_reg_merc))
		@include('DocumentosSoportes.edit')
		
	@else
		
		@include('DocumentosSoportes.create')
	@endif
@else
	Usuario no registrado
@endif

@stop
