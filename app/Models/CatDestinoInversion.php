<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CatDestinoInversion extends Model
{
    protected $table = 'cat_destino_inversion';
    
    protected $fillable = [
        'nombre_destino',
    ];
}
