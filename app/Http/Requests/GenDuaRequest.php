<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GenDuaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          'numero_dua'=>'required',
          'numero_referencia'=>'required',
          'gen_agente_aduana_id'=>'required',
          'gen_transporte_id'=>'required',
          'gen_consignatario_id'=>'required',
          'gen_aduana_salida_id'=>'required',
          'lugar_salida'=>'required|min:3',
          'aduana_llegada'=>'required',
          'lugar_llegada'=>'required',
          'fregistro_dua_aduana'=>'required',
          'fembarque'=>'required',
          'farribo'=>'required',
        ];
    }
}
