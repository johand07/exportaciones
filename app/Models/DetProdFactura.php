<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class DetProdFactura extends Model
{
    protected $table='det_prod_factura';

    protected $fillable =[

     'id',
     'gen_factura_id',
     'unidad_medida_id',
     'codigo_arancel',
     'descripcion_arancelaria',
     'descripcion_comercial',
     'cantidad_producto',
     'precio_producto',
     'permiso_id',
     'disponibles',
     'monto_total',
     'bactivo'


    ];


    public function unidadesMedidas(){

     return $this->hasOne('App\Models\GenUnidadMedida','id','unidad_medida_id');
    }

     public function permiso(){

     return $this->hasOne('App\Models\permiso','id','permiso_id');
    }

     public function factura(){

     return $this->hasOne('App\Models\GenFactura','id','gen_factura_id');
    }


}
