<?php

namespace App\Http\Controllers;

use App\Models\BandejaCoordinadorCertificado;
use App\Models\GenCertificado;
use App\Models\GenStatus;
use App\Models\DetUsuario;
use App\Models\DetProductosCertificado;
use App\Models\DetDeclaracionCertificado;
use App\Models\ImportadorCertificado;
use App\Models\DocumentosCertificado;
use Illuminate\Http\Request;
use Auth;
use Laracasts\Flash\Flash;
use Alert;

class CoordinadorCertificadOrigenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $titulo= 'Verificación Certificado de Origen';
        $descripcion= 'Perfil del Coordinador';

        /***Consulta de solicitudes de certificado de Origen****/

        //$coordinadorCertificado=BandejaCoordinadorCertificado::where('gen_usuario_id',Auth::user()->id)->orderBy('id','ASC')->get();
        $coordinadorCertificado=GenCertificado::where('bactivo',1)->with('rBandejaCoordinador')->with('rBandejaAnalista.GenUsuario')->get();

        // dd($coordinadorCertificado);



        return view('CoordinadorCertificadoOrigen.index',compact('titulo','descripcion','coordinadorCertificado'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
            
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\BandejaCoordinadorCertificado  $bandejaCoordinadorCertificado
     * @return \Illuminate\Http\Response
     */
    public function show(BandejaCoordinadorCertificado $bandejaCoordinadorCertificado, $id)
    {
        $titulo= 'Verificación Certificado de Origen';
        $descripcion= 'Perfil del Coordinador';

        $certificados=GenCertificado::find($id);

        /**********Consultas de los tipos de certificados******/
        $tipocertificado=$certificados->gen_tipo_certificado_id;

        $validaratencioncert=BandejaCoordinadorCertificado::where('gen_certificado_id',$certificados->id)->where('gen_usuario_id',Auth::user()->id)->first();

        /********Consulta de los estatus de observacion del coordinador***/

        $estado_observacion=GenStatus::whereIn('id', [11,16,17])->pluck('nombre_status','id');


        $det_usuario=DetUsuario::where('gen_usuario_id',$certificados->gen_usuario_id)->first();
      

        /************Consulta del detalle de productos  donde el id de ese certificado exista**/

        $detalleProduct=DetProductosCertificado::where('gen_certificado_id',$certificados->id)->get();


        /************Consulta del detalle de Declaracion certificado para traerme todas***/

        $detdeclaracionCert=DetDeclaracionCertificado::where('gen_certificado_id',$certificados->id)->get();

      /************Consulta para traerme los datos del importador certificado para traerme todas***/

      $importadorCertificado=ImportadorCertificado::where('gen_certificado_id',$certificados->id)->first();

      

        $docCertificados=DocumentosCertificado::where('gen_certificado_id',$certificados->id)->first();



       if(!empty($docCertificados['file_1'])){
           $exten_file_1=explode(".", $docCertificados['file_1']);
          $extencion_file_1 = $exten_file_1[1];
      } else {
          $extencion_file_1 = '';
      }
      if (!empty($docCertificados['file_2'])) {
          $exten_file_2=explode(".", $docCertificados['file_2']);
          $extencion_file_2 = $exten_file_2[1];
      }else {
          $extencion_file_2 = '';
      }
      if (!empty($docCertificados['file_3'])) {
          $exten_file_3=explode(".", $docCertificados['file_3']);
          $extencion_file_3 = $exten_file_3[1];
      }else {
          $extencion_file_3 = '';
      }
      if (!empty($docCertificados['file_4'])) {
          $exten_file_4=explode(".", $docCertificados['file_4']);
          $extencion_file_4 = $exten_file_4[1];
      }else {
          $extencion_file_4 = '';
      }
      if (!empty($docCertificados['file_5'])) {
          $exten_file_5=explode(".", $docCertificados['file_5']);
          $extencion_file_5 = $exten_file_5[1];
      }else {
          $extencion_file_5 = '';
      }
      if (!empty($docCertificados['file_6'])) {
          $exten_file_6=explode(".", $docCertificados['file_6']);
          $extencion_file_6 = $exten_file_6[1];
      }else {
          $extencion_file_6 = '';
      }


      return view('CoordinadorCertificadoOrigen.show',compact('titulo','descripcion','estado_observacion','certificados','detalleProduct','detdeclaracionCert','importadorCertificado','det_usuario','validaratencioncert','tipocertificado','docCertificados','extencion_file_1','extencion_file_2','extencion_file_3','extencion_file_4','extencion_file_5','extencion_file_6'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\BandejaCoordinadorCertificado  $bandejaCoordinadorCertificado
     * @return \Illuminate\Http\Response
     */
    public function edit(BandejaCoordinadorCertificado $bandejaCoordinadorCertificado,$id)
    {
        $titulo= 'Verificación Certificado de Origen';
        $descripcion= 'Perfil del Coordinador';
       
        //$id=Auth::user()->id;

         $certificados=GenCertificado::find($id);
        
        //dd($certificados);

        /**********Consultas de los tipos de certificados******/
        $tipocertificado=$certificados->gen_tipo_certificado_id;

        //dd($tipocertificados);


        $validaratencioncert=BandejaCoordinadorCertificado::where('gen_certificado_id',$certificados->id)->where('gen_usuario_id',Auth::user()->id)->first();
         //dd($validaratencioncert);

         if(empty($validaratencioncert)){
            /*Consulto mis solicitudes con el status id 10 atendido*/
            $certificados->gen_status_id=14;
            $certificados->save();
             /***Inserto en la bandeja coordinadorcertificado me traigo el id de esa solicitud con el usuario   analista le actualizo el status a 14 que es en Observacion la actualizo a la fecha de hoy****/
            $certificadosOrig= new BandejaCoordinadorCertificado;
            $certificadosOrig->gen_certificado_id=$certificados->id;
            $certificadosOrig->gen_usuario_id=Auth::user()->id;
            $certificadosOrig->gen_status_id=14;
            $certificadosOrig->fstatus=date('Y-m-d');
            $certificadosOrig->save();

          }

          /********Consulta de los estatus de observacion del coordinador***/

          $estado_observacion=GenStatus::whereIn('id', [11,16,17])->pluck('nombre_status','id');


          $det_usuario=DetUsuario::where('gen_usuario_id',$certificados->gen_usuario_id)->first();
        
          //dd($det_usuario);

          /************Consulta del detalle de productos  donde el id de ese certificado exista**/

          $detalleProduct=DetProductosCertificado::where('gen_certificado_id',$certificados->id)->get();
          //dd($detalleProduct);


          /************Consulta del detalle de Declaracion certificado para traerme todas***/

          $detdeclaracionCert=DetDeclaracionCertificado::where('gen_certificado_id',$certificados->id)->get();
          //dd($detdeclaracionCert);

        /************Consulta para traerme los datos del importador certificado para traerme todas***/

        $importadorCertificado=ImportadorCertificado::where('gen_certificado_id',$certificados->id)->first();

        


         $docCertificados=DocumentosCertificado::where('gen_certificado_id',$certificados->id)->first();

          // dd($exten_file_1);
        // 
        //$extencion_file_1 = $exten_file_1[1];

         if(!empty($docCertificados['file_1'])){
             $exten_file_1=explode(".", $docCertificados['file_1']);
            $extencion_file_1 = $exten_file_1[1];
        } else {
            $extencion_file_1 = '';
        }
        if (!empty($docCertificados['file_2'])) {
            $exten_file_2=explode(".", $docCertificados['file_2']);
            $extencion_file_2 = $exten_file_2[1];
        }else {
            $extencion_file_2 = '';
        }
        if (!empty($docCertificados['file_3'])) {
            $exten_file_3=explode(".", $docCertificados['file_3']);
            $extencion_file_3 = $exten_file_3[1];
        }else {
            $extencion_file_3 = '';
        }
        if (!empty($docCertificados['file_4'])) {
            $exten_file_4=explode(".", $docCertificados['file_4']);
            $extencion_file_4 = $exten_file_4[1];
        }else {
            $extencion_file_4 = '';
        }
        if (!empty($docCertificados['file_5'])) {
            $exten_file_5=explode(".", $docCertificados['file_5']);
            $extencion_file_5 = $exten_file_5[1];
        }else {
            $extencion_file_5 = '';
        }
        if (!empty($docCertificados['file_6'])) {
            $exten_file_6=explode(".", $docCertificados['file_6']);
            $extencion_file_6 = $exten_file_6[1];
        }else {
            $extencion_file_6 = '';
        }


        return view('CoordinadorCertificadoOrigen.edit',compact('titulo','descripcion','estado_observacion','certificados','detalleProduct','detdeclaracionCert','importadorCertificado','det_usuario','validaratencioncert','tipocertificado','docCertificados','extencion_file_1','extencion_file_2','extencion_file_3','extencion_file_4','extencion_file_5','extencion_file_6'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\BandejaCoordinadorCertificado  $bandejaCoordinadorCertificado
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BandejaCoordinadorCertificado $bandejaCoordinadorCertificado,$id)
    {
        //dd($request->all());
        if(!empty($request->observacion_coordinador ) && $request->gen_status_id==11){
         //dd(1);
                 $certificados=GenCertificado::find($id)->update(['gen_status_id'=>11,'observacion_coordinador'=>$request->observacion_coordinador,'updated_at' => date("Y-m-d H:i:s") ]);

                 $bandejaCoordinadorCertificado=BandejaCoordinadorCertificado::where('gen_certificado_id',$id)->update(['gen_status_id'=>11,'updated_at' => date("Y-m-d H:i:s") ]);

               
            }elseif($request->gen_status_id == 16){
              //dd(2);
              $certificados=GenCertificado::find($id)->update(['gen_status_id'=>16,'observacion_coordinador'=>$request->observacion_coordinador,'fecha_certicado'=>$request->fecha_certicado,'updated_at' => date("Y-m-d H:i:s") ]);

               $bandejaCoordinadorCertificado=BandejaCoordinadorCertificado::where('gen_certificado_id',$id)->update(['gen_status_id'=>16,'updated_at' => date("Y-m-d H:i:s") ]);

            }else{
              //dd(3);
              $certificados=GenCertificado::find($id)->update(['gen_status_id'=>17,'observacion_coordinador'=>$request->observacion_coordinador,'updated_at' => date("Y-m-d H:i:s") ]);

               $bandejaCoordinadorCertificado=BandejaCoordinadorCertificado::where('gen_certificado_id',$id)->update(['gen_status_id'=>17,'updated_at' => date("Y-m-d H:i:s") ]);

            }

            Alert::warning('Su Solicitud ha sido Atendida ')->persistent("OK");
            return redirect()->action('CoordinadorCertificadOrigenController@index'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\BandejaCoordinadorCertificado  $bandejaCoordinadorCertificado
     * @return \Illuminate\Http\Response
     */
    public function destroy(BandejaCoordinadorCertificado $bandejaCoordinadorCertificado)
    {
        //
    }
}
