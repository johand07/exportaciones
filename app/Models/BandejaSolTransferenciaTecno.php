<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BandejaSolTransferenciaTecno extends Model
{
    protected $table = 'bandeja_sol_transfer_tecno';

    protected $fillable = [

    'Gen_trasfer_tecno_inversion_id',
    'gen_usuario_id',
    'gen_status_id',
    'fstatus',
    'bactivo',
   ];


	public function rtransftecno() {

     return $this->hasOne('App\Models\GenUsuario','id','Gen_trasfer_tecno_inversion_id');

    }

     public function rtransftecnouser() {

     return $this->hasOne('App\Models\GenUsuario','id','gen_usuario_id');

    }
    

    public function statustranfer() {

     return $this->hasOne('App\Models\GenStatus','id','gen_status_id');

    }

}
