<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableImportadorCertificado extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('importador_certificado', function (Blueprint $table) {
            $table->string('rif_importador')->nullable()->after('gen_certificado_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('importador_certificado', function (Blueprint $table) {
            $table->dropColumn('rif_importador');
        });
    }
}
