@extends('templates/layoutlte')

@section('content')

{!!Form::open(['route'=>'AgenteAduanal.store','method'=>'POST','id'=>'formagenteaduanal']) !!}
<div class="panels">
      <div class="panel" id="one-panel">
<div class="container">
	<div class="row">
		<div class="12">
			<div class="col-md-3">
			</div>
			<div class="col-md-6">

				{!! Form::hidden('gen_usuario_id',Auth::user()->id,['readonly'=>'true'])!!}
				<div class="form-group"><br>
					{!! Form::label('','Nombre o Razón Social')!!}
					{!! Form::text('nombre_agente',null,['class'=>'form-control','id'=>'nombre_agente','onkeypress'=>'return soloNumerosyLetras(event)']) !!}

        		</div>

        		<div class="form-group">
        			{!! Form::label('','Rif')!!}
              <table style="width: 100%">
              <tr>
                <td style="width: 10%">
                  <select class="form-control" name="letra_rif_agente">
                    <option value="V-">V</option>
                    <option value="E-">E</option>
                    <option value="J-">J</option>
                    <option value="G-">G</option>
                    <option value="P-">P</option>
                  </select>
                </td>
                <td>
                  {!!Form::text('rif_agente',null,['class'=>'form-control','id'=>'rif_agente','maxlength'=>'9','onkeypress'=>'return soloNumeros(event)'])!!}
                </td>
              </tr>
            </table>


        		</div>
        		<div class="form-group">
        			{!! Form::label('','Número de Registro')!!}
          			{!!Form::text('numero_registro',null,['class'=>'form-control','id'=>'numero_registro','maxlength'=>'60','onkeypress'=>'return soloNumerosyLetras(event)'])!!}
        		</div>
        		<div class="form-group">
        			{!! Form::label('','Ciudad')!!}
          			{!!Form::text('ciudad_agente',null,['class'=>'form-control','id'=>'ciudad_agente','maxlength'=>'100','onkeypress'=>'return soloNumerosyLetras(event)'])!!}
        		</div>
        		<div class="form-group">
        			{!! Form::label('','Teléfono')!!}
          			{!!Form::text('telefono_agente',null,['class'=>'form-control','id'=>'telefono_agente','onkeypress'=>'return soloNumeros(event)','maxlength'=>'13'])!!}
        		</div>
			</div>
			<div class="col-md-3">
      		</div>
		</div>
	</div>
	<div class="row text-center"><br><br>
		<a class="btn btn-primary" href="{{url('exportador/AgenteAduanal')}}">Regresar</a>
     	<input type="submit" name="Enviar" class="btn btn-success center" value="Enviar" onclick="validacionForm()">
	</div>
</div>
</div>
</div>
{{Form::close()}}


<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="{{asset('js/ajax_v2.1.1.js')}}"></script>

<script type="text/javascript">
  	
	//Validación de formulario, se puede llamar y pasar la sección que contenga los inputs a validar

    	var error = '<div class="msg_alert"><span style="color:red" class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span><strong id="cod_aran-error" class="added"> El campo es obligatorio.</strong></div>';
		var valido = '<div class="msg_alert"><span style="color:green" class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span></div>';
		var mens = ['Estimado Usuario. Debe completar los campos solicitados']

		validacionForm = function() {

		$('#formagenteaduanal').submit(function(event) {
		
		var campos = $('#formagenteaduanal').find('input:text, select');
		var n = campos.length;
		var err = 0;

		$("div").remove(".msg_alert");
		//bucle que recorre todos los elementos del formulario
		for (var i = 0; i < n; i++) {
				var cod_input = $('#formagenteaduanal').find('input:text, select').eq(i);
				if (!cod_input.attr('noreq')) {
					if (cod_input.val() == '' || cod_input.val() == null)
					{
						err = 1;
						cod_input.css('border', '1px solid red').after(error);
					}
					else{
						if (err == 1) {err = 1;}else{err = 0;}
						cod_input.css('border', '1px solid green').after(valido);
					}
					
				}
		}

		//Si hay errores se detendrá el submit del formulario y devolverá una alerta
		if(err==1){
				event.preventDefault();
				swal("Por Favor!", mens[0], "warning")
			}

			});
		}
</script>


@stop
