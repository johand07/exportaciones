<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>CERTIFICADO DE ORIGEN ACUERDO DE ALCANCE PARCIAL COLOMBIA - VENEZUELA</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style type="text/css">
        .centrar{
            text-align: center;
        }

        .titulo{
            
            font-weight: bold;
        }

        .nro-correlativo{
            color: red;
            font-size: 16px;
        }

        .derecha{
            text-align: right;

        }

        .text-normal{
            font-size: 14px;
        }

        .text-medio{
            font-size: 12px;
        }
        .text-11{
            font-size: 11px;
        }
        .text-small{
            font-size: 10px;
        }

        .text-9{
            font-size: 8px;
        }

        .text-8{
            font-size: 8px;
        }

        .espacio-superior{
            margin-top: 10px;
        }

        .espacio-superior-small{
            margin-top: 5px;
        }

        .ancho-medio{
            width: 50%;
        }

        .ancho-full{
            width: 100%;
        }

        .ancho-medio-alto{
            width: 75%;
        }

        .ancho-90{
            width: 90%;
        }

        .table{
            border-collapse: collapse;
        }
        
        .borde{
            border: 1px solid #000;
        }

        .sin-borde{
            border: none;
        }

        .top{
            vertical-align: top;
        }

        .under-dotted{    
            border-bottom: 1px dotted #000;
            text-decoration: none;
        }

        .content-1{
            width: 5%;
        }

        .content-9{
            width: 20%;
        }

        .sangria-1{
            margin-left: 28px;
        }

        .sangria-2{
            margin-left: 36px;
        }

        .sangria-3{
            margin-left: 54px;
        }

        .sangria-sub{
            margin-left: 14px;
            
        }

        
        .page-break { page-break-before: always; }

        .table-center{
            margin: 0 auto; 
        }

        .justify{
            text-align: justify;
        }

        td.justify {
            padding-left: 8px;
            padding-right: 8px; 
        }
        .lineal{
            
        }
        .lineal>div{
            max-width:91%;

        }
        .lineal>*{
           display: inline-block;
           vertical-align:top;
        }

        .sangria-lista{
            list-style-type: none;
            margin: 0;
            padding: 0;
            margin-bottom: 4px;
        }

        .sangria-lista>li{
            margin-top: 5px;
            padding-left: 10px;
            padding-right: 10px;
        }
    </style>
</head>
<body>
    
    <!--div class="derecha titulo text-medio"><b style="color:red">N°</b> del Certificado <span class="">{{ $certificado->num_certificado}}</span></div-->
    <div class="centrar titulo text-normal espacio-superior">CERTIFICADO DE ORIGEN<br>ACUERDO DE ALCANCE PARCIAL COLOMBIA - VENEZUELA</div>
    <div class="centrar espacio-superior">
        <table border="0" class="ancho-full">
        <tr>
            <td class="ancho-medio centrar">
            <div class="text-normal">(1) PAÍS EXPORTADOR: <u>{{$certificado->pais_exportador}}</u></div>
            </td>
            <td class="ancho-medio centrar">
            <div class="text-normal">(2) PAÍS IMPORTADOR: <u>{{$certificado->pais_importador}}</u></div>
            </td>
        </tr>
        </table>
    </div>

    <div class="centrar espacio-superior">
        <table class="ancho-full borde table">
            <tr class="text-11 ">
                <td class="centrar top titulo borde" style="width: 13%;">(3) No. de Orden</td>
                <td class="centrar top titulo borde" style="width: 19%;">(4) Código Arancelario <br>en la nomenclatura de<br> la parte exportadora</td>
                <td class="centrar top titulo borde" style="width: 29%;">(5) Denominación de las Mercancías <br>descripción arancelaria y comercial</td>
                <td class="centrar top titulo borde" style="width: 10%;">(6) Unidad <br> física y <br>Cantidad<br> según <br>Factura</td>
                <td class="centrar top titulo borde" style="width: 17%;">(7) Valor FOB de cada <br>Mercancía según <br>factura en US$</td>
                <td class="centrar top titulo borde" style="width: 13%;">(8) Cantidad según factura para cupos Capítulo 72</td>
            </tr>
           
            @foreach($detalleProduct as $value)
            
            <tr class="text-medio">
                <td class="centrar top">{{$value->num_orden}}</td>
                <td class="centrar top">{{$value->codigo_arancel}}</td>
                <td class="centrar top">{{$value->denominacion_mercancia}}</td>
                <td class="centrar top">{{$value->unidad_fisica}}</td>
                <td class="centrar top">{{$value->valor_fob}}</td>
                <td class="centrar top">{{$value->cantidad_segun_factura}}</td>
            </tr>
            
            
            @endforeach

            @if(count($detalleProduct)<=1)
                @for($auxConut = 0; $auxConut<=3-count($detalleProduct); $auxConut++)
                <tr class="text-medio">
                    <td class="centrar top">&nbsp;</td>
                    <td class="centrar top">&nbsp;</td>
                    <td class="centrar top">&nbsp;</td>
                    <td class="centrar top">&nbsp;</td>
                    <td class="centrar top">&nbsp;</td>
                    <td class="centrar top">&nbsp;</td>
                </tr>
                @endfor
            @endif
        
        </table>
    </div>
    <div class="centrar titulo text-medio espacio-superior">(9) DECLARACION DE ORIGEN</div>
    <div class="text-medio espacio-superior">DECLARAMOS que las mercancías indicadas en el presente formulario, correspondientes a la(s) Factura(s) Comercial(es) No. <u>{{ $detdeclaracionCert[0]->numero_factura }}</u>
    de fecha <u>{{ $detdeclaracionCert[0]->f_fac_certificado }}</u> cumplen con lo establecido en las Normas de Origen del presente Acuerdo, de conformidad con el siguiente desglose.</div>

    <div class="centrar espacio-superior">
        <table class="ancho-full borde table">
            <tr class="text-medio">
                <td class="centrar top titulo borde" style="width: 15%;">(10) No. de Orden</td>
                <td class="centrar top titulo borde" style="width: 85%;">(11) Criterio para la Calificación de Origen</td>
            </tr>
            @foreach($detdeclaracionCert as $value1)
            
                <tr class="text-center">
                    <td class="centrar top">{{$value1->num_orden_declaracion}}</td>
                    <td class="centrar top">{{$value1->normas_criterio}}</td>
                </tr>

            @endforeach

            @if(count($detdeclaracionCert)<=1)
                @for($auxConut = 0; $auxConut<=3-count($detdeclaracionCert); $auxConut++)
                <tr class="text-medio">
                    <td class="centrar top">&nbsp;</td>
                    <td class="centrar top">&nbsp;</td>
                </tr>
                @endfor
            @endif
        
        </table>
    </div>

    <div class="centrar espacio-superior">
        <table class="ancho-full table borde">
            <tr class="text-medio">
                <td class="top titulo borde" style="width: 65%;">(12) PRODUCTOR O EXPORTADOR</td>
                 <td class="top titulo borde" colspan="2">(13) Sello Y firma del Productor o Exportador</td>
            </tr>
           
            <tr class="text-medio">
                <td class="top borde">12.1 Nombre o Razón Social: {{ $certificado->GenUsuario->DetUsuario->razon_social }}</td>
                <td colspan="2"></td>
            </tr>
            <tr class="text-medio">
                <td class="top borde">12.2 Número de Identificación Fiscal: {{ $certificado->GenUsuario->DetUsuario->rif }}</td>
                <td colspan="2"></td>
            </tr>
            <tr class="text-medio">
                <td class="top borde">12.3 Dirección: {{ $certificado->GenUsuario->DetUsuario->direccion }}</td>
                <td colspan="2"></td>
            </tr>
            <tr class="text-medio">
                <td class="top borde">12.4 País y Ciudad: {{ strtoupper($certificado->pais_cuidad_exportador) }}</td>
                <td colspan="2"></td>
            </tr>
            <tr class="text-medio">
                <td class="top borde">12.5 E-mail: {{ $certificado->GenUsuario->DetUsuario->correo }}</td>
                <td colspan="2"></td>
            </tr>
            <tr class="text-medio">
                <td class="top borde">12.6 Teléfono:{{$certificado->GenUsuario->DetUsuario->telefono_movil}}</td>
                <td colspan="2"></td>
            </tr>
            <tr class="text-medio">
                <td class="top borde">12.7 Fecha de Emisión: {{ $certificado->fecha_emision_exportador}}</td>
                <td colspan="2"></td>
            </tr>

            <tr class="text-medio">
                <td class="top titulo borde">(14) IMPORTADOR</td>
                <td colspan="2"></td>
            </tr>
            <tr class="text-medio">
                <td class="top borde">14.1 Nombre o Razón Social: {{ strtoupper($importadorCertificado->razon_social_importador) }}</td>
                <td colspan="2"></td>
            </tr>
            <tr class="text-medio">
                <td class="top borde">14.2 Dirección: {{ strtoupper($importadorCertificado->direccion_importador) }}</td>
                <td colspan="2"></td>
            </tr>
            <tr class="text-medio">
                <td class="top borde">14.3 País y Ciudad: {{ strtoupper($importadorCertificado->pais_ciudad_importador) }}</td>
                <td colspan="2"></td>
            </tr>
            <tr class="text-medio">
                <td class="top borde">14.4 E-mail: {{ $importadorCertificado->email_importador}}</td>
                <td colspan="2"></td>
            </tr>
            <tr class="text-medio">
                <td class="top borde">14.5 Teléfono: {{ $importadorCertificado->telefono}}</td>
                <td colspan="2"></td>
            </tr>
            <tr class="text-medio">
                <td class="top borde">(15) Medio de Transporte (si es conocido): {{ ($importadorCertificado->medio_transporte_importador) }}</td>
                <td colspan="2"></td>
            </tr>
            <tr class="text-medio">
                <td class="top borde">(16) Puerto o Lugar de Embarque (si es conocido): {{ ($importadorCertificado->lugar_embarque_importador) }}</td>
                <td colspan="2"></td>
            </tr>
        </table>
        <div class="text-normal borde table justify"><b>17. OBSERVACIONES</b><br><br>{{$certificado->observaciones}}<br><br></div>
    </div>

    <div class="centrar espacio-superior">
        <table class="ancho-full borde table">
            <tr class="text-medio">
                <td class="top titulo" style="width: 50%;">(18) CERTIFICACIÓN DE ORIGEN</td>
                <td class="top text-small borde" style="width: 50%;" rowspan="2">(19) Nombre y firma del funcionario habilitado y sello de la Autoridad Competente</td>
            </tr>
            <tr class="text-medio">
                <td class="top text-small" style="width: 50%;"><div class="">Certifico la veracidad de la presente Declaracion en la ciudad de:<br><span class="under-dotted espacio-superior">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><br><span class="espacio-superior">A los: <u>{{date('d')}}</u>/<u>{{date('m')}}</u>/<u>{{date('Y')}}</u><br><br></span></div></td>
            </tr>
        </table>
    </div>

<div class="page-break"></div>
<div class="centrar titulo text-small">INSTRUCTIVO PARA EL LLENADO DEL CERTIFICADO DE ORIGEN<br>ACUERDO DE ALCANCE PARCIAL COLOMBIA - VENEZUELA</div>
<div class="titulo text-9 espacio-superior">CAMPOS DEL CERTIFICADO DE ORIGEN</div>

<div class="text-9 sangria-3"><b>NÚMERO DEL CERTIFICADO:</b> Corresponde a un número que la Autoridad Competente asigna a los certificados de origen que emite. Este campo solo debe ser llenado por dicha<br/> autoridad</div>
<div class="lineal"><b class="text-9 sangria-1">(1)</b><div class="text-9 sangria-sub"><b>PAÍS EXPORTADOR:</b> Indicar el nombre del país de exportacion del cual la mercancía es originaria.</div></div>
<div class="lineal"><b class="text-9 sangria-1">(2)</b><div class="text-9 sangria-sub"><b>PAÍS IMPORTADOR:</b> Indicar el nombre del país de destino de la mercancía a exportar.</div></div>
<div class="lineal"><b class="text-9 sangria-1">(3)</b><div class="text-9 sangria-sub"><b>NÚMERO DE ORDEN:</b> Numerar en forma consecutiva las mercancias que ampara el Certificado. En caso que el espacio sea insuficiente, se continuará la numeración de la mercancía en otro ejemplar, situación que se deberá indicar en el campo de "Observaciones".</div></div><!--
--><div class="lineal"><b class="text-9 sangria-1">(4)</b><div class="text-9 sangria-sub"><b>CÓDIGO ARANCELARIO:</b> Indicar la clasificación arancelaria de la mercancia a exportar utilizando la nomenclatura vigente de la parte exportadora.</div></div>
<div class="lineal"><b class="text-9 sangria-1">(5)</b><div class="text-9 sangria-sub"><b>DENOMINACIÓN DE LAS MERCANCÍAS:</b> Indicar la descripción arancelaria de la mercancia y su denominación comercial, expresada en términos suficientemente como para permitir su identificacion y clasificacion a nivel de subpartida, asi como las caracteristicas, tipo, clase, modelo, dimensiones, capacidad, etc. </div></div>
<div class="lineal"><b class="text-9 sangria-1">(6)</b><div class="text-9 sangria-sub"><b>UNIDAD FÍSICA SEGUN FACTURA:</b> Indicar la cantidad y la unidad de medida por cada número de orden.</div></div>
<div class="lineal"><b class="text-9 sangria-1">(7)</b><div class="text-9 sangria-sub"><b>VALOR FOB EN DÓLARES:</b> Indicar el Valor FOB de la mercancia en dólares de los Estados Unidos por cada número de orden. Este valor debe coincidir con el indicado en la factura comercial.</div></div>
<div class="lineal"><b class="text-9 sangria-1">(8)</b><div class="text-9 sangria-sub"><b>CANTIDAD SEGÚN FACTURA PARA CUPOS CAPITULO 72:</b> Para los cupos con criterio de origen general de las partidas 7210, 7214, 7215, 7216 y 7217 indicar únicamente el monto total de la exportacion en toneladas.</div></div>
<div class="lineal"><b class="text-9 sangria-1">(9)</b><div class="text-9 sangria-sub"><b>DECLARACION DE ORIGEN:</b> Deben llenarse los espacios correspondientes a factura comercial No. y Fecha.</div></div>
<div class="text-9 sangria-3">
    <ul class="sangria-lista">
        <li><span><u>Factura(s) Comercial(es) No.:</u> indicar el(los) números de factura(s) comercial(es) que ampara la exportacion</span></li>
        <li><span><u>Fecha:</u> Indicar la efecha de emisi{on de la factura comercial.</span></li>
        <li>Cuando la mercancia originaria sea facturada por un operador de un país distinto al de origen de la mercancial, sea o no Parde del Cacuerdo, en el campo relativo "Observaciones" del certificado de origen se deberá señalar que la mercancia será facturada por ese operador, indicando el nombre, denominacion o razon social y domicilio de quien en definitiva facture a destino, asi como el numero y la fecha de factura comercial correspondiente.</li>
        <li>En caso de existir mas de una factura comercial debera hacerce la aclaracion en la casilla de "Observaciones", indicacndo numeros y fechas corrrespondientes.</li>
    </ul>
</div>
<div class="lineal"><b class="text-9 sangria-1">(10)</b><div class="text-9 sangria-sub"><b>NÚMERO DE ORDEN:</b> Este número de orden deberá corresponder con el(los) número(s) de orden especificado(s) en la casilla (3).</div></div>
<div class="lineal"><b class="text-9 sangria-1">(11)</b><div class="text-9 sangria-sub"><b>CRITERIO PARA LA CALIFICACIÓN DE ORIGEN:</b> En esta casilla se debe identificar el criterio de origen que cumple la mercancia a exportar individualizada por su número de orden Los criterios de origen establecidos en este Acuerdo, deberán citarse de la forma en que aparecen en la columna derecha del siguiente cuadro explicativo.</div></div>
<div class="text-8 ancho-full espacio-superior centrar">
    <table class=" table borde table-center ancho-90">  
            <tr><td class="ancho-medio borde titulo centrar">CRITERIO DE ORIGEN</td><td class="ancho-medio borde titulo centrar">IDENTIFICACIÓN DEL CRITERIO EN EL CERTIFICADO</td></tr>
            <tr><td class="borde top justify">Las mercancias obtenidas en su totalidad o producidas entramente en territorio de una o ambas Partes.</td><td class="borde centrar">Anexo II, Arículo 3 parrado I. Literal a)</td></tr>
            <tr><td class="borde top justify">Las mercancias que sean producidas enteramente en territorio de una Parte, a partir exclusivamente de materiales que califican como originarios de conformidad con el Régimen de Origen del Anexo II del Acuerdo.</td><td class="borde centrar">Anexo II, Articulo 3 parrado I. Literal b)</td></tr>
            <tr><td class="borde top justify">Las mercancias que en su elaboración utilicen matriales no originarios, seran considerados originarias cuando cumplan con los requisitos especificos de origen previstos en el Apéndice 1 del Anexo II del Acuerdo. Los requisitos especificos de origen prevalecerán sobre los criterios establecidos en el Anexo II. Artículo 3 párrafo 1. Literal d).</td><td class="borde centrar">Anexo II, Articulo 3 parrado I. Literal c)</td></tr>
            <tr><td class="borde top justify">Las mercancias que incorporen en su producción materiales no originarios, que resulten de un proceso de ensamblaje o montaje, realizado en el territorio de cualquiera de las Partes, siempre que en su elaboración se utilicen materiales originarios y el valor CIF de los materiales no originarios no exceda el 50 porciento valor FOB de exportacion de la mercancia.</td><td class="borde centrar">Anexo II, Articulo 3 parrado I. Literal d), inciso i.</td></tr>
            <tr><td class="borde top justify">Las mercancias que incorporen en si produccion materiales no originarios que resulten de un proceso de transformacion, distinto al ensamblaje o montaje, realizado en el territorio de cualquiera de las partes, que les confiera una nueva individualidad. Esa nueva individualidad implica que, en el Sistema Armonizado, las mercancias se clasifiquen en una partida diferente a aquellas en las que se clasifiquen cada uno de los materiales no originarios.</td><td class="borde centrar">Anexo II, Articulo 3 parrado I. Literal d), inciso ii.</td></tr>
            <tr><td class="borde top justify">Las mercancias que incorporen en su produccion materiales no originarios y que no complan con lo establecido en el literal d) inciso ii, Articulo 3 del Anexo II porque el proceso de transformacion, distinto al ensamblaje o montaje, realizado en el territorio de cualquiera de las Partes no implica un cambio de partida arancelaria, deberan cumplir con que el valor CIF de los materiales no originarios no exceda el 50% del valor FOB de exportacion de la mercancia y que en su elaboracion se utilicen materiales originarios de Las Partes.</td><td class="borde centrar">Anexo II, Articulo 3 parrado I. Literal d), inciso iii.</td></tr>
            <tr><td class="borde top justify">Los juegos surtidos, clasificados segun las Reglas Generales para la interpretacion de la Nomenclatura 1, 3 o 6 del Sistema Armonizado, seran considerados originarios cuando todas las mercancias que los componen sean originarias. Sin embargo, cuando un juego o surtido esté compuesto por mercancias originarias y mercancias no originarias, ese juego o surtido sera considerado originario en su conjunto, si el valor CIF de las mercancias no originarios no excede el 15% del precio FOB del juego o surtido.</td><td class="borde centrar">Anexo II, Articulo 7</td></tr>
    </table>
</div>
<div class="lineal espacio-superior"><b class="text-9 sangria-1">(12)</b><div class="text-9 sangria-sub"><b>PRODUCTOR O EXPORTADOR:</b> </div></div>
<div class="lineal"><span class="text-9 sangria-2">12.1</span><div class="text-9 sangria-sub"><u>Nombre o Razón Social:</u> Indicar los datos de la persona natural o juridica qie realiza la exportación.</div></div>
<div class="lineal"><span class="text-9 sangria-2">12.2</span><div class="text-9 sangria-sub"><u>Número de Identificacion Fiscal:</u> Indicar el número de identificacion fiscal o tributaria de la persona natural o juridica que realiza la exportacion.</div></div>
<div class="lineal"><span class="text-9 sangria-2">12.3</span><div class="text-9 sangria-sub"><u>Direccion:</u> Indicar el domicilio legal o registrado para los efectos fiscales de la persona natural o juridica que realiza la exportacion.</div></div>
<div class="lineal"><span class="text-9 sangria-2">12.4</span><div class="text-9 sangria-sub"><u>País y Ciudad:</u> Indicar el nombre del país y la ciudad donde se encuentra el domicilio legal o registrado para efectos fiscales de la persona natural o juridica que realiza la exportacion.</div></div>
<div class="lineal"><span class="text-9 sangria-2">12.5</span><div class="text-9 sangria-sub"><u>E-mail:</u> Indicar el correo electronico de la persona natural o juridica que realiza la exportacion.</div></div>
<div class="lineal"><span class="text-9 sangria-2">12.6</span><div class="text-9 sangria-sub"><u>Teléfono:</u> Indicar el numero telefonico de la persona natural o juridica que realiza la exportacion.</div></div>
<div class="lineal"><span class="text-9 sangria-2">12.7</span><div class="text-9 sangria-sub"><u>Fecha de emisión:</u> Indicar la fecha en la cual el certificado de origen fue llenado y firmado por la persona natural o juridica que realiza la exportacion.</div></div>

<div class="lineal"><b class="text-9 sangria-1">(13)</b><div class="text-9 sangria-sub"><b>SELLO Y FIRMA DEL PRODUCTOR O EXPORTADOR:</b> Este campo debe ser completado con la firma de la persona natural o juridica que realiza la exportación.</div></div>
<div class="lineal"><b class="text-9 sangria-1">(14)</b><div class="text-9 sangria-sub"><b>IMPORTADOR</b> </div></div>
<div class="lineal"><span class="text-9 sangria-2">14.1</span><div class="text-9 sangria-sub"><u>Nombre o Razón Social:</u> Indicar los datos de la persona natural o juridica qie realiza la importación.</div></div>
<div class="lineal"><span class="text-9 sangria-2">14.2</span><div class="text-9 sangria-sub"><u>Direccion:</u> Indicar el domicilio legal o registrado para los efectos fiscales de la persona natural o juridica que realiza la importación.</div></div>
<div class="lineal"><span class="text-9 sangria-2">14.3</span><div class="text-9 sangria-sub"><u>País y Ciudad:</u> Indicar el nombre del país y la ciudad donde se encuentra el domicilio legal o registrado para efectos fiscales de la persona natural o juridica que realiza la importación.</div></div>
<div class="lineal"><span class="text-9 sangria-2">14.4</span><div class="text-9 sangria-sub"><u>E-mail:</u> Indicar el correo electronico de la persona natural o juridica que realiza la importación.</div></div>
<div class="lineal"><span class="text-9 sangria-2">14.5</span><div class="text-9 sangria-sub"><u>Teléfono:</u> Indicar el numero telefonico de la persona natural o juridica que realiza la importación.</div></div>
<div class="lineal"><b class="text-9 sangria-1">(15)</b><div class="text-9 sangria-sub"><b>MEDIO DE TRANSPORTE:</b> Indique el tipo de transporte previsto para el desplazamiento de la mercancia.</div></div>
<div class="lineal"><b class="text-9 sangria-1">(16)</b><div class="text-9 sangria-sub"><b>PUERTO O LUGAR DE EMBARQUE:</b> Indique el nombre del lugar de embarque de las mercancias.</div></div>
<div class="lineal"><b class="text-9 sangria-1">(17)</b><div class="text-9 sangria-sub"><b>OBSERVACIONES:</b> En este espacio se puede incluir cualquier observacion y/o aclaracion que considere necesaria, además de las previstas especificamente en este instructivo y/o el Acuerdo, tales como:</div></div>
<div class="text-9 sangria-3">
    <ul class="sangria-lista">
        <li>Fecha de recepcion de la declaracion jurada a que hace refeencia el Articulo 16 Anexo II.</li>
        <li>La continuacion de la numeracion de las mercancias en otro ejemplar del certificado en caso que el espacio sea insuficiente.</span></li>
        <li>Facturacion de la mercancia por un operador de un pais distinto al de origen de la mercancia sea o no Parte del Acuerdo, didicando el nombre , denominacion o razon social y domicilio de quien en definitiva facture la operacion a destino asi como el numero y la fecha de la factura comercial correspondiente.</li>
        <li>Para el caso de los cupos para los productos del Capitulo 72 indique "los bienes clasificados en la partida cumplen con lo establecido en el Apéndice I Anexo II".</li>
    </ul>
</div>
<div class="lineal"><b class="text-9 sangria-1">(18)</b><div class="text-9 sangria-sub"><b>CERTIFICACION DE ORIGEN:</b> Este campo solo debe ser llenado por la Autoridad Competente.</div></div>
<div class="lineal"><b class="text-9 sangria-1">(19)</b><div class="text-9 sangria-sub"><b>NOMBRE Y FIRMA DE FUNCIONARIO HABILITADO Y SELLO DE LA AUTORIDAD COMPETENTE:</b> Este campo debe ser llenado con el nombre y la firma autógrafa del funcionario habilitado por las Partes para tal efecto, así como el sello de la Autoridad Competente.</div></div>
<div class="titulo text-9 espacio-superior">Nota:</div>
<div class="text-9">El llenado de todas las casillas del certificado de origen será obligatorio, con excepción de las casillas 14 y 15 que serán llenadas si la informacion es conocida, y la casilla 17 que sera llenada cuando corresponda.</div>
</body>
</html>