@extends('templates/layoutlte')
@section('content')

   <div class="row">
  <div class="col-md-12">
    <div class="row">
      <div class="col-md-10"></div>
      <div class="col-md-2">
        <a class="btn btn-primary" href="{{url('/exportador/resguardoAduanero/create')}}">
          <span class="glyphicon glyphicon-file" title="Agregar" aria-hidden="true"></span>
           Registrar solicitud
        </a>
      </div>
    </div>
    <table id="listaResguardo" class="table table-striped table-bordered" style="width:100%">
            <thead>
                <tr>
                    <th>Nro. solicitud</th>
                    <th>Fecha de Emisión</th>
                    <th>Estatus</th>
                    <th>Razon social</th>
                    <th>Rif</th>
                    <th>Aprobada por Resguardo</th>
                    <th>Aprobada por CNA</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody>
              @foreach($resguardoAduanero as  $resguardo)
                <tr>                   
                    <td>{{$resguardo->num_sol_resguardo}}</td>
                    <td>{{$resguardo->created_at->format('d-m-Y')}}</td>
                    <td>
                      @if($resguardo->status_resguardo==11 ||$resguardo->status_cna==11)
                        Con Observación!
                      @elseif($resguardo->status_resguardo==15 ||$resguardo->status_cna==15)
                        Doc Imcompletos
                      @elseif($resguardo->status_resguardo==22 ||$resguardo->status_cna==23)
                        Aprobada
                      @elseif($resguardo->status_resguardo==24 ||$resguardo->status_cna==25)
                        Negada
                      @endif
                      </td>
                    <td>{{$resguardo->rUsuario->detUsuario->razon_social}}</td>
                    <td>{{$resguardo->rUsuario->detUsuario->rif}}</td>
                    <td>
                      @if($resguardo->aprobada_resguardo==1)
                        <p style="color:green;">Aprobada</p>
                      @else
                        En Proceso
                      @endif
                    </td>
                    <td>
                      @if($resguardo->aprobada_cna==1)
                      <p style="color:green;">Aprobada</p>
                      @else
                        En Proceso
                      @endif
                    </td>
                    <td>
                      @if($resguardo->status_resguardo==15 || $resguardo->status_cna==15)
                      <a href="{{route('resguardoAduanero.edit',$resguardo->id)}}" class="btn btn-warning btn-sm" title="Doc Incompletos" aria-hidden="true">Doc Incompletos</a>
                      @endif
                      @if($resguardo->status_resguardo==11 || $resguardo->status_cna==11)
                      <a href="{{route('resguardoAduanero.edit',$resguardo->id)}}" class="glyphicon glyphicon-edit btn btn-warning btn-sm" title="Corregir" aria-hidden="true">Corregir</a>
                      @endif
                      @if($resguardo->status_resguardo==22 && $resguardo->aprobada_resguardo==1)
                      <a href="{{ route('CertificadoReguardoAduanero.pdf',['cerId'=>$resguardo->id])}}" class="glyphicon glyphicon-file btn btn-success btn-sm" title="Certificado Resguardo" aria-hidden="true"></a>
                      @endif
                      @if($resguardo->status_cna==23 && $resguardo->aprobada_cna==1)
                        <a href="{{action('PdfController@CertificadoVuceCna',array('id'=>$resguardo->id))}}" class="glyphicon glyphicon-file btn btn-default btn-sm" target="_blank"  aria-hidden="true" title="Certificado CNA"></a>
                      @endif
                      @if($resguardo->status_resguardo==24)
                        <p style="color:red;">Negada</p>
                      @endif
                      @if($resguardo->status_cna==25)
                        <p style="color:red;">Negada</p>
                      @endif
                       
                    </td>
                </tr>
              @endforeach
            </tbody>
        </table>
  </div>
</div>
@stop
