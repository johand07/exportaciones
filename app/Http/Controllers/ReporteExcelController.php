<?php

namespace App\Http\Controllers;

use App\Models\GenCertificado;
use App\Models\GenStatus;
use App\Models\DetUsuario;
use App\Models\DetProductosCertificado;
use App\Models\DetDeclaracionCertificado;
use App\Models\ImportadorCertificado;
use App\Models\DocumentosCertificado;
use App\Models\FirmantesCertificado;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use Illuminate\Http\Response;
use Maatwebsite\Excel\Facades\Excel;

use Illuminate\Support\Facades\DB;
use Session;
use Auth;
use Alert;

class ReporteExcelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\GenCertificado  $genCertificado
     * @return \Illuminate\Http\Response
     */
    public function show(GenCertificado $genCertificado)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\GenCertificado  $genCertificado
     * @return \Illuminate\Http\Response
     */
    public function edit(GenCertificado $genCertificado)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\GenCertificado  $genCertificado
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, GenCertificado $genCertificado)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\GenCertificado  $genCertificado
     * @return \Illuminate\Http\Response
     */
    public function destroy(GenCertificado $genCertificado)
    {
        //
    }



     public function excelColombia(Request $request, GenCertificado $genCertificado)
    {
        //dd($request->all());
        
            $gen_certificado_id = decrypt($request->cerId)? decrypt($request->cerId) : 0;
        //dd($gen_certificado_id);
        
        // consulta de Certificado de origen y todos sus datos asociados

        $certificado= GenCertificado::where('id',$gen_certificado_id)->first(); 
        //dd($certificado);
        //
//consulta para seleccionar el tipo del certificado
           $tipocertificado=$certificado->gen_tipo_certificado_id;
          //dd($tipocertificado);


         $det_usuario=DetUsuario::where('gen_usuario_id',$certificado->gen_usuario_id)->first();
         //dd($det_usuario);


           //dd($det_usuario)
          if($tipocertificado == 9) {
             $detalleProduct=DB::table('det_productos_certificado as det')
                ->select('det.num_orden',//
                        'det.cantidad_segun_factura',//
                        'det.descripcion_comercial',//
                        'det.unidad_fisica',//
                        'cri.normas_criterio',//
                        'cri.numero_factura',//
                        'cri.f_fac_certificado'
                        
                        )
                ->join('det_declaracion_certificado as cri', 'det.gen_certificado_id', '=', 'cri.gen_certificado_id')//
                
                ->where('det.bactivo',1)
                ->where('det.gen_certificado_id',$gen_certificado_id)
                ->get();
                //dd($detalleProduct);
          }

          $detalleProduct=DetProductosCertificado::where('gen_certificado_id',$gen_certificado_id)->get();
          //dd($detalleProduct);


          /************Consulta del detalle de Declaracion certificado para traerme todas***/

          $detdeclaracionCert=DetDeclaracionCertificado::where('gen_certificado_id',$gen_certificado_id)->get();
          //dd($detdeclaracionCert);

          $datosfirmante=FirmantesCertificado::where('bactivo',1)->where('tipo_certificado','Inversiones','ente_firmante')->first();


        /************Consulta para traerme los datos del importador certificado para traerme todas***/

        $importadorCertificado=ImportadorCertificado::where('gen_certificado_id',$gen_certificado_id)->first();
        switch ($tipocertificado) {
          case 1:
            
            $myFile   = Excel::create('COLOMBIA - VENEZUELA',function($excel)use($certificado, $detalleProduct,$detdeclaracionCert, $importadorCertificado, $det_usuario, $gen_certificado_id){ // Nombre princial del archivo
                    $excel->sheet('Colombia',function($sheet)use ($certificado, $detalleProduct,$detdeclaracionCert, $importadorCertificado, $det_usuario, $gen_certificado_id){ // se crea la hoja con el nombre de datos
                       // dd($empresas);
                        //header
                        $sheet->setStyle(array(
                        'font' => array(
                            'name' =>  'itálico',
                            'size' =>  8,
                            'bold' =>  true
                        )
                        ));
                        $sheet->loadView('ReportesPdf.CertificadoOrigenPExcel',compact('certificado','detalleProduct','detdeclaracionCert','importadorCertificado','det_usuario','gen_certificado_id'));
                        /* $sheet->mergeCells('A1:I1'); // union de celdas desde A a F
                        $sheet->row(1,['CERTIFICADO DE ORIGEN ACUERDO DE ALCANCE PARCIAL COLOMBIA - VENEZUELA']);
                        $sheet->row(2,[]);
                        $sheet->row(3,['(3) No. de Orden','(4) Código Arancelario en la nomenclatura de la parte exportadora','(5) Denominación de las Mercancías descripción arancelaria y comercial','(6) Unidad física y Cantidad según Factura','(7) Valor FOB de cada Mercancía según factura en US$','(8) Cantidad según factura para cupos Capítulo 72']);

                        //$sheet->fromArray($empresas, null, 'A4', null, false);
                        // consulta

                        
                       //dd($empresas);
                        //recorrido
                        foreach ($detalleProduct as $prod) {


                           $row=[]; // un arreglo vacio que se va a llenar con las posicion 
                           $row[0]=$prod->num_orden;
                           $row[1]=$prod->codigo_arancel;
                           $row[2]=$prod->denominacion_mercancia;
                           $row[3]=$prod->unidad_fisica;
                           $row[4]=$prod->valor_fob;
                           $row[5]=$prod->cantidad_segun_factura;
                           

                           $sheet->appendRow($row); // agrega una fila vacia al final del reporte

                        }*/


                    });
               // dd($empresas);
           
                })->download('xlsx');
                /*$myFile   = $myFile->string('xls'); 
                $response = array(
                    'name' => $name, 
                    'file' => "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64," . base64_encode($myFile), 
                );

                return response()->json($response);*/
          break;
          case 2:
          $name="CERTIFICADO DE ORIGEN ACUERDO DE COMERCIO DE LOS PUEBLOS PARA LA COMPLEMENTARIEDAD ECONÓMICA, PRODUCTIVA Y EL INTERCAMBIO COMERCIAL SOLIDARIO, ENTRE EL GOBIERNO DE LA REPUBLICA BOLIVARIANA DE VENEZUELA Y EL ESTADO PLURINACIONAL DE BOLIVIA";
            $myFile   = Excel::create(' ESTADO PLURINACIONAL DE BOLIVIA',function($excel)use($certificado,$detalleProduct, $detdeclaracionCert,$importadorCertificado,$det_usuario, $gen_certificado_id){ // Nombre princial del archivo
                    $excel->sheet('Bolivia',function($sheet)use ($certificado, $detalleProduct, $detdeclaracionCert,$importadorCertificado,$det_usuario, $gen_certificado_id){ // se crea la hoja con el nombre de datos
                       // dd($empresas);
                        //header
                        $sheet->setStyle(array(
                        'font' => array(
                            'name' =>  'itálico',
                            'size' =>  8,
                            'bold' =>  true
                        )
                        ));
                        $sheet->loadView('ReportesPdf.CertificadoOrigenBoliviaExcel',compact('certificado','detalleProduct','detdeclaracionCert','importadorCertificado','det_usuario','gen_certificado_id'));

                        /*$sheet->mergeCells('A1:I1'); // union de celdas desde A a F
                        $sheet->row(1,['CERTIFICADO DE ORIGEN ACUERDO DE COMERCIO DE LOS PUEBLOS PARA LA COMPLEMENTARIEDAD ECONÓMICA, PRODUCTIVA Y EL INTERCAMBIO COMERCIAL SOLIDARIO, ENTRE EL GOBIERNO DE LA REPUBLICA BOLIVARIANA DE VENEZUELA Y EL ESTADO PLURINACIONAL DE BOLIVIA']);
                        $sheet->row(2,[]);
                        $sheet->row(3,['(3) No. de Orden','(4) Código Arancelario en la nomenclatura de la parte exportadora','(5) Denominación de las Mercancías descripción arancelaria y comercial','(6) Peso o Cantidad','(7) Valor FOB en (U$S)']);

                        //$sheet->fromArray($empresas, null, 'A4', null, false);
                        // consulta

                        
                       //dd($empresas);
                        //recorrido
                        foreach ($detalleProduct as $prod) {


                           $row=[]; // un arreglo vacio que se va a llenar con las posicion 
                           $row[0]=$prod->num_orden;
                           $row[1]=$prod->codigo_arancel;
                           $row[2]=$prod->denominacion_mercancia;
                           $row[3]=$prod->unidad_fisica;
                           $row[4]=$prod->valor_fob;
                           // $row[5]=$prod->cantidad_segun_factura;
                           

                           $sheet->appendRow($row); // agrega una fila vacia al final del reporte

                        }*/


                    });
               // dd($empresas);
                 })->download('xlsx');
                /*$myFile   = $myFile->string('xls'); 
                $response = array(
                    'name' => $name, 
                    'file' => "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64," . base64_encode($myFile), 
                );

                return response()->json($response);*/
          break;
          case 3:
             $name="COMPLEMENTACION ECONOMICA No. 23 Chile";
            $myFile   = Excel::create('CHILE-VENEZUELA',function($excel)use($certificado, $detalleProduct,$detdeclaracionCert, $importadorCertificado, $det_usuario, $gen_certificado_id){ // Nombre princial del archivo
                    $excel->sheet('Chile',function($sheet)use ($certificado, $detalleProduct,$detdeclaracionCert, $importadorCertificado, $det_usuario, $gen_certificado_id){ // se crea la hoja con el nombre de datos
                       // dd($empresas);
                        //header
                        $sheet->setStyle(array(
                        'font' => array(
                            'name' =>  'itálico',
                            'size' =>  8,
                            'bold' =>  true
                        )
                        ));
                        $sheet->loadView('ReportesPdf.CertificadoOrigenChileExcel',compact('certificado','detalleProduct','detdeclaracionCert','importadorCertificado','det_usuario','gen_certificado_id'));

                        /*$sheet->mergeCells('A1:I1'); // union de celdas desde A a F
                        $sheet->row(1,['CERTIFICADO DE ORIGEN<br>
                        ASOCIACION LATINOAMERICANA DE INTEGRACION<br>
                        ACUERDO DE COMPLEMENTACION ECONOMICA No. 23']);
                        $sheet->row(2,[]);
                        $sheet->row(3,['(3) No. de Orden','(4) Código Arancelario en la nomenclatura de la parte exportadora','(5) Denominación de las Mercancías descripción arancelaria y comercial','(6) Peso o Cantidad','(7) Valor FOB en (U$S)']);

                        //$sheet->fromArray($empresas, null, 'A4', null, false);
                        // consulta

                        
                       //dd($empresas);
                        //recorrido
                        foreach ($detalleProduct as $prod) {


                           $row=[]; // un arreglo vacio que se va a llenar con las posicion 
                           $row[0]=$prod->num_orden;
                           $row[1]=$prod->codigo_arancel;
                           $row[2]=$prod->denominacion_mercancia;
                           $row[3]=$prod->unidad_fisica;
                           $row[4]=$prod->valor_fob;
                           // $row[5]=$prod->cantidad_segun_factura;
                           

                           $sheet->appendRow($row); // agrega una fila vacia al final del reporte

                        }*/


                    });
               // dd($empresas);
           
                })->download('xlsx');

                /*$myFile   = $myFile->string('xls'); 
                $response = array(
                    'name' => $name, 
                    'file' => "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64," . base64_encode($myFile), 
                );

                return response()->json($response);*/
          break;

          case 4:
           $name="CERTIFICADO ALADI";
            $myFile   = Excel::create('ALADI-VENEZUELA',function($excel)use($certificado, $detalleProduct,$detdeclaracionCert, $det_usuario, $gen_certificado_id){ // Nombre princial del archivo
                    $excel->sheet('Aladi',function($sheet)use ($certificado, $detalleProduct,$detdeclaracionCert,$det_usuario, $gen_certificado_id){ // se crea la hoja con el nombre de datos
                       // dd($empresas);
                        //header
                        $sheet->setStyle(array(
                        'font' => array(
                            'name' =>  'itálico',
                            'size' =>  8,
                            'bold' =>  true
                        )
                        ));
                        $sheet->loadView('ReportesPdf.CertificadoOrigenAladiExcel',compact('certificado','detalleProduct','detdeclaracionCert','det_usuario','gen_certificado_id'));

                        /*$sheet->mergeCells('A1:I1'); // union de celdas desde A a F
                        $sheet->row(1,['CERTIFICADO DE ORIGEN<br>
                        ASOCIACION LATINOAMERICANA DE INTEGRACION<br>
                        ACUERDO DE COMPLEMENTACION ECONOMICA No. 23']);
                        $sheet->row(2,[]);
                        $sheet->row(3,['(3) No. de Orden','(4) Código Arancelario en la nomenclatura de la parte exportadora','(5) Denominación de las Mercancías descripción arancelaria y comercial','(6) Peso o Cantidad','(7) Valor FOB en (U$S)']);

                        //$sheet->fromArray($empresas, null, 'A4', null, false);
                        // consulta

                        
                       //dd($empresas);
                        //recorrido
                        foreach ($detalleProduct as $prod) {


                           $row=[]; // un arreglo vacio que se va a llenar con las posicion 
                           $row[0]=$prod->num_orden;
                           $row[1]=$prod->codigo_arancel;
                           $row[2]=$prod->denominacion_mercancia;
                           $row[3]=$prod->unidad_fisica;
                           $row[4]=$prod->valor_fob;
                           // $row[5]=$prod->cantidad_segun_factura;
                           

                           $sheet->appendRow($row); // agrega una fila vacia al final del reporte

                        }*/


                    });
               // dd($empresas);
           
                })->download('xlsx');
            
                /*$myFile   = $myFile->string('xls'); 
                $response = array(
                    'name' => $name, 
                    'file' => "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64," . base64_encode($myFile), 
                );

                return response()->json($response);*/
          break;

          case 5:

          $name="CERTIFICADO DE ORIGEN <br>ACUERDO MERCOSUR-COLOMBIA, ECUADOR Y VENEZUELA";
            $myFile   = Excel::create('MERCOSUR-VENEZUELA',function($excel)use($certificado, $detalleProduct,$detdeclaracionCert, $importadorCertificado, $det_usuario, $gen_certificado_id){ // Nombre princial del archivo
                    $excel->sheet('Mercosur',function($sheet)use ($certificado, $detalleProduct,$detdeclaracionCert, $importadorCertificado, $det_usuario, $gen_certificado_id){ // se crea la hoja con el nombre de datos
                       // dd($empresas);
                        //header
                        $sheet->setStyle(array(
                        'font' => array(
                            'name' =>  'itálico',
                            'size' =>  8,
                            'bold' =>  true
                        )
                        ));
                        $sheet->loadView('ReportesPdf.CertificadoOrigMercosurExcel',compact('certificado','detalleProduct','detdeclaracionCert','det_usuario','gen_certificado_id','importadorCertificado'));

                        /*$sheet->mergeCells('A1:I1'); // union de celdas desde A a F
                        $sheet->row(1,['CERTIFICADO DE ORIGEN<br>
                        ASOCIACION LATINOAMERICANA DE INTEGRACION<br>
                        ACUERDO DE COMPLEMENTACION ECONOMICA No. 23']);
                        $sheet->row(2,[]);
                        $sheet->row(3,['(3) No. de Orden','(4) Código Arancelario en la nomenclatura de la parte exportadora','(5) Denominación de las Mercancías descripción arancelaria y comercial','(6) Peso o Cantidad','(7) Valor FOB en (U$S)']);

                        //$sheet->fromArray($empresas, null, 'A4', null, false);
                        // consulta

                        
                       //dd($empresas);
                        //recorrido
                        foreach ($detalleProduct as $prod) {


                           $row=[]; // un arreglo vacio que se va a llenar con las posicion 
                           $row[0]=$prod->num_orden;
                           $row[1]=$prod->codigo_arancel;
                           $row[2]=$prod->denominacion_mercancia;
                           $row[3]=$prod->unidad_fisica;
                           $row[4]=$prod->valor_fob;
                           // $row[5]=$prod->cantidad_segun_factura;
                           

                           $sheet->appendRow($row); // agrega una fila vacia al final del reporte

                        }*/


                    });
               // dd($empresas);
           
                })->download('xlsx');
            
                /*$myFile   = $myFile->string('xls'); 
                $response = array(
                    'name' => $name, 
                    'file' => "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64," . base64_encode($myFile), 
                );

                return response()->json($response);*/
          break;

           case 6:

           $name="CERTIFICADO DE ORIGEN<br><br>ACUERDO DE ALCANCE PARCIAL DE NATURALEZA COMERCIAL ENTRE LA REPÚBLICA BOLIVARIANA DE VENEZUELA Y LA REPÚBLICA DEL PERÚ";
            $myFile   = Excel::create('PERÚ-VENEZUELA',function($excel)use($certificado, $detalleProduct,$detdeclaracionCert, $importadorCertificado, $det_usuario, $gen_certificado_id){ // Nombre princial del archivo
                    $excel->sheet('Peru',function($sheet)use ($certificado, $detalleProduct,$detdeclaracionCert, $importadorCertificado, $det_usuario, $gen_certificado_id){ // se crea la hoja con el nombre de datos
                       // dd($empresas);
                        //header
                        $sheet->setStyle(array(
                        'font' => array(
                            'name' =>  'itálico',
                            'size' =>  8,
                            'bold' =>  true
                        )
                        ));
                        $sheet->loadView('ReportesPdf.CertificadoOrigenPeruExcel',compact('certificado','detalleProduct','detdeclaracionCert','det_usuario','gen_certificado_id','importadorCertificado'));

                        /*$sheet->mergeCells('A1:I1'); // union de celdas desde A a F
                        $sheet->row(1,['CERTIFICADO DE ORIGEN<br>
                        ASOCIACION LATINOAMERICANA DE INTEGRACION<br>
                        ACUERDO DE COMPLEMENTACION ECONOMICA No. 23']);
                        $sheet->row(2,[]);
                        $sheet->row(3,['(3) No. de Orden','(4) Código Arancelario en la nomenclatura de la parte exportadora','(5) Denominación de las Mercancías descripción arancelaria y comercial','(6) Peso o Cantidad','(7) Valor FOB en (U$S)']);

                        //$sheet->fromArray($empresas, null, 'A4', null, false);
                        // consulta

                        
                       //dd($empresas);
                        //recorrido
                        foreach ($detalleProduct as $prod) {


                           $row=[]; // un arreglo vacio que se va a llenar con las posicion 
                           $row[0]=$prod->num_orden;
                           $row[1]=$prod->codigo_arancel;
                           $row[2]=$prod->denominacion_mercancia;
                           $row[3]=$prod->unidad_fisica;
                           $row[4]=$prod->valor_fob;
                           // $row[5]=$prod->cantidad_segun_factura;
                           

                           $sheet->appendRow($row); // agrega una fila vacia al final del reporte

                        }*/


                    });
               // dd($empresas);
           
                })->download('xlsx');
            
                /*$myFile   = $myFile->string('xls'); 
                $response = array(
                    'name' => $name, 
                    'file' => "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64," . base64_encode($myFile), 
                );

                return response()->json($response);*/
          break;


          case 7:
           $name="CERTIFICADO DE ORIGEN ACUERDO DE COMPLEMENTACIÓN ECONÓMICA ENTRE LA REPÚBLICA DE CUBA Y LA REPUBLICA BOLIVARIANA DE VENEZUELA";
            $myFile   = Excel::create('Cuba-VENEZUELA',function($excel)use($certificado, $detalleProduct,$detdeclaracionCert, $importadorCertificado, $det_usuario, $gen_certificado_id){ // Nombre princial del archivo
                    $excel->sheet('Cuba',function($sheet)use ($certificado, $detalleProduct,$detdeclaracionCert, $importadorCertificado, $det_usuario, $gen_certificado_id){ // se crea la hoja con el nombre de datos
                       // dd($empresas);
                        //header
                        $sheet->setStyle(array(
                        'font' => array(
                            'name' =>  'itálico',
                            'size' =>  8,
                            'bold' =>  true
                        )
                        ));
                        $sheet->loadView('ReportesPdf.CertificadoOrigenCubaExcel',compact('certificado','detalleProduct','detdeclaracionCert','det_usuario','gen_certificado_id','importadorCertificado'));

                        /*$sheet->mergeCells('A1:I1'); // union de celdas desde A a F
                        $sheet->row(1,['CERTIFICADO DE ORIGEN<br>
                        ASOCIACION LATINOAMERICANA DE INTEGRACION<br>
                        ACUERDO DE COMPLEMENTACION ECONOMICA No. 23']);
                        $sheet->row(2,[]);
                        $sheet->row(3,['(3) No. de Orden','(4) Código Arancelario en la nomenclatura de la parte exportadora','(5) Denominación de las Mercancías descripción arancelaria y comercial','(6) Peso o Cantidad','(7) Valor FOB en (U$S)']);

                        //$sheet->fromArray($empresas, null, 'A4', null, false);
                        // consulta

                        
                       //dd($empresas);
                        //recorrido
                        foreach ($detalleProduct as $prod) {


                           $row=[]; // un arreglo vacio que se va a llenar con las posicion 
                           $row[0]=$prod->num_orden;
                           $row[1]=$prod->codigo_arancel;
                           $row[2]=$prod->denominacion_mercancia;
                           $row[3]=$prod->unidad_fisica;
                           $row[4]=$prod->valor_fob;
                           // $row[5]=$prod->cantidad_segun_factura;
                           

                           $sheet->appendRow($row); // agrega una fila vacia al final del reporte

                        }*/


                    });
               // dd($empresas);
           
                })->download('xlsx');
            
                /*$myFile   = $myFile->string('xls'); 
                $response = array(
                    'name' => $name, 
                    'file' => "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64," . base64_encode($myFile), 
                );

                return response()->json($response);*/
          break;



          case 8:
           $name="CERTIFICADO DE ORIGEN TURQUIA";
            $myFile   = Excel::create('TURQUIA',function($excel)use($certificado, $detalleProduct,$detdeclaracionCert, $importadorCertificado, $det_usuario, $gen_certificado_id,$datosfirmante){ // Nombre princial del archivo
                    $excel->sheet('Turquia',function($sheet)use ($certificado, $detalleProduct,$detdeclaracionCert, $importadorCertificado, $det_usuario, $gen_certificado_id,$datosfirmante){ // se crea la hoja con el nombre de datos
                       // dd($empresas);
                        //header
                        $sheet->setStyle(array(
                        'font' => array(
                            'name' =>  'itálico',
                            'size' =>  8,
                            'bold' =>  true
                        )
                        ));
                        $sheet->loadView('ReportesPdf.CertificadoOrigenTurquiaExcel',compact('certificado','detalleProduct','detdeclaracionCert','det_usuario','gen_certificado_id','importadorCertificado','datosfirmante'));

                        /*$sheet->mergeCells('A1:I1'); // union de celdas desde A a F
                        $sheet->row(1,['CERTIFICADO DE ORIGEN<br>
                        ASOCIACION LATINOAMERICANA DE INTEGRACION<br>
                        ACUERDO DE COMPLEMENTACION ECONOMICA No. 23']);
                        $sheet->row(2,[]);
                        $sheet->row(3,['(3) No. de Orden','(4) Código Arancelario en la nomenclatura de la parte exportadora','(5) Denominación de las Mercancías descripción arancelaria y comercial','(6) Peso o Cantidad','(7) Valor FOB en (U$S)']);

                        //$sheet->fromArray($empresas, null, 'A4', null, false);
                        // consulta

                        
                       //dd($empresas);
                        //recorrido
                        foreach ($detalleProduct as $prod) {


                           $row=[]; // un arreglo vacio que se va a llenar con las posicion 
                           $row[0]=$prod->num_orden;
                           $row[1]=$prod->codigo_arancel;
                           $row[2]=$prod->denominacion_mercancia;
                           $row[3]=$prod->unidad_fisica;
                           $row[4]=$prod->valor_fob;
                           // $row[5]=$prod->cantidad_segun_factura;
                           

                           $sheet->appendRow($row); // agrega una fila vacia al final del reporte

                        }*/


                    });
               // dd($empresas);
           
                })->download('xlsx');
            
                /*$myFile   = $myFile->string('xls'); 
                $response = array(
                    'name' => $name, 
                    'file' => "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64," . base64_encode($myFile), 
                );

                return response()->json($response);*/
          break;

          case 9:
           $name="CERTIFICADO DE ORIGEN UNION EUROPEA";
            $myFile   = Excel::create('UNION EUROPEA',function($excel)use($certificado, $detalleProduct,$detdeclaracionCert, $importadorCertificado, $det_usuario, $gen_certificado_id){ // Nombre princial del archivo
                    $excel->sheet('Union Europea',function($sheet)use ($certificado, $detalleProduct,$detdeclaracionCert, $importadorCertificado, $det_usuario, $gen_certificado_id){ // se crea la hoja con el nombre de datos
                       // dd($empresas);
                        //header
                        $sheet->setStyle(array(
                        'font' => array(
                            'name' =>  'itálico',
                            'size' =>  8,
                            'bold' =>  true
                        )
                        ));
                        $sheet->loadView('ReportesPdf.CertificadoOrigenUExcel',compact('certificado','detalleProduct','detdeclaracionCert','det_usuario','gen_certificado_id','importadorCertificado'));

                        /*$sheet->mergeCells('A1:I1'); // union de celdas desde A a F
                        $sheet->row(1,['CERTIFICADO DE ORIGEN<br>
                        ASOCIACION LATINOAMERICANA DE INTEGRACION<br>
                        ACUERDO DE COMPLEMENTACION ECONOMICA No. 23']);
                        $sheet->row(2,[]);
                        $sheet->row(3,['(3) No. de Orden','(4) Código Arancelario en la nomenclatura de la parte exportadora','(5) Denominación de las Mercancías descripción arancelaria y comercial','(6) Peso o Cantidad','(7) Valor FOB en (U$S)']);

                        //$sheet->fromArray($empresas, null, 'A4', null, false);
                        // consulta

                        
                       //dd($empresas);
                        //recorrido
                        foreach ($detalleProduct as $prod) {


                           $row=[]; // un arreglo vacio que se va a llenar con las posicion 
                           $row[0]=$prod->num_orden;
                           $row[1]=$prod->codigo_arancel;
                           $row[2]=$prod->denominacion_mercancia;
                           $row[3]=$prod->unidad_fisica;
                           $row[4]=$prod->valor_fob;
                           // $row[5]=$prod->cantidad_segun_factura;
                           

                           $sheet->appendRow($row); // agrega una fila vacia al final del reporte

                        }*/


                    });
               // dd($empresas);
           
                })->download('xlsx');
            
                /*$myFile   = $myFile->string('xls'); 
                $response = array(
                    'name' => $name, 
                    'file' => "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64," . base64_encode($myFile), 
                );

                return response()->json($response);*/
          break;

        }

        
    }
}
