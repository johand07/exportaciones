   /********************Scrip para carga de archivos**************** */
   function cambiarImagen(event) {
    var id= event.target.getAttribute('name');
    var reader = new FileReader();
    reader.onload = function(){
      var output = document.getElementById('vista_previa_'+id);
      output.src = reader.result;
    };
    //let archivo = $(".file-input").val();
    //if (!archivo) {

      let archivo=$('#'+id).val();
      //console.log(reader);
    //}
    var extensiones = archivo.substring(archivo.lastIndexOf(".")).toLowerCase();
    
    //var imgfile="/exportaciones/public/img/file.png";

//alert(imgfile);
    if(extensiones != ".doc" && extensiones != ".docx" && extensiones != ".pdf" && extensiones != ".pptx" && extensiones != ".ppt"){
      swal("Formato invalido", "Formato de archivo invalido debe ser .doc, .docx, .ppt, .pptx o pdf", "warning"); 
        
        
    }else{
      //Pintame la vista previa el Documento azulito cuando se carga un archivo con imgdocumento_
      $('#imgdocumento_'+id).show();
      let file=$('#'+id).val();
     // alert(file);
     //Y pintame ruta y nombre del archivo acargado con nombre_

      $('#nombre_'+id).html('<h4>'+file+'</h4>');
      //La traia la rura y el nombre del Documento cargado pero era negrita y NO se difernciaba co el titulo
      //$('#nombre_'+id).html('<strong>'+file+'</strong>');
     
      reader.readAsDataURL(event.target.files[0]);

    }

  }

  ///////////////Para validar documentos//////////////////

  function cambiarImagenDoc(event) {

    
    //console.log(event);

    var id= event.target.getAttribute('id');
    //alert(id);
    
  
      archivo=$('#'+id).val();
    //}
    var extensiones = archivo.substring(archivo.lastIndexOf(".")).toLowerCase();
    
    //var imgfile="/exportaciones/public/img/file.png";

    
    if(extensiones != ".doc" && extensiones != ".docx" && extensiones != ".pdf" && extensiones != ".pptx" && extensiones != ".ppt")
    {
      swal("Formato invalido", "Formato de archivo invalido debe ser .doc, .docx, .ppt, .pptx o pdf", "warning"); 
        $('#'+id).val('');
        
    }else{
      $('#imgdocumento_'+id).show();
      let file=$('#'+id).val();
     // alert(file);
      $('#nombre_'+id).html('<strong>'+file+'</strong>');
     
      reader.readAsDataURL(event.target.files[0]);

    }

  }
/////////////////Validacion acondicionada para cuanto la carga de archivo NO puede sercargado un archivo que pee mas de 2.5 megabytes////////////////
    $(document).on('change', '.file-input', function(evt) {
      
      let size=this.files[0].size;
      if (size>8500000) {

         swal("Tamaño de archivo invalido", "El tamaño del archivo No puede ser mayor a 8.5 megabytes","warning"); 
          this.value='';
         //this.files[0].value='';
      }else{
        cambiarImagen(evt);
      }
      
           
    });
///////////LO mismo para carga de archivo lista dinamica//////////
  $(document).on('change', '.file_multiple', function(evt) {
           

          let size=this.files[0].size;
      if (size>8500000) {

         swal("Tamaño de archivo invalido", "El tamaño del archivo No puede ser mayor a 8.5 megabytes","warning"); 
         this.value='';
      }else{
        cambiarImagenDoc(evt);
      }

    });


/********************************************************************** */

$(document).ready(function (){
	 /* agregar para productos completos en el caso colombia*/

	var i = 0;
	$('#add-productos').click(function(){
	i++;

	 	$('#productos tr:last').after('<tr id="row1'+i+'"display="block" class="show_div"><td>{!!Form::text("num_orden[]",null,["class"=>"form-control","id"=>"num_orden", "noreq"=>"noreq"])!!}</td><td>{!!Form::text("codigo_arancel[]",null,["class"=>"form-control","id"=>"codigo_arancel", "noreq"=>"noreq"])!!}</td><td>{!!Form::text("denominacion_mercancia[]",null,["class"=>"form-control","id"=>"denominacion_mercancia", "noreq"=>"noreq"])!!}</td><td>{!!Form::text("unidad_fisica[]",null,["class"=>"form-control","id"=>"unidad_fisica", "noreq"=>"noreq"])!!}</td><td>{!!Form::text("valor_fob[]",null,["class"=>"form-control","id"=>"valor_fob","onkeypress"=>"return soloNumeros(event)", "noreq"=>"noreq"])!!}</td><td>{!!Form::text("cantidad_segun_factura[]",null,["class"=>"form-control","id"=>"cantidad_segun_factura","onkeypress"=>"return soloNumeros(event)", "noreq"=>"noreq"])!!}</td><td><button  name="remove" type="button" id="'+i+'" value="" class="btn btn-danger btn-remove">x</button></td></tr>');

	});

	$(document).on('click','.btn-remove',function(){
		var id_boton= $(this).attr("id");
		$("#row1"+id_boton+"").remove();

	});




/********************************************************************** */

	/*Aqui va lista dinamica de Criterios*/

	var i = 0;
	$('#add-criterios').click(function(){
	i++;

	 	$('#criterios tr:last').after('<tr id="row2'+i+'"display="block" class="show_div"><td>{!!Form::text("num_orden_declaracion[]",null,["class"=>"form-control","id"=>"num_orden_declaracion", "noreq"=>"noreq"])!!}</td><td>{!!Form::text("normas_criterio[]",null,["class"=>"form-control","id"=>"normas_criterio", "noreq"=>"noreq"])!!}</td><td><button name="remove"type="button" id="'+i+'" value=""class="btn btn-danger btn-remove2">x</button></td></tr>');

	});

	$(document).on('click','.btn-remove2',function(){
		var id_boton= $(this).attr("id");
		$("#row2"+id_boton+"").remove();

	});


		//Validación de formulario, se puede llamar y pasar la sección que contenga los inputs a validar
		
		var error = '<div class="msg_alert"><span style="color:red" class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span><strong id="cod_aran-error" class="added"> El campo es obligatorio.</strong></div>';
		var valido = '<div class="msg_alert"><span style="color:green" class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span></div>';
		var mens=['Estimado Usuario. Debe completar los campos solicitados',]

		validacionForm = function() {

		$('#FormProd').submit(function(event) {
		
		var campos = $('#FormProd').find('input:text, select, input:file, textarea');
		var n = campos.length;
		var err = 0;

		$("div").remove(".msg_alert");
		//bucle que recorre todos los elementos del formulario
		for (var i = 0; i < n; i++) {
				var cod_input = $('#FormProd').find('input:text, select, input:file, textarea').eq(i);
				if (!cod_input.attr('noreq')) {
					if (cod_input.val() == '' || cod_input.val() == null)
					{
						err = 1;
						cod_input.css('border', '1px solid red').after(error);
					}
					else{
						if (err == 1) {err = 1;}else{err = 0;}
						cod_input.css('border', '1px solid green').after(valido);
					}
					
				}
		}

		//Si hay errores se detendrá el submit del formulario y devolverá una alerta
		if(err==1){
				event.preventDefault();
				swal("Por Favor!", mens[0], "warning")
			}

			});
		}



 });/*llave del document).ready*/