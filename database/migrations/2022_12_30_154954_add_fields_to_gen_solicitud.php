<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToGenSolicitud extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('gen_solicitud', function (Blueprint $table) {
            $table->text('observacion_analista_intra')->nullable()->after('coperador_cambiario');
            $table->text('observacion_coordinador_intra')->nullable()->after('observacion_analista_intra');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('gen_solicitud', function (Blueprint $table) {
            //
            $table->dropColumn('observacion_analista_intra');
            $table->dropColumn('observacion_coordinador_intra');
        });
    }
}
