@extends('templates/layoutlte')
@section('content')


  <div class="content">
	{{Form::model($datosempresa,['route'=>['DatosJuridicos.update',$datosempresa->id],'method'=>'PATCH','id'=>'FormDatosJuridicos']) }}
	

<!--***************Parte 1 Registro****************************-->
  <!--Se le coloco una miga de pan a cada parte del registro para identificar el orden-->
	  <div >
		<div class="panel-heading">DATOS BÁSICOS</div>
		<div class="panel-body">

			<div class="row">
				<div class="col-md-4">
					<div class="form-group">
						{!! Form::label('','Nombre o Razón Social')!!}
						{!! Form::text('razon_social',null, ['class'=>'form-control','id'=>'razon_social','onkeypress'=>'return soloNumerosyLetras(event)','maxlength'=>'100']) !!}
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						{!! Form::label('','Siglas')!!}
						{!! Form::text('siglas',null, ['class'=>'form-control','id'=>'siglas','onkeypress'=>'return soloLetras(event)','maxlength'=>'50']) !!}
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						{!! Form::label('','Rif')!!}
						{!! Form::text('rif',null, ['class'=>'form-control','id'=>'rif','readonly'=>'true','maxlength'=>'11']) !!}
					</div>
				</div>
			</div>
			<div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  {!! Form::label('','Página Web')!!}
                  {!! Form::text('pagina_web',null, ['class'=>'form-control','id'=>'pagina_web','onkeypress'=>'return soloNumerosyLetras(event)','maxlength'=>'60','value'=>'']) !!}
                </div>
              </div>
				<div class="col-md-6">
					<div class="form-group">
						{!! Form::label('','Tipo de Empresa')!!}
						{!!Form::select('gen_tipo_empresa_id',$tipos_empresas,null,['placeholder'=>'Seleccione','class'=>'form-control','id'=>'gen_tipo_empresa_id']) !!}
					</div>
				</div>
			</div>
			<div class="panel panel-primary">
                <div class="panel-heading">TIPO DE ACTIVIDAD ECONÓMICA</div>
              </div>
              <div class="row">
                <div class="col-md-3">
                   {!! Form::checkbox('export','1',$datosempresa->export, ['class'=>'checkbox-inline','id'=>'export']) !!}
                   {!! Form::label('','Exportador')!!}
                </div>
                <div class="col-md-3">
                   {!! Form::checkbox('invers','1',$datosempresa->invers,['class'=>'checkbox-inline','id'=>'invers']) !!}
                   {!! Form::label('','Inversionista')!!}
                </div>
                <div class="col-md-3">
                   {!! Form::checkbox('produc','1',$datosempresa->produc, ['class'=>'checkbox-inline','id'=>'produc']) !!}
                   {!! Form::label('','Productor')!!}
                </div>
                <div class="col-md-3">
                   {!! Form::checkbox('comerc','1',$datosempresa->comerc, ['class'=>'checkbox-inline','id'=>'comerc']) !!}
                   {!! Form::label('','Comercializadora')!!}
               </div>
           </div><br>
           <br>
           {{--<div class="row">
           	<div class="col-md-3">
           		<div class="form-group">
           			{!! Form::label('','Actividad Económica Comercial')!!}
           			{!!Form::select('',$paises,null,['class'=>'form-control','placeholder'=>'---Seleccione una Opción---','id'=>'']) !!}
           		</div>
           	</div>
           	<div class="col-md-3">
           		<div class="form-group">
           			{!! Form::label('','Actividad Económica Secundaria')!!}
           			{!!Form::select('',$paises,null,['class'=>'form-control','placeholder'=>'---Seleccione una Opción---','id'=>'']) !!}
           		</div>
           	</div>
           	<div class="col-md-3">
           		<div class="form-group">
           			{!! Form::label('','Sector Comercial')!!}
           			{!!Form::select('',$paises,null,['class'=>'form-control','placeholder'=>'---Seleccione una Opción---','id'=>'']) !!}
           		</div>
           	</div>
           	<div class="col-md-3">
           		<div class="form-group">
           			{!! Form::label('','Sub Sector Comercial')!!}
           			{!! Form::text('',null, ['class'=>'form-control','id'=>'','onkeypress'=>'return soloLetras(event)','maxlength'=>'100']) !!}
           		</div>
           	</div>

           </div>--}}
         <div class="row">
         	<div class="col-md-6">  
					<div class="form-group">
						{!! Form::label('','Sector')!!}
						{!!Form::select('id_gen_sector',$sectores,null, ['class'=>'form-control','id'=>'id_gen_sector']) !!}
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						{!! Form::label('','Actividad Económica')!!}
						{!!Form::select('gen_actividad_eco_id',$actividadeco,null, ['placeholder'=>'Seleccione','class'=>'form-control','id'=>'id_gen_actividad_eco']) !!}
					</div>
				</div>
           </div>
           <br>
			<div class="panel panel-primary">
				<div class="panel-heading">DOMICILIO FISCAL</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<div class="form-group">
						{!! Form::label('','Estado')!!}
						{!!Form::select('estado_id',$estados,null,['placeholder'=>'Seleccione','class'=>'form-control','id'=>'id_estado']) !!}
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						{!! Form::label('','Municipio')!!}
						{!!Form::select('municipio_id',$municipios,null, ['placeholder'=>'Seleccione','class'=>'form-control','id'=>'id_municipio']) !!}
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						{!! Form::label('','Parroquia')!!}
						{!!Form::select('parroquia_id',$parroquias,null, ['placeholder'=>'Seleccione','class'=>'form-control','id'=>'id_parroquia']) !!}
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						{!! Form::label('','Dirección')!!}
						{!! Form::text('direccion',null, ['class'=>'form-control','id'=>'direccion','onkeypress'=>'return soloNumerosyLetras(event)','maxlength'=>'200']) !!}
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						{!! Form::label('','Teléfono Local')!!}
						{!! Form::text('telefono_local',null, ['class'=>'form-control','id'=>'telefono_local','onkeypress'=>'return soloNumeros(event)','maxlength'=>'11']) !!}
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						{!! Form::label('','Teléfono Celular')!!}
						{!! Form::text('telefono_movil',null, ['class'=>'form-control','id'=>'telefono_movil','onkeypress'=>'return soloNumeros(event)','maxlength'=>'11']) !!}
					</div>
				</div>
			</div>
			<div class="row">
              <div class="col-md-12">
                <div class="col-md-4"></div>
                <div class="col-md-4 text-center">
                  <div class="form-group">
                    <a class="btn btn-primary" href="{{route('DatosEmpresa.index')}}" id="atras1">Atrás</a>
                    <input type="submit" name="Guardar" value="Guardar" class="btn btn-success">
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
       </div> 
	{{Form::close()}}
</div>

@stop