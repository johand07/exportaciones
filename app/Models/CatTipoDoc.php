<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CatTipoDoc extends Model
{
	protected $table = 'cat_tipo_doc';

    protected $fillable = [

           'tipo_doc',
           'bactivo',
    ];

    protected $date =['created_at','updated_at'];
    

}
