<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\FacturaSolicitud;
use App\Models\GenFactura;
use App\Models\DetProdFactura;
use App\Models\DetProdTecnologia;
use App\Models\GenSolicitud;
use App\Models\GenDua;
use Auth;
use Session;
use Alert;

class DetProdSolicitudController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

     // dd(session()->all());
        $data=FacturaSolicitud::select('gen_factura_id')->where('gen_solicitud_id',Session::get('gen_solictud_id'))->get();

          $monto=0;$montoCif=0; $id_solicitud=[];


            foreach ($data as $value) {


                $id_solicitud[]=$value->gen_factura_id;

              }
              //hacerle un array unico  $id_solicitud[] luego pasarselo a todas las demas consultas

              $id_solicitud=array_unique($id_solicitud);

         Session::put('facturasId',$id_solicitud);
         $cons_factura=GenFactura::whereIn('id',$id_solicitud)->get();

          foreach ($cons_factura as $cif) {

              /*Se le agrego la resta del monto fob*/
              $montoCif=$montoCif+$cif->monto_total-$cif->monto_fob;


            }

           Session::put('monto_cif',$montoCif);

           $tipo_solicitud=GenFactura::find($data->first()->gen_factura_id);

            if($tipo_solicitud->tipo_solicitud_id==1) {


                  $productos=DetProdFactura::whereIn('gen_factura_id',$id_solicitud)->get();

                  foreach ($productos as $montoTotal) {

                      $monto=$monto+$montoTotal->monto_total;

                    }


                  Session::put('monto_total_facturas',$monto);


               }else{


                  $productos=DetProdTecnologia::whereIn('gen_factura_id',$id_solicitud)->get();



                  foreach ($productos as $montoTotal) {

                      $monto=$monto+$montoTotal->valor_fob;

                    }
                  Session::put('monto_total_facturas',$monto);



            }




      return view('detSolicitud.create',compact('productos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


     /*$x=GenFactura::find(Session::get('facturasId.0'));
     
        $dua=GenDua::find($x->gen_dua_id);
        $dua->bactivo=0;
        $dua->save();

        */


     $solicitud=GenSolicitud::find(Session::get('gen_solictud_id'));
     $solicitud->monto_solicitud=Session::get('monto_total_facturas');
     $solicitud->monto_cif=(Session::get('monto_cif'))+(Session::get('monto_total_facturas'));
     $solicitud->save();



        for ($i=0; $i < count(Session::get('facturasId')) ; $i++) {

          $solicitud=GenFactura::find(Session::get('facturasId.'.$i));
          $solicitud->bactivo=0;
          $solicitud->save();

        }

        // $verificar_bactivo=GenFactura::find(Session::get('facturasId.0'));

  

      Alert::success('Registro Exitoso!','Solicitud registrada con éxito, puede descargar la planilla ')->persistent("Ok");
 
      return redirect()->action('TipoSolicitudController@index');

      

   


    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
