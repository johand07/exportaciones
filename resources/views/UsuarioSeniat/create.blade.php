@extends('templates/layoutlte_seniat')

@section('content')

{{Form::open(['route'=>'UsuarioSeniat.store','method'=>'POST','id'=>'UsuarioSeniat','enctype' => 'multipart/form-data'])}}
<div class="row"> 
    <div class="col-md-12">
        <div id="datos_persona1">
            <ol class="breadcrumb">
            <li class="active">Paso 1</li>
            </ol>
            <div class="panel panel-primary">
            <div class="panel-heading">CERTIFICADO DE ORIGEN</div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label('Codigo Arancelario','Codigo Arancelario') !!}
                                <select name="codigo_arancelario" id="codigo_arancelario" class="form-control">
                                    
                                    @foreach($arancel as $row)
                                        <option value="{{ $row['codigo'] }}">{{ $row['codigo'] }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label('Certificado de Origen','Certificado de Origen') !!}
                                {!! Form::file('file', array('class' => 'file-input','style'=>'margin:10px;','id'=>'file','title'=>'formato permitido .pdf')) !!}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-4"></div>
                            <div class="col-md-4 text-center">
                            <a href="{{ url('Seniat/UsuarioSeniat-lista') }}" class="btn btn-primary">Cancelar</a>
                            <input type="submit" class="btn btn-success" value="Enviar" name="" onclick="enviardebito()">
                            </div>
                            <div class="col-md-4"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

{{Form::close()}}
@stop