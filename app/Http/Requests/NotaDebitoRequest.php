<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class NotaDebitoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        'gen_usuario_id'=>'required',
        'gen_factura_id'=>'required',
        'gen_consignatario_id'=>'required',
        'num_nota_debito'=>'required',
        'fecha_emision'=>'required',
        //'er'=>'required',
        'concepto_nota_debito'=>'required',
        'monto_nota_debito'=>'required',
        'justificacion'=>'required',
        
        ];
    }
}
