@extends('templates/layoutlte')
@section('content')
{{Form::open(['route'=>'Dua.store','method'=>'POST','id'=>'formdua']) }}
<div class="panels">
<div class="container">
    <div class="row">
        <div class="col-md-10">
            <div class="col-md-6">
	            <div class="form-group">
	            {!! Form::hidden('gen_usuario_id',Auth::user()->id) !!}
	            	{!! Form::label('','Número de DUA') !!}
	              {!! Form::text('numero_dua',null,['class'=>'form-control','id'=>'numero_dua','onkeypress'=>'return soloNumeros(event)','maxlength'=>'80']) !!}
	            </div>
	            <div class="form-group">
	            	  {!! Form::label('','Número de Referencia') !!}
	              {!! Form::text('numero_referencia',null,['class'=>'form-control','id'=>'numero_referencia','onkeypress'=>'return soloNumerosyLetras(event)','maxlength'=>'80']) !!}
	            </div>
	            <div class="form-group">
	            	  {!! Form::label('','Agente de Aduana') !!}
	              {!! Form::select('gen_agente_aduana_id',$agente,null,['class'=>'form-control', 'id'=>'gen_agente_aduana_id','placeholder'=>'Seleccione un Agente Aduanal',]) !!}
	            </div>
	            <div class="form-group">
	            	   {!! Form::label('','Modalidad de Transporte') !!}
	              {!!Form::select('gen_transporte_id',$transport,null,['class'=>'form-control', 'id'=>'gen_transporte_id','placeholder'=>'Seleccione una Modalidad de Transporte']) !!}
	            </div>
	            <div class="form-group">
	            	    {!! Form::label('','Aduana de Salida') !!}
	              {!! Form::select('gen_aduana_salida_id',$aduanasal,null,['class'=>'form-control','placeholder'=>'Seleccione la Aduana de Salida','id'=>'gen_aduana_salida_id']) !!}
	            </div>
	               <div class="form-group">
	              {!! Form::label('','Lugar de Salida') !!}
	              {!! Form::text('lugar_salida',null,['class'=>'form-control','id'=>'lugar_salida','onkeypress'=>'return soloLetras(event)','maxlength'=>'100']) !!}
	            </div>
            </div>
            <div class="col-md-6">
	            <div class="form-group">
	            	{!! Form::label('','País Destino') !!}

	            	{!! Form::select('aduana_llegada',$pais,null,['class'=>'form-control', 'id'=>'aduana_llegada','placeholder'=>'Seleccione el pais']) !!}
	           

	            </div>
	             <div class="form-group">
	            	{!! Form::label('','Lugar de llegada') !!}
	              {!! Form::text('lugar_llegada',null,['class'=>'form-control','id'=>'lugar_llegada','onkeypress'=>'return soloLetras(event)','maxlength'=>'100']) !!}
	            </div>
	            <div class="form-group">
	            	{!! Form::label('','Fecha Registro DUA en Aduanas') !!}
	            	<div class="input-group date">
	            		{!! Form::text('fregistro_dua_aduana',null,['class'=>'form-control','readonly'=>'readonly','id'=>'fregistro_dua_aduana']) !!}
	            		<span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
	            	</div>
	            </div>
	            <div class="form-group">
	            	  {!! Form::label('','Fecha de Embarque') !!}
	            	  <div class="input-group date">
	            	  	{!! Form::text('fembarque',null,['class'=>'form-control','readonly'=>'readonly','id'=>'fembarque']) !!}
	            	  	<span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
	            	  </div>
	            </div>
	             <div class="form-group">
	            	 {!! Form::label('','Fecha de Estimada de Arribo') !!}
	            	 <div class="input-group date">
	            	 	{!! Form::text('farribo',null,['class'=>'form-control','readonly'=>'readonly','id'=>'farribo']) !!}
	            	 	<span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
	            	 </div>
	            </div>
                <div class="form-group">
                {!! Form::label('', 'Comprador') !!}
                {!!Form::select('gen_consignatario_id',$consignatario,null,['placeholder'=>'Seleccione','class'=>'form-control','id'=>'gen_consignatario_id']) !!}
                 </div>

            </div>
        </div>
    </div>
        <div class="row">
            <div class="col-md-12">
            <div class="col-md-4"></div>
                <div class="col-md-4 text-center">
                  <div class="form-group">
                    <a href="{{route('Dua.index')}}" class="btn btn-primary">Cancelar</a>
                    <input class="btn btn-success" type="submit" name="" value="Enviar" onclick="validacionForm()">
                </div>
            </div>
            <div class="col-md-4"></div>
            </div>
        </div>
</div>
</div>
{{Form::close()}}

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="{{asset('js/ajax_v2.1.1.js')}}"></script>

<script type="text/javascript">
  	
	//Validación de formulario, se puede llamar y pasar la sección que contenga los inputs a validar

    	var error = '<div class="msg_alert"><span style="color:red" class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span><strong id="cod_aran-error" class="added"> El campo es obligatorio.</strong></div>';
		var valido = '<div class="msg_alert"><span style="color:green" class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span></div>';
		var mens = ['Estimado Usuario. Debe completar los campos solicitados', 'La fecha de registro DUA no puede ser mayor a la fecha de embarque']

		validacionForm = function() {

		$('#formdua').submit(function(event) {
		
		var campos = $('#formdua').find('input:text, select');
		var n = campos.length;
		var err = 0;

		$("div").remove(".msg_alert");
		//bucle que recorre todos los elementos del formulario
		for (var i = 0; i < n; i++) {
				var cod_input = $('#formdua').find('input:text, select').eq(i);
				if (!cod_input.attr('noreq')) {
					if (cod_input.val() == '' || cod_input.val() == null)
					{
						err = 1;
						cod_input.css('border', '1px solid red').after(error);
					}
					else{
						if (err == 1) {err = 1;}else{err = 0;}
						cod_input.css('border', '1px solid green').after(valido);
					}
					
				}
		}

		//Si hay errores se detendrá el submit del formulario y devolverá una alerta
		if(err==1){
				event.preventDefault();
				swal("Por Favor!", mens[0], "warning")
			}

			});
		}

</script>

@stop
