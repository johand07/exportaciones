<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DocumentsVariosInversiones extends Model
{
	  protected $table='documentos_varios_inversiones';

    protected $fillable =[

     'id',
     'cat_documentos_id',
     'gen_sol_inversionista_id',
     'file',
     'bactivo'


    ];


    
}
