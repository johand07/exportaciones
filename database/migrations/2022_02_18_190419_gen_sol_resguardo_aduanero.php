<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GenSolResguardoAduanero extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gen_sol_resguardo_aduanero', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('gen_usuario_id')->index()->nullable();
            $table->foreign('gen_usuario_id')->references('id')->on('gen_usuario');
            $table->unsignedInteger('status_resguardo')->index()->nullable();
            $table->foreign('status_resguardo')->references('id')->on('gen_status');
            $table->date('fstatus_resguardo')->nullable();
            $table->unsignedInteger('status_cna')->index()->nullable();
            $table->foreign('status_cna')->references('id')->on('gen_status');
            $table->date('fstatus_cna')->nullable();
            $table->string('num_sol_resguardo', 255)->nullable();
            $table->boolean('aprobada_resguardo')->default(0)->nullable();
            $table->boolean('aprobada_cna')->default(0)->nullable();
            $table->boolean('edo_observacion_resguardo')->default(0)->nullable();
            $table->text('observacion_resguardo')->nullable();
            $table->boolean('edo_observacion_cna')->default(0)->nullable();
            $table->text('observacion_cna')->nullable();
            $table->string('tlf_1', 150)->nullable();
            $table->string('tlf_2', 150)->nullable();
            $table->string('tlf_3', 150)->nullable();
            $table->text('ubicacion_georeferencial')->nullable();
            $table->boolean('bactivo')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gen_sol_resguardo_aduanero');
    }
}
