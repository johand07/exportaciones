<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GenDeclaracionJO extends Model
{
    //
    protected $table = 'gen_declaracion_jo';

	protected $fillable = [

           'gen_usuario_id',
           'gen_status_id',
           'fstatus',
           'num_solicitud',
           'num_productos',
           'num_paises',
           'tipoUsuario',
           'bactivo',
          
    ];
    
    public function usuario() {

     return $this->hasOne('App\Models\GenUsuario','id','gen_usuario_id');

    }
     public function status() {

     return $this->hasOne('App\Models\GenStatus','id','gen_status_id');

    }
    //con esta se pintan las planillas desde la 2 hasta la 6
    public function rDeclaracionProduc() {

     return $this->hasMany('App\Models\DetDeclaracionProduc','gen_declaracion_jo_id','id');

    }
    public function rDetUsuario() {

     return $this->hasOne('App\Models\DetUsuario','gen_usuario_id','gen_usuario_id');

    }
    public function rGenStatus() {

     return $this->hasOne('App\Models\GenStatus','id','gen_status_id');

    }
    public function rBandejaEntradaDjo() {

     return $this->hasOne('App\Models\GenBandejaEntradaDjo','gen_declaracion_jo_id','id');

    }
    public function rAnalisisCalificacion() {

     return $this->hasOne('App\Models\AnalisisCalificacion','gen_declaracion_jo_id','id');

    }

    public function rDocAdicionalDjo() {

     return $this->hasOne('App\Models\DocAdicionalDjo','gen_declaracion_jo_id','id');

    }


    //Relacion para acceder a la Observacion de la planilla2
    public function rPlanilla2Djo() {

     return $this->hasOne('App\Models\DocAdicionalDjo','gen_declaracion_jo_id','id');

    }


    

}
