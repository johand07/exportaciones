@extends('templates/layoutlte')
@section('content')
@php $contador=count($cuotas); @endphp
{{Form::hidden('metodo','Editar')}}
{{Form::hidden('contador',$contador)}}
{{Form::model($factura,['route'=>['factura.update',$factura->id],'method'=>'PATCH','id'=>'formFacturaEdit'])}}
  @include('factura.fields')
{{Form::close()}}
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="{{asset('js/ajax_v2.1.1.js')}}"></script>
<script>
    $(document).ready(function(){
   // escucha el evento submit del formulario miFormulario
   var err_edit = 1;
    //Funcion para detectar si existe algÃºn cambio dentro del formulario

   $('#formFacturaEdit').submit(function(e){
       var error = '<div class="msg_alert"><span style="color:red" class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span><strong id="cod_aran-error" class="added"> El campo es obligatorio.</strong></div>';
           var valido = '<div class="msg_alert"><span style="color:green" class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span></div>';
           var mens=['Estimado Usuario. Debe completar los campos solicitados',]
           
           var campos = $('#formFacturaEdit').find('input:text, select, input:file, textarea');
           var n = campos.length;
           var err = 0;
           
   
           $("div").remove(".msg_alert");
           //bucle que recorre todos los elementos del formulario
           for (var i = 0; i < n; i++) {
                   var cod_input = $('#formFacturaEdit').find('input:text, select, input:file, textarea').eq(i);
                   if (!cod_input.attr('noreq')) {
                       
                       if (cod_input.val() == '' || cod_input.val() == null)
                       {
                           err = 1;
                           cod_input.css('border', '1px solid red').after(error);
                       }
                       else{
                           if (err == 1) {err = 1;}else{err = 0;}
                           cod_input.css('border', '1px solid green').after(valido);
                       }
                       
                   }
           }
   
           //Si hay errores se detendrá el submit del formulario y devolverá una alerta
           if(err==1){
                   event.preventDefault();
                   swal("Por Favor!", mens[0], "warning")
               }  
               if(err_edit==1){
			event.preventDefault();
			swal("Por Favor debe hacer un modificacion para poder actualizar el registro!", mens[1], "warning")
		}

   });
  
   $("input:text, select, input:file, textarea").change(function(){
err_edit = 0;
});
});
   
  
</script>
@stop