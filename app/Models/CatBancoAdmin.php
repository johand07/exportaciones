<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CatBancoAdmin extends Model
{
    protected $table = 'cat_banco_admin';

    protected $fillable = [

           'nombre_banco',
           'tipo_cuenta',
           'num_cuenta',
           'ci_rif',
           'nombre_ente',
           'telf_ente',
           'img_banco',
           'moneda',
           'bactivo',
    ];

    protected $date =['created_at','updated_at'];

}
