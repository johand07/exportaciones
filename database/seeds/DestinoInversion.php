<?php

use Illuminate\Database\Seeder;
use App\Models\CatDestinoInversion;

class DestinoInversion extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $destinoInversion = [
            [
                'id'         => 1,
                'nombre_destino'       => 'Empresas nacionales, extranjeras o sucursales en todos sus tipos',
                'created_at' => '2021-07-20 20:59:08',
                'updated_at' => '2021-07-20 20:59:08',
            ],
            [
                'id'         => 2,
                'nombre_destino'       => 'Negocios fiduciarios',
                'created_at' => '2021-07-20 20:59:08',
                'updated_at' => '2021-07-20 20:59:08',
            ],
            [
                'id'         => 3,
                'nombre_destino'       => 'Inmuebles',
                'created_at' => '2021-07-20 20:59:08',
                'updated_at' => '2021-07-20 20:59:08',
            ],
            [
                'id'         => 4,
                'nombre_destino'       => 'Entidades de naturaleza cooperativa',
                'created_at' => '2021-07-20 20:59:08',
                'updated_at' => '2021-07-20 20:59:08',
            ],
            [
                'id'         => 5,
                'nombre_destino'       => 'Entidades sin ánimo de lucro',
                'created_at' => '2021-07-20 20:59:08',
                'updated_at' => '2021-07-20 20:59:08',
            ],
            [
                'id'         => 6,
                'nombre_destino'       => 'Instrumentos financieros como bonos y acciones',
                'created_at' => '2021-07-20 20:59:08',
                'updated_at' => '2021-07-20 20:59:08',
            ],
            [
                'id'         => 7,
                'nombre_destino'       => 'Actos o contratos sin participación en el capital',
                'created_at' => '2021-07-20 20:59:08',
                'updated_at' => '2021-07-20 20:59:08',
            ],
            [
                'id'         => 8,
                'nombre_destino'       => 'Fondos de capital privado',
                'created_at' => '2021-07-20 20:59:08',
                'updated_at' => '2021-07-20 20:59:08',
            ],
            
            
        ];

        CatDestinoInversion::insert($destinoInversion);
    }
}
