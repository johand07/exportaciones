@extends('templates/layoutlte')
@section('content')
{{Form::open(['route' =>'productos.store' ,'method'=>'POST','id'=>'FormProd','name'=>'add_product','onSubmit="validarProductosFactura(); return false"'])}}
{{Form::hidden('metodo','Crear')}}

@include('productos.fields')
@push('productos')
var i = 0;
$('#add').click(function(e){
e.preventDefault();
i++;
var row = 'row'+i;
$('#fields tr:last').after('<tr id="row'+i+'">
    <td>{!!Form::text('codigo_arancel[]',null,['class'=>'form-control','data-toggle'=>"modal",'data-target'=>"#modal",'id'=>"cod_arancel",'onkeypress'=>'return
        soloNumerosDouble(event)','required'])!!}</td>
    <td>{!!Form::text('descripcion_arancelaria[]',null,['class'=>'form-control','id'=>'arancel_descrip','rows' =>
        1,'required'])!!}</td>
    <td>{!!Form::text('descripcion_comercial[]',null,['class'=>'form-control','onkeypress'=>'return
        soloLetras(event)','rows' => 1,'required'])!!}</td>
    <td>{!!Form::text('cantidad_producto[]',null,['class'=>'form-control format','id'=>'cantidad_producto','maxlength'
        =>'10','required'])!!}</td>
    <td>{!!Form::text('precio_producto[]',null,['class'=>'form-control format text-right','id'=>'precio_producto','required'])!!}
    </td>
    <td>{!!Form::text('valor_fob[]',null,['class'=>'form-control text-right','readOnly'=>'readOnly','id'=>'montoFob'])!!}</td>
    <td>{!!Form::select('unidad_medida_id[]',$medidas,null,['class'=>'form-control','placeholder'=>'Seleccione','id'=>'unidad_medida_id','required'])!!}
    </td>
    <td><button name="remove" type="button" id="'+i+'" value="" class="btn btn-danger btn-remove">x</button></td>
</tr>');
var main = document.getElementById(row);
main.querySelectorAll('#cantidad_producto')[0].setAttribute("onInput",
"calcularMonto('"+row+"','cantidad_producto','precio_producto','montoFob')");
main.querySelectorAll('#precio_producto')[0].setAttribute("onInput",
"calcularMonto('"+row+"','cantidad_producto','precio_producto','montoFob')");
$(".format").on({
"keypress": function(e) {
key = e.keyCode || e.which;
tecla = String.fromCharCode(key).toLowerCase();
letras = "0123456789.";
especiales = "8-37-39-46";
tecla_especial = false
for(var i in especiales){
if(key == especiales[i]){
tecla_especial = true;
break;
}
}
if(letras.indexOf(tecla)==-1 && !tecla_especial){
return false;
}
}
});
});
@endpush
<script src="{{asset('js/ajax_v2.1.1.js')}}"></script>
<script>
    $(document).ready(function () {
        @stack('productos')
    });
</script>
@stop