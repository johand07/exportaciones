function cambiarImagen(event) {
    var id= event.target.getAttribute('name');
    var reader = new FileReader();
    reader.onload = function(){
      var output = document.getElementById('vista_previa_'+id);
      output.src = reader.result;
    };
    let archivo = $(".file-input").val();
    if (!archivo) {
      archivo=$('#'+id).val();
    }
    var extensiones = archivo.substring(archivo.lastIndexOf(".")).toLowerCase();
    
    //var imgfile="/exportaciones/public/img/file.png";

//alert(imgfile);
    if(extensiones != ".doc" && extensiones != ".docx" && extensiones != ".pdf" && extensiones != ".ppt" && extensiones != ".pptx"){
      swal("Formato invalido", "Formato de archivo invalido debe ser .doc, .docx, .ppt, .pptx  o pdf", "warning");
        
        
    }else{
      $('#imgdocumento_'+id).show();
      $('#imgdocumentoEdit_'+id).hide();
       let file=$('#'+id).val();
     // alert(file);
     //Y pintame ruta y nombre del archivo acargado con nombre_
        $('#nombre_'+id).html('<h4>'+file+'</h4>');

      reader.readAsDataURL(event.target.files[0]);
    }

  }

   $(document).on('change', '.file-input', function(evt) {
           cambiarImagen(evt);
    });


/********************************************************************** */


/********************************************************************** */

$(document).ready(function (){
     /* agregar para productos completos en el caso colombia*/

     var x = $('input[name="contador"]').val();
    var i=parseInt(x);
    
    $('#add-productos').click(function(){
        i++;

        $('#productos tr:last').after('<tr id="row1'+i+'"display="block" class="show_div"><td>{!!Form::text("codigo_arancel[]",null,["class"=>"form-control","id"=>"codigo_arancel"])!!}</td><td>{!!Form::text("denominacion_mercancia[]",null,["class"=>"form-control","id"=>"denominacion_mercancia"])!!}</td><td>{!!Form::text("unidad_fisica[]",null,["class"=>"form-control","id"=>"unidad_fisica"])!!}</td"><td>{!!Form::hidden("id_productos[]",0)!!}<button  name="remove" type="button" id="'+i+'" value="" class="btn btn-danger btn-remove">x</button></td></tr>');
    });

   $(document).on('click','.btn-remove',function(){
        var id_boton= $(this).attr("id");
        var id=$(this).prev().val();
        
        if(id!=0){

            if (confirm('Deseas eliminar este Registro?')) {

                $.ajax({

                'type':'get',
                'url': 'eliminarRegistroProd',
                'data':{ 'id':id},
                success: function(data){

                    $('#row'+id_boton).remove();

                    }

                });

            }



        }else{

        $('#row'+id_boton).remove();
        }

    });


    //Validación de formulario, se puede llamar y pasar la sección que contenga los inputs a validar
		
		var error = '<div class="msg_alert"><span style="color:red" class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span><strong id="cod_aran-error" class="added"> El campo es obligatorio.</strong></div>';
		var valido = '<div class="msg_alert"><span style="color:green" class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span></div>';
		var mens=['Estimado Usuario. Debe completar los campos solicitados', 'Estimado Usuario. Debe realizar los cambios solicitados en la observación']
    var err_edit = 1;

		validacionForm = function() {

		$('#FormProd').submit(function(event) {
		
		var campos = $('#FormProd').find('input:text, select, textarea');
		var n = campos.length;
    var err = 0;

		$("div").remove(".msg_alert");
		//bucle que recorre todos los elementos del formulario
		for (var i = 0; i < n; i++) {
				var cod_input = $('#FormProd').find('input:text, select, textarea').eq(i);
				if (!cod_input.attr('noreq')) {
					if (cod_input.val() == '' || cod_input.val() == null)
					{
						err = 1;
						cod_input.css('border', '1px solid red').after(error);
					}
					else{
						if (err == 1) {err = 1;}else{err = 0;}
						cod_input.css('border', '1px solid green').after(valido);
					}
					
				}
		}

		//Si hay errores se detendrá el submit del formulario y devolverá una alerta
		if(err==1){
				event.preventDefault();
				swal("Por Favor!", mens[0], "warning")
			}

      if(err_edit==1){
				event.preventDefault();
				swal("Por Favor!", mens[1], "warning")
			}

			});
	}

  //Función para detectar si existe algún cambio dentro del formulario
  $("input:text, select, input:file, textarea").change(function(){
    err_edit = 0;
  });



 });/*llave del document).ready*/