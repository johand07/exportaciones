@extends('templates/layoutlte')
@section('content')
<div class="panels">
   <div class="row">
  <div class="col-md-12">
    <div class="row">
   
      <div class="col-md-2">
        <a class="btn btn-primary" href="{{url('/exportador/Dua/create')}}">
          <span class="glyphicon glyphicon-file" title="Agregar" aria-hidden="true"></span>
           Registro DUA
        </a>
      </div>
    </div> <hr>
    <table id="listadua" class="table table-striped table-bordered" style="width:100%">
            <thead>
                <tr>

                    <th>#</th>
                    <th>N° Dua</th>
                    <th>Consignatario</th>
                    <th>Ref</th>
                    <th>Aduana Sal.</th>
                    <th>Lugar Sal.</th>
                    <th>Aduana LLegada</th>
                    <!--th>LLegada</!--th-->
                    <th>F. Reg. Aduana</th>
                    <th>F. Embarque</th>
                    <th>F. Arribo</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody>
              @foreach($duas as $key=> $dua)
                <tr>

                    
                    <td>{{$key+1}}</td>
                    <td>{{$dua->numero_dua}}</td>
                   <td>@if(!is_null($dua->consignatario)){{$dua->consignatario->nombre_consignatario}}@endif</td>
                    <td>{{$dua->numero_referencia}}</td>
                    <td>{{$dua->aduanassal->daduana}}</td>
                    <td>{{$dua->lugar_salida}}</td>
                    <td>{{$dua->aduana_llegada}}</td>
                    <!--td>{{$dua->lugar_llegada}}</!--td-->
                    <td>{{$dua->fregistro_dua_aduana}}</td>
                    <td>{{$dua->fembarque}}</td>
                    <td>{{$dua->farribo}}</td>
                    <td>
                    @if($dua->bactivo!=0)
                       <a href="{{route('Dua.edit',$dua->id)}}" class="glyphicon glyphicon-edit btn btn-primary btn-sm" title="Modificar" aria-hidden="true"></a>

                       <input id="token" type="hidden" name="_token" value="{{ csrf_token() }}">
                       <a href="#" class="btn btn-danger btn-sm" title="Eliminar" onclick="return eliminar('Dua/',{{$dua->id}},'listadua')"> <i class="glyphicon glyphicon-trash"></i> </a>
                     @else
                      
                       <span style="color:red">Dua Inactiva</span>

                     @endif
                    </td>
                </tr>
              @endforeach
            </tbody>
        </table>
  </div>
</div>
</div>
@stop
