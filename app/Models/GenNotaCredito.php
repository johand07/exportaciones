<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GenNotaCredito extends Model
{
    protected $table = 'gen_nota_credito';

    protected $fillable = [

        'gen_usuario_id',
        'gen_consignatario_id',
        'numero_nota_credito',
        'femision',
        'concepto_nota_credito',
        'monto_nota_credito',
        'gen_factura_id',
        'justificacion',
        'bactivo',
    ];
	protected $date =['created_at','updated_at'];

	 public function factura() {

     return $this->hasOne('App\Models\GenFactura','id','gen_factura_id');

    }

     public function usuario() {

     return $this->hasOne('App\Models\GenUsuario','id','gen_usuario_id');

    }


    public function consignatario() {

     return $this->hasOne('App\Models\GenConsignatario','id','gen_consignatario_id');

    }
}
