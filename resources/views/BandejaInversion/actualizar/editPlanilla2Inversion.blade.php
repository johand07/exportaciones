@extends('templates/layoutlte')

@section('content')

<!--Aqui Form::open-->

{{Form::model($planillaDjir02,['route'=>['actualizarPlanilla2Inversion.update',$planillaDjir02->id],'method'=>'PATCH','id'=>'formPlanilla1Inversion'])}}
@if(isset($planillaDjir02->descrip_observacion) && $planillaDjir02->estado_observacion == 1)
<div class="alert alert-dismissible alert-warning">
  <button type="button" class="close" data-dismiss="alert">&times;</button>
  <h4 class="alert-heading">Observación!</h4>
  <p class="mb-0">{{$planillaDjir02->descrip_observacion}}</p>
</div>
@endif
<!--***************Parte 1 Registro****************************-->
{!! Form::hidden('estado_observacion',null,['class'=>'form-control','id'=>'estado_observacion','onkeypress'=>'return soloNumeros(event)'])!!}
{!! Form::hidden('planilla2_id',$planillaDjir02->id,['class'=>'form-control text-center']) !!}
{!! Form::hidden('gen_declaracion_inversion_id',$gen_declaracion_inversion_id,['class'=>'form-control text-center']) !!}
      <div id="datos_inver1">
        <ol class="breadcrumb">
          <li class="active" style="color:#337AB7">Paso 1</li>
        </ol>
        <div class="panel panel-primary">
          <div class="panel-heading"><h4>Información General</h4></div>
          <div class="panel-body">
             <div class="row"><br>
                   <div class="col-md-1"></div>
                   <div class="col-md-10 text-center">
                      {!! Form::label('','Pertenece a un Grupo Empresarial Internacional?',['class'=>'form-control'])!!}<br>
                      {!! Form::label('','Si',['class'=>'radio-inline'])!!}
                      {!! Form::radio('grupo_emp_internacional','1',false,['class'=>'radio-inline','id'=>'inver1'])!!}
                      {!! Form::label('','No',['class'=>'radio-inline'])!!}
                      {!! Form::radio('grupo_emp_internacional','2',false,['class'=>'radio-inline','id'=>'inver2'])!!}
                    </div>
                  <div class="col-md-1"></div>
                </div><br>
                <div class="row" id="datos_repre" style="display:none;">
                  <div class="col-md-12">
                     <table class="table borde table-responsive table-bordered" border="2">
                      <tr>
                        <td><h4><b>Si respondió si, por favor mencione las empresas relacionadas:</b></h4></td>
                        <td>{!! Form::text('emp_relacionadas_internacional',null,['class'=>'form-control text-center','onkeypress'=>'return soloNumerosyLetras(event)','id'=>'emp_relacionadas_internacional']) !!}<span><b style="color:#E01313">Debe incluir las empresas separadas por guiones</b></span></td>
                      </tr>
                      <tr>
                        <td style="width:40%"><b>Casa Matriz:</b></td>
                        <td>{!! Form::text('casa_matriz',null,['class'=>'form-control text-center','onkeypress'=>'return soloLetras(event)','id'=>'casa_matriz']) !!}</td>
                      </tr>
                      <tr>
                        <td style="width:40%"><b>Empresa Representante de la Casa Matriz para <br>América Latina:</b></td>
                        <td>{!! Form::text('emp_repre_casa_matriz',null,['class'=>'form-control text-center','onkeypress'=>'return soloLetras(event)','id'=>'emp_repre_casa_matriz']) !!}</td>
                      </tr>
                      <tr>
                        <td style="width:40%"><b>Afiliadas:</b></td>
                        <td>{!! Form::text('afiliados_internacional',null,['class'=>'form-control text-center','onkeypress'=>'return soloLetras(event)','id'=>'afiliados_internacional']) !!}</td>
                      </tr>
                      <tr>
                        <td style="width:40%"><b>Subsidiarias de su empresa en el Extranjero:</b></td>
                        <td>{!! Form::text('sub_emp_extranjero',null,['class'=>'form-control text-center','onkeypress'=>'return soloLetras(event)','id'=>'sub_emp_extranjero']) !!}</td>
                      </tr>
                    </table>
                  </div>
                </div>
                <br>
                <div class="row"><br>
                   <div class="col-md-1"></div>
                   <div class="col-md-10 text-center">
                      {!! Form::label('','Pertenece a un Grupo Empresarial Nacional?',['class'=>'form-control'])!!}<br>
                      {!! Form::label('','Si',['class'=>'radio-inline'])!!}
                      {!! Form::radio('grupo_emp_nacional',1,false,['class'=>'radio-inline','id'=>'nacional1'])!!}
                      {!! Form::label('','No',['class'=>'radio-inline'])!!}
                      {!! Form::radio('grupo_emp_nacional',2,false,['class'=>'radio-inline','id'=>'nacional2'])!!}
                    </div>
                  <div class="col-md-1"></div>
                </div><br>
                <div class="row" id="datos_emp" style="display:none;">
                  <div class="col-md-12">
                     <table class="table borde table-responsive table-bordered" border="2">
                      <tr>
                        <td style="width:40%"><b>Empresa Holding, Corporación o Consorcio:</b></td>
                        <td>{!! Form::text('emp_haldig_coorp',null,['class'=>'form-control text-center','onkeypress'=>'return soloNumerosyLetras(event)','id'=>'emp_haldig_coorp']) !!}<span><b style="color:#E01313">Debe incluir las empresas o Consorcio separadas por guiones</b></span></td>
                      </tr>
                      <tr>
                        <td style="width:40%"><b>Afiliadas:</b></td>
                        <td>{!! Form::text('afiliados_nacional',null,['class'=>'form-control text-center','onkeypress'=>'return soloLetras(event)','id'=>'afiliados_nacional']) !!}</td>

                      </tr>
                      <tr>
                        <td style="width:40%"><b>Subsidiarias locales:</b></td>
                        <td>{!! Form::text('sub_locales',null,['class'=>'form-control text-center','onkeypress'=>'return soloLetras(event)','id'=>'sub_locales']) !!}</td>
                      </tr>
                    </table>
                  </div>
                </div>
                 <div class="row"><br>
                   <div class="col-md-1"></div>
                   <div class="col-md-10 text-center">
                    <label class="form-control" style="height:5%"><b>Ha presentado ante el Viceministerio de Comercio Exterior y Promoción de Inversiones  los documentos que acreditan la representación legal o poder?</b></label><br>
                      {!! Form::label('','Si',['class'=>'radio-inline'])!!}
                      {!! Form::radio('present_doc_repre_legal','1',false,['class'=>'radio-inline','id'=>'reprelegal1'])!!}
                      {!! Form::label('','No',['class'=>'radio-inline'])!!}
                      {!! Form::radio('present_doc_repre_legal','2',false,['class'=>'radio-inline','id'=>'reprelegal2'])!!}
                    </div><br>
                  <div class="col-md-1"></div>
                </div><br>
                 <div class="row" id="representante" style="display:none;">
                  <div class="col-md-12">
                   <table class="table borde table-responsive table-bordered" border="2">
                      <tr>
                        <td><b>EN CASO AFIRMATIVO, PROPORCIONE LOS SIGUIENTES DATOS:</b></td>
                      </tr>
                      <tr>
                        <td style="width:40%"><b>N° de Apostilla:</b></td>
                        <td>{!! Form::number('num_apostilla',null,['class'=>'form-control text-center','onkeypress'=>'return soloLetras(event)','id'=>'num_apostilla']) !!}</td>
                      </tr>
                      <tr>
                        <td style="width:40%"><b>Fecha:</b></td>
                        <td><div class="input-group date">
                        {!! Form::text('fecha_apostilla',null,['class'=>'form-control','readonly'=>'readonly','id'=>'fecha_apostilla']) !!}
                        <span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
                      </div></td>
                      </tr>
                      <tr>
                        <td style="width:40%"><b>País:</b></td>
                        <td>{!! Form::text('pais',null,['class'=>'form-control text-center','onkeypress'=>'return soloLetras(event)','id'=>'pais']) !!}</td>
                      </tr>
                       <tr>
                        <td style="width:40%"><b>Estado o Ciudad:</b></td>
                        <td>{!! Form::text('estado',null,['class'=>'form-control text-center','onkeypress'=>'return soloLetras(event)','id'=>'estado']) !!}</td>
                      </tr>
                       <tr>
                        <td style="width:40%"><b>Autoridad competente para
                        <br>apostillar:</b></td>
                        <td>{!! Form::text('autoridad_apostilla',null,['class'=>'form-control text-center','onkeypress'=>'return soloLetras(event)','id'=>'autoridad_apostilla']) !!}</td>
                      </tr>
                      <tr>
                        <td style="width:40%"><b>Cargo:</b></td>
                        <td>{!! Form::text('cargo',null,['class'=>'form-control text-center','onkeypress'=>'return soloLetras(event)','id'=>'cargo']) !!}</td>
                      </tr>
                       <tr>
                        <td style="width:40%"><b>Traductor:</b></td>
                        <td>{!! Form::text('traductor',null,['class'=>'form-control text-center','onkeypress'=>'return soloLetras(event)','id'=>'traductor']) !!}</td>
                      </tr>
                      <tr>
                        <td style="width:40%"><b>Datos Adicionales:</b></td>
                        <td>{!! Form::text('datos_adicionales',null,['class'=>'form-control text-center','onkeypress'=>'return soloLetras(event)','id'=>'datos_adicionales']) !!}</td>
                      </tr>
                    </table>
                  </div>
                </div>
<!------------------------------>
              <div class="row">
                <div class="col-md-12">
                  <br>
                  <table class="table borde table-responsive table-bordered" border="2">
                    <tr>
                      <td style="width:40%"><b>Ingresos anuales promedio del último ejercicio </b></td>
                      <td>{!! Form::number('ingresos_anual_ult_ejer',null,['class'=>'form-control text-center','onkeypress'=>'return soloNumeros(event)','id'=>'ingresos_anual_ult_ejer']) !!}</td>

                    </tr>
                    <tr>
                      <td style="width:40%"><b>Egresos anuales promedio del último ejercicio </b></td>
                      <td>{!! Form::number('egresos_anual_ult_ejer',null,['class'=>'form-control text-center','onkeypress'=>'return soloNumeros(event)','id'=>'egresos_anual_ult_ejer']) !!}</td>

                    </tr>
                    <tr>
                      <td style="width:40%"><b>Total balance del último ejercicio</b></td>
                      <td>{!! Form::number('total_balance_ult_ejer',null,['class'=>'form-control text-center','onkeypress'=>'return soloNumeros(event)','id'=>'total_balance_ult_ejer']) !!}</td>
                    </tr>
                  </table>
                </div>
              </div>    
          <!-----Botones------>
            <div class="row">
              <div class="col-md-12">
                <div class="col-md-4"></div>
                <div class="col-md-4 text-center">
                  <div class="form-group">
                    <a class="btn btn-primary" href="#" id="seguir1">Siguiente</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
<!--**********************************************************************-->
<div id="datos_inver2" style="display: none;">
  <ol class="breadcrumb">
    <li href="#" vid="paso4" style="color:#337AB7">Paso 1</li>
    <li class="active"  id="paso3" style="color:#337AB7">Paso 2</li>
  </ol>
  <div class="panel panel-primary">
  <div class="panel-heading"><h4>Detalle de la Declaración Jurada de la Inversión Realizada</h4></div>
    <div class="panel-body">
      <div class="row">
        <div class="col-md-12">
            <label><h4><b>Año de Declaración</b></h4></label>
          {!!Form::select('anio_informacion_financiera',$aniodeclaracion,null,
              ['class'=>'form-control','id'=>'anio_informacion_financiera','placeholder'=>'--Seleccione una Opción--'])!!}
        </div>
      </div>
      <br><br>
      <div class="row">
        <div class="panel panel-primary">
          <div class="panel-heading"><h4>Inversión Financiera en divisas y/o cualquier otro medio de cambio</h4></div>
          <div class="col-md-12"><br>
            <label><h4><b>Tipo de Moneda</b></h4></label>
             {!!Form::select('moneda_informacion_financiera',$divisas,null,
              ['class'=>'form-control','id'=>'moneda_informacion_financiera','placeholder'=>'--Seleccione una Opción--'])!!}
          </div>
          <div class="col-md-12">
            <br>
              <table class="table borde table-responsive table-bordered" border="2">
              <tr>
                <td style="width:40%"><b>Moneda extranjera:</b></td>
                <td>{!! Form::number('moneda_extranjera',null,['class'=>'form-control text-center','onkeypress'=>'return soloNumeros(event)','id'=>'moneda_extranjera']) !!}</td>
                
              </tr>
              <tr>
                <td style="width:40%"><b>Utilidades reinvertidas:</b></td>
                <td>{!! Form::number('utilidades_reinvertidas',null,['class'=>'form-control text-center','onkeypress'=>'return soloNumeros(event)','id'=>'utilidades_reinvertidas']) !!}</td>
                
              </tr>
              <tr>
                <td style="width:40%"><b>Crédito con casa matriz y/o filial extranjera: </b></td>
                <td>{!! Form::number('credito_casa_matriz',null,['class'=>'form-control text-center','onkeypress'=>'return soloNumeros(event)','id'=>'credito_casa_matriz']) !!}</td>
              </tr>
            </table>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="panel panel-primary">
          <div class="panel-heading"><h4>Bienes de capital fisico o tangibles (Maquinarias, equipos,  herramientas o cualquier otro tipo de activo tangible). </h4></div>
           <div class="col-md-12"><br>
            <label><h4><b>Tipo de Moneda</b></h4></label>
              {!!Form::select('moneda_bienes_tangibles',$divisas,null,
              ['class'=>'form-control','id'=>'moneda_bienes_tangibles','placeholder'=>'--Seleccione una Opción--'])!!}
          </div>
          <div class="col-md-12">
            <br>
            <table class="table borde table-responsive table-bordered" border="2">
              <tr>
                <td style="width:40%"><b>Tierras y terrenos:</b></td>
                <td>{!! Form::number('tierras_terrenos',null,['class'=>'form-control text-center','onkeypress'=>'return soloNumeros(event)','id'=>'tierras_terrenos']) !!}</td>

              </tr>
              <tr>
                <td style="width:40%"><b>Edificios y otras construcciones:</b></td>
                <td>{!! Form::number('edificios_construcciones',null,['class'=>'form-control text-center','onkeypress'=>'return soloNumeros(event)','id'=>'edificios_construcciones']) !!}</td>

              </tr>
              <tr>
                <td style="width:40%"><b>Maquinarias, equipos y herramientas:</b></td>
                <td>{!! Form::number('maquinarias_eqp_herra',null,['class'=>'form-control text-center','onkeypress'=>'return soloNumeros(event)','id'=>'maquinarias_eqp_herra']) !!}</td>

              </tr>
              <tr>
                <td style="width:40%"><b>Equipos de transporte:</b></td>
                <td>{!! Form::number('eqp_transporte',null,['class'=>'form-control text-center','onkeypress'=>'return soloNumeros(event)','id'=>'eqp_transporte']) !!}</td>

              </tr>
              <tr>
                <td style="width:40%"><b>Otros activos fijos tangibles:</b></td>
                <td>{!! Form::number('otros_activos_tangibles',null,['class'=>'form-control text-center','onkeypress'=>'return soloNumeros(event)','id'=>'otros_activos_tangibles']) !!}</td>

              </tr>
              <tr>
                <td style="width:40%"><b>Muebles, enseres y equipos de oficina:</b></td>
                <td>{!! Form::number('muebles_enceres',null,['class'=>'form-control text-center','onkeypress'=>'return soloNumeros(event)','id'=>'muebles_enceres']) !!}</td>

              </tr>
            </table>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="panel panel-primary">
          <div class="panel-heading"><h4>Bienes inmateriales o intangible (Software, patentes, derechos de marca u otros activos intangibles).</h4></div>
           <div class="col-md-12"><br>
            <label><h4><b>Tipo de Moneda</b></h4></label>
               {!!Form::select('moneda_bienes_intangibles',$divisas,null,
              ['class'=>'form-control','id'=>'moneda_bienes_intangibles','placeholder'=>'--Seleccione una Opción--'])!!}
          </div>
          <div class="col-md-12">
            <br>
              <table class="table borde table-responsive table-bordered" border="2">
              <tr>
                <td style="width:40%"><b>Software:</b></td>
                <td>{!! Form::number('software',null,['class'=>'form-control text-center','onkeypress'=>'return soloNumeros(event)','id'=>'software']) !!}</td>
              </tr>
               <tr>
                <td style="width:40%"><b>Derechos de propiedad intelectual:</b></td>
                <td>{!! Form::number('derecho_prop_intelectual',null,['class'=>'form-control text-center','onkeypress'=>'return soloNumeros(event)','id'=>'derecho_prop_intelectual']) !!}</td>
              </tr>
              <tr>
                <td style="width:40%"><b>Contribuciones tecnológicas intangibles:</b></td>
                <td>{!! Form::number('contribuciones_tecno',null,['class'=>'form-control text-center','onkeypress'=>'return soloNumeros(event)','id'=>'contribuciones_tecno']) !!}</td>
                
              </tr>
              <tr>
                <td style="width:40%"><b>Otros activos fijos intangibles:</b></td>
                <td>{!! Form::number('otros_activos_intangibles',null,['class'=>'form-control text-center','onkeypress'=>'return soloNumeros(event)','id'=>'otros_activos_intangibles']) !!}</td>
              </tr>
               <tr>
                <td style="width:40%" class="text-center"><b>TOTAL</b></td>
                <td>{!! Form::number('total_modalidad_inv',null,['class'=>'form-control text-center','onkeypress'=>'return soloNumeros(event)','id'=>'total_modalidad_inv']) !!}</td>
              </tr>
            </table>
          </div>
        </div>
      </div>
        <br>
        <br>
      <!---Botones-->
      <div class="row">
        <div class="col-md-12">
          <div class="col-md-4"></div>
          <div class="col-md-4 text-center">
            <div class="form-group">
              <a class="btn btn-primary" href="#" id="atras1">Atras</a>
              <a class="btn btn-primary" href="#" id="seguir2">Siguiente</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!--*************************************-->
<div id="datos_inver3" style="display: none;">
  <ol class="breadcrumb"> 
    <li href="#" vid="paso4" style="color:#337AB7">Paso 1</li>
    <li href="#" vid="paso4" style="color:#337AB7">Paso 2</li>
    <li class="active"  id="paso3" style="color:#337AB7">Paso 3</li>
  </ol>
  <div class="panel panel-primary">
  <div class="panel-heading"><h4>Tipo de Inversión</h4></div>
    <div class="panel-body">
      <div class="row">
        <table class="table table-responsive table-bordered">
          <thead>
            <tr>
             <td style="width:50%"><b>Inversión Extranjera Directa</b> se entiende la inversión productiva efectuada a través de los aportes realizados por los inversionistas extranjeros conformados por recursos tangibles o financieros, destinados 
              a formar parte del patrimonio de los sujetos receptores de inversión extranjera en el territorio nacional, con la finalidad de generar valor agregado al proceso productivo en el que se inserta. Estos aportes deben representar una participación igual o superior al 10% del capital societario)</td>
              <td style="width:50%">{!! Form::radio('tipo_inversion',1,false,['class'=>'radio-inline','id'=>'tipo_inversion'])!!}
              </td>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td style="width:50%"><b>Inversión de Cartera</b>(Se refiere a la adquisición de acciones o participaciones societarias en todo tipo de empresas que representen un nivel de participación en el patrimonio societario inferior al diez por ciento (10%)</td>
              <td style="width:50%">{!! Form::radio('tipo_inversion',2,false,['class'=>'radio-inline','id'=>'tipo_inversion'])!!}</td>
            </tr>
          </tbody>
        </table>
      </div>
      <div class="row">
        <div class="panel panel-primary">
          <div class="panel-heading"><h4>RESUMEN <br>Estimación de la Inversión Realizada (DJIR -02)</h4></div>
          <div class="col-md-12">
            <br>
            <table class="table borde table-responsive table-bordered" border="2">
              <tr>
                <td style="width:40%" class="text-center"><b>Modalidad de la Inversión</b></td>
                <td style="width:40%" class="text-center"><b>Monto</b></td>
              </tr>
              <tr>
                <td style="width:40%"><b>Inversión Financiera en divisas y/o cualquier otro medio de cambio</b></td>
                <td style="width:50%">{!! Form::number('invert_divisa_cambio',null,['class'=>'form-control text-center','onkeypress'=>'return soloNumeros(event)','id'=>'invert_divisa_cambio']) !!}</td>
              </tr>
              <tr>
                <td style="width:40%"><b>Bienes de capital fisico o tangibles (Maquinarias, equipos, herramientas o cualquier otro tipo de activo tangible).</b></td>
                <td style="width:50%">{!! Form::number('bienes_cap_fisico_tangibles',null,['class'=>'form-control text-center','onkeypress'=>'return soloNumeros(event)','id'=>'bienes_cap_fisico_tangibles']) !!}</td>
              </tr>
              <tr>
                <td style="width:40%"><b>Bienes inmateriales o intangible (Software, patentes, derechos de marca u otros activos intangibles.</b></td>
                <td style="width:50%">{!! Form::number('bienes_inmateriales_intangibles',null,['class'=>'form-control text-center','onkeypress'=>'return soloNumeros(event)','id'=>'bienes_inmateriales_intangibles']) !!}</td>
              </tr>
              <tr>
                <td style="width:40%"><b>Reinversiones de utilidades ( SOLO PARA PROCESO DE ACTUALIZACIÓN)</b></td>
                <td style="width:50%">{!! Form::number('reinversiones_utilidades',null,['class'=>'form-control text-center','onkeypress'=>'return soloNumeros(event)','id'=>'reinversiones_utilidades']) !!}</td>
              </tr>
              <tr>
                <td style="width:40%"><b>Otra (especifique)</b></td>
                <td>{!! Form::number('especifique_otro',null,['class'=>'form-control text-center','onkeypress'=>'return soloNumeros(event)','id'=>'especifique_otro']) !!}</td>
              </tr>
              <tr>
                <td style="width:40%" class="text-center"><b>TOTAL</b></td>
                <td>{!! Form::number('total_modalidad_inv',null,['class'=>'form-control text-center','onkeypress'=>'return soloNumeros(event)','id'=>'total_modalidad_inv']) !!}</td>
              </tr>
            </table>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <table class="table borde table-responsive table-bordered" border="2">
                  <tr>
                    <td><b>Cuál fue la base de estimación?</b></td>
                    <td></td>
                  </tr>
                  <tr>
                    <td><b>Valor en libros (valor del patrimonio)</b></td>
                    <td>{!! Form::number('valor_libros',null,['class'=>'form-control text-center','onkeypress'=>'return soloNumeros(event)','id'=>'valor_libros']) !!}</td>
                  </tr>
                  <tr>
                    <td><b>Valor neto de activos</b></td>
                    <td>{!! Form::number('valor_neto_activos',null,['class'=>'form-control text-center','onkeypress'=>'return soloNumeros(event)','id'=>'valor_neto_activos']) !!}</td>
                  </tr>
                  <tr>
                    <td><b>Valor de una empresa similar</b></td>
                    <td>{!! Form::number('valor_emp_similar',null,['class'=>'form-control text-center','onkeypress'=>'return soloNumeros(event)','id'=>'valor_emp_similar']) !!}</td>
                  </tr>
                   <tr>
                    <td><b>Otros (especificar)</b></td>
                    <td>{!! Form::number('otro_especifique',null,['class'=>'form-control text-center','onkeypress'=>'return soloNumeros(event)','id'=>'otro_especifique']) !!}</td>
                  </tr>
            </table>
        </div>
      </div>
      <br><br>
      <!---Botones-->
      <div class="row">
        <div class="col-md-12">
          <div class="col-md-4"></div>
          <div class="col-md-4 text-center">
            <div class="form-group">
            <a href="#" class="btn btn-primary" id="atras2">Atras</a>
               <input type="submit" class="btn btn-success" value="Guardar" name="Guardar" onclick="enviarInversionistaEdit()"> 
            </div>
          </div>
          <div class="col-md-4"></div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-------Modal--->
{{Form::close()}}
@stop
 