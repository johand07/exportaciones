<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GenDua extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('gen_dua', function (Blueprint $table) {
          $table->engine = 'InnoDB';
          $table->increments('id');
          $table->unsignedInteger('gen_usuario_id')->index()->nullable();
          $table->foreign('gen_usuario_id')->references('id')->on('gen_usuario');
          $table->unsignedInteger('gen_agente_aduana_id')->index()->nullable();
          $table->foreign('gen_agente_aduana_id')->references('id')->on('gen_agente_aduanal');
          $table->unsignedInteger('gen_transporte_id')->index()->nullable();
          $table->foreign('gen_transporte_id')->references('id')->on('gen_transporte');
          $table->unsignedInteger('gen_aduana_salida_id')->index()->nullable();
          $table->foreign('gen_aduana_salida_id')->references('id')->on('gen_aduana_salida');
          $table->unsignedInteger('gen_consignatario_id')->index()->nullable();
          $table->foreign('gen_consignatario_id')->references('id')->on('gen_consignatario');
          $table->string('lugar_salida', 100)->nullable();
          $table->string('numero_dua', 80)->nullable();
          $table->string('numero_referencia', 80)->nullable();
          $table->string('aduana_llegada', 100)->nullable();
          $table->string('lugar_llegada', 100)->nullable();
          $table->date('fregistro_dua_aduana')->nullable();
          $table->date('fembarque')->nullable();
          $table->date('farribo')->nullable();
          $table->boolean('bactivo')->default(1);
          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::dropIfExists('gen_dua');
    }
}
