<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
//use Illuminate\Foundation\Auth\User;

class Dudas extends Mailable
{
    use Queueable, SerializesModels;
    public $nombreape;
    public $correo;
    public $telefono;
    public $tipo;
    public $texto;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($nombreape,$correo,$telefono,$tipo,$texto)
    {
      $this->nombreape=$nombreape;
      $this->correo = $correo;
      $this->telefono = $telefono;
      $this->tipo=$tipo;
      $this->texto=$texto;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $dia=date("d.m.Y");
        $hora=date("H:i:s");
        return $this->view('emails.Dudas')
        ->subject("Sistema Integrado de Comercio Exterior - Enviado $dia - $hora");
        //->from('hiperiom412@gmail.com','RansesFrom')
    }
}
