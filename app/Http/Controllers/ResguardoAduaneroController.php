<?php

namespace App\Http\Controllers;
use Illuminate\Filesystem\Filesystem;
use App\Http\Requests\StoreResguardoRequest;
use App\Models\GenUsuario;
use App\Models\DetUsuario;
use App\Models\GenSolResguardoAduanero;
use App\Models\GenAduanaSalida;
use App\Models\GenDocResguardo;
use Illuminate\Support\Str;
use Session;
use Auth;
use Alert;
use File;
use Illuminate\Http\Request;


class ResguardoAduaneroController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $resguardoAduanero = GenSolResguardoAduanero::where('gen_usuario_id',Auth::user()->id)->get();
        
        return view('resguardoAduanero.index', compact('resguardoAduanero'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $detUsuario = DetUsuario::where('gen_usuario_id',Auth::user()->id)->where('bactivo', 1)->first();
        
        $AduanaSalida=GenAduanaSalida::where('bactivo',1)->whereIn('id', [1,4,7])->pluck('daduana','id');

        return view('resguardoAduanero.create', compact('detUsuario','AduanaSalida'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreResguardoRequest $request)
    {//dd($request->all());

        $genResguardo = new GenSolResguardoAduanero;
        $genResguardo->gen_usuario_id           = Auth::user()->id;
        $genResguardo->status_resguardo         =       9;
        $genResguardo->fstatus_resguardo        =       date('Y-m-d');
        $genResguardo->status_cna               =       9;
        $genResguardo->fstatus_cna              =       date('Y-m-d');
        $genResguardo->num_sol_resguardo        =       $this->generarRamdom();   
        $genResguardo->tlf_1                    =       $request->tlf_1;
        $genResguardo->tlf_2                    =       $request->tlf_2;
        $genResguardo->tlf_3                    =       $request->tlf_3;
        $genResguardo->ubicacion_georeferencial =       $request->ubicacion_georeferencial;
        $genResguardo->gen_aduana_salida_id =       $request->gen_aduana_salida_id;
        $genResguardo->save();

        //Guardar las imagenes
        $destino = 'img/resguardo/'.Auth::user()->detUsuario->cod_empresa;
        $destinoPrivado = public_path($destino);
        
        if (!file_exists($destinoPrivado)) {
            $filesystem = new Filesystem();
            $filesystem->makeDirectory($destinoPrivado, $mode = 0777, true, true); 
        }


        for( $i=1; $i <=24 ; $i++ ){

            $imagen = $request->file('file_'.$i);
            
            if (!empty($imagen)) {
                
               $nombre_imagen = $imagen->getClientOriginalName();
            
                $imagen->move($destinoPrivado, $nombre_imagen);
                $auxStr='file_'.$i;
                $$auxStr = $nombre_imagen;
               
            }
            

        }
        
            $docResguardo                                   = new GenDocResguardo;
            $docResguardo->gen_sol_resguardo_aduanero_id    = $genResguardo->id;
            $docResguardo->file_1               = $destino.'/'.$file_1;

            if (!empty($file_2)) {
                $docResguardo->file_2 = $destino.'/'.$file_2;
            }
            if (!empty($file_3)) {
                $docResguardo->file_3 = $destino.'/'.$file_3;
            }
            if (!empty($file_4)) {
                $docResguardo->file_4 = $destino.'/'.$file_4;
            }
            if (!empty($file_5)) {
                $docResguardo->file_5 = $destino.'/'.$file_5;
            }
            if (!empty($file_6)) {
                $docResguardo->file_6 = $destino.'/'.$file_6;
            }
            if (!empty($file_7)) {
                $docResguardo->file_7 = $destino.'/'.$file_7;
            }
            if (!empty($file_8)) {
                $docResguardo->file_8 = $destino.'/'.$file_8;
            }
            if (!empty($file_9)) {
                $docResguardo->file_9 = $destino.'/'.$file_9;
            }
            if (!empty($file_10)) {
                $docResguardo->file_10 = $destino.'/'.$file_10;
            }
            if (!empty($file_11)) {
                $docResguardo->file_11 = $destino.'/'.$file_11;
            }
            if (!empty($file_12)) {
                $docResguardo->file_12 = $destino.'/'.$file_12;
            }
            if (!empty($file_13)) {
                $docResguardo->file_13 = $destino.'/'.$file_13;
            }
            if (!empty($file_14)) {
                $docResguardo->file_14 = $destino.'/'.$file_14;
            }
            if (!empty($file_15)) {
                $docResguardo->file_15 = $destino.'/'.$file_15;
            }
            if (!empty($file_16)) {
                $docResguardo->file_16 = $destino.'/'.$file_16;
            }
            if (!empty($file_17)) {
                $docResguardo->file_17 = $destino.'/'.$file_17;
            }
            if (!empty($file_18)) {
                $docResguardo->file_18 = $destino.'/'.$file_18;
            }
            if (!empty($file_19)) {
                $docResguardo->file_19 = $destino.'/'.$file_19;
            }
            if (!empty($file_20)) {
                $docResguardo->file_20 = $destino.'/'.$file_20;
            }
            if (!empty($file_21)) {
                $docResguardo->file_21 = $destino.'/'.$file_21;
            }
            if (!empty($file_22)) {
                $docResguardo->file_22 = $destino.'/'.$file_22;
            }
            if (!empty($file_23)) {
                $docResguardo->file_23 = $destino.'/'.$file_23;
            }
            if (!empty($file_24)) {
                $docResguardo->file_24 = $destino.'/'.$file_24;
            }

            
            $docResguardo->bactivo=1;

            $docResguardo->save();
            Alert::success('REGISTRO EXITOSO','Resguardo Agregado')->persistent("Ok");
            return redirect()->action('ResguardoAduaneroController@index');

        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\GenUsuario  $genUsuario
     * @return \Illuminate\Http\Response
     */
    public function show(GenUsuario $genUsuario)
    {
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\GenUsuario  $genUsuario
     * @return \Illuminate\Http\Response
     */
    public function edit(GenUsuario $genUsuario, $id)
    {
         $resguardo= GenSolResguardoAduanero::find($id);
         $documentos=GenDocResguardo::where('gen_sol_resguardo_aduanero_id',$id)->first();

         $AduanaSalida=GenAduanaSalida::where('bactivo',1)->whereIn('id', [1,4,7])->pluck('daduana','id');

         return view('resguardoAduanero.edit', compact('resguardo','documentos','AduanaSalida'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\GenUsuario  $genUsuario
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, GenUsuario $genUsuario, $id)
    {//dd($request->all());

        $genResguardo = GenSolResguardoAduanero::find($id);
        $genResguardo->gen_usuario_id           = Auth::user()->id;
        $genResguardo->status_resguardo         =       18;
        $genResguardo->status_cna               =       18; 
        $genResguardo->tlf_1                    =       $request->tlf_1;
        $genResguardo->tlf_2                    =       $request->tlf_2;
        $genResguardo->tlf_3                    =       $request->tlf_3;
        $genResguardo->ubicacion_georeferencial =       $request->ubicacion_georeferencial;
        $genResguardo->gen_aduana_salida_id =       $request->gen_aduana_salida_id;
        $genResguardo->save();


        //Guardar las imagenes
        $destino = 'img/resguardo/'.Auth::user()->detUsuario->cod_empresa;
        $destinoPrivado = public_path($destino);
        
        if (!file_exists($destinoPrivado)) {
            $filesystem = new Filesystem();
$filesystem->makeDirectory($destinoPrivado, $mode = 0777, true, true); 
        }

        
        for( $i=1; $i <=24 ; $i++ ){

            $imagen = $request->file('file_'.$i);
            
            if (!empty($imagen)) {
                
                $nombre_imagen = $imagen->getClientOriginalName();
                
                $imagen->move($destinoPrivado, $nombre_imagen);
                $auxStr='file_'.$i;
                $$auxStr = $nombre_imagen;
               
            }
            

        }
          
        
        $doCertif                       =  GenDocResguardo::find($request->file_id);
        //dd($doCertif);
        if(!empty($file_1)){
            $auxFile1= $doCertif->file_1;
            $doCertif->file_1 = $destino.'/'.$file_1;
        }
        if(!empty($file_2)){
            $auxFile2= $doCertif->file_2;
            $doCertif->file_2 = $destino.'/'.$file_2;
        }
        if(!empty($file_3)){
            $auxFile3= $doCertif->file_3;
            $doCertif->file_3 = $destino.'/'.$file_3;
        }
        if(!empty($file_4)){
            $auxFile4= $doCertif->file_4;
            $doCertif->file_4 = $destino.'/'.$file_4;
        }
        if(!empty($file_5)){
            $auxFile5= $doCertif->file_5;
            $doCertif->file_5 = $destino.'/'.$file_5;
        }
        if(!empty($file_6)){
            $auxFile6= $doCertif->file_6;
            $doCertif->file_6 = $destino.'/'.$file_6;
        }
        if (!empty($file_7)) {
            $auxFile7= $doCertif->file_7;
            $doCertif->file_7 = $destino.'/'.$file_7;
        }
        if (!empty($file_8)) {
            $auxFile8= $doCertif->file_8;
            $doCertif->file_8 = $destino.'/'.$file_8;
        }
        if (!empty($file_9)) {
            $auxFile9= $doCertif->file_9;
            $doCertif->file_9 = $destino.'/'.$file_9;
        }
        if (!empty($file_10)) {
            $auxFile10= $doCertif->file_10;
            $doCertif->file_10 = $destino.'/'.$file_10;
        }
        if(!empty($file_11)){
            $auxFile11= $doCertif->file_11;
            $doCertif->file_11 = $destino.'/'.$file_11;
        }
        if(!empty($file_12)){
            $auxFile12= $doCertif->file_12;
            $doCertif->file_12 = $destino.'/'.$file_12;
        }
        if(!empty($file_13)){
            $auxFile13= $doCertif->file_13;
            $doCertif->file_13 = $destino.'/'.$file_13;
        }
        if(!empty($file_14)){
            $auxFile14= $doCertif->file_14;
            $doCertif->file_14 = $destino.'/'.$file_14;
        }
        if(!empty($file_15)){
            $auxFile15= $doCertif->file_15;
            $doCertif->file_15 = $destino.'/'.$file_15;
        }
        if(!empty($file_16)){
            $auxFile16= $doCertif->file_16;
            $doCertif->file_16 = $destino.'/'.$file_16;
        }
        if (!empty($file_17)) {
           $auxFile17= $doCertif->file_17;
            $doCertif->file_17 = $destino.'/'.$file_17;
        }
        if (!empty($file_18)) {
            $auxFile18= $doCertif->file_18;
                $doCertif->file_18 = $destino.'/'.$file_18;
        }
        if (!empty($file_19)) {
            $auxFile19= $doCertif->file_19;
                $doCertif->file_19 = $destino.'/'.$file_19;
        }
        if (!empty($file_20)) {
            $auxFile20= $doCertif->file_20;
                $doCertif->file_20 = $destino.'/'.$file_20;
        }
        if (!empty($file_21)) {
            $auxFile21= $doCertif->file_21;
            $doCertif->file_21 = $destino.'/'.$file_21;
        }
        if (!empty($file_22)) {
            $auxFile22= $doCertif->file_22;
            $doCertif->file_22 = $destino.'/'.$file_22;
        }
        if (!empty($file_23)) {
            $auxFile23= $doCertif->file_23;
            $doCertif->file_23 = $destino.'/'.$file_23;
        }
        if (!empty($file_24)) {
            $auxFile24= $doCertif->file_24;
            $doCertif->file_24 = $destino.'/'.$file_24;
        }
           
          
            
        if($doCertif->touch()){
            if(!empty($auxFile1)){
                if(is_file('.'.$auxFile1))
                unlink('.'.$auxFile1); //elimino el fichero
            }
            if(!empty($auxFile2)){
                if(is_file('.'.$auxFile2))
                unlink('.'.$auxFile2); //elimino el fichero
            }
            if(!empty($auxFile3)){
                if(is_file('.'.$auxFile3))
                unlink('.'.$auxFile3); //elimino el fichero
            }
            if(!empty($auxFile4)){
                if(is_file('.'.$auxFile4))
                unlink('.'.$auxFile4); //elimino el fichero
            }
            if(!empty($auxFile5)){
                if(is_file('.'.$auxFile5))
                unlink('.'.$auxFile5); //elimino el fichero
            }
            if(!empty($auxFile6)){
                if(is_file('.'.$auxFile6))
                unlink('.'.$auxFile6); //elimino el fichero
            }
            if(!empty($auxFile7)){
                if(is_file('.'.$auxFile7))
                unlink('.'.$auxFile7); //elimino el fichero
            }
            if(!empty($auxFile8)){
                if(is_file('.'.$auxFile8))
                unlink('.'.$auxFile8); //elimino el fichero
            }
            if(!empty($auxFile9)){
                if(is_file('.'.$auxFile9))
                unlink('.'.$auxFile9); //elimino el fichero
            }
            if(!empty($auxFile10)){
                if(is_file('.'.$auxFile10))
                unlink('.'.$auxFile10); //elimino el fichero
            }
            if(!empty($auxFile11)){
                if(is_file('.'.$auxFile11))
                unlink('.'.$auxFile11); //elimino el fichero
            }
            if(!empty($auxFile12)){
                if(is_file('.'.$auxFile12))
                unlink('.'.$auxFile12); //elimino el fichero
            }
            if(!empty($auxFile13)){
                if(is_file('.'.$auxFile13))
                unlink('.'.$auxFile13); //elimino el fichero
            }
            if(!empty($auxFile14)){
                if(is_file('.'.$auxFile14))
                unlink('.'.$auxFile14); //elimino el fichero
            }
            if(!empty($auxFile15)){
                if(is_file('.'.$auxFile15))
                unlink('.'.$auxFile15); //elimino el fichero
            }
            if(!empty($auxFile16)){
                if(is_file('.'.$auxFile16))
                unlink('.'.$auxFile16); //elimino el fichero
            }
            if(!empty($auxFile17)){
            if(is_file('.'.$auxFile17))
            unlink('.'.$auxFile17); //elimino el fichero
            }
            if(!empty($auxFile18)){
                if(is_file('.'.$auxFile18))
                unlink('.'.$auxFile18); //elimino el fichero
            }
            if(!empty($auxFile19)){
                if(is_file('.'.$auxFile19))
                unlink('.'.$auxFile19); //elimino el fichero
            }
            if(!empty($auxFile20)){
                if(is_file('.'.$auxFile20))
                unlink('.'.$auxFile20); //elimino el fichero
            }
            if(!empty($auxFile21)){
                if(is_file('.'.$auxFile21))
                unlink('.'.$auxFile21); //elimino el fichero
            }
            if(!empty($auxFile22)){
                if(is_file('.'.$auxFile22))
                unlink('.'.$auxFile22); //elimino el fichero
            }
            if(!empty($auxFile23)){
                if(is_file('.'.$auxFile23))
                unlink('.'.$auxFile23); //elimino el fichero
            }
            if(!empty($auxFile24)){
                if(is_file('.'.$auxFile24))
                unlink('.'.$auxFile24); //elimino el fichero
            }
    
        }else{
            if (!empty($file_1)) {
                if(is_file('.'.$destino.'/'.$file_1))
                unlink('.'.$destino.'/'.$file_1);
            }
            
            if (!empty($file_2)) {
                if(is_file('.'.$destino.'/'.$file_2))
                unlink('.'.$destino.'/'.$file_2);
            }
            
            if (!empty($file_3)) {
                if(is_file('.'.$destino.'/'.$file_3))
                unlink('.'.$destino.'/'.$file_3);
            }
    
            if (!empty($file_4)) {
                if(is_file('.'.$destino.'/'.$file_4))
                unlink('.'.$destino.'/'.$file_4);
            }
    
            if (!empty($file_5)) {
                if(is_file('.'.$destino.'/'.$file_5))
                unlink('.'.$destino.'/'.$file_5);
            }
    
            if (!empty($file_6)) {
                if(is_file('.'.$destino.'/'.$file_6))
                unlink('.'.$destino.'/'.$file_6);
            }
            if (!empty($file_7)) {
                if(is_file('.'.$destino.'/'.$file_7))
                unlink('.'.$destino.'/'.$file_7);
            }
            if (!empty($file_8)) {
                if(is_file('.'.$destino.'/'.$file_8))
                unlink('.'.$destino.'/'.$file_8);
            }
            if (!empty($file_9)) {
                if(is_file('.'.$destino.'/'.$file_9))
                unlink('.'.$destino.'/'.$file_9);
            }
            if (!empty($file_10)) {
                if(is_file('.'.$destino.'/'.$file_10))
                unlink('.'.$destino.'/'.$file_10);
            }

            if (!empty($file_11)) {
            if(is_file('.'.$destino.'/'.$file_11))
            unlink('.'.$destino.'/'.$file_11);
            }
            
            if (!empty($file_12)) {
                if(is_file('.'.$destino.'/'.$file_12))
                unlink('.'.$destino.'/'.$file_12);
            }
            
            if (!empty($file_13)) {
                if(is_file('.'.$destino.'/'.$file_13))
                unlink('.'.$destino.'/'.$file_13);
            }
    
            if (!empty($file_14)) {
                if(is_file('.'.$destino.'/'.$file_14))
                unlink('.'.$destino.'/'.$file_14);
            }
    
            if (!empty($file_15)) {
                if(is_file('.'.$destino.'/'.$file_15))
                unlink('.'.$destino.'/'.$file_15);
            }
    
            if (!empty($file_16)) {
                if(is_file('.'.$destino.'/'.$file_16))
                unlink('.'.$destino.'/'.$file_16);
            }
            if (!empty($file_17)) {
                if(is_file('.'.$destino.'/'.$file_17))
                unlink('.'.$destino.'/'.$file_17);
            }
            if (!empty($file_18)) {
                if(is_file('.'.$destino.'/'.$file_18))
                unlink('.'.$destino.'/'.$file_18);
            }
            if (!empty($file_19)) {
                if(is_file('.'.$destino.'/'.$file_19))
                unlink('.'.$destino.'/'.$file_19);
            }
            if (!empty($file_20)) {
                if(is_file('.'.$destino.'/'.$file_20))
                unlink('.'.$destino.'/'.$file_20);
            }
            if (!empty($file_21)) {
                if(is_file('.'.$destino.'/'.$file_21))
                unlink('.'.$destino.'/'.$file_21);
            }
            if (!empty($file_22)) {
                if(is_file('.'.$destino.'/'.$file_22))
                unlink('.'.$destino.'/'.$file_22);
            }
            if (!empty($file_23)) {
                if(is_file('.'.$destino.'/'.$file_23))
                unlink('.'.$destino.'/'.$file_23);
            }
            if (!empty($file_24)) {
                if(is_file('.'.$destino.'/'.$file_24))
                unlink('.'.$destino.'/'.$file_24);
            }
                
    
            Alert::warning('No es posible realizar su solicitud. Intente nuevamente.','Acción Denegada!')->persistent("OK");
            return redirect()->back()->withInput();
        }
        
        Alert::success('ACTUALIZACIÓN EXITOSA','Registro Actualizado')->persistent("Ok");
        return redirect()->action('ResguardoAduaneroController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\GenUsuario  $genUsuario
     * @return \Illuminate\Http\Response
     */
    public function destroy(GenUsuario $genUsuario)
    {
        //
    }

    private function generarRamdom()
    {
        $user = Auth::user();
        $random = Str::random(4);
        $id  = $user->id;
        $rif = substr($user->detUsuario->rif, -2);
        $fecha = date('Ymd');
        return $id.'-'.$random.$fecha.$rif;
    }
}
