@extends('layouts.app')

@section('content')

@component('modal_dialogo',['arancel'=>$arancel])

@slot('header') <span>Código Arancelario</span> @endslot

@slot('body')  <div id="arancel"></div>

@endslot

@slot('footer')@endslot

@endcomponent
<div class="container">


 {{Form::open(['method'=>'POST','id'=>'inscripcion', 'enctype' => 'multipart/form-data'])}}

 <!--***************Parte 1 Registro****************************-->
      <div id="datos_persona1">
        <ol class="breadcrumb">
          <li class="active">Paso 1</li>
        </ol>
        <div class="panel panel-primary">
          <div class="panel-heading">DATOS BÁSICOS</div>
          <div class="panel-body">
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  {!!Form::label('','Nombre o Razón Social')!!}
                  {!!Form::text('razon_social',(!is_null($datos))?$datos->DNOMBRE:old('razon_social'),['class'=>'form-control','id'=>'razon_social','onkeypress="return soloNumerosyLetras(event)"'])!!}
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label>Siglas</label><br>
                  {!!Form::text('siglas',(!is_null($datos))?$datos->DSIGLAS:old('siglas'),['class'=>'form-control','id'=>'siglas','onkeypress="return soloLetras(event)"','maxlength'=>'50'])!!}
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label>Rif</label><br>
                  @if(!is_null($datos))
                 <input type="text" name="rif" value="{{$datos->CRIF}}" readonly id="rif" class="form-control" />
                 @else
                   <input type="text" name="rif" value="{{$rif}}" readonly id="rif" class="form-control" />
                 @endif
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label>Página Web</label><br>
                  {!! Form::text('pagina_web',null, ['class'=>'form-control','id'=>'pagina_web','onkeypress'=>'return soloNumerosyLetras(event)','maxlength'=>'60','value'=>'', 'noreq'=>'noreq']) !!}
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>Tipo de Empresa</label><br>
                  {!!Form::select('id_tipo_empresa',$tipos_empresas,null,['class'=>'form-control','id'=>'id_tipo_empresa','placeholder'=>'--Seleccione una Opción--'])!!}
                </div>
              </div>
            </div>
              <div class="panel panel-primary">
                <div class="panel-heading">TIPO DE ACTIVIDAD ECONÓMICA</div>
              </div>
              <div class="row">
                <div class="col-md-3">
                  <label>Exportador</label>
                  {!!Form::checkbox('export','export',null,['class'=>'checkbox-inline','id'=>'export'])!!}
               </div>
               <div class="col-md-3">
                <label>Inversionista</label>
                {!!Form::checkbox('invers','Inversionista',null,['class'=>'checkbox-inline','id'=>'invers'])!!}
              </div>
              <div class="col-md-3">
                <label>Productor</label>
                {!!Form::checkbox('produc','Productor',null,['class'=>'checkbox-inline','id'=>'produc'])!!}
              </div>
              <div class="col-md-3">
                <label>Comercializadora</label>
                {!!Form::checkbox('comerc','Comercializadora',null,['class'=>'checkbox-inline','id'=>'comerc'])!!}
              </div>
            </div><br>
            <br>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label>Sector</label><br>
                  {!!Form::select('id_gen_sector',$sectores,null,
                  ['class'=>'form-control','id'=>'id_gen_sector','placeholder'=>'--Seleccione una Opción--'])!!}
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>Actividad Económica</label><br>
                  {!!Form::select('id_gen_actividad_eco',$actividad_eco,null,['class'=>'form-control','id'=>'id_gen_actividad_eco','placeholder'=>'--Seleccione una Opción--'])!!}
                </div>
              </div>
            </div>
            <br>
            <div class="panel panel-primary">
              <div class="panel-heading">DOMICILIO FISCAL</div>
            </div>
              <div class="row">
                <div class="col-md-4">
                  <div class="form-group">
                    <label>Estado</label><br>
                     {!!Form::select('id_estado',$estados,null,
              ['class'=>'form-control','id'=>'id_estado','placeholder'=>'--Seleccione una Opción--'])!!}
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label>Municipio</label><br>
                    {!!Form::select('id_municipio',$municipios,null,['class'=>'form-control','id'=>'id_municipio'])!!}
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label>Parroquia</label><br>
                    {!!Form::select('id_parroquia',$parroquias,null,['class'=>'form-control','id'=>'id_parroquia'])!!}
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Dirección</label><br>
                    {!!Form::text('direccion',(!is_null($datos))?$datos->DDIRECCION:old('direccion'),['class'=>'form-control','id'=>'direccion','onkeypress="return soloNumerosyLetras(event)"','maxlength'=>'200'])!!}
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group">
                    <label>Teléfono Local</label><br>

                    {!!Form::text('telefono_local',(!is_null($datos))?$datos->CTELEFONO:old('telefono_local'),['class'=>'form-control','id'=>'telefono_local','onkeypress="return soloNumeros(event)"','maxlength'=>'11'])!!}
                    <small>Ejemplo:02125554555</small>
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group">
                    <label>Teléfono Celular</label><br>
                     {!!Form::text('telefono_movil',(!is_null($datos))?$datos->CCELULAR:old('telefono_movil'),['class'=>'form-control','id'=>'telefono_movil','onkeypress="return soloNumeros(event)"','maxlength'=>'11', 'noreq'=>'noreq'])!!}
                    <small>Ejemplo:04165554555</small>
                  </div>
                </div>
              </div>
              <div class="row">
              <div class="col-md-12">
                <div class="col-md-4"></div>
                <div class="col-md-4 text-center">
                  <div class="form-group">

                    <a class="btn btn-primary" href="#" id="seguir1">Siguiente</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
       </div>

  <!--***************Parte 2 Registro****************************-->
        <div id="datos_persona2" style="display: none;">
            <ol class="breadcrumb">
              <li><a href="#" id="paso1">Paso 1</a></li>
              <li class="active" id="paso2">Paso 2</li>
            </ol>
          <div class="panel panel-primary">
              <div class="panel-heading">DATOS DE LA PERSONA JURIDICA O FIRMA PERSONAL</div>
            <div class="panel-body">
            <div class="row">
              <div class="col-md-4">
                <div class="form-group">
                  <label>Registro de Circunscripción Judicial</label>
                {!!Form::select('id_circuns_judicial',$circuns_judiciales,null,
                   ['class'=>'form-control','id'=>'id_circuns_judicial','placeholder'=>'--Seleccione una Opción--'])!!}
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label>Número de Documento</label><br>
                    {!!Form::text('num_registro',(!is_null($datos))?$datos->NREGISTRO_MER:old('num_registro'),['class'=>'form-control','id'=>'num_registro','onkeypress="return soloNumerosyLetras(event)"','maxlength'=>'60'])!!}
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label>Número de Tomo</label><br>
                    {!!Form::text('tomo',(!is_null($datos))?$datos->CTOMO_MER:old('tomo'),['class'=>'form-control','id'=>'tomo','onkeypress="return soloNumerosyLetras(event)"','maxlength'=>'30'])!!}
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-4">
                <div class="form-group">
                  <label>Numero de Folio</label><br>
                    {!!Form::text('folio',(!is_null($datos))?$datos->CFOLIO_MER:old('folio'),['class'=>'form-control','id'=>'folio','onkeypress="return soloNumerosyLetras(event)"','maxlength'=>'30'])!!}
                </div>
              </div>
              <div class="col-md-4">
                  <div class="form-group">
                    <label>Oficina</label><br>
                      {!!Form::text('Oficina',null,['class'=>'form-control','id'=>'Oficina','onkeypress="return soloNumeros(event)"','maxlength'=>'60'])!!}
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label>Fecha de Constitución</label>
                  <div class="input-group date">
                        {!!Form::text('f_registro_mer',null,['class'=>'form-control','id'=>'f_registro_mer','onchange="validarfechaRM(this.value,"f_registro_mer");"','maxlength'=>'200'])!!}
                     {{--<input id="f_registro_mer" name="f_registro_mer" type="text" class="form-control" onchange="validarfechaRM(this.value,'f_registro_mer');" readonly="readonly">--}}
                     <span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
                 </div>
                </div>
              </div>
            </div>
            <div class="panel panel-primary">
              <div class="panel-heading">DATOS DEL REPRESENTANTE LEGAL</div>
            </div>
            <div class="row">
              <div class="col-md-4">
                <div class="form-group">
                  <label>Nombre</label><br>
                  {!!Form::text('nombre_repre',null,['class'=>'form-control','id'=>'nombre_repre','onkeypress="return soloLetras(event)"','maxlength'=>'50'])!!}
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label>Apellido</label><br>
                  {!!Form::text('apellido_repre',null,['class'=>'form-control','id'=>'apellido_repre','onkeypress="return soloLetras(event)"','maxlength'=>'50'])!!}
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                <label>Cédula</label><br>
               @if(!is_null($datos))
               <input class="form-control" type="text" id="ci_repre" name="ci_repre" value="{{$datos->CCEDULA_REP}}" onkeypress="return CedulaFormatrep(this,'Cedula de Indentidad Invalida',-1,true,event)" maxlength="10">
               @else
                <input class="form-control" type="text" id="ci_repre" name="ci_repre" value="{{old('ci_repre')}}" onkeypress="return CedulaFormatrep(this,'Cedula de Indentidad Invalida',-1,true,event)" maxlength="10">
               @endif
                <small>Ejemplo:V-00000000</small>
                </div>
              </div>
              </div>
              <div class="row">
              <div class="col-md-5">
                <div class="form-group">
                  <label>Cargo</label><br>
                    {!!Form::text('cargo_repre',(!is_null($datos))?$datos->DCARGO_REP:old('cargo_repre'),['class'=>'form-control','id'=>'cargo_repre','onkeypress="return soloLetras(event)"','maxlength'=>'80'])!!}
                  {{--<input class="form-control" type="text" id="cargo_repre" name="cargo_repre" value="{{ $datos->DCARGO_REP}}" onkeypress="return soloLetras(event)" maxlength="80">--}}
                </div>
              </div>

              <div class="col-md-7">
                <div class="form-group">
                  {!! Form::label('','Correo Electrónico')!!}
                  {!! Form::text('correo_repre',null, ['class'=>'form-control','id'=>'correo_repre','onkeyup'=>"minuscula(this.value,'#email');",'maxlength'=>'80']) !!}
                </div>
              </div>
            </div><br>
            <br>
          </div>
        </div>
      </div>
      <div id="datos_accionistas" style="display: none;">
            <div class="panel panel-primary">
              <div class="panel-heading">DATOS DE LOS ACCIONISTAS</div>
            </div>
            <div class="row">
              <div class="col-md-3"></div>
              <div class="col-md-6 text-center">
                <div class="form-group">
                  {!! Form::label('','¿Posée Accionista?')!!}
                  {!! Form::label('','Si')!!}
                  {!! Form::radio('accionista','Si',false,['class'=>'radio-inline','id'=>'accion_si'])!!}
                  {!! Form::label('','No')!!}
                  {!! Form::radio('accionista','No',true,['class'=>'radio-inline','id'=>'accion_no'])!!}
                </div>
              </div>
              <div class="col-md-3"></div>
            </div>
            <table class="table table-bordered text-center" id="fields" style="display:none;">
                    <tr>
                      <th>Nombre</th>
                      <th>Apellido</th>
                      <th>Rif</th>
                      <th>Cargo</th>
                      <th>Teléfono</th>
                      <th>Nacionalidad</th>
                      <th>% Participación</th>
                      <th>Correo Electrónico</th>
                      <th></th>
                    </tr>
                    
                          <tr id="row" align="center">

                            <td>{!!Form::text('nombre_accionista[]',null,['class'=>'form-control','id'=>'nombre_accionista','onkeypress'=>'return soloLetras(event)','maxlength'=>'50'])!!}</td>

                            <td>{!!Form::text('apellido_accionista[]',null,['class'=>'form-control','id'=>'apellido_accionista','onkeypress'=>'return soloLetras(event)','maxlength'=>'50'])!!}</td>
                             
                            <td align="text-justify">{!!Form::text('cedula_accionista[]',null,['class'=>'form-control','id'=>'cedula_accionista','onkeypress'=>'return CedulaFormat1(this,"Cedula de Indentidad Invalida",-1,true,event)','maxlength'=>'15','pattern'=>'^([VEJPG]{1})([-]{1})([0-11]{9})$'])!!}<small>Ejemplo: V-00000000, J-00000000</small></td>

                           <td>{!!Form::text('cargo_accionista[]',null,['class'=>'form-control','id'=>'cargo_accionista','onkeypress'=>'return soloLetras(event)','maxlength'=>'80'])!!}</td>

                           <td>{!!Form::text('telefono_accionista[]',null,['class'=>'form-control','id'=>'telefono_accionista','onkeypress'=>'return soloNumeros(event)','maxlength'=>'11'])!!}<small>Ejemplo:02125554555</small></td>

                           <td>{!!Form::text('nacionalidad_accionista[]',null,['class'=>'form-control','id'=>'nacionalidad_accionista','onkeypress'=>'return soloLetras(event)','maxlength'=>'50'])!!}</td>

                           <td>{!!Form::text('participacion_accionista[]',null,['class'=>'form-control','id'=>'participacion_accionista','onkeypress'=>'return soloNumerosletrasyPorcentaje(event)','maxlength'=>'50'])!!}</td>

                           <td>{!!Form::text('correo_accionista[]',null,['class'=>'form-control','id'=>'correo_accionista','onkeyup'=>"minuscula(this.value,'#correo_accionista');",'maxlength'=>'80'])!!}</td>
                           
                           <td><button  name="add" type="button" id="add" value="add more" class="btn btn-success">Agregar</button></td>
                      </tr> 
            </table>
            <div class="row">
              <div class="col-md-12">
                <div class="col-md-4"></div>
                <div class="col-md-4 text-center">
                  <div class="form-group">
                    <a class="btn btn-primary" href="#" id="atras1">Atras</a>
                    <a class="btn btn-primary" href="#" id="seguir2">Siguiente</a>
                  </div>
                </div>
              </div>
            </div>
        </div>

  <!--***************Paso 3 ****************************--> 
   <div id="datos_persona3" style="display: none;">
            <ol class="breadcrumb">
              <li><a href="#" id="paso11">Paso 1</a></li>
              <li><a href="#" id="paso22">Paso 2</a></li>
              <li class="active" id="paso3">Paso 3</li>
            </ol>
    <div class="panel panel-primary">
    <div class="panel-heading">CASA MATRIZ</div>

    <div class="panel-body">
      <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6 text-center">
          <div class="form-group">
            {!! Form::label('','¿Posée Casa Matriz?')!!}
            {!! Form::label('','Si',['class'=>'radio-inline'])!!}
            {!! Form::radio('casa_matriz','1',false,['class'=>'radio-inline','id'=>'casa_matriz_si'])!!}
            {!! Form::label('','No',['class'=>'radio-inline'])!!}
            {!! Form::radio('casa_matriz','2',true,['class'=>'radio-inline','id'=>'casa_matriz_no'])!!}
          </div>
        </div>
        <div class="col-md-3"></div>
      </div>
      <div class="row" id="pais_origen" style="display:none;">
        <div class="col-md-3"></div>
        <div class="col-md-6">
          <div class="form-group"><!--Cambiar el id de este campo-->
            {!! Form::label('','País de Origen')!!}
            {!!Form::select('pais_id',$paises,null,['class'=>'form-control','placeholder'=>'Seleccione un País','id'=>'pais_id', 'noreq'=>'noreq']) !!}
          </div>
        </div>
        <div class="col-md-3"></div>
      </div>
    <div class="panel panel-primary">
      <div class="panel-heading">ÚLTIMA EXPORTACIÓN</div>
    </div>
    <div class="row">
      <div class="col-md-6">
        <div class="form-group">
          {!! Form::label('','País')!!}<!--Pais de la ultima exportacion-->
          {!!Form::select('pais_ult_export',$paises,null,['class'=>'form-control','placeholder'=>'Seleccione un País de la Última Exportacion','id'=>'pais_ult_export', 'noreq'=>'noreq']) !!}
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group">
          {!! Form::label('','Fecha de Embarque de Última Exportación')!!}
          <div class="input-group date">
            {!! Form::text('fecha_ult_export',null, ['class'=>'form-control','id'=>'fecha_ult_export','onchange'=>'return validarfechaRM(this.value,"fecha_ult_export")','maxlength'=>'','readonly'=>'readonly', 'noreq'=>'noreq']) !!}
            <span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
          </div> 
        </div>
      </div>
    </div>
    <div class="panel panel-primary">
      <div class="panel-heading">SOPORTE</div>
    </div>
    <div class="row">
      <div class="col-md-4">
         <div class="form-group">
         {!! Form::label('','CI')!!}
          {!!Form::file('ruta_doc',null,['class'=>'form-control','id'=>'ruta_doc']) !!}
        </div>
      </div>
      <div class="col-md-4">
         <div class="form-group">
         {!! Form::label('','RIF')!!}
          {!!Form::file('ruta_rif',null,['class'=>'form-control','id'=>'ruta_rif']) !!}
        </div>
      </div>
      <div class="col-md-">
        {!! Form::label('','REGISTRO MERCANTIL')!!}
          {!!Form::file('ruta_reg_merc',null,['class'=>'form-control','id'=>'ruta_reg_merc']) !!}
      </div>
    </div>
    
    <div class="row">
      <div class="col-md-12">
        <div class="col-md-4"></div>
        <div class="col-md-4 text-center">
          <div class="form-group">
            <a class="btn btn-primary" href="#" id="atras2">Atrás</a>
            <a class="btn btn-primary" href="#" id="seguir3">Siguiente</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>

<!--*********************************Parte 4 Registro*****************************************************************-->
          <div id="datos_persona4" style="display: none;">
            <ol class="breadcrumb">
              <li><a href="#" id="paso111">Paso 1</a></li>
              <li><a href="#" id="paso222">Paso 2</a></li>
              <li><a href="#" id="paso333">Paso 3</a></li>
              <li class="active" id="paso4">Paso 4</li>
            </ol>
            <div class="panel panel-primary">
              <div class="panel-heading">DATOS DE LOS PRODUCTOS QUE PRODUCE Y/O COMERCIALIZA</div>
             <div class="panel-body">
                <div id="radios_export">
                <div class="row">
                   <div class="col-md-1"></div>
                   <div class="col-md-10 text-center">
                      {!! Form::label('','¿Exporta Productos de Servicios y/o Tecnología?',['class'=>'form-control'])!!}
                      {!! Form::label('','Si',['class'=>'radio-inline'])!!}
                      {!! Form::radio('produc_tecno','1',false,['class'=>'radio-inline','id'=>'prod_tecnologia_si'])!!}
                      {!! Form::label('','No',['class'=>'radio-inline'])!!}
                      {!! Form::radio('produc_tecno','2',false,['class'=>'radio-inline','id'=>'prod_tecnologia_no'])!!}

                    </div>
                  <div class="col-md-1"></div>
                </div>
                </div><br>
                <div class="row" id="tecno_especifique" style="display: none;">
                  <div class="col-md-1"></div>
                  <div class="col-md-10">
                    {!! Form::label('','Especifique')!!}
                    {!!Form::text('especifique_tecno',null,['class'=>'form-control','id'=>'especifique_tecno','onkeypress'=>'return soloLetras(event)','maxlength'=>'100'])!!}
                  </div>
                  <div class="col-md-1"></div>
                  <br>
                </div>

               <div class="row" id="codigo_arancelarios" style="display: none;">
                   <div class="col-md-3"></div>
                    <div class="col-md-6 text-center">
                     {{-- {!!Form::radio('tipo_arancel','1',false,['class'=>'radio-inline','id'=>'cod_andina'])!!}
                      {!! Form::label('','Nandina',['class'=>'radio-inline'])!!}--}}
                      {!! Form::radio('tipo_arancel','2',false,['class'=>'radio-inline','id'=>'cod_mercosur'])!!}
                      {!! Form::label('','Mercosur (NCM)',['class'=>'radio-inline'])!!}
                    </div>
                  <div class="col-md-3"></div>
                <br>
                <table class="table table-bordered text-center" id="fields2" style="display: none;">

                  <tr>
                    <th>Código Arancelario </th>
                    <th>Descripción Arancelaria</th>
                    <th>Descripción Comercial</th>
                    <th>Unidad de Medida</th>
                    <th>Capacidad Operativa Anual</th>
                    <th>Capacidad Instalada Anual</th>
                    <th>Capacidad de Almacenamiento Anual</th>
                    <th>Capacidad de Exportación Anual</th>
                    <th></th>

                  </tr>
                  <tr id="fila">

                    <td>{!!Form::text('codigo[]',null,['class'=>'form-control','data-toggle'=>"modal",'data-target'=>"#modal",'id'=>'codigo','readonly'=>'true'])!!}
                    </td>
                    <td>{!!Form::text('descripcion[]',null,['class'=>'form-control','id'=>'descripcion','onkeypress'=>'return soloLetras(event)'])!!}</td>
                    <td>{!!Form::text('descrip_comercial[]',null,['class'=>'form-control','id'=>'descrip_comercial','onkeypress'=>'return soloLetras(event)'])!!}</td>
                    <td>{!!Form::select('gen_unidad_medida_id[]',$unidad_medida,null,['placeholder'=>'--Seleccione una Unidad de Medida--', 'class'=>'form-control','id'=>'gen_unidad_medida_id']) !!}</td>
                    <td>{!!Form::text('cap_opera_anual[]',null,['class'=>'form-control','id'=>'cap_opera_anual','onkeypress'=>'return soloNumeros(event)'])!!}</td>
                    <td>{!!Form::text('cap_insta_anual[]',null,['class'=>'form-control','id'=>'cap_insta_anual','onkeypress'=>'return soloNumerosletrasyPorcentaje(event)'])!!}</td>
                    <td>{!!Form::text('cap_alamcenamiento[]',null,['class'=>'form-control','id'=>'cap_alamcenamiento','onkeypress'=>'return soloNumeros(event)'])!!}</td>
                    <td>{!!Form::text('cap_exportacion[]',null,['class'=>'form-control','id'=>'cap_exportacion','onkeypress'=>'return soloNumeros(event)'])!!}</td>

                    <td><button  name="add2" type="button" id="add2" value="add more" class="btn btn-success">Agregar</button></td>
                  </tr>

                </table>
              </div>
                <div class="row">
                  <div class="col-md-12">
                    <div class="col-md-4"></div>
                    <div class="col-md-4 text-center">
                      <div class="form-group">
                        <a class="btn btn-primary" href="#" id="atras3">Atras</a>
                        <a class="btn btn-primary" href="#" id="seguir4">Siguiente</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div><!--cierre de panel body-->
            </div>
          </div>




<!--***************Parte 5 Registro****************************-->
          <div id="datos_persona5" style="display: none;">
            <ol class="breadcrumb">
              <li><a href="#" id="paso1111">Paso 1</a></li>
              <li><a href="#" id="paso2222">Paso 2</a></li>
              <li><a href="#" id="paso3333">Paso 3</a></li>
              <li><a href="#" id="paso4444">Paso 4</a></li>
              <li class="active" id="paso5">Paso 5</li>
            </ol>
            <div class="panel panel-primary">
              <div class="panel-heading">DATOS DE ACCESO</div>
            <div class="panel-body">
            <div class="row">
              <div class="col-md-3">
                <div class="form-group">
                  <label>Correo Electrónico</label><br>
                   {!!Form::text('email',(!is_null($datos))?$datos->CMAIL_PRIM:old('email'),['class'=>'form-control','id'=>'email','maxlength'=>'80','onkeyup="minuscula(this.value,"#email");"'])!!}
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label>Correo Alternativo</label><br>
                   {!!Form::text('email_seg',(!is_null($datos))?$datos->CMAIL_SEC:old('email_seg'),['class'=>'form-control','id'=>'email_seg','onkeyup="minuscula(this.value,"#email_seg");"','maxlength'=>'80'])!!}
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label>Contraseña</label><br>
                    {!!Form::password('password',['class'=>'form-control','id'=>'password','maxlength'=>'100'])!!}

                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label>Confirme Contraseña</label><br>
                  {!!Form::password('password_confirm',['class'=>'form-control','id'=>'password_confirm','maxlength'=>'100'])!!}
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group" id="lista_pregunta">
                  <label>Pregunta de Seguridad</label>
                  {!!Form::select('id_cat_preguntas_seg',$preguntas_seg,null,
                  ['class'=>'form-control','id'=>'id_cat_preguntas_seg','onchange="preguntaSeg ();"','placeholder'=>'--Seleccione una Opción--'])!!}

                </div>
                <div class="form-group" id="preg_alternativa" style="display: none;">
                  <label>Pregunta Alternativa</label><br>
                {!!Form::text('pregunta_alter',null,['class'=>'form-control','id'=>'pregunta_alter','maxlength'=>'200','onkeypress="return soloNumerosyLetras(event)"'])!!}
                  <span style="color:#3097D1;cursor:pointer;" id="lista_preg">lista de preguntas</span>
                </div>


              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>Respuesta de Seguridad</label><br>
                   {!!Form::text('respuesta_seg',null,['class'=>'form-control','id'=>'respuesta_seg','maxlength'=>'100','onkeypress="return soloNumerosyLetras(event)"'])!!}


                </div>
              </div>
            </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="col-md-4"></div>
                  <div class="col-md-4 text-center">
                    <div class="form-group">

                      <a class="btn btn-primary" href="#" id="atras4">Atras</a>
                      <a class="btn btn-primary" href="#" id="seguir5" data-toggle="modal" data-target="#myModal">Completar inscripción</a>

                    </div>
                  </div>
                  <div class="col-md-4"></div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Declaración Jurada del Exportador</h4>
      </div>
      <div class="modal-body">
      <div class="col-md-12">
        <p class="text-justify">Usted en su carácter de (Presidente, representante legal, apoderado), de la empresa, declara conocer las condiciones y términos establecidos en el basamento legal aplicable para esta solicitud, así como las consecuencias que de ellas se deslinden; a tales efectos, manifiesta que tanto la información como la documentación suministrada, son ciertas, fidedignas e irrefutables, ya que reflejan con total precisión, los elementos de comprobación necesarios para su validez y eficacia; en este sentido autorizo al Ministerio del Poder Popular de Economía, Finanzas y Comercio Exterior, o a quien este designe, a encauzar todos los medios de confirmación que considere necesarios, sin más limitaciones que las establecidas en la Constitución y las Leyes de la República, sin perjuicio de las acciones civiles, penales, administrativas que como resultado de dicha comprobación tuvieren lugar en razón de una omisión o suministro de información falsa a la Administración Pública.
 
        De igual modo declara que, la recepción de toda notificación, mensaje de datos o incidencias relacionadas con esta solicitud, será efectiva desde el momento que sea emitida por este Sistema, de conformidad a lo estipulado en los artículos  9 y 10 el Decreto con Rango, Valor y Fuerza de Ley Nº 1204, de fecha 10 de febrero de 2001, que regula la materia de Mensaje de Datos y Firmas Electrónicas, publicado en la Gaceta Oficial de la República Bolivariana de Venezuela bajo el número 37.148, de fecha 28 de febrero de 2001.
        </p>
        <div class="row">
          <div class="colo-md-1"></div>
          <div class="col-md-10">

             <input type="checkbox" name="condicion" id="condicion" value="si" class="checkbox-inline"><label>Declaro haber leído y aceptado las condiciones antes expuestas</label>
          </div>
          <div class="colo-md-1"></div>
        </div>
      </div>

      </div>

      <div class="modal-footer">
        <div class="row">
          <div class="col-md-12">
            <div class="col-md-4"></div>
            <div class="col-md-4 text-center">
              <div class="form-group">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <a class="btn btn-primary" style="cursor: pointer;" onclick="enviar()" id="guardar">Guardar</a>
              </div>
            </div>
            <div class="col-md-4"></div>
          </div>
        </div>
      </div>
    </div>

  </div>
</div>
<!-- fin Modal -->
{{Form::close()}}

</div>

<script src="{{asset('js/ajax_v2.1.1.js')}}"></script>
<script src="{{asset('js/validator_registro.js')}}"></script>


@endsection
