<?php

namespace App\Http\Controllers;

use App\Models\GenDVDSolicitud;
use App\Models\GenFactura;
use App\Models\GenDua;
use App\Models\GenParametro;
use App\Models\DocumentosDvdBdv;
use Session;
use Auth;
use Laracasts\Flash\Flash;
use Alert;
use Illuminate\Http\Request;

class ListaVentaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\GenDVDSolicitud  $genDVDSolicitud
     * @return \Illuminate\Http\Response
     */
    public function show(GenDVDSolicitud $genDVDSolicitud)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\GenDVDSolicitud  $genDVDSolicitud
     * @return \Illuminate\Http\Response
     */
    public function edit(GenDVDSolicitud $genDVDSolicitud, $id)
    {
        $titulo="Declaración de Ventas de Divisas";
        $descripcion="Ventas de Divisas Registradas Solicitud Exportación Realizada (ER)";
        $listaventa=GenDVDSolicitud::where('gen_solicitud_id',$id)->where('bactivo',1)->get();
        //dd($id);
        // $Doc_Dvd=DocumentosDvdBdv::where('gen_dvd_solicitud_id',$listaventa->id)->first();
        $idsolicitud=$id;
        //dd($listaventa);
        if (!empty($listaventa[0])) {
            return view('ListaVenta.edit',compact('listaventa','titulo','descripcion','idsolicitud'));
        } else {
            //dd($listaventa);
             Alert::warning('Debe registrar por lo menos una', 'No hay venta de divisa Registrada!')->persistent("Ok");
            return view('ListaVenta.edit',compact('listaventa','titulo','descripcion','idsolicitud'));

        }
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\GenDVDSolicitud  $genDVDSolicitud
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, GenDVDSolicitud $genDVDSolicitud)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\GenDVDSolicitud  $genDVDSolicitud
     * @return \Illuminate\Http\Response
     */
    public function destroy(GenDVDSolicitud $genDVDSolicitud)
    {
        //
    }
}
