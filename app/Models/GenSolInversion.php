<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GenSolInversion extends Model
{
    protected $table = 'gen_sol_inversion';

    protected $fillable = [
    	'gen_status_id',
    	'gen_sol_inversionista_id',
    	'gen_divisa_id',
    	'nro_sol_inversion',
    	'fincripcion_rif',
    	'fvencimiento_rif',
    	'codigo_ciiu',
    	'zona_posta',
    	'fax',
    	'empleos_actuales_directos',
    	'empleos_actuales_indirectos',
    	'empleos_generar_directos',
    	'empleos_generar_indirectos',
    	'capital_social_suscrito',
    	'capital_social_pagado',
    	'num_acciones',
    	'valor_nominal_accion',
    	'monto_inversion',
    	'adquisicion_bienes',
    	'maquinaria',
    	'software',
    	'capital_trabajo',
    	'reinversion',
    	'otra',
    	'total',
    	'estado_observacion',
    	'observacion_accionista',
    	'bactivo',
    ];

	protected $date =['created_at','updated_at'];

	public function status(){
		return $this->hasOne('App\Models\GenStatus','id','gen_status_id');
	}

	public function inversionista(){
		return $this->hasOne('App\Models\GenSolInversionista','id','gen_sol_inversionista_id');
	}

	public function divisa(){
		return $this->hasOne('App\Models\GenDivisa','id','gen_divisa_id');
	}

}
