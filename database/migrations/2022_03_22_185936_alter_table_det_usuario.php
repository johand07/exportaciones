<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableDetUsuario extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('det_usuario', function (Blueprint $table) {
            
            $table->string('tipo_documento', 200)->nullable()->after('especifique_tecno');
            $table->string('ruta_doc', 500)->nullable()->after('especifique_tecno');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('det_usuario', function (Blueprint $table) {
            //
            $table->dropColumn('tipo_documento');
            $table->dropColumn('ruta_doc');
        });
    }
}
