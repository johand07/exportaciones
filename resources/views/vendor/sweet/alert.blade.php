
@if($errors->first('cantidad_producto.*')!=null or ($errors->first('precio_producto.*')!=null) or ($errors->first('codigo_arancel.*')!=null) or ($errors->first('descripcion_comercial.*')!=null) or ($errors->first('descripcion_arancelaria.*')!=null) or ($errors->first('unidad_medida_id.*')!=null) )
    <script>
    swal({
           title: "{{$errors->first('cantidad_producto.*')}}"+'\n'+
                   "{{$errors->first('precio_producto.*')}}"+'\n'+
                    "{{$errors->first('codigo_arancel.*')}}"+'\n'+
                     "{{$errors->first('descripcion_comercial.*')}}"+'\n'+
                      "{{$errors->first('descripcion_arancelaria.*')}}"+'\n'+
                       "{{$errors->first('unidad_medida_id.*')}}",


           timer: null,
           type: "{!! Session::get('sweet_alert.type') !!}",
           showConfirmButton: "{!! Session::get('sweet_alert.showConfirmButton') !!}",
           // more options
       });
    </script>

@elseif(Session::has('sweet_alert.alert'))


  <script>
   swal({!! Session::pull('sweet_alert.alert') !!});
  </script>


@endif
