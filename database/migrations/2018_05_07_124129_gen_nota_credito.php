->nullable()<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GenNotaCredito extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('gen_nota_credito', function (Blueprint $table) {
           $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('gen_factura_id')->index()->nullable();
            $table->foreign('gen_factura_id')->references('id')->on('gen_factura');
            $table->unsignedInteger('gen_usuario_id')->index()->nullable();
            $table->foreign('gen_usuario_id')->references('id')->on('gen_usuario');
            $table->unsignedInteger('gen_consignatario_id')->index()->nullable();
            $table->foreign('gen_consignatario_id')->references('id')->on('gen_consignatario');
            $table->integer('numero_nota_credito')->nullable();
            $table->date('femision')->nullable();
            $table->string('concepto_nota_credito', 100)->nullable();
            $table->double('monto_nota_credito')->nullable();
            $table->string('justificacion', 100)->nullable();
            $table->boolean('bactivo')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::dropIfExists('gen_nota_credito');
    }
}
