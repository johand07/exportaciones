<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GenSolicitud extends Model
{
     protected $table = 'gen_solicitud';
      
      protected $fillable = [

      	'gen_usuario_id',
      	'fsolicitud',
      	'tipo_solicitud_id',
        'gen_status_id',
        'fstatus',
      	'opcion_tecnologia',
      	'observacion_analista_intra',
      	'observacion_coordinador_intra',
      	'bactivo',
    ];

    protected $date =['created_at','updated_at'];

    public function usuario() {

     return $this->hasOne('App\Models\GenUsuario','id','gen_usuario_id');

    }

     public function tiposolictud() {

     return $this->hasOne('App\Models\TipoSolicitud','id','tipo_solicitud_id');

    }

    public function status() {

     return $this->hasOne('App\Models\GenStatus','id','gen_status_id');

    }

    public function facturasolicitud() {

     return $this->belongsToMany('App\Models\GenFactura');

    }
}
