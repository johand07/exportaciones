@extends('templates/layoutlte_coord_inversionista')

@section('content')

<div class="container" style="background-color: #fff">
    <div class="panel-group" style="background-color: #fff;">
      <div class="panel-primary">
        <div class="panel-heading"><h3>Solicitudes de Potencial Inversionista Extranjero</h3></div>
        <div class="panel-body">
            <div class="row">
              <div class="col-md-12">
                <div class="row">
                  <div class="col-md-8">

                           <h4 class="text-info">Bandeja de Solicitudes Extranjero:</h4><hr>
                          
                  </div>
                  <div class="col-md-2">
                   
                  </div>
                  <div class="col-md-2">
                    <a class="btn btn-primary" href="{{route('ListaCoordExtranjero.index')}}">
                      <span class="glyphicon glyphicon-refresh" title="Agregar" aria-hidden="true"></span>
                        Actualizar
                    </a>
                  </div>
                </div>
                <div style="overflow-y: auto;">
                  <table id="listaCoordExtrajro" class="table table-striped table-bordered" style="width:100%">
                      <thead>
                        <tr class="text-center">
                          <th class="text-center">N° Solicitud</th>
                          <th class="text-center">N° Rif</th>
                          <th class="text-center">Observación</th>
                          <th class="text-center">Fecha de Solicitud</th>
                          <th class="text-center">Fecha de Estatus</th>
                          <th class="text-center">Estatus</th>
                          <th class="text-center">Analista</th>
                          <th class="text-center">Coordinador</th>
                          <th class="text-center">Acciones</th>
                        </tr>
                      </thead>
                      <tbody>
                      @foreach($coordinador_inversion as $key => $coordinador_inv)
                          <tr>
                             <td class="text-center text-primary">{{$coordinador_inv->rinversionista->num_sol_inversionista}}</td>
                             <td class="text-center">{{$coordinador_inv->rinversionista->DetUsuario->rif}}</td>
                             <td class="text-center">
                             @if($coordinador_inv->rinversionista->gen_status_id==13 &&!empty($coordinador_inv->rinversionista->observacion_anulacion))
                              {{$coordinador_inv->rinversionista->observacion_anulacion}}
                            @elseif($coordinador_inv->rinversionista->gen_status_id==15 &&!empty($coordinador_inv->rinversionista->observacion_doc_incomp))
                              {{$coordinador_inv->rinversionista->observacion_doc_incomp}}
                              
                            @elseif($coordinador_inv->rinversionista->gen_status_id==17 &&!empty($coordinador_inv->rinversionista->observacion_anulacion))
                              {{$coordinador_inv->rinversionista->observacion_anulacion}}
                             @endif
                             @if($coordinador_inv->rinversionista->gen_status_id==11 &&!empty($coordinador_inv->rinversionista->observacion_inversionista))
                              {{$coordinador_inv->rinversionista->observacion_inversionista}}
                             @endif
                             </td>
                            
                             <td class="text-center">{{$coordinador_inv->rinversionista->created_at}}</td>
                             <td class="text-center">{{$coordinador_inv->rinversionista->fstatus}}</td>
                             <td class="text-center">{{$coordinador_inv->rinversionista->estatus->nombre_status}}</td>
                             <td>{{$coordinador_inv->rinversionistauser->email}}</td>
                             <td>{{$coordinador_inv->rcoordinversion ? $coordinador_inv->rcoordinversion->rusuario->email : 'sin atender'}}</td>
                             <td class="col-md-4 text-center">
                               <!--Validacion de la Bandeja Coordinador-->
                          
                            @if($coordinador_inv->rinversionista->gen_status_id==12 )
                              <a href="{{ route('ListaCoordExtranjero.edit',$coordinador_inv->rinversionista->id)}}" class="btn btn-sm btn-danger" id="atender"><i class="glyphicon glyphicon-user"></i><b> Atender</b></a>
                            @endif

                            @if($coordinador_inv->rinversionista->gen_status_id==14 )
                              <a href="{{ route('ListaCoordExtranjero.edit',$coordinador_inv->rinversionista->id)}}" class="btn btn-sm btn-warning" id="atender"><i class="glyphicon glyphicon-user"></i><b> Continuar</b></a>
                            @endif

                            
                            @if($coordinador_inv->rinversionista->gen_status_id ==9 || $coordinador_inv->rinversionista->gen_status_id ==10||$coordinador_inv->rinversionista->gen_status_id ==11 || $coordinador_inv->rinversionista->gen_status_id ==12 || $coordinador_inv->rinversionista->gen_status_id ==14||$coordinador_inv->rinversionista->gen_status_id ==16 || $coordinador_inv->rinversionista->gen_status_id ==17 || $coordinador_inv->rinversionista->gen_status_id ==18 ||
                              $coordinador_inv->rinversionista->gen_status_id ==19 ||$coordinador_inv->rinversionista->gen_status_id ==26)

                            <a href="{{action('PdfController@planillaSolInversionista',array('id'=>$coordinador_inv->rinversionista->id))}}" class="glyphicon glyphicon-file btn btn-default btn-sm" target="_blank" title="Planilla Forma RPI-01" aria-hidden="true"></a>
                            @endif
                           
                            @if($coordinador_inv->rinversionista->gen_status_id==13 && $coordinador_inv->rinversionista->estado_observacion == 1 && !empty($coordinador_inv->rinversionista->observacion_anulacion)) 
                             <!--a href="{{ route('ListaCoordExtranjero.edit',$coordinador_inv->rinversionista->id)}}" class="btn btn-sm btn-warning list-inline" id="anulacion"><i class="glyphicon glyphicon-trash"></i><b> Anulación</b></a-->
                            @endif

                            {{--@if($coordinador_inv->rinversionista->gen_status_id==18) 
                             <a href="{{ route('ListaCoordExtranjero.edit',$coordinador_inv->rinversionista->id)}}" class="btn btn-sm btn-default list-inline" id="reatender"><i class="glyphicon glyphicon-share"></i><b> reatender</b></a>
                            @endif--}}

                            @if($coordinador_inv->rinversionista->gen_status_id==16 ||$coordinador_inv->rinversionista->gen_status_id == 26)
                            <a href="{{action('PdfController@CertificadoInversionista',array('id'=>$coordinador_inv->rinversionista->id))}}" target="_blank" class="glyphicon glyphicon-file btn btn-success btn-sm" title="Certificado Inversionista" aria-hidden="true">Certificado</a>
                            
                            <a href="{{ route('ListaCoordExtranjero.show',$coordinador_inv->rinversionista->id)}}" class="btn btn-sm btn-primary" id="atender"><i class="glyphicon glyphicon-eye-open"></i><b> Ver</b></a>
                            @endif

                             </td>
                         </tr>
                      @endforeach
                      </tbody>   
                  </table>
                </div>
              </div>
            </div><!-- Cierre 1° Row-->
        </div>
      </div>
    </div>
</div><!-- fin content-->
@stop
