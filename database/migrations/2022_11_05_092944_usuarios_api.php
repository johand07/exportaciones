<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UsuariosApi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuarios_api', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('gen_tipo_usuario_id')->default(1)->index()->nullable();
            $table->foreign('gen_tipo_usuario_id')->references('id')->on('gen_tipo_usuario');
            $table->unsignedInteger('gen_status_id')->default(1)->index()->nullable();
            $table->foreign('gen_status_id')->references('id')->on('gen_status');
            $table->string('email', 150)->unique()->nullable();
            $table->string('password', 100)->nullable();
            $table->string('api_token', 200)->nullable();
            $table->boolean('bactivo')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usuarios_api');
    }
}
