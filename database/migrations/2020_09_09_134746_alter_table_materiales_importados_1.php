<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableMaterialesImportados1 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
    {
         Schema::table('materiales_importados', function (Blueprint $table) {
            //
        
            $table->string('rif_productor')->nullable()->after('incidencia_costo');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('materiales_importados', function (Blueprint $table) {
            //
            $table->dropColumn('rif_productor');
        });
    }
}
