<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTablePlanillaDjir02 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('planilla_djir02', function (Blueprint $table) {
            //
            $table->string('moneda_informacion_financiera')->nullable()->after('anio_informacion_financiera'); 
            $table->string('moneda_bienes_tangibles')->nullable()->after('credito_casa_matriz');
            $table->double('moneda_bienes_intangibles')->nullable()->after('otros_activos_tangibles');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('planilla_djir02', function (Blueprint $table) {
            //
            $table->dropColumn('moneda_informacion_financiera');
            $table->dropColumn('moneda_bienes_tangibles');
            $table->dropColumn('moneda_bienes_intangibles');
        });
    }
}
