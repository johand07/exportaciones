<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PermisoProductosRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'codigo_arancel.*'=>'required',
            'descrip_arancel.*'=>'required',
            'descrip_comercial.*'=>'required',
            'cantidad.*'=>'required|numeric',
            'gen_unidad_medida_id.*'=>'required'
        ];
    }
}
