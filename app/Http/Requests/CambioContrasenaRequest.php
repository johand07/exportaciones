<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CambioContrasenaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email'=>'email', 
            'password'=>'required|max:100|min:6',
            'password_confirm'=>'required|min:6|same:password',
        ];
    }
}
