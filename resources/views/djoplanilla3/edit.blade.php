@extends('templates/layoutlte')
@section('content')
<div class="content">
  @if(!empty($planilla3->estado_observacion))
  <div id="space_alert1" class="alert alert-danger" role="alert" style="display: block;">
    <strong>
      <span class="glyphicon glyphicon-circle-arrow-right"></span> Observación Indicada por el Analista: {{$planilla3->descrip_observacion}}
    </strong>
  </div>
@endif
  <div class="panel-group" style="background-color: #fff;">
      <div class="panel-primary">
          <div class="panel-heading"><h3>(P-3) - 5. Estructura de Costos por Unidad de Medida del Producto a Exportar</h3> </div>
          <div class="panel-body">

              <div class="row" style="background-color: #fff">
                  <div class="col-md-12">
                {!! Form::model($planilla3,['route' =>array('DJOPlanilla3.update',$planilla3->id) ,'method'=>'PUT','id'=>'planilla3UpdateForm']) !!}


                          <div class="row">
                              
                              <div class="col-md-6">
                                  <div class="form-group">
                                      {!! Form::label('descripcion_comercial','5.1 Denominación Comercial')!!}
                                      {!! Form::text('descripcion_comercial',$det_declaracion_product->rProducto->descrip_comercial,['class'=>'form-control','readonly'=>'readonly'])!!}
                                  </div>
                              </div>
                              <div class="col-md-6">
                                  <div class="form-group">
                                    <label>5.2 Moneda</label><br>
                                    {{Form::select('divisa',$divisas,$planilla3->gen_divisa_id,['placeholder'=>'Seleccione','class'=>'form-control','id'=>'id_gen_sector'])}}
                                      
                                  </div>
                              </div>
                              
                              <div class="col-md-12"><hr></div>

                              
                          </div>
                          <div class="row">
                              <div class="col-md-12">
                                  <div class="col-md-6">                                     
                                      <table border="2"  class=" table-responsive table-striped table-hover table">
                                        <thead>
                                          <th>COSTOS DIRECTOS</th>
                                          <th>TOTAL COSTOS</th>
                                        </thead>
                                        <tbody>
                                          <tr>
                                            <td>MATERIALES IMPORTADOS = (4.6.5 DE LA P2)</td>
                                            <td class="text-center">
                                                {!! Form::number('total_importado',$det_declaracion_product->rPlanilla2->total_mate_import,['class'=>'form-control text-center','id'=>'total_importado','readonly'=>'readonly'])!!}
                                            </td>
                                          </tr>
                                          <tr>
                                            <td>MATERIALES NACIONALES = (4.6.11 DE LA P2)</td>
                                            <td class="text-center">
                                                {!!Form::number('total_nacionales',$det_declaracion_product->rPlanilla2->total_mate_nac,['class'=>'form-control text-center','id'=>'total_nacionales','readonly'=>'readonly','step' => '0.01'])!!}
                                            </td>
                                          </tr>
                                          <tr>
                                            <td>MANO DE OBRA</td>
                                            <td>
                                                
                                                {!! Form::number('mano_obra',$planilla3->costosDirectos->mano_obra,['class'=>'form-control text-center ', 'id'=>'mano_obra','placeholder'=>'Ingresar monto','step' => '0.01'])!!}
                                            </td>
                                          </tr>
                                          <tr>
                                            <td>DEPRECIACIÓN</td>
                                            <td>
                                                {!! Form::number('depreciacion',$planilla3->costosDirectos->depreciacion,['class'=>'form-control text-center','id'=>'depreciacion', 'placeholder'=>'Ingresar monto','step' => '0.01'])!!}
                                            </td>
                                          </tr>
                                          <tr>
                                            <td>OTROS COSTOS DIRECTOS</td>
                                            <td>
                                                {!! Form::number('otros_costos',$planilla3->costosDirectos->otros_costos_dir,['class'=>'form-control text-center','id'=>'otros_costos', 'placeholder'=>'Ingresar monto','step' => '0.01'])!!}
                                            </td>
                                          </tr>
                                          <tr>
                                            <td colspan="2" class="text-center"><b>(1)  TOTAL COSTOS DIRECTOS</b>
                                           
                                                {!! Form::number('total_costos_directos','null',['class'=>'form-control text-center','id'=>'total_costos_directos', 'readonly'=>'readonly','step' => '0.01'])!!}
                                            </td>
                                          </tr>
                                        </tbody>
                                      </table>
                                  </div>
                                  <div class="col-md-6">
                                     <table border="2"  class=" table-responsive table-striped table-hover table">
                                        <thead>
                                          <th>COSTOS INDIRECTOS</th>
                                          <th>TOTAL COSTOS</th>
                                        </thead>
                                        <tbody>
                                          <tr>
                                            <td>GASTOS ADMINISTRATIVOS = (4.6.5 DE LA P2)</td>
                                            <td> 
                                              {!! Form::number('gastos_administrativos',$planilla3->costosInDirectos->gastos_admin,['class'=>'form-control text-center','id'=>'gastos_administrativos', 'placeholder'=>'Ingresar monto','step' => '0.01'])!!}
                                            </td>
                                          </tr>
                                          <tr>
                                            <td>GASTOS DE PUBLICIDAD = (4.6.11 DE LA P2)</td>
                                            <td>
                                               {!! Form::number('gastos_publicidad',$planilla3->costosInDirectos->gastos_publicidad,['class'=>'form-control text-center','id'=>'gastos_publicidad', 'placeholder'=>'Ingresar monto','step' => '0.01'])!!}
                                            </td>
                                          </tr>
                                          <tr>
                                            <td>GASTOS DE VENTAS</td>
                                            <td>
                                               {!! Form::number('gastos_ventas',$planilla3->costosInDirectos->gastos_ventas,['class'=>'form-control text-center','id'=>'gastos_ventas', 'placeholder'=>'Ingresar monto','step' => '0.01'])!!}
                                            </td>
                                          </tr>
                                         
                                          <tr>
                                            <td>OTROS COSTOS DIRECTOS</td>
                                            <td>
                                               {!! Form::number('tros_gastos',$planilla3->costosInDirectos->otros_gastos_indi,['class'=>'form-control text-center','id'=>'tros_gastos', 'placeholder'=>'Ingresar monto','step' => '0.01'])!!}
                                            </td>
                                          </tr>
                                          <tr><td></td></tr>
                                          <tr>
                                            <td colspan="2" class="text-center"><b>(2) TOTAL GASTOS INDIRECTOS</b>
                                              {!! Form::number('total_gastos_indirectos','0,00',['class'=>'form-control text-center','id'=>'total_gastos_indirectos', 'readonly'=>'readonly','step' => '0.01'])!!}
                                            </td>
                                          </tr>
                                        </tbody>
                                      </table>
                                  </div>
                                  <div class="col-md-12">
                                    
                                    <table border="2"  class=" table-responsive table-striped table-hover table">
                                        
                                        <tbody>
                                          <tr>
                                            <td><b>(3)</b> UTILIDAD</td>
                                            <td> 
                                              {!! Form::number('utilidad',null,['class'=>'form-control text-center','id'=>'utilidad', 'placeholder'=>'Ingresar monto','step' => '0.01', 'required'=>'true'])!!}
                                            </td>
                                          </tr>
                                          <tr>
                                            <td><b>(4)</b> PRECIO A PUERTA DE FÁBRICA = 1 + 2 + 3</td>
                                            <td>
                                               {!! Form::number('precio_fabrica',null,['class'=>'form-control text-center','id'=>'precio_fabrica','readonly'=>'readonly', 'placeholder'=>'Ingresar monto','step' => '0.01'])!!}
                                            </td>
                                          </tr>
                                          <tr>
                                            <td><b>(5)</b> FLETE INTERNO </td>
                                            <td>
                                               {!! Form::number('flete_interno',null,['class'=>'form-control text-center','id'=>'flete_interno', 'placeholder'=>'Ingresar monto','step' => '0.01', 'required'=>'true'])!!}
                                            </td>
                                          </tr>
                                         
                                          <tr>
                                            <td><b>(6)</b> PRECIO FOB DE EXPORTACIÓN = 4 + 5</td>
                                            <td>
                                               {!! Form::number('precio_fob','',['class'=>'form-control text-center','id'=>'precio_fob','readonly'=>'readonly', 'placeholder'=>'Ingresar monto','step' => '0.01'])!!}
                                            </td>
                                          </tr>
                                          
                                        </tbody>
                                      </table>  
                                  </div> 
                              </div>
                          </div>
                          
                          <hr>
                          <div class="row" style="background-color: #fff">

                                <div class="col-md-12" align="center">

                                    <!--input type="submit"  name="submit" id="submit" class="btn btn-info"  value="Siguiente"/-->
                                    <button  type="submit"  name="submit" id="submit"  class="btn btn-primary" onclick="validacionForm()"><b>Corregir</b></button>

                                </div>
                              
                          </div>

                    {{Form::close()}}
                </div>
              </div>

        </div>
      </div>
  </div><!-- Fin panel -->
</div><!-- Fin content -->


<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

<script type="text/javascript">
$(document).ready(function ()
{
    sumarCostosDirectos();
    sumarGastosIndirectosDirectos();
    sumarPrecioFabrica();

     function sumarCostosDirectos()
     {  
          var suma =0;
            var total_m_importado=parseFloat($('#total_importado').val());
            var total_m_nacional=parseFloat($('#total_nacionales').val());
            var total_materiales=total_m_importado+total_m_nacional;

            var suma =parseFloat(total_materiales);

 
            var manoObra=parseFloat($('#mano_obra').val());
            var depreciacion=parseFloat($('#depreciacion').val());
            var otros_costos=parseFloat($('#otros_costos').val());

            if(!isNaN(manoObra)){
            suma +=manoObra;
            
            }
            if(!isNaN(depreciacion)){
            suma +=depreciacion;
            
            }
            if(!isNaN(otros_costos)){
            suma +=otros_costos;
            
            }
       
            $("#total_costos_directos").val(suma);

            sumarPrecioFabrica();
            
              
      }

      function sumarGastosIndirectosDirectos()
      {  

          

            var suma =0;

            var gastos_administrativos=parseFloat($('#gastos_administrativos').val());
            var gastos_publicidad=parseFloat($('#gastos_publicidad').val());
            var gastos_ventas=parseFloat($('#gastos_ventas').val());
            var tros_gastos=parseFloat($('#tros_gastos').val());

            if(!isNaN(gastos_administrativos)){
            suma +=gastos_administrativos;
            
            }

            if(!isNaN(gastos_publicidad)){
            suma +=gastos_publicidad;
            
            }
            if(!isNaN(gastos_ventas)){
            suma +=gastos_ventas;
            }
            if(!isNaN(tros_gastos)){
            suma +=tros_gastos;
            
            }
       
            $("#total_gastos_indirectos").val(suma);

            sumarPrecioFabrica();
            
      }
      function sumarPrecioFabrica()
      {  

          

            var suma =0;

            var total_costos_directos=parseFloat($('#total_costos_directos').val());
            var total_gastos_indirectos=parseFloat($('#total_gastos_indirectos').val());
            var utilidad=parseFloat($('#utilidad').val());
            

            if(!isNaN(total_costos_directos)){
            suma +=total_costos_directos;
            
            }

            if(!isNaN(total_gastos_indirectos)){
            suma +=total_gastos_indirectos;
            
            }
            if(!isNaN(utilidad)){
            suma +=utilidad;
            }
           
       
            $("#precio_fabrica").val(suma);

            sumarPrecioFOB();   
      }
       function sumarPrecioFOB()
      {  

          

            var suma =0;

            var precio_fabrica=parseFloat($('#precio_fabrica').val());
            var flete_interno=parseFloat($('#flete_interno').val());
           

            if(!isNaN(precio_fabrica)){
            suma +=precio_fabrica;
            
            }

            if(!isNaN(flete_interno)){
            suma +=flete_interno;
            
            }
            
       
            $("#precio_fob").val(suma);
              
      }


        /* Acciones de los campos */ 
 
        $(document).on('keyup','#mano_obra',function(){
              sumarCostosDirectos();
            });
         $(document).on('keyup','#depreciacion',function(){
              sumarCostosDirectos();
            });
         $(document).on('keyup','#otros_costos',function(){
              sumarCostosDirectos();
            });
        $(document).on('keyup','#gastos_administrativos',function(){
              sumarGastosIndirectosDirectos();
            });
         $(document).on('keyup','#gastos_publicidad',function(){
              sumarGastosIndirectosDirectos();
            });
         $(document).on('keyup','#gastos_ventas',function(){
              sumarGastosIndirectosDirectos();
            });
         $(document).on('keyup','#tros_gastos',function(){
              sumarGastosIndirectosDirectos();
            });
         $(document).on('keyup','#utilidad',function(){
              sumarPrecioFabrica();
            });
         $(document).on('keyup','#flete_interno',function(){
              sumarPrecioFOB();
            });
         /* ********************** */


              
          //Validación de formulario, se puede llamar y pasar la sección que contenga los inputs a validar
              
              var error = '<div class="msg_alert"><span style="color:red" class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span><strong id="cod_aran-error" class="added"> El campo es obligatorio.</strong></div>';
              var valido = '<div class="msg_alert"><span style="color:green" class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span></div>';
              var mens=['Estimado Usuario. Debe completar los campos solicitados',]

              validacionForm = function() {

              $('#planilla3UpdateForm').submit(function(event) {
              
              var campos = $('#planilla3UpdateForm').find('input, select');
              var n = campos.length;
              var err = 0;

              $("div").remove(".msg_alert");
              //bucle que recorre todos los elementos del formulario
              for (var i = 0; i < n; i++) {
                  var cod_input = $('#planilla3UpdateForm').find('input, select').eq(i);
                  if (!cod_input.attr('noreq')) {
                    if (cod_input.val() == '' || cod_input.val() == null)
                    {
                      err = 1;
                      cod_input.css('border', '1px solid red').after(error);
                    }
                    else{
                      if (err == 1) {err = 1;}else{err = 0;}
                      cod_input.css('border', '1px solid green').after(valido);
                    }
                    
                  }
              }

              //Si hay errores se detendrá el submit del formulario y devolverá una alerta
              if(err==1){
                  event.preventDefault();
                  swal("Por Favor!", mens[0], "warning")
                }

                });
              }

});
</script>

@endsection