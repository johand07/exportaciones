@extends('templates/layoutlte')
@section('content')
{{Form::model($solicitud,['route'=>['SolicitudER.update',$solicitud->id],'method'=>'PATCH','id'=>'formsolicituder'])}}
<br>
<div class="panels">
<div class="row"> 
	<div class="col-md-12">
		<div class="col-md-2"></div>
		<div class="col-md-6">
			<div class="form-group">
				{!! Form::label('fsolictud','Fecha de Solictud')!!}
	            {!! Form::text('fsolicitud',$fecha,['class'=>'form-control','id'=>'fsolicitud','readonly'=>'true'])!!}
            </div>
             <div class="form-group">
	            {!! Form::label('tsolicitud','Tipo de Solictud')!!}
	            {!! Form::select('tipo_solicitud_id',$tiposolic,null,['class'=>'form-control','placeholder'=>'Seleccione el Tipo de Solictud','id'=>'tipo_solicitud_id','onchange'=>'tiposolictud()']) !!}
	         </div>
        </div>
	</div>
	<div class="col-md-2"></div>
</div>

<div class="row">
      <div class="col-md-2"></div>
      <div class="col-md-6 text-center">
        @if($num==2)
        <div class="form-group" id="tipotecnologia">
          {!! Form::label('','Tecnología')!!}
          {!! Form::radio('opcion_tecnologia','Tecnologia',false,['class'=>'radio-inline','id'=>'accionsi'])!!}
          {!! Form::label('','Servicios')!!}
          {!! Form::radio('opcion_tecnologia','Servicios',false,['class'=>'radio-inline','id'=>'accionno'])!!}  
          {!! Form::select('consig_tecno',$consig,null,['class'=>'form-control','id'=>'consig_tecno']) !!}
          
        </div>
  @endif
      </div>
      <div class="col-md-4"></div>
</div>

<br><div class="row">
	<div class="col-md-12">
		<div class="col-md-4"></div>
		<div class="col-md-4 text-center">
		<a href="{{ route('SolicitudER.index') }}" class="btn btn-primary">Cancelar</a>
		<button type="submit" class="btn btn-success" value="Procesar" name="">Procesar</button>
		</div>
		<div class="col-md-4"></div>
	</div>
	</div>
	</div>
{{Form::close()}}
@stop
