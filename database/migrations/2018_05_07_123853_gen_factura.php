->nullable()<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GenFactura extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('gen_factura', function (Blueprint $table) {
          $table->engine = 'InnoDB';
          $table->increments('id');
          $table->unsignedInteger('gen_usuario_id')->index()->nullable();
          $table->foreign('gen_usuario_id')->references('id')->on('gen_usuario');
          $table->unsignedInteger('tipo_solicitud_id')->index()->nullable();
          $table->foreign('tipo_solicitud_id')->references('id')->on('tipo_solicitud');
          $table->unsignedInteger('forma_pago_id')->index()->nullable();
          $table->date('fecha_estimada_pago')->index()->nullable();
          $table->integer('plazo_pago')->nullable();
          $table->string('tipo_plazo')->nullable();
          $table->foreign('forma_pago_id')->references('id')->on('forma_pago');
          $table->unsignedInteger('tipo_instrumento_id')->index()->nullable();
          $table->foreign('tipo_instrumento_id')->references('id')->on('tipo_instrumento');
          $table->unsignedInteger('gen_dua_id')->index()->nullable();
          $table->foreign('gen_dua_id')->references('id')->on('gen_dua');
          $table->unsignedInteger('gen_divisa_id')->index()->nullable();
          $table->foreign('gen_divisa_id')->references('id')->on('gen_divisa');
          $table->unsignedInteger('incoterm_id')->index()->nullable();
          $table->foreign('incoterm_id')->references('id')->on('incoterm');
          $table->string('numero_factura', 80)->nullable();
          $table->date('fecha_factura')->nullable();
          $table->string('especif_instrumento', 100)->nullable();
          $table->double('monto_fob')->nullable();
          $table->double('monto_flete')->nullable();
          $table->double('monto_seguro')->nullable();
          $table->double('otro_monto')->nullable();
          $table->string('especif_pago', 200)->nullable();
          $table->double('monto_total')->nullable();
          $table->boolean('bactivo')->default(1);
          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::dropIfExists('gen_factura');
    }
}
