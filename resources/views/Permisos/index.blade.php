@extends('templates/layoutlte')
@section('content')

  <div class="row">
    <div class="col-md-12">
      <div class="row">
        <div class="col-md-10"></div>
        <div class="col-md-2">
          <a class="btn btn-primary" href="{{url('/exportador/Permisos/create')}}">
            <span class="glyphicon glyphicon-file" title="Agregar" aria-hidden="true"></span>
             Registro de Permisos
          </a>
        </div>
      </div>
      <table id="listapermiso" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>Tipo de Permiso</th>
                <th>Nro. de Permiso</th>
                <th>Fecha del Permiso</th>
                <th>Fecha Aprobación del Permiso</th>
                <th>Aduana Salida</th>
                <th>País Destino</th>
                <th>Acciones</th>
            </tr>
        </thead>
        <tbody>      
          @foreach($permiso as  $permis)
            <tr>                  
                <td>{{$permis->tipermiso->nombre_permiso}}</td>
                <td>{{$permis->numero_permiso}}</td>
                <td>{{$permis->fecha_permiso}}</td>
                <td>{{$permis->fecha_aprobacion}}</td>
                <td>{{$permis->aduanassal->daduana}}</td>
                <td>{{$permis->pais->dpais}}</td>
                <td>
                   <a href="{{route('Permisos.edit',$permis->id)}}" class="glyphicon glyphicon-edit btn btn-primary btn-sm" title="Modificar" aria-hidden="true"></a>    
                   
                   <input id="token" type="hidden" name="_token" value="{{ csrf_token() }}">
                   <a href="#" class="btn btn-danger btn-sm" title="Eliminar" onclick="return eliminar('Permisos/',{{$permis->id}},'listapermiso')"> <i class="glyphicon glyphicon-trash"></i> </a>
                </td>
            </tr>
          @endforeach
        </tbody>
      </table>
    </div>
</div>
@stop
