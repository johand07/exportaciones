<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Er</title>
    <style>
        body{
            text-align:center;
           font-family: Arial;
                 color: #58585A;
        }
        th{
            font-size:10px;
            background-color:#818286;
            color: #fff;
        }
        td{
            font-size:10px;
            border: 1px solid #818286;
        }
        .td2{
            font-size:10px;
            border: 0px solid #818286;
            width: 55%;
            text-align:center;
            font-size:14px;
        }
        .td3{

            border: 0px solid #818286;
            width: 15%;
            text-align:center;

        }
        .td4{

            border: 0px solid #818286;


        }
        table{
            border: 1px solid #818286;
        }
        .table2{
            border: 0px solid #818286;
        }

       table > tr > td {

          text-align: center;
        }
      .tabla > h4,h6{

        text-align: left;
        margin-bottom: 0;
        font-size:10px;

      }
      .tabla > table > thead > tr > th:nth-child(1){

         width: 30%;
      }
      .tabla > table > .tipo > tr:nth-child(1){

        border-top: 0;
      }

      .tabla2 > table > thead > tr > th:nth-child(0){

         width: 10%;
      }

    </style>
</head>
    <body>

      <div class="">
        <table style="width:100%;" class="table2">
          <tbody>
            <tr>
              <td class="td3">
                <img class="img-responsive" style="width: 165px; height: 115px" src='img/MIPPCOEXIN.png'>
              </td>
              <td class="td2">
                <h3>Exportación Realizada (ER) </h3>
              </td>
              <td class="td4">
                <table  style="width:100%">
                  <thead>
                    <tr>
                      <th>1) N° SOLICITUD</th>

                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td><b>{{$id}}</b></td>

                    </tr>
                  </tbody>
                </table>
                <table  style="width:100%">
                  <thead>
                    <tr>
                      <th>2) FECHA/HORA</th>

                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>{{$gen_solicitud->fsolicitud}}</td>

                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>

          </tbody>
        </table>


      </div>


      <div class="tabla">
        <div align="left">
          <span style="font-size:12px"><b>Datos del Exportador</b></span>
          <span style="margin-left:180px;">Código de Seguridad</span>
          <p style="font-size:10px;" align="center"><b>{{$gen_solicitud->cfirma_seguridad}}</b></p>
        </div>
          <table  style="width:100%">
            <thead>
              <tr>
                <th>3) NÚMERO DE RIF</th>
                <th>4) NOMBRE O RAZÓN SOCIAL</th>
                <th>5) SIGLAS REGISTRADAS </th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>{{$users->rif}}</td>
                <td>{{ucwords($users->razon_social)}}</td>
                <td>{{ucwords($users->siglas)}}</td>
              </tr>
            </tbody>
          </table>
      </div>


      <div class="tabla">
        <h4>Datos del Comprador o Consignatario</h4>
        <table  style="width:100%">
          <thead>
            <tr>
              <th>6) NOMBRE O RAZON SOCIAL</th>
              <th>7) PAIS </th>
              <th>8) CIUDAD</th>
              <th>9) CORREO ELECTRÓNICO </th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>
              @if (@$consig && isset($consig->nombre_consignatario))
                  {{ $consig->nombre_consignatario }}
              @endif  
             </td>
              <td>
              @if (@$consig && isset($consig->pais))  
                   {{$consig->pais->dpais}}
              @endif
             </td>
              <td>  @if (@$consig && isset($consig->ciudad_consignatario))  
                    {{$consig->ciudad_consignatario}}
                    @endif
              </td>
              <td>@if (@$consig && isset($consig->correo))  
                 {{$consig->correo}}
                 @endif
            </td>
            </tr>
          </tbody>
        </table>
      </div>


      <div class="tabla">
        <h4>DESCRIPCIÓN DE LA EXPORTACIÓN </h4>
        <table  class="table2" style="width: 100%;">
          <tr>
            <td class="td4">
              <h4>Tipo de Exportación: </h4>
            </td>
            @foreach($tipoSoli as $data)
              <td class="td4">

                <input type="checkbox" style="display:inline" @if($data->id==Session::get('tipo_solicitud_id')) checked @endif >&nbsp;&nbsp;&nbsp;
                {{$data->solicitud}}
              </td>
            @endforeach
          </tr>
        </table>

        <table  style="width:100%">
          <thead>
          <tr>

              <th>11) TIPO DE EXPORTACIÓN</th>
              <th>12) TIPO DE CONVENIO INTERNACIONAL </th>
            </tr>
          </thead>

          <tbody>
            <tr>

              <td>
                {{$gen_solicitud->opcion_tecnologia}}
              </td>
              <td>
                @foreach($tipoConv as $data)
                  <input type="checkbox" style="display:inline" @if($data->id==@$consig->cat_tipo_convenio_id) checked @endif />&nbsp;&nbsp;&nbsp;{{$data->nombre_convenio}}
                @endforeach
              </td>
            </tr>
          </tbody>
        </table>
      </div>
      <div class="tabla">
        <table  style="width:100%">
          <thead>
            <tr>
              <th>13) NÚMERO DE FACTURA </th>
              <th>14) FECHA DE EMISION DE FACTURA</th>
              <th>15) FECHA EMBARQUE</th>
              <th>16) N° DECLARACIÓN ADUANA EXPORTACIÓN  </th>

            </tr>
          </thead>
          <tbody>
            <tr>
              <td>@foreach($num_fac as $data)

                 {{$data}}<br>
                @endforeach
              </td>
              <td>@foreach($fecha_emision as $data)

                 {{$data}}<br>
                @endforeach</td>
              <td></td>
              <td></td>

            </tr>
          </tbody>
        </table>
      </div>
      <div class="tabla">
        <table  style="width:100%">
          <thead>
            <tr>
              <th>17) PUERTO EMBARQUE</th>
              <th>18) PUERTO DESTINO </th>


            </tr>
          </thead>
          <tbody>
            <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>

            </tr>
          </tbody>
        </table>
      </div>
      <div class="tabla">
        <table  style="width:100%">
          <thead>
            <tr>
              <th>18) N° NOTA CREDITO</th>
              <th>19) FECHA EMISIÓN NOTA CREDITO</th>
              <th>20) CONCEPTO NOTA CREDITO</th>


            </tr>
          </thead>
          <tbody>
            <tr>
            <td>
            @if(!is_null($notaCre))
                @foreach($notaCre as $data_n)
                  {{$data_n->numero_nota_credito}},
                @endforeach
              @endif
              </td>
            <td>@if(!is_null($notaCre))
                @foreach($notaCre as $data_n)
                  {{$data_n->femision}},
                @endforeach
              @endif
            </td>
            <td>
              @if(!is_null($notaCre))
                @foreach($notaCre as $data_n)
                  {{$data_n->concepto_nota_credito}},
                @endforeach
              @endif
              </td>
            </tr>
          </tbody>
        </table>
      </div>
      <div class="tabla">
        <table  style="width:100%">
          <thead>
            <tr>
              <th>21) DIVISA</th>
              <th>22) FORMA DE PAGO</th>
              <th>23) TIPO DE INSTRUMENTO</th>
            </tr>
          </thead>

          <tbody>
            <tr>
              <td>
                @if(!is_null($divisa))
                 @foreach($divisa as $data)
                 {{$data}}<br>
                 @endforeach
              @endif
              </td>
              <td>
                @foreach($forma_pago as $data)
                  <input type="checkbox" style="display:inline"
                  @foreach($formaPago as $pago)
                    @if($data->id==$pago) checked @endif
                  @endforeach
                   />&nbsp;&nbsp;&nbsp;{{$data->descrip_pago}}
                @endforeach
              </td>
              <td>
                @foreach($tipoInst as $data)
                    <input type="checkbox" style="display:inline"
                    @foreach($tipo_inst as $tipo)
                     @if( ($data->id==$tipo)) checked @endif
                     @endforeach
                      />&nbsp;&nbsp;&nbsp;{{$data->descrip_tipo_instrumento}}
                  @endforeach<br>
               

                @if($factura->forma_pago_id == 2)
                <b>Plazo de Pago</b> {{$factura->plazo_pago}} días
                @endif
              </td>

            </tr>
          </tbody>
        </table>
      </div>

      <div class="tabla">
        <table  style="width:100%">
          <thead>
            <tr>
              <th>24) CÓDIGO ARANCELARIO</th>
              <th>25) DESCRIPCIÓN DEL PRODUCTO</th>
              <th>26) CANTIDAD</th>
              <th>27) UNIDAD DE MEDIDA</th>
              <th>28) PRECIO UNITARIO</th>
              <th>29) VALOR FOB</th>
            </tr>
          </thead>

          <tbody>

              @foreach($products as $data)

           <tr>
              <td>{{$data->codigo_arancel}}</td>
              <td>{{$data->descripcion}}</td>
              <td>{{$data->cantidad}}</td>
              <td>{{$data->unidadesMedidas->dunidad}}</td>
              <td>{{$data->precio}}</td>
              <td>{{$data->valor_fob}}</td>

           </tr>
            @endforeach


          </tbody>
        </table>
      </div>

      <div class="tabla">
        <table  style="width:100%" >



          <tbody>

          <tr><td rowspan="3" colspan="4"> <b>OBSERVACIONES</b> </td>
              <td  width="17%" rowspan="3">
                SUB-TOTAL <br>
                MONTO NOTA DE CRÉDITO <br>
                FLETE <br>
                SEGURO <br>
                OTROS: <br>
                TOTAL</td>

              <td  width="10%" rowspan="3" align="center">
               {{$montoTotal}}<br>
               {{$notaCre_valor}}<br>
               {{$montoFlete}}<br>
               {{$montoSeguro}}<br>
               {{$montoOtro}}<br>
              <b> {{$total}}</b>



              </td>
          </tr>


          </tbody>
        </table>
      </div>

        <div>
        <table style="width:100%">
          <thead>
            <tr>
              <th colspan="2" align="center"> DECLARACIÓN JURADA DEL EXPORTADOR</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td colspan="2" align="justify">
                <p>De Conformidad con la normativa cambiaria, el exportador o representante legal declara conocer las condiciones y términos establecidos en el basamento legal aplicable para esta solicitud, así como las consecuencias que de ellas se deslinden; por tanto manifiesta que tanto la información como la documentación suministrada, son ciertas, fidedignas e irrefutables, ya que reflejan con total precisión, los elementos de comprobación necesarios para su validez y eficacia; en este sentido autorizo al Centro Nacional de Comercio Exterior (CENCOEX), o a quien este designe , a encauzar todos los medios de confirmación que considere necesarios, sin más limitaciones que las establecidas en la Constitución y las Leyes de la República, sin perjucios de las acciones civiles, penales, administrativas que como resultado de dicha comprobación tuvieren lugar.</p>

                <p>De Igual modo declaro que, la recepción de toda notificación, mensaje de datos o incidencias relacionadas con esta solicitud, será efectiva desde el momento que sea emitida por el Sistema de Exportadores del CENCOEX, de conformidad a lo estipulado en el articulo 11 del Decreto con Rango, Valor y Fuerza de Ley N° 1204, de fecha de 10 de febrero de 2001, que regula la materia de Mensaje de Datos y Firmas Electrónicas, publicado en la Gaceta Oficial de la República Bolivariana de Venezuela bajo el número 37.148, de fecha 28 de febrero de 2001.</p><br>


                <div align="center">

                 <p align="left" style="margin-left:98px">____________________________________________</p>

                <span>FIRMA DEL EXPORTADOR O REPRESENTANTE LEGAL</span>
                  <span>

                    &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;   LUGAR Y FECHA ____________________________/____/____/<br><br>
                  </span>
                </div>

          </td>
        </tr>

          </tbody>
        </table>
      </div>

      <br><br><br><br><div>
        <table style="width:100%">
          <thead>
            <tr>
              <th colspan="2" align="center"> DECLARACIÓN JURADA DEL OPERADOR CAMBIARIO</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td colspan="2" align="justify">
                 <br>
                <p>El Operador cambiario declara que ha examinado todos los documentos para dar cumplimiento, comprobar con los términos y condiciones previstos en la normativa cambiaria para efecto de cualquiera medida de control.</p><br><br>

                <div align="center">

                <p align="left" style="margin-left:98px"> ____________________________________________ </p>
                <span>

                  FIRMA DEL OPERADOR CAMBIARIO AUTORIZADO
                </span>

                <span>

                  &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;   LUGAR Y FECHA ____________________________/____/____/<br><br>
                </span>
            </div>

          </td>
        </tr>

          </tbody>
        </table>
      </div>





    </body>
</html>
