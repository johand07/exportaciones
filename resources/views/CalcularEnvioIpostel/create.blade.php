@extends('templates/layoutlte')
@section('content')




{{-- {!! Form::open(['route' => 'CalcularEnvioIpostel.store' ,'method'=>'POST']) !!} --}}
<div class="row">
	<div class="col-md-2"></div>
	<div class="col-md-8">
		<div class="row">
			<h4 class="text-center" style="color: red;">Calcula tu Envío</h4>
		</div>
		<div class="row">
			<div class="form-group">
				{!! Form::label('','Seleccione el Destino') !!}


		        {!! Form::select('pais_id',$pais,null,['class'=>'form-control','id'=>'pais_id',
	          'placeholder'=>'Seleccione el Destino','required'=>'true']) !!}
	        </div>

	        <div class="form-group">
				{!! Form::label('','Seleccione Origen de Envío') !!}


		        {!! Form::select('gen_aduana_salida_id',$aduanasal,null,['class'=>'form-control','id'=>'gen_aduana_salida_id',
	          'placeholder'=>'Seleccione Origen de Envío','required'=>'true']) !!}
	        </div>

	        <div class="form-group">
				<div class="form-group">
                  {!! Form::label('','Destino de Envío')!!}
                  {!! Form::text('destino_envio',null,['class'=>'form-control','id'=>'destino_envio','required'=>'true'])!!}
                </div>
	        </div>
		</div>
		<div class="row">
			<table style="width: 100%;">
				<tr>
					<td>
						<div class="form-group">
		                  {!! Form::text('alto',null,['class'=>'form-control','id'=>'alto','required'=>'true'])!!} 
		                </div>
					</td>
					<td>
						x
					</td>
					<td>
						<div class="form-group">
		                  {!! Form::text('ancho',null,['class'=>'form-control','id'=>'ancho','required'=>'true'])!!} 
		                </div>
					</td>
					<td>
						x
					</td>
					<td>
						<div class="form-group">
		                  {!! Form::text('largo',null,['class'=>'form-control','id'=>'largo','required'=>'true'])!!}
		                </div>
					</td>
				</tr>
			</table>
		</div>
		<div class="row">
			<div class="form-group">
                {!! Form::label('','Peso')!!}
                {!! Form::text('peso',null,['class'=>'form-control','id'=>'peso','required'=>'true'])!!}
            </div>
		</div>
		
		<div class="row text-center">
          <button class="btn btn-success" id="calcular_envio_ipostel"> Calcular </button>
        
		</div>
	</div>
	<div class="col-md-2"></div>
</div>

{{-- {{Form::close()}} --}}
<br><br>
<div class="row" id="resultado" style="display: none;">
	<h4 class="text-center" style="color: red;"><div id="total"></div></h4>
</div>	


@stop
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="{{asset('js/ajax_v2.1.1.js')}}"></script>
<script type="text/javascript">
$(document).ready(function (){	
	$('#calcular_envio_ipostel').click(function(event) {
		let myArray = [10, 25, 35, 45, 55, 65];
		let rand = Math.random()*myArray.length | 0;
		let rValue = myArray[rand];
		console.log(rValue)
		let alto = $('#alto').val();
		let ancho = $('#ancho').val();
		let largo = $('#largo').val();
		let peso = $('#peso').val();
		
		let dimension = alto * ancho * largo;
		let embalaje = peso * rValue;
		let total = embalaje + dimension / 3;
		console.log('total', total);
		$('#total').html(parseFloat(total).toFixed(2)+ ' BS');
        
        $('#resultado').show('show');
    });
});
</script>	