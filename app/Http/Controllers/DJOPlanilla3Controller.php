<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\GenUnidadMedida;
use App\Models\Productos;
use App\Models\Pais;
use App\Models\DetDeclaracionProduc;
use App\Models\Planilla2;
use App\Models\Planilla3;
use App\Models\CostosDirectos;
use App\Models\CostosInDirectos;
use App\Http\Requests\DJOPlanilla3Request;
use App\Models\GenDivisa;
use Auth;
use Alert;
use Session;
use DB;
use View;
use Response;

class DJOPlanilla3Controller extends Controller
{
    //
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
     
        return $this->create($request);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        
        $productos_id= $request['producto_id'];
        $gen_declaracion_jo_id = Session::get('gen_declaracion_jo_id');

        // validamos que exista la declaracion 
        if(empty($gen_declaracion_jo_id))
        {
          Alert::warning('La solicitud que intenta realizar requiere que este asociada a una Declaracion Jurada de Origen. Seleccione o creela.','Acción Denegada!')->persistent("OK");

          return redirect()->action('ListaDeclaracionJOController@index'); 
        }

        // consultamos el producto asociado a la declaracion
        $det_declaracion_product=DetDeclaracionProduc::with('rProducto')->with('rPlanilla2')->with('rPlanilla3')->where('gen_declaracion_jo_id',$gen_declaracion_jo_id)->where('id',$productos_id)->get()->last();
        
        // validamos que existe el producto asociado a la declaracion
        if(empty($det_declaracion_product->id)){

          Alert::warning('La solicitud que intenta realizar requiere que el producto este asociado a la Declaracion Jurada de Origen. Intente nuevamente.','Acción Denegada!')->persistent("OK");

          return redirect()->action('DeclaracionJOController@index');

        }else{
        
            Session::put('det_declaracion_product_id',$det_declaracion_product->id);
        
        }
        // Consultamos las divisas 
        $divisas=GenDivisa::whereIn('id',[18,32])->pluck('ddivisa_abr','id');

        
        // Verificamos si existe datos en la planilla asociada al producto 
        if(!empty($det_declaracion_product->rPlanilla3->id)){

           return redirect()->action('DJOPlanilla3Controller@edit', ['id' => $det_declaracion_product->rPlanilla3->id]);
        }


          $titulo= 'Declaración Jurada de Origen';
          $descripcion= 'Planilla 3';

        return view('djoplanilla3.create',compact('det_declaracion_product','titulo','descripcion','divisas'));  
    }

    public function store(DJOPlanilla3Request $request)
    {

      DB::beginTransaction();
      $productos_id= Session::get('det_declaracion_product_id');
      $det_declaracion_product_id = Session::get('det_declaracion_product_id');
      $gen_declaracion_jo_id = Session::get('gen_declaracion_jo_id');
      $descripcion_comercial= $request->descripcion_comercial;
      $divisa= $request->divisa;
      $total_importado= $request->total_importado;
      $total_nacionales= $request->total_nacionales;

      $mano_obra= $request->mano_obra;
      $depreciacion= $request->depreciacion;
      $otros_costos= $request->otros_costos;
      $total_costos_directos= $request->total_costos_directos;
      $gastos_administrativos= $request->gastos_administrativos;
      $gastos_publicidad= $request->gastos_publicidad;
      $gastos_ventas= $request->gastos_ventas;
      $tros_gastos= $request->tros_gastos;
      $total_gastos_indirectos= $request->total_gastos_indirectos;

      $utilidad= $request->utilidad;
      $precio_fabrica= $request->precio_fabrica;
      $flete_interno= $request->flete_interno;
      $precio_fob= $request->precio_fob;
      $datosverificados="";

        if(!empty($datosverificados)){

             Alert::warning('Verificar los datos.')->persistent("OK");

            return redirect()->back()->withInput();
        }

       $modelPlanilla3 = new Planilla3;
       $modelPlanilla3->det_declaracion_produc_id= $det_declaracion_product_id;
       $modelPlanilla3->gen_divisa_id= $divisa;
       $modelPlanilla3->utilidad = $utilidad;
       $modelPlanilla3->precio_puerta_fabri = $precio_fabrica;
       $modelPlanilla3->flete_interno = $flete_interno;
       $modelPlanilla3->precio_fob_export = $precio_fob;
       $modelPlanilla3->bactivo=1;

       //dd($modelPlanilla3);

       if ($modelPlanilla3->save())
       {

          /* costos directos */
          $modelCostoDirectos = new costosDirectos;
          $modelCostoDirectos->planilla_3_id=$modelPlanilla3->id;
          $modelCostoDirectos->mate_importados=$total_importado;
          $modelCostoDirectos->mate_nacional=$total_nacionales;
          $modelCostoDirectos->mano_obra=$mano_obra;
          $modelCostoDirectos->depreciacion=$depreciacion;
          $modelCostoDirectos->otros_costos_dir=$otros_costos;
          $modelCostoDirectos->total=$total_costos_directos;
          $modelCostoDirectos->bactivo=1;
          /* validacion */
          if($modelCostoDirectos->save())
          {
             Alert::warning('No se pudo procesar su solicitud','Acción Denegada!')->persistent("OK");
          }else{

            Alert::warning('Inserto Costos Directos!!')->persistent("OK");
            DB::rollback();
            return redirect()->back()->withInput();

          }
 


          /* costos indirectos */
          $modelCostoInDirectos = new costosInDirectos;
          $modelCostoInDirectos->planilla_3_id=$modelPlanilla3->id;
          $modelCostoInDirectos->gastos_admin=$gastos_administrativos;
          $modelCostoInDirectos->gastos_publicidad=$gastos_publicidad;
          $modelCostoInDirectos->gastos_ventas=$gastos_ventas;
          $modelCostoInDirectos->otros_gastos_indi=$tros_gastos;
          $modelCostoInDirectos->total=$total_gastos_indirectos;
          $modelCostoInDirectos->bactivo=1;
            /* validadcion */
            if($modelCostoInDirectos->save())
            {
              Alert::warning('Inserto Costos InDirectos!!')->persistent("OK");
            }else{

              Alert::warning('No se pudo procesar su solicitud','Acción Denegada!')->persistent("OK");
              DB::rollback();
              return redirect()->back()->withInput();
            }


          /* actualizar estatus de planilla3 */

            $detDeclaracionProducModel = $modelPlanilla3->declaracionProduc;
            $detDeclaracionProducModel->estado_p3=1;
            $detDeclaracionProducModel->save();

            DB::commit();
            Alert::success('Se han guardado los datos correspondientes a la Planilla 3!','Registrado Correctamente!')->persistent("Ok");            
            return redirect()->action('DeclaracionJOController@index');
             
          /* ******************* */
       }else{

          Alert::warning('No se pudo procesar su solicitud','Acción Denegada!')->persistent("OK");
          DB::rollback();
          return redirect()->back()->withInput();
       } 
    }

    /**
     * Display the specified resource.
     *
     * @param  Request  $request
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
  
        $layout= $request->layout==="false"? false: true;

        if(!empty($request->det_declaracion_id))
        {
            
            $det_declaracion= DetDeclaracionProduc::with('rPlanilla3')->find($request->det_declaracion_id);
        
            if(empty($det_declaracion)){
                Alert::error('La planilla que intenta visualizar no esta registrada en el sistema. D1','Acción Denegada!')->persistent("OK");
                return $layout? redirect()->action('DeclaracionJOController@index') : '<div class="alert alert-danger">La planilla que intenta visualizar no esta registrada en el sistema. D1</div>';
            }

            $planilla3 = $det_declaracion->rPlanilla3;


        }else{
            $id= $request->id ;
            $planilla3 = Planilla3::with('declaracionProduc')->with('costosDirectos')->with('costosInDirectos')->find($id);
        }

        /***** Validar que haya un registro de la planilla ****/
        if(empty($planilla3)){

             Alert::error('La planilla que intenta visualizar no esta registrada en el sistema.','Acción Denegada!')->persistent("OK");

            return  $layout? redirect()->action('DeclaracionJOController@index') : '<div class="alert alert-danger">La planilla que intenta visualizar no esta registrada en el sistema.</div>';
        }

        /***** Validar que la planilla sea de un usuario en especifico ****/
        if($planilla3->declaracionProduc->declaracionJO->gen_usuario_id!=Auth::user()->id && Auth::user()->gen_tipo_usuario_id!=5 && Auth::user()->gen_tipo_usuario_id!=6 && Auth::user()->gen_tipo_usuario_id!=1 && Auth::user()->gen_tipo_usuario_id!=8){

            Alert::error('No tiene permisos para visualizar esta planilla.','Acción Denegada!')->persistent("OK");

           return  $layout? redirect()->action('DeclaracionJOController@index') : '<div class="alert alert-danger">No tiene permisos para visualizar esta planilla.</div>';
       }

       $producto = $planilla3->declaracionProduc->rProducto;
       $costos_directos= $planilla3->costosDirectos;
       $costos_indirectos= $planilla3->costosInDirectos;
       $divisa= $planilla3->GenDivisa;

        if($layout)
            return view('djoplanilla3.show',compact('layout','planilla3','producto','costos_directos','costos_indirectos','divisa'));
        else
            return View::make('djoplanilla3.show',compact('layout','planilla3','producto','costos_directos','costos_indirectos','divisa'))->renderSections()['content'];
    }

     /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // get the planilla3
        $planilla3 = Planilla3::find($id);
        $det_declaracion_product = $planilla3->declaracionProduc;
        $divisas=GenDivisa::whereIn('id',[18,32])->pluck('ddivisa_abr','id');
        
        // show the edit form and pass the planilla3
        return View::make('djoplanilla3.edit',compact('planilla3','det_declaracion_product','divisas'));
    }

     /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(DJOPlanilla3Request $request, $id)
    {
    
      $planilla3 = Planilla3::find($id);
      $productos_id= $planilla3->declaracionProduc->rProducto->id;
      $det_declaracion_product_id = $planilla3->declaracionProduc->id;

      $descripcion_comercial= $request->descripcion_comercial;
      $divisa= $request->divisa;
      $total_importado= $request->total_importado;
      $total_nacionales= $request->total_nacionales;

      $mano_obra= $request->mano_obra;
      $depreciacion= $request->depreciacion;
      $otros_costos= $request->otros_costos;
      $total_costos_directos= $request->total_costos_directos;
      $gastos_administrativos= $request->gastos_administrativos;
      $gastos_publicidad= $request->gastos_publicidad;
      $gastos_ventas= $request->gastos_ventas;
      $tros_gastos= $request->tros_gastos;
      $total_gastos_indirectos= $request->total_gastos_indirectos;

      $utilidad= $request->utilidad;
      $precio_fabrica= $request->precio_fabrica;
      $flete_interno= $request->flete_interno;
      $precio_fob= $request->precio_fob;
      $datosverificados="";

        if(!empty($datosverificados)){

             Alert::warning('Verificar los datos.')->persistent("OK");

            return redirect()->back()->withInput();
        }

       
       $planilla3->det_declaracion_produc_id= $det_declaracion_product_id;
       $planilla3->gen_divisa_id= $divisa;
       $planilla3->utilidad = $utilidad;
       $planilla3->precio_puerta_fabri = $precio_fabrica;
       $planilla3->flete_interno = $flete_interno;
       $planilla3->precio_fob_export = $precio_fob;
       $planilla3->bactivo=1;
       $planilla3->estado_observacion=0;

       if ($planilla3->touch())
       {

          /* costos directos */
          $modelCostoDirectos =  $planilla3->costosDirectos;
          $modelCostoDirectos->mate_importados=$total_importado;
          $modelCostoDirectos->mate_nacional=$total_nacionales;
          $modelCostoDirectos->mano_obra=$mano_obra;
          $modelCostoDirectos->depreciacion=$depreciacion;
          $modelCostoDirectos->otros_costos_dir=$otros_costos;
          $modelCostoDirectos->total=$total_costos_directos;
          $modelCostoDirectos->bactivo=1;
          /* validacion */
          if($modelCostoDirectos->touch())
          {
             //dd($modelCostoDirectos);
             Alert::warning('Inserto Costos Directos!!')->persistent("OK");
          }
          /* ******************* */


          /* costos indirectos */
          $modelCostoInDirectos = $planilla3->costosInDirectos;
          $modelCostoInDirectos->gastos_admin=$gastos_administrativos;
          $modelCostoInDirectos->gastos_publicidad=$gastos_publicidad;
          $modelCostoInDirectos->gastos_ventas=$gastos_ventas;
          $modelCostoInDirectos->otros_gastos_indi=$tros_gastos;
          $modelCostoInDirectos->total=$total_gastos_indirectos;
          $modelCostoInDirectos->bactivo=1;
            /* validadcion */
            if($modelCostoInDirectos->touch())
            {
              
              Alert::warning('Inserto Costos In Directos!!')->persistent("OK");

              /*return redirect()->back()->withInput();*/
            }
          /* ******************* */

          /* actualizar estatus de planilla3 */
           
            $detDeclaracionProducModel = $planilla3->declaracionProduc;
            $detDeclaracionProducModel->estado_p3=1;
            $detDeclaracionProducModel->touch();
             Alert::success('Se han actualizado los datos correspondientes a la Planilla 3!','Actualizacion Exitosa!')->persistent("Ok");
            
            return redirect()->action('DeclaracionJOController@index');
          /* ******************* */
       }
    }

    /** Para consultar las observaciones mediante ajax **/
    /**
     * Se recibe como parametro el $id de la planilla
     * 
     */
    public function obtenerObservacion(Request $request,$id=null){

        if(!empty($request->det_declaracion_id)){
            $detDeclaracion= DetDeclaracionProduc::with('rPlanilla3')->find($request->det_declaracion_id);
            $planilla3= $detDeclaracion->rPlanilla3;
        }else{
        $planilla3 = Planilla3::find($id);
        }
        return Response::json(array('observacion'=>$planilla3->descrip_observacion));
        
    }

      /** Para guardar las observaciones mediante ajax **/
    /**
     * Se recibe como parametro el $id de la planilla
     * 
     */
    public function storeObservacion(Request $request,$id=null){

        
        if(!empty($request->det_declaracion_id)){
            $detDeclaracion= DetDeclaracionProduc::with('rPlanilla3')->find($request->det_declaracion_id);
            $planilla3= $detDeclaracion->rPlanilla3;
        }else{
            $planilla3 = Planilla3::find($id);
        }
        if(!empty($request->observacion)){
            $planilla3->descrip_observacion=$request->observacion;
            $planilla3->estado_observacion=1;
        }else{
            $planilla3->descrip_observacion=null;
            $planilla3->estado_observacion=0;
        }

        if($planilla3->touch())
            return Response::json(array('respuesta'=>'Solicitud procesada con exito','estado'=>1));
        else
            return Response::json(array('respuesta'=>'La solicitud no pudo ser procesada','estado'=>0));
        
    }
}
