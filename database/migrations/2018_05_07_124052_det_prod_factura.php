->nullable()<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DetProdFactura extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('det_prod_factura', function (Blueprint $table) {
           $table->engine = 'InnoDB';
           $table->increments('id');
           $table->unsignedInteger('gen_factura_id')->index()->nullable();
           $table->foreign('gen_factura_id')->references('id')->on('gen_factura');
           $table->unsignedInteger('unidad_medida_id')->index()->nullable();
           $table->foreign('unidad_medida_id')->references('id')->on('gen_unidad_medida');

           $table->string('codigo_arancel', 100)->nullable();
           $table->string('tipo_arancel', 100)->nullable();
           $table->string('descripcion_arancelaria', 100)->nullable();
           $table->string('descripcion_comercial', 100)->nullable();
           $table->double('cantidad_producto')->nullable();
           $table->double('precio_producto')->nullable();
           $table->integer('disponibles')->nullable();
           $table->double('monto_total')->nullable();
           $table->unsignedInteger('permiso_id')->index()->nullable();
           $table->foreign('permiso_id')->references('id')->on('permiso');
           $table->boolean('bactivo')->default(1);
           $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('det_prod_factura');
    }
}
