<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app['validator']->extend('min_items_coma', function($attribute, $value, $parameters) {

            $data = $value;
            
            $dataExplode = explode(",",$data);
            if(empty($dataExplode[0])){
                return true;
            }
            
            return count($dataExplode) >= $parameters[0];
        });

        $this->app['validator']->replacer('min_items_coma', function($message, $attribute, $rule, $parameters){
            return str_replace(':min', $parameters[0], $message);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
