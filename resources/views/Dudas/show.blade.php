@extends('templates/layoutlte')

@section('content')
{!!Form::open(['route'=>'Dudas.store','method'=>'POST','id'=>'dudas']) !!}
@php
  $motivo_contacto = ['Atención al Exportador', 'Soporte al Usuario'];
@endphp
<div class="panels">
  <div class="">
    <div class="row"><br>
      <div class="col-md-12">
        <div class="col-md-2"></div>
        <div class="col-md-4">
          <div class="form-group">
            {!! Form::label('','Nombre o Razón Social') !!}
            {!! Form::text('razon_social',$datosempresa->razon_social,['class'=>'form-control','id'=>'razon_social','onkeypress'=>'return soloLetras(event)','maxlength'=>'40', 'disabled']) !!}
          </div> 
          <div class="form-group">
            {!! Form::label('', 'Correo Electrónico') !!}
            {!! Form::email('correo', $datosempresa->correo, ['class' => 'form-control','id'=>'correo', 'disabled']) !!}
          </div> 
        </div><!--1era columna de 4-->
        <div class="col-md-4">
          <div class="form-group">
          {!! Form::label('','Teléfono') !!}
          {!! Form::text('telefono_movil',$datosempresa->telefono_movil,['class'=>'form-control','id'=>'telefono_movil','onkeypress'=>'return soloNumeros(event)','maxlength'=>'12', 'disabled']) !!}
          </div> 
          <div class="form-group">
            <label>Motivo de Contacto</label>
            {!! Form::text('tipo',$asistencia_usuario['motivo_de_contacto'],['class'=>'form-control','id'=>'tipo','onkeypress'=>'return soloNumeros(event)','maxlength'=>'12', 'disabled']) !!}  
          </div> 
        </div><!-- 2da columna de 4-->
        <div class="col-md-2"></div>
      </div>    
    </div>

    <div class="row">
      <div class="col-md-12 text-center">
      <div class="col-md-2"></div>
        <div class="col-md-8">
          <div class="form-group">
            {!! Form::label('','Mensaje') !!}
            {!! Form::textarea('mensaje_registrado',$asistencia_usuario['mensaje'],['class'=>'form-control','id'=>'mensaje_registrado','onkeypress'=>'return soloNumerosyLetras(event)','maxlength'=>'500', 'required'=>'true', 'disabled']) !!}
          </div>  
        </div>
        <div class="col-md-2"></div>
      </div> 
    </div>

    
    <div class="row">
      <div class="col-md-12 text-center">
      <div class="col-md-2"></div>
        <div class="col-md-8">
          <div class="form-group">
            @if(!empty($asistencia_usuario['respuesta']))
              {!! Form::label('','Respuesta') !!}
              {!! Form::textarea('respuesta',$asistencia_usuario['respuesta'],['class'=>'form-control','id'=>'respuesta','onkeypress'=>'return soloNumerosyLetras(event)','maxlength'=>'500', 'required'=>'true', 'disabled']) !!}
            @else
              {!! Form::label('','Sin Respuesta') !!}
            @endif
          </div>  
        </div>
        <div class="col-md-2"></div>
      </div> 
    </div>
    

    <div class="row">
        <div class="col-md-12">
        <div class="col-md-4"></div>
            <div class="col-md-4 text-center">
              <div class="form-group">
                <a href="{{url('/exportador/Dudas')}}" class="btn btn-primary">Volver</a>
                <!-- <input class="btn btn-success" type="submit" name="" value="Enviar" onclick="enviardua()"> -->
            </div>
        </div>
        <div class="col-md-4"></div>
        </div>
    </div>

  </div>
</div>

{!! Form::hidden('gen_usuario_id', $datosempresa->gen_usuario_id)!!}

{{Form::close()}}


<script>
    $('#mensaje_registrado, #respuesta_registrada').prop('disabled', true);
</script>
@stop
