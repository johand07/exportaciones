@extends('templates/layoutlte')
@section('content')
<div class="panels">
	<div class="row">
		<div class="col-md-12">
			<div class="row">
				<div class="col-md-4 text-left">
					<a class="btn btn-primary" href="{{route('permisoEspecialBovino.create')}}">
						<span class="glyphicon glyphicon-file" title="Agregar" aria-hidden="true"></span>
						Registrar Autorización Bovino
					</a>
				</div>
				<div class="col-md-8"></div>
			</div>
			<br><br>
			<table id="listaDebito" class="table table-striped table-bordered" style="width:100%">
				<thead>
					<tr>

						<th>Volumen</th>
						<th>Puerto de Salida</th>
						<th>Puerto de Entrada</th>
						<th>Fecha de Embarque</th>
						<th>Fecha Valides</th>
						<th>Fecha Creacion</th>
						<th>Acciones</th>
					</tr>
				</thead>
				<tbody>
					@if(isset($permisosBovinos))
						@foreach($permisosBovinos as  $permiso)
						<tr>                
							<td>{{$permiso->volumen}}</td>   
							<td>{{$permiso->puerto_salida}}</td>
							<td>{{$permiso->puerto_entrada}}</td>
							<td>{{$permiso->fembarque_bovino}}</td>
							<td>{{$permiso->fvalido}}</td>
							<td>{{$permiso->created_at}}</td>
							<td>
								<a href="{{action('PdfController@AutorizacionEspecialBovino',array('id'=>$permiso->id))}}" class="glyphicon glyphicon-file btn btn-success btn-sm" target="_blank" title="Autorización Especial de Exportación" aria-hidden="true">Autorización</a>  


							</td>
						</tr>
						@endforeach
					@endif
				</tbody>
			</table>
		</div>
	</div>
</div>
@stop