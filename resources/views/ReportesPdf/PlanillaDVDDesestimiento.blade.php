<html>
  <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <title>DVD</title>
      <style>
          body{
              text-align:center;
             font-family: Arial;
                   color: #58585A;
          }
          th{
              font-size:10px;
              background-color:#818286;
              color: #fff;
          }
          td{
              font-size:10px;
              border: 1px solid #818286;
          }

          .p{
              border: 0px solid #fff;
          }

          .td2{
              font-size:10px;
              border: 0px solid #818286;
              width: 55%;
              text-align:center;
              font-size:14px;
          }
          .td3{
              
              border: 0px solid #818286;
              width: 15%;
              text-align:center;
              
          }
          .td4{
              
              border: 0px solid #818286;
              
              
          }
          table{
              border: 1px solid #818286;
          }
          .table2{
              border: 0px solid #818286;
          }

         table > tr > td {

            text-align: center;
          }
        .tabla > h4,h6{

          text-align: left;
          margin-bottom: 0;
          font-size:10px;

        }
        .tabla > table > thead > tr > th:nth-child(1){

           width: 30%;
        }
        .tabla > table > .tipo > tr:nth-child(1){

          border-top: 0;
        }

        .tabla2 > table > thead > tr > th:nth-child(0){

           width: 10%;
        }

      </style>
  </head>
      <body>          
        <div class="">
          <table style="width:100%;" class="table2">
            <tbody>
              <tr>
                <td class="td3">
                  
                  <img class="img-responsive" width="165px" height="115px" src='img/MIPPCOEXIN.png'>
                </td>
                <td class="td2">
                  <h3>Desistimiento de Ventas de Divisas al BCV<br> Provenientes de las Exportaciones</h3>
                </td>
                <td class="td4">
                  <table  style="width:100%">
                    <thead>
                      <tr>
                        <th>1) N° EXPORTACIÓN</th>
                        
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>{{$gen_solicitud->gen_solicitud_id}}</td>
                        
                      </tr>
                    </tbody>
                  </table>
                  <table  style="width:100%">
                    <thead>
                      <tr>
                        <th>2) FECHA/HORA</th> 
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>{{$gen_solicitud->created_at}}</td>
                        
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
              
            </tbody>
          </table>                    
        </div>

         <div class="tabla">
         <div align="left">
          <span style="font-size:12px"><b>Datos del Exportador</b></span>
          <span style="margin-left:180px;">Código de Seguridad</span>
          <p style="font-size:10px;" align="center">{{$gen_solicitud->cod_seguridad}}</p>
        </div>
            <table  style="width:100%">
              <thead>
                <tr>
                  <th>3) NÚMERO DE RIF</th>
                  <th>4) NOMBRE O RAZÓN SOCIAL</th>
                  <th>5) SIGLAS REGISTRADAS </th>
                </tr>
              </thead>
             <tbody>
                <tr>
                  <td>{{$users->rif}}</td>
                  <td>{{$users->razon_social}}</td>
                  <td>{{$users->siglas}}</td>
                </tr>
              </tbody>
            </table>
        </div><br>

        <div class="tabla">
          <h4>DESCRIPCIÓN DE LA EXPORTACIÓN</h4>
        </div>
        <table  style="width:100%">
          <thead>
            <tr>
              <th>DECLARACIÓN JURADA DEL EXPORTADOR</th>
            </tr>
          </thead>
          <tbody>
            <tr>
            <td align="justify">
            <p><br>OBSERVACIONES:</p>           
            <p><br>{{$gen_solicitud->descripcion}}</p><br>
            </td>
          </tr>
          </tbody>
        </table>
      </body>
</html>