@extends('templates/layoutlte')
@section('content')


!-- Button trigger modal -->
<button type="button" class="btn btn-primary btn-lg btn-view-planillas" data-toggle="modal" data-target="#modalPlanillas" data-declaracion-product="4">Producto 1</button>
<button type="button" class="btn btn-primary btn-lg btn-view-planillas" data-toggle="modal" data-target="#modalPlanillas" data-declaracion-product="7">Producto 2</button>
<button type="button" class="btn btn-primary btn-lg btn-view-planillas" data-toggle="modal" data-target="#modalPlanillas" data-declaracion-product="5">Producto 3</button>
<button type="button" class="btn btn-primary btn-lg btn-view-planillas" data-toggle="modal" data-target="#modalPlanillas" data-declaracion-product="4">Producto 4</button>
<button type="button" class="btn btn-primary btn-lg btn-view-planillas" data-toggle="modal" data-target="#modalPlanillas" data-declaracion-product="4">Producto 5</button>
<button type="button" class="btn btn-primary btn-lg btn-view-planillas" data-toggle="modal" data-target="#modalPlanillas" data-declaracion-product="4">Producto 6</button>
<button type="button" class="btn btn-primary btn-lg btn-view-planillas" data-toggle="modal" data-target="#modalPlanillas" data-declaracion-product="4">Producto 7</button>
<button type="button" class="btn btn-primary btn-lg btn-view-planillas" data-toggle="modal" data-target="#modalPlanillas" data-declaracion-product="4">Producto 8</button>

<!-- Modal -->
<div class="modal fade" id="modalPlanillas" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>

                </button>
                 <h4 class="modal-title" id="myModalLabel">Planillas Declaracion Jurada de Origen</h4>

            </div>
            <div class="modal-body">
                <div role="tabpanel">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#p2" class="cargador-planilla" aria-controls="p2" role="tab" data-toggle="tab" id="tab-inicial">Planilla 2</a>
                        </li>
                        <li role="presentation"><a href="#p3" class="cargador-planilla" aria-controls="p3" role="tab" data-toggle="tab">Planilla 3</a>
                        </li>
                        <li role="presentation"><a href="#p4" class="cargador-planilla" aria-controls="p4" role="tab" data-toggle="tab">Planilla 4</a>
                        </li>
                        <li role="presentation"><a href="#p5" class="cargador-planilla" aria-controls="p5" role="tab" data-toggle="tab">Planilla 5</a>
                        </li>
                        <li role="presentation"><a href="#p6" class="cargador-planilla" aria-controls="p6" role="tab" data-toggle="tab">Planilla 6</a>
                        </li>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="p2">p2</div>
                        <div role="tabpanel" class="tab-pane" id="p3">p3</div>
                        <div role="tabpanel" class="tab-pane" id="p4">p4</div>
                        <div role="tabpanel" class="tab-pane" id="p5">p5</div>
                        <div role="tabpanel" class="tab-pane" id="p6">p6</div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar ventana</button>
            </div>
        </div>
    </div>
</div>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script type="text/javascript">

$(document).ready(function(){
    /** Escuchando evento click en los elementos que contengan la clase btn-view-planillas 
     *  donde se creara una variable local que contendra el id de det declaracion produc
     *  adicionalmente se induce el evento click en la pestaña inicial de los tabs**/
    $(document).on('click','.btn-view-planillas',function(){
        var det_declaracion_id = this.getAttribute('data-declaracion-product');
        localStorage.setItem("det_declaracion_id","");
        
        if(!det_declaracion_id==false){
            localStorage.setItem("det_declaracion_id",det_declaracion_id);
        }

        document.getElementById('tab-inicial').click();
    });

/** Escuchando evento click en los elementos que contengan la clase cargador-planilla
 *  donde se obtienen el nombre del ancla (id_href) del enlace y el nombre la planilla a mostrar
 *  consulta via ajax de la visual de la planilla correspondiente y la carga de los datos recibidos en
 * el contenedor especifico **/
    $(document).on('click','.cargador-planilla',function(){
        var id_href = this.getAttribute('href');
        var nombre = this.getAttribute('aria-controls');
        var det_declaracion_id= localStorage.getItem("det_declaracion_id");

        if(!id_href==false && !nombre==false){
            var base= document.location.origin;
            var url= '/exportador/';
         
            if(nombre=="p2"){
                url+='DJOPlanilla2/show';
            }else if(nombre=="p3"){
                url+='DJOPlanilla3/show';
            }else if(nombre=="p4"){
                url+='DJOPlanilla4/show';
            }else if(nombre=="p5"){
                url+='DJOPlanilla5/show';
            }else if(nombre=="p6"){
                url+='DJOPlanilla6/show';
            }else{
                url+='unknow';
            }

            var datos_enviados = {
                'det_declaracion_id' : det_declaracion_id,
                'layout' : false
            }

            var request = $.ajax({
            url: base + url ,
            method: "GET",
            data: datos_enviados,
            dataType: "html"
            });


            request.done(function( data ) {
                $(id_href).html(data);
            });

            request.fail(function( jqXHR, textStatus ) {
            alert( "Hubo un error: " + textStatus );
            });
        }
    });


});


</script>

@endsection