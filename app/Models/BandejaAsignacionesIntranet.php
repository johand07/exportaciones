<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BandejaAsignacionesIntranet extends Model
{
    protected $table = 'bandeja_asignaciones_intranet';



    protected $fillable = [

           'id',
           'gen_solicitud_id',
           'gen_usuario_id',
           'gen_status_id',
           'fstatus',
           'asignado_por',
           'analizado_por',
           'finalizado_por',
           'bactivo',
    ];

    protected $date =['created_at','updated_at'];

    public function rGenSolicitud() {

     return $this->hasOne('App\Models\GenSolicitud','id','gen_solicitud_id');

    }

    public function rGenStatus() {

     return $this->hasOne('App\Models\GenStatus','id','gen_status_id');

    }

     public function rGenUsuario() {

     return $this->hasOne('App\Models\GenUsuario','id','gen_usuario_id');

    }
}
