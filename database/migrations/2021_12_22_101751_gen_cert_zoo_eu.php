<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GenCertZooEu extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gen_cert_zoo_eu', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('gen_usuario_id');
            $table->foreign('gen_usuario_id', 'ge_u_i_fk1')->references('id')->on('gen_usuario');
            $table->string('num_ref_cert',150)->nullable();
            $table->string('pais_export',150)->default('venezuela');
            $table->string('cod_iso_pais_export',150)->default('VE');
            $table->string('nombre_import',150)->nullable();
            $table->text('direccion_import')->nullable();
            $table->string('pais_import',150)->nullable();
            $table->string('cod_iso_pais_inport',150)->nullable();
            $table->string('nombre_oper_respons',150)->nullable();
            $table->text('direccion_oper_respons')->nullable();
            $table->string('pais_oper_respons',150)->nullable();
            $table->string('cod_iso_pais_oper_respons',150)->nullable();
            $table->string('pais_origen',150)->nullable();
            $table->string('cod_iso_pais_origen',150)->nullable();
            $table->string('pais_destino',150)->nullable();
            $table->string('cod_iso_pais_destino',150)->nullable();
            $table->string('region_origen',150)->nullable();
            $table->string('cod_reg_origen',150)->nullable();
            $table->string('region_destino',150)->nullable();
            $table->string('cod_reg_dest',150)->nullable();
            $table->string('nombre_lug_exp',150)->nullable();
            $table->string('num_reg_exp',150)->nullable();
            $table->string('dir_lug_exp',150)->nullable();
            $table->string('pais_lug_exp',150)->nullable();
            $table->string('cod_iso_lug_exp',150)->nullable();
            $table->string('nombre_lug_dest',150)->nullable();
            $table->string('num_reg_lug_dest',150)->nullable();
            $table->text('dir_lug_dest')->nullable();
            $table->string('pais_lug_dest',150)->nullable();
            $table->string('cod_iso_lug_dest',150)->nullable();
            $table->text('lug_carga')->nullable();
            $table->timestamp('fecha_hora_salida');
            $table->unsignedInteger('cat_medio_transport_id');
            $table->foreign('cat_medio_transport_id')->references('id')->on('cat_medio_transport');
            $table->string('puest_control_front',150)->nullable();
            $table->string('tipo_doc_acomp',150)->nullable();
            $table->string('cod_doc_acomp',150)->nullable();
            $table->string('pais_doc_acomp',150)->nullable();
            $table->string('cod_iso_doc_acomp',150)->nullable();
            $table->string('ref_doc_comer',150)->nullable();
            $table->string('ident_medio_transport',150)->nullable();
            $table->unsignedInteger('cat_cond_transport_id');
            $table->foreign('cat_cond_transport_id')->references('id')->on('cat_cond_transport');
            $table->integer('num_recipiente')->nullable();
            $table->integer('num_precinto')->nullable();
            $table->unsignedInteger('cat_certificado_efectos_de_id');
            $table->foreign('cat_certificado_efectos_de_id')->references('id')->on('cat_certificado_efectos_de');
            /*$table->unsignedInteger('descrip_partida__zoo_id');
            $table->foreign('descrip_partida__zoo_id')->references('id')->on('descrip_partida_zoo_eu');*/
            $table->string('para_merc_interior',150)->nullable();
            $table->float('num_total_bultos')->nullable();
            $table->float('cant_total')->nullable();
            $table->float('peso_neto_pn')->nullable();
            $table->float('peso_bruto')->nullable();
            $table->boolean('bactivo')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gen_cert_zoo_eu');
    }
}
