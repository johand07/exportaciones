<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PlanillaDjir_03 extends Model
{
    protected $table = 'planilla_djir03';
    
    protected $fillable = [
        'gen_declaracion_inversion_id',
        'inversion_recursos_externos',
        'det_invert_recursos',
        'proyecto_nuevo',
        'nombre_proyecto',
        'ubicacion',
        'pais_id',
        'ampliacion_actual_emp',
        'ampliacion_accionaria_dir',
        'utilidad_reinvertida',
        'credito_casa_matriz',
        'creditos_terceros',
        'otra_dir',
        'especifique_directa',
        'participacion_accionaria_cart',
        'bonos_pagare',
        'otra_cart',
        'periodo_inversion',
        'gen_status_id',
        'estado_observacion',
        'descrip_observacion',
    ];

    public function genDecInversion()
	{
        return $this->hasOne('App\Models\GenDeclaracionInversion','id','gen_declaracion_inversion_id');
    }

    public function genStatus()
	{
        return $this->hasOne('App\Models\GenStatus','id','gen_status_id');
    }

    public function pais()
	{
        return $this->hasOne('App\Models\Pais','id','pais_id');
    }
}
