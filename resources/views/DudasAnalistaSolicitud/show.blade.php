@extends('templates/layoutlte_analist_djo')

@section('content')
{!!Form::open(['route'=> ['DudasAnalistaSolicitud.update', $solicitud->id], 'method'=>'PUT','id'=>'dudas' ]) !!}
@php
  $motivo_contacto = ['Atención al Exportador', 'Soporte al Usuario'];
@endphp

<div class="panels">
    <div class="row"><br>
      <div class="col-md-12">
        <div class="col-md-2"></div>
        <div class="col-md-4">
          <div class="form-group">
            {!! Form::label('','Nombre o Razón Social') !!}
            {!! Form::text('razon_social',$solicitud->razon_social,['class'=>'form-control','id'=>'razon_social','onkeypress'=>'return soloLetras(event)','maxlength'=>'40', 'disabled']) !!}
          </div> 
          <div class="form-group">
            {!! Form::label('', 'Correo Electrónico') !!}
            {!! Form::email('correo', $solicitud->correo, ['class' => 'form-control','id'=>'correo', 'disabled']) !!}
          </div> 
        </div><!--1era columna de 4-->
        <div class="col-md-4">
          <div class="form-group">
          {!! Form::label('','Teléfono') !!}
          {!! Form::text('telefono_movil',$solicitud->telefono_movil,['class'=>'form-control','id'=>'telefono_movil','onkeypress'=>'return soloNumeros(event)','maxlength'=>'12', 'disabled']) !!}
          </div> 
          <div class="form-group">
            <label>Motivo de Contacto</label>
            {!! Form::text('tipo',$solicitud->motivo_de_contacto,['class'=>'form-control','id'=>'tipo','onkeypress'=>'return soloNumeros(event)','maxlength'=>'12', 'disabled']) !!}
          </div> 
        </div><!-- 2da columna de 4-->
        <div class="col-md-2"></div>
      </div>    
    </div>

    <div class="row">
      <div class="col-md-12 text-center">
      <div class="col-md-2"></div>
        <div class="col-md-8">
          <div class="form-group">
            {!! Form::label('','Duda del Exportador') !!}
            {!! Form::textarea('mensaje',$solicitud->mensaje,['class'=>'form-control','id'=>'mensaje_show','onkeypress'=>'return soloNumerosyLetras(event)','maxlength'=>'500', 'required'=>'true', 'disabled']) !!}
          </div>  
        </div>
        <div class="col-md-2"></div>
      </div> 
    </div>

    <div class="row">
      <div class="col-md-12 text-center">
      <div class="col-md-2"></div>
        <div class="col-md-8">
          <div class="form-group">
              @if($solicitud->status == 10)
                  {!! Form::label('','Respuesta') !!}
                  {!! Form::textarea('respuesta_analista',$solicitud->respuesta,['class'=>'form-control','id'=>'respuesta_show','onkeypress'=>'return soloNumerosyLetras(event)','maxlength'=>'500', 'required'=>'true']) !!}
              @endif
          </div>  
        </div>
        <div class="col-md-2"></div>
      </div> 
    </div>
    {!! Form::hidden('gen_asistencia_usuario_id', $solicitud->id)!!}
    <div class="row">
        <div class="col-md-12">
        <div class="col-md-4"></div>
            <div class="col-md-4 text-center">
              <div class="form-group row">
                  <a href="{{url('/calificaciondjo/DudasAnalistaSolicitud')}}" class="btn btn-primary">Volver</a>
                @if($solicitud->status == 9)
                  <a href="{{ route('DudasAnalistaSolicitud.edit',$solicitud->id) }}" class="btn btn-danger"><i class="glyphicon glyphicon-share"></i> <b>Atender</b></a>
                @elseif($solicitud->status == 10)
                  <input class="btn btn-success" type="button" id="actualizar_respuesta" name="actualizar_respuesta" data-id="{{$solicitud->id}}" value="Actualizar">
                @endif
            </div>
        </div>
        <div class="col-md-4"></div>
        </div>
    </div>
</div>

{!! Form::hidden('gen_usuario_atencion_id', Auth::user()->id)!!}


{{Form::close()}}

<script>
  
</script>
@stop
