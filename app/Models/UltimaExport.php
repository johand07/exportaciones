<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UltimaExport extends Model
{
     protected $table = 'ultima_export';

	protected $fillable = [

           'gen_usuario_id',
           'pais_id',
            'fecha_ult_export',
           'bactivo',
    ];

    protected $date =['created_at','updated_at'];

    public function usuarios() {

     return $this->hasOne('App\Models\GenUsuario','id','gen_usuario_id');

    }
}
