<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CapitalSocialAccionistas extends Model
{
    protected $table = 'capital_social_accionista';

    protected $fillable = [
    	'gen_sol_inversion_id',
    	'accionistas_id',
    	'capital_social',
    	'bactivo',
    ];
    
    protected $date =['created_at','updated_at'];

    public function inversion(){
    	return $this->hasOne('App\Models\GenSolInversion','id','gen_sol_inversion_id');
    }

    public function accionista() {

     return $this->hasOne('App\Models\Accionistas','id','accionistas_id');

    }
}
