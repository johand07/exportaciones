<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Er</title>
    <style>
      header {
                position: fixed;
                top: -30px;
                left: 0px;
                right: 0px;
                height: 50px;

                /** Extra personal styles **/
                /*background-color: #03a9f4;
                color: white;
                text-align: center;
                line-height: 35px;*/
            }
        body{
            text-align:center;
           font-family: Arial;
                 color: #000;
        }
        th{
            font-size:10px;
            background-color:#818286;
            color: #fff;
        }
        td{
            font-size:10px;
            border: 1px solid #818286;
        }
        .td2{
            font-size:10px;
            border: 0px solid #818286;
            width: 55%;
            text-align:center;
            font-size:14px;
        }
        .td3{

            border: 0px solid #818286;
            width: 15%;
            text-align:center;

        }
        .td4{

            border: 0px solid #818286;


        }
        table{
            border: 1px solid #818286;
        }
        .table2{
            border: 0px solid #818286;
        }

       table > tr > td {

          text-align: center;
        }
      .tabla > h4,h6{

        text-align: left;
        margin-bottom: 0;
        font-size:10px;

      }
      .tabla > table > thead > tr > th:nth-child(1){

         width: 30%;
      }
      .tabla > table > .tipo > tr:nth-child(1){

        border-top: 0;
      }

      .tabla2 > table > thead > tr > th:nth-child(0){

         width: 10%;
      }
       .centrar{
            text-align: center;
        }

        .titulo{
            
            font-weight: bold;
        }
         .derecha{
            text-align: left;

        }

         .borde{
            border: 1px solid #fff;
        }
        

    </style>
</head>
    <body>
  <table style="width:100%;" class="table2 borde">
          <tbody>
            <tr>
              <td class="td3 borde">
                <img class="img-responsive" style="width:200px; height: 40px" src='img/MIPPCOEXIN.png'> 
              </td>
              <td class="borde"></td>
              <td class="borde"></td>
               <td>
                  <img class="img-responsive" style="width: 300px; height: 40px color:#ffff;" src='img/cintillo.png'>
              </td>
            </tr>
          </tbody>
        </table>
    <div>
      <table style="width:100%;" class="table2">
          <tr align="text-left">
            <td class="td3">
                  <img src="data:image/png;base64,{!! base64_encode(QrCode::format('png')->size(200)->generate(URL::to('/api/VerificacionCertificadoInversionista/'.encrypt($solicitud_de_inversionista->num_sol_inversionista)))) !!}" width="160px" height="145px">
            </td>
            <td class="borde"></td>
            <td class="borde"></td>
            <td class="borde"></td>
            <td class="borde">
             <table>
               <tr>
                <td class="titulo left" align="left"><b>Nº  Tramite: RI-{{$solicitud_de_inversionista->num_sol_inversionista}}</b></td>
               </tr>
               <tr>
                <td class="titulo left" align="left"><b>Fecha expiración: {{$solicitud_de_inversionista->fecha_expiracion}}</b></td>
               </tr>
             </table>
         </td>
          </tr> 
      </table>
    </div>
    <div class="centrar titulo text-normal espacio-superior"><b style="font-size:18px; font-family: Arial background:color:#000">SISTEMA ÚNICO DE REGISTRO DE INVERSIÓN</b></div>
<p align="justify" style="font-size:16px;">El Ministerio del Poder Popular de Economía, Finanzas y Comercio Exterior, a través del Despacho de Viceministro para el Comercio Exterior y Promoción de Inversiones, de conformidad con lo establecido en la Ley Constitucional de Inversión Extranjera Productiva, publicada en la Gaceta Oficial de la República Bolivariana de Venezuela N° 41.310, del 29 de diciembre de 2017, certifica que:</p>
<div class="tabla">
  <table  style="width:100%">
    <thead>
      <tr>
        <th style="width:50%">4) NOMBRE O RAZÓN SOCIAL</th>
        <th style="width:50%">5) PAÍS DE ORIGEN </th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>{{ucwords($users->razon_social)}}</td>
        <td>{{$solicitud_de_inversionista->pais->dpais}}</td>
      </tr>
    </tbody>
  </table>
  <table  style="width:100%">
    <thead>
      <tr>
        <th style="width:50%">Nombre del Representante Legal</th>
        <th style="width:50%">N° Doc. de Identidad </th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>{{$solicitud_de_inversionista->nombre_repre_legal}}  {{$solicitud_de_inversionista->apellido_repre_legal}}</td>
        <td>{{$solicitud_de_inversionista->n_doc_identidad}}</td>
      </tr>
    </tbody>
  </table>
  <table  style="width:100%">
    <thead>
    <tr>
        <th style="width:50%">Domicilio</th>
        <th style="width:25%">N° de Teléfono</th>
        <th style="width:25%">Correo Electrónico</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>{{$solicitud_de_inversionista->DetUsuario->direccion}}</td>
        <td>{{$solicitud_de_inversionista->DetUsuario->telefono_movil}}</td>
        <td>{{$solicitud_de_inversionista->DetUsuario->correo}}</td>
      </tr>
    </tbody>
  </table>
   <div class="justify">
        <table class="" style="width:100%">
            <tr class="text-normal">
                <td class="" style="font-size:14px;">Ha manifestado su intención de invertir en la República Bolivariana de Venezuela y reúne condiciones para ser catalogado como: <br><br></td>
            </tr>

            <tr>
                <td class="centrar" style="font-size:16px;"><b>POTENCIAL INVERSIONISTA EXTRANJERO <br></b></td>
            </tr>
            <tr>
                <td class="justify" style="font-size:14px;"><p>Con este certificado el prenombrado se considera inscrito en el Sistema Único de Registro de Inversión bajo la categoría ya señalada, y le permitirá a su titular realizar actos y negocios jurídicos, comerciales, financieros o actividades lucrativas legales en la República Bolivariana de Venezuela, por un período de un (01) año. 
                </p>

                <p class="justify">Con la emisión del presente documento, el interesado deberá efectuar en un plazo  no mayor de un (01) año la correspondiente <b>Declaración Jurada de Inversión Realizada</b> a los efectos de obtener posteriormente el respectivo <b>Certificado de Inversionista Extranjero</b>, todo ello de conformidad con lo establecido en  el artículo 37 de la Ley Constitucional de Inversión Extranjera Productiva, y con ello poder optar a los demás derechos y beneficios consagrados en el referido instrumento normativo.</p><br></b>
                </td>
            </tr>
        </table>
        <table style="width:100%">
          <tr>
              <td class="centrar" style="width:40%">
              <h4><u>Caracas {{$Fecha}}</u></h4>
                    <br>Lugar y fecha
              </td><br>
              <br>
              <td class="centrar">

                                     <b>{{$datosfirmante->nombre_firmante}} {{$datosfirmante->apellido_firmante}}</b>
                <p class="centrar"><b>{{$datosfirmante->cargo_firmante}}</b></p>
                <p class="centrar"><b>{{$datosfirmante->gaceta_firmante}}</b></p>
              </td>
            </tr>
        </table>
        <div class="justify" style="font-size:12px;" align="text-justify">Este certificado tendrá una vigencia de un (1) año a partir de la fecha de su emisión.
        Este certificado se expide sobre la base de la información y los recaudos suministrados por su solicitante, cualquier modificación o alteración de la misma, dejará sin efecto el presente acto administrativo y deberá ser denunciado ante las autoridades correspondientes.</div>
    </div>
    </div>
</div>

    


      
     

      



