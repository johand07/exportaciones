@extends('templates/layoutlte_admin')

@section('content')
<br><br><br>
<div class="container">
  <div class="row">
    <div class="col-md-2"></div>
    <div class="col-md-8">

{{Form::open(['route'=>'enviarNotificacionesEmail','method'=>'POST','id'=>'notificaciones']) }}
         <div class="form-group">
            {!! Form::label('','Seleccione el Correo que desea Enviar:') !!}
            {!!Form::select('eventos_id',$eventos,null,['class'=>'form-control', 'id'=>'eventos_id','placeholder'=>'Seleccione una opcion']) !!}
          </div>

        <div class="row">
            <div class="col-md-12">
            <div class="col-md-4"></div>
                <div class="col-md-4 text-center">
                  <div class="form-group">
                    <a href="{{url('/admin')}}" class="btn btn-primary">Cancelar</a>
                    <input class="btn btn-success" type="submit" name="" value="Enviar">
                </div>
            </div>
            <div class="col-md-4"></div>
            </div>
        </div>

{{Form::close()}}
    </div>
    <div class="col-md-2"></div>
  </div>
</div>

@stop
