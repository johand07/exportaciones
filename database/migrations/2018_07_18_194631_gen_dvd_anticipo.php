<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GenDvdAnticipo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gen_dvd_anticipo', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('gen_anticipo_id')->index()->nullable();
            $table->foreign('gen_anticipo_id')->references('id')->on('gen_anticipo');
            $table->unsignedInteger('gen_operador_cambiario_id')->index()->nullable();
            $table->foreign('gen_operador_cambiario_id')->references('id')->on('gen_operador_cambiario');
            $table->unsignedInteger('gen_status_id')->index()->nullable();
            $table->foreign('gen_status_id')->references('id')->on('gen_status');
            $table->date('fdisponibilidad')->nullable();
            $table->double('mfob')->nullable();
            $table->double('mvendido')->nullable();
            $table->double('mretencion')->nullable();
            $table->double('mpendiente')->nullable();
            $table->string('descripcion', 200)->nullable();
            $table->date('fventa_bcv')->nullable();
            $table->boolean('realizo_venta')->nullable();
            $table->boolean('h_lote')->nullable();    
            $table->boolean('bactivo')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gen_dvd_anticipo');
    }
}
