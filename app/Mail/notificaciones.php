<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
//use Illuminate\Foundation\Auth\User;

class notificaciones extends Mailable
{
    use Queueable, SerializesModels;
    public $evento;
    public $invitado;
    public $invitacionImagen;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($evento,$invitado,$invitacionImagen)
    {
      $this->evento=$evento;
      $this->invitado = $invitado;
      $this->invitacionImagen = $invitacionImagen;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $dia=date("d.m.Y");
        $hora=date("H:i:s");
        
        return $this->view('emails.notificaciones')
        ->subject("VENTANILLA UNICA DE COMERCIO EXTERIOR (VUCE) - Enviado $dia - $hora")
        ->attachData($this->invitacionImagen,'invitacion.png', [ 'as' => 'imagen', 'mime' => 'image/png' ]);
        //->from('hiperiom412@gmail.com','RansesFrom')
    }
}
