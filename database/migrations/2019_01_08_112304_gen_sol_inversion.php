<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GenSolInversion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('gen_sol_inversion', function (Blueprint $table) {
          $table->engine = 'InnoDB';
          $table->increments('id');
          $table->unsignedInteger('gen_status_id')->index()->nullable();
          $table->foreign('gen_status_id')->references('id')->on('gen_status');
          $table->unsignedInteger('gen_sol_inversionista_id')->index()->nullable();
          $table->foreign('gen_sol_inversionista_id')->references('id')->on('gen_sol_inversionista');
          $table->unsignedInteger('gen_divisa_id')->index()->nullable();
          $table->foreign('gen_divisa_id')->references('id')->on('gen_divisa');
          $table->string('nro_sol_inversion',100)->nullable();
          $table->date('fincripcion_rif')->nullable();
          $table->date('fvencimiento_rif')->nullable();
          $table->string('codigo_ciiu','100')->nullable();
          $table->string('zona_posta','10')->nullable();
          $table->string('fax','20')->nullable();
          $table->string('empleos_actuales_directos','100')->nullable();
          $table->string('empleos_actuales_indirectos','100')->nullable();
          $table->string('empleos_generar_directos','100')->nullable();
          $table->string('empleos_generar_indirectos','100')->nullable();
          $table->double('capital_social_suscrito')->nullable();
          $table->double('capital_social_pagado')->nullable();
          $table->string('num_acciones','100')->nullable();
          $table->double('valor_nominal_accion')->nullable();
          $table->double('monto_inversion')->nullable();
          $table->string('adquisicion_bienes','100')->nullable();
          $table->string('maquinaria','100')->nullable();
          $table->string('software','100')->nullable();
          $table->string('capital_trabajo','100')->nullable();
          $table->string('reinversion','100')->nullable();
          $table->string('otra','100')->nullable();
          $table->double('total')->nullable();
          $table->string('estado_observacion','100')->nullable();
          $table->string('observacion_accionista','100')->nullable();
          $table->boolean('bactivo')->default(1);
          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gen_sol_inversion');
    }
}
