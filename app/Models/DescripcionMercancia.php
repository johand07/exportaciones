<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DescripcionMercancia extends Model
{
    protected $table = 'descrip_mercancia';
    protected $fillable = [

        'cert_exporta_facil_id',
        'serie',
        'sub_partida_arancel',
        'descripcion',
        'valor_fob',
        'cantidad',
        'fecha',
        'regimen',
        'bactivo',
    ];

    protected $date =['created_at','updated_at'];

    public function CertExportaFacil() {

        return $this->hasOne('App\Models\CertExportaFacil','id','cert_exporta_facil_id');

    }
}
