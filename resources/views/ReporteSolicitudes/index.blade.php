@extends('templates/layoutlte_admin')

@section('content')
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <title></title>
</head>
<body>
<!-- estilo para toda la pagina -->
<style type="text/css">
@media print{@page { width: 50%;
 height: 50%;
 margin: 1% 0% 1% 0%;
 filter: progid:DXImageTransform.Microsoft.BasicImage(Rotation=3);
 size: landscape;
 
    }}
</style>
<!--estilo para toda la pagina-->
<div id="printf">
  <div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-10">
      {!! Form::open(['route'=>'ReporteSolicitudes.store','method'=>'POST','id'=>'reportsolicitud']) !!}
      <div class="col-md-7 text-center">
        <div class="input-daterange input-group" id="datepicker">
          <span class="input-group-addon"><b>Desde:</b> </span>
          <input type="text" class="form-control" name="desde" id="desde">
          <span class="input-group-addon" id="hasta"><b>Hasta:</b> </span>
          <input type="text" class="form-control" name="hasta" id="hasta">
        </div> 
      </div>  
      <div class="col-md-3">
        <a class="btn btn-primary" href="{{url('admin/AdminUsuario')}}">Cancelar</a>
        <input type="submit" name="Consultar" class="btn btn-success center" value="Consultar" onclick="">
      </div>
      <div class="col-md-2">
        @if(!empty($sol_bienes) &&  !empty($sol_tecno) && !empty($dvd_bienes) && !empty($dvd_tecno))
          <button type="button" onClick="javascript:printfun()" class="btn btn-primary"><span class="glyphicon glyphicon-print" aria-hidden="true"></span>Imprimir</button> 
        @endif
    </div>
      {{Form::close()}}  
    </div>
    <br><br><br>
    <div class="col-md-1"></div>
  </div>
  </div>
 <div class="row" style="display:none;" id="reportesol"><br>
  <table style="width: 100%" align="text-rigth">
    <tr>
      <td></td>
      <td><img class="img-responsive" style="width: 800px;" src={{ asset('img/cintillo_reporte.png') }}></td>
    </tr>
    <tr>
      <td></td>
      <td align="center">Desde: @if (!empty($desde)) {{$desde}}@endif     Hasta: @if (!empty ($hasta)) {{$hasta}}@endif</td>

    </tr>
  </table>
</div>
  
  <div class="row">
    <div class="col-md-12">
      <div class="col-md-2"></div>
      <div class="col-md-8 text-center">
        <p><b>SOLICITUDES DE USUARIOS</b></p>
        <div id="solicitudes_er" style="width: 95%; height: 395px;"></div>
        <div class="col-md-2"></div>
      </div>
    </div>
  </div>
  <!-- Grafico de DVd-->
  <div class="row">
    <div class="col-md-12">
      <div class="col-md-2 hidden-print"></div>
      <div class="col-md-8 text-center">
        <p><b>DVD DE USUARIOS</b></p>
        <div id="dvd_solicitudes" style="width: 95%; height: 395px;"></div>
        <div class="col-md-2"></div>
      </div>
    </div>
  </div>
  </body>
  </html>
      <script type="text/javascript">


                    var chart;
                    var legend;

                    var chartData = [
                    {
                      "country": "Solicitudes ER de Bienes",
                      "value": {{$sol_bienes}}
                    },
                    {
                      "country": "Solicitudes ER de Servicios y/o de Tecnologia",
                      "value": {{$sol_tecno}}
                    }

                    ];


                    AmCharts.ready(function () {
                    // PIE CHART
                    chart = new AmCharts.AmPieChart();
                    chart.dataProvider = chartData;
                    chart.titleField = "country";//titulo del grafico//
                    chart.valueField = "value";// Valor de la torta//
                    chart.outlineColor = "#FFFFFF";//color de la linea de la torta///
                    chart.outlineAlpha = 0.8;
                    chart.outlineThickness = 3;//Grosor de la linea de la torta//
                    chart.balloonText = "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>";
                    // this makes the chart 3D
                    chart.depth3D = 12; // 20 tamaño original real //
                    chart.angle = 12; // 20 tamaño principal//

                // WRITE
                chart.write("solicitudes_er");
                });


/**********************************************Solicitud de DVD*************************/

             var usr;

            var usrData = [
                {
                    "country": "DVD de Bienes",
                    "visits": {{$dvd_bienes}}
                },
                {
                    "country": "DVD de Servicios y/o de Tecnologia",
                    "visits": {{$dvd_tecno}}
                }

            ];


            AmCharts.ready(function () {
                // PIE CHART
                usr = new AmCharts.AmPieChart();

                // title of the chart
                usr.addTitle("", 16);

                usr.dataProvider = usrData;
                usr.titleField = "country";
                usr.valueField = "visits";
                usr.sequencedAnimation = true;
                usr.startEffect = "elastic";
                usr.innerRadius = "30%";
                usr.startDuration = 3;
                usr.labelRadius = 15;
                usr.balloonText = "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>";
                // the following two lines makes the chart 3D
                usr.depth3D = 30;// tamaño original del grafico 30
                usr.angle = 20; //tamaño del grosor del grafico 20

                // WRITE
                usr.write("dvd_solicitudes");
            });
    </script>
@stop
<script type="text/javascript">
//funcion para imprimir el pdf///
  function printfun(){
    IE = window.navigator.appName.toLowerCase().indexOf("micro") != -1; //se determina el tipo de  navegador
    (IE)?sColl = "all.":sColl = "getElementById('";
    (IE)?sStyle = ".style":sStyle = "').style";
    eval("document." + sColl + "printf" + sStyle + ".display = 'none';"); //se ocultan los botones
    
    
    eval("document." + sColl + "reportesol" + sStyle + ".display = 'block';");// Para mostrar el contenido//


$('.main-footer').hide();
    window.print();
    //print();
    /*if (i == cantidad) {
       print(); //se llama el dialogo de impresiÃ³n
    }*/
    //$('#ver').hide();//Casa 
    
    eval("document." + sColl + "printf" + sStyle + ".display = '';"); // se muestran los botones nuevamente
  
    eval("document." + sColl + "reportesol" + sStyle + ".display = 'none';");
   
 $('.main-footer').show();  
  }
</script>
