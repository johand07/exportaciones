<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GenCertificado extends Model
{
    protected $table = 'gen_certificado';



    protected $fillable = [

           'id',
           'gen_usuario_id',
           'gen_status_id',
           'gen_tipo_certificado_id',
           'rif_productor',
           'razon_social_productor',
           'direccion_productor',
           'num_certificado',
           'pais_exportador',
           'pais_cuidad_exportador',
           'pais_importador',
           'fecha_emision_exportador',
           'observaciones',
           'fecha_certicado',
           'ciudad_certificado',
           'observacion_analista',
           'observacion_coordinador',
           'bactivo',
    ];

    protected $date =['created_at','updated_at'];

    public function GenTipoCertificado() {

     return $this->hasOne('App\Models\GenTipoCertificado','id','gen_tipo_certificado_id');

    }

    public function GenStatus() {

     return $this->hasOne('App\Models\GenStatus','id','gen_status_id');

    }

     public function GenUsuario() {

     return $this->hasOne('App\Models\GenUsuario','id','gen_usuario_id');

    }

     public function rBandejaCoordinador() {

     return $this->hasMany('App\Models\BandejaCoordinadorCertificado','id','gen_certificado_id');

    }

    public function rBandejaAnalista() {

     return $this->hasOne('App\Models\BandejaAnalisisCertificado','gen_certificado_id','id');

    }
}
