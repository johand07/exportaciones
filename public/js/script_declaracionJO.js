 var validarProductosFac;
 var verificacion;
 var productosDJO;
 var productosComercializadorDJO;
 var validarproductoDJO;
 var soloNumerosDouble;

$(document).ready(function($) {
  
  $(document).on('click','#cod_aran',function(){


    $('#buscador_aran').keyup();


  });

  $(document).on('click','#cod_aran_c',function(){


    $('#buscador_aran').keyup();


  });
  /************ Mostrar modal y buscador de productos en ( create-update) ***/
  $(document).on('keyup','#buscador_aran',function(){

  
    var metodo=$('input[name="metodo"]').val();
    var tipoAran=localStorage.getItem('arancel_id');
    if(metodo=="Crear")
     {
         $.ajax({

            type:'get',
            url: 'create',
            data: {'valor': $(this).val(),'tipoAran': tipoAran},

            success: function(data){

                $table='<table class="table table-bordered table-hover" id="tabla"><tr><th class="hidden">Nº</th><th>Cód Arancelario</th><th>Descripción</th><th>Descrip comercial</th></tr>';

                $('#arancel_certificado').html($table);
                if(data.length==0){
                      message='<tr><td colspan="3" style="color:red;text-align:center" >Todos sus productos se encuentran declarados...</td></tr>';
                      message+='<tr align="center"><td colspan="3"></td></tr>';
                   
                    
                    $('#tabla tr:last').after(message);

                }else{

                  $.each(data,function(index,value){

                    $('#tabla tr:last').after('<tr class="data_certificado"><td class="hidden">'+value.id+'</td><td>'+value.codigo+'</td><td>'+value.descripcion+'</td><td>'+value.descrip_comercial+'</td></table>');

                  });
                }
            }

         });

   }else{

          $.ajax({
              type:'get',
              url: 'edit',
              data: {'valor': $(this).val(),'tipoAran': tipoAran},
              success: function(data)
              {

                $table='<table class="table table-bordered table-hover" id="tabla"><tr><th>Cód Arancelario</th><th>Descripción</th></th><th>Descripción Comercial</th></tr>';

                $('#arancel_certificado').html($table);
                  if(data.length==0){

                     $('#tabla tr:last').after('<tr><td colspan="2" style="color:red;text-align:center" >Sus Productos se encuentran declarados..</td></tr>');

                  }else{
                          $.each(data,function(index,value){

                            $('#tabla tr:last').after('<tr class="data_certificado"><td>'+value.codigo+'</td><td>'+value.descripcion+'</td><td>'+value.descrip_comercial+'</td></table');

                          });
                  }
              }
          });
        }
  });


  var global_id=localStorage.setItem('arancel_certi'," ");

  $(document).on('click','#cod_aran',function(){

    var id=$(this).parents("tr").attr('id');
    console.log(id);
    localStorage.setItem('arancel_certi',id);
  });

  $(document).on('click','#cod_aran_c',function(){

    var id=$(this).parents("tr").attr('id');
    localStorage.setItem('arancel_certi',id);
  });

  $(document).on('click','tr.data_certificado',function()
  {

    var id=localStorage.getItem('arancel_certi');
    var prod_id=$(this).find("td").eq(0).text();
    var cod=$(this).find("td").eq(1).text();
    var ara=$(this).find("td").eq(2).text();
    var comer=$(this).find("td").eq(3).text();
    
      $("tr#"+id).find("td").eq(0).children("input").val(prod_id);
      $("tr#"+id).find("td").eq(1).children("input").val(cod);
      $("tr#"+id).find("td").eq(2).children("input").val(ara);
      $("tr#"+id).find("td").eq(3).children("input").val(comer);


      $('#modal button').click();
      $('#buscador_aran').val("");

  });

  var mens=[
    'Estimado Usuario. Debe Seleccionar los Productos Disponibles para Completar el Registro',
  ]
  var error = '<div class="msg_alert"><span style="color:red" class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span><strong id="cod_aran-error" class="added"> El campo es obligatorio.</strong></div>';
  var valido = '<div class="msg_alert"><span style="color:green" class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span></div>';

  productosDJO= function()
  {


    $('#FormProd').submit(function(event) {
      var form = document.getElementById('productor');
      var campos = form.querySelectorAll('[name="productos_id[]"]');
      var n = campos.length;
      var err = 0;
    ///////////////////////////////////////
      $("div").remove(".msg_alert");
      for (var i = 0; i < n; i++) {
              var cod_aran_input = $('input[name="codigo_arancel[]"]:eq(' + i + ')');
              if (cod_aran_input.val() == '')
              {
                err = 1;
                cod_aran_input.css('border', '1px solid red').after(error);
              }
              else{
                if (err == 1) {err = 1;}else{err = 0;}
                cod_aran_input.css('border', '1px solid green').after(valido);
              }
      }
       if(err==1){
        event.preventDefault(); 
        swal("Por Favor!", mens[0], "warning")
      }
  });
   /////////////////////////////////////////////////////
  }

  productosComercializadorDJO= function()
  {

		$('#FormProd').submit(function(event) {
		
      var campos = $('#comercializador').find('input:text');
      var n = campos.length;
      var err = 0;
  
      $("div").remove(".msg_alert");
      //bucle que recorre todos los elementos del formulario
      for (var i = 0; i < n; i++) {
          var cod_input = $('#comercializador').find('input:text').eq(i);
            if (cod_input.val() == '' || cod_input.val() == null)
            {
              err = 1;
              cod_input.css('border', '1px solid red').after(error);
            }
            else{
              if (err == 1) {err = 1;}else{err = 0;}
              cod_input.css('border', '1px solid green').after(valido);
            }
            
      }
  
      //Si hay errores se detendrá el submit del formulario y devolverá una alerta
      if(err==1){
          event.preventDefault();
          swal("Por Favor!", mens[0], "warning")
        }
  
        });

  }


});


soloNumerosDouble =function(e){
   key = e.keyCode || e.which;
   tecla = String.fromCharCode(key).toLowerCase();
   letras = "+0123456789.";
   especiales = "8-37-39-46";

   tecla_especial = false
   for(var i in especiales){
        if(key == especiales[i]){
            tecla_especial = true;
            break;
        }
    }

    if(letras.indexOf(tecla)==-1 && !tecla_especial){
        return false;
    }
}
