@extends('templates/layoutlte')
@section('content')
{{ Form::model($user,['route'=>['CambioContrasena.update',$user->id],'method'=>'PATCH','id'=>'formcambContras'])}}

<div class="row">

	<div class="col-md-12">
		<div class="col-md-3"></div>
		<div class="col-md-6">
			<div class="form-group">
				{!! Form::label('','Correo Electrónico') !!}
				{!! Form::text('email',Auth::user()->email,['class'=>'form-control','readonly'=>'true','id'=>'email']) !!}
			</div>
			<br>
			<div class="form-group">
				{!! Form::label('','Contraseña Nueva') !!}
				{!! Form::password('password',['class' => 'form-control','id'=>'password']) !!}
			</div>
			<br>
			<div class="form-group">
				{!! Form::label('','Confirmación Contraseña') !!}
				{!! Form::password('password_confirm',['class'=>'form-control','id'=>'password','id'=>'password_confirm']) !!}
				<br>
			</div> 
			<div class="col-md-3"></div>
		</div>

	</div>
</div>
<div class="row">
	<div class="col-md-12 text-center">
		<div class="form-group">
		     <a href="{{url('/home')}}" class="btn btn-primary">Cancelar</a>
		     {!! Form::submit('Enviar', ['class' => 'btn btn-primary','onclick'=>'enviarcambioContrasena()' ] ) !!}
		</div>
	</div>
</div>

{{Form::close()}}

@stop
