<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DetUsuario extends Model
{
    protected $table = 'det_usuario';

    protected $fillable = [
                
        'correo'                       
    ];

/*relaciones*/
    public function usuario() {

        return $this->hasOne('App\Models\GenUsuario','id','gen_usuario_id');
    
    }

    public function actividadeco() {

        return $this->hasOne('App\Models\GenActividadEco','id','gen_actividad_eco_id');

    }

    public function tipoempresa() {

        return $this->hasOne('App\Models\GenTipoEmpresa','id','gen_tipo_empresa_id');
    
    }

    public function pais() {

        return $this->hasOne('App\Models\Pais','id','pais_id');
    
    }

    public function estado() {

     return $this->hasOne('App\Models\Estado','id','estado_id');

    }

    public function municipio() {

        return $this->hasOne('App\Models\Municipio','id','municipio_id');
    
    }

    public function parroquia() {

        return $this->hasOne('App\Models\Parroquia','id','parroquia_id');

    }

    public function circunsjudicial() {

        return $this->hasOne('App\Models\CircunsJudicial','id','circuns_judicial_id');
    
    }
    public function DetDeclaracion() {

        return $this->hasOne('App\Models\GenDeclaracionJO','id','gen_usuario_id');
    
    }

    public function getIsAccionista(){
        return (!empty($this->accionista) && (strtolower($this->accionista)=="si") || $this->accionista==1);
    }

    public function getIsInversionista(){
        return (!empty($this->invers) && (strtolower($this->invers)=="si") || $this->invers==1 || strtolower($this->invers)=="inversionista");
    }

    public function getIsExportador(){
        return (!empty($this->export) && (strtolower($this->export)=="si") || $this->export==1 || strtolower($this->export)=="exportador");
    }

    public function getIsProductor(){
        return (!empty($this->produc) && (strtolower($this->produc)=="si") || $this->produc==1 || strtolower($this->produc)=="productor");
    }

    public function getIsComercializadora(){
        return (!empty($this->comerc) && (strtolower($this->comerc)=="si") || $this->comerc==1 || strtolower($this->comerc)=="comercializadora");
    }
}
