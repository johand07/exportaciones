<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DetDeclaracionCertificado extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('det_declaracion_certificado', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('gen_certificado_id')->default(1)->index()->nullable();
            $table->foreign('gen_certificado_id')->references('id')->on('gen_certificado');
            $table->string('num_orden_declaracion', 100)->nullable();
            $table->string('normas_criterio', 250)->nullable();
            $table->string('numero_factura', 80)->nullable();
            $table->string('nombre_norma_origen', 150)->nullable();
            $table->date('f_fac_certificado')->nullable();
            $table->boolean('bactivo')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('det_declaracion_certificado');
    }
}
