<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AspectoCumplidoCertRegSeniat extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aspecto_cumplido_cert_reg_seniat', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('gen_cert_reg_seniat_id')->nullable();
            $table->foreign('gen_cert_reg_seniat_id')->references('id')->on('gen_cert_reg_seniat');
            $table->unsignedInteger('cat_cert_reg_seniat_id')->nullable();
            $table->foreign('cat_cert_reg_seniat_id')->references('id')->on('cat_cert_reg_seniat');
            $table->boolean('bactivo')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('aspecto_cumplido_cert_reg_seniat');
    }
}
