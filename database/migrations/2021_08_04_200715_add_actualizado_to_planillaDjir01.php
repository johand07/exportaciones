<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddActualizadoToPlanillaDjir01 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('planilla_djir01', function (Blueprint $table) {
            
            $table->boolean('actualizado')->default(0)->after('gen_status_id');
            
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('planilla_djir01', function (Blueprint $table) {
            
            $table->dropColumn('actualizado');
           
        
       });
    }
}
