<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CatTurquiaVzla extends Model
{
    protected $table = 'cat_turquia_vzla';

    protected $fillable = [

        'codigo',
        'descripcion',
        'bactivo',
    ];

    protected $date =['created_at','updated_at'];
}
