<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CostosInDirectos extends Model
{
    protected $table = 'costos_indirectos';
    protected $fillable = [

           'planilla_3_id',
           'gastos_admin',
           'gastos_publicidad',
           'gastos_ventas',
           'otros_gastos_indi',
           'total',
           'bactivo',
          
    ];
   
    public function planilla3Id() 
    {

      return $this->hasOne('App\Models\Planilla3','id','planilla_3_id');

    }

   


}
