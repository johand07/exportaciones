<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PlanillaDjir02 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('planilla_djir02', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('gen_declaracion_inversion_id');
            $table->foreign('gen_declaracion_inversion_id', 'g_d_i_fk5')->references('id')->on('gen_declaracion_inversion');
            $table->string('grupo_emp_internacional', 150)->nullable();
            $table->string('emp_relacionadas_internacional', 150)->nullable();
            $table->string('casa_matriz', 150)->nullable();
            $table->string('emp_repre_casa_matriz', 150)->nullable();
            $table->string('afiliados_internacional', 150)->nullable();
            $table->string('sub_emp_extranjero', 150)->nullable();
            $table->string('grupo_emp_nacional', 150)->nullable();
            $table->string('emp_relacionados_nacional', 150)->nullable();
            $table->string('emp_haldig_coorp', 150)->nullable();
            $table->string('afiliados_nacional', 150)->nullable();
            $table->string('sub_locales', 150)->nullable();
            $table->string('present_doc_repre_legal', 150)->nullable();
            $table->string('num_apostilla', 150)->nullable();
            $table->date('fecha_apostilla')->nullable();
            $table->string('pais', 150)->nullable();
            $table->string('estado', 150)->nullable();
            $table->string('autoridad_apostilla', 150)->nullable();
            $table->string('cargo', 150)->nullable();
            $table->string('traductor', 150)->nullable();
            $table->text('datos_adicionales')->nullable();
            $table->string('ingresos_anual_ult_ejer', 150)->nullable();
            $table->string('egresos_anual_ult_ejer', 150)->nullable();
            $table->string('total_balance_ult_ejer', 150)->nullable();
            $table->string('anio_informacion_financiera', 150)->nullable();
            $table->string('moneda_extranjera', 150)->nullable();
            $table->string('utilidades_reinvertidas', 150)->nullable();
            $table->string('credito_casa_matriz')->nullable();
            $table->string('tierras_terrenos')->nullable();
            $table->string('edificios_construcciones', 150)->nullable();
            $table->string('total_costos_declaracion', 150)->nullable();
            $table->string('maquinarias_eqp_herra', 150)->nullable();
            $table->string('eqp_transporte', 150)->nullable();
            $table->string('muebles_enceres', 150)->nullable();
            $table->string('otros_activos_tangibles', 150)->nullable();
            $table->string('software', 150)->nullable();
            $table->string('derecho_prop_intelectual', 150)->nullable();
            $table->string('contribuciones_tecno', 150)->nullable();
            $table->string('otros_activos_intangibles', 150)->nullable();
            $table->string('tipo_inversion', 150)->nullable();
            $table->string('invert_divisa_cambio', 150)->nullable();
            $table->string('bienes_cap_fisico_tangibles', 150)->nullable();
            $table->string('bienes_inmateriales_intangibles', 150)->nullable();
            $table->string('reinversiones_utilidades', 150)->nullable();
            $table->string('especifique_otro', 150)->nullable();
            $table->string('valor_libros', 150)->nullable();
            $table->string('valor_neto_activos', 150)->nullable();
            $table->string('valor_emp_similar', 150)->nullable();
            $table->string('otro_especifique', 150)->nullable();
            $table->string('total_modalidad_inv', 150)->nullable();
            $table->unsignedInteger('gen_status_id')->index();
            $table->foreign('gen_status_id')->references('id')->on('gen_status');
            $table->string('estado_observacion', 150);
            $table->string('descrip_observacion', 150);
            $table->boolean('bactivo')->default(1);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('planilla_djir02');
    }
}
