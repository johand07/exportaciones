UPDATE `cat_documentos` SET `nombre_documento` = 'Certificado de domicilio en Venezuela o en el extranjero.' WHERE `cat_documentos`.`id` = 17;

UPDATE `cat_documentos` SET `nombre_documento` = 'Original y una (1) copia del documento constitutivo de la empresa receptora de inversión, con sus modificaciones estatutarias.' WHERE `cat_documentos`.`id` = 12;

UPDATE `cat_documentos` SET `nombre_documento` = 'Datos y documentos de identificación del Representante legal de la empresa receptora de inversión.(ej.: documento de identidad, pasaporte, visa, etc).' WHERE `cat_documentos`.`id` = 14;

UPDATE `cat_documentos` SET `nombre_documento` = 'Estatutos sociales de la Empresa Inversionista debidamente traducida en idioma español, apostillados o legalizados, según el caso. ' WHERE `cat_documentos`.`id` = 22;
