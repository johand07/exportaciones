<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GenDocDecInversion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gen_doc_dec_inversion', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('gen_declaracion_inversion_id');
            $table->foreign('gen_declaracion_inversion_id', 'g_d_i_fk8')->references('id')->on('gen_declaracion_inversion');
            $table->string('file_1', 150)->nullable();
            $table->string('file_2', 150)->nullable();
            $table->string('file_3', 150)->nullable();
            $table->string('file_4', 150)->nullable();
            $table->string('file_5', 150)->nullable();
            $table->string('file_6', 150)->nullable();
            $table->string('file_7', 150)->nullable();
            $table->string('file_8', 150)->nullable();
            $table->string('file_9', 150)->nullable();
            $table->string('file_10', 150)->nullable();
            $table->string('file_11', 150)->nullable();
            $table->boolean('bactivo')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gen_doc_dec_inversion');
    }
}
