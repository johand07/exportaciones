<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TipoPago extends Model
{
    protected $table = 'tipo_pago';

    protected $fillable = [

           'nombre_tipo_pago',
           'monto_bolivares',
           'monto_divisa',
           'bactivo',
    ];

    protected $date =['created_at','updated_at'];
}
