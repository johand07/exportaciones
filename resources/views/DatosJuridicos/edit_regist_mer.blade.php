@extends('templates/layoutlte')
@section('content')

 <div class="content">

{{Form::model($RegMercantil,['route'=>['RegisMercantil.update',$RegMercantil->id],'method'=>'PATCH','id'=>'FormRegMercantil']) }}

			<div class="panel panel-primary">
				<div class="panel-heading">DATOS DE LA PERSONA JURIDICA O FIRMA PERSONAL</div>

			  <div class="panel-body">
			  <div class="row">
				<div class="col-md-4">
					<div class="form-group">
						{!! Form::label('','Registro de Circunscripción Judicial')!!}
						{!!Form::select('circuns_judicial_id',$circuns_judiciales,null,['placeholder'=>'Seleccione','class'=>'form-control','id'=>'circuns_judicial_id']) !!}
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						{!! Form::label('','Número de Documento')!!}
						{!! Form::text('num_registro',null, ['class'=>'form-control','id'=>'num_registro','onkeypress'=>'return soloNumerosyLetras(event)','maxlength'=>'60']) !!}
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						{!! Form::label('','Número de Tomo')!!}
						{!! Form::text('tomo',null, ['class'=>'form-control','id'=>'tomo','onkeypress'=>'return soloNumerosyLetras(event)','maxlength'=>'30']) !!}
					</div>
				</div>
			</div>
				<div class="row">
				<div class="col-md-4">
					<div class="form-group">
						{!! Form::label('',' Numero de Folio')!!}
						{!! Form::text('folio',null, ['class'=>'form-control','id'=>'folio','onkeypress'=>'return soloNumerosyLetras(event)','maxlength'=>'30']) !!}
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						{!! Form::label('','Oficina')!!}
						{!! Form::text('Oficina',null, ['class'=>'form-control','id'=>'Oficina','onkeypress'=>'return soloNumeros(event)','maxlength'=>'60']) !!}
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						{!! Form::label('','Fecha de Constitución')!!}
						<div class="input-group date">
							{!! Form::text('f_registro_mer',null, ['class'=>'form-control','id'=>'f_registro_mer','onchange'=>'return validarfechaRM(this.value,"f_registro_mer")','maxlength'=>'','readonly'=>'readonly']) !!}
							<span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
						</div>
					</div>
				</div>
			</div>
			<div class="panel panel-primary">
				<div class="panel-heading">DATOS DEL REPRESENTANTE LEGAL</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<div class="form-group">
						{!! Form::label('','Nombre')!!}
						{!! Form::text('nombre_repre',null, ['class'=>'form-control','id'=>'nombre_repre','onkeypress'=>'return soloLetras(event)','maxlength'=>'50']) !!}
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						{!! Form::label('','Apellido')!!}
						{!! Form::text('apellido_repre',null, ['class'=>'form-control','id'=>'apellido_repre','onkeypress'=>'return soloLetras(event)','maxlength'=>'50']) !!}
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						{!! Form::label('','Cédula')!!}
						{!! Form::text('ci_repre',null, ['class'=>'form-control','id'=>'ci_repre','onkeypress'=>'return CedulaFormatrep(this,"Cedula de Indentidad Invalida",-1,true,event)','maxlength'=>'11']) !!}<small>Ejemplo:V-00000000</small>
					</div>
				</div>
				</div>
				<div class="row">
				<div class="col-md-5">
					<div class="form-group">
						{!! Form::label('','Cargo')!!}
						{!! Form::text('cargo_repre',null, ['class'=>'form-control','id'=>'cargo_repre','onkeypress'=>'return soloLetras(event)','maxlength'=>'80']) !!}
					</div>
				</div>
				<div class="col-md-7">
					<div class="form-group">
						{!! Form::label('','Correo Electrónico')!!}
					{!! Form::text('correo_repre',null, ['class'=>'form-control','id'=>'correo_repre','onkeyup'=>"minuscula(this.value,'#email');",'maxlength'=>'80']) !!}
					</div>
				</div>
			</div><br>
			<br>
			<div class="panel panel-primary">
				{{--<div class="panel-heading">DATOS DE LOS PRODUCTOS QUE PRODUCE Y/O COMERCIALIZA</div>--}}
			</div>
			<div class="row">
                <div class="col-md-1"></div>
                {{-- <div class="col-md-10 text-center">
                      {!! Form::label('','¿Exporta Productos de Servicios y/o Tecnología?',['class'=>'form-control'])!!}
                      {!! Form::label('','Si',['class'=>'radio-inline'])!!}
                      {!! Form::radio('produc_tecno','1',false,['class'=>'radio-inline','id'=>'prod_tecnologia_si','disabled'=>true])!!}
                      {!! Form::label('','No',['class'=>'radio-inline'])!!}
                      {!! Form::radio('produc_tecno','2',false,['class'=>'radio-inline','id'=>'prod_tecnologia_no','disabled'=>true])!!}

                    </div>--}}
                  <div class="col-md-1"></div>
                </div><br>
		        <div class="row" id="tecno_especifique" style="display: none;">
                  <div class="col-md-1"></div>
                  <div class="col-md-10">
                    {!! Form::label('','Especifique')!!}
                    {!!Form::text('especifique_tecno',null,['class'=>'form-control','id'=>'especifique_tecno','onkeypress'=>'return soloLetras(event)','maxlength'=>'100'])!!}
                  </div>
                  <div class="col-md-1"></div>
                </div><br>
                <div class="row">
                  <div class="col-md-12">
                    <div class="col-md-4"></div>
                    <div class="col-md-4 text-center">
                      <div class="form-group">
                        <a class="btn btn-primary" href="{{route('DatosEmpresa.index')}}" id="atras1">Atrás</a>
                       <input type="submit" name="Guardar" value="Guardar" class="btn btn-success">
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
{{Form::close()}}
@stop

<script src="{{asset('js/ajax_v2.1.1.js')}}"></script>

<script type="text/javascript">

function CedulaFormatrep(vCedulaName,mensaje,postab,escribo,evento) {

  //  var tipoPerson=document.getElementsByName('tipo_usuario');
    tecla=getkey(evento);
    vCedulaName.value=vCedulaName.value.toUpperCase();
    vCedulaValue=vCedulaName.value;
    valor=vCedulaValue.substring(2,12);
    var numeros='0123456789/';
    var digit;
    var noerror=true;
    if (shift && tam>1) {
    return false;
    }
    for (var s=0;s<valor.length;s++){
    digit=valor.substr(s,1);
    if (numeros.indexOf(digit)<0) {
    noerror=false;
    break;
    }
    }
    tam=vCedulaValue.length;
    if (escribo) {
    if ( tecla==8 || tecla==37) {
    if (tam>2)
    vCedulaName.value=vCedulaValue.substr(0,tam-1);
    else
    vCedulaName.value='';
    return false;
    }
    if (tam==0 && tecla==69) {
    vCedulaName.value='E-';
    return false;
    }
    if (tam==0 && tecla==69) {
    //vCedulaName.value='E-';
    return false;
    }


    if (tam==0 && tecla==86) {
    vCedulaName.value='V-';
    return false;
    }


        if (tam==0 && tecla==74) {
    vCedulaName.value='J-';
    return false;
    }
    if (tam==0 && tecla==71) {
    vCedulaName.value='G-';
    return false;
    }
    if (tam==0 && tecla==80) {
    vCedulaName.value='P-';
    return false;
    }

    else if ((tam==0 && ! (tecla<14 || tecla==69 || tecla==86 || tecla==46)))
    return false;
    else if ((tam>1) && !(tecla<14 || tecla==16 || tecla==46 || tecla==8 || (tecla >= 48 && tecla <= 57) || (tecla>=96 && tecla<=105)))
    return false;
    }

}



function getkey(e){
    if (window.event) {
    shift= event.shiftKey;
    ctrl= event.ctrlKey;
    alt=event.altKey;
    return window.event.keyCode;
    }
    else if (e) {
    var valor=e.which;
    if (valor>96 && valor<123) {
    valor=valor-32;
    }
    return valor;
    }
    else
    return null;
}
/***************************************************************************************/

$(document).ready(function (){
	/****************************Validacion de radio tecnologia********************************************/
if ($("#prod_tecnologia_si").is(':checked')){
	$('#tecno_especifique').show(1000).animate({width: "show"}, 1000,"linear");

}
if ($("#prod_tecnologia_no").is(':checked')){
	$('#tecno_especifique').hide(1000).animate({height: "hide"}, 1000,"linear");
}

$('#prod_tecnologia_si').click(function(event) {

        $('#tecno_especifique').show(1000).animate({width: "show"}, 1000,"linear");

      });

       $('#prod_tecnologia_no').click(function(event) {
        $('#tecno_especifique').hide(1000).animate({height: "hide"}, 1000,"linear");

      });

});
</script>
