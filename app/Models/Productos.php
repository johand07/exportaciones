<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Productos extends Model
{
    protected $table = 'productos';
    

    protected $fillable = [

           'gen_usuario_id',
           'codigo',
           'descripcion',
           'descrip_comercial',
           'cap_insta_anual',
           'cap_opera_anual',
           'cap_alamcenamiento',
           'gen_unidad_medida_id',
           'cap_exportacion',
           'bactivo',
    ];

    protected $date =['created_at','updated_at'];

    public function usuarios() {

     return $this->hasOne('App\Models\GenUsuario','id','gen_usuario_id');

    }
    
    public function unidadMedida() {

     return $this->hasOne('App\Models\GenUnidadMedida','id','gen_unidad_medida_id');
     
    }
    public function rArancelNan() {

     return $this->hasOne('App\Models\GenArancelNandina','codigo','codigo');
     
    }
    public function rArancelMer() {

     return $this->hasOne('App\Models\GenArancelMercosur','codigo','codigo');
     
    }


    public function rDeclaracionProduct() {

     return $this->belongsTo('App\Models\DetDeclaracionProduc','id','productos_id');

    }

 }
