<?php

namespace App\Http\Controllers;

use App\Models\GenUsuario;
use App\Models\Accionistas;
use App\Models\Productos;
use App\Models\DetUsuario;
use App\Models\GenTipoEmpresa;
use App\Models\GenUnidadMedida;
use App\Models\GenDivisa;
use App\Models\CatTipoUsuario;
use App\Models\GenDeclaracionInversion;
use App\Models\PlanillaDjir_01;
use App\Models\PlanillaDjir_02;
use App\Models\PlanillaDjir_03;
use App\Models\Estado;
use App\Models\Municipio;
use App\Models\GenStatus;
use App\Models\Pais;
use App\Models\CatSectorProductivoEco;
use App\Http\Requests\Planilla3Request;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Auth;
use Session;
use Laracasth\Flash\Flash;
use Alert;

class BandejaInversionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $declaracionesInversion = GenDeclaracionInversion::where('gen_usuario_id',Auth::user()->id)->where('bactivo', 1)->with('rPlanillaDjir02')->get();
        //dd($declaracionesInversion);
        return view('BandejaInversion.index', compact('declaracionesInversion'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       
        $tipousuario=CatTipoUsuario::where('bactivo',1)->pluck('nombre','id');
        /* $complete = 1;
        $UltimatSolicitud=GenDeclaracionInversion::where('gen_usuario_id',Auth::user()->id)->get()->last();
        $planillaDjir01=PlanillaDjir_01::where('gen_declaracion_inversion_id',$UltimatSolicitud->id)->first();
        $planillaDjir02=PlanillaDjir_02::where('gen_declaracion_inversion_id',$UltimatSolicitud->id)->first();
        $planillaDjir03=PlanillaDjir_03::where('gen_declaracion_inversion_id',$UltimatSolicitud->id)->first();
        if(empty($planillaDjir01) || empty($planillaDjir02) ||  empty($planillaDjir03) ) {
            $complete = 0;
        } */
        // $num_declaracion= $UltimatSolicitud->num_declaracion;
        return view('BandejaInversion.create', compact('tipousuario') );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\GenUsuario  $genUsuario
     * @return \Illuminate\Http\Response
     */
    public function show(GenUsuario $genUsuario)
    {
        // Creo que es aqui para llenar el formulario
        // consultar en detusuario
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\GenUsuario  $genUsuario
     * @return \Illuminate\Http\Response
     */
    public function edit(GenUsuario $genUsuario)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\GenUsuario  $genUsuario
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, GenUsuario $genUsuario)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\GenUsuario  $genUsuario
     * @return \Illuminate\Http\Response
     */
    public function destroy(GenUsuario $genUsuario)
    {
        //
    }

     public function RedirecFormDeclaracionInversionComplet(Request $request, $id)
    {
        // dd($id);
        
        //$titulo= 'Certificado de Origen';
        $descripcion= 'Peril del Exportador';/* nsantos*/
        ///////////////////////////////////////////////////////////////////////////////
        

        $gen_declaracion_inversion_id = $id ? $id : 0;
        Session::put('gen_declaracion_inversion_id',$gen_declaracion_inversion_id);

        $planillaDjir01=PlanillaDjir_01::where('gen_declaracion_inversion_id',$gen_declaracion_inversion_id)->first();
        $planillaDjir02=PlanillaDjir_02::where('gen_declaracion_inversion_id',$gen_declaracion_inversion_id)->first();
        $planillaDjir03=PlanillaDjir_03::where('gen_declaracion_inversion_id',$gen_declaracion_inversion_id)->first();

        return view('BandejaInversion.datos_inversion',compact('gen_declaracion_inversion_id', 'planillaDjir01','planillaDjir02','planillaDjir03'));
       
      

    }

    ///////////////////////////////////////////////////////////
    //Metodo para elegir el tipo de usuario Declaración de Inversion
     public function RedirecFormDeclaracionInversion(Request $request)
    {
        // dd($id);
        
        //$titulo= 'Certificado de Origen';
        $descripcion= 'Peril del Exportador';/* nsantos*/
        ///////////////////////////////////////////////////////////////////////////////
        
        if (!empty($request->cat_tipo_usuario_id)) {
          
            Session::put('cat_tipo_usuario_id',$request->cat_tipo_usuario_id);
        }

        
        $cat_tipo_usuario_id=$request->cat_tipo_usuario_id ? $request->cat_tipo_usuario_id : Session::get('cat_tipo_usuario_id') ;
       // dd($cat_tipo_usuario_id);
       // 
        $declaracion_inversion = new GenDeclaracionInversion;
        $declaracion_inversion->gen_usuario_id = Auth::user()->id;
        $declaracion_inversion->cat_tipo_usuario_id = $cat_tipo_usuario_id;
        $declaracion_inversion->gen_status_id = 9;
        $declaracion_inversion->num_declaracion = $this->generarRamdom();
        $declaracion_inversion->fstatus = date('Y-m-d');
        $declaracion_inversion->save();

        $gen_declaracion_inversion_id = $declaracion_inversion->id;

        Session::put('gen_declaracion_inversion_id',$gen_declaracion_inversion_id);

        $planillaDjir01=PlanillaDjir_01::where('gen_declaracion_inversion_id',$gen_declaracion_inversion_id)->first();
        $planillaDjir02=PlanillaDjir_02::where('gen_declaracion_inversion_id',$gen_declaracion_inversion_id)->first();
        $planillaDjir03=PlanillaDjir_03::where('gen_declaracion_inversion_id',$gen_declaracion_inversion_id)->first();

        return view('BandejaInversion.datos_inversion',compact('cat_tipo_usuario_id', 'gen_declaracion_inversion_id', 'planillaDjir01','planillaDjir02','planillaDjir03'));
       
      

    }

    private function generarRamdom()
    {
        $user = Auth::user();
        $random = Str::random(4);
        $id  = $user->id;
        $rif = substr($user->detUsuario->rif, -2);
        $fecha = date('Ymd');
        $UltimatSolicitud=GenDeclaracionInversion::where('gen_usuario_id',Auth::user()->id)->get()->last();
        $idUltimatSolicitud = !empty($UltimatSolicitud) ? $UltimatSolicitud->id : 0;
        return 'DI-'.$id.'-'.$fecha.$random.$rif.$idUltimatSolicitud;
    }

    private function generarRamdomActualizar()
    {
        $user = Auth::user();
        $random = Str::random(4);
        $id  = $user->id;
        $rif = substr($user->detUsuario->rif, -2);
        $fecha = date('Ymd');
        $UltimatSolicitud=GenDeclaracionInversion::where('gen_usuario_id',Auth::user()->id)->get()->last();
        $idUltimatSolicitud = !empty($UltimatSolicitud) ? $UltimatSolicitud->id : 0;
        return 'ADI-'.$id.'-'.$fecha.$random.$rif.$idUltimatSolicitud;
    }
    

    public function Planilla1Inversion(Request $request)
    {

         $id=Auth::user()->id;
        $estados=Estado::where('id',"<>",null)->orderBy('estado','asc')->pluck('estado','id');
        $municipios=Municipio::all()->pluck('municipio','id');
        
        // dd($accionistas);
         $nacionalidad=['V'=>'V','E'=>'E'];
         $productos=Productos::where('gen_usuario_id',Auth::user()->id)->get();
         $unidad_medida=GenUnidadMedida::all()->pluck('dunidad','id');
         $tipos_empresas=GenTipoEmpresa::whereIn('id', [4,5,6,7])->pluck('d_tipo_empresa','id');
         $divisa=GenDivisa::whereIn('id',[32,33,34,35,36,37,38])->pluck('ddivisa','id');

         $gen_declaracion_inversion_id=$request->gen_declaracion_inversion_id;
       

       return view('BandejaInversion.Planilla1Inversion',compact('id','nacionalidad','productos','unidad_medida','tipos_empresas','divisa','gen_declaracion_inversion_id','estados','municipios'));

    }

    protected function getMunicipios (Request $request, $id){
        if ($request->ajax()) {
            $municipios=Municipio::municipios($id);
            return response()->json($municipios);
        }
    }

    
    /////////////////////////////////////////////////////////////
    public function Planilla2Inversion(Request $request)
    {

         $gen_declaracion_inversion_id=$request->gen_declaracion_inversion_id;

          $divisas=GenDivisa::orderBy('ddivisa_abr','asc')->pluck('ddivisa_abr','id');
         //dd($divisas);
       
       return view('BandejaInversion.Planilla2Inversion',compact('gen_declaracion_inversion_id','divisas'));


    }

   
    /////////////////////////////////////////////////////////////
    public function Planilla3Inversion(Request $request)
    {
       $estado=Pais::all()->pluck('dpais','id');
   // dd($planillaDjir01);

         $gen_declaracion_inversion_id=$request->gen_declaracion_inversion_id;
       
       return view('BandejaInversion.Planilla3Inversion',compact('gen_declaracion_inversion_id','estado'));

   }


     public function storePlanilla3Inversion(Planilla3Request $request)


    {

        
        $cat_tipo_usuario_id=$request->cat_tipo_usuario_id;
        
        return view('BandejaInversion.datos_inversion',compact('cat_tipo_usuario_id'));

    }

    public function ActualizarDeclaracion(Request $request, $id)
    {
        $descripcion= 'Peril del Exportador';/* nsantos*/
        ///////////////////////////////////////////////////////////////////////////////
        //$titulo= 'Certificado de Origen';
        $descripcion= 'Peril del Exportador';/* nsantos*/
        ///////////////////////////////////////////////////////////////////////////////
        $declaracion = GenDeclaracionInversion::find($id);
        
        $gen_declaracion_inversion_id = $id;
        Session::put('gen_declaracion_inversion_id',$gen_declaracion_inversion_id);
        
        $planillaDjir01=PlanillaDjir_01::where('gen_declaracion_inversion_id',$gen_declaracion_inversion_id)->first();
        $planillaDjir02=PlanillaDjir_02::where('gen_declaracion_inversion_id',$gen_declaracion_inversion_id)->first();
        $planillaDjir03=PlanillaDjir_03::where('gen_declaracion_inversion_id',$gen_declaracion_inversion_id)->first();
        

        if($planillaDjir01->actualizado != 1 && $planillaDjir02->actualizado != 1 && $planillaDjir03->actualizado != 1){
            if (!empty($declaracion->cat_tipo_usuario_id)) {
          
                Session::put('cat_tipo_usuario_id',$request->cat_tipo_usuario_id);
            }
    
            
            $cat_tipo_usuario_id=$request->cat_tipo_usuario_id ? $request->cat_tipo_usuario_id : Session::get('cat_tipo_usuario_id') ;
           // dd($cat_tipo_usuario_id);
           // 
            $declaracion_inversion = new GenDeclaracionInversion;
            $declaracion_inversion->gen_usuario_id = Auth::user()->id;
            $declaracion_inversion->cat_tipo_usuario_id = $declaracion->cat_tipo_usuario_id;//$cat_tipo_usuario_id;
            $declaracion_inversion->gen_status_id = 9;
            $declaracion_inversion->num_declaracion = $this->generarRamdomActualizar();
            $declaracion_inversion->num_declaracion_actualizada = $declaracion->num_declaracion;
            $declaracion_inversion->fstatus = date('Y-m-d');
            $declaracion_inversion->save();
    
            $declaracion_id = $declaracion_inversion->id;
    
            Session::put('gen_declaracion_inversion_id',$declaracion_id);
        }else{
            //comsultar la declaracion ya creada
            $genDeclaracion = GenDeclaracionInversion::where('num_declaracion_actualizada', $declaracion->num_declaracion)->first();
            $declaracion_id = $genDeclaracion->id;
        }


        return view('BandejaInversion.actualizar.datos_inversion',compact('declaracion_id', 'gen_declaracion_inversion_id', 'planillaDjir01','planillaDjir02','planillaDjir03'));
    }

 
 


}
