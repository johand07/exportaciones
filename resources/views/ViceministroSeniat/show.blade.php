@extends('templates/layoutlte_seniat')

@section('content')
<div class="row"> 
    <div class="col-md-12">
        <div class="row">
            <div class="panel-default"> Historial de la Planilla</div>
            <div class="col-md-12">
                <div class="col-md-6">
                    <li>
                        <ul>
                            <b>{{ $certificado->GenStatus->nombre_status}}
                                @if(!empty($bandejaanalista))
                                {{$bandejanalista->GenUsuario->email}}
                                {{ $certificado->fecha_certificado}}
                                @elseif(!empty($bandejavice))
                                {{$bandejavice->GenUsuario->email}}
                                {{ $certificado->fecha_certificado}}</b>
                                @endif
                        </ul>
                    </li>
                </div>
            </div>
        </div><br>
        <div id="datos_persona1">
            <div class="panel panel-primary">
            <div class="panel-heading">CERTIFICADO DE ORIGEN</div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-6">
                            <label>Código Arancelario</label><br>
                            <strong>{{ $certificado->codigo_arancelario }}</strong><br><br><br>

                            <label>Archivo</label><br>
                            <a href="{{ asset($file->file) }}" download="Certificado" class="btn btn-primary btn-lg">Certificado de Origen</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            
                        </div>
                    </div>
                   
                        </div>
                    @if(!empty($id_aspctos))  
                        <div class="panel-heading">Aspectos que cumple el certificado de Origen del Acuerdo Comercial con Colombia</div><br>

                          
                        @foreach($catCert as $key => $cat)
                           
                                @if(in_array($cat->id, $id_aspctos)) 
        
                                    <ul>
                                        <b class="text-info">{{ $key + 1 }}. {{ $cat->pregunta }}</b>
                                    </ul>
                                        
                                @else
                                    <ul>
                                        <b class="text-danger">{{ $key + 1 }}. {{ $cat->pregunta }}
                                    </ul>
                                @endif       
                            @endforeach
                        @elseif(!empty($certificaddo->observacion_analista))
                        <div class="panel-heading">Observaciones:</div><br>
                            {{ $certificado->observacion_analista }}
                        @else

                        @endif
                    </div>

                    @if($certificado->gen_status_id == 27)
                        <div class="panel-footer">
                            <div class="row">
                                <div class="col-md-6 text-center">
                                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                                            Desaprobar
                                    </button>
                                </div>
                                <div class="col-md-6 text-left">
                                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal1">
                                            Aprobar
                                    </button>
                                </div>
                        </div>

                    @elseif($certificado->gen_status_id == 28 || $certificado->gen_status_id == 12) 
                        
                            <div class="panel-footer">
                                <div class="row">
                                    {{Form::open(['route' =>'ViceministroSeniat.store' ,'method'=>'POST','id'=>''])}}
                                        <div class="col-md-12 text-center">
                                            <input type="hidden" name="id" value="{{ $certificado->id }}">
                                            <input type="submit" class="btn btn-primary" name="solo_aprobar" value="Aprobar">
                                        </div>
                                    {{Form::close()}}
                    @elseif(!empty($id_aspctos)) 
                    <div class="panel-footer">
                            <div class="row">
                @if($certificado->gen_status_id == 28 || $certificado->gen_status_id == 12 )
                {{Form::open(['route' =>'ViceministroSeniat.store' ,'method'=>'POST','id'=>''])}}
                                <div class="col-md-12 text-center">
                                    <input type="hidden" name="id" value="{{ $certificado->id }}">
                                    <input type="submit" class="btn btn-primary" value="Aprobar" name="solo_aprobar">
                                </div>
                {{Form::close()}}
                @else

                @endif

                        </div>

                            </div>
                        
                    @endif
        </div>

    </div>


    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="exampleModalLabel">Aspectos a revisar en el Certificado de Origen del Acuerdo Comercial con Colombia</h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            {{Form::open(['route' =>'ViceministroSeniat.store' ,'method'=>'POST','id'=>''])}}
            <div class="modal-body">
                
                    <div class="form-group">
                        <label for="observaciones">Observaciones</label>
                        <textarea name="observacion_analista" class="form-control"></textarea>
                        <input type="hidden" name='id' value="{{ $certificado->id }}">
                    </div>
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <input type="submit" class="btn btn-primary" value="Desaprobar" name="desaprobar">
            </div>
            {{Form::close()}} 
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog  modal-lg" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="exampleModalLabel">Aspectos a revisar en el Certificado de Origen del Acuerdo Comercial con Colombia</h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            {{Form::open(['route' =>'ViceministroSeniat.store' ,'method'=>'POST','id'=>''])}}
            <div class="modal-body">
                
                    
                @foreach($catCert as $key => $row)
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="gen_cert_reg_seniat_id[]" value="{{$row->id}}" id="{{$row->id}}" />
                                <label class="form-check-label" for="{{$row->id}}">{{$key + 1}} . {{$row->pregunta}}</label>
                            </div>
                        </div>
                    </div>
                @endforeach
                <input type="hidden" name='id' value="{{ $certificado->id }}">
                    
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <input type="submit" class="btn btn-primary" value="Aprobar" name="aprobar">
            </div>
            {{Form::close()}} 
            </div>
        </div>
    </div>
</div>
@stop