<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Productos;
use App\Models\GenUnidadMedida;
use App\Models\GenArancelNandina;
use App\Models\GenArancelMercosur;
use Laracasts\Flash\Flash;
use Illuminate\Support\Facades\Session;
use Auth;
use Alert;

class ProductosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $usu=Productos::where('gen_usuario_id',Auth::user()->id)->first();
        if(is_null($usu)){

         return redirect()->action('ProductosController@create');

        }else{

         return redirect()->action('ProductosController@edit',[0]);

        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

      if($request->ajax()){
        if($request['valor']==""){
          $arancel=GenArancelMercosur::whereRaw("LENGTH(REPLACE(codigo, '.', '')) = 10")->take(50)->get();
          return $arancel;
       }else{
         $data=GenArancelMercosur::whereRaw("LENGTH(REPLACE(codigo, '.', '')) = 10")->where( function ($query) use($request){
                      return $query->where('codigo','like','%'.$request['valor'].'%')
                       ->orWhere('descripcion','like','%'.$request['valor'].'%');
                     })->take(50)->get();

         return $data;
       }
        /*  if( ($request['tipoAran']==1) && ($request['valor']=="") ){

             $arancel=GenArancelNandina::take(50)->get();
             return $arancel;

          }elseif( ($request['tipoAran']==2) && ($request['valor']=="")){

               $arancel=GenArancelMercosur::take(50)->get();
               return $arancel;

          }elseif($request['valor']!="") {

              if( ($request['tipoAran']==1) && ($request['valor']!="")){

                  $data=GenArancelNandina::where('codigo','like','%'.$request['valor'].'%')->orWhere('descripcion','like','%'.$request['valor'].'%')->take(50)->get();

              return $data;

              }elseif(($request['tipoAran']==2) && ($request['valor']!="")){

                  $data=GenArancelMercosur::where('codigo','like','%'.$request['valor'].'%')->orWhere('descripcion','like','%'.$request['valor'].'%')->take(50)->get();

                  return $data;

             }

          }else{
              if($request['tipoAran']==1){

              $arancel=GenArancelNandina::take(50)->get();
               return $arancel;

              }else{

              $arancel=GenArancelMercosur::take(50)->get();
              return $arancel;

             }
          }
*/
      }else{

         $arancel=$arancel=GenArancelMercosur::all();
  /***********************Consulto la tabla Mercosur********************************/
      }


       $titulo="Productos";
       $descripcion="Registrar Productos";
       $unidad_medida=GenUnidadMedida::all()->pluck('dunidad','id');
       return view('productos_export.create',compact('unidad_medida','arancel','titulo','descripcion'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      
//dd($request->all());
       
        for($i=0;$i<count($request->codigo_arancel);$i++)
               {
                 $insert2=Productos::create([
                'gen_usuario_id'=>Auth::user()->id,
                'codigo'=>$request->codigo_arancel[$i],
                'descripcion'=>$request->descrip_arancel[$i],
                'descrip_comercial'=>$request->descrip_comercial[$i],
                'gen_unidad_medida_id'=>$request->gen_unidad_medida_id[$i],
                'cap_opera_anual'=>$request->cap_opera_anual[$i],
                'cap_insta_anual'=>$request->cap_insta_anual[$i],
                'cap_alamcenamiento'=>$request->cap_alamcenamiento[$i],
                'cap_exportacion'=>$request->cap_exportacion[$i],
                'bactivo'=>1,
                 ]);
               }

              Alert::success('Registro Exitoso!')->persistent("Ok");
              return redirect("exportador/DatosEmpresa");

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,$id)
    {
      
      

//dd(1);
      if($request->ajax()){
        
        if($request->valor==""){
           $arancel=GenArancelMercosur::whereRaw("LENGTH(REPLACE(codigo, '.', '')) = 10")->take(50)->get();
           return $arancel;
        }else{
          $data=GenArancelMercosur::whereRaw("LENGTH(REPLACE(codigo, '.', '')) = 10")->where( function ($query) use($request){
                       return $query->where('codigo','like','%'.$request['valor'].'%')
                        ->orWhere('descripcion','like','%'.$request['valor'].'%');
                      })->take(50)->get();

          return $data;
        }

      }else{

         $arancel=$arancel=GenArancelMercosur::all();
  /***********************Consulto la tabla Mercosur********************************/
      }

    $productos=Productos::where('gen_usuario_id',Auth::user()->id)->get();
    // $productos=Productos::where('gen_usuario_id',Auth::user()->id)->get();
     $unidad_medida=GenUnidadMedida::all()->pluck('dunidad','id');
     $titulo="Editar Productos";

  // dd($productos);


     $descripcion="Detalle de los productos";

       if ($productos->isEmpty()) { 
        // dd(1);

         return redirect()->action('ProductosController@create');

        }else{

        // return redirect()->action('ProductosController@edit',[0]);
         return view('productos_export.edit',compact('titulo','productos','arancel','id','unidad_medida','descripcion'));

        }


     

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {


       //dd($request->all());

     
        /*for($i=0;$i<count($request->codigo);$i++){
          //dd($request->id[$i]);
           Productos::updateOrCreate(
           ['id'=>$request->id[$i]],
           ['gen_usuario_id'=>Auth::user()->id,
           'codigo'=>$request->codigo[$i],
           'descripcion'=>$request->descripcion[$i],
           'descrip_comercial'=>$request->descrip_comercial[$i],
           'cap_insta_anual'=>$request->cap_insta_anual[$i],
           'cap_opera_anual'=>$request->cap_opera_anual[$i],
           'cap_alamcenamiento'=>$request->cap_almacenamiento[$i],
           'cap_exportacion'=>$request->cap_exportacion[$i],
           'gen_unidad_medida_id'=>$request->gen_unidad_medida_id[$i],
           'bactivo'=>1]

           );

       }*/
  $cont_productos=count($request->codigo);
        for($i=0;$i<($cont_productos);$i++){
         Productos::create(
        
         ['gen_usuario_id'=>Auth::user()->id,
         'codigo'=>$request->codigo[$i],
         'descripcion'=>$request->descripcion[$i],
         'descrip_comercial'=>$request->descrip_comercial[$i],
         'cap_insta_anual'=>$request->cap_insta_anual[$i],
         'cap_opera_anual'=>$request->cap_opera_anual[$i],
         'cap_alamcenamiento'=>$request->cap_almacenamiento[$i],
         'cap_exportacion'=>$request->cap_exportacion[$i],
         'gen_unidad_medida_id'=>$request->gen_unidad_medida_id[$i],
         'bactivo'=>1]

         );
    }


          Alert::success('Producto Agregado de manera Exitosa!')->persistent("Ok");
          return redirect("exportador/DatosEmpresa");
    }//  return view('productos_export.edit');

    public function eliminar($id)
      {

         $delete=Productos::find($_GET['id'])->delete();
         if($delete)
         {
            return 1;
         }else
         {
           return 0;
         }

      }






    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function editar($id)
    {
      $producto = Productos::find($_GET['id']);

      return $producto;
    }

    public function actualizar(Request $request)
    {//dd(1);
      $producto = Productos::find($request->id);
      $producto->codigo               =   $request->codigo_arancel;
      $producto->descripcion          =   $request->descrip_arancel;
      $producto->descrip_comercial    =   $request->descrip_comercial;
      $producto->gen_unidad_medida_id =   $request->gen_unidad_medida_id;
      $producto->cap_opera_anual      =   $request->cap_opera_anual;
      $producto->cap_insta_anual      =   $request->cap_insta_anual;
      $producto->cap_alamcenamiento   =   $request->cap_alamcenamiento;
      $producto->cap_exportacion      =   $request->cap_exportacion;
      $producto->save();

      Alert::success('Actualización Exitosa!')->persistent("Ok");
      return redirect()->action('ProductosController@edit',[0]);
    }
}
