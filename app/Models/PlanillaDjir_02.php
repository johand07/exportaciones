<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PlanillaDjir_02 extends Model
{
    protected $table = 'planilla_djir02';
    
    protected $fillable = [
        
        'gen_declaracion_inversion_id',
        'gen_status_id', 
        'grupo_emp_internacional',
        'emp_relacionadas_internacional',
        'casa_matriz',
        'emp_repre_casa_matriz',
        'afiliados_internacional',
        'sub_emp_extranjero',
        'grupo_emp_nacional',
        'emp_relacionados_nacional',
        'emp_haldig_coorp',
        'afiliados_nacional',
        'sub_locales',
        'present_doc_repre_legal',
        'num_apostilla',
        'fecha_apostilla',
        'pais',
        'estado',
        'autoridad_apostilla',
        'cargo',
        'traductor',
        'datos_adicionales',
        'ingresos_anual_ult_ejer',
        'egresos_anual_ult_ejer',
        'total_balance_ult_ejer',
        'anio_informacion_financiera',
        'moneda_informacion_financiera',
        'moneda_extranjera',
        'utilidades_reinvertidas',
        'credito_casa_matriz',
        'moneda_bienes_tangibles',
        'tierras_terrenos',
        'edificios_construcciones',
        'maquinarias_eqp_herra',
        'muebles_enceres',
        'otros_activos_tangibles',
        'moneda_bienes_intangibles',
        'software',
        'derecho_prop_intelectual',
        'eqp_transporte',
        'contribuciones_tecno',
        'otros_activos_intangibles',
        'total_costos_declaracion',
        'tipo_inversion',
        'invert_divisa_cambio',
        'bienes_cap_fisico_tangibles',
        'bienes_inmateriales_intangibles',
        'reinversiones_utilidades',
        'especifique_otro',
        'valor_libros',
        'valor_neto_activos',
        'valor_emp_similar',
        'otro_especifique',
        'total_modalidad_inv',
        'estado_observacion',
        'descrip_observacion',
        'base_estimacion',
    ];

    public function genDecInversion()
	{
        return $this->hasOne('App\Models\GenDeclaracionInversion','id','gen_declaracion_inversion_id');
    }

    public function genStatus()
	{
        return $this->hasOne('App\Models\GenStatus','id','gen_status_id');
    }

    public function infoFinanciera()
	{
        return $this->hasOne('App\Models\GenDivisa','id','moneda_informacion_financiera');
    }

    public function bienesTangibles()
	{
        return $this->hasOne('App\Models\GenDivisa','id','moneda_bienes_tangibles');
    }

    public function bienesIntangibles()
	{
        return $this->hasOne('App\Models\GenDivisa','id','moneda_bienes_intangibles');
    }
}
