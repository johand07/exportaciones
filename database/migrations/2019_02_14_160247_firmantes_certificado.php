<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FirmantesCertificado extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('firmantes_certificado', function (Blueprint $table) {
          $table->engine = 'InnoDB';
          $table->increments('id');
          $table->string('img_firma',100)->nullable();
          $table->string('img_sello',100)->nullable();
          $table->string('nombre_firmante',100)->nullable();
          $table->string('apellido_firmante',100)->nullable();
          $table->string('cargo_firmante',100)->nullable();
          $table->string('ente_firmante',100)->nullable();
          $table->text('gaceta_firmante')->nullable();
          $table->string('tipo_certificado',100)->nullable();
          $table->boolean('bactivo')->default(1);
          $table->timestamps();
          
         });    
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::dropIfExists('firmantes_certificado'); 
    }
}
