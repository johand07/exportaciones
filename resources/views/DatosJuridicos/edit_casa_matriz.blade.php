@extends('templates/layoutlte')
@section('content')

<div class="content">
 {{Form::model(@$matriz,['route'=>['CasaMatriz.update',@$matriz->id],'method'=>'PATCH','id'=>'Formcamatriz']) }}
  <div class="panel panel-primary">
    <div class="panel-heading">CASA MATRIZ</div>

    <div class="panel-body">
      <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6 text-center">
          <div class="form-group">
          {!! Form::label('','¿Posée Casa Matriz?')!!}

          @foreach($radio as $rad)
            {!! Form::label('',$rad->nombre,['class'=>'radio-inline'])!!}
            {!! Form::radio('casa_matriz',$rad->id,false,['class'=>'radio-inline','id'=>"casa_matriz_$rad->id"])!!}
          @endforeach
          </div>
        </div>
        <div class="col-md-3"></div>
      </div>
      <div class="row" id="pais_origen" style="display:none;">
        <div class="col-md-3"></div>
        <div class="col-md-6">
          <div class="form-group"><!--Cambiar el id de este campo-->
            {!! Form::label('','País de Origen')!!}
            {!!Form::select('pais_id',$paises,null,['class'=>'form-control','placeholder'=>'Seleccione un País','id'=>'pais_id']) !!}
          </div>
        </div>
        <div class="col-md-3"></div>
      </div>
    <div class="row">
      <div class="col-md-12">
        <div class="col-md-4"></div>
        <div class="col-md-4 text-center">
          <div class="form-group">
            <a class="btn btn-primary" href="{{route('DatosEmpresa.index')}}" id="atras1">Atrás</a>
            <input type="submit" name="Guardar" value="Guardar" class="btn btn-success">
          </div>
        </div>
      </div>
    </div>
   </div><!--Cierra el Panel body!-->
  </div>
</div>
<br><br>
{{Form::close()}}
@stop

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

<script type="text/javascript">

  /***************************************************************************************/

$(document).ready(function (){
    /****************************Validacion de radio Casa Matriz********************************************/
if ($("#casa_matriz_1").is(':checked')){
  $('#pais_origen').show(1000).animate({width: "show"}, 1000,"linear");//Si casa matriz tiene el checked muestro el pais de origen///

}

if ($("#casa_matriz_2").is(':checked')){
  $('#pais_origen').hide(1000).animate({height: "hide"}, 1000,"linear");//Casa matriz tiene el checked en no oculto el pais de origen///
}

$('#casa_matriz_1').click(function(event) {

        $('#pais_origen').show(1000).animate({width: "show"}, 1000,"linear");  

       });

        $('#casa_matriz_2').click(function(event) {
          $('#pais_origen').hide(1000).animate({height: "hide"}, 1000,"linear");

      });

});
</script>