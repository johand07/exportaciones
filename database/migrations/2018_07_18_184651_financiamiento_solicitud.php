<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FinanciamientoSolicitud extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('financiamiento_solicitud', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('gen_solicitud_id')->index()->nullable();
            $table->foreign('gen_solicitud_id')->references('id')->on('gen_solicitud');
            $table->unsignedInteger('gen_nota_credito_id')->index()->nullable();
            $table->foreign('gen_nota_credito_id')->references('id')->on('gen_nota_credito');
            $table->unsignedInteger('gen_anticipo_id')->index()->nullable();
            $table->foreign('gen_anticipo_id')->references('id')->on('gen_anticipo');
            $table->unsignedInteger('cat_regimen_export_id')->index()->nullable();
            $table->foreign('cat_regimen_export_id')->references('id')->on('cat_regimen_export');
            $table->unsignedInteger('tipo_instrumento_id')->index()->nullable();
            $table->foreign('tipo_instrumento_id')->references('id')->on('tipo_instrumento');
            $table->string('nota_credito', 30)->nullable();
            $table->string('anticipo', 30)->nullable();
            $table->unsignedInteger('cat_regimen_especial_id')->index()->nullable();
            $table->foreign('cat_regimen_especial_id')->references('id')->on('cat_regimen_especial');
            $table->string('especifique_regimen', 150)->nullable();
            $table->string('financiamiento', 30)->nullable();
            $table->string('entidad_financiera', 30)->nullable();
            $table->string('nro_contrato', 20)->nullable();
            $table->date('fapro_contrato')->nullable();
            $table->date('fvenci_contrato')->nullable();
            $table->double('monto_apro_contrato')->nullable();
            $table->string('num_cuota_contrato', 20)->nullable();
            $table->double('monto_amort_contrato')->nullable();
            $table->string('num_cuota_amortizado', 20)->nullable();
            $table->string('envio_muestra', 20)->nullable();
            $table->boolean('bactivo')->default(1);
            $table->timestamps();
        });

    
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('financiamiento_solicitud');
    }
}
