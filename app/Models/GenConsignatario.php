<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GenConsignatario extends Model
{
    protected $table = 'gen_consignatario';

    protected $fillable = [

           'cat_tipo_cartera_id',
           'gen_usuario_id',
           'rif_empresa',
           'nombre_consignatario',
           'perso_contac',
           'cargo_contac',
           'pais_id',
           'cat_tipo_convenio_id',
           'ciudad_consignatario',
           'direccion',
           'telefono_consignatario',
           'correo',
           'bactivo'


    ];

    protected $date =['created_at','updated_at'];

    public function usuario() {

     return $this->hasOne('App\Models\GenUsuario','id','gen_usuario_id');

    }


    public function pais() {

      return $this->hasOne('App\Models\Pais','id','pais_id');
    	
    }

    public function contacto() {

      return $this->hasOne('App\Models\CatTipoCartera','id','cat_tipo_cartera_id');
      
    }

     public function convenio() {

      return $this->hasOne('App\Models\CatTipoConvenio','id','cat_tipo_convenio_id');
      
    }
















}
