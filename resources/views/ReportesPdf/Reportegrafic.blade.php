@extends('templates/layoutlte_admin')

@section('content')


<!-- estilo para toda la pagina del pdf-->
<style type="text/css">
@media print{@page { width: 50%;
 height: 40%;
 margin: 0% 2% 0% 2%;
 filter: progid:DXImageTransform.Microsoft.BasicImage(Rotation=3);
 size: landscape;
 font-size:3px;
    }}
</style>
    <div id="printf">
        <div class="row">
            <form class="form-horizontal" method="POST" action="/admin/Graficanual" id="formgrafico">
                {{ csrf_field() }}
                <div class="col-md-2"></div>
                <div class="col-md-6">
                    <div class="form-group">
                        {!! Form::label('', 'Año') !!}
                        <div class="input-group" >
                            {!! Form::text('fecha_grafico', null, ['class' => 'form-control','id'=>'fecha_grafico']) !!}
                            <span class="input-group-addon" ><i id="fecha_grafico" class="glyphicon glyphicon-th"></i></span>
                        </div>
                    </div>   
                </div>
                <div class="col-md-2"><br>
                    <input type="submit" name="Consultar" class="btn btn-success center" value="Consultar" onclick="">
                </div>
                <br>
                <div class="col-md-2">
                    <button type="button" onClick="javascript:printfun()" class="btn btn-primary"><span class="glyphicon glyphicon-print" aria-hidden="true"></span>Imprimir</button>
                </div>
            </form>
        </div>
    </div>
    <div class="row" style="display:none;" id="cintillo1"><br>
        <table style="width: 100%">
            <tr>    
                <td></td>
                <td><img class="img-responsive" style="width: 800px;" src={{ asset('img/cintillo_reporte.png') }}></td>
            </tr>
        </table>
    </div>

	<div class="row">
        <div class="col-md-2"></div>
	    <div class="col-md-8">
	    <p><b>Usuarios Registrados año {{$anio}}</b></p>
	    <div id="UsuariosRegistrados" style="width: 97%; height: 295px;"></div>
	    </div>
        <div class="col-md-2"></div>
    </div>

    <div class="row">
    	<div class="col-md-3"></div>
	    	<div class="col-md-6">
	    		<p><b>Usuarios Naturales y Juridicos año {{$anio}}</b></p>
	        	<div id="chartdiv" style="width:650px; height:350px;" class="text-center"></div>
	        </div>
        <div class="col-md-3"></div>
    </div>
        <br><br><br><br><br><br>
<script>

//-**********************************JS PARA EL GRAFICO NUMERO UNO DE BARRA************************
            var chart;

            var chartData = [
                {
                    "country": "Ene",
                    "visits": {{$Ene}},
                    "color": "#FF0F00"
                },
                {
                    "country": "Feb",
                    "visits": {{$Feb}},
                    "color": "#FF6600"
                },
                {
                    "country": "Mar",
                    "visits": {{$Mar}},
                    "color": "#FF9E01"
                },
                {
                    "country": "Abr",
                    "visits": {{$Abr}},
                    "color": "#FCD202"
                },
                {
                    "country": "May",
                    "visits": {{$May}},
                    "color": "#F8FF01"
                },
                {
                    "country": "Jun",
                    "visits": {{$Jun}},
                    "color": "#B0DE09"
                },
                {
                    "country": "Jul",
                    "visits": {{$Jul}},
                    "color": "#04D215"
                },
                {
                    "country": "Ago",
                    "visits": {{$Ago}},
                    "color": "#0D8ECF"
                },
                {
                    "country": "Sep",
                    "visits": {{$Sep}},
                    "color": "#0D52D1"
                },
                {
                    "country": "Oct",
                    "visits": {{$Oct}},
                    "color": "#2A0CD0"
                },
                {
                    "country": "Nov",
                    "visits": {{$Nov}},
                    "color": "#8A0CCF"
                },
                {
                    "country": "Dic",
                    "visits": {{$Dic}},
                    "color": "#CD0D74"
                }
            ];


            AmCharts.ready(function () {
                // SERIAL CHART
                chart = new AmCharts.AmSerialChart();
                chart.dataProvider = chartData;
                chart.categoryField = "country";
                chart.startDuration = 2; // duracion de estrella
                chart.depth3D = 59;//40 profundida
                chart.angle = 10;//30 angulo 
                chart.marginRight = -35;//35

                // AXES
                // category
                var categoryAxis = chart.categoryAxis;
                categoryAxis.gridAlpha = 0;
                categoryAxis.axisAlpha = 0;
                categoryAxis.gridPosition = "start";

                // value
                var valueAxis = new AmCharts.ValueAxis();
                valueAxis.axisAlpha = 0;//eje alfa
                valueAxis.gridAlpha = 0;//rejilla alfa
                chart.addValueAxis(valueAxis);

                // GRAPH
                var graph = new AmCharts.AmGraph();
                graph.valueField = "visits";
                graph.colorField = "color";
                graph.balloonText = "<b>[[category]]: [[value]]</b>";
                graph.type = "column";
                graph.lineAlpha = 0.5;
                graph.lineColor = "#FFFFFF";
                graph.topRadius = 1;
                graph.fillAlphas = 0.9;
                chart.addGraph(graph);

                // CURSOR
                var chartCursor = new AmCharts.ChartCursor();
                chartCursor.cursorAlpha = 0;
                chartCursor.zoomable = false;
                chartCursor.categoryBalloonEnabled = false;
                chartCursor.valueLineEnabled = true;
                chartCursor.valueLineBalloonEnabled = true;
                chartCursor.valueLineAlpha = 1;
                chart.addChartCursor(chartCursor);

                chart.creditsPosition = "top-right";

                // WRITE
                chart.write("UsuariosRegistrados");
            });





            //******************Js Para la grafica de torta*******************************
            
            var usr;

            var usrData = [
                {
                    "country": "Usuarios Naturales",
                    "visits": {{$usr_natural}}
                },
                {
                    "country": "Usuarios Juridicos",
                    "visits": {{$usr_juridico}}
                }
            ];


            AmCharts.ready(function () {
                // PIE CHART
                usr = new AmCharts.AmPieChart();

                // title of the chart
                usr.addTitle("", 16);

                usr.dataProvider = usrData;
                usr.titleField = "country";
                usr.valueField = "visits";
                usr.sequencedAnimation = true;
                usr.startEffect = "elastic";
                usr.innerRadius = "30%";
                usr.startDuration = 2;
                usr.labelRadius = 10;// tamaño real 15
                usr.balloonText = "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>";
                // the following two lines makes the chart 3D
                usr.depth3D = 15; // tamaño 15
                usr.angle = 17; // tamaño 20

                // WRITE
                usr.write("chartdiv");
            });
</script>
@stop

<script type="text/javascript">
//funcion para imprimir el pdf///
  function printfun(){
    IE = window.navigator.appName.toLowerCase().indexOf("micro") != -1; //se determina el tipo de  navegador
    (IE)?sColl = "all.":sColl = "getElementById('";
    (IE)?sStyle = ".style":sStyle = "').style";
    eval("document." + sColl + "printf" + sStyle + ".display = 'none';"); //se ocultan los botones
    
    
    eval("document." + sColl + "cintillo1" + sStyle + ".display = 'block';");// Para mostrar el contenido//

//$('#UsuariosRegistrados').css('width', 80); 

  //ajustar el tamaño al momento de imprimir
 $('#chartdiv').css('font-size', 5); // tamaño de la fuente
$('.main-footer').hide();
    window.print();
    //print();
    /*if (i == cantidad) {
       print(); //se llama el dialogo de impresiÃ³n
    }*/
    //$('#ver').hide();//Casa 
    
    eval("document." + sColl + "printf" + sStyle + ".display = '';"); // se muestran los botones nuevamente
  
    eval("document." + sColl + "cintillo1" + sStyle + ".display = 'none';"); // se muestran los botones nuevamente

   //$('#UsuariosRegistrados').css('width', 100); 
$('.main-footer').show();

  }

</script>

