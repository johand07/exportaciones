<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Pais;
use App\Models\GenDeclaracionJO;
use App\Models\DetDeclaracionProduc;
use App\Models\GenUnidadMedida;
use App\Models\Productos;
use App\Models\DetUsuario;
use App\Models\GenConsignatario;
use App\Http\Requests\ListaDeclaracionJO;
use App\Http\Requests\ListaDeclaracionJOComercializador;
use Illuminate\Support\Facades\DB;
use Auth;
use Alert;

class ListaDeclaracionJOController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //dd(1);
         $declaraciones=DB::table('gen_declaracion_jo')
            ->join('det_usuario','gen_declaracion_jo.gen_usuario_id','=','det_usuario.gen_usuario_id')
             ->join('gen_status','gen_declaracion_jo.gen_status_id','=','gen_status.id')
            ->select(
                'gen_declaracion_jo.id',
                'det_usuario.rif',
                'det_usuario.razon_social',
                'det_usuario.correo',
                'det_usuario.ruta_doc',
                'det_usuario.ruta_rif',
                'det_usuario.ruta_reg_merc',
                'gen_declaracion_jo.num_solicitud',
                'gen_declaracion_jo.num_productos',
                'gen_declaracion_jo.num_paises',
                'gen_declaracion_jo.gen_status_id',
                'gen_declaracion_jo.fstatus',
                'gen_declaracion_jo.observacion_corregida',
                'gen_status.nombre_status',
                'gen_declaracion_jo.created_at',
                'gen_declaracion_jo.bactivo'
                )
            
            ->where('gen_declaracion_jo.gen_usuario_id','=', Auth::user()->id)
            //->groupBy('det_usuario.rif')
            ->orderBy('gen_declaracion_jo.id','desc')
            ->get();
            //consulta de los documentos soportes
            $usuario=DetUsuario::where('gen_usuario_id',Auth::user()->id)->first();
            
                $titulo= 'Declaración Jurada de Origen';
                $descripcion= 'Lista Declaraciones Juradas de Origen';
                $error=0;

        return view('ListaDeclaracionJO.index',compact('declaraciones', 'descripcion', 'titulo','usuario','error'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create( Request $request)
    {

        if($request->ajax())
        {

            if($request['valor']=="")
            {
              
                $arancel = DB::table('productos')
                    ->where('productos.gen_usuario_id',Auth::user()->id)
                    ->whereRaw("LENGTH(REPLACE(productos.codigo, '.', '')) = 10")
                    ->whereNotIn('id',function($query){
                        $query->select('productos_id')->from('det_declaracion_produc')
                                ->join('gen_declaracion_jo', 'gen_declaracion_jo.id', '=','det_declaracion_produc.gen_declaracion_jo_id')
                                ->where('gen_declaracion_jo.gen_usuario_id',Auth::user()->id);
                     })
                    ->take(50)
                    ->get();

                return $arancel;

                //dd($arancel);


            }else{
                // dd($request['valor']);
                $arancel = DB::table('productos')
                    ->where('productos.gen_usuario_id',Auth::user()->id)
                    ->whereRaw("LENGTH(REPLACE(productos.codigo, '.', '')) = 10")
                    /*->whereNotIn('id',function($query){
                        $query->select('productos_id')->from('det_declaracion_produc')
                                ->join('gen_declaracion_jo', 'gen_declaracion_jo.id', '=','det_declaracion_produc.gen_declaracion_jo_id')
                                ->where('gen_declaracion_jo.gen_usuario_id',Auth::user()->id);
                    })*/
                    ->where(function($q) use ($request){
                        $q->where('codigo','like','%'.$request['valor'].'%')
                            ->orWhere('descripcion','like','%'.$request['valor'].'%');
                    })
                    ->take(50)
                    ->get();

                return $arancel;
            }

       }else{


                    $ProductosDeclarados= Productos::select('codigo','descripcion','descrip_comercial',DB::raw('CONCAT("Declarado") AS estatus'))
                            ->where('productos.gen_usuario_id',Auth::user()->id)
                            //->whereRaw("LENGTH(REPLACE(productos.codigo, '.', '')) = 10")
                            ->whereIn('id',function($query){
                                $query->select('productos_id')->from('det_declaracion_produc')
                                        ->join('gen_declaracion_jo', 'gen_declaracion_jo.id', '=','det_declaracion_produc.gen_declaracion_jo_id')
                                        ->where('gen_declaracion_jo.gen_usuario_id',Auth::user()->id)
                                        ;
                             });

                            // dd($ProductosDeclarados);
                            
                    $arancel= Productos::select('codigo','descripcion','descrip_comercial',DB::raw('CONCAT("No Declarado") AS estatus'))
                            ->where('productos.gen_usuario_id',Auth::user()->id)
                            ->whereNotIn('id',function($query){
                                $query->select('productos_id')->from('det_declaracion_produc')
                                        ->join('gen_declaracion_jo', 'gen_declaracion_jo.id', '=','det_declaracion_produc.gen_declaracion_jo_id')
                                        ->where('gen_declaracion_jo.gen_usuario_id',Auth::user()->id);
                             })
                            ->union($ProductosDeclarados)             
                            ->get();
                   $detUsuario= Auth::user()->detUsuario;

                   //dd($arancel);
                   //dd($detUsuario);

                   
       }

        $titulo='Declaración Jurada de Origen';
        $descripcion= 'Seleccione los productos que asociara a su nueva declaración';
        $error=0;
        return view('ListaDeclaracionJO.create',compact('arancel','titulo','descripcion','detUsuario','error'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ListaDeclaracionJO $request)
    {
        //dd($request->all());
        DB::beginTransaction();
        $model= new GenDeclaracionJO;

        #----------Numero de Solicutud -------------

        $idUsuario=rand(10,99).Auth::user()->id;
        $numSolicitudesDJO=$model->where('gen_usuario_id',Auth::user()->id)->get()->count(); 
        $idLastSolicitudDJO=$model->where('gen_usuario_id',Auth::user()->id)->get()->last();
        if ($idLastSolicitudDJO==null){$idLastSolicitudDJO=0;}else{$idLastSolicitudDJO=$idLastSolicitudDJO->id;}
        $nProductosDJO=count($request->productos_id);
        $fecha=date('myd');

        $num_Solicitud="".($numSolicitudesDJO+1)."-".$idUsuario.$nProductosDJO.$fecha.$idLastSolicitudDJO;


        #---------- Save Gen Declaracion Jurada de Origen -> con Estatus no Compledata-------------

        $model->gen_usuario_id    = Auth::user()->id;
        $model->gen_status_id = 7;
        $model->fstatus=$fecha;
        $model->num_solicitud = $num_Solicitud;
        $model->num_productos = $nProductosDJO;
        $model->num_paises    = 0;
        $model->tipoUsuario   = $request->tipoUsuario ? $request->tipoUsuario :'Productor';

        if( $model->save()) 
        {

           #---------- Save det Declaracion Productos -> asociados a la Declaracion ----------------- 
           foreach ( $request->productos_id as $id => $id_producto) 
           {

                    $modelProducto= new DetDeclaracionProduc;
                    $modelProducto->gen_declaracion_jo_id = $model->id; #------ id de la declaracion
                    $modelProducto->productos_id = $id_producto;
                    $modelProducto->estado_p2=0;
                    $modelProducto->estado_p3=0;
                    $modelProducto->estado_p4=0;
                    $modelProducto->estado_p5=0;
                    $modelProducto->estado_p6=0;

                    if ($modelProducto->save()) {
                        # code...
                    }else{

                         DB::rollback();

                         Alert::warning('No es Posible Validar la Solicitud. Intente Nuevamente.','Acción Denegada!')->persistent("OK");
                         return redirect()->back()->withInput();
                    }
            }

             

            Alert::success('Debe seleccionar las planillas correspondientes a sus productos!','Para Completar su Declaración')->persistent("Ok");
            DB::commit();
            return redirect()->action('DeclaracionJOController@index',['djo'=>encrypt($model->id)]);

        }else{

            DB::rollback();

            Alert::warning('No es Posible Validar la Solicitud. Intente Nuevamente.','Acción Denegada!')->persistent("OK");
            return redirect()->back()->withInput();
        }

           

    }

        /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeComercializador(ListaDeclaracionJOComercializador $request)
    {
    
       //dd($request->all());
        DB::beginTransaction();
        $model= new GenDeclaracionJO;

        #----------Numero de Solicutud -------------

        $idUsuario=rand(10,99).Auth::user()->id;
        $numSolicitudesDJO=$model->where('gen_usuario_id',Auth::user()->id)->get()->count(); 
        $idLastSolicitudDJO=$model->where('gen_usuario_id',Auth::user()->id)->get()->last();
        if ($idLastSolicitudDJO==null){$idLastSolicitudDJO=0;}else{$idLastSolicitudDJO=$idLastSolicitudDJO->id;}
        $nProductosDJO=0;
        
        foreach($request->productos_id_c as $producs)
        $nProductosDJO+= count($producs);

        $fecha=date('myd');

        $num_Solicitud="".($numSolicitudesDJO+1)."-".$idUsuario.$nProductosDJO.$fecha.$idLastSolicitudDJO;


        #---------- Save Gen Declaracion Jurada de Origen -> con Estatus no Compledata-------------

        $model->gen_usuario_id    = Auth::user()->id;
        $model->gen_status_id = 7;
        $model->fstatus=$fecha;
        $model->num_solicitud = $num_Solicitud;
        $model->num_productos = $nProductosDJO;
        $model->num_paises    = 0;
         $model->tipoUsuario   = $request->tipoUsuario ? $request->tipoUsuario :'Comercializador';

        if( $model->save()) 
        {

          #---------- Save det Declaracion Productos -> asociados a la Declaracion ----------------- 
          foreach ( $request->productos_id_c as $clave => $producs) 
          {
            $modelConsignatario = new GenConsignatario;
            $modelConsignatario->gen_usuario_id= Auth::user()->id;
            $modelConsignatario->cat_tipo_cartera_id=2;
            $modelConsignatario->cat_tipo_convenio_id=7;
            $modelConsignatario->pais_id=168;
            $modelConsignatario->nombre_consignatario=$request->razon_social[$clave];
            $modelConsignatario->rif_empresa=$request->rif[$clave];
            $modelConsignatario->siglas=$request->siglas[$clave];
            $modelConsignatario->registro_directo=1;

            if($modelConsignatario->save()){
                
               foreach($producs as $id_producto){
                   $modelProducto= new DetDeclaracionProduc;
                   $modelProducto->gen_declaracion_jo_id = $model->id; #------ id de la declaracion
                   $modelProducto->productos_id = $id_producto;
                   $modelProducto->estado_p2=0;
                   $modelProducto->estado_p3=0;
                   $modelProducto->estado_p4=0;
                   $modelProducto->estado_p5=0;
                   $modelProducto->estado_p6=0;
                   $modelProducto->id_gen_consignatario = $modelConsignatario->id;
                   if ($modelProducto->save()) {
                       # code...
                   }else{

                        DB::rollback();

                        Alert::warning('No es Posible Validar la Solicitud. Intente Nuevamente.','Acción Denegada!')->persistent("OK");
                        return redirect()->back()->withInput();
                   }
               }
            }else{
                DB::rollback();

                Alert::warning('No es Posible Validar la Solicitud. Intente Nuevamente.','Acción Denegada!')->persistent("OK");
                return redirect()->back()->withInput();
            }
           }

             

            Alert::success('Debe seleccionar las planillas correspondientes a sus productos!','Para Completar su Declaración')->persistent("Ok");
            DB::commit();
            return redirect()->action('DeclaracionJOController@index',['djo'=>encrypt($model->id)]);

        }else{

            DB::rollback();

            Alert::warning('No es Posible Validar la Solicitud. Intente Nuevamente.','Acción Denegada!')->persistent("OK");
            return redirect()->back()->withInput();
        }

           

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
