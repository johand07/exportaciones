@extends('templates/layoutlte_admin')

@section('content')
<style type="text/css">
@media print{@page { width: 50%;
 height: 50%;
 margin: 0% 0% 0% 0%;
 filter: progid:DXImageTransform.Microsoft.BasicImage(Rotation=3);
 size: landscape;
    }}
</style>
<br>
<div id="printf">
  <div class="row">
    <div class="col-md-9">
      {!! Form::open(['route'=>'DetalleEmpresas.store','method'=>'POST','id'=>'detalleemp']) !!}
      
      <div class="col-md-6">
        <div class="input-daterange input-group" id="datepicker">
          <span class="input-group-addon"><b>Desde:</b> </span>
          {!!Form::text('desde',(!is_null($desde))?$desde:old('desde'),['class'=>'form-control','id'=>'desde'])!!}
          <span class="input-group-addon"><b>Hasta:</b> </span>
          {!!Form::text('hasta',(!is_null($hasta))?$hasta:old('hasta'),['class'=>'form-control','id'=>'hasta'])!!}
        </div> 
      </div>  
      <div class="col-md-3">
        <a class="btn btn-primary" href="{{url('admin/AdminUsuario')}}">Cancelar</a>
        <input type="submit" name="Consultar" class="btn btn-success center" value="Consultar" onclick="">
      </div>   
      {{Form::close()}}  
    </div>
    <div class="col-md-3">
   
      <button type="button" onClick="javascript:printfun({{count($empresas)}})" class="btn btn-primary"><span class="glyphicon glyphicon-print" aria-hidden="true"></span>Imprimir</button>

      <a href="#" id="btnExport" class="btn btn-primary" onclick="exportarexel()"><span class="glyphicon glyphicon-print" aria-hidden="true" ></span>Exportar Excel</a>
    
    </div>
  </div>
  <br><br>
  <div class="row">
    <div class="col-md-8">
    </div>
    
    <div class="col-md-4">
      <div class="input-group">
        <span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
        <input id="system_search" name="busqueda" type="text" class="form-control" placeholder="Buscar">
      </div>
    </div>
  </div>
</div>

<div class="row" style="display:none;" id="cintillo"><br>
  <table style="width: 100%" >
    <tr>
      <td></td>
      <td align="center"><img class="img-responsive" style="width: 800px;" src={{ asset('img/cintillo_reporte.png') }}></td>
    </tr>
    <tr>
      <td></td>
      <td align="center">Desde: @if (!empty($desde)) {{$desde}}@endif     Hasta: @if (!empty ($hasta)) {{$hasta}}@endif</td>

    </tr>
  </table>
</div>

<div class="row" style="display:none;" id="cintillo"><br>
  <table style="width: 100%" >
    <tr>
      <td></td>
      <td align="center"><img class="img-responsive" style="width: 800px;" src={{ asset('img/cintillo_reporte.png') }}></td>
    </tr>
    <tr>
      <td></td>
      <td align="center">Desde: @if (!empty($desde)) {{$desde}}@endif     Hasta: @if (!empty ($hasta)) {{$hasta}}@endif</td>

    </tr>
  </table>
</div>

<div class="row">
  <div class="col-md-12">
    <div class="row">
      <div class="col-md-6">
        <b>Cantidad de Empresas Resgitradas:</b><span class="label label-warning">{{count($empresas)}}</span> 
        
      </div>
      <div class="col-md-6"></div>
    </div>
    <br>
    <table id="listaDetalleEmpresa" class="table-list-search tbody table table-bordered table-hover" style="width:100%">
            <thead>
                <tr>
                    <th id="ver">Ver</th>
                    <th>Rif</th>
                    <th>Nombre empresa</th>
                    <th>Email</th>
                    <th>Teléf Local</th>
                    <th>Estado</th>     

                    
                </tr>
            </thead>
            <tbody>
            
            	@foreach($empresas as $key=>$empre)
                <tr>

                    <td id="det_{{$key}}"><a title="Consultar detalle" target="_black" href="{{route('DetalleEmpresas.show',$empre->gen_usuario_id)}}" class="btn btn-primary btn-sm"><i class="glyphicon glyphicon-search" ></i> {{$empre->rif ? $empre->rif :''}}</a></td>
                    <td>{{$empre->rif ? $empre->rif :''}}</td>
                    <td>{{$empre->razon_social ? $empre->razon_social :''}}</td>
                    <td>{{$empre->correo ? $empre->correo :''}}</td>
                    <td>{{$empre->telefono_local ? $empre->telefono_local:''}}</td>
                    <td>{{$empre->estado ? $empre->estado->estado :''}}</td>            
                </tr>
              @endforeach
            </tbody>
        </table>
  </div>
  
</div>

@stop
<script src="{{asset('js/ajax_v2.1.1.js')}}"></script>
<script type="text/javascript">

  function printfun(cantidad){
    IE = window.navigator.appName.toLowerCase().indexOf("micro") != -1; //se determina el tipo de  navegador
    (IE)?sColl = "all.":sColl = "getElementById('";
    (IE)?sStyle = ".style":sStyle = "').style";
    eval("document." + sColl + "printf" + sStyle + ".display = 'none';"); //se ocultan los botones
    eval("document." + sColl + "ver" + sStyle + ".display = 'none';"); //se ocultan los botones
    for (var i =0; i <=cantidad; i++) {
      $("#det_"+i).hide();
      //eval("document." + sColl + "det_"+i + sStyle + ".display = 'none';"); //se ocultan los botones

    }

    eval("document." + sColl + "cintillo" + sStyle + ".display = 'block';");
    $('.main-footer').hide();


    window.print();
    //print();
    /*if (i == cantidad) {
       print(); //se llama el dialogo de impresiÃ³n
    }*/
    //$('#ver').hide();//Casa 
    
    eval("document." + sColl + "printf" + sStyle + ".display = '';"); // se muestran los botones nuevamente
    eval("document." + sColl + "ver" + sStyle + ".display = '';"); //se ocultan los botones
    for (var i =0; i <=cantidad; i++) {
      $("#det_"+i).show();
      //eval("document." + sColl + "det_"+i + sStyle + ".display = 'none';"); //se ocultan los botones

    }

    eval("document." + sColl + "cintillo" + sStyle + ".display = 'none';");
    $('.main-footer').show();

  }
    /*$("#btnExport").click(function(e) {

        window.open('data:application/vnd.ms-excel,' + encodeURIComponent($('#listaDetalleEmpresa').html()));

        e.preventDefault();

    });*/


    /*--------------ajax reporte exel---------------------------------------*/
function exportarexel(){
    $.ajax({    //AgenteAduanal.destroy O exportador/AgenteAduanal/{AgenteAduanal}
            url: '/admin/ExcelDetalleEmpresas',
            type: 'GET',

            data: {
                    
                    'desde': $('#desde').val(),
                    'hasta': $('#hasta').val()

                },
        })
        .done(function(data) {
            //alert(data.file);
              var a = document.createElement("a");
              a.href = data.file;
              a.download = data.name;
              document.body.appendChild(a);
              a.click();
              a.remove();
              console.log(data);
       



        })
        .fail(function(jqXHR, textStatus, thrownError) {
            errorAjax(jqXHR,textStatus)

        })
}
</script>