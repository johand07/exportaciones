<?php

namespace App\Http\Controllers;

use App\Models\GenAgenteAduanal;
use App\Models\GenDua;
use App\Http\Requests\GenAgenteAduanalRequest;
use Session;
use Auth;
use Laracasts\Flash\Flash;
use Alert;

use Illuminate\Http\Request;

class AgenteAduanalController extends Controller
{

    /* * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $titulo="Agente de Aduana";
        $descripcion="Agente de Aduanas registrados";

        $agentes=GenAgenteAduanal::where('gen_usuario_id',Auth::user()->id)->where('bactivo',1)->get();
        //dd($agentes);
        return view('AgenteAduanal.index',compact('agentes','titulo','descripcion'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $titulo="Agente de Aduana";
        $descripcion="Registro agente de aduana";
        return view('AgenteAduanal.create',compact('titulo','descripcion'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(GenAgenteAduanalRequest $request)
    {
       GenAgenteAduanal::create($request->all());
        
        //concateno letra del rif con numero de rif
        
        /*
        $agente_aduanal= new GenAgenteAduanal;
        $agente_aduanal->gen_usuario_id=Auth::user()->id;
        $agente_aduanal->nombre_agente=$request->nombre_agente;
        $agente_aduanal->letra_rif_agente=$request->letra_rif_agente;
        $agente_aduanal->rif_agente=$request->rif_agente;
        $agente_aduanal->numero_registro=$request->numero_registro;
        $agente_aduanal->ciudad_agente=$request->ciudad_agente;
        $agente_aduanal->telefono_agente=$request->telefono_agente;
        $agente_aduanal->save();

        */
       Alert::success('REGISTRO EXITOSO','Agente Aduanal agregado')->persistent("Ok");
       return redirect()->action('AgenteAduanalController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\GenAgenteAduanal  $genAgenteAduanal
     * @return \Illuminate\Http\Response
     */
    public function show(GenAgenteAduanal $genAgenteAduanal)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\GenAgenteAduanal  $genAgenteAduanal
     * @return \Illuminate\Http\Response
     */
     //edit trae el formulario lleno
    public function edit(GenAgenteAduanal $genAgenteAduanal, $id)
    {
      $titulo="Editar Agente de Aduana";
      $descripcion="Editar agente de aduana";
      $agente=GenAgenteAduanal::find($id);//find es para buscar en la tabla agente aduanal el que tenga este id
      return view('AgenteAduanal.edit',compact('agente','titulo','descripcion'));//Retorna Carpeta y Edit formulario


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\GenAgenteAduanal  $genAgenteAduanal
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, GenAgenteAduanal $genAgenteAduanal, $id)
    {

        GenAgenteAduanal::find($id)->update($request->all());
        Alert::success('registro Actualizado!', 'ACTUALIZACIÓN EXITOSA.')->persistent("Ok");
        return redirect()->action('AgenteAduanalController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\GenAgenteAduanal  $genAgenteAduanal
     * @return \Illuminate\Http\Response
     */
    public function destroy(GenAgenteAduanal $genAgenteAduanal, $id)
    {

        $consulta_dua=GenDua::where('gen_agente_aduana_id',$id)->first();

        //dd($consulta_dua);
        if (!empty($consulta_dua)) {

            return 3;

            /*$datos =
                            array(
                                    'valor_caso'  => 3,
                                    'mensaje'   => 'DUA'

                                );
                //Devolvemos el array pasado a JSON como objeto
                return json_encode($datos, JSON_FORCE_OBJECT);*/

        } else {

            $agente_destroy=GenAgenteAduanal::find($id)->update(['bactivo' => 0, 'updated_at' => date("Y-m-d H:i:s") ]);
            if ($agente_destroy) {
               return 1;
            }else{
                return 2;
            }


        }


    }
}
