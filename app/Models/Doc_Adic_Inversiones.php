<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Doc_Adic_Inversiones extends Model
{
    protected $table = 'doc_adicional_inversiones';



    protected $fillable = [

           'gen_sol_inversionista_id',
           'file_1',
           'file_2',
           'file_3',
           'file_4',
           'file_5',
           'file_6',
           'bactivo',
    ];

    protected $date =['created_at','updated_at'];

    public function GenSolInversionista() {

     return $this->hasOne('App\Models\GenSolInversionista','id','gen_sol_inversionista_id');

    }
}
