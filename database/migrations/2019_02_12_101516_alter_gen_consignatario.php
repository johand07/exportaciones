<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterGenConsignatario extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('gen_consignatario', function (Blueprint $table) {
            //
            $table->string('rif_empresa', 11)->nullable()->after('nombre_consignatario');
            $table->string('siglas', 20)->nullable()->after('rif_empresa');
            $table->boolean('registro_directo')->default(0)->after('bactivo');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('gen_consignatario', function (Blueprint $table) {
            //
            $table->dropColumn('rif_empresa');
            $table->dropColumn('registro_directo');
            $table->dropColumn('siglas');
        });
    }
}
