@extends('templates/layoutlte')
@section('content')

<!--Modal de los codigos Arancelarios-->
@component('modal_dialogo',['arancel'=>$arancel])

@slot('header') <span>Código Arancelario</span> @endslot

@slot('body')  <div id="arancel"></div>

@endslot

@slot('footer')@endslot

@endcomponent

<!--Fin de la modal de codigos Arancelarios-->

  <div class="content">
	{{Form::model($productosjur,['route'=>['ProductosJuridicos.update',$id],'method'=>'PATCH','id'=>'FormProducJurid']) }}

@php $contador_prod=count($productosjur); @endphp

  {{Form::hidden('metodo','Editar')}}
  {{Form::hidden('contador_prod',$contador_prod)}}

<!--***************Productos Juridicos*****************************************************-->
            <div class="panel panel-primary">
              <div class="panel-heading">DATOS DE LOS PRODUCTOS QUE PRODUCE Y/O COMERCIALIZA</div>
             <div class="panel-body">
             	<div class="row">
                <div class="col-md-1"></div>
                   <div class="col-md-10 text-center">
                      {!! Form::label('','¿Exporta Productos de Servicios y/o Tecnología?',['class'=>'form-control'])!!}
                      {!! Form::label('','Si',['class'=>'radio-inline'])!!}
                      {!! Form::radio('prod_tecnologia','1',false,['class'=>'radio-inline','id'=>'prod_tecnologia_si'])!!}
                      {!! Form::label('','No',['class'=>'radio-inline'])!!}
                      {!! Form::radio('prod_tecnologia','2',false,['class'=>'radio-inline','id'=>'prod_tecnologia_no'])!!}

                    </div>
                  <div class="col-md-1"></div>
                </div><br>
                <div class="row" id="tecno_especifique" style="display: none;">
                  <div class="col-md-1"></div>
                  <div class="col-md-10">
                    {!! Form::label('','Especifique')!!}
                    {!!Form::text('especifique_tecno',null,['class'=>'form-control','id'=>'especifique_tecno','onkeypress'=>'return soloLetras(event)','maxlength'=>'100'])!!}
                  </div>
                  <div class="col-md-1"></div>
                </div>

               <div class="row" id="codigo_arancelarios" style="display: none;">
                   <div class="col-md-3"></div>
                    <div class="col-md-6 text-center">
                      {!!Form::radio('tipo_arancel','1',false,['class'=>'radio-inline','id'=>'cod_andina'])!!}
                      {!! Form::label('','Nandina',['class'=>'radio-inline'])!!}
                      {!! Form::radio('tipo_arancel','2',false,['class'=>'radio-inline','id'=>'cod_mercosur'])!!}
                      {!! Form::label('','Mercosur',['class'=>'radio-inline'])!!}
                    </div>
                  <div class="col-md-3"></div>
                </div><br>
                <table class="table table-bordered text-center" id="fields2" style="display: none;">
                    <tr><td><button  name="add2" type="button" id="add2" value="add more" class="btn btn-success">Agregar</button></td></tr>
                  <tr>
                    <th>Código Arancelario </th>
                    <th>Descripción Arancelaria</th>
                    <th>Descripción Comercial</th>
                    <th>Unidad de Medida</th>
                    <th>Capacidad Operativa Anual</th>
                    <th>Capacidad Instalada Anual</th>
                    <th>Capacidad de Almacenamiento Anual</th>
                    <th>Capacidad de Exportación</th>
                    <th></th>

                  </tr>
                  	@if (isset($productos))
                        @foreach ($productos as $key=>$producto)

			                <tr id="fila{{$key+1}}">

			                    <td>
			                    {!!Form::text('cod_arancel[]',$producto->cod_arancel,['class'=>'form-control','data-toggle'=>"modal",'data-target'=>"#modal",'id'=>'cod_arancel_edit','readonly'=>'true'])!!}
			                    {!!Form::hidden('id',$producto->id)!!}
			                    </td>
			                    <td>{!!Form::text('descrip_arancel[]',$producto->descrip_arancel,['class'=>'form-control','id'=>'descrip_arancel','onkeypress'=>'return soloLetras(event)'])!!}</td>
			                    <td>{!!Form::text('descrip_comercial[]',$producto->descrip_comercial,['class'=>'form-control','id'=>'descrip_comercial','onkeypress'=>'return soloLetras(event)'])!!}</td>
			                    <td>{!!Form::select('gen_unidad_medida_id[]',$unidad_medida,$producto->gen_unidad_medida_id,['placeholder'=>'--Seleccione una Unidad de Medida--', 'class'=>'form-control','id'=>'gen_unidad_medida_id']) !!}</td>
			                    <td>{!!Form::text('cap_opera_anual[]',$producto->cap_opera_anual,['class'=>'form-control','id'=>'cap_opera_anual','onkeypress'=>'return soloNumeros(event)'])!!}</td>
			                    <td>{!!Form::text('cap_insta_anual[]',$producto->cap_insta_anual,['class'=>'form-control','id'=>'cap_insta_anual','onkeypress'=>'return soloNumerosletrasyPorcentaje(event)'])!!}</td>
			                    <td>{!!Form::text('cap_alamcenamiento[]',$producto->cap_alamcenamiento,['class'=>'form-control','id'=>'cap_alamcenamiento','onkeypress'=>'return soloNumeros(event)'])!!}</td>
			                    <td> {!!Form::hidden('id[]',$producto->id)!!}<button  name="remove" type="button" id="{{$key+1}}" value="" class="btn btn-danger btn-remove2">x</button></td>
			                </tr>
                 		@endforeach
                    @endif
                </table>
                <div class="row">
                  <div class="col-md-12">
                    <div class="col-md-4"></div>
                    <div class="col-md-4 text-center">
                      <div class="form-group">
                        <a class="btn btn-primary" href="{{route('DatosEmpresa.index')}}" id="atras">Atrás</a>
                        <input type="submit" class="btn btn-success" value="Guardar" name="">
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

{{Form::close()}}
</div>

<script src="{{asset('js/ajax_v2.1.1.js')}}"></script>

<script type="text/javascript">


$(document).ready(function (){
 var xx = $('input[name="contador_prod"]').val();
 var j=parseInt(xx);
      $('#add2').click(function(){
       j++;

/*****************************************Campos dinamicos de codigos arancelarios********************************************************************/

   $('#fields2 tr:last').after('<tr id="fila'+j+'" display="block" class="show_div"><td>{!!Form::text('cod_arancel[]',null,['class'=>'form-control ejem','data-toggle'=>"modal",'data-target'=>"#modal",'id'=>"cod_arancel",'readonly'=>'true'])!!}</td><td>{!!Form::text('descrip_arancel[]',null,['class'=>'form-control','onkeypress'=>'return soloLetras(event)'])!!}</td><td>{!!Form::text('descrip_comercial[]',null,['class'=>'form-control','onkeypress'=>'return soloLetras(event)'])!!}</td><td>{!!Form::select('gen_unidad_medida_id[]',$unidad_medida,null,['placeholder'=>'--Seleccione una Unidad de Medida--', 'class'=>'form-control']) !!}</td><td>{!!Form::text('cap_opera_anual[]',null,['class'=>'form-control','onkeypress'=>'return soloNumeros(event)'])!!}</td><td>{!!Form::text('cap_insta_anual[]',null,['class'=>'form-control','onkeypress'=>'return soloNumerosletrasyPorcentaje(event)'])!!}</td><td>{!!Form::text('cap_alamcenamiento[]',null,['class'=>'form-control','onkeypress'=>'return soloNumeros(event)'])!!}</td><td>{!!Form::hidden('productos_id',$id)!!}{!!Form::hidden('id[]',0)!!}<button  name="remove" type="button" id="'+j+'" value="" class="btn btn-danger btn-remove4">x</button></td></tr>');


       });



$(document).on('click','.btn-remove',function(){

       var id_boton= $(this).attr("id");
       var id=$(this).prev().val();

    if(id!=0){

        if (confirm('Deseas eliminar este Producto?')) {

	      $.ajax({

	       'type':'get',
	       'url':'eliminarAccionista',
	       'data':{ 'id':id},
	       success: function(data){

	        //$('#row'+id_boton).remove();

	         }

	       });

        }



     }else{

      $('#row'+id_boton).remove();
     }



    });



     $(document).on('click','.btn-remove4',function(){
        var id_boton= $(this).attr("id");
        $("#fila"+id_boton+"").remove();

     });


    /****************************Validacion de radio tecnologia********************************************/

      $('#prod_tecnologia_si').click(function(event) {
        $('#fields2,#codigo_arancelarios').hide(1000).animate({height: "hide"}, 1000,"linear");
        $('#tecno_especifique').show(1000).animate({width: "show"}, 1000,"linear");

      });

       $('#prod_tecnologia_no').click(function(event) {
        $('#tecno_especifique').hide(1000).animate({height: "hide"}, 1000,"linear");
        $('#fields2,#codigo_arancelarios').show(1000).animate({width: "show"}, 1000,"linear");
      });


});

/*************************Script de los codigos arancelarios *******************************************************************/

  $('input:radio[name="tipo_arancel"]').change( function(){




     if($(this).is(':checked')) {

       var parametros = {
               'id': $(this).val()

       };
       var token= $('input[name="_token"]').val();
       console.log(token);
       $('#id_arancel').val(parametros.id);
      $('#cod_arancel').prop('disabled',false);
       $.ajax({
           type: 'GET',
           url: '/Arancel',
           data: {'arancel': $(this).val(),'_token': token},
           success: function(data){

              $table='<table class="table table-bordered table-hover" id="tabla"><tr><th>Código Arancelario</th><th>Descripción</th></tr>';

              $('#arancel').html($table);
              $.each(data,function(index,value){

             $('#tabla tr:last').after('<tr class="data"><td>'+value.codigo+'</td><td>'+value.descripcion+'</td></tr></table>');

              });

           }

       });

    }
});

$('#cod_arancel').show('modal');

/**------------------------------------------------------------------------**/
 var global_id=localStorage.setItem('arancel'," ");


 $(document).on('click','#cod_arancel',function(){

   var id=$(this).parents("tr").attr('id');
   localStorage.setItem('arancel',id);

});

 $(document).on('click','tr.data',function(){

  var id=localStorage.getItem('arancel');
  var cod=$(this).find("td").eq(0).text();
  var ara=$(this).find("td").eq(1).text();

  $("tr#"+id).find("td").eq(0).children("input").val(cod);
  $("tr#"+id).find("td").eq(1).children("input").val(ara);

  $('#modal button').click();
  $('#buscador_arancel').val("");

  });

 /*****************Script Buscador del Codigo Arancelario******************************************/

 $(document).on('keyup','#buscador_arancel',function(){

 var tipo=$('input[name="tipo_arancel"]').val();

   $.ajax({

      type:'get',
      url: '/Arancel',
      data: {'valor': $(this).val(), 'tipoAran':tipo},
      success: function(data){

        $table='<table class="table table-bordered table-hover" id="tabla"><tr><th>Código Arancelario</th><th>Descripción</th></tr>';

        $('#arancel').html($table);
        if(data.length==0){

         $('#tabla tr:last').after('<tr><td colspan="2" style="color:red;text-align:center" >Códigos no existen</td></tr>');

       }else{
        $.each(data,function(index,value){

       $('#tabla tr:last').after('<tr class="data"><td>'+value.codigo+'</td><td>'+value.descripcion+'</td><tr></table');

        });

      }
      }

   });

});

</script>
@stop
