@extends('templates/layoutlte')
@section('content')
@php $contador=count($cuotas); @endphp
{{Form::hidden('metodo','Editar')}}
{{Form::hidden('contador',$contador)}}
{{Form::model($factura,['route'=>['factura.update',$factura->id],'method'=>'PATCH', 'name' => 'facturaForm','id' => 'facturaFormId'])}}

  @include('factura.fields')
{{Form::close()}}
<script src="{{asset('js/ajax_v2.1.1.js')}}"></script>
<script>
    $(document).ready(function () {
        @stack('readyscripts')
    });
</script>
@stop
