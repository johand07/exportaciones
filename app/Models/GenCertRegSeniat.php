<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GenCertRegSeniat extends Model
{
    protected $table = 'gen_cert_reg_seniat';
    protected $fillable = [

           'id',
           'gen_usuario_id',
           'gen_status_id',
           'codigo_arancelario',
           'fecha_certificado',
           'observacion_analista',
           'observacion_viceministro',
           'bactivo',
    ];

    protected $date =['created_at','updated_at'];
   
        public function GenUsuario() {
   
        return $this->hasOne('App\Models\GenUsuario','id','gen_usuario_id');
   
       }

       public function GenStatus() {

        return $this->hasOne('App\Models\GenStatus','id','gen_status_id');
   
       }

}
