<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>CERTIFICADO DE ORIGEN <br><br>ACUERDO DE COMERCIO DE LOS PUEBLOS PARA LA COMPLEMENTARIEDAD ECONÓMICA, PRODUCTIVA Y EL INTERCAMBIO COMERCIAL <br>SOLIDARIO, ENTRE EL GOBIERNO DE LA REPUBLICA BOLIVARIANA DE VENEZUELA Y EL ESTADO PLURINACIONAL DE BOLIVIA</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style type="text/css">
        .centrar{
            text-align: center;
        }

        .titulo{
            
            font-weight: bold;
        }

        .nro-correlativo{
            color: red;
            font-size: 16px;
        }

        .derecha{
            text-align: right;

        }

        .text-normal{
            font-size: 10px;
        }

        .text-medio{
            font-size: 11px;
        }
        .text-11{
            font-size: 11px;
        }
        .text-small{
            font-size: 10px;
        }

        .text-9{
            font-size: 8px;
        }

        .text-8{
            font-size: 8px;
        }

        .espacio-superior{
            margin-top: 10px;
        }

        .espacio-superior-small{
            margin-top: 5px;
        }

        .ancho-medio{
            width: 50%;
        }

        .ancho-full{
            width: 100%;
        }

        .ancho-medio-alto{
            width: 75%;
        }

        .ancho-90{
            width: 90%;
        }

        .table{
            border-collapse: collapse;
        }
        
        .borde{
            border: 1px solid #000;
        }

        .sin-borde{
            border: none;
        }

        .top{
            vertical-align: top;
        }

        .under-dotted{    
            border-bottom: 1px dotted #000;
            text-decoration: none;
        }

        .content-1{
            width: 5%;
        }

        .content-9{
            width: 20%;
        }

        .sangria-1{
            margin-left: 28px;
        }

        .sangria-2{
            margin-left: 36px;
        }

        .sangria-3{
            margin-left: 54px;
        }

        .sangria-sub{
            margin-left: 14px;
            
        }

        
        .page-break { page-break-before: always; }

        .table-center{
            margin: 0 auto; 
        }

        .justify{
            text-align: justify;
        }

        td.justify {
            padding-left: 8px;
            padding-right: 8px; 
        }
        .lineal{
            
        }
        .lineal>div{
            max-width:91%;

        }
        .lineal>*{
           display: inline-block;
           vertical-align:top;
        }

        .sangria-lista{
            list-style-type: none;
            margin: 0;
            padding: 0;
            margin-bottom: 4px;
        }

        .sangria-lista>li{
            margin-top: 5px;
            padding-left: 10px;
            padding-right: 10px;
        }
    </style>
</head>
<body>
    
    <!--div class="derecha titulo text-medio"><b style="color:red">N°</b> del Certificado <span class="">{{ $certificado->num_certificado}}</span></div-->
    <div class="centrar titulo text-normal espacio-superior">CERTIFICADO DE ORIGEN <br><br>ACUERDO DE COMERCIO DE LOS PUEBLOS PARA LA COMPLEMENTARIEDAD ECONÓMICA, PRODUCTIVA Y EL INTERCAMBIO COMERCIAL <br>SOLIDARIO, ENTRE EL GOBIERNO DE LA REPUBLICA BOLIVARIANA DE VENEZUELA Y EL ESTADO PLURINACIONAL DE BOLIVIA</div><br>
    <div class="centrar espacio-superior">
        <table border="0" class="ancho-full">
            <tr>
                <td class="ancho-medio centrar">
                    <div class="text-normal titulo">(1) PAÍS EXPORTADOR: <u>{{$certificado->pais_exportador}}</u></div>
                </td>
                <td class="ancho-medio centrar">
                    <div class="text-normal titulo">(2) PAÍS IMPORTADOR: <u>{{$certificado->pais_importador}}</u></div>
                </td>
            </tr>
        </table>
    </div>
    <!--Productos-->
     <div class="centrar espacio-superior">
        <table class="ancho-full borde table">
            <tr class="text-11">
               <td class="centrar top titulo borde" style="width: 11%;">(3) No. de Orden</td>
                <td class="centrar top titulo borde" style="width: 19%;">(4) Código Arancelario</td>
                <td class="centrar top titulo borde" style="width: 30%;">(5) Denominación de las Mercancías</td>
                <td class="centrar top titulo borde" style="width: 10%;">(6) Peso o Cantidad</td>
                <td class="centrar top titulo borde" style="width: 17%;">(7) Valor FOB en (U$S)</td>
            </tr>
           
            @foreach($detalleProduct as $key => $value)
            
                <tr class="text-medio">
                    <td class="centrar top">{{$value->num_orden}}</td>
                    <td class="centrar top">{{$value->codigo_arancel}}</td>
                    <td class="centrar top">{{$value->denominacion_mercancia}}</td>
                    <td class="centrar top">{{$value->unidad_fisica}}</td>
                    <td class="centrar top">{{$value->valor_fob}}</td>
                </tr>
            @endforeach

            @if(count($detalleProduct)<=1)
                @for($auxConut = 0; $auxConut<=3-count($detalleProduct); $auxConut++)
                 <tr class="text-medio">
                    <td class="centrar top">&nbsp;</td>
                    <td class="centrar top">&nbsp;</td>
                    <td class="centrar top">&nbsp;</td>
                    <td class="centrar top">&nbsp;</td>
                    <td class="centrar top">&nbsp;</td>
                    <td class="centrar top">&nbsp;</td>
                    <td class="centrar top">&nbsp;</td>
                </tr>
                @endfor
            @endif
        </table>
    </div>
    <!--Productos-->
    <div class="centrar titulo text-medio espacio-superior">(8) DECLARACION DE ORIGEN</div>
    <div class="text-medio espacio-superior">DECLARAMOS que las mercancías indicadas en el presente formulario, correspondientes a la Factura Comercial No. <b>EX</b><u>{{ $detdeclaracionCert[0]->numero_factura }}</u>
    de fecha <u>{{ $detdeclaracionCert[0]->f_fac_certificado }}</u> cumplen con lo establecido en las Normas de Origen del presente Acuerdo de Comercio de los Pueblos para la Complementariedad Económica, Productiva de conformidad con el siguiente desglose.</div>
<!--Declaracion-->
<!--Detalle declaracion-->
<div class="centrar espacio-superior">
        <table class="ancho-full borde table">
            <tr class="text-medio">
                <td class="centrar top titulo borde" style="width: 15%;">(9) No. de Orden</td>
                <td class="centrar top titulo borde" style="width: 85%;">(10) Normas</td>
            </tr>
            @foreach($detdeclaracionCert as $key =>$value1)
            
                 <tr class="text-center">
                    <td class="centrar top">{{$value1->num_orden_declaracion}}</td>
                    <td class="centrar top">{{$value1->normas_criterio}}</td>
                </tr>

            @endforeach

            @if(count($detdeclaracionCert)<=1)
                @for($auxConut = 0; $auxConut<=3-count($detdeclaracionCert); $auxConut++)
                <tr class="text-medio">
                    <td class="centrar top">&nbsp;</td>
                    <td class="centrar top">&nbsp;</td>
                </tr>
                @endfor
            @endif
        
        </table>
    </div>

    <div class="centrar espacio-superior">
        <table class="ancho-full table borde">
            <tr class="text-medio">
                <td class="top titulo borde text-medio" colspan="52">(11) EXPORTADOR O PRODUCTOR</td>
                 <td class="top titulo borde text-medio" colspan="48">(12) Sello y firma del Productor o Exportador</td>
            </tr>
           
           <tr class="text-medio">
                <td class="top borde" colspan="52">11.1 Nombre o Razón Social: {{ $certificado->GenUsuario->DetUsuario->razon_social }}</td>
                <td colspan="2"></td>
            </tr>
           <tr class="text-medio">
                <td class="top borde" colspan="52">11.2 Dirección: {{ $certificado->GenUsuario->DetUsuario->direccion }}</td>
                <td colspan="2"></td>
            </tr>
            <tr class="text-medio">
                <td class="top borde" colspan="52">11.3 Fecha: {{ $certificado->fecha_emision_exportador}}</td>
                <td colspan="2"></td>
            </tr>
            <tr class="text-medio">
                <td class="top titulo borde" colspan="52">(13) IMPORTADOR</td>
                <td colspan="2"></td>
            </tr>
            <tr class="text-medio">
                <td class="top borde" colspan="52">13.1 Nombre o Razón Social: {{ strtoupper($importadorCertificado->razon_social_importador) }}</td>
                <td colspan="2"></td>
            </tr>
            <tr class="text-medio">
                <td class="top borde" colspan="52">13.2 Dirección: {{ strtoupper($importadorCertificado->direccion_importador) }}</td>
                <td colspan="2"></td>
            </tr>
            <tr class="text-medio">
                <td class="top borde" colspan="52">(14) Medio de Transporte: {{ ($importadorCertificado->medio_transporte_importador) }}</td>
                <td colspan="2"></td>
            </tr>

            <tr class="text-medio">
                <td class="top borde" colspan="52">(15) Puerto o Lugar de Embarque: {{ ($importadorCertificado->lugar_embarque_importador) }}</td>
                <td colspan="2"></td>
            </tr>
            <tr class="text-medio">
                <td class="top text-normal titulo borde" colspan="100">(16)<b> OBSERVACIONES </b></td> 
            </tr>
            <tr>
                <td class="borde" colspan="100">{{ $certificado->observaciones}} <br><br></td>
            </tr>
        </table>
    </div>
     <div class="centrar espacio-superior">
        <table class="ancho-full borde table">
            <tr class="text-medio">
                <td class="top titulo borde table" colspan="50">(17) CERTIFICACIÓN DEL ORIGEN</td>
                <td class="top titulo borde table" colspan="50" rowspan="3">(18) Sello y firma de funcionario habilitado y sello de la Autoridad <br>Competente:</td>
            </tr>
            <tr class="text-medio">
                <td class="top text-small" colspan="50"><div class="">Certifico la veracidad de la presente Declaracion en la ciudad de:<br><span class="under-dotted espacio-superior">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><br><span class="espacio-superior">A los: <u>{{date('d')}}</u>/<u>{{date('m')}}</u>/<u>{{date('Y')}}</u><br><br><br><br><br></span></div></td>
            </tr>
            <tr class="text-medio">
                <td class="top text-small borde table" colspan="50"><div class="">(19) Nombre de la Autoridad Competente:<br><br><span class="">&nbsp;</span>Ministerio del Poder Popular Para el Comercio Exterior <br> e Inversion Internacional <br><br><br><br><br></div></td>
            </tr>
        </table>
    </div>
    <div class="justify text-small">Referencias (1) Esta columna indica el orden en que se individualizan las mercancias comprendidas en el presente certificado. En caso de ser insuficiente, se continuara la individualizacion de las mercancias en ejemplares suplementarios de este certificado,numerados correlativamente. <br>(2) En esta columna se identifica la Norma de Origen que cumple cada mercancia individualizada por su numero de orden <br> Notas:(a) El formulario no podra presentar raspadura, tachaduras o enmiendas. (b) El formulario solo sera valido si todos sus campos, excepto el de "Observaciones", estuvieran debidamente llenos.</div>
    <br>
<h5 class="centrar">1</h5>
    

</body>
</html>