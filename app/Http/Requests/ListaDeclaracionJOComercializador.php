<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ListaDeclaracionJOComercializador extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'rif.*'=>'required',
            'razon_social.*'=>'required',
            'siglas.*'=>'required',
            'codigo_arancel_c.*'=>'required',
            'descripcion_arancelaria_c.*'=>'required',
            'descripcion_comercial_c.*'=>'required',
        ];
    }
}
