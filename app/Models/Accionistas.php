<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Accionistas extends Model
{
    protected $table = 'accionistas';



	protected $fillable = [

           'gen_usuario_id',
           'nombre_accionista',
           'apellido_accionista',
           'cedula_accionista',
           'cargo_accionista',
           'telefono_accionista',
           'nacionalidad_accionista',
           'participacion_accionista',
           'correo_accionista',
           'bactivo',
    ];

    protected $date =['created_at','updated_at'];

    public function usuarios() {

     return $this->hasOne('App\Models\GenUsuario','id','gen_usuario_id');

    }
     


}
