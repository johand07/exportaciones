<?php

use Illuminate\Database\Seeder;
use App\Models\CatDocumentos;

class CatDocumentos extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $catDocumentos = [
            [
                'id'         => 47,
                'gen_ente_id'=> 1,
                'nombre_documento'=> 'Documento(s) de Embarque (Bill Of lading, guiá aérea, CPIC, guiá de entrega o despacho).',
                'created_at' => '2021-07-20 20:59:08',
                'updated_at' => '2021-07-20 20:59:08',
            ],
            [
                'id'         => 48,
                'gen_ente_id'=> 1,
                'nombre_documento'=> 'Nota de Crédito asociado a la Factura Comercial, si fuese el caso.',
                'created_at' => '2022-05-09 20:59:08',
                'updated_at' => '2022-05-09 20:59:08',
            ],
            
            
            
        ];

        CatDocumentos::insert($catDocumentos);
    }
}
