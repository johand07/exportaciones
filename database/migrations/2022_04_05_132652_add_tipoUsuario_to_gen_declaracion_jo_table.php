<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTipoUsuarioToGenDeclaracionJoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('gen_declaracion_jo', function (Blueprint $table) {
            
            $table->string('tipoUsuario', 255)->nullable()->after('gen_usuario_id');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('gen_declaracion_jo', function (Blueprint $table) {
            //
            $table->dropColumn('tipoUsuario');
            
        });
    }
}
