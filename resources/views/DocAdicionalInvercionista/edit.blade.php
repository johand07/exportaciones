@extends('templates/layoutlte')
@section('content')
<div class="content">
<div class="panel-group" style="background-color: #fff;">
      <div class="panel-primary">
        <div class="panel-heading"><h3>Carga de Documentos Adicionales</h3></div>
        <div class="panel-body">
          @if(!empty($inver->estado_observacion) && $inver->estado_observacion == 1 )
            <script>

              document.onreadystatechange = function () {
                var state = document.readyState;
                if (state == 'complete') {
                  swal("Estimado usuario debera corregír las siguientes observaciones", "{{$inver->observacion_inversionista}}", "warning");
                }
              }
            </script>
            <br>

            <div class="alert alert-danger" role="alert">
              <strong>
              Observación Indicada por el Analista:
                <br>
                <ul>
                    <li>{{$inver->observacion_inversionista}}</li>
                </ul>
              </strong>
            </div>

          @endif

                  <div class="row" style="background-color: #fff">
                      <div class="col-md-12">
                  @if(isset($detDocAdicionalInversiones))
                  {{ Form::model($detDocAdicionalInversiones, array('route' => array('DocAdicionalInvercionista.update', $detDocAdicionalInversiones->id), 'method' => 'PUT','enctype' => 'multipart/form-data')) }}   

                          <div class="row">
                              <div class="col-md-12">
                                  <table border="0" class="col-md-12">
                                    <thead>
                                      <tr>
                                        <th class="text-center" width="50%" height="40px"><h3><b>Sección de Carga</b></h3></th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                  <input type="hidden" name="gen_sol_inversionista_id" value="{{$gen_sol_inversionista_id}}" placeholder="" id="gen_sol_inversionista_id">
                                      <tr>
                                        <td class="text-center" valign="top" > 
                                          <span><b>1.)</b></span>
                                          <span class="btn btn-default btn-file" style="margin: 3px;">
                                              Cargar Archivo <span class="glyphicon glyphicon-picture"></span>
                                              {!! Form::file('file_1', array('class' => 'file-input','style'=>'margin: 10px;')) !!}
                                          </span>
                                        </td>
                                        <td class="text-center" height="40px"><img id="vista_previa_file_1" src="" alt="" width="300px" style="margin: 3px;"><img id="imgdocumento_file_1" src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px; display: none;">
                                          
                                          @if(isset($detDocAdicionalInversiones->file_1))
                                            <img id="imgdocumentoEdit_file_1" src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px;">
                                          @endif
                                        </td>
                                      </tr>

                                      <tr>
                                        <td class="text-center" valign="top" >
                                          <span><b>2.)</b></span> 
                                          <span class="btn btn-default btn-file" style="margin: 3px;">
                                              Cargar Archivo <span class="glyphicon glyphicon-picture"></span>
                                              {!! Form::file('file_2', array('class' => 'file-input','style'=>'margin: 10px;')) !!}
                                          </span>
                                        </td>
                                        <td class="text-center" height="40px"><img id="vista_previa_file_2" src="" alt="" width="300px" style="margin: 3px;"><img id="imgdocumento_file_2" src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px; display: none;">
                                          @if(isset($detDocAdicionalInversiones->file_2))
                                            <img id="imgdocumentoEdit_file_2" src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px;">
                                          @endif
                                        </td>
                                      </tr>

                                      <tr>
                                        <td class="text-center" valign="top" >
                                          <span><b>3.)</b></span> 
                                          <span class="btn btn-default btn-file" style="margin: 3px;">
                                              Cargar Archivo <span class="glyphicon glyphicon-picture"></span>
                                              {!! Form::file('file_3', array('class' => 'file-input','style'=>'margin: 10px;')) !!}
                                          </span>
                                        </td>
                                        <td class="text-center" height="40px"><img id="vista_previa_file_3" src="" alt="" width="300px" style="margin: 3px;"><img id="imgdocumento_file_3" src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px; display: none;">
                                          @if(isset($detDocAdicionalInversiones->file_3))
                                            <img id="imgdocumentoEdit_file_3" src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px;">
                                          @endif
                                        </td>
                                      </tr>

                                      <tr>
                                        <td class="text-center" valign="top" >
                                          <span><b>4.)</b></span> 
                                          <span class="btn btn-default btn-file" style="margin: 3px;">
                                              Cargar Archivo<span class="glyphicon glyphicon-picture"></span>
                                              {!! Form::file('file_4', array('class' => 'file-input','style'=>'margin: 10px;')) !!}
                                          </span>
                                        </td>
                                        <td class="text-center" height="40px"><img id="vista_previa_file_4" src="" alt="" width="300px" style="margin: 3px;"><img id="imgdocumento_file_4" src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px; display: none;">
                                          @if(isset($detDocAdicionalInversiones->file_4))
                                            <img id="imgdocumentoEdit_file_4" src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px;">
                                          @endif
                                        </td>
                                      </tr>

                                      <tr>
                                        <td class="text-center" valign="top" >
                                          <span><b>5.)</b></span> 
                                          <span class="btn btn-default btn-file" style="margin: 3px;">
                                              Cargar Archivo <span class="glyphicon glyphicon-picture"></span>
                                              {!! Form::file('file_5', array('class' => 'file-input','style'=>'margin: 10px;')) !!}
                                          </span>
                                        </td>
                                        <td class="text-center" height="40px"><img id="vista_previa_file_5" src="" alt="" width="300px" style="margin: 3px;"><img id="imgdocumento_file_5" src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px; display: none;">
                                          @if(isset($detDocAdicionalInversiones->file_5))
                                            <img id="imgdocumentoEdit_file_5" src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px;">
                                          @endif
                                        </td>
                                      </tr>

                                      <tr>
                                        <td class="text-center" valign="top" >
                                          <span><b>6.)</b></span> 
                                          <span class="btn btn-default btn-file" style="margin: 3px;">
                                              Cargar Archivo <span class="glyphicon glyphicon-picture"></span>
                                              {!! Form::file('file_6', array('class' => 'file-input','style'=>'margin: 10px;')) !!}
                                          </span>
                                        </td>
                                        <td class="text-center" height="40px"><img id="vista_previa_file_6" src="" alt="" width="300px" style="margin: 3px;"><img id="imgdocumento_file_6" src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px; display: none;">
                                          @if(isset($detDocAdicionalInversiones->file_6))
                                            <img id="imgdocumentoEdit_file_6" src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px;">
                                          @endif
                                        </td>
                                      </tr>

                                    </tbody>
                                  </table>
                              </div>
                          </div>
<hr>
                          <div class="row" style="background-color: #fff">

                                <div class="col-md-12" align="center">

                                     <input type="submit"  name="submit" id="submit" class="btn btn-primary"  value="Guardar" >
                                      <a href="{{ route('BandejaInversionista.index')}}" class="btn btn-danger">Cancelar</a>

                                </div>
                              
                            </div>


                  {{Form::close()}}
                  @else 
                    {!! Form::open(['route' =>'DocAdicionalInvercionista.store' ,'method'=>'POST', 'enctype' => 'multipart/form-data']) !!}

                            <div class="row">
                                <div class="col-md-12">
                                    <table border="0" class="col-md-12">
                                      <thead>
                                        <tr>
                                          <th class="text-center" width="50%" height="40px"><h3><b>Sección de Carga</b></h3></th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                    <input type="hidden" name="gen_sol_inversionista_id" value="{{$gen_sol_inversionista_id}}" placeholder="" id="gen_sol_inversionista_id">
                                        <tr>
                                          <td class="text-center" valign="top" > 
                                            <span><b>1.)</b></span>
                                            <span class="btn btn-default btn-file" style="margin: 3px;">
                                                Cargar Archivo <span class="glyphicon glyphicon-picture"></span>
                                                {!! Form::file('file_1', array('class' => 'file-input','style'=>'margin: 10px;')) !!}
                                            </span>
                                          </td>
                                          <td class="text-center" height="40px"><img id="vista_previa_file_1" src="" alt="" width="300px" style="margin: 3px;"><img id="imgdocumento_file_1" src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px; display: none;"></td>
                                        </tr>

                                        <tr>
                                          <td class="text-center" valign="top" >
                                            <span><b>2.)</b></span> 
                                            <span class="btn btn-default btn-file" style="margin: 3px;">
                                                Cargar Archivo <span class="glyphicon glyphicon-picture"></span>
                                                {!! Form::file('file_2', array('class' => 'file-input','style'=>'margin: 10px;')) !!}
                                            </span>
                                          </td>
                                          <td class="text-center" height="40px"><img id="vista_previa_file_2" src="" alt="" width="300px" style="margin: 3px;"><img id="imgdocumento_file_2" src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px; display: none;"></td>
                                        </tr>

                                        <tr>
                                          <td class="text-center" valign="top" >
                                            <span><b>3.)</b></span> 
                                            <span class="btn btn-default btn-file" style="margin: 3px;">
                                                Cargar Archivo <span class="glyphicon glyphicon-picture"></span>
                                                {!! Form::file('file_3', array('class' => 'file-input','style'=>'margin: 10px;')) !!}
                                            </span>
                                          </td>
                                          <td class="text-center" height="40px"><img id="vista_previa_file_3" src="" alt="" width="300px" style="margin: 3px;"><img id="imgdocumento_file_3" src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px; display: none;"></td>
                                        </tr>

                                        <tr>
                                          <td class="text-center" valign="top" >
                                            <span><b>4.)</b></span> 
                                            <span class="btn btn-default btn-file" style="margin: 3px;">
                                                Cargar Archivo<span class="glyphicon glyphicon-picture"></span>
                                                {!! Form::file('file_4', array('class' => 'file-input','style'=>'margin: 10px;')) !!}
                                            </span>
                                          </td>
                                          <td class="text-center" height="40px"><img id="vista_previa_file_4" src="" alt="" width="300px" style="margin: 3px;"><img id="imgdocumento_file_4" src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px; display: none;"></td>
                                        </tr>

                                        <tr>
                                          <td class="text-center" valign="top" >
                                            <span><b>5.)</b></span> 
                                            <span class="btn btn-default btn-file" style="margin: 3px;">
                                                Cargar Archivo <span class="glyphicon glyphicon-picture"></span>
                                                {!! Form::file('file_5', array('class' => 'file-input','style'=>'margin: 10px;')) !!}
                                            </span>
                                          </td>
                                          <td class="text-center" height="40px"><img id="vista_previa_file_5" src="" alt="" width="300px" style="margin: 3px;"><img id="imgdocumento_file_5" src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px; display: none;"></td>
                                        </tr>

                                        <tr>
                                          <td class="text-center" valign="top" >
                                            <span><b>6.)</b></span> 
                                            <span class="btn btn-default btn-file" style="margin: 3px;">
                                                Cargar Archivo <span class="glyphicon glyphicon-picture"></span>
                                                {!! Form::file('file_6', array('class' => 'file-input','style'=>'margin: 10px;')) !!}
                                            </span>
                                          </td>
                                          <td class="text-center" height="40px"><img id="vista_previa_file_6" src="" alt="" width="300px" style="margin: 3px;"><img id="imgdocumento_file_6" src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px; display: none;"></td>
                                        </tr>

                                      </tbody>
                                    </table>
                                </div>
                            </div>
  <hr>
                            <div class="row" style="background-color: #fff">

                                  <div class="col-md-12" align="center">

                                       <input type="submit"  name="submit" id="submit" class="btn btn-primary"  value="Guardar" >
                                      <a class="btn btn-danger btn-md" href="{{ url('exportador/DeclaracionJO/create') }}">Cancelar</a>

                                  </div>
                                
                              </div>


                    {{Form::close()}}
                  @endif
                      </div>
                  </div>

        </div>
      </div>
    </div>

  </div><!-- Fin content -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script>

   function cambiarImagen(event) {
    var id= event.target.getAttribute('name');
    var reader = new FileReader();
    reader.onload = function(){
      var output = document.getElementById('vista_previa_'+id);
      output.src = reader.result;
    };
    var archivo = $(".file-input").val();
    var extensiones = archivo.substring(archivo.lastIndexOf(".")).toLowerCase();
    //alert(archivo);
    //var imgfile="/exportaciones/public/img/file.png";

//alert(imgfile);
    if(extensiones != ".jpeg" && extensiones != ".png" && extensiones != ".jpg" && extensiones != ".gif"){
        $('#imgdocumento_'+id).show();
        $('#imgdocumentoEdit_'+id).hide();
    }else{
      $('#imgdocumento_'+id).hide();
      reader.readAsDataURL(event.target.files[0]);
    }

    
  
  }

    $(document).on('change', '.file-input', function(evt) {
           cambiarImagen(evt);
    });


</script>

@endsection