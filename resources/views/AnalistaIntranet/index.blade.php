@extends('templates/layoutlte_analista_intranet')
@section('content')
<div class="row  text-center">
    <h3>Solicitudes Asignadas</h3>
</div>
<div class="row">
    <div class="col-md-12">
        <table id="asignadasIntra" class="table" style="width:100%">
            <thead>
                <tr class="text-center">
                    <th class="col-md-2 text-center">Rif</th>  
                    <th class="col-md-2 text-center">Razon Social</th>  
                    <th class="col-md-2 text-center">Nº Solicitud</th> 
                    <th class="col-md-2 text-center">Tipo</th>        
                    <th class="col-md-3 texstatus-center">OCA</th>
                    <th class="col-md-2 text-center">Asignado por</th>
                    <th class="col-md-2 text-center">Acciones</th>
                
                    
                </tr>
            </thead>
            <tbody>
                @foreach($solicitudes as $key =>  $solicitud)
                    @if($solicitud->bactivo == 1)
                        <tr>           
                            <td class="text-center"  > {{ $solicitud->rif }}</td>          
                            <td class="text-center"  >{{ $solicitud->razon_social }}</td>
                            <td class="text-center"  ><b class="text-primary">{{ $solicitud->gen_solicitud_id }}</b></td>
                            <td class="text-center"  >{{ $solicitud->solicitud }}</td>
                            <td class="text-center"  >{{ $solicitud->nombre_oca }}</td>
                            <td class="text-center"  >{{ $solicitud->asignado_por }}</td>
                            <td>
                                @if($solicitud->gen_status_id == 6)
                                    <a href="{{route('AnalistaIntranet.create', ['gen_solicitud_id'=>encrypt($solicitud->gen_solicitud_id)])}}" class="btn btn-danger"> Atender</a>
                                @endif
                                @if($solicitud->gen_status_id == 30)
                                    <a href="{{route('AnalistaIntranet.create', ['gen_solicitud_id'=>encrypt($solicitud->gen_solicitud_id)])}}" class="btn btn-warning"> Continuar</a>
                                @endif
                            </td>
                        
                        </tr>   
                    @endif
                @endforeach
            </tbody>
        </table>
    </div>
    
</div>
@stop