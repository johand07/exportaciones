<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CatCargados extends Model
{
    protected $table = 'cat_cargados';

    protected $guarded = [];
}
