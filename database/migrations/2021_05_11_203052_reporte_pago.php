<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ReportePago extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reporte_pago', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('tipo_pago_id')->index()->nullable();
            $table->foreign('tipo_pago_id')->references('id')->on('tipo_pago');
            $table->unsignedInteger('cat_banco_admin_id')->index()->nullable();
            $table->foreign('cat_banco_admin_id')->references('id')->on('cat_banco_admin');
            $table->unsignedInteger('gen_operador_cambiario_id')->index()->nullable();
            $table->foreign('gen_operador_cambiario_id')->references('id')->on('gen_operador_cambiario');
            $table->unsignedInteger('gen_status_id')->index()->nullable();
            $table->foreign('gen_status_id')->references('id')->on('gen_status');
            $table->unsignedInteger('gen_sol_inversionista_id')->index()->nullable();
            $table->foreign('gen_sol_inversionista_id')->references('id')->on('gen_sol_inversionista');
            $table->unsignedInteger('gen_dvd_solicitud_id')->index()->nullable();
            $table->foreign('gen_dvd_solicitud_id')->references('id')->on('gen_dvd_solicitud');
            $table->unsignedInteger('gen_declaracion_jo_id')->index()->nullable();
            $table->foreign('gen_declaracion_jo_id')->references('id')->on('gen_declaracion_jo');
            $table->unsignedInteger('gen_certificado_id')->index()->nullable();
            $table->foreign('gen_certificado_id')->references('id')->on('gen_certificado');
            $table->string('num_referencia',100)->nullable();
            $table->date('fecha_reporte')->nullable();
            $table->string('img_capture',200)->nullable();
            $table->date('fstatus')->nullable();
            $table->boolean('bactivo')->default(1);
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reporte_pago');
    }
}
