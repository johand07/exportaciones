<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GenDeclaracionInversion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gen_declaracion_inversion', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('cat_tipo_usuario_id')->index();
            $table->foreign('cat_tipo_usuario_id')->references('id')->on('cat_tipo_usuario');
            $table->unsignedInteger('gen_status_id')->index();
            $table->foreign('gen_status_id')->references('id')->on('gen_status');
            $table->string('num_declaracion', 150)->unique();
            $table->date('fstatus')->nullable();
            $table->boolean('bactivo')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gen_declaracion_inversion');
    }
}
