<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DetConvenioPais extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('det_convenio_pais', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('pais_id')->index()->nullable();
            $table->foreign('pais_id')->references('id')->on('pais');
            $table->unsignedInteger('cat_tipo_convenio_id')->index()->nullable();
            $table->foreign('cat_tipo_convenio_id')->references('id')->on('cat_tipo_convenio');
            $table->unsignedInteger('acuerdo')->index()->nullable();
            $table->string('descripcion', 200)->nullable();
            $table->boolean('bactivo')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
         Schema::dropIfExists('det_pais_convenio');
    }
}
