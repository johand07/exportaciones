<?php

namespace App\Http\Controllers;

use App\models\GenUsuario;
use App\Models\CatPreguntaSeg;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Laracasts\Flash\Flash;
use App\Http\Requests\GenUsuarioRequest;
use App\Http\Requests\CamPreSegRequest;
use Alert;
use Auth;

class CambioPreSegController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\models\GenUsuario  $genUsuario
     * @return \Illuminate\Http\Response
     */
    public function show(GenUsuario $genUsuario)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\models\GenUsuario  $genUsuario
     * @return \Illuminate\Http\Response
     */
    public function edit(GenUsuario $genUsuario, $id)
    {
        $titulo= 'Cambio de Pregunta de Seguridad';
        $descripcion= 'Registra tu nueva pregunta';
        $usuario=GenUsuario::find($id);
        $preguntaseg=CatPreguntaSeg::all()->pluck('descripcion_preg','id');
        return view('CambioPregunta.edit', compact('usuario','preguntaseg','titulo','descripcion'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\models\GenUsuario  $genUsuario
     * @return \Illuminate\Http\Response
     */
    public function update(CamPreSegRequest $request, GenUsuario $GenUsuario, $id)
    {
    if (isset($request->cat_preguntas_seg_id)) {
      if($request->cat_preguntas_seg_id==8) {

         $pregunta=$request['pregunta_alter'];

      }else{

        $pregunta='N/A';

      }

    }

      $usuario=GenUsuario::find($id);
      $usuario->cat_preguntas_seg_id=$request->cat_preguntas_seg_id;
      $usuario->respuesta_seg=$request->respuesta_seg;
      $usuario->pregunta_alter=$pregunta;
      $usuario->save();
      Alert::success('Actualización Exitosa!', 'Actulización Exitosa.')->persistent("OK");
        return redirect('/home');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\models\GenUsuario  $genUsuario
     * @return \Illuminate\Http\Response
     */
    public function destroy(GenUsuario $genUsuario)
    {
        //
    }
}
