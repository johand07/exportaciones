<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>MERCOSUR-VENEZUELA</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/style_Certificado.css">
</head>
<body>
    
    <!--div class="derecha titulo text-medio"><b style="color:red">N°</b> del Certificado <span class="">{{ $certificado->num_certificado}}</span></div-->
    <!--div class="centrar titulo text-normal espacio-superior">CERTIFICADO DE ORIGEN<br>ACUERDO MERCOSUR-COLOMBIA, ECUADOR Y VENEZUELA</div-->
    <div class="centrar titulo text-normal espacio-superior">
        <table border="0" class="ancho-full">
            <tr>
                <td></td>
                <td class="ancho-medio centrar" colspan="3">
                    <div class="text-normal">CERTIFICADO DE ORIGEN </div>
                </td>
                
                <td></td>
                <td></td>
                
            </tr>
            <tr>
                <td></td>
                <td class="ancho-medio centrar" colspan="3">
                <div class="text-normal"> ACUERDO MERCOSUR-COLOMBIA, ECUADOR Y VENEZUELA</div>
                </td>
                <td></td>
                <td></td>
            </tr>
        </table>
    </div>
    <div class="centrar espacio-superior">
        <table border="1" class="ancho-full">
        <tr>
            <td class="ancho-medio centrar" colspan="3" style="height:30px;">
            <div class="text-normal">(1) PAÍS EXPORTADOR: <u>{{$certificado->pais_exportador}}</u></div>
            </td>
            <td class="ancho-medio centrar" colspan="3" style="height:30px;">
            <div class="text-normal">(2) PAÍS IMPORTADOR: <u>{{$certificado->pais_importador}}</u></div>
            </td>
        </tr>
        </table>
    </div>

    <div class="centrar espacio-superior">
        <table class="ancho-full borde table" boder="1">
            <tr class="text-11 ">
                <td class="centrar top titulo borde" style="width: 13%;" colspan="1">3. No. de Orden</td>
                <td class="centrar top titulo borde" style="width: 19%;" colspan="1">4. NALADISA</td>
                <td class="centrar top titulo borde" style="width: 75%;" colspan="1">5. DENOMINACIÓN DE LA MERCANCIA</td>
                <td class="centrar top titulo borde" style="width: 45%;" colspan="1">6. Peso o Cantidad</td>
                <td class="centrar top titulo borde" style="width: 45%;" colspan="1">7. Valor FOB <br>en US$</td>
            </tr>
           
            @foreach($detalleProduct as $value)
            
            <tr class="text-medio">
                <td class="centrar top" colspan="1">{{$value->num_orden}}</td>
                <td class="centrar top" colspan="1">{{$value->codigo_arancel}}</td>
                <td class="centrar top" colspan="1">{{$value->denominacion_mercancia}}</td>
                <td class="centrar top" colspan="1">{{$value->unidad_fisica}}</td>
                <td class="centrar top" colspan="1">{{$value->valor_fob}}</td>
            </tr>
            
            
            @endforeach

            @if(count($detalleProduct)<=1)
                @for($auxConut = 0; $auxConut<=3-count($detalleProduct); $auxConut++)
                <tr class="text-medio">
                    <td class="centrar top">&nbsp;</td>
                    <td class="centrar top">&nbsp;</td>
                    <td class="centrar top">&nbsp;</td>
                    <td class="centrar top">&nbsp;</td>
                    <td class="centrar top">&nbsp;</td>
                </tr>
                @endfor
            @endif
        
        </table>
    </div>
    <div class="centrar titulo text-medio espacio-superior">8 DECLARACION DE ORIGEN</div>
    <div class="text-medio espacio-superior">DECLARAMOS que las mercancías indicadas en el presente formulario, correspondientes a la (8.1) Factura Comercial No. <u>{{ $detdeclaracionCert[0]->numero_factura }}</u>
    de <br>(8.2) fecha <u>{{ $detdeclaracionCert[0]->f_fac_certificado }}</u> cumplen con lo establecido en las Normas de Origen del presente Acuerdo <b>ACE 59</b> de conformidad con el siguiente desglose.</div>

    <div class="centrar espacio-superior">
        <table class="ancho-full borde table">
            <tr class="text-medio">
                <td class="centrar top titulo borde" style="width: 15%;">9. No. de Orden</td>
                
                <td class="centrar top titulo borde" style="width: 85%;">10. NORMAS</td>
            </tr>
            @foreach($detdeclaracionCert as $value1)
            
                <tr class="text-center">
                    <td class="centrar top">{{$value1->num_orden_declaracion}}</td>
                    <td class="centrar top">{{$value1->normas_criterio}}</td>
                </tr>

            @endforeach

            @if(count($detdeclaracionCert)<=1)
                @for($auxConut = 0; $auxConut<=3-count($detdeclaracionCert); $auxConut++)
                <tr class="text-medio">
                    <td class="centrar top">&nbsp;</td>
                    <td class="centrar top">&nbsp;</td>
                </tr>
                @endfor
            @endif
        
        </table>
    </div>
    <div class="centrar espacio-superior">
        <table class="ancho-full table borde" style="border: 1px solid black;">
            <tr class="text-medio">
                <td class="top titulo borde" style="width: 65%;" colspan="2">11 EXPORTADOR O PRODUCTOR</td>
                
                 <td class="top titulo borde" style="width: 15%;" colspan="3">(12) Sello Y firma del Exportador o Productor</td>
            </tr>
            <tr class="text-medio">
                <td class="top borde" colspan="2" style="height:20px;">11. Razón Social: {{ $certificado->GenUsuario->DetUsuario->razon_social }}</td>
                <td colspan="2"></td>
            </tr>
            <tr class="text-medio">
                <td class="top borde" colspan="2" style="height:20px;">11.2 Dirección: {{ $certificado->GenUsuario->DetUsuario->direccion }}</td>
                <td colspan="2"></td>
            </tr>
            <tr class="text-medio">
                <td class="top borde" colspan="2" style="height:20px;">11.3 Fecha: {{ $certificado->fecha_emision_exportador}}</td>
                <td colspan="2"></td>
            </tr>

            <tr class="text-medio">
                <td class="top titulo borde" colspan="2" style="height:20px;">13. IMPORTADOR</td>
                <td colspan="2"></td>
            </tr>
            <tr class="text-medio">
                <td class="top borde" colspan="2" style="height:20px;">13.1 Razón Social: {{ strtoupper($importadorCertificado->razon_social_importador) }}</td>
                <td colspan="2"></td>
            </tr>
            <tr class="text-medio">
                <td class="top borde" colspan="2" style="height:20px;">13.1 Dirección: {{ strtoupper($importadorCertificado->direccion_importador) }}</td>
                <td colspan="2"></td>
            </tr>
            <tr class="text-medio">
                <td class="top borde" colspan="2" style="height:20px;">14. Medio de Transporte: {{ ($importadorCertificado->medio_transporte_importador) }}</td>
                <td colspan="2"></td>
            </tr>
            <tr class="text-medio">
                <td class="top borde" colspan="2" style="height:20px;">15. Puerto o Lugar de Embarque: {{ ($importadorCertificado->lugar_embarque_importador) }}</td>
                <td colspan="2"></td>
            </tr>
        </table>
        <table class="ancho-full table borde">
            <tr class="text-medio">
                <td class="top titulo borde" colspan="5" style="height:30px;">16. OBSERVACIONES</td>
            </tr>
            <tr class="text-medio">
                <td class="top borde" colspan="5" style="height:60px;">{{$certificado->observaciones}}</td>
                <td colspan="2"></td>
            </tr>
        </table>
        <!--div class="text-normal borde table justify" style="height:100px"><b>16. OBSERVACIONES</b><br><br>{{$certificado->observaciones}}<br><br></div-->
        <div>
           <table class="ancho-full table borde" style="border: 1px solid black;">
            <tr class="text-medio">
                <td class="top titulo borde" style="width: 15%; height:160px" colspan="2"><b><center>17. CERTIFICACIÓN DE ORIGEN</b></center><br><br><br><br>17.1 Certifico la veracidad de la presente declaracion, en la ciudad de:_____________ a los:____/____/____ <br><br><br><br>
                17.2 Nombre de la Entidad Certificados:_________________________________ <br><br>
                </td>
                
                 <td class="top titulo borde" style="width: 15%;" colspan="3">18. Sello Y firma de La Entidad Certificadora</td>
            </tr>
            <tr class="text-medio" style="height:60px;"></tr>
            <!--tr class="text-medio"></tr>
            <tr class="text-medio"></tr>
            <tr class="text-medio"></tr-->
        </table>
         <table class="ancho-full table borde">
            <tr class="text-medio izquierda table borde">
                <td class="justify text-11" style="height:30px;"><b class="text-11">ORIGINAL: ADUANA DE DESTINO DUPLICADO: EXPORTADOR</b></td>
                <td class="centrar" style="height:30px;"><b class="text-11">TRIPLICADO: MINISTERIO CUADRUPLICADO:MINISTERIO</b></td>
                
                <td class="centrar" colspan="3" style="height:30px;"><b class="text-11">EL FORMULARIO NO PODRA PRESENTAR RASPADURAS, TACHADURAS O ENMIENDAS.</b></td>
            </tr>
        </table>
        </div>
    </div>
</body>
</html>