<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ConsigTecno extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('consig_tecno', function (Blueprint $table) {
         $table->engine = 'InnoDB';
         $table->increments('id');
         $table->unsignedInteger('gen_tipo_solicitud')->index()->nullable();
         $table->foreign('gen_tipo_solicitud')->references('id')->on('gen_solicitud');
         $table->unsignedInteger('gen_id_consignatario')->index()->nullable();//Se agrego el pais de ult exportacion
         $table->foreign('gen_id_consignatario')->references('id')->on('gen_consignatario');
         $table->boolean('bactivo')->default(1);
         $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
          Schema::dropIfExists('consig_tecno');
    }
}
