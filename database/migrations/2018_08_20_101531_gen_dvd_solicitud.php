<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GenDvdSolicitud extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gen_dvd_solicitud', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('gen_solicitud_id')->index()->nullable();
            $table->foreign('gen_solicitud_id')->references('id')->on('gen_solicitud');
            $table->unsignedInteger('tipo_solicitud_id')->index()->nullable();
            $table->foreign('tipo_solicitud_id')->references('id')->on('tipo_solicitud');
            $table->unsignedInteger('gen_status_id')->index()->nullable();
            $table->foreign('gen_status_id')->references('id')->on('gen_status');
            $table->unsignedInteger('gen_operador_cambiario_id')->index()->nullable();
            $table->foreign('gen_operador_cambiario_id')->references('id')->on('gen_operador_cambiario');
            $table->string('cod_seguridad', 200)->nullable();   
            $table->date('fdisponibilidad')->nullable();
            $table->unsignedInteger('gen_factura_id')->index()->nullable();
            $table->foreign('gen_factura_id')->references('id')->on('gen_factura');
            $table->double('mfob')->nullable();
            $table->double('mpercibido')->nullable();
            $table->double('mvendido')->nullable();
            $table->double('mretencion')->nullable();
            $table->double('mpendiente')->nullable();
            $table->string('descripcion', 200)->nullable();
            $table->date('fstatus')->nullable();
            $table->string('num_op', 100)->nullable();
            $table->date('fventa_bcv')->nullable();
            $table->boolean('realizo_venta')->nullable();   
            $table->boolean('bactivo')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gen_dvd_solicitud');
    }
}
