-- phpMyAdmin SQL Dump
-- version 4.2.12deb2+deb8u2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 18, 2019 at 12:49 PM
-- Server version: 10.0.32-MariaDB-0+deb8u1
-- PHP Version: 7.1.26-1+0~20190113101856.12+jessie~1.gbp7077bb

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `exportaciones`
--

-- --------------------------------------------------------

--
-- Table structure for table `cat_tipo_visa`
--

CREATE TABLE IF NOT EXISTS `cat_tipo_visa` (
`id` int(10) unsigned NOT NULL,
  `nombre_visa` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bactivo` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cat_tipo_visa`
--

INSERT INTO `cat_tipo_visa` (`id`, `nombre_visa`, `bactivo`, `created_at`, `updated_at`) VALUES
(1, 'Transeunte de negocios (TR-N)', 1, '2019-01-08 04:00:00', '2019-01-08 04:00:00'),
(2, 'Transeunte  inversionista (TR-I)', 1, '2019-01-08 04:00:00', '2019-01-08 04:00:00'),
(3, 'Transeunte empresario/industria (TR-E-I)', 1, '2019-01-08 04:00:00', '2019-01-08 04:00:00'),
(4, 'Sin Visa', 1, '2019-02-11 04:00:00', '2019-02-11 04:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cat_tipo_visa`
--
ALTER TABLE `cat_tipo_visa`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cat_tipo_visa`
--
ALTER TABLE `cat_tipo_visa`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
