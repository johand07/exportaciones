INSERT INTO `exportaciones`.`gen_status` (`id`, `nombre_status`, `descripcion_status`, `bactivo`, `created_at`, `updated_at`) VALUES ('27', 'En espera de aprobación de la información', 'En espera de aprobación de la información', '1', '2022-11-28 00:00:00', '2022-11-28 00:00:00');
INSERT INTO `exportaciones`.`gen_status` (`id`, `nombre_status`, `descripcion_status`, `bactivo`, `created_at`, `updated_at`) VALUES ('28', 'Rechazada', 'Rechazada', '1', '2022-11-28 00:00:00', '2022-11-28 00:00:00');
INSERT INTO `exportaciones`.`gen_status` (`id`, `nombre_status`, `descripcion_status`, `bactivo`, `created_at`, `updated_at`) VALUES ('29', 'Aprobado por el Viceministro', 'Aprobado por el Viceministro', '1', '2022-11-28 00:00:00', '2022-11-28 00:00:00');

