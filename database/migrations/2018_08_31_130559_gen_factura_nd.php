<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GenFacturaNd extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('gen_factura_nd', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('gen_factura_id')->index()->nullable();
            $table->foreign('gen_factura_id')->references('id')->on('gen_factura');
            $table->unsignedInteger('nota_debito_id')->index()->nullable();
            $table->foreign('nota_debito_id')->references('id')->on('nota_debito');
            $table->boolean('bactivo')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::dropIfExists('gen_factura_nd');
    }
}
