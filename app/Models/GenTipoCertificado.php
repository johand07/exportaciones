<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GenTipoCertificado extends Model
{
    protected $table = 'gen_tipo_certificado';
    protected $fillable = [

           'id',
           'nombre_certificado',
           'bactivo',
    ];

    protected $date =['created_at','updated_at'];
}
