<!--div class="panel-group"-->
{{Form::model($solicitudDeclaracion,['route'=>['AnalistDeclaracionInversion.update',$solicitudDeclaracion->id],'method'=>'PATCH','id'=>'formanalistaplanilla2'])}}
@if(isset($planillaDjir02))
<div class="row">
    <div class="col-md-12">
        <div class="col-md-6">
        </div>
        <div class="col-md-6 text-right">
            <h2>Estatus: <span class="text-success">{{$planillaDjir02->genStatus->nombre_status}}</span></h2>
        </div>
    </div>
</div>
<div class="panel panel primary">
    <div class="panel-heading">
        <div class="panel-body">
            <div class="row">
               <div class="col-md-6"><h4 style="color:#337AB7"><b>Tipo de Persona:</b></h4>
                <b>{{ $solicitudDeclaracion->CatTipoUsuario->nombre}}</b>
            </div>
                <div class="col-md-2"><!-- Traerme la consulta con el numero de solicitud-->
                    <h4><b style="color:#337AB7">Nº Solicitud:</b></h4>
                    <b>{{$solicitudDeclaracion->num_declaracion}}</b>
                </div>
                <div class="col-md-2"><!-- Traerme la consulta con la fecha de solicitud-->
                    <h4><b style="color:#337AB7">Fecha Solicitud:</b></h4>
                    <b>{{$solicitudDeclaracion->created_at}}</b>
                </div>
            </div>
            <br><br>
           <div class="row">
                <div class="panel panel-primary">
                    <div class="panel-heading"><h4>Información General</h4></div>
                </div>
                <div class="col-md-12">
                    <div class="col-md-6">
                    <div class="form-group">
                        <h4>Pertenece a un Grupo Empresarial Internacional?</h4>
                    </div>
                    @if(isset($planillaDjir02->grupo_emp_internacional) && $planillaDjir02->grupo_emp_internacional == 1)
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Si </label>
                            {{$planillaDjir02->SI}}
                        </div>
                    @else
                        <div class="form-group">
                            <label>No</label>
                            {{$planillaDjir02->No}}
                        </div>
                    @endif
                    </div>
                </div>
            </div>
            <br>
            @if(isset($planillaDjir02->grupo_emp_internacional) && $planillaDjir02->grupo_emp_internacional == 1)
            <div class="row" id="datos_repre">
                <div class="col-md-12">
                    <table class="table borde table-responsive table-bordered" border="2">
                      <tr>
                        <td class="col-md-6"><h5><b>Si respondió si, por favor mencione las empresas relacionadas:</b></h5></td>
                        <td>{{$planillaDjir02->emp_relacionadas_internacional ? $planillaDjir02->emp_relacionadas_internacional : ''}}</td>
                      </tr>
                      <tr>
                        <td><b>Casa Matriz</b></td>
                        <td>{{$planillaDjir02->casa_matriz ? $planillaDjir02->casa_matriz : ''}}</td>
                      </tr>
                      <tr>
                        <td><b>Empresa Representante de la Casa Matriz para <br>América Latina:</b></td>
                        <td>{{$planillaDjir02->emp_repre_casa_matriz ? $planillaDjir02->emp_repre_casa_matriz : ''}}</td>
                      </tr>
                      <tr>
                        <td><b>Afiliadas</b></td>
                        <td>{{$planillaDjir02->afiliados_internacional ? $planillaDjir02->afiliados_internacional : ''}}</td>
                      </tr>
                      <tr>
                        <td><b>Subsidiarias de su empresa en el Extranjero</b></td>
                        <td>{{$planillaDjir02->sub_emp_extranjero ? $planillaDjir02->sub_emp_extranjero : ''}}</td>
                      </tr>
                    </table>
                </div>
            </div>
            @endif
            <br>
              <div class="row">
                <div class="col-md-12">
                    <div class="col-md-6">
                        <div class="form-group">
                        <h4>Pertenece a un Grupo Empresarial Nacional?</h4>
                    </div>
                    @if(isset($planillaDjir02->grupo_emp_nacional) && $planillaDjir02->grupo_emp_nacional ==1)
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                        <label>Si</label>
                        {{$planillaDjir02->SI}}
                        </div>
                     @else
                        <div class="form-group">
                        <label>No</label>
                        {{$planillaDjir02->NO}}
                        </div>
                    @endif
                    </div>
                </div>
            </div>
            <br>
             @if(isset($planillaDjir02->grupo_emp_nacional) && $planillaDjir02->grupo_emp_nacional == 1)
            <div class="row" id="datos_emp">
                  <div class="col-md-12">
                     <table class="table borde table-responsive table-bordered" border="2">
                      <tr>
                        <td class="col-md-6"><b>Empresa Holding, Corporación o Consorcio</b></td>
                        <td>{{$planillaDjir02->emp_haldig_coorp ? $planillaDjir02->emp_haldig_coorp : ''}}</td>
                      </tr>
                      <tr>
                        <td><b>Afiliadas:</b></td>
                        <td>{{$planillaDjir02->afiliados_nacional ? $planillaDjir02->afiliados_nacional : ''}}</td>
                      </tr>
                      <tr>
                        <td><b>Subsidiarias locales:</b></td>
                        <td>{{$planillaDjir02->sub_locales ? $planillaDjir02->sub_locales : '' }}</td>
                      </tr>
                    </table>
                  </div>
                </div>
            @endif
            <br>
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-6">
                        <div class="form-group">
                        <h4>Ha presentado ante el Despacho de Viceministro para el Comercio Exterior y Promoción de Inversiones los documentos que acreditan la representación legal o poder?</h4>
                    </div>
                    @if(isset($planillaDjir02->present_doc_repre_legal) && $planillaDjir02->present_doc_repre_legal ==1)
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                        <label>Si</label>
                        {{$planillaDjir02->SI}}
                        </div>
                     @else
                        <div class="form-group">
                        <label>No</label>
                        {{$planillaDjir02->NO}}
                        </div>
                    @endif
                    </div>
                </div>
            </div>

            @if(isset($planillaDjir02->present_doc_repre_legal) && $planillaDjir02->present_doc_repre_legal ==1)   
            <div class="row" id="representante">
                  <div class="col-md-12">
                   <table class="table borde table-responsive table-bordered" border="2">
                      <tr>
                        <td><b>EN CASO AFIRMATIVO, PROPORCIONE LOS SIGUIENTES DATOS:</b></td>
                      </tr>
                      <tr>
                        <td class="col-md-6"><b>N° de Apostilla</b></td>
                        <td>{{$planillaDjir02->num_apostilla ? $planillaDjir02->num_apostilla : ''}}</td>
                      </tr>
                      <tr>
                        <td><b>Fecha</b></td>
                        <td>{{$planillaDjir02->fecha_apostilla ? $planillaDjir02->fecha_apostilla : '' }}</td>
                      </tr>
                      <tr>
                        <td><b>País</b></td>
                        <td>{{$planillaDjir02->pais ?  $planillaDjir02->pais : ''}}</td>
                      </tr>
                       <tr>
                        <td><b>Estado o Ciudad</b></td>
                        <td>{{$planillaDjir02->estado ? $planillaDjir02->estado : ''}}</td>
                      </tr>
                       <tr>
                        <td ><b>Autoridad competente para
                        <br>apostillar</b></td>
                        <td>{{$planillaDjir02->autoridad_apostilla ? $planillaDjir02->autoridad_apostilla : ''}}</td>
                      </tr>
                      <tr>
                        <td><b>Cargo</b></td>
                        <td>{{$planillaDjir02->cargo ?  $planillaDjir02->cargo : ''}}</td>
                      </tr>
                       <tr>
                        <td><b>Traductor:</b></td>
                        <td>{{$planillaDjir02->traductor ? $planillaDjir02->traductor : '' }}</td>
                      </tr>
                      <tr>
                        <td><b>Datos Adicionales:</b></td>
                        <td>{{$planillaDjir02->datos_adicionales ? $planillaDjir02->datos_adicionales : ''}}</td>
                      </tr>
                    </table>
                  </div>
                </div>
            @endif
                <br>

            <!---Primera parte de la planilla-->
            <div class="row">
                <div class="col-md-12">
                    <br>
                    <table class="table borde table-responsive table-bordered" border="2">
                        <tr>
                            <td><b>Ingresos anuales promedio del último ejercicio:</b></td>
                            <td>{{$planillaDjir02->ingresos_anual_ult_ejer ? $planillaDjir02->ingresos_anual_ult_ejer : ''}}</td>
                        </tr>
                        <tr>
                            <td><b>Egresos anuales promedio del último ejercicio: </b></td>
                            <td>{{$planillaDjir02->egresos_anual_ult_ejer ? $planillaDjir02->egresos_anual_ult_ejer : ''}}</td>
                        </tr>
                        <tr>
                            <td><b>Total balance del último ejercicio:</b></td>
                            <td>{{$planillaDjir02->total_balance_ult_ejer ? $planillaDjir02->total_balance_ult_ejer : ''}}</td>
                        </tr>
                    </table>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="panel-primary">
                    <div class="panel-heading">
                        <h4><b>Detalle de la Declaración Jurada de la Inversión Realizada</b></h4>
                    </div><br>
                    <div class="col-md-12">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label><h5><b>Año de Declaración</b></h5></label>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <p>{{$planillaDjir02->anio_informacion_financiera ? $planillaDjir02->anio_informacion_financiera :''}}</p>
                        </div>
                    </div>  
                    </div>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="panel-primary">
                    <div class="panel-heading">
                        <h4><b>Inversión Financiera en divisas y/o cualquier otro medio de cambio</b></h4>
                    </div>
                    <div class="col-md-12"><br>
                        <label><h5><b>Tipo de Moneda</b></h5></label>
                        <div class="form-group">
                           <p>{{$planillaDjir02->infoFinanciera ? $planillaDjir02->infoFinanciera->ddivisa_abr : ''}}</p> 
                        </div>
                    </div>
                    <div class="col-md-12">
                        <br> 

                        <table class="table borde table-responsive table-bordered" border="2"><br>
                            <tr>
                                <td class="col-md-6"><b>Moneda extranjera</b></td>
                                <td class="col-md-6">{{$planillaDjir02->moneda_extranjera ? $planillaDjir02->moneda_extranjera : ''}}</td>
                            </tr>
                            <tr>
                                <td class="col-md-6"><b>Utilidades reinvertidas</b></td>
                                <td class="col-md-6">{{$planillaDjir02->utilidades_reinvertidas ? $planillaDjir02->utilidades_reinvertidas : ''}}</td>
                            </tr>
                            <tr>
                                <td><b>Crédito con casa matriz y/o filial extranjera </b></td>
                                <td>{{$planillaDjir02->credito_casa_matriz ? $planillaDjir02->credito_casa_matriz : ''}}</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <br>
  <div class="row">
    <div class="panel panel-primary">
        <div class="panel-heading"><h4><b>Bienes de capital fisico o tangibles (Maquinarias, equipos,  herramientas o cualquier otro tipo de activo tangible). </b></h4></div>
        <div class="col-md-12"><br>
            <label><h5><b>Tipo de Moneda</b></h5></label>
            <div class="form-group">
               <p>{{$planillaDjir02->bienesTangibles ? $planillaDjir02->bienesTangibles->ddivisa_abr : ''}}</p> 
            </div>
        </div>
        <div class="col-md-12">
            <br>
            <table class="table borde table-responsive table-bordered" border="2">
                <tr>
                    <td class="col-md-6"><b>Tierras y terrenos</b></td>
                    <td>{{$planillaDjir02->tierras_terrenos ? $planillaDjir02->tierras_terrenos : ''}}</td>
                </tr>
                <tr>
                    <td><b>Edificios y otras construcciones</b></td>
                    <td>{{$planillaDjir02->edificios_construcciones ? $planillaDjir02->edificios_construcciones : ''}}</td>
                </tr>
                <tr>
                    <td><b>Maquinarias, equipos y herramientas</b></td>
                    <td>{{$planillaDjir02->maquinarias_eqp_herra ? $planillaDjir02->maquinarias_eqp_herra : ''}}</td>
                </tr>
                <tr>
                    <td><b>Equipos de transporte</b></td>
                    <td>{{$planillaDjir02->eqp_transporte ? $planillaDjir02->eqp_transporte : ''}}</td>
                </tr>
                <tr>
                    <td><b>Otros activos fijos tangibles</b></td>
                    <td>{{$planillaDjir02->otros_activos_tangibles ? $planillaDjir02->otros_activos_tangibles : ''}}</td>
                </tr>
                <tr>
                    <td><b>Muebles, enseres y equipos de oficina</b></td>
                    <td>{{$planillaDjir02->muebles_enceres ? $planillaDjir02->muebles_enceres : ''}}
                    </tr>
                </table>
            </div>
        </div>
    </div>
      <div class="row">
        <div class="panel panel-primary">
            <div class="panel-heading"><h4><b>Bienes inmateriales o intangible (Software, patentes, derechos de marca u otros activos intangibles).</b></h4></div>
        <div class="col-md-12"><br>
            <label><h5><b>Tipo de Moneda</b></h5></label>
            <div class="form-group">
               <p>{{$planillaDjir02->bienesIntangibles ? $planillaDjir02->bienesIntangibles->ddivisa_abr : ''}}</p>
            </div>
        </div>
            <div class="col-md-12">
                <br>
                <table class="table borde table-responsive table-bordered" border="2">
                    <tr>
                        <td class="col-md-6"><b>Software</b></td>
                        <td>{{$planillaDjir02->software ? $planillaDjir02->software : ''}}</td>
                    </tr>
                    <tr>
                        <td><b>Derechos de propiedad intelectual</b></td>
                        <td>{{$planillaDjir02->derecho_prop_intelectual ? $planillaDjir02->derecho_prop_intelectual : ''}}</td>
                    </tr>
                    <tr>
                        <td><b>Contribuciones tecnológicas intangibles</b></td>
                        <td>{{$planillaDjir02->contribuciones_tecno ? $planillaDjir02->contribuciones_tecno : ''}}</td>  
                    </tr>
                    <tr>
                        <td><b>Otros activos fijos intangibles</b></td>
                        <td>{{$planillaDjir02->otros_activos_intangibles ? $planillaDjir02->otros_activos_intangibles : ''}}</td>
                    </tr>
                    <tr>
                        <td class="text-center"><b>TOTAL</b></td>
                        <td>{{$planillaDjir02->total_costos_declaracion ? $planillaDjir02->total_costos_declaracion : ''}}</td>
                    </tr>
                </table>
            </div>
        </div>
      </div>
   <div class="row">
    <div class="panel-primary">
        <div class="panel-heading">
            <h4><b>Tipo de Inversión</b></h4>
        </div>
        <div class="col-md-12">
            <table class="table table-responsive table-bordered">
                 @if(isset($planillaDjir02->tipo_inversion) && $planillaDjir02->tipo_inversion == 1)
                <tr><br>
                    <td class="col-md-6 justify"><b>Inversión Extranjera Directa</b> se entiende la inversión productiva efectuada a través de los aportes realizados por los inversionistas extranjeros conformados por recursos tangibles o financieros, destinados a formar parte del patrimonio de los sujetos receptores de inversión extranjera en el territorio nacional, con la finalidad de generar valor agregado al proceso productivo en el que se inserta. Estos aportes deben representar una participación igual o superior al 10% del capital societario)</td>
                </tr>
                @else
                <tr>
                    <td><b>Inversión de Cartera</b>(Se refiere a la adquisición de acciones o participaciones societarias en todo tipo de empresas que representen un nivel de participación en el patrimonio societario inferior al diez por ciento (10%)</td>
                    
                </tr>
                @endif
            </table>
        </div>
    </div>
   </div>
   <div class="row">
    <div class="panel panel-primary">
    <div class="panel-heading"><h4><b>RESUMEN - Estimación de la Inversión Realizada (DJIR -02)</b></h4></div>
        <div class="col-md-12">
            <br>
            <table class="table borde table-responsive table-bordered" border="2">
                <tr>
                    <td style="width:40%" class="text-center"><b>Modalidad de la Inversión</b></td>
                    <td><b>Monto</b></td>
                </tr>
                <tr>
                    <td class="col-md-6"><b>Inversión Financiera en divisas y/o cualquier otro medio de cambio</b></td>
                    <td>{{$planillaDjir02->invert_divisa_cambio ? $planillaDjir02->invert_divisa_cambio : ''}}</td>
                </tr>
                <tr>
                    <td><b>Bienes de capital fisico o tangibles (Maquinarias, equipos, herramientas o cualquier otro tipo de activo tangible).</b></td>
                    <td>{{$planillaDjir02->bienes_cap_fisico_tangibles ? $planillaDjir02->bienes_cap_fisico_tangibles : ''}}</td>
                </tr>
                <tr>
                    <td><b>Bienes inmateriales o intangible (Software, patentes, derechos de marca u otros activos intangibles.</b></td>
                    <td>{{$planillaDjir02->bienes_inmateriales_intangibles ? $planillaDjir02->bienes_inmateriales_intangibles : ''}}</td>
                </tr>
                <tr>
                    <td><b>Reinversiones de utilidades ( SOLO PARA PROCESO DE ACTUALIZACIÓN)</b></td>
                    <td>{{$planillaDjir02->reinversiones_utilidades ? $planillaDjir02->reinversiones_utilidades : ''}}</td>
                </tr>
                <tr>
                    <td><b>Otra (especifique)</b></td>
                    <td>{{$planillaDjir02->especifique_otro ? $planillaDjir02->especifique_otro : ''}}</td>
                </tr>
                <tr>
                    <td style="width:40%" class="text-center"><b>TOTAL</b></td>
                    <td>{{$planillaDjir02->total_modalidad_inv ? $planillaDjir02->total_modalidad_inv : ''}}</td>
                </tr>
            </table>
        </div>
    </div>
   </div>
      <br>
      <div class="row">
        <div class="col-md-12">
            <table class="table borde table-responsive table-bordered" border="2">
                <tr>
                    <td><h5><b>Cuál fue la base de estimación?</b></h5></td>
                </tr>
                <tr>
                    <td>{{$planillaDjir02->base_estimacion ? $planillaDjir02->base_estimacion : ''}}</td>
                </tr>
            @if(isset($planillaDjir02->otro_especifique))
                <tr>
                    <td><b>Otros (especificar)</b></td>
                    <td>{{$planillaDjir02->otro_especifique ? $planillaDjir02->otro_especifique : ''}}</td>
                </tr>
            @endif
            </table>
        </div>
      </div>
      <br><br>
      <div class="row">
        <div class="col-md-12">

          <div class="form-group">
          {{Form::hidden('planillaDec',2)}}
            <label>Estatus de Planilla</label><br>
            {!!Form::select('gen_status_id',$estado_observacion_planilla02,null,['placeholder'=>'Seleccione','class'=>'form-control','id'=>'gen_status_id_PDIJ02','onchange'=>'validarObsPDIJ02()'])!!}
          </div>

        </div>
      </div>
    <br>
    <div class="row" id="observacionplanilla2" style="display:none;">
        <b><u>OBSERVACIONES:</u></b>
        <textarea rows="4"  class="form-control text-justify" name="descrip_observacion" id="descrip_observacionPDIJ02" style="font-weight:bold; text-transform: capitalize;" ></textarea>
    </div>
      <div class="row text-center"><br><br>
            <a class="btn btn-primary" href="{{url('AnalistaInversion/AnalistDeclaracionInversion')}}">Cancelar</a>
             <input type="submit" name="Guardar Observacion" class="btn btn-danger center" value="Guardar Observacion" onclick="return enviarplanillaD2 ()">   
        </div>

        </div>
    </div>
</div>
@else
<div class="text-center text-warning"> <h4>No se han cargado datos para la panilla Djir02</h4> </div>
@endif<!--Panel Primary-->
<!--/div--><!--Panel Group-->

{{Form::close()}}
<script>

function validarObsPDIJ02 (){

    var estatus=$('#gen_status_id_PDIJ02').val();
    // alert(estatus);
    if(estatus == 11 || estatus== 13 || estatus==15) {
    
        $('#observacionplanilla2').show('show');
        $('#descrip_observacionPDIJ02').prop("required", true);
    }else{
        $('#observacionplanilla2').hide('slow');
        $('#descrip_observacionPDIJ02').prop('required',false);
        $('#descrip_observacionPDIJ02').removeAttr("required");
    
    }

}
</script>