<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class genDocDecInversion extends Model
{
    protected $table = 'gen_doc_dec_inversion';

    protected $fillable = [

    	   'gen_declaracion_inversion_id',
           'file_1',
           'file_2',
           'file_3',
           'file_4',
           'file_5',
           'file_6',
           'file_7',
           'file_8',
           'file_9',
           'file_10',
           'file_11',
           'bactivo',
    ];

    protected $date =['created_at','updated_at'];

  	
    public function genDecInversion()
	{
        return $this->hasOne('App\Models\GenDeclaracionInversion','id','gen_declaracion_inversion_id');
    }
}
