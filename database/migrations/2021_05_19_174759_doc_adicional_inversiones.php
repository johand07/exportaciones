<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DocAdicionalInversiones extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('doc_adicional_inversiones', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('gen_sol_inversionista_id')->index()->nullable();
            $table->foreign('gen_sol_inversionista_id')->references('id')->on('gen_sol_inversionista');
            $table->string('file_1',300)->nullable();
            $table->string('file_2',300)->nullable();
            $table->string('file_3',300)->nullable();
            $table->string('file_4',300)->nullable();
            $table->string('file_5',300)->nullable();
            $table->string('file_6',300)->nullable();
            $table->boolean('bactivo')->default(1);
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::dropIfExists('doc_adicional_inversiones');
    }
}
