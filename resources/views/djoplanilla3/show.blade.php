@extends('templates/layoutlte')
@section('content')
<div class="content">
  <div class="panel-group" style="background-color: #fff;">
      <div class="panel-primary">
          <div class="panel-heading"><h3>(P-3) - 5. Estructura de Costos por Unidad de Medida del Producto a Exportar</h3> </div>
          <div class="panel-body">

              <div class="row" style="background-color: #fff">
                  <div class="col-md-12">
 
               

                          <div class="row">
                              
                              <div class="col-md-6">
                                  <div class="form-group">
                                     <label>5.1 Denominación Comercial</label>
                                      <label class="form-control">{{$producto->descrip_comercial or ''}}</label>
                                  </div>
                              </div>
                              <div class="col-md-6">
                                  <div class="form-group">
                                    <label>5.2 Moneda</label><br>
                                    <label class="form-control">{{$divisa->ddivisa_abr or ''}}</label>                                      
                                  </div>
                              </div>
                              
                              <div class="col-md-12"><hr></div>

                              <div class="col-md-12">
                                  <div class="col-md-6">                                     
                                      <table border="2"  class=" table-responsive table-striped table-hover table">
                                        <thead>
                                          <th>COSTOS DIRECTOS</th>
                                          <th>TOTAL COSTOS</th>
                                        </thead>
                                        <tbody>
                                          <tr>
                                            <td>MATERIALES IMPORTADOS = (4.6.5 DE LA P2)</td>
                                            <td class="text-center">
                                            <label class="form-control">{{$costos_directos->mate_importados or ''}}</label>
                                            </td>
                                          </tr>
                                          <tr>
                                            <td>MATERIALES NACIONALES = (4.6.11 DE LA P2)</td>
                                            <td class="text-center">
                                            <label class="form-control">{{$costos_directos->mate_nacional or ''}}</label>
                                            </td>
                                          </tr>
                                          <tr>
                                            <td>MANO DE OBRA</td>
                                            <td class="text-center">
                                            <label class="form-control">{{$costos_directos->mano_obra or ''}}</label>
                                            </td>
                                          </tr>
                                          <tr>
                                            <td>DEPRECIACIÓN</td>
                                            <td class="text-center">
                                            <label class="form-control">{{$costos_directos->depreciacion or ''}}</label>
                                            </td>
                                          </tr>
                                          <tr>
                                            <td>OTROS COSTOS DIRECTOS</td>
                                            <td class="text-center">
                                            <label class="form-control">{{$costos_directos->otros_costos_dir or ''}}</label>
                                            </td>
                                          </tr>
                                          <tr>
                                            <td colspan="2" class="text-center"><b>(1)  TOTAL COSTOS DIRECTOS</b>
                                            <label class="form-control">{{$costos_directos->total or ''}}</label>
                                            </td>
                                          </tr>
                                        </tbody>
                                      </table>
                                  </div>
                                  <div class="col-md-6">
                                     <table border="2"  class=" table-responsive table-striped table-hover table">
                                        <thead>
                                          <th>COSTOS INDIRECTOS</th>
                                          <th>TOTAL COSTOS</th>
                                        </thead>
                                        <tbody>
                                          <tr>
                                            <td>GASTOS ADMINISTRATIVOS = (4.6.5 DE LA P2)</td>
                                            <td class="text-center"> 
                                            <label class="form-control">{{$costos_indirectos->gastos_admin or ''}}</label>
                                            </td>
                                          </tr>
                                          <tr>
                                            <td>GASTOS DE PUBLICIDAD = (4.6.11 DE LA P2)</td>
                                            <td class="text-center">
                                            <label class="form-control">{{$costos_indirectos->gastos_publicidad or ''}}</label>
                                            </td>
                                          </tr>
                                          <tr>
                                            <td>GASTOS DE VENTAS</td>
                                            <td class="text-center">
                                            <label class="form-control">{{$costos_indirectos->gastos_ventas or ''}}</label>
                                            </td>
                                          </tr>
                                         
                                          <tr>
                                            <td>OTROS COSTOS INDIRECTOS</td>
                                            <td class="text-center">
                                            <label class="form-control">{{$costos_indirectos->otros_gastos_indi or ''}}</label>
                                            </td>
                                          </tr>
                                          <tr><td></td><td><input class="form-control hidden" type="" name=""></td></tr>
                                          <tr>
                                            <td colspan="2" class="text-center"><b>(2) TOTAL GASTOS INDIRECTOS</b>
                                            <label class="form-control">{{$costos_indirectos->total or ''}}</label>
                                            </td>
                                          </tr>
                                        </tbody>
                                      </table>
                                  </div>
                                  <div class="col-md-12">
                                    
                                    
                                    <table border="2"  class=" table-responsive table-striped table-hover table">
                                        
                                        <tbody>
                                          <tr>
                                            <td><b>(3)</b> UTILIDAD</td>
                                            <td class="text-center"> 
                                            <label class="form-control">{{$planilla3->utilidad or ''}}</label>
                                            </td>
                                          </tr>
                                          <tr>
                                            <td><b>(4)</b> PRECIO A PUERTA DE FÁBRICA = 1 + 2 + 3</td>
                                            <td class="text-center">
                                            <label class="form-control">{{$planilla3->precio_puerta_fabri or ''}}</label>
                                            </td>
                                          </tr>
                                          <tr>
                                            <td><b>(5)</b> FLETE INTERNO </td>
                                            <td class="text-center">
                                            <label class="form-control">{{$planilla3->flete_interno or ''}}</label>
                                            </td>
                                          </tr>
                                         
                                          <tr>
                                            <td><b>(6)</b> PRECIO FOB DE EXPORTACIÓN = 4 + 5</td>
                                            <td class="text-center">
                                            <label class="form-control">{{$planilla3->precio_fob_export or ''}}</label>
                                            </td>
                                          </tr>
                                          
                                        </tbody>
                                      </table>  
                                  </div> 
                              </div>
                          </div>
                          
                         

              
                </div>
              </div>

        </div>
      </div>
  </div><!-- Fin panel -->
</div><!-- Fin content -->


<!--script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script-->

@endsection