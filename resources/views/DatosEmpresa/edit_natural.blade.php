@extends('templates/layoutlte')
@section('content')
<div class="container">
{{Form::model($datosempresa,['route'=>['DatosEmpresa.update',$datosempresa->id],'method'=>'PATCH','id'=>'FormDatosEmpresaEdit']) }}


<div class="panel panel-primary">
  <div class="panel-heading">DATOS BÁSICOS</div>
  <div class="panel-body">
    <div class="row">
      <div class="col-md-3">
        <div class="form-group">
          {!! Form::label('','Nombre o Razón Social')!!}
          {!! Form::text('razon_social',null, ['class'=>'form-control','id'=>'razon_social','onkeypress'=>'return soloNumerosyLetras(event)','maxlength'=>'100']) !!}
        </div>
      </div>
      <div class="col-md-3">
        <div class="form-group">
          {!! Form::label('','Siglas')!!}
          {!! Form::text('siglas',null, ['class'=>'form-control','id'=>'siglas','onkeypress'=>'return soloLetras(event)','maxlength'=>'50']) !!}
        </div>
      </div>
      <div class="col-md-3">
        <div class="form-group">
          {!! Form::label('','Rif')!!}
          {!! Form::text('rif',null, ['class'=>'form-control','id'=>'rif','readonly'=>'true','maxlength'=>'11']) !!}
        </div>
      </div>
      <div class="col-md-3">
          <div class="form-group">
            <label>Página Web</label><br>
            {!! Form::text('pagina_web',null, ['class'=>'form-control','id'=>'pagina_web','onkeypress'=>'return soloNumerosyLetras(event)','maxlength'=>'60']) !!}
          </div>
        </div> 
      </div>
      <div class="panel panel-primary">
        <div class="panel-heading">TIPO DE ACTIVIDAD ECONÓMICA</div>
      </div>
      <div class="row">
        <div class="col-md-3">

          {!! Form::checkbox('export','1',$datosempresa->export, ['class'=>'checkbox-inline','id'=>'export']) !!}
          {!! Form::label('','Exportador')!!}
        </div>
        <div class="col-md-3">
          {!! Form::checkbox('invers','1',$datosempresa->invers,['class'=>'checkbox-inline','id'=>'invers']) !!}
          {!! Form::label('','Inversionista')!!}
        </div>
        <div class="col-md-3">
          {!! Form::checkbox('produc','1',$datosempresa->produc, ['class'=>'checkbox-inline','id'=>'produc']) !!}
          {!! Form::label('','Productor')!!}
        </div>
        <div class="col-md-3">
          {!! Form::checkbox('comerc','1',$datosempresa->comerc, ['class'=>'checkbox-inline','id'=>'comerc']) !!}
          {!! Form::label('','Comercializadora')!!}
        </div>
      </div>
      <br>
    <div class="row">
      <div class="col-md-6">
        <div class="form-group">
          <label>Sector</label><br>
          {{Form::select('id_gen_sector',$sectores,$show_sector->gen_sector_id,['placeholder'=>'Seleccione','class'=>'form-control','id'=>'id_gen_sector'])}}
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group">
          {!! Form::label('','Actividad Económica')!!}
          {!!Form::select('gen_actividad_eco_id',$actividadeco,null, ['placeholder'=>'Seleccione','class'=>'form-control','id'=>'id_gen_actividad_eco']) !!}
        </div>
      </div>
    </div>
      <div class="panel panel-primary">
        <div class="panel-heading">DOMICILIO FISCAL</div>
      </div>
      <div class="row">
        <div class="col-md-4">
          <div class="form-group">
            {!! Form::label('','Estado')!!}
						{!!Form::select('estado_id',$estados,null,['placeholder'=>'Seleccione','class'=>'form-control','id'=>'id_estado']) !!}
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            {!! Form::label('','Municipio')!!}
						{!!Form::select('municipio_id',$municipios,null, ['placeholder'=>'Seleccione','class'=>'form-control','id'=>'id_municipio']) !!}
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            {!! Form::label('','Parroquia')!!}
						{!!Form::select('parroquia_id',$parroquias,null, ['placeholder'=>'Seleccione','class'=>'form-control','id'=>'id_parroquia']) !!}
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            {!! Form::label('','Dirección')!!}
						{!! Form::text('direccion',null, ['class'=>'form-control','id'=>'direccion','onkeypress'=>'return soloNumerosyLetras(event)','maxlength'=>'200']) !!}
          </div>
        </div>
        <div class="col-md-3">
          <div class="form-group">
            {!! Form::label('','Teléfono Local')!!}
            {!! Form::text('telefono_local',null, ['class'=>'form-control','id'=>'telefono_local','onkeypress'=>'return soloNumeros(event)','maxlength'=>'11']) !!}
            <small>Ejemplo:02125554555</small>
          </div>
        </div>
        <div class="col-md-3">
          <div class="form-group">
            {!! Form::label('','Teléfono Celular')!!}
            {!! Form::text('telefono_movil',null, ['class'=>'form-control','id'=>'telefono_movil','onkeypress'=>'return soloNumeros(event)','maxlength'=>'11']) !!}
            <small>Ejemplo:04165554555</small>
          </div>
        </div>
      </div>

     <!-- <div class="panel panel-primary">
        <div class="panel-heading">DATOS DE LOS PRODUCTOS QUE PRODUCE Y/O COMERCIALIZA</div>
      </div>-->
      <div class="row" style="display:none">
         <div class="col-md-1"></div>
         <div class="col-md-10 text-center">
            {!! Form::label('','¿Exporta Productos de Servicios y/o Tecnología?',['class'=>'form-control'])!!}
            @foreach($radio as $rad)

            {!! Form::label('',$rad->nombre,['class'=>'radio-inline'])!!}
            {!! Form::radio('produc_tecno',$rad->id,false,['class'=>'radio-inline','id'=>"prod_tecnologia_$rad->id"])!!}
            @endforeach

          </div>
        <div class="col-md-1"></div>
      </div><br>
      <div class="row" id="tecno_especifique" style="display: none;">
        <div class="col-md-1"></div>
        <div class="col-md-10">
          {!! Form::label('','Especifique')!!}
          {!!Form::text('especifique_tecno',null,['class'=>'form-control','id'=>'especifique_tecno','onkeypress'=>'return soloLetras(event)','maxlength'=>'100'])!!}
        </div>
        <div class="col-md-1"></div>
      </div><br>
      <div class="panel panel-primary">
        <div class="panel-heading">DATOS DE ACCESO</div>
      </div>
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            {!! Form::label('','Correo Electrónico')!!}
            {!! Form::text('correo',null, ['class'=>'form-control','id'=>'correo','onkeyup'=>"minuscula(this.value,'#email');",'maxlength'=>'80']) !!}
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            {!! Form::label('','Correo Alternativo')!!}
            {!! Form::text('correo_sec',null, ['class'=>'form-control','id'=>'correo_sec','onkeyup'=>"minuscula(this.value,'#email');",'maxlength'=>'80']) !!}
          </div>
        </div>
      </div>
    </div>


   <div class="row">
     <div class="col-md-12">
       <div class="col-md-4"></div>
       <div class="col-md-4 text-center">
         <div class="form-group">

           <a href="{{url('exportador/DatosEmpresa')}}" class="btn btn-primary">Atrás</a>
           <a class="btn btn-success" href="#"  onclick="enviarNaturalEdit()">Guardar</a>

         </div>
       </div>

       <div class="col-md-4"></div>
     </div>
   </div>



  </div>




{{Form::close()}}
</div>
<script src="{{asset('js/ajax_v2.1.1.js')}}"></script>
<script type="text/javascript">

$(document).ready(function(){

var x=document.getElementsByName('produc_tecno');
var y=document.getElementById('especifique_tecno');
if(x[0].checked){

  y.style.display="block";
}


});

$('#prod_tecnologia_1').click(function(event) {
  $('#fields2,#codigo_arancelarios').hide(1000).animate({height: "hide"}, 1000,"linear");
  $('#tecno_especifique').show(1000).animate({width: "show"}, 1000,"linear");

});

 $('#prod_tecnologia_2').click(function(event) {
  $('#tecno_especifique').hide(1000).animate({height: "hide"}, 1000,"linear");
  $('#fields2,#codigo_arancelarios').show(1000).animate({width: "show"}, 1000,"linear");
});


</script>



@endsection
