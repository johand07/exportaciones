<?php

namespace App\Http\Controllers;
use Illuminate\Filesystem\Filesystem;
use App\Models\DocAdicionalDjo;
use App\Models\DetDeclaracionProduc;
use Illuminate\Http\Request;
use Auth;
use Alert;
use File;
use Session;
use DB;
use View;
use Response;

class DocAdicionalDjoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request,$gen_declaracion_jo_id)

    {
        
       //dd($gen_declaracion_jo_id);
       return view('DocAdicional.create',compact('gen_declaracion_jo_id'));//
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        

        /*******************   Validar reglas de las imagenes   ********************/
         $this->validate($request, [

                'file_1' => 'required|mimes:pdf,doc,docx,zip,xlsx,ppt,pptx,xls,jpeg,png,jpg|max:9500',
                //'foto_2' => 'required|image|mimes:jpeg,png,jpg,gif|max:250',
                //'foto_3' => 'required|image|mimes:jpeg,png,jpg,gif|max:250',

            ]);
         //dd($request->file('foto_2'));
         /*****************   Validar que se tenga la session de la declaracion asociada al producto  ****************/

            
            if(empty($request->gen_declaracion_jo_id)){

                Alert::warning('No es posible realizar su solicitud. Intente nuevamente.','Acción Denegada!')->persistent("OK");

                return redirect()->back()->withInput();
            }


         /*************************** Validar que no hayan registros previas en DocAdicionalDjo*****/

            /*$planilla7 = DocAdicionalDjo::where('gen_declaracion_jo_id',$request->gen_declaracion_jo_id)->get()->last();

            if(!empty($planilla6)){
                 Alert::error('Usted ya registro documentos para la declaracion. Intente nuevamente.','Acción Denegada!')->persistent("OK");

                return redirect()->action('DeclaracionJOController@index');
            }*/


        /*************************** Guardar las imagenes  ******************************/
    
        $destino = '/img/declaracionjurada/'.Auth::user()->detUsuario->cod_empresa;
        $destinoPrivado = public_path($destino);
        
        if (!file_exists($destinoPrivado)) {
            $filesystem = new Filesystem();
            $filesystem->makeDirectory($destinoPrivado, $mode = 0777, true, true); 
        }
        /*************************** Validar que exista el directorio para enviar el archivo y guardar en la base de datos**********************/
        if (file_exists($destinoPrivado)) {
            $docadicional = new DocAdicionalDjo;
            $docadicional->gen_declaracion_jo_id = $request->gen_declaracion_jo_id;
        for( $i=1; $i <=6 ; $i++ ){
            $imagen = $request->file('file_'.$i);
            if (!empty($imagen)) {
                $nombre_imagen = $this->nameFile($imagen, $i);//$imagen->getClientOriginalName();
                $imagen->move($destinoPrivado, $nombre_imagen);
                $auxStr='file_'.$i;
                $$auxStr = $nombre_imagen;
             
            }
        }
            $docadicional->file_1 = $destino.'/'.$file_1;
            if (!empty($file_2)) {
                $docadicional->file_2 = $destino.'/'.$file_2;
            }
            if (!empty($file_3)) {
                $docadicional->file_3 = $destino.'/'.$file_3;
            }
            if (!empty($file_4)) {
                $docadicional->file_4 = $destino.'/'.$file_4;
            }
            if (!empty($file_5)) {
                $docadicional->file_5 = $destino.'/'.$file_5;
            }
            if (!empty($file_6)) {
                $docadicional->file_6 = $destino.'/'.$file_6;
            }
            $docadicional->bactivo=1;
            $docadicional->save();
            Alert::success('Se han guardado los documentos correspondientes!','Registrado Correctamente!')->persistent("Ok");

    }

            return redirect()->action('DeclaracionJOController@create');
           
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\DocAdicionalDjo  $docAdicionalDjo
     * @return \Illuminate\Http\Response
     */
    public function show(DocAdicionalDjo $docAdicionalDjo, Request $request)
    {
        $layout= $request->layout==="false"? false: true;
//dd($request->gen_declaracion_jo_id);
        if(!empty($request->gen_declaracion_jo_id))
        {
            
            $det_declaracion= DocAdicionalDjo::where('gen_declaracion_jo_id',$request->gen_declaracion_jo_id)->first();
            
            if(empty($det_declaracion)){
                Alert::error('Los documentos que intenta visualizar no estan registrados en el sistema.','Acción Denegada!')->persistent("OK");
                return $layout? redirect()->action('DeclaracionJOController@index') : '<div class="alert alert-danger">La planilla que intenta visualizar no esta registrada en el sistema. D1</div>';
            }

            $planilla7 = $det_declaracion;


        }else{
            // dd($request->det_declaracion_id);
            $detDeclaProd=DetDeclaracionProduc::where('id',$request->det_declaracion_id)->first();
            $planilla7 = DocAdicionalDjo::where('gen_declaracion_jo_id',$detDeclaProd->gen_declaracion_jo_id)->first();
        }

        /***** Validar que haya un registro de la planilla ****/
        if(empty($planilla7)){

             Alert::error('Los documentos que intenta visualizar no estan registrados en el sistema.','Acción Denegada')->persistent("OK");

            return $layout? redirect()->action('DeclaracionJOController@index') : '<div class="alert alert-danger">La planilla que intenta visualizar no esta registrada en el sistema.</div>';
        }

        /***** Validar que la planilla sea de un usuario en especifico ****/
        if($planilla7->GenDeclaracionJO->usuario->gen_usuario_id!=Auth::user()->id && Auth::user()->gen_tipo_usuario_id!=5 && Auth::user()->gen_tipo_usuario_id!=6 && Auth::user()->gen_tipo_usuario_id!=1 && Auth::user()->gen_tipo_usuario_id!=8){

            Alert::error('No tiene permisos para visualizar esta planilla.','Acción Denegada!')->persistent("OK");

           return  $layout? redirect()->action('DeclaracionJOController@index') : '<div class="alert alert-danger">No tiene permisos para visualizar esta planilla.</div>';
       }

       
        //dd($exten_file_3=explode(".", $planilla7->file_3));
        $exten_file_1=explode(".", $planilla7->file_1);

        $extencion_file_1 = $exten_file_1[1];
//dd($layout);
        if(!empty($planilla7->file_2)){
            $exten_file_2=explode(".", $planilla7->file_2);
            $extencion_file_2 = $exten_file_2[1];
        } else {
            $extencion_file_2 = '';
        }
        if (!empty($planilla7->file_3)) {
            $exten_file_3=explode(".", $planilla7->file_3);
            $extencion_file_3 = $exten_file_3[1];
            //dd($extencion_file_3);
        }else {
            $extencion_file_3 = '';
        }

        if (!empty($planilla7->file_4)) {
            $exten_file_4=explode(".", $planilla7->file_4);
            $extencion_file_4 = $exten_file_4[1];
        } else {
            $extencion_file_4 = '';
        }

        if (!empty($planilla7->file_5)) {
            $exten_file_5=explode(".", $planilla7->file_5);
            $extencion_file_5 = $exten_file_5[1];
        }else {
            $extencion_file_5 = '';
        }

        if (!empty($planilla7->file_6)) {
            $exten_file_6=explode(".", $planilla7->file_6);
            $extencion_file_6 = $exten_file_6[1];
        } else {
            $extencion_file_6 = '';
        }
        if($layout){
            return view('DocAdicional.show',compact('layout','planilla7','extencion_file_1','extencion_file_2','extencion_file_3','extencion_file_4','extencion_file_5','extencion_file_6'));
        }else{
            return View::make('DocAdicional.show',compact('layout','planilla7','extencion_file_1','extencion_file_2','extencion_file_3','extencion_file_4','extencion_file_5','extencion_file_6'))->renderSections()['content'];
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\DocAdicionalDjo  $docAdicionalDjo
     * @return \Illuminate\Http\Response
     */
    public function edit(DocAdicionalDjo $docAdicionalDjo, $gen_declaracion_jo_id)
    {
        //dd($gen_declaracion_jo_id);
        $detDocAdicionalDeclaracion= DocAdicionalDjo::where('gen_declaracion_jo_id',$gen_declaracion_jo_id)->first();
        //dd($detDocAdicionalDeclaracion);
        return View::make('DocAdicional.edit',compact('detDocAdicionalDeclaracion','gen_declaracion_jo_id'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\DocAdicionalDjo  $docAdicionalDjo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DocAdicionalDjo $docAdicionalDjo, $id)
    {
         /*******************   Validar reglas de las imagenes   ********************/
       $this->validate($request, [

        //'file_1' => 'required|mimes:pdf,doc,docx,zip|max:1000',
        //'foto_2' => 'required|image|mimes:jpeg,png,jpg,gif|max:250',
        //'foto_3' => 'required|image|mimes:jpeg,png,jpg,gif|max:250',

    ]);




/*************************** Guardar las imagenes  ******************************/
        $destino = '/img/declaracionjurada/'.Auth::user()->detUsuario->cod_empresa;
        $destinoPrivado = public_path($destino);
        
        if (!file_exists($destinoPrivado)) {
            $filesystem = new Filesystem();
$filesystem->makeDirectory($destinoPrivado, $mode = 0777, true, true); 
        }


        for( $i=1; $i <=6 ; $i++ ){

            $imagen = $request->file('file_'.$i);
            if (!empty($imagen)) {
                //dd($imagen);
               $nombre_imagen = $this->nameFile($imagen, $i);//$imagen->getClientOriginalName();
               // dd($destinoPrivado);
                $imagen->move($destinoPrivado, $nombre_imagen);
                $auxStr='file_'.$i;
                $$auxStr = $nombre_imagen;
               // dd($$auxStr);
            }
            

        }

    $planilla7 = DocAdicionalDjo::find($id);

    
    if(!empty($file_1)){
        $auxFile1= $planilla7->file_1;
        $planilla7->file_1 = $destino.'/'.$file_1;
    }
    if(!empty($file_2)){
        $auxFile2= $planilla7->file_2;
        $planilla7->file_2 = $destino.'/'.$file_2;
    }
    if(!empty($file_3)){
        $auxFile3= $planilla7->file_3;
        $planilla7->file_3 = $destino.'/'.$file_3;
    }
    if(!empty($file_4)){
        $auxFile4= $planilla7->file_4;
        $planilla7->file_4 = $destino.'/'.$file_4;
    }
    if(!empty($file_5)){
        $auxFile5= $planilla7->file_5;
        $planilla7->file_5 = $destino.'/'.$file_5;
    }
    if(!empty($file_6)){
        $auxFile6= $planilla7->file_6;
        $planilla7->file_6 = $destino.'/'.$file_6;
    }

    // $planilla7->bactivo=1;
    $planilla7->estado_observacion=0;


    if($planilla7->touch()){
        if(!empty($auxFile1)){
            if(is_file('.'.$auxFile1))
            unlink('.'.$auxFile1); //elimino el fichero
        }
        if(!empty($auxFile2)){
            if(is_file('.'.$auxFile2))
            unlink('.'.$auxFile2); //elimino el fichero
        }
        if(!empty($auxFile3)){
            if(is_file('.'.$auxFile3))
            unlink('.'.$auxFile3); //elimino el fichero
        }
        if(!empty($auxFile4)){
            if(is_file('.'.$auxFile4))
            unlink('.'.$auxFile4); //elimino el fichero
        }
        if(!empty($auxFile5)){
            if(is_file('.'.$auxFile5))
            unlink('.'.$auxFile5); //elimino el fichero
        }
        if(!empty($auxFile6)){
            if(is_file('.'.$auxFile6))
            unlink('.'.$auxFile6); //elimino el fichero
        }

    }else{
        if (!empty($file_1)) {
            if(is_file('.'.$destino.'/'.$file_1))
            unlink('.'.$destino.'/'.$file_1);
        }
        
        if (!empty($file_2)) {
            if(is_file('.'.$destino.'/'.$file_2))
            unlink('.'.$destino.'/'.$file_2);
        }
        
        if (!empty($file_3)) {
            if(is_file('.'.$destino.'/'.$file_3))
            unlink('.'.$destino.'/'.$file_3);
        }

        if (!empty($file_4)) {
            if(is_file('.'.$destino.'/'.$file_4))
            unlink('.'.$destino.'/'.$file_4);
        }

        if (!empty($file_5)) {
            if(is_file('.'.$destino.'/'.$file_5))
            unlink('.'.$destino.'/'.$file_5);
        }

        if (!empty($file_6)) {
            if(is_file('.'.$destino.'/'.$file_6))
            unlink('.'.$destino.'/'.$file_6);
        }
        

        Alert::warning('No es posible realizar su solicitud. Intente nuevamente.','Acción Denegada!')->persistent("OK");
        return redirect()->back()->withInput();
    }
    
// redirect
Alert::success('Se han actualizado los datos correspondientes a los Documentos Adicionales!','Actualizado Correctamente!')->persistent("Ok");

return redirect()->action('DeclaracionJOController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\DocAdicionalDjo  $docAdicionalDjo
     * @return \Illuminate\Http\Response
     */
    public function destroy(DocAdicionalDjo $docAdicionalDjo)
    {
        //
    }

          /** Para consultar las observaciones mediante ajax **/
    /**
     * Se recibe como parametro el $id de la planilla
     * 
     */
    public function obtenerObservacion(Request $request,$id=null){

        if(!empty($request->gen_declaracion_jo_id)){
            $detDeclaracion= DocAdicionalDjo::where('gen_declaracion_jo_id',$request->gen_declaracion_jo_id)->first();
            $planilla7= $detDeclaracion;
        }else{
            
        $detDeclaProd=DetDeclaracionProduc::where('id',$request->det_declaracion_id)->first();
        $planilla7 = DocAdicionalDjo::where('gen_declaracion_jo_id',$detDeclaProd->gen_declaracion_jo_id)->first();
        }

        //dd($planilla7->descrip_observacion);
        return Response::json(array('observacion'=>$planilla7->descrip_observacion));
        
    }

      /** Para guardar las observaciones mediante ajax **/
    /**
     * Se recibe como parametro el $id de la planilla
     * 
     */
    public function storeObservacion(Request $request,$id=null){

        
        if(!empty($request->gen_declaracion_jo_id)){
            $detDeclaracion= DocAdicionalDjo::where('gen_declaracion_jo_id',$request->gen_declaracion_jo_id)->first();
            $planilla7= $detDeclaracion;
        }else{
            $detDeclaProd=DetDeclaracionProduc::where('id',$request->det_declaracion_id)->first();
            $planilla7 = DocAdicionalDjo::where('gen_declaracion_jo_id',$detDeclaProd->gen_declaracion_jo_id)->first();
        }
        if(!empty($request->observacion)){
            $planilla7->descrip_observacion=$request->observacion;
            $planilla7->estado_observacion=1;
        }else{
            $planilla7->descrip_observacion=null;
            $planilla7->estado_observacion=0;
        }

        if($planilla7->touch())
            return Response::json(array('respuesta'=>'Solicitud procesada con exito','estado'=>1));
        else
            return Response::json(array('respuesta'=>'La solicitud no pudo ser procesada','estado'=>0));
        
    }

    public function nameFile($imagen, $numero){

        $texto = preg_replace('([^A-Za-z0-9])', '', $imagen->getClientOriginalName());
        //dd($texto);
        $extension = pathinfo($imagen->getClientOriginalName(), PATHINFO_EXTENSION);
        return $texto.'_'.$numero.'.'.$extension;
    }
}
