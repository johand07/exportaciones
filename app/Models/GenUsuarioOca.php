<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GenUsuarioOca extends Model
{
    protected $table = 'gen_usuario_oca';

    	protected $fillable = [

           'gen_usuario_id',
           'gen_operador_cambiario_id',
           'bactivo',
    ];

    protected $date =['created_at','updated_at'];

    public function usuarios() {

     return $this->hasOne('App\Models\GenUsuario','id','gen_usuario_id');

    }

    public function oca() {

     return $this->hasOne('App\Models\GenOperadorCamb','id','gen_operador_cambiario_id');

    }
}
