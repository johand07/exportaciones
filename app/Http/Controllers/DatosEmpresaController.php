<?php

namespace App\Http\Controllers;
use App\Http\Requests\DatosEmpresaRequest;
use App\Models\GenTipoEmpresa;
use App\Models\GenActividadEco;
use App\Models\DetUsuario;
use App\Models\GenSector;
use App\Models\Pais;
use App\Models\Estado;
use App\Models\Municipio;
use App\Models\Parroquia;
use App\Models\CircunsJudicial;
use \App\Models\GenArancelNandina;
use \App\Models\GenArancelMercosur;
use \App\Models\Accionistas;
use \App\Models\Productos;
use \App\Models\GenUnidadMedida;
use App\Models\CatExportProd;
use Illuminate\Http\Request;
use Auth;
use Session;
use Laracasth\Flash\Flash;
use Alert;

class DatosEmpresaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()


    {
        $TipoPersona=DetUsuario::where('gen_usuario_id',Auth::user()->id)->first();
        
      

      
        $valor=explode('-',$TipoPersona->rif);
        if( ($valor[0]=='V') || ($valor[0]=='E') || ($valor[0]=='P') ) {

          return view('DatosEmpresa.botones_natural');

        }else{

          $titulo="Lista";
          $descripcion="Actualización de Datos de la Empresa";
          return view('DatosJuridicos.botones_juridicos');

        }


    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\DetUsuario  $detUsuario
     * @return \Illuminate\Http\Response
     */
    public function show(DetUsuario $detUsuario)
    {
        //
    }




    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\DetUsuario  $detUsuario
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, DetUsuario $detUsuario,$id)
    {
        if($request->ajax()){

                //dd($request['arancel']);
            if($request['arancel']==1){

               // $arancel=GenArancelNandina::take(50)->get();
                return "si";

            }elseif($request['arancel']==2){

                 $arancel=GenArancelMercosur::take(50)->get();
                 return $arancel;

            }elseif($request['valor']!="") {

                if( ($request['tipoAran']==1) && ($request['valor']!="")){

                    $data=GenArancelNandina::where('codigo','like','%'.$request['valor'].'%')->orWhere('descripcion','like','%'.$request['valor'].'%')->take(50)->get();

                return $data;

                }elseif(($request['tipoAran']==2) && ($request['valor']!="")){

                    $data=GenArancelMercosur::where('codigo','like','%'.$request['valor'].'%')->orWhere('descripcion','like','%'.$request['valor'].'%')->take(50)->get();

                    return $data;

               }

            }else{
                if($request['tipoAran']==1){

                $arancel=GenArancelNandina::take(50)->get();
                 return $arancel;

                }else{

                $arancel=GenArancelMercosur::take(50)->get();
                return $arancel;

               }
            }

        }else{

           $arancel=$arancel=GenArancelMercosur::all();
    /***********************Consulto la tabla Mercosur********************************/
        }

        /*****Consulta tabla gen_actividad_eco para mostrar todas las activdades economicas*************/
        $actividadeco=GenActividadEco::all()->pluck('dactividad','id');///firt es para obtener el primero que consiga,get trae toda las filas que coindan con la condicion
        //dd($actividadeco);
        $datosempresa=DetUsuario::where('gen_usuario_id',$id)->first();
        //dd($datosempresa);
        $municipios=Municipio::all()->pluck('municipio','id');
        $parroquias=Parroquia::all()->pluck('parroquia','id');
        ///Consulta para traerme las actividades economicas,datos de la empresa,municipios,parroquias.
        ########Defino las variables que contienen el titulo y descripcion del modulo Datos empresa###############################
        $titulo="Registro del Exportador";
        $descripcion="Actualización de Datos ";

        ##########################################################
         //consulto tabla de pais para mostrar Venezuela
            $paises=Pais::where('cpais', '=', 8503)->orderBy('dpais','asc')->pluck('dpais','id');
        //consulto tabla estados
            $estados=Estado::where('id',"<>",null)->orderBy('estado','asc')->pluck('estado','id');

        /******consulto la tabla gen_sector para traerme todos los sectores*******************/

             $sectores=GenSector::all()->pluck('descripcion_sector','id');
             $show_sector=GenActividadEco::find($datosempresa->gen_actividad_eco_id);
            // dd($show_sector);
             //Muestra todos los sectores

            //$sectores=GenSector::where('id','=',2)->pluck('descripcion_sector','id');

           //$sectores=GenActividadEco::all()->pluck('gen_sector_id','id');


        //consulto tabla gen_tipoempresa me traigo todos los tipos de empresa
            $tipos_empresas=GenTipoEmpresa::all()->pluck('d_tipo_empresa','id');

        /**********consulto tabla circunscripcion judicial********************************************/
            $circuns_judiciales=CircunsJudicial::all()->pluck('registro','id');

           //$accionistas=DetUsuario::all()->pluck('accionista','id');

        /****** Consulta la tabla accionista para traerme los datos de los accionistas********/
            //$accionistas=Accionistas::all();
            ///$accionistas=Accionistas::where('id')->pluck('');
            //dd($accionistas);


         /***********Consulta la tabla gen_unidad_medida para traerme todas las unidad de medida**********************/
         $unidad_medida=GenUnidadMedida::all()->pluck('dunidad','id');

    /************************Consulto la tabla accionista para traerme los que tengo registrado*****************/

         $accionistas=Accionistas::where('gen_usuario_id',Auth::user()->id)->where('bactivo',1)->get();

 /***********************************Consulto la tabla productos para traerme los que tengo registrado********/
         $productos=Productos::where('gen_usuario_id',Auth::user()->id)->where('bactivo',1)->get();

         $id=$id;
        //dd($accionistas);

         $edit_datos_empresa=DetUsuario::where('gen_usuario_id',$id)->first();

         
        
/*************************Returno de la vista con el metodo edit************************************/
        $datos=DetUsuario::where('gen_usuario_id',$id)->first();
        $extraer=explode('-',$datos->rif);
        $radio=CatExportProd::all();


        if( ($extraer[0]=='V') || ($extraer[0]=='E') || ($extraer[0]=='P') ) {


         return view('DatosEmpresa.edit_natural',compact('titulo','descripcion','sectores','estados','paises','actividadeco','municipios','parroquias','datosempresa','unidad_medida','show_sector','radio','edit_datos_empresa'));

        }else{

        return view('DatosEmpresa.edit',compact('titulo','descripcion','sectores','tipos_empresas','estados','paises','circuns_judiciales','datosempresa','actividadeco','municipios','parroquias','arancel','unidad_medida','accionistas','productos','id'));
      }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\DetUsuario  $detUsuario
     * @return \Illuminate\Http\Response
     */
public function update(Request $request, DetUsuario $detUsuario,$id){

  
        /*************Pregunto si el valor del accionista es igual si para ser el insert********/

        if ($request['accionista']=='Si') {
           $valor_accionista="Si";

         /*return view('DatosEmpresa.edit_accionista',compact('titulo','descripcion','datosempresa','accionistas','id'));*/

        }else{
           $valor_accionista="No";
        }
        //dd($request->all());




    /***************************evaluamos si  exporta productos tecnologia o no***************************/


                if ($request->produc_tecno==1) {
                        $especif_tecno=$request->especifique_tecno;
                }else{
                    $especif_tecno="No exporta tecnología";
                }

/******************************Evaluamos si los checkbox vienen lleno o no si es Si lo colocamos en 1 y no es 0 ***********/
                if (!empty($request->invers)){

                     $invers=1;
                }else{
                    $invers=0;

                }

                if (!empty($request->export)){

                    $export=1;
                }else{

                    $export=0;
                }

                if (!empty($request->produc)){

                    $produc=1;

                }else{
                    $produc=0;
                }

                if (!empty($request->comerc)) {

                    $comerc=1;
                }else{
                    $comerc=0;
                }


    /**************************************************************************************/

        $datosempresa=DetUsuario::find($id);
        $datosempresa->razon_social=$request->razon_social;
        $datosempresa->siglas=$request->siglas;
        $datosempresa->rif=$request->rif;
        $datosempresa->gen_actividad_eco_id=$request->gen_actividad_eco_id;
        $datosempresa->gen_tipo_empresa_id=$request->gen_tipo_empresa_id;
        //$datosempresa->pais_id=$request->pais_id;
        $datosempresa->pagina_web=$request->pagina_web;
        $datosempresa->estado_id=$request->estado_id;
        $datosempresa->municipio_id=$request->municipio_id;
        $datosempresa->parroquia_id=$request->parroquia_id;
        $datosempresa->direccion=$request->direccion;
        $datosempresa->telefono_local=$request->telefono_local;
        $datosempresa->telefono_movil=$request->telefono_movil;
        $datosempresa->circuns_judicial_id=$request->circuns_judicial_id;
        $datosempresa->num_registro=$request->num_registro;
        $datosempresa->tomo=$request->tomo;
        $datosempresa->folio=$request->folio;
        $datosempresa->Oficina=$request->Oficina;
        $datosempresa->f_registro_mer=$request->f_registro_mer;


        /*$datosempresa->nombre_presid=$request->nombre_presid;
        $datosempresa->apellido_presid=$request->apellido_presid;
        $datosempresa->ci_presid=$request->ci_presid;
        $datosempresa->cargo_presid=$request->cargo_presid;*/


        $datosempresa->nombre_repre=$request->nombre_repre;
        $datosempresa->apellido_repre=$request->apellido_repre;
        $datosempresa->ci_repre=$request->ci_repre;
        $datosempresa->cargo_repre=$request->cargo_repre;
        $datosempresa->correo_repre=$request->correo_repre;
        $datosempresa->correo=$request->correo;
        $datosempresa->correo_sec=$request->correo_sec;

        $datosempresa->accionista=$request->valor_accionista;

        $datosempresa->invers=$invers;
        $datosempresa->export=$export;
        $datosempresa->produc=$produc;
        $datosempresa->comerc=$comerc;

        $datosempresa->produc_tecno=$request->produc_tecno;
        $datosempresa->especifique_tecno=$especif_tecno;

        $datosempresa->save();



        /*************************insertamos  los accionistas************************************/
        if ($request['accionista']=='Si') {

            $cont_accionista=count($request->nombre_accionista);
            ////Contador para los accionista///
            for($i=0;$i<=($cont_accionista-1);$i++){
            Accionistas::firstOrCreate(
            ['id'=>$request->id[$i]],
            ['gen_usuario_id'=>Auth::user()->id,
             'nombre_accionista'=>$request->nombre_accionista[$i],
             'apellido_accionista'=>$request->apellido_accionista[$i],
             'cedula_accionista'=>$request->cedula_accionista[$i],
             'cargo_accionista'=>$request->cargo_accionista[$i],
             'cargo_accionista'=>$request->cargo_accionista[$i],
             'telefono_accionista'=>$request->telefono_accionista[$i],
             'nacionalidad_accionista'=>$request->nacionalidad_accionista[$i],
             'participacion_accionista'=>$request->participacion_accionista[$i],
             'correo_accionista'=>$request->correo_accionista[$i],
             'bactivo'=>1]

               );

            }
        }

  /****************************Insertamos los productos de la empresa*****************************/
         //if ()

           $cont_productos=count($request->descripcion);

            if (!empty($request->descripcion[0])) {
                    //dd($request->descripcion[0]);*/
                for($i=0;$i<=($cont_productos-1);$i++){
                Productos::firstOrCreate(
                ['id'=>$request->id[$i]],///Cuando se agrega un campo nuevo no trae el input///
                ['gen_usuario_id'=>Auth::user()->id,
                 'codigo'=>$request->codigo[$i],
                 'descripcion'=>$request->descripcion[$i],
                 'descrip_comercial'=>$request->descrip_comercial[$i],
                 'gen_unidad_medida_id'=>$request->gen_unidad_medida_id[$i],
                 'cap_opera_anual'=>$request->cap_opera_anual[$i],
                 'cap_insta_anual'=>$request->cap_insta_anual[$i],
                 'cap_alamcenamiento'=>$request->cap_alamcenamiento[$i],
                 'cap_exportacion'=>$request->cap_exportacion[$i],
                 'bactivo'=>1]

                   );

                }

            }

        Alert::success('','ACTUALIZACIÓN EXITOSA.')->persistent("Ok");
        return redirect('/exportador/DatosEmpresa');
}

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\DetUsuario  $detUsuario
     * @return \Illuminate\Http\Response
     */
    public function destroy(DetUsuario $detUsuario)
    {
        //
    }

    public function eliminarAccionista(Request $request,$id)
    {

       $delete=Accionistas::find($_GET['id'])->delete();
       if($delete)
       {
          return "Accionista Eliminado";
       }else
       {
         return "Accionista No existe";
       }



    }

    protected function ActividadEcoEmp (Request $request, $id){
        if ($request->ajax()) {
            $actividadeco=GenActividadEco::actividadeco($id);
            return response()->json($actividadeco);
        }
    }

    protected function getMunicipios (Request $request, $id){
        if ($request->ajax()) {
            $municipios=Municipio::municipios($id);
            return response()->json($municipios);
        }
    }

    protected function getParroquias (Request $request, $id){
        if ($request->ajax()) {
            $parroquias=Parroquia::parroquias($id);
            return response()->json($parroquias);
        }
    }



}
