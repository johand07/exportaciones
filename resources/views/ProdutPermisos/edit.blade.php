@extends('templates/layoutlte')
@section('content')
{{Form::model($permiso, ['route' =>['ProdutPermisos.update',$id],'method'=>'PATCH','id'=>'add_product','name'=>'add_product'])}}

@php $contador=count($permiso); @endphp
{{Form::hidden('metodo','Editar')}}
{{Form::hidden('contador',$contador)}}
    <!--Tabla de los Productos-->



   <table class="table table-bordered" id="fields">    

    <tr><td><button  name="add" type="button" id="add_productos" value="add more" class="btn btn-success" >Agregar</button></td></tr>
      <tr>
        <th>Código Arancelario </th>
        <th>Descripcion Arancelaria</th>
        <th>Descripcion Comercial</th>
        <th>Cantidad </th>
        <th>Unidad de Medida</th>
      </tr>

        @foreach($permiso as $key =>$valor)
      <tr id="row{{$key+1}}">
          <td>{!!Form::text('codigo_arancel[]',$valor->codigo_arancel,['class'=>'form-control','data-toggle'=>"modal",'data-target'=>"#modal",'id'=>'cod_arancel','readonly'=>'true'])!!}
          </td>

          <td>{!!Form::text('descrip_arancel[]',$valor->descrip_arancel,['class'=>'form-control','id'=>'arancel_descrip','onkeypress'=>'return soloLetras(event)','readonly'=>'true'])!!}</td>

          <td>{!!Form::text('descrip_comercial[]',$valor->descrip_comercial,['class'=>'form-control','onkeypress'=>'return soloLetras(event)'])!!}</td>

          <td>{!!Form::text('cantidad[]',$valor->cantidad,['class'=>'form-control','onkeypress'=>'return soloNumeros(event)'])!!}</td>

          <td>{!!Form::select('gen_unidad_medida_id[]',$medidas,$valor->gen_unidad_medida_id,['class'=>'form-control','placeholder'=>'Seleccione'])!!}</td>    

          <td>
           {!!Form::hidden('id[]',$valor->id)!!}
          <button  name="remove" type="button" id="{{$key+1}}" value="" class="btn btn-danger btn-remove">x</button></td>
      </tr>
        @endforeach

    </table>

   <!--Espacio para lo Botones-->
    <br><div class="row">
      <div class="col-md-12">
        <div class="col-md-4"></div>
        <div class="col-md-4 text-center">
        <a href="../../../exportador/Permisos/{{$id}}/edit" class="btn btn-primary">Regresar</a>
        <input type="submit" class="btn btn-success" value="Enviar" name="">
        </div>
        <div class="col-md-4"></div>
      </div>
    </div>

    @component('modal_dialogo',['arancel'=>$arancel])

      @slot('header') <span>Código Arancelario</span> @endslot

      @slot('body')  <div id="arancel"></div>

      @endslot

      @slot('footer')@endslot

    @endcomponent








    
{{Form::close()}}
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script type="text/javascript">

$(document).ready(function (){

var x = $('input[name="contador"]').val();
var i=parseInt(x); 

      $('#add_productos').click(function(){
       i++;


    $('#fields tr:last').after('<tr id="row'+i+'" display="block" class="show_div"><td>{!!Form::text('codigo_arancel[]',null,['class'=>'form-control','data-toggle'=>"modal",'data-target'=>"#modal",'id'=>"cod_arancel"])!!}</td><td>{!!Form::text('descrip_arancel[]',null,['class'=>'form-control','id'=>'arancel_descrip'])!!}</td><td>{!!Form::text('descrip_comercial[]',null,['class'=>'form-control'])!!}</td><td>{!!Form::text('cantidad[]',null,['class'=>'form-control'])!!}</td><td>{!!Form::select('gen_unidad_medida_id[]',$medidas,null,['class'=>'form-control','placeholder'=>'Seleccione'])!!}</td><td>{!!Form::hidden('id[]',0)!!}<button  name="remove" type="button" id="'+i+'" value="" class="btn btn-danger btn-remove">x</button></td></tr>');



       });



     $(document).on('click','.btn-remove',function(){
        var id_boton= $(this).attr("id");
        var id=$(this).prev().val();
    
    if(id!=0){

         if (confirm('Deseas eliminar este Producto?')) {

      $.ajax({

       'type':'get',
       'url':'eliminar',
       'data':{ 'id':id},
       success: function(data){

        $('#row'+id_boton).remove();

         }

       });

       }



     }else{

      $('#row'+id_boton).remove();
     }

     });




});



</script>
@stop
