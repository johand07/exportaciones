<?php

namespace App\Http\Controllers;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\PlanillaDjir_01;
use App\Models\PlanillaDjir_02;
use App\Models\PlanillaDjir_03;
use App\Models\Estado;
use App\Models\Municipio;
use App\Models\genDocDecInversion;
use App\Models\GenAccionistasDeclaracionDjir_01;
use Auth;
use Alert;

class ActualizarPlanilla1DecInversionController extends Controller
{
    public function Actualizar(PlanillaDjir_01 $planillaDjir_01, $id, $new_gen_declaracion_inversion_id)
    {
        $planillaDjir01 = PlanillaDjir_01::where('gen_declaracion_inversion_id',$id)->first();
       
        //dd($planillaDjir01);

        $documentos=genDocDecInversion::where('gen_declaracion_inversion_id',$id)->first();// Uno el que pido el primero que consiga
        // dd($documentos);
        $estados=Estado::where('id',"<>",null)->orderBy('estado','asc')->pluck('estado','id');

        $municipios=Municipio::all()->pluck('municipio','id');

        $accionistas=GenAccionistasDeclaracionDjir_01::where('gen_declaracion_inversion_id',$id)->where('bactivo',1)->get();

        return view('BandejaInversion.actualizar.editPlanilla1Inversion', compact('new_gen_declaracion_inversion_id', 'planillaDjir01','estados','municipios','accionistas','documentos'));
    }

    public function update(Request $request)
    {//dd($request->all());
        switch($request->gen_tipo_empresa){
            case 1:
                $publica = 1;
                $privada = 0;
                $cap_mix = 0;
                $otro    = 0;
                break;
            case 2:
                $publica = 0;
                $privada = 1;
                $cap_mix = 0;
                $otro    = 0;
                break;
            case 4:
                $publica = 0;
                $privada = 0;
                $cap_mix = 1;
                $otro    = 0;
                break;
            case 5:
                $publica = 0;
                $privada = 0;
                $cap_mix = 0;
                $otro    = 1;
        }

        


        $planilla1 = new PlanillaDjir_01;
        $planilla1->gen_declaracion_inversion_id            = $request->gen_declaracion_inversion_id;
        $planilla1->gen_status_id                           = 9;
        $planilla1->razon_social_solicitante                = $request->razon_social_solicitante;
        $planilla1->rif_solicitante                         = $request->rif_solicitante;
        $planilla1->finicio_op                              = $request->finicio_op;
        $planilla1->fingreso_inversion                      = $request->fingreso_inversion;
        $planilla1->emp_privada                             = $privada;
        $planilla1->emp_publica                             = $publica;
        $planilla1->emp_mixto                               = $cap_mix;
        $planilla1->emp_otro                                = $otro;
        $planilla1->especifique_otro                        = (isset($request->especifique_otro))?$request->especifique_otro:'';
        $planilla1->registro_publico                        = $request->registro_publico;
        $planilla1->estado_emp                              = (isset($request->estado_emp))?$request->estado_emp:'';
        $planilla1->municipio_emp                           = (isset($request->municipio_emp))?$request->municipio_emp:'';
        $planilla1->numero_r_mercantil                      = $request->numero_r_mercantil;
        $planilla1->folio                                   = $request->folio;
        $planilla1->tomo                                    = $request->tomo;
        $planilla1->finspeccion                             = (isset($request->finspeccion))?$request->finspeccion:'';
        $planilla1->direccion_emp                           = $request->direccion_emp;
        $planilla1->tlf_emp                                 = $request->tlf_emp;
        $planilla1->pagina_web_emp                          = $request->pagina_web_emp;
        $planilla1->correo_emp                              = $request->correo_emp;
        $planilla1->rrss                                    = $request->rrss;
        $planilla1->emple_actual_directo                    = $request->emple_actual_directo;
        $planilla1->emple_actual_indirecto                  = $request->emple_actual_indirecto;
        $planilla1->emple_generar_directo                   = $request->emple_generar_directo;
        $planilla1->emple_generar_indirecto                 = $request->emple_generar_indirecto;
        $planilla1->capital_suscrito                        = $request->capital_suscrito;
        $planilla1->capital_apagado                         = $request->capital_apagado;
        $planilla1->num_acciones_emitidas                   = $request->num_acciones_emitidas;
        $planilla1->valor_nominal_accion                    = $request->valor_nominal_accion;
        $planilla1->fmodificacion_accionaria                = $request->fmodificacion_accionaria;
        $planilla1->finscripcion                            = $request->finscripcion;
        $planilla1->nombre_repre                            = $request->nombre_repre;
        $planilla1->apellido_repre                          = $request->apellido_repre;
        $planilla1->numero_doc_identidad                    = $request->numero_doc_identidad;
        $planilla1->numero_pasaporte_repre                  = $request->numero_pasaporte_repre;
        $planilla1->cod_postal_repre                        = $request->cod_postal_repre;
        $planilla1->rif_repre                               = $request->rif_repre;
        $planilla1->telefono_1                              = $request->telefono_1;
        $planilla1->telefono_2                              = $request->telefono_2;
        $planilla1->correo_repre                            = $request->correo_repre;
        $planilla1->direccion_repre                         = $request->direccion_repre;
        $planilla1->estado_observacion                      = (isset($request->estado_observacion))?$request->estado_observacion:0;
        $planilla1->descripcion_observacion                 = (isset($request->descripcion_observacion))?$request->estado_observacion:null;
        $planilla1->save();
        //Accionistas
        $contar = count($request->razon_social_accionista);
        for($i=0; $i<=$contar-1; $i++){
            $accionistas = new GenAccionistasDeclaracionDjir_01;
            $accionistas->gen_declaracion_inversion_id  = $request->gen_declaracion_inversion_id;
            $accionistas->razon_social_accionista   = $request->razon_social_accionista[$i];
            $accionistas->identificacion_accionista = $request->identificacion_accionista[$i];
            $accionistas->nacionalidad_accionista   = $request->nacionalidad_accionista[$i];
            $accionistas->capital_social_suscrito   = $request->capital_social_suscrito[$i];
            $accionistas->capital_social_pagado = $request->capital_social_pagado[$i];
            $accionistas->porcentaje_participacion  = $request->porcentaje_participacion[$i];
            $accionistas->save();
        }
        
        //files
        $destino = '/img/declaracioninversion/'.Auth::user()->detUsuario->cod_empresa;
        $destinoPrivado = public_path($destino);
        
        if (!file_exists($destinoPrivado)) {
            $filesystem = new Filesystem();
            $filesystem->makeDirectory($destinoPrivado, $mode = 0777, true, true); 
        }


        for( $i=1; $i <=10 ; $i++ ){

            $imagen = $request->file('file_'.$i);
            //dd($imagen);
            if (!empty($imagen)) {
                //dd($imagen);
               $nombre_imagen = $imagen->getClientOriginalName();
               // dd($destinoPrivado);
                $imagen->move($destinoPrivado, $nombre_imagen);
                $auxStr='file_'.$i;
                $$auxStr = $nombre_imagen;
               //dd($$auxStr);
            }
            

        }
            
            $docadicional = new genDocDecInversion;
            $docadicional->gen_declaracion_inversion_id = $request->gen_declaracion_inversion_id;

            
            if (!empty($file_1)) {
                $docadicional->file_1 = $destino.'/'.$file_1;
            }

            if (!empty($file_2)) {
                $docadicional->file_2 = $destino.'/'.$file_2;
            }
            if (!empty($file_3)) {
                $docadicional->file_3 = $destino.'/'.$file_3;
            }
            if (!empty($file_4)) {
                $docadicional->file_4 = $destino.'/'.$file_4;
            }
            if (!empty($file_5)) {
                $docadicional->file_5 = $destino.'/'.$file_5;
            }
            if (!empty($file_6)) {
                $docadicional->file_6 = $destino.'/'.$file_6;
            }
            if (!empty($file_7)) {
                $docadicional->file_7 = $destino.'/'.$file_7;
            }
            if (!empty($file_8)) {
                $docadicional->file_8 = $destino.'/'.$file_8;
            }
            if (!empty($file_9)) {
                $docadicional->file_9 = $destino.'/'.$file_9;
            }
            if (!empty($file_10)) {
                $docadicional->file_10 = $destino.'/'.$file_10;
            }
            
                $docadicional->bactivo=1;

                $docadicional->save();
                
                
            //return redirect()->action('DeclaracionJOController@create');
        // realizar validate creo falta definir quien es obligatorio y quien no
        // 
        $gen_declaracion_inversion_id=$request->gen_declaracion_inversion_id;
        // realizar insert
        // 
        // 
        $updatePlanilla1 = PlanillaDjir_01::where('gen_declaracion_inversion_id', $request->declaracion_old)->first();
        $updatePlanilla1->actualizado = 1;
        $updatePlanilla1->save();


        $planillaDjir01=PlanillaDjir_01::where('gen_declaracion_inversion_id',$gen_declaracion_inversion_id)->first();
        $planillaDjir02=PlanillaDjir_02::where('gen_declaracion_inversion_id',$gen_declaracion_inversion_id)->first();
        $planillaDjir03=PlanillaDjir_03::where('gen_declaracion_inversion_id',$gen_declaracion_inversion_id)->first();
            //dd($request->all());
        Alert::success('Se ha registrado en la planilla 1!','Registrado Correctamente!')->persistent("Ok");
        return view('BandejaInversion.datos_inversion',compact('gen_declaracion_inversion_id','planillaDjir01','planillaDjir02','planillaDjir03'));
    }
}
