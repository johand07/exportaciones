<?php

namespace App\Http\Controllers;

use App\Models\GenConsignatario;
use App\Models\Pais;
use App\Models\CatTipoCartera;
use App\Models\CatTipoConvenio;
use App\Models\GenDua;
use App\Models\DetUsuario;
use App\Http\Requests\GenConsignatariosRequest;

use Session;
use Auth;
use Laracasts\Flash\Flash;
use Alert;

use Illuminate\Http\Request;

class RegistroConsignatarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $titulo="Comprador o Consignatario / Proveedor";
        $descripcion="Comprador o Consignatario / Proveedor Registrados";
        $consigs=GenConsignatario::where('gen_usuario_id',Auth::user()->id)->where('bactivo', 1)->get();
        return view('consignatario.index',compact('consigs','titulo','descripcion'));


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $titulo="Comprador o Consignatario / Proveedor";
        $descripcion="Comprador o Consignatario / Proveedor Registrados";
        $tipoExpor=DetUsuario::where('gen_usuario_id',Auth::user()->id)->first();
        if($tipoExpor->comerc==1){
          $tipocartera=CatTipoCartera::whereIn('id',[1,2])->pluck('descrip_tipocartera','id');
        }else{

        $tipocartera=CatTipoCartera::whereIn('id',[1])->pluck('descrip_tipocartera','id');
        }
        
        $paises=Pais::all()->pluck('dpais','id');
        $convenio=CatTipoConvenio::all()->pluck('nombre_convenio','id');
        
        return view('consignatario.create', compact('paises','titulo','descripcion','tipocartera','convenio'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(GenConsignatariosRequest $request)
    {   
      //dd($request->all());
      if (!empty($request->rif_empresa)) {
          $validar_rif=GenConsignatario::where('rif_empresa',$request->rif_empresa)->where('gen_usuario_id',Auth::user()->id)->first();
          if(!empty($validar_rif)){

            Alert::success('Debe registrar un nuevo rif','Este Rif ya se Encuentra Registrado')->persistent("Ok");
           return redirect()->action('RegistroConsignatarioController@create');
          }
      }
      
    if($request->cat_tipo_cartera_id==2){


        $explo=explode("-",$request->rif_empresa);
        $rif_seniat=$explo[0].$explo[1];
        //dd($rif_seniat);
        
        /*validamos que el rif sea valido con el ultimo digito verificador*/
            $retorno = preg_match("/^([VEJPG]{1})([0-9]{9}$)/", $rif_seniat);

        if ($retorno) {

            $digitos = str_split($rif_seniat);

            $digitos[8] *= 2;

            $digitos[7] *= 3;

            $digitos[6] *= 4;

            $digitos[5] *= 5;

            $digitos[4] *= 6;

            $digitos[3] *= 7;

            $digitos[2] *= 2;

            $digitos[1] *= 3;

            // Determinar dígito especial según la inicial del RIF

            // Regla introducida por el SENIAT

            switch ($digitos[0]) {

                case 'V':

                    $digitoEspecial = 1;

                    break;

                case 'E':

                    $digitoEspecial = 2;

                    break;

                case 'J':

                    $digitoEspecial = 3;

                    break;

                case 'P':

                    $digitoEspecial = 4;

                    break;

                case 'G':

                    $digitoEspecial = 5;

                    break;

            }

            $suma = (array_sum($digitos) - $digitos[9]) + ($digitoEspecial*4);

            $residuo = $suma % 11;

            $resta = 11 - $residuo;

            $digitoVerificador = ($resta >= 10) ? 0 : $resta;

            if ($digitoVerificador != $digitos[9]) {

                $retorno = false;

            }

        }
        /*fin validacion del rif*/
        //validamos si el rif cumplio con las condiciones
      
     /*if ($retorno == false) {
            Alert::warning('Error en el Rif!', 'El rif ingresado es incorrecto.')->persistent("Ok");
            //flash('<b>Email Incorrecto!</b>,  el campo no puede estar vacío.', 'danger');
            return redirect()->action('RegistroConsignatarioController@create');
        } */
    }
       //GenConsignatario::create($request->all());

       //$sin_dua=GenDua::where('numero_dua', '=', 'Sin Dua')->where('gen_usuario_id',Auth::user()->id)->first();
        
       //$rif_empre_conc=$request->letra_rif.'-'.$request->rif;

       //$rif_empresa=$request->rif_empresa;
       if($request->cat_tipo_cartera_id==1){
        $pais=$request->pais_id;
        $convenio=$request->cat_tipo_convenio_id;
       }elseif($request->cat_tipo_cartera_id==2){
          $pais=168;
          $convenio=7;
       }
        $consignatario= new GenConsignatario;
        $consignatario->cat_tipo_cartera_id=$request->cat_tipo_cartera_id;
        $consignatario->gen_usuario_id=Auth::user()->id;
        $consignatario->rif_empresa=$request->rif_empresa;
        $consignatario->nombre_consignatario=$request->nombre_consignatario;
        $consignatario->perso_contac=$request->perso_contac;
        $consignatario->cargo_contac=$request->cargo_contac;
        $consignatario->pais_id=$pais;
        $consignatario->cat_tipo_convenio_id=$convenio;
        $consignatario->ciudad_consignatario=$request->ciudad_consignatario;
        $consignatario->direccion=$request->direccion;
        $consignatario->telefono_consignatario=$request->telefono_consignatario;
        $consignatario->correo=$request->correo;
        $consignatario->save();


        //dd($rif_empresa);

       Alert::success('REGISTRO EXITOSO','Consignatario agregado')->persistent("Ok");
       return redirect()->action('RegistroConsignatarioController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\GenConsignatario  $genConsignatario
     * @return \Illuminate\Http\Response
     */
    public function show(GenConsignatario $genConsignatario)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\GenConsignatario  $genConsignatario
     * @return \Illuminate\Http\Response
     */
    public function edit(GenConsignatario $genConsignatario, $id)
    {
       $titulo="Editar Comprador o Consignatario / Proveedor";
       $descripcion="Editar Comprador o Consignatario / Proveedor Registrados";
       $consig=GenConsignatario::find($id);
       $tipocartera=CatTipoCartera::all()->pluck('descrip_tipocartera','id');
       $paises=Pais::all()->pluck('dpais','id');
       $convenio=CatTipoConvenio::all()->pluck('nombre_convenio','id');
       //$rif_empre_conc=$request->letra_rif.'-'.$request->rif;

       return view('consignatario.edit',compact('consig','paises','titulo','descripcion','tipocartera','convenio'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\GenConsignatario  $genConsignatario
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, GenConsignatario $genConsignatario, $id)
    {   

    if($request->cat_tipo_cartera_id==2){
        $explo=explode("-",$request->rif_empresa);
        $rif_seniat=$explo[0].$explo[1];
        //dd($rif_seniat);
        
        /*validamos que el rif sea valido con el ultimo digito verificador*/
            $retorno = preg_match("/^([VEJPG]{1})([0-9]{9}$)/", $rif_seniat);

        if ($retorno) {

            $digitos = str_split($rif_seniat);

            $digitos[8] *= 2;

            $digitos[7] *= 3;

            $digitos[6] *= 4;

            $digitos[5] *= 5;

            $digitos[4] *= 6;

            $digitos[3] *= 7;

            $digitos[2] *= 2;

            $digitos[1] *= 3;

            // Determinar dígito especial según la inicial del RIF

            // Regla introducida por el SENIAT

            switch ($digitos[0]) {

                case 'V':

                    $digitoEspecial = 1;

                    break;

                case 'E':

                    $digitoEspecial = 2;

                    break;

                case 'J':

                    $digitoEspecial = 3;

                    break;

                case 'P':

                    $digitoEspecial = 4;

                    break;

                case 'G':

                    $digitoEspecial = 5;

                    break;

            }

            $suma = (array_sum($digitos) - $digitos[9]) + ($digitoEspecial*4);

            $residuo = $suma % 11;

            $resta = 11 - $residuo;

            $digitoVerificador = ($resta >= 10) ? 0 : $resta;

            if ($digitoVerificador != $digitos[9]) {

                $retorno = false;

            }

        }
        /*fin validacion del rif*/
        //validamos si el rif cumplio con las condiciones
        if ($retorno == false) {
            Alert::warning('Error en el Rif!', 'El rif ingresado es incorrecto.')->persistent("Ok");
            //flash('<b>Email Incorrecto!</b>,  el campo no puede estar vacío.', 'danger');
            return redirect('exportador/consignatario/'.$id.'/edit');
        }
    }

        //GenConsignatario::find($id)->update($request->all());
    if($request->cat_tipo_cartera_id==1){
        $pais=$request->pais_id;
        $convenio=$request->cat_tipo_convenio_id;
       }elseif($request->cat_tipo_cartera_id==2){
          $pais=168;
          $convenio=7;
       }
        $consignatario=GenConsignatario::find($id);
        $consignatario->cat_tipo_cartera_id=$request->cat_tipo_cartera_id;
        $consignatario->gen_usuario_id=Auth::user()->id;
        $consignatario->rif_empresa=$request->rif_empresa;
        $consignatario->nombre_consignatario=$request->nombre_consignatario;
        $consignatario->perso_contac=$request->perso_contac;
        $consignatario->cargo_contac=$request->cargo_contac;
        $consignatario->pais_id=$pais;
        $consignatario->cat_tipo_convenio_id=$convenio;
        $consignatario->ciudad_consignatario=$request->ciudad_consignatario;
        $consignatario->direccion=$request->direccion;
        $consignatario->telefono_consignatario=$request->telefono_consignatario;
        $consignatario->correo=$request->correo;
        $consignatario->save();

        Alert::success('Registro Actualizado!', 'ACTUALIZACIÓN EXITOSA.')->persistent("Ok");
        return redirect()->action('RegistroConsignatarioController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\GenConsignatario  $genConsignatario
     * @return \Illuminate\Http\Response
     */
    public function destroy(GenConsignatario $genConsignatario, $id)
    {

        $consig=GenConsignatario::find($id)->update(['bactivo' => 0, 'updated_at' => date("Y-m-d H:i:s") ]);
            if ($consig) {
               return 1;
            }else{
                return 2;
            }
    }
}
