<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BandejaAnalistaResguardo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bandeja_analista_resguardo', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('gen_sol_resguardo_aduanero_id')->index()->nullable();
            $table->foreign('gen_sol_resguardo_aduanero_id')->references('id')->on('gen_sol_resguardo_aduanero');
            $table->unsignedInteger('gen_usuario_id')->index()->nullable();
            $table->foreign('gen_usuario_id')->references('id')->on('gen_usuario');
            $table->unsignedInteger('gen_status_id')->index()->nullable();
            $table->foreign('gen_status_id')->references('id')->on('gen_status');
            $table->date('fstatus')->nullable();
            $table->boolean('bactivo')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bandeja_analista_resguardo');
    }
}
