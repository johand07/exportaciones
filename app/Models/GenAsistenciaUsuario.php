<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GenAsistenciaUsuario extends Model
{
    protected $table = 'gen_asistencia_usuario';
    protected $fillable = [
        'gen_usuario_id',
        'motivo_de_contacto',
        'mensaje',
        'gen_status_id',
        'gen_usuario_atencion_id'
    ];

    /*relaciones*/
    public function asistenciaUsuario() {
        return $this->belongs('App\Models\GenUsuario','id','gen_usuario_id');
    }

    public function statusUsuario() {
        return $this->belongs('App\Models\GenStatus','id','gen_usuario_id');
    }

}