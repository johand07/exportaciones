<?php

namespace App\Http\Controllers;

use App\Models\PlanillaDjir_01;
use App\Models\PlanillaDjir_02;
use App\Models\PlanillaDjir_03;
use App\Models\GenSectorProductivoDjir_03;
use App\Models\GenDestinoInversionDjir_03;
use App\Models\GenDeclaracionInversion;
use App\Models\Pais;
use Illuminate\Http\Request;
use App\Http\Requests\Planilla3Request;
use Laracasth\Flash\Flash;
use Alert;

class Planilla3DecInversionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    //dd($request->all());


        // realizar validate creo falta definir quien es obligatorio y quien no
        // 
        //dd(explode(',', $request->cat_sector_productivo_eco_id));
        // realizar insert
            $planilla3 = new PlanillaDjir_03;
            $planilla3->gen_declaracion_inversion_id = $request->gen_declaracion_inversion_id ;
            $planilla3->inversion_recursos_externos = $request->inversion_recursos_externos ;
            $planilla3->det_invert_recursos = $request->det_invert_recursos;
            $planilla3->proyecto_nuevo = (isset($request->proyecto_nuevo))?$request->proyecto_nuevo:'';
            $planilla3->nombre_proyecto = ($request->proyecto_nuevo=='Si')?$request->nombre_proyecto:'';
            $planilla3->ubicacion = ($request->proyecto_nuevo=='Si')?$request->ubicacion:'';
            $planilla3->pais_id = ($request->inversion_recursos_externos == 'Si' && $request->proyecto_nuevo=='Si')?$request->pais_id:168;
            $planilla3->ampliacion_actual_emp = (isset($request->ampliacion_actual_emp))?$request->ampliacion_actual_emp:'';
            $planilla3->ampliacion_accionaria_dir = (isset($request->ampliacion_accionaria_dir))?$request->ampliacion_accionaria_dir:'';
            $planilla3->utilidad_reinvertida = (isset($request->ampliacion_actual_emp))?$request->utilidad_reinvertida:'';
            $planilla3->credito_casa_matriz = (isset($request->ampliacion_actual_emp))?$request->credito_casa_matriz:'';
            $planilla3->creditos_terceros = (isset($request->ampliacion_actual_emp))?$request->creditos_terceros:'';
            $planilla3->otra_dir = (isset($request->otra_dir))?$request->otra_dir:'';
            $planilla3->especifique_directa = (isset($request->especifique_directa))?$request->especifique_directa:'';
            $planilla3->participacion_accionaria_cart = (isset($request->participacion_accionaria_cart))?$request->participacion_accionaria_cart:'';
            $planilla3->bonos_pagare = (isset($request->bonos_pagare))?$request->bonos_pagare:'';
            $planilla3->otra_cart = (isset($request->otra_cart))?$request->otra_cart:'';
            $planilla3->periodo_inversion = (isset($request->periodo_inversion))?$request->periodo_inversion:'';
            $planilla3->gen_status_id = 16;
            $planilla3->estado_observacion = (isset($request->estado_observacion))?$request->estado_observacion:0;
            $planilla3->descrip_observacion = (isset($request->descrip_observacion))?$request->descrip_observacion:'';
            $planilla3->save();

            if(isset($request->cat_sector_productivo_eco_id[0])){
                foreach ($request->cat_sector_productivo_eco_id as $idTablaProducto => $idSectorProd) {
            
                    $idSectProd=explode(',',$idSectorProd);
        
                    foreach ($idSectProd as $key => $idSector) {
                        if (isset($idSector) && $idSector != '') {
                            $sectorProd = new GenSectorProductivoDjir_03;
                            $sectorProd->gen_declaracion_inversion_id = $request->gen_declaracion_inversion_id;
                            $sectorProd->cat_sector_productivo_eco_id = $idSector;
                            $sectorProd->save();
                        }
                    }
                }
            }
            
            if(isset($request->cat_destino_inversion_id[0])){
                foreach ($request->cat_destino_inversion_id as $idTablaProducto => $idDestinInv) {
            
                    $idDestInv=explode(',',$idDestinInv);
        
                    foreach ($idDestInv as $key => $idSector) {
                        if (isset($idSector) && $idSector != '') {
                            $sectorProd = new GenDestinoInversionDjir_03;
                            $sectorProd->gen_declaracion_inversion_id = $request->gen_declaracion_inversion_id;
                            $sectorProd->cat_destino_inversion_id = $idSector;
                            $sectorProd->save();
                        }                          
                        
                    }
                }  
            }
            
            //dd(1);
        // 
        
        $gen_declaracion_inversion_id=$request->gen_declaracion_inversion_id;

        // 
       // dd($gen_declaracion_inversion_id);

        $planillaDjir01=PlanillaDjir_01::where('gen_declaracion_inversion_id',$gen_declaracion_inversion_id)->first();
        $planillaDjir02=PlanillaDjir_02::where('gen_declaracion_inversion_id',$gen_declaracion_inversion_id)->first();
        $planillaDjir03=PlanillaDjir_03::where('gen_declaracion_inversion_id',$gen_declaracion_inversion_id)->first();

        if($planillaDjir01->gen_status_id == 16 && $planillaDjir02->gen_status_id == 16 && $planillaDjir03->gen_status_id == 16){
            
            $dec = GenDeclaracionInversion::find($gen_declaracion_inversion_id);
            $dec->gen_status_id = 16;
            $dec->save();
        }
        Alert::success('Se ha registrado en la planilla 3!','Registrado Correctamente!')->persistent("Ok");
       return redirect()->action('BandejaInversionController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PlanillaDjir_03  $planillaDjir_03
     * @return \Illuminate\Http\Response
     */
    public function show(PlanillaDjir_03 $planillaDjir_03)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\PlanillaDjir_03  $planillaDjir_03
     * @return \Illuminate\Http\Response
     */
    public function edit(PlanillaDjir_03 $planillaDjir_03, $id)
    {

       

        $planillaDjir03 = PlanillaDjir_03::where('gen_declaracion_inversion_id', $id)->first();

//dd($planillaDjir03->ampliacion_actual_emp_edit);

        $gen_destino_inversion_djir03=GenDestinoInversionDjir_03::where('gen_declaracion_inversion_id',$id)->get();

        $gen_sector_productivo_djir03=GenSectorProductivoDjir_03::where('gen_declaracion_inversion_id',$id)->get(); 
        $estado=Pais::all()->pluck('dpais','id');

        return view('BandejaInversion.editPlanilla3Inversion', compact('planillaDjir03','estado','gen_destino_inversion_djir03','gen_sector_productivo_djir03'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\PlanillaDjir_03  $planillaDjir_03
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PlanillaDjir_03 $planillaDjir_03)
    {
        //dd($request->all());
        if(isset($request->estado_observacion) && $request->estado_observacion == 0){
            $status=9;
            $estado_observacion=0;
        } else {
            $status=18;
            $estado_observacion=0;
        }
        /*$solDecla=GenDeclaracionInversion::where('id',$request->gen_declaracion_inversion_id)->update(['gen_status_id'=>$status,'updated_at' => date("Y-m-d H:i:s"), 'fstatus'=>date('Y-m-d') ]);*/

        $planilla3 = PlanillaDjir_03::where('gen_declaracion_inversion_id', $request->gen_declaracion_inversion_id)->first();
        $planilla3->inversion_recursos_externos = $request->inversion_recursos_externos ;
        $planilla3->det_invert_recursos = $request->det_invert_recursos;
        $planilla3->proyecto_nuevo = (isset($request->proyecto_nuevo))?$request->proyecto_nuevo:null;
        $planilla3->nombre_proyecto = ($request->proyecto_nuevo=='Si')?$request->nombre_proyecto:null;
        $planilla3->ubicacion = ($request->proyecto_nuevo=='Si')?$request->ubicacion:null;
        $planilla3->pais_id = ($request->proyecto_nuevo=='Si')?$request->pais_id:168;
        $planilla3->ampliacion_actual_emp = (isset($request->ampliacion_actual_emp))?$request->ampliacion_actual_emp:null;
        $planilla3->ampliacion_accionaria_dir = (isset($request->ampliacion_accionaria_dir))?$request->ampliacion_accionaria_dir:'';
        $planilla3->utilidad_reinvertida = (isset($request->ampliacion_actual_emp))?$request->utilidad_reinvertida:'';
        $planilla3->credito_casa_matriz = (isset($request->ampliacion_actual_emp))?$request->credito_casa_matriz:null;
        $planilla3->creditos_terceros = (isset($request->ampliacion_actual_emp))?$request->creditos_terceros:null;
        $planilla3->otra_dir = (isset($request->otra_dir))?$request->otra_dir:'';
        $planilla3->especifique_directa = (isset($request->especifique_directa))?$request->especifique_directa:'';
        $planilla3->participacion_accionaria_cart = (isset($request->participacion_accionaria_cart))?$request->participacion_accionaria_cart:'';
        $planilla3->bonos_pagare = (isset($request->bonos_pagare))?$request->bonos_pagare:'';
        $planilla3->otra_cart = (isset($request->otra_cart))?$request->otra_cart:'';
        $planilla3->periodo_inversion = (isset($request->periodo_inversion))?$request->periodo_inversion:null;
        $planilla3->gen_status_id = $status;
        $planilla3->estado_observacion = $estado_observacion;
        $planilla3->descrip_observacion = (isset($request->descrip_observacion))?$request->descrip_observacion:null;
        $planilla3->save();
       
        if(isset($request->cat_sector_productivo_eco_id[0])){

            $genSec = GenSectorProductivoDjir_03::where('gen_declaracion_inversion_id', $request->gen_declaracion_inversion_id)->get();
            foreach($genSec as $sect){
                GenSectorProductivoDjir_03::find($sect->id)->delete();
            }
            foreach ($request->cat_sector_productivo_eco_id as $idTablaProducto => $idSectorProd) {
        
                $idSectProd=explode(',',$idSectorProd);
    
                foreach ($idSectProd as $key => $idSector) {
                    
                    if(isset($idSector) && $idSector != ''){
                        $sectorProd = new GenSectorProductivoDjir_03;
                        $sectorProd->gen_declaracion_inversion_id = $request->gen_declaracion_inversion_id;
                        $sectorProd->cat_sector_productivo_eco_id = $idSector;
                        $sectorProd->save();
                    }
                    
                }
            }
        }
        
        if(isset($request->cat_destino_inversion_id[0])){
            $genSec = GenDestinoInversionDjir_03::where('gen_declaracion_inversion_id', $request->gen_declaracion_inversion_id)->get();
            foreach($genSec as $sect){
                GenDestinoInversionDjir_03::find($sect->id)->delete();
            }
            foreach ($request->cat_destino_inversion_id as $idTablaProducto => $idDestinInv) {
        
                $idDestInv=explode(',',$idDestinInv);
                
                foreach ($idDestInv as $key => $idSector) {
                    if(isset($idSector) && $idSector != ''){
                        $sectorProd = new GenDestinoInversionDjir_03;
                        $sectorProd->gen_declaracion_inversion_id = $request->gen_declaracion_inversion_id;
                        $sectorProd->cat_destino_inversion_id = $idSector;
                        $sectorProd->save();
                    }                    
                }
            }  
        }
        
        

        $gen_declaracion_inversion_id=$request->gen_declaracion_inversion_id;

        // 
       // dd($gen_declaracion_inversion_id);

        $planillaDjir01=PlanillaDjir_01::where('gen_declaracion_inversion_id',$gen_declaracion_inversion_id)->first();
        $planillaDjir02=PlanillaDjir_02::where('gen_declaracion_inversion_id',$gen_declaracion_inversion_id)->first();
        $planillaDjir03=PlanillaDjir_03::where('gen_declaracion_inversion_id',$gen_declaracion_inversion_id)->first();
        Alert::success('Se ha Actualizado la planilla 3!','Actualizado Correctamente!')->persistent("Ok");
       return redirect()->action('BandejaInversionController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PlanillaDjir_03  $planillaDjir_03
     * @return \Illuminate\Http\Response
     */
    public function destroy(PlanillaDjir_03 $planillaDjir_03)
    {
        //
    }
}
