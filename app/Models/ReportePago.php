<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ReportePago extends Model
{
    protected $table = 'reporte_pago';

    protected $fillable = [

           'tipo_pago_id',
           'cat_banco_admin_id',
           'gen_operador_cambiario_id',
           'gen_status_id',
           'gen_sol_inversionista_id',
           'gen_dvd_solicitud_id',
           'gen_declaracion_jo_id',
           'gen_certificado_id',
           'num_referencia',
           'fecha_reporte',
           'img_capture',
           'fstatus',
           'bactivo',
    ];

    protected $date =['created_at','updated_at'];


    public function TipoPago() {

     return $this->hasOne('App\Models\TipoPago','id','tipo_pago_id');

    }

    public function CatBancoAdmin() {

     return $this->hasOne('App\Models\CatBancoAdmin','id','cat_banco_admin_id');

    }

    public function GenOperadorCamb() {

     return $this->hasOne('App\Models\GenOperadorCamb','id','gen_operador_cambiario_id');

    }

    public function GenStatus() {

     return $this->hasOne('App\Models\GenStatus','id','gen_status_id');

    }

    public function GenSolInversionista() {

     return $this->hasOne('App\Models\GenSolInversionista','id','gen_sol_inversionista_id');

    }

    public function GenDVDSolicitud() {

     return $this->hasOne('App\Models\GenDVDSolicitud','id','gen_dvd_solicitud_id');

    }

    public function GenDeclaracionJO() {

     return $this->hasOne('App\Models\GenDeclaracionJO','id','gen_declaracion_jo_id');

    }

    public function GenCertificado() {

     return $this->hasOne('App\Models\GenCertificado','id','gen_certificado_id');

    }
}
