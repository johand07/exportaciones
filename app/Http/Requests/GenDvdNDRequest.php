<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GenDvdNDRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
           return [
          'realizo_venta'=>'required',
          'gen_operador_cambiario_id'=>'required_if:realizo_venta,1',
          'fdisponibilidad'=>'required_if:realizo_venta,1',
          'gen_factura_id'=>'required_if:realizo_venta,1',
          'mpercibido'=>'required_if:realizo_venta,1',
          'mvendido'=>'required_if:realizo_venta,1',
          'mretencion'=>'required_if:realizo_venta,1',
          'descripcion'=>'required_if:realizo_venta,1',
          'descripciondes'=>'required_if:realizo_venta,2',
          //'ruta_doc_dvd' => 'required_if:gen_operador_cambiario_id,2',<!--Congelan el oca en el caso de bdv -->


          
        ];
    }
}