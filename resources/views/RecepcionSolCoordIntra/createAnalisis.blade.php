@extends('templates/layoutlte_coordinador_intranet')
@section('content')
{{Form::open(['route'=>'AnalisisCoordinador','method'=>'POST','id'=>'formAnalisisCoordIntra']) }}
<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-12">
                @if(isset($solicitud))
                <table class="table table-striped table-bordered" style="width:100%">
                    <thead>
                        <tr>
                            <th>Número de Solicitud </th>
                            <th>Tipo de Solicitud</th>
                            <th>Estatus Solicitud</th>
                            <th>Fecha Solicitud</th>
                            <th>Monto Solicitud</th>
                            <th>Planilla ER</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>{{$solicitud->id}}</td>
                            <td>{{$solicitud->tiposolictud->solicitud}}</td>
                            <td>@if(!is_null($solicitud->gen_status_id)){{$solicitud->status->nombre_status}}@endif</td>
                            <td>{{$solicitud->fsolicitud}}</td>
                            <td>{{$solicitud->monto_solicitud}}</td>
                            <td>
                                @if( !is_null($solicitud->monto_solicitud)) 
                                    @if(Helper::consulta($solicitud->id,1)) 
                                        @if($solicitud->tipo_solicitud_id==1)
                                            <a href="{{action('PdfController@planillaEr',array('id'=>$solicitud->id))}}" class="glyphicon glyphicon-file btn btn-default btn-sm" title="Planilla ER" aria-hidden="true"></a>
                                            @else
                                            <a href="{{action('PdfController@planillaTecno',array('id'=>$solicitud->id))}}" class="glyphicon glyphicon-file btn btn-default btn-sm" title="Planilla ER" aria-hidden="true"></a>
                                        @endif
                                    @endif
                                @endif
                            </td>
                            <td>
                                @if(!is_null($solicitud->monto_solicitud) && ($solicitud->monto_solicitud > 0))
                                    <span style="color:green">Finalizada</span>
                                @else
                                    <span style="color:red">Incompleto</span>
                                @endif
                            </td>
                        </tr>
                    </tbody>
                </table>
                @else
                @endif
            </div>
        </div>
        <br><br>
        <div class="row text-center">
            <h3>Dvds</h3>
        </div>
        <div class="row">
            <div class="col-md-12">
                @if(isset($listaventa)) 
                    <table class="table table-striped table-bordered" style="width:100%">
                        <thead>
                            <tr>
                                <th>Número de Factura</th>
                                <th>Fecha Creación DVD</th>
                                <th>Monto FOB</th>
                                <th>Monto Percibido de la Venta</th>
                                <th>Monto Vendido al BCV</th>
                                <th>Monto Pendiente por Vender</th>
                                <th>Divisas</th>
                                <th>Observación</th>
                                <th>Fecha de Arribo</th>
                                <th>Planillas DVD</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($listaventa as  $lista)
                            <tr>
                                <td align="center">{{$lista->factura ? $lista->factura->numero_factura : 'Desistimiento - sin factura'}}</td>
                                <td align="center">{{$lista->fventa_bcv}}</td>
                                <td align="center">{{$lista->realizo_venta == 1 ? $lista->mfob : 'Desistimiento'}}</td>
                                <td align="center">{{$lista->realizo_venta == 1 ? $lista->mpercibido : 'Desistimiento'}}</td>
                                <td align="center">{{$lista->realizo_venta == 1 ? $lista->mvendido : 'Desistimiento'}}</td>
                                <td align="center">{{$lista->realizo_venta == 1 ? $lista->mpendiente : 'Desistimiento'}}</td>
                                <td align="center">{{$lista->factura ? $lista->factura->divisa->ddivisa_abr : 'Desistimiento - sin divisa'}}</td>
                                <td align="center">{{$lista->descripcion}}</td>
                                <td align="center">{{$lista->factura ? $lista->factura->dua->fembarque : 'Desistimiento'}}</td>
                                <td>
                                @if($lista->realizo_venta==1)
                                    <a href="{{action('PdfController@planillaDvd',array('id'=>$lista->id))}}" class="glyphicon glyphicon-file btn btn-default btn-sm" title="Planilla ER" aria-hidden="true">DVD</a>
                                @else
                                    <a href="{{action('PdfController@planillaDvdDes',array('id'=>$lista->id))}}" class="glyphicon glyphicon-file btn btn-default btn-sm" title="Planilla ER" aria-hidden="true">DVD</a>
                                @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @else
                @endif
            </div>
        </div>
        <br><br>
        <div class="row text-center">
            <h3>Notas de Crédito</h3>
        </div>
        <div class="row">
            <div class="col-md-12">
                @if(isset($ncredito)  && count($ncredito) > 0) 
                    <table class="table table-striped table-bordered" style="width:100%">
                        <thead>
                            <tr>
                            
                                <th>Consignatario</th>
                                <th>Nro. Nota de Crédito</th>
                                <th>Fecha de Emisión</th>
                                <th>Concepto Nota de Crédito</th>
                                <th>Monto Nota de Crédito</th>
                                <th>Nro. Factura Asociada</th>
                                <th>Justificación</th>
                            
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($ncredito as  $credit)
                            <tr>                   
                                <td>{{$credit->consignatario->nombre_consignatario}}</td>
                                <td>{{$credit->numero_nota_credito}}</td>
                                <td>{{$credit->femision}}</td>
                                <td>{{$credit->concepto_nota_credito}}</td>
                                <td>{{$credit->monto_nota_credito}}</td>
                                <td>{{$credit->factura->numero_factura}}</td>
                                <td>{{$credit->justificacion}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @else
                    <h3 class="text-success">No se encontraron Notas de Crédito Asociadas</h3>
                @endif
            </div>
        </div>
        <br><br>
        <div class="row text-center">
            <h3>Notas de Débito</h3>
        </div>
        <div class="row">
            <div class="col-md-12">
                @if(isset($NDebito) && count($listaventaNd) > 0)
                <table class="table table-striped table-bordered" style="width:100%">
                    <thead>
                        <tr>
                           
                            <th>Consignatario</th>
                            <th>Nro. Nota de Débito</th>
                            <th>Fecha de Emisión</th>
                            <th>Concepto de la Nota de Débito</th>
                            <th>Monto Nota de Débito</th>
                            <th>Nro. Factura Asociada</th>
                            <th>Justificacion</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($NDebito as  $debito)
                        <tr>                
                            <td>{{$debito->consignatario->nombre_consignatario}}</td>   
                            <td>{{$debito->num_nota_debito}}</td>
                            <td>{{$debito->fecha_emision}}</td>
                            <td>{{$debito->concepto_nota_debito}}</td>
                            <td>{{$debito->monto_nota_debito}}</td>
                            <td>{{$debito->facturand->numero_factura}}</td>
                            <td>{{$debito->justificacion}}</td>
                        </tr>
                      @endforeach
                    </tbody>
                </table>
                @else
                    <h3 class="text-success">No se encontraron Notas de Débito Asociadas</h3>
                @endif
            </div>
        </div>
        <br><br>
        <div class="row text-center">
            <h3>DVDs Notas de Débito</h3>
        </div>
        <div class="row">
            <div class="col-md-12">
                @if(isset($listaventaNd) && count($listaventaNd) > 0) 
                    <table class="table table-striped table-bordered" style="width:100%">
                        <thead>
                            <tr>
                                <th>Número de Factura</th>
                                <th>Fecha Creación Dvd</th>
                                <th>Monto Fob</th>
                                <th>Monto Percibido de la Venta</th>
                                <th>Monto Vendido al BCV</th>
                                <th>Monto Pendiente por Vender</th>
                                <th>Divisa</th>
                                <th>Observaciones</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($listaventaNd as  $lista)
                            <tr>
                                <td>@if(!is_null($lista->gen_factura_id)){{$lista->factura->numero_factura}}@endif</td>
                                <td>{{$lista->fventa_bcv}}</td>
                                <td>@if(!is_null($lista->mfob)){{$lista->mfob}}@endif</td>
                                <td>@if(!is_null($lista->mpercibido)){{$lista->mpercibido}}@endif</td>
                                <td>@if(!is_null($lista->mvendido)){{$lista->mvendido}}@endif</td>
                                <td>@if(!is_null($lista->mpendiente)){{$lista->mpendiente}}@endif</td>
                                <td>@if(!is_null($lista->gen_factura_id)){{$lista->factura->divisa->ddivisa_abr}}@endif</td>
                                <td>{{$lista->descripcion}}</td>
                                <td>
                                    @if($lista->realizo_venta==1)
                                        <a href="{{action('PdfController@planillaNotaD',array('id'=>$lista->id))}}" class="glyphicon glyphicon-file btn btn-default btn-sm" title="Planilla ER" aria-hidden="true">DVD</a>
                                    @else
                                        <a href="{{action('PdfController@planillaDvdDesND',array('id'=>$lista->id))}}" class="glyphicon glyphicon-file btn btn-default btn-sm" title="Planilla ER" aria-hidden="true">DVD</a>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                @else
                    <h3 class="text-success">No se encontraron DVDs de Notas de Débito Asociadas</h3>
                @endif
            </div>
        </div>
        <br><br>
        <div class="row">
            <div class="col-md-12">
                <!--campo oculto gen_solicitud_id-->
                <input type="hidden" name="gen_solicitud_id" value="{{$solicitudId}}">
                <div class="form-group">
                    {!! Form::label('', 'Seleccione un Estatus') !!}
                    {!!Form::select('gen_status_id',$status,null,['placeholder'=>'Seleccione un estatus','class'=>'form-control','id'=>'gen_status_id', 'required'=>'true']) !!}
                </div>
                <br>
                <div class="form-group" id="observacion_analisis" style="display: none;">
                    <label><b><u>Observaciones:</u></b></label>
					<textarea rows="4"  class="form-control text-left" name="observacion_coordinador_intra" id="observacion_coordinador_intra"></textarea>
                </div>
            </div>
        </div>
        <div class="row text-center">
            <div class="col-md-12">
                <br>
                <a href="{{route('SolEvaluadasAnalistas')}}" class="btn btn-warning">Cancelar</a>
                <input class="btn btn-success" type="submit" name="" value="Asignar">
            </div>
        </div>
    </div>
</div>

{{Form::close()}}
@stop
<script>
    function observacionStatus (){
        let status = $('#gen_status_id').val();
        console.log('status analista', status);
        if(status === '31') {
            $('#observacion_analisis').show();
        } else {
            $('#observacion_analisis').hide();
        }
    }
</script>