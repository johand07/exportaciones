<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class Planilla3Request extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
         return [
          'cat_sector_productivo_eco_id'    =>      'required',
          'cat_destino_inversion_id'        =>      'required',
          'inversion_recursos_externos'     =>      'required',
          'det_invert_recursos'             =>      'required',
          'proyecto_nuevo'                  =>      'required',
          'nompre_proyecto'                 =>      'required',
          'ubicacion'                       =>      'required',
          'pais_id'                         =>      'required',
          'ampliacion_actual_emp'           =>      'required',
          'ampliacion_accionaria_dir'       =>      'required',
          'utilidad_reinvertida'            =>      'required',
          'credito_casa_matriz'             =>      'required',
          'creditos_terceros'               =>      'required',
          'otra_dir'                        =>      'required',
          'participacion_accionaria_cart'   =>      'required',
          'bonos_pagare'                    =>      'required',
          'otra_cart'                       =>      'required',
          'periodo_inversion'               =>      'required',
          'condicion'                       =>      'required',
        ];
    }
}
