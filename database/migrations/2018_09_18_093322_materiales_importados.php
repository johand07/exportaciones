<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MaterialesImportados extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('materiales_importados', function (Blueprint $table) {
          $table->engine = 'InnoDB';
          $table->increments('id');
          $table->unsignedInteger('planilla_2_mi_id')->index()->nullable();
          $table->foreign('planilla_2_mi_id')->references('id')->on('planilla_2');
          $table->unsignedInteger('pais_id')->index()->nullable();
          $table->foreign('pais_id')->references('id')->on('pais');
          $table->string('codigo',100)->nullable();
          $table->string('descripcion',100)->nullable();
          $table->string('incidencia_costo', 100)->nullable();
          $table->boolean('bactivo')->default(1);
          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('materiales_importados'); 
    }
}
