<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use App\Models\GenFactura;
use App\Models\GenNotaCredito;
use App\Models\FacturaSolicitud;
use Illuminate\Http\Request;
use Laracasts\Flash\Flash;
use App\Models\GenDivisa;
use App\Models\Incoterm;
use App\Models\GenConsignatario;
use App\Models\GenDua;
use App\Models\TipoSolicitud;
use App\Models\NumeroCuotas;
use App\Models\TipoInstrumento;
use App\Models\DetUsuario;
use App\Http\Requests\GenFacturaRequest;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Log;
use Auth;
use Alert;


class GenFacturaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $titulo="Factura";
      $descripcion="Listado de Factura";
      $facturas=GenFactura::where('gen_usuario_id',Auth::user()->id)->orderBy('gen_dua_id','ASC')->get();
      
      if($facturas->first()==null){
        $id_sol=null;
      }else{
        $id_sol=$facturas[0]->tipo_solicitud_id;
      }
      
    
      $tipoExpor=DetUsuario::where('gen_usuario_id',Auth::user()->id)->first();


      return view('factura.index',compact('titulo','descripcion','facturas','tipoExpor','id_sol'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

      $dua=array();
      $titulo="Registro de Factura";
      //$descripcion="Registro de Factura";
      $descripcion="Se le recomienda, tener toda la información relacionada a sus facturas y sus productos, asi como tambien verificar los montos y fechas solicitada por el sistema";

    /* $dias_plazo=[
        '10'=>'10',
        '15'=>'15',
        '30'=>'30',
        '40'=>'40',
        '60'=>'60',
        '80'=>'80',
        '120'=>'120',
        '180'=>'180'
      ];*/

      $incoterm=Incoterm::all()->pluck('nombre_incoterm','id');
      $divisas=GenDivisa::orderBy('ddivisa_abr','asc')->pluck('ddivisa_abr','id');
      $tipoSoli=TipoSolicitud::whereIn('id',[1,2])->get();
      Session::put('metodo',"Registro");
      $tipoInstru=TipoInstrumento::where('descrip_tipo_instrumento','<>','Sin Instrumento')->whereIn('id',[1,2,3])->get();

      $tipoExpor=DetUsuario::where('gen_usuario_id',Auth::user()->id)->first();

       $data=GenDua::where('gen_usuario_id',Auth::user()->id)->with('aduanassal')->whereNotIn('numero_dua',['Sin Dua'])->where('bactivo',1)->get();


      foreach($data as $key=>$value) {

      $dua[$value->id]=$value->numero_dua.'-'.$value->aduanassal->daduana;

      }



      $validar_dua=GenDua::where('gen_usuario_id',Auth::user()->id)->first();

      
    if($tipoExpor->produc_tecno==2) { 

       if($validar_dua==null) { 

        Alert::warning('No existen Duas registradas ,culmine ese proceso para continuar. ','Acción Denegada!')->persistent("OK");

        return redirect()->action('GenFacturaController@index');

        }else{ 

         
        $d=GenDua::where('gen_usuario_id',Auth::user()->id)->whereNotIn('numero_dua',['Sin Dua'])->get();

        

        foreach ($d as $key => $value) {
               if($value->bactivo==1){
                   $z=1;
                   break;
               }else{ $z=0;}
        }


       
         if($z==0) {

             Alert::warning('No existen duas disponible, registre una para continuar. ','Acción Denegada!')->persistent("OK");

              return redirect()->action('GenFacturaController@index');

            }
            else { 

            return view('factura.create',compact('divisas','incoterm','dua','titulo','descripcion','tipoSoli','tipoInstru','tipoExpor'));
 
         }



        }
    }else{

     return view('factura.create',compact('divisas','incoterm','dua','titulo','descripcion','tipoSoli','tipoInstru','tipoExpor'));

    }

  }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(GenFacturaRequest $request)
    {

     if ($request->tipo_solicitud_id==1) {

       $id_dua=$request->gen_dua_id;
       //dd(1);

      }else{

        $dua_insert=GenDua::where('numero_dua', '=', 'Sin Dua')->where('gen_usuario_id',Auth::user()->id)->first();
        //dd($dua_insert);

        if(!is_null($dua_insert)){

        $id_dua=$dua_insert->id;


        }else{

        $dua_defecto=new GenDua;
        $dua_defecto->gen_usuario_id=Auth::user()->id;
        $dua_defecto->gen_agente_aduana_id  =1;
        $dua_defecto->gen_transporte_id=1;
        $dua_defecto->gen_aduana_salida_id=1;
        $dua_defecto->numero_dua='Sin Dua';
        $dua_defecto->save();

        $dua_insert=GenDua::where('numero_dua', '=', 'Sin Dua')->where('gen_usuario_id',Auth::user()->id)->first();

        $id_dua=$dua_insert->id;

      }

    }

///////////////////////Validacion para forma de pago//////////////////


      if ($request->forma_pago_id==2) {


      $plazopago=$request->plazo_pago;

      }else{

      
            $plazopago=0;

            }
    

////////////////////////////////////////////////////////////////////////
     //dd($id_dua);

      $factura= new GenFactura;
      $factura->gen_usuario_id=$request->gen_usuario_id;
      $factura->tipo_solicitud_id=$request->tipo_solicitud_id;
      $factura->forma_pago_id=$request->forma_pago_id;
      $factura->gen_divisa_id=$request->gen_divisa_id;
      $factura->fecha_estimada_pago=$request->fecha_estimada_pago;
      $factura->plazo_pago=$plazopago;
      $factura->tipo_plazo=$request->tipo_plazo;
      $factura->tipo_instrumento_id=$request->tipo_instrumento_id;
      $factura->gen_dua_id=$id_dua;
      $factura->incoterm_id=$request->incoterm_id;
      $factura->numero_factura=$request->numero_factura;
      $factura->fecha_factura=$request->fecha_factura;
      $factura->especif_instrumento=$request->especif_instrumento;
      $factura->monto_fob=$request->monto_fob;
      $factura->monto_flete=$request->monto_flete;
      $factura->monto_seguro=$request->monto_seguro;
      $factura->otro_monto=$request->otro_monto;
      $factura->especif_pago=$request->especif_pago;
      $factura->monto_total=$request->monto_total;
      $factura->save();



    $data=GenFactura::where('numero_factura',$request->numero_factura)->where('gen_usuario_id',Auth::user()->id)->first();
    Session::put('factura_id',$data->id);



    if($request->tipo_plazo=="Parcial")
    {
        for($i=0;$i<count($request->monto);$i++)
       {
         $insert2=NumeroCuotas::create([
        'gen_factura_id'=>$data->id,
        'num_cuotas'=>$request->num_cuotas[$i],
        'monto'=>$request->monto[$i],
       'fecha_pago'=>$request->fecha_pago[$i]
         ]);
      }

    }

        if($request->tipo_solicitud_id==1)
        {
        return redirect('exportador/productos/create');
        }else{
        return redirect('exportador/productos_tecno/create');
        }

  }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\GenFactura  $genFactura
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {
       //dd($id);
       //dd($request->gen_factura_id);
        $gen_factura=GenNotaCredito::where('gen_factura_id',$id)->get();
       if (!empty($gen_factura)) {
         return response()->json($gen_factura);
       }else{
          $gen_factura=array(
                  'numero_nota_credito'  => 0,
                  'id'  => 0
                );
          return response()->json($gen_factura);
       }
    }
    

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\GenFactura  $genFactura
     * @return \Illuminate\Http\Response
     */
    public function edit(GenFactura $factura)
    {

      //dd($factura);
      $dua=array();
      $titulo="Editar Factura";
      $descripcion="Recuerde que si su factura esta asociada a la solicitud de regstro de exportación, dicho documento tambien sera afecto ";

    /*  $dias_plazo=[
      '10'=>'10',
      '15'=>'15',
      '30'=>'30',
      '40'=>'40',
      '60'=>'60',
      '80'=>'80',
      '120'=>'120',
      '180'=>'180'
    ];*/



      $tipoSoli=TipoSolicitud::all();
      $cuotas=NumeroCuotas::where('gen_factura_id',$factura->id)->get();
      $incoterm=Incoterm::all()->pluck('nombre_incoterm','id');
      $divisas=GenDivisa::all()->pluck('ddivisa_abr','id');
      $tipoExpor=DetUsuario::where('gen_usuario_id',Auth::user()->id)->first();
      $data=GenDua::where('gen_usuario_id',Auth::user()->id)->with('aduanassal')->whereNotIn('numero_dua',['Sin Dua'])->get();
      $tipoInstru=TipoInstrumento::where('descrip_tipo_instrumento','<>','Sin Instrumento')->whereIn('id',[1,2,3])->get();
      Session::put('metodo',"Editar");
      foreach($data as $key=>$value) {

      $dua[$value->id]=$value->numero_dua.'-'.$value->aduanassal->daduana;

      }

      return view('factura.edit',compact('factura','titulo','descripcion','incoterm','divisas','dua','tipoSoli','cuotas','tipoInstru','tipoExpor'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\GenFactura  $genFactura
     * @return \Illuminate\Http\Response
     */
    public function update(GenFacturaRequest  $request, GenFactura $factura)
    {
      GenFactura::find($factura->id)->update($request->all());
      $data=GenFactura::where('numero_factura',$request->numero_factura)->first();
      $genuser = $data->gen_usuario_id;
      $fob_ant=$request->mont_fob_ant;
      $fob_act=$request->monto_fob;
///////////////////////////////////////////////////
      if (floatval($fob_ant) !== floatval($fob_act)) {
         $table =  DB::table('factura_solicitud')
                    ->join('gen_factura', 'factura_solicitud.gen_factura_id', '=', 'gen_factura.id')
                    ->join('gen_solicitud', 'factura_solicitud.gen_solicitud_id', '=', 'gen_solicitud.id')
                    ->where('gen_solicitud.gen_usuario_id', $genuser)
                    ->where('gen_factura.id', $factura->id)
                    ->select('gen_solicitud.*')
                    ->first();
                  if (@$table->id){
                  $fob=$table->monto_solicitud;
                  $fob=$fob-$fob_ant;
                  $fob=$fob+$fob_act;
                  DB::table('gen_solicitud')->where('id', $table->id)->update(['monto_solicitud' => $fob]);
                  }          
  }
 /////////////////////////////////////////////////////////////
       if($request->tipo_plazo=="Parcial"){
          for($i=0;$i<count($request->monto);$i++)
          {
             $insert2=NumeroCuotas::updateOrCreate(
            [ 'id'=>$request->id[$i]],
            ['gen_factura_id'=>$data->id,
            'num_cuotas'=>$request->num_cuotas[$i],
            'monto'=>$request->monto[$i],
           'fecha_pago'=>$request->fecha_pago[$i]
             ]);
          }
      }

      if($data->tipo_solicitud_id==1){
       return redirect("exportador/productos/$factura->id/edit");
      }else {
       return redirect("exportador/productos_tecno/$factura->id/edit");
      }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\GenFactura  $genFactura
     * @return \Illuminate\Http\Response
     */
    public function destroy(GenFactura $factura)
    {
        //
    }
    public function EliminarRegistroFacturaUpdate(Request $request, GenFactura $factura)
    {
      
      Log::info($request);
    
      $FacturaSolicitud=GenFactura::where('id', $request->id)->update(['bactivo' => 2 ]);
     
      $response=1;
      return response()->json($response);
      return redirect("exportador/factura");
    }

}
