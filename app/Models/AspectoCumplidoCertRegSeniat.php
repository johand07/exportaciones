<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AspectoCumplidoCertRegSeniat extends Model
{
    protected $table = 'aspecto_cumplido_cert_reg_seniat';
    protected $fillable = [

           'id',
           'gen_cert_reg_seniat_id',
           'cat_cert_reg_seniat_id',
           'bactivo',
    ];

    protected $date =['created_at','updated_at'];

    public function GenCertRegSeniat() {
   
        return $this->hasOne('App\Models\GenCertRegSeniat','id','gen_cert_reg_seniat_id');
   
       }

       public function CatCertRegSeniat() {

        return $this->hasOne('App\Models\CatCertRegSeniat','id','cat_cert_reg_seniat_id');
   
       }
}
