<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GenSolicitudRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

        'fsolicitud'=>'required',
        'tipo_solicitud_id'=>'required',
        'opcion_tecnologia'=>'required_if:tipo_solicitud_id,2',
        'consig_tecno'=>'required_if:tipo_solicitud_id,2',


        ];
    }
}
