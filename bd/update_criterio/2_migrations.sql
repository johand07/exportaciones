-- phpMyAdmin SQL Dump
-- version 4.6.6deb5ubuntu0.5
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 11-02-2021 a las 10:10:35
-- Versión del servidor: 5.7.33-0ubuntu0.18.04.1
-- Versión de PHP: 7.1.29-1+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `exportaciones`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2018_05_04_120440_cat_tipos_usuarios', 1),
(2, '2018_05_07_110633_cat_export_prod', 1),
(3, '2018_05_07_121302_gen_status', 1),
(4, '2018_05_07_121428_gen_tipo_usuario', 1),
(5, '2018_05_07_121548_cat_preguantas_seg', 1),
(6, '2018_05_07_121642_gen_usuario', 1),
(7, '2018_05_07_121643_gen_sector', 1),
(8, '2018_05_07_121744_gen_actividad_eco', 1),
(9, '2018_05_07_121857_gen_tipo_empresa', 1),
(10, '2018_05_07_121918_pais', 1),
(11, '2018_05_07_122024_estado', 1),
(12, '2018_05_07_122128_municipio', 1),
(13, '2018_05_07_122249_parroquia', 1),
(14, '2018_05_07_122342_circuns_judicial', 1),
(15, '2018_05_07_122420_det_usuario', 1),
(16, '2018_05_07_122539_gen_roles', 1),
(17, '2018_05_07_122622_gen_usuario_roles', 1),
(18, '2018_05_07_122731_gen_funciones', 1),
(19, '2018_05_07_122850_gen_roles_funciones', 1),
(20, '2018_05_07_122940_cat_tipo_cartera', 1),
(21, '2018_05_07_122941_cat_tipo_convenio', 1),
(22, '2018_05_07_122942_gen_consignatario', 1),
(23, '2018_05_07_123026_gen_agente_aduanal', 1),
(24, '2018_05_07_123142_gen_aduana_salida', 1),
(25, '2018_05_07_123240_gen_transporte', 1),
(26, '2018_05_07_123311_gen_dua', 1),
(27, '2018_05_07_123415_gen_divisa', 1),
(28, '2018_05_07_123515_forma_pago', 1),
(29, '2018_05_07_123630_tipo_instrumento', 1),
(30, '2018_05_07_123707_incoterm', 1),
(31, '2018_05_07_123850_tipo_solicitud', 1),
(32, '2018_05_07_123853_gen_factura', 1),
(33, '2018_05_07_123915_gen_unidad_medida', 1),
(34, '2018_05_07_124047_tipo_permiso', 1),
(35, '2018_05_07_124048_permiso', 1),
(36, '2018_05_07_124049_permiso_productos', 1),
(37, '2018_05_07_124052_det_prod_factura', 1),
(38, '2018_05_07_124129_gen_nota_credito', 1),
(39, '2018_05_07_124351_gen_juridico_cencoex', 1),
(40, '2018_05_07_124424_gen_juridico_seniat', 1),
(41, '2018_05_07_124533_gen_arancel_mercosur', 1),
(42, '2018_05_07_124643_gen_arancel_nandina', 1),
(43, '2018_07_04_110844_accionistas', 1),
(44, '2018_07_04_111925_productos', 1),
(45, '2018_07_09_085950_nota_debito', 1),
(46, '2018_07_09_091127_gen_anticipo', 1),
(47, '2018_07_10_171347_numero_cuotas', 1),
(48, '2018_07_12_134844_det_prod_tecnologia', 1),
(49, '2018_07_18_184519_gen_solicitud', 1),
(50, '2018_07_18_184528_cat_regimen_export', 1),
(51, '2018_07_18_184530_cat_regimen_especial', 1),
(52, '2018_07_18_184651_financiamiento_solicitud', 1),
(53, '2018_07_18_184749_factura_solicitud', 1),
(54, '2018_07_18_184840_det_producto_solicitud', 1),
(55, '2018_07_18_194530_gen_operador_cambiario', 1),
(56, '2018_07_18_194631_gen_dvd_anticipo', 1),
(57, '2018_08_14_102746_casa_matriz', 1),
(58, '2018_08_14_102804_ultima_export', 1),
(59, '2018_08_20_101531_gen_dvd_solicitud', 1),
(60, '2018_08_21_161834_gen_parametro', 1),
(61, '2018_08_22_052737_gen_dvd_nd', 1),
(62, '2018_08_27_153725_ConsigTecno', 1),
(63, '2018_08_31_130559_gen_factura_nd', 1),
(64, '2018_09_05_092944_usuarios_api', 1),
(65, '2018_09_06_170141_gen_usuario_oca', 1),
(66, '2018_09_06_170204_gen_ente', 1),
(67, '2018_09_06_170205_cat_documentos', 1),
(68, '2018_09_06_170206_documentos_dvd', 1),
(69, '2018_09_07_110304_tipo_doc_banco', 1),
(70, '2018_09_07_130028_er_recepcion_oca', 1),
(71, '2018_09_07_130039_nd_recepcion_oca', 1),
(72, '2018_09_12_162657_gen_declaracion_jo', 1),
(73, '2018_09_12_162856_det_declaracion_produc', 1),
(74, '2018_09_17_163405_planilla_2', 1),
(75, '2018_09_18_093322_materiales_importados', 1),
(76, '2018_09_18_095054_materiales_nacionales', 1),
(77, '2018_09_18_095719_planilla_3', 1),
(78, '2018_09_18_101210_costos_directos', 1),
(79, '2018_09_18_113915_costos_inderectos', 1),
(80, '2018_09_18_114427_planilla_4', 1),
(81, '2018_09_18_114845_planilla_5', 1),
(82, '2018_09_18_115129_planilla_6', 1),
(83, '2018_09_18_121455_analisis_calificacion', 1),
(84, '2018_09_18_121946_criterio_analisis', 1),
(85, '2018_09_21_101832_documentos_nd', 1),
(86, '2018_09_28_110552_det_declaracion_produc_pais', 1),
(87, '2018_10_15_120150_det_convenio_pais', 1),
(88, '2018_10_16_114851_bandeja_entrada_djo', 1),
(89, '2018_10_16_124848_cat_bolivia_vzla', 1),
(90, '2018_10_16_125035_alter_table_planilla2', 1),
(91, '2018_10_16_125644_cat_colombia_vzla', 1),
(92, '2018_10_16_125658_cat_peru_vzla', 1),
(93, '2018_10_16_125708_cat_cuba_vzla', 1),
(94, '2018_10_16_125734_cat_ald_tp', 1),
(95, '2018_10_16_125752_cat_can_mercosur', 1),
(96, '2018_10_16_144937_alter_table_planilla3', 1),
(97, '2018_10_16_144951_alter_table_planilla4', 1),
(98, '2018_10_16_144959_alter_table_planilla5', 1),
(99, '2018_10_16_145010_alter_table_planilla6', 1),
(100, '2018_10_25_141826_alter_table_pais', 1),
(101, '2018_10_26_152846_alter_table_criterio', 1),
(102, '2018_10_26_152918_alter_table_analisis', 1),
(103, '2018_11_07_173322_alter_table_gen_declaracion_jo', 1),
(104, '2018_10_08_133441_nota_credito_solicitud', 2),
(105, '2018_12_26_155348_cat_tipo_visa', 2),
(106, '2018_12_26_155845_gen_sol_inversionista', 2),
(107, '2018_12_28_093437_gen_accionista_inversionista', 2),
(108, '2018_12_28_110348_bandeja_sol_inversionista', 2),
(109, '2019_01_08_105021_Cat_tipo_contrato', 2),
(110, '2019_01_08_105054_Gen_trasfer_tecno_inversion', 2),
(111, '2019_01_08_112304_gen_sol_inversion', 2),
(112, '2019_01_08_112325_productos_inversion', 2),
(113, '2019_01_08_112340_capital_social_accionista', 2),
(114, '2019_01_11_103104_hist_sesion_user', 2),
(115, '2019_01_17_175433_cat_doc_inversiones', 2),
(116, '2019_01_30_134612_eventos', 2),
(117, '2019_01_30_134613_gestion_invitacion', 2),
(118, '2019_01_14_172116_alter_det_declaracion', 3),
(119, '2019_02_04_084558_bandeja_coord_sol_inversion', 3),
(120, '2019_02_08_151815_alter_table_gen_sol_inversionista', 3),
(121, '2019_02_12_101516_alter_gen_consignatario', 3),
(122, '2019_02_14_160247_firmantes_certificado', 3),
(123, '2019_02_25_160448_cat_tipo_doc', 3),
(124, '2019_08_23_195933_cat_usa_vzla', 4),
(125, '2019_08_28_221413_cat_ue_vzla', 4),
(126, '2019_08_28_221435_cat_cnd_vzla', 4),
(127, '2020_09_09_132118_alter_table_planilla_2_djo', 5),
(128, '2020_09_09_134746_alter_table_materiales_importados_1', 6),
(129, '2020_09_09_135420_alter_table_materiales_importados', 6);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=130;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
