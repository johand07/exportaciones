<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FacturaSolicitud extends Model
{
    protected $table='factura_solicitud';

    protected $fillable =[

     'id',
     'gen_factura_id',
     'gen_solicitud_id',
     'numero_nota_credito',
     'bactivo',
    ]; 

  public function factura(){
     
     return $this->hasOne('App\Models\GenFactura','id','gen_factura_id');
  }

    /*public function solicitud() {

     return $this->hasMany('App\Models\GenSolicitud','id','gen_solicitud_id');

    }*/
  public function solicitud(){
     
     return $this->hasOne('App\Models\GenSolicitud','id','gen_solicitud_id');
  }

}
