<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
//use Illuminate\Foundation\Auth\User;

class welcome extends Mailable
{
    use Queueable, SerializesModels;
    public $razon_social;
    public $rif;
    public $email;
    public $clave;
    public $cod_empresa;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($razon_social,$rif,$email,$clave, $cod_empresa)
    {
      $this->razon_social=$razon_social;
      $this->rif = $rif;
      $this->email = $email;
      $this->clave=$clave;
      $this->cod_empresa=$cod_empresa;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $dia=date("d.m.Y");
        $hora=date("H:i:s");
        return $this->view('emails.welcome')
        ->subject("VENTANILLA UNICA DE COMERCIO EXTERIOR (VUCE) - Enviado $dia - $hora");
        //->from('hiperiom412@gmail.com','RansesFrom')
    }
}
