@extends('templates/layoutlte')
@section('content')
<div class="panels">
  <div class="row">
    <div class="col-md-1">
      <a class="btn btn-primary" href="{{action('PdfController@PlanillaConsigDoc')}}">
        <span class="glyphicon glyphicon-download" title="Agregar" aria-hidden="true"></span>
        Planilla Consignación de Documentos
      </a>
    </div>
    <div class="col-md-3"></div>
    <div class="col-md-1">
      <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal" title="Agregar"
        aria-hidden="true">Registar Nueva Venta
      </button>
    </div>
    <div class="col-md-3"></div>
  </div>
  <hr>
  <div class="row">
    <div class="col-md-12">
      @if(isset($listaventa))
      <table id="listaventa" class="table table-striped table-bordered" style="width:100%">
        <thead>
          <tr>
            <th>Factura</th>
            <th>F. Creación</th>
            <th>Mont FOB</th>
            <th>Mont Pend</th>
            <th>Mont Vend</th>
            <th>Mont Pend</th>
            <th>Divisas</th>
            <!-- th>Observación</!-- -->
            <th>F. Arribo</th>
            <th>Descarga</th>
            <th>Acciones</th>
          </tr>
        </thead>
        <tbody>
          @foreach($listaventa as $lista)
          <tr>
            <td align="center">{{$lista->factura ? $lista->factura->numero_factura : 'Desistimiento - sin factura'}}
            </td>
            <td align="center">{{$lista->fventa_bcv}}</td>
            <td align="center">{{$lista->realizo_venta == 1 ? $lista->mfob : 'Desistimiento'}}</td>
            <td align="center">{{$lista->realizo_venta == 1 ? $lista->mpercibido : 'Desistimiento'}}</td>
            <td align="center">{{$lista->realizo_venta == 1 ? $lista->mvendido : 'Desistimiento'}}</td>
            <td align="center">{{$lista->realizo_venta == 1 ? $lista->mpendiente : 'Desistimiento'}}</td>
            <td align="center">{{$lista->factura ? $lista->factura->divisa->ddivisa_abr : 'Desistimiento - sin divisa'}}
            </td>
            <!--td align="center">{{$lista->descripcion}}</!--td -->
            <td align="center">{{$lista->factura ? $lista->factura->dua->fembarque : 'Desistimiento'}}</td>
            <td>
              @if($lista->realizo_venta==1)
              <a href="{{action('PdfController@planillaDvd',array('id'=>@$lista->id))}}"
                class="glyphicon glyphicon-file btn btn-default btn-sm" title="Planilla ER" aria-hidden="true">DVD</a>
              @else
              <a href="{{action('PdfController@planillaDvdDes',array('id'=>@$lista->id))}}"
                class="glyphicon glyphicon-file btn btn-default btn-sm" title="Planilla ER" aria-hidden="true">DVD</a>
              @endif
              <!--Boton para usuarios ya declarados de documentos dvd-->
              <!--Congelando boton para la carga de doc de usuario con dvd ya declaradas-->
              {{--  @if($lista->gen_operador_cambiario_id==2)
                    @if(empty($lista->docDvd))
             <a href="{{route('DocumentosDVD.create',array('id'=>$lista->id))}}" class="glyphicon
              glyphicon-file btn btn-primary btn-sm" aria-hidden="">Documentos <br>DVD</a>
              @endif
              @endif--}}
            </td>
            <td align="center">
            <!--a href="{{ route('documentos-dvd.delete-dvd', ['genDVDSolicitud' => $idsolicitud,'id' =>$idsolicitud]) }}" class="btn btn-danger btn-remove" aria-hidden="">  <span class="glyphicon glyphicon-trash"></span> Anular</!--a -->

            <a href="#" class="glyphicon glyphicon-trash btn  btn btn-danger  btn-sm" id="{{$idsolicitud}}"  data-remove="true" aria-hidden=""></span></a>
           
  
          </td>
          </tr>
          @endforeach
        </tbody>
      </table>
      @else
      @endif
    </div>
  </div>
</div>
<script>
$(document).on('click','[data-remove]',function(){
               var id= $(this).attr("id");
             
                 swal({
                     title: "¿Eliminar?",
                     text: "¿Está seguro de eliminar esta Solicitud?",
                     type: "warning",
                     showCancelButton: true,
                     confirmButtonColor: "#DD6B55",
                     confirmButtonText: "Si!, Borrarlo!",
                     cancelButtonText: "No, Cancelar!",
                     closeOnConfirm: false,
                     closeOnCancel: false,
                     showLoaderOnConfirm: true
                     },

    function(isConfirm){
         if (isConfirm) {
     
              $.ajax({
               'type':'get',
               'url': '{{route('documentos-dvd.delete-dvd', ['id'=>0])}}',
               'data':{ 'id':id},
               success: function(data){
                //$('#row'+id_boton).remove();
                if(data==1){
                   swal("Eliminación exitosa","Solicitud DVD Eliminado ", "success");
                   location.reload();
                  }
                 }
             });
         }else{
             swal("Cancelado", "No se ha procesado la eliminación", "warning");
        }
        });
       
 });

 </script>
@stop
<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <!--<button type="button" class="close" data-dismiss="modal">&times;</button>-->
        <h4 class="modal-title">ATENCIÓN EXPORTADORES</h4>
      </div>
      <div class="modal-body">
        <img class="img-responsive" src={{ asset('img/dvd.jpg') }}>
      </div>
      <div class="modal-footer">
        <a href="{{route('VentaSolicitud.edit',$idsolicitud)}}" type="button"
          class="btn btn-success btn-block"><b>ENTENDIDO</b></a>
      </div>
    </div>
  </div>
</div>
