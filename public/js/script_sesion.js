 //variables glovales
    var strSeperator = '/';
    var shift=false;
    var crtl=false;
    var alt=false;

$(document).ready(function() {


/*script para refrescar capcha loguin y registro refrescar*/
$(".btn-refresh").click(function(){
    $.ajax({
        type:'GET',
        url:'/refresh_captcha',
        success:function(data) {

            $(".captcha span").html(data.captcha);

        }
    })
})


/*script para calendarios bootstrap*/
$('.input-group.date').datepicker({
    format: "yyyy/mm/dd",//formato de la fecha
    todayBtn: "linked",//al oprimir el boton hoy automaticamente te agrela la fecha actual en el campo
    clearBtn: true,//boton para limpiar el campo
    language: "es",//lenguaje del calendario
    orientation: "bottom left",//ubicacion del calendario alrededor del campo
    keyboardNavigation: true,//ni idea
    //daysOfWeekDisabled: "0,6", //bloquea los dias que se le indiquen
    daysOfWeekHighlighted: "0,6", //resalta los dias que se le indiquen
    autoclose: true, //permite que se cierre el calendario al seleccionar la fecha
    todayHighlight: true, //resalta la fecha de hoy
    beforeShowMonth: function (date){
                  if (date.getMonth() == 8) {
                    return false;
                  }
                },
    //defaultViewDate: { year: 1977, month: 04, day: 25 }
});

//script sector y actividad economica
var sector=$("#id_gen_sector").change(function(event) {
        $.get('/ActividadEco/'+event.target.value+'',function(response, sector){
            //console.log(response);
            $("#id_gen_actividad_eco").empty();
            for (var i =0; i < response.length; i++) {
                $("#id_gen_actividad_eco").append("<option value='"+response[i].id+"'>"+response[i].dactividad+"</option>");
            }
        });
    });


//Script para combo estado municipio parroquia

    var estado=$("#id_estado").change(function(event) {
        $.get('/minicipios/'+event.target.value+'',function(response, estado){
            //console.log(response);
            $("#id_municipio").empty();
            for (var i =0; i < response.length; i++) {
                if(response.length == 1){
                    $("#id_municipio").append("<option value=''> --Seleccione una Opción--</option");
                    //$("#id_municipio").append("<option value='"+response[i].id+"'>"+response[i].municipio+"</option>");

                }
                $("#id_municipio").append("<option value='"+response[i].id+"'>"+response[i].municipio+"</option>");
            }
        });
    });

    var municipio=$("#id_municipio").change(function(event) {
        $.get('/parroquias/'+event.target.value+'',function(response, municipio){
            //console.log(response);
            $("#id_parroquia").empty();
            for (var i =0; i < response.length; i++) {
                if(response.length == 1){
                    $("#id_parroquia").append("<option value=''> --Seleccione una Opción--</option");
                    //$("#id_parroquia").append("<option value='"+response[i].id+"'>"+response[i].municipio+"</option>");

                }
                $("#id_parroquia").append("<option value='"+response[i].id+"'>"+response[i].parroquia+"</option>");
            }
        });
    });





} );


function soloLetras(e){
   key = e.keyCode || e.which;
   tecla = String.fromCharCode(key).toLowerCase();
   letras = " áéíóúabcdefghijklmnñopqrstuvwxyz";
   especiales = "8-37-39-46";

   tecla_especial = false
   for(var i in especiales){
        if(key == especiales[i]){
            tecla_especial = true;
            break;
        }
    }

    if(letras.indexOf(tecla)==-1 && !tecla_especial){
        return false;
    }
}

function soloLetrasyPunto(e){
   key = e.keyCode || e.which;
   tecla = String.fromCharCode(key).toLowerCase();
   letras = " áéíóúabcdefghijklmnñopqrstuvwxyz.";
   especiales = "8-37-39-46";

   tecla_especial = false
   for(var i in especiales){
        if(key == especiales[i]){
            tecla_especial = true;
            break;
        }
    }

    if(letras.indexOf(tecla)==-1 && !tecla_especial){
        return false;
    }
}

//funcion para cambio de pregunta de seguridad por pregunta alternativa
function preguntaSeg (){
    var pregunta=$('#id_cat_preguntas_seg').val()
    //alert(pregunta);
    if (pregunta == 8) {
        $('#lista_pregunta').hide('slow');
        $('#preg_alternativa').show('show');
    }

    $('#lista_preg').click(function(event) {
        $('#preg_alternativa').hide('slow');
        $('#lista_pregunta').show('show');
    });

}

//funcion para que todos los correos se guarden en minuscula
function minuscula(cadena, selector){
    var result;
    $(selector).val(cadena.toLowerCase());
}

function enviar() {

   // alert('uyutyutyutyuty');
    /*inicio validacion para el formato de la fecha
    var fTemp = $("#fregistro_mer").val();
    var fcampo = fTemp.split("/")
    var result = fcampo[2].substr(2, 4)
        $("#fregistro_mer").val(fcampo[0]+"/"+fcampo[1]+"/"+result);
    /*fin validacion para el formato de la fecha*/

var mens=[
    'Estimado usuario. Debe ingresar NOMBRE O RAZÓN SOCIAL En el Paso 1 para completar la inscripción',//0
    'Estimado usuario. Debe ingresar SIGLAS  En el Paso 1 para completar la inscripción',//1
    'Estimado usuario. Debe ingresar RIF En el Paso 1 para completar la inscripción',//2
    'Estimado usuario. Debe seleccionar TIPO DE EMPRESA En el Paso 1 para completar la inscripción',//3
    'Estimado usuario. Debe seleccionar Si es EXPORTADOR, INVERSIONISTA, PRODUCTOR, COMERCIALIZADORA En el Paso 1 para completar la inscripción',//4
    'Estimado usuario. Debe ingresar un SECTOR En el Paso 1  para completar la inscripción',//5
    'Estimado usuario. Debe seleccionar ACTIVIDAD ECONÓMICA En el Paso 1  para completar la inscripción',//6
    'Estimado usuario. Debe seleccionar ESTADO En el Paso 1  para completar la inscripción',//7
    'Estimado usuario. Debe seleccionar MUNICIPIO  En el Paso 1 para completar la inscripción',//8
    'Estimado usuario. Debe seleccionar PARROQUIA En el Paso 1  para completar la inscripción',//9
    'Estimado usuario. Debe ingresar una DIRECCIÓN En el Paso 1 para completar la inscripción',//10
    'Estimado usuario. Debe ingresar TELÉFONO LOCAL En el Paso 1  para completar la inscripción',//11
    'Estimado usuario. Debe ingresar TELÉFONO CELULAR En el Paso 1  para completar la inscripción',//12

    'Estimado usuario. Debe ingresar REGISTRO DE CIRCUNSCRIPCIÓN JUDICIAL En el Paso 2 para completar la inscripción',//13
    'Estimado usuario. Debe ingresar NÚMERO DE DOCUMENTO En el Paso 2 para completar la inscripción',//14
    'Estimado usuario. Debe ingresar  NÚMERO DE TOMO En el Paso 2 para completar la inscripción',//15
    'Estimado usuario. Debe ingresar NÚMERO DE FOLIO En el Paso 2 para completar la inscripción',//16
    'Estimado usuario. Debe ingresar OFICINA En el Paso 2 para completar la inscripción',//17
    'Estimado usuario. Debe seleccionar FECHA DE CONSTITUCIÓN En el Paso 2 para completar la inscripción',//18
    'Estimado usuario. Debe ingresar NOMBRE REPRESENTANTE LEGAL En el Paso 2 para completar la inscripción',//19
    'Estimado usuario. Debe ingresar APELLIDO REPRESENTANTE LEGAL En el Paso 2 para completar la inscripción',//20
    'Estimado usuario. Debe ingresar CÉDULA REPRESENTANTE LEGAL En el Paso 2 para completar la inscripción',//21
    'Estimado usuario. Debe ingresar CARGO REPRESENTANTE LEGAL En el Paso 2 para completar la inscripción',//22
    'Estimado usuario. Debe ingresar CORREO REPRESENTANTE LEGAL En el Paso 2 para completar la inscripción',//23

    'Estimado usuario. Debe Seleccionar Si Posee o No Accionista En el Paso 2 para completar la inscripción',//24
    'Estimado usuario. Debe ingresar NOMBRE DEL ACCIONISTA  En el Paso 2 para completar la inscripción',//25
    'Estimado usuario. Debe ingresar APELLIDO DEL ACCIONISTA  En el Paso 2 para completar la inscripción',//26
    'Estimado usuario. Debe ingresar CÉDULA DEL ACCIONISTA En el Paso 2 para completar la inscripción',//27
    'Estimado usuario. Debe ingresar CARGO DEL ACCIONISTA  En el Paso 2 Paso para completar la inscripción',//28
    'Estimado usuario. Debe ingresar TELÉFONO DEL ACCIONISTA  En el Paso 2 Paso para completar la inscripción',//29
    'Estimado usuario. Debe ingresar NACIONALIDAD DEL ACCIONISTA  En el Paso 2 Paso para completar la inscripción',//30
    'Estimado usuario. Debe ingresar PARTICIPACIÓN DEL ACCIONISTA  En el Paso 2 Paso para completar la inscripción',//31
    'Estimado usuario. Debe ingresar CORREO DEL ACCIONISTA  En el Paso 2 Paso para completar la inscripción',//32

    'Estimado usuario. Debe Seleccionar Si Posée o No CASA MATRIZ En el Paso 3 Paso para completar la inscripción',//33
    'Estimado usuario. Debe Seleccionar un PAÍS DE ORIGEN En el Paso 3 Paso para completar la inscripción',//34

    'Estimado usuario. Debe ingresar Especifique Productos de Tecnología En el Paso 4 Paso para completar la inscripción',//35
    'Estimado usuario. Debe Seleccionar un CÓDIGO ARANCELARIO NANDINA O MERCOSUR En el Paso 4 para completar la inscripción',//36
    'Estimado usuario. Debe ingresar CÓDIGO ARANCELARIO En el Paso 4 para completar la inscripción',////37
    'Estimado usuario. Debe ingresar una DESCRIPCIÓN ARANCELARIA Paso En el Paso 4  para completar la inscripción',//38
    'Estimado usuario. Debe ingresar una DESCRIPCIÓN COMERCIAL En el Paso 4 para completar la inscripción',//39
    'Estimado usuario. Debe ingresar una UNIDAD DE MEDIDA En el Paso 4 para completar la inscripción',//40
    'Estimado usuario. Debe ingresar la CAPACIDAD OPERATIVA ANUAL En el En el Paso 4 para completar la inscripción',//41
    'Estimado usuario. Debe ingresar la CAPACIDAD INSTALADA ANUAL En el Paso 4 para completar la inscripción',//42
    'Estimado usuario. Debe ingresar la CAPACIDAD DE ALMACENAMIENTO ANUAL En el Paso 4  para completar la inscripción',//43
    'Estimado usuario. Debe ingresar la CAPACIDAD DE EXPORTACIÓN En el Paso 4  para completar la inscripción',//44

    'Estimado usuario. Debe ingresar CORREO ELECTRÓNICO En el Paso 5 para completar la inscripción',//45
    'Estimado usuario. Debe ingresar CORREO ELECTRÓNICO ALTERNATIVO En el Paso 5 para completar la inscripción',//46
    'Estimado usuario. Debe ingresar CONTRASEÑA En el Paso 5 para completar la inscripción',//47
    'Estimado usuario. Debe ingresar CONFIRMACIÓN DE CONTRASEÑA En el Paso 5 para completar la inscripción',//48
    'Estimado usuario. Debe ingresar UNA PREGUNTA DE SEGURIDAD En el Paso 5  para completar la inscripción',//49
    'Estimado usuario. Debe ingresar UNA RESPUESTA DE SEGURIDAD En el Paso 5 para completar la inscripción',//50

    'Estimado usuario. Debe ACEPTAR LA DECLARACION ANTES EXPUESTAS para completar la inscripción',//51
]

function validarForm(input, mensaje,num) {
    if ($("#"+input).val()=="") {
        swal("Por Favor!", mensaje, "warning")
        //swal(mensaje, "info");
        //alert(mensaje)
        $("#"+input).focus();
        key=1;
        $('#ok'+num).remove()
        $("#"+input).css('border', '1px solid red').after('<span id="ok'+num+'" style="color:red" class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span>');
        return 1;
    }else{
        $('#ok'+num).remove()
        $("#"+input).css('border', '1px solid green').after('<span id="ok'+num+'" style="color:green" class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span>');
        return 0;
    }
}

function validarradio(input, mensaje,num){
    if ( ($("#"+input).is(':checked')) || ($("#invers").is(':checked')) || ($("#produc").is(':checked')) || ($("#comerc").is(':checked')) ) {
        $('#ok'+num).remove()
        $("#"+input).css('border', '1px solid green').after('<span id="ok'+num+'" style="color:green" class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span>');
        return 0;

    }else{
        swal("Por Favor!", mensaje, "warning")
        $("#"+input).focus();
        key=1;
        $('#ok'+num).remove()
        $("#"+input).css('border', '1px solid red').after('<span id="ok'+num+'" style="color:red" class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span>');
        return 1;
    }
}

    var key=0;
    /*****************Parte 1 Del Registro**********/
    key = validarForm('razon_social', mens[0],0)//
    if(key == 1){return false}
    key = validarForm('siglas', mens[1],1)//
    if(key == 1){return false}
    key = validarForm('rif', mens[2],2)///
    if(key == 1){return false}
    key = validarForm('id_tipo_empresa', mens[3],3)//
    if(key == 1){return false}
    key = validarradio('export', mens[4],4)//
    if(key == 1){return false}
    key = validarForm('id_gen_sector', mens[5],5)//
    if(key == 1){return false}
    key = validarForm('id_gen_actividad_eco', mens[6],6)//
    if(key == 1){return false}
    key = validarForm('id_estado', mens[7],7)//
    if(key == 1){return false}
    key = validarForm('id_municipio', mens[8],8)//
    if(key == 1){return false}
    key = validarForm('id_parroquia', mens[9],9)//
    if(key == 1){return false}
    key = validarForm('direccion', mens[10],10)//
    if(key == 1){return false}
    key = validarForm('telefono_local', mens[11],11)//
    if(key == 1){return false}

/*****************Parte 2 Del Registro**********/
    key = validarForm('id_circuns_judicial', mens[13],13)//
    if(key == 1){return false}
    key = validarForm('num_registro', mens[14],14)//
    if(key == 1){return false}
    key = validarForm('tomo', mens[16],16)//
    if(key == 1){return false}
    key = validarForm('folio', mens[16],16)//
    if(key == 1){return false}
    key = validarForm('Oficina', mens[17],17)//
    if(key == 1){return false}
    key = validarForm('f_registro_mer', mens[18],18)//
    if(key == 1){return false}
    key = validarForm('nombre_repre', mens[19],19)//
    if(key == 1){return false}
    key = validarForm('apellido_repre', mens[20],20)//
    if(key == 1){return false}
    key = validarForm('ci_repre', mens[21],21)//
    if(key == 1){return false}
    key = validarForm('cargo_repre', mens[22],22)//
    if(key == 1){return false}
    key = validarForm('correo_repre', mens[23],23)//
    if(key == 1){return false}

       if ($("#accion_si").is(':checked')) {
         key = validarForm('nombre_accionista', mens[25],25)//
        if(key == 1){return false}
         key = validarForm('apellido_accionista', mens[26],26)//
        if(key == 1){return false}
         key = validarForm('cedula_accionista', mens[27],27)//
        if(key == 1){return false}
        key = validarForm('cargo_accionista', mens[28],28)//
        if(key == 1){return false}
        key = validarForm('telefono_accionista', mens[29],29)//
        if(key == 1){return false}
        key = validarForm('nacionalidad_accionista', mens[30],30)//
        if(key == 1){return false}
        key = validarForm('participacion_accionista', mens[31],31)//
        if(key == 1){return false}
        key = validarForm('correo_accionista', mens[32],32)//
        if(key == 1){return false}
        }
/*****************Parte 3 Del Registro**********/
        if ($("#casa_matriz_si").is(':checked')) {
          key = validarForm('pais_id', mens[34],34)//
        if(key == 1){return false}   
        }
/*****************Parte 4 Del Registro**********/
if ($("#prod_tecnologia_si").is(':checked') || $("#prod_tecnologia_no").is(':checked')) {

}else{
    swal("Por Favor!", "Estimado Usuario. Debe Seleccionar si Exporta o no Productos de Servicios y/o Tecnología En el Paso 4 para completar la inscripción", "warning")
        $("#prod_tecnologia_no").focus();
        key=1;
        $('#ok'+'33').remove()
        $("#prod_tecnologia_no").css('border', '1px solid red').after('<span id="ok'+'33'+'" style="color:red" class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span>');
        return 1;
}

if ($("#prod_tecnologia_si").is(':checked')){
    key = validarForm('especifique_tecno', mens[35],35)
    if(key == 1){return false}
}

if ($("#prod_tecnologia_no").is(':checked')) {
    if ($("#cod_mercosur").is(':checked') || $("#cod_andina").is(':checked')) {
        key = validarForm('cod_arancel', mens[37],37)
        if(key == 1){return false}
        key = validarForm('descrip_arancel', mens[38],38)
        if(key == 1){return false}
        key = validarForm('descrip_comercial', mens[39],39)
        if(key == 1){return false}
        key = validarForm('gen_unidad_medida_id', mens[40],40)
        if(key == 1){return false}
        key = validarForm('cap_opera_anual', mens[41],41)
        if(key == 1){return false}
        key = validarForm('cap_insta_anual', mens[42],42)
        if(key == 1){return false}
        key = validarForm('cap_alamcenamiento', mens[43],43)
        if(key == 1){return false}
        key = validarForm('cap_exportacion', mens[44],44)
        if(key == 1){return false}
    }else{
        key = validarradio('cod_mercosur', mens[36],36)
        if(key == 1){return false}
    }
}
/*****************Parte 5 Del Registro**********/
    key = validarForm('email', mens[45],45)
    if(key == 1){return false}
    key = validarForm('email_seg', mens[46],46)
    if(key == 1){return false}
    key = validarForm('password', mens[47],47)
    if(key == 1){return false}
    key = validarForm('password_confirm', mens[48],48)
    if(key == 1){return false}
     key = validarForm('id_cat_preguntas_seg', mens[49],49)
    if(key == 1){return false}
    key = validarForm('respuesta_seg', mens[50],50)
    if(key == 1){return false}

    key=validarCorreo($('#email').val(),$('#email_seg').val())
    if(key == 1)
    {
       return false;
    }

    key=validarclave($('#password').val(),$('#password_confirm').val())
    if(key == 1)
    {
       return false;
    }

    key = validarradio('condicion', mens[51],51)//
    if(key == 1){return false}

    if (key==0) {
        document.forms["inscripcion"].action="/registroUsr";
        document.forms["inscripcion"].submit();
    }
}
/******************** VALIDACION FORMULARIO DE PERSONA NATURAL ***************/

function enviarNatural() {
    /*inicio validacion para el formato de la fecha
    var fTemp = $("#fregistro_mer").val();
    var fcampo = fTemp.split("/")
    var result = fcampo[2].substr(2, 4)
        $("#fregistro_mer").val(fcampo[0]+"/"+fcampo[1]+"/"+result);
    /*fin validacion para el formato de la fecha*/

var mens=[
    'Estimado usuario. Debe ingresar NOMBRE O RAZÓN SOCIAL para completar la inscripción',//0
    'Estimado usuario. Debe ingresar RIF para completar la inscripción',//1
    'Estimado usuario. Debe seleccionar Si es EXPORTADOR, INVERSIONISTA, PRODUCTOR, COMERCIALIZADORA  para completar la inscripción',//2
    'Estimado usuario. Debe ingresar un SECTOR   para completar la inscripción',//3
    'Estimado usuario. Debe seleccionar ACTIVIDAD ECONÓMICA  para completar la inscripción',//4
    'Estimado usuario. Debe seleccionar ESTADO  para completar la inscripción',//5
    'Estimado usuario. Debe seleccionar MUNICIPIO para completar la inscripción',//6
    'Estimado usuario. Debe seleccionar PARROQUIA  para completar la inscripción',//7
    'Estimado usuario. Debe ingresar una DIRECCIÓN  para completar la inscripción',//8
    'Estimado usuario. Debe ingresar TELÉFONO LOCAL   para completar la inscripción',//9
    'Estimado usuario. Debe ingresar TELÉFONO CELULAR  para completar la inscripción',//10

    'Estimado usuario. Debe Seleccionar si Exporta o no Productos de Tecnologia Paso para completar la inscripción',//11
    'Estimado usuario. Debe Seleccionar un CÓDIGO ARANCELARIO NANDINA O MERCOSUR En el Paso 3 para completar la inscripción',//12
    
    'Estimado usuario. Debe ingresar CÓDIGO ARANCELARIO  para completar la inscripción',////13
    'Estimado usuario. Debe ingresar una DESCRIPCIÓN ARANCELARIA  para completar la inscripción',//14
    'Estimado usuario. Debe ingresar una DESCRIPCIÓN COMERCIAL para completar la inscripción',//15
    'Estimado usuario. Debe ingresar una UNIDAD DE MEDIDA  para completar la inscripción',//16
    'Estimado usuario. Debe ingresar la CAPACIDAD OPERATIVA ANUAL  para completar la inscripción',//17
    'Estimado usuario. Debe ingresar la CAPACIDAD INSTALADA ANUAL  para completar la inscripción',//18
    'Estimado usuario. Debe ingresar la CAPACIDAD DE ALMACENAMIENTO ANUAL  para completar la inscripción',//19
    'Estimado usuario. Debe ingresar la CAPACIDAD DE EXPORTACIÓN  para completar la inscripción',//20
    


    'Estimado usuario. Debe ingresar CORREO ELECTRÓNICO para completar la inscripción',//21
    'Estimado usuario. Debe ingresar CORREO ELECTRÓNICO SECUNDARIO  para completar la inscripción',//22
    'Estimado usuario. Debe ingresar PASSWORD para completar la inscripción',//23
    'Estimado usuario. Debe ingresar CONFIRMACIÓN DE PASSWORD para completar la inscripción',//24
    'Estimado usuario. Debe ingresar UNA PREGUNTA DE SEGURIDAD  para completar la inscripción',//25
    'Estimado usuario. Debe ingresar UNA RESPUESTA DE SEGURIDAD  para completar la inscripción',//26
    'Estimado usuario. Debe ingresar LA ESPECIFICACIÓN DE LA PRODUCCIÓN DE SERVICIOS Y/O TECNLOGÍA  para completar la inscripción',//27



    'Estimado usuario. Debe ACEPTAR LAS CONDICIONES ANTES EXPUESTAS para completar la inscripción',//28
]

function validarForm_pnatural(input, mensaje,num) {



    if ($("#"+input).val()=="") {
      //  console.log($("#"+input).val());
        swal("Por Favor!", mensaje, "warning")
        //swal(mensaje, "info");
        //alert(mensaje)
        $("#"+input).focus();
        key=1;
        $('#ok'+num).remove()
        $("#"+input).css('border', '1px solid red').after('<span id="ok'+num+'" style="color:red" class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span>');
        return 1;
    }else{
        $('#ok'+num).remove()
        $("#"+input).css('border', '1px solid green').after('<span id="ok'+num+'" style="color:green" class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span>');
        return 0;
    }

}


function validarradio(input, mensaje,num){


  if ( ($("#"+input).is(':checked')) || ($("#invers").is(':checked')) || ($("#produc").is(':checked')) || ($("#comerc").is(':checked')) ) {


        $('#ok'+num).remove()
        $("#"+input).css('border', '1px solid green').after('<span id="ok'+num+'" style="color:green" class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span>');
        return 0;

    }else{

        swal("Por Favor!", mensaje, "warning")
        $("#"+input).focus();
        key=1;
        $('#ok'+num).remove()
        $("#"+input).css('border', '1px solid red').after('<span id="ok'+num+'" style="color:red" class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span>');
        return 1;
    }
}


function validarCondicion(input,mensaje,num){

  if ( $("#"+input).is(':checked') ) {


        $('#ok'+num).remove()
        $("#"+input).css('border', '1px solid green').after('<span id="ok'+num+'" style="color:green" class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span>');
        return 0;

    }else{

        swal("Por Favor!", mensaje, "warning")
        $("#"+input).focus();
        key=1;
        $('#ok'+num).remove()
        $("#"+input).css('border', '1px solid red').after('<span id="ok'+num+'" style="color:red" class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span>');
        return 1;
    }

}

 function validarRadio(input,mensaje,num){


   var radio_expor_prod="";


   $('input[name='+input+']').each(function(index,value){

        if(value.checked) { radio_expor_prod=1; }
   });

  //console.log(almacen);
  if(radio_expor_prod==""){

    swal("Por Favor!", mensaje, "warning")
    //swal(mensaje, "info");

    $("#"+input).focus();
    key=1;
    $('#ok'+num).remove();
    $("#"+input).css('border', '1px solid red').after('<span id="ok'+num+'" style="color:red" class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span>');

    return 1;

    }else{


        $('#ok'+num).remove()
        $("#"+input).css('border', '1px solid green').after('<span id="ok'+num+'" style="color:green" class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span>');
        return 0;

       }

 }

    var key=0;

    key = validarForm_pnatural('razon_social', mens[0],0)//
    if(key == 1){return false}
    key = validarForm_pnatural('rif', mens[1],1)///
    if(key == 1){return false}
        key = validarradio('export', mens[2],2)//
    if(key == 1){return false}
    key = validarForm_pnatural('id_gen_sector', mens[3],3)//
    if(key == 1){return false}
    key = validarForm_pnatural('id_gen_actividad_eco', mens[4],4)//
    if(key == 1){return false}
    key = validarForm_pnatural('id_estado', mens[5],5)//
    if(key == 1){return false}
    key = validarForm_pnatural('id_municipio', mens[6],6)//
    if(key == 1){return false}
    key = validarForm_pnatural('id_parroquia', mens[7],7)//
    if(key == 1){return false}
    key = validarForm_pnatural('direccion', mens[8],8)//
    if(key == 1){return false}
    key = validarForm_pnatural('telefono_local', mens[9],9)//
    if(key == 1){return false}
    key = validarForm_pnatural('telefono_movil', mens[10],10)//
    if(key == 1){return false}

    /*key = validarRadio('prod_tecnologia_si', mens[27],27)//
    if(key == 1){return false}*/

    key = validarForm_pnatural('email', mens[21],21)
    if(key == 1){return false}
    key = validarForm_pnatural('email_seg', mens[22],22)
    if(key == 1){return false}
    key = validarForm_pnatural('password', mens[23],23)
    if(key == 1){return false}
    key = validarForm_pnatural('password_confirm', mens[24],24)
    if(key == 1){return false}
     key = validarForm_pnatural('id_cat_preguntas_seg', mens[25],25)
    if(key == 1){return false}
    key = validarForm_pnatural('respuesta_seg', mens[26],26)
    if(key == 1){return false}

    /*if ($("#prod_tecnologia_si").is(':checked')){
    key = validarForm_pnatural('especifique_tecno', mens[11],11)
    if(key == 1){return false}
    }*/

    key=validarCorreo($('#email').val(),$('#email_seg').val())
    if(key == 1)
    {
       return false;

    }

    key=validarclave($('#password').val(),$('#password_confirm').val())
    if(key == 1)
    {
       return false;

    }



    k=2;

    if(k==2){

      var v=document.getElementById('guardar_personal');
       v.setAttribute('onclick','');
       v.setAttribute('data-toggle','modal');
       v.setAttribute('data-target','#myModal');

      }


     key = validarCondicion('condicion', mens[28],28)
     if(key == 1){return false}



    if (key==0) {


      document.forms["formNatural"].action="/ProcesarNatural";
      document.forms["formNatural"].submit();
    }



}

/******************************************************************************/


function enviarLogin() {

var mens=[
    'Estimado usuario. Debe seleccionar un tipo de usuario para completar el registro',
    'Estimado usuario. Debe ingresar el Rif para completar el registro',
    'Estimado usuario. Debe ingresar el Captcha para completar el registro',
    'Estimado usuario. Debe seleccionar una opción para completar el registro',
]

function validarformLogin(input, mensaje,num) {

  if( ($("#"+input)[0].type=="text") || ($("#"+input)[0].type=="select-one")){


    if ($("#"+input).val()=="") {

        swal("Por Favor!", mensaje, "warning")
        //swal(mensaje, "info");
        //alert(mensaje)
        $("#"+input).focus();
        key=1;
        $('#ok'+num).remove()
        $("#"+input).css('border', '1px solid red').after('<span id="ok'+num+'" style="color:red" class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span>');
        return 1;
    }else{
        $('#ok'+num).remove()
        $("#"+input).css('border', '1px solid green').after('<span id="ok'+num+'" style="color:green" class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span>');
        return 0;
    }


}else{


  var radio="";
  $('input[name='+input+']').each(function(index,value){

       if(value.checked) { radio=1; }
  });


 if(radio==""){

   swal("Por Favor!", mensaje, "warning")
   //swal(mensaje, "info");

   $("#"+input).focus();
   key=1;
   $('#ok'+num).remove();
   $("#"+input).css('border', '1px solid red').after('<span id="ok'+num+'" style="color:red" class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span>');

   return 1;

   }else{



       $('#ok'+num).remove()
       $("#"+input).css('border', '1px solid green').after('<span id="ok'+num+'" style="color:green" class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span>');
       return 0;


   }


}

}

 var key=0;
$("#formLogin").submit(function() {
    key = validarformLogin('tipoUsu', mens[0],0)
    if(key == 1){return false}
    key = validarformLogin('select_tipoPersona', mens[3],3)
    if(key == 1){return false}
    key = validarformLogin('rif', mens[1],1)
    if(key == 1){return false}
    key = validarformLogin('captcha', mens[2],2)
    if(key == 1){return false}

    if (key==0) {
        return true;
    }
  });
}

/****************************************************************************/
function validarfechaRM(fechaCalendario){
var  msg= "La fecha ingresada no puede ser mayor a la fecha actual";
var f = new Date();
var fAhora = f.getFullYear() + "/" + (f.getMonth()+1)+ "/" +f.getDate();
//var fechaCalendario = $('#f_registro_mer').val();
var  x=true;
if((Date.parse(fechaCalendario)) > (Date.parse(fAhora))){
        swal("Por Favor!", msg, "warning")
         x=false;
         $("#f_registro_mer").val('');

}
if(x==true){

       $("#f_registro_mer").css('border', '1px solid green').after('<span id="ok20" style="color:green" class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span>');
    }else{
        $("#f_registro_mer").focus();
         $("#f_registro_mer").css('border', '1px solid red').after('<span id="ok20" style="color:red" class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span>');
    }
}

function validarclave(clave1,clave2){
    var  msg= "El Campo Contraseñaaaaa debe tener minimo 6 caracteres";
    var  msj2="El Campo Contraseña no puede estar Vacio";
    var  msj3="El Campo Confirmación de Contraseña tiene que ser igual al Campo Contraseña";
    var clave1 = document.getElementById('password').value;
    var clave2 = document.getElementById('password_confirm').value;
    var  x=true;


    if (clave1.length< 6) {
        swal("Por Favor!", msg, "warning")
        //swal(msj2, "info");
         //alert(msj2);
         x=false;
         return 1;
    }

    if (clave2 != clave1) {
        swal("Por Favor!", msj3, "warning")
        //swal(msj2, "info");
         //alert(msj2);
        x=false;
        return 1;
    }

    if(x==true){

       $("#password").css('border', '1px solid green').after('<span id="ok20" style="color:green" class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span>');
       $("#password_confirm").css('border', '1px solid green').after('<span id="ok21" style="color:green" class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span>');
       return 0;
    }
}

function validarCorreo(email,email1){

    expr = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    var  msg= "El Campo esta Vacio...Debe Ingresar un Email ";
    var  msj2="La Direccion de Email Ingresada es Incorrecta";
    var  msj3="La Dirección de Email Secundario no debe ser igual al Email Principal";
    var email = document.getElementById('email').value;
    var email_seg = document.getElementById('email_seg').value;
    var  x=true;


      if( ( (!expr.test(email)) && (email!='') )) {


        swal("Por Favor!", msj2, "warning")
        //swal(msj2, "info");
         //alert(msj2);
         x=false;
        $("#email").focus();

        $('#ok1').remove()
        $("#email").css('border', '1px solid red').after('<span id="ok1" style="color:red" class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span>');
         return 1;





        }

      if ( (!expr.test(email1)) && (email1!='') ) {

        swal("Por Favor!", msj2, "warning")
        //swal(msj2, "info");
         //alert(msj2);
         x=false;
        $("#email_seg").focus();

        $('#ok2').remove()
        $("#email_seg").css('border', '1px solid red').after('<span id="ok2" style="color:red" class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span>');
         return 1;
      }

      if( email=='' ) {
        swal("Por Favor!", msg, "warning")

        //swal(msg, "info");
        //alert(msg);
        x=false;
        return 1;
        }


       if(email==email1 ) {
        swal("Por Favor!", msj3, "warning")
        //swal(msj3, "info");
       //alert(msj3);
       x=false;
       $("#email_seg").focus();

        $('#ok2').remove()
        $("#email_seg").css('border', '1px solid red').after('<span id="ok2" style="color:red" class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span>');
         return 1;

       }


       if(x==true){

       $("#email").css('border', '1px solid green').after('<span id="ok20" style="color:green" class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span>');
       $("#email_seg").css('border', '1px solid green').after('<span id="ok21" style="color:green" class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span>');
       return 0;

       }

  }

function soloNumerosyLetras(e){
   key = e.keyCode || e.which;
   //alert(key)
   tecla = String.fromCharCode(key).toLowerCase();
   letras = " -áéíóúabcdefghijklmnñopqrstuvwxyz0123456789.";
   especiales = "8-37-39";

   tecla_especial = false
   for(var i in especiales){
        if(key == especiales[i]){
            tecla_especial = true;
            break;
        }
    }
    //alert(letras.indexOf(tecla))
    if(letras.indexOf(tecla)==-1 && !tecla_especial){
        return false;
    }
}

/**********************************VALIDACION PARA LOS CAMPOS CON PORCENTAJE***********************/

function soloNumerosletrasyPorcentaje(e){
   key = e.keyCode || e.which;
   //alert(key)
   tecla = String.fromCharCode(key).toLowerCase();
   letras = " %0123456789.";
   especiales = "8-39";

   tecla_especial = false
   for(var i in especiales){
        if(key == especiales[i]){
            tecla_especial = true;
            break;
        }
    }
    //alert(letras.indexOf(tecla))
    if(letras.indexOf(tecla)==-1 && !tecla_especial){
        return false;
    }
}

/**************************************************************************************************/

function soloNumeros(e){
   key = e.keyCode || e.which;
   tecla = String.fromCharCode(key).toLowerCase();
   letras = "0123456789";
   especiales = "8-37-39-46";

   tecla_especial = false
   for(var i in especiales){
        if(key == especiales[i]){
            tecla_especial = true;
            break;
        }
    }

    if(letras.indexOf(tecla)==-1 && !tecla_especial){
        return false;
    }
}



/*FIN********************************validaciones ranses 01-08-2016**********************************************/
function CedulaFormat1(vCedulaName,mensaje,postab,escribo,evento) {

  //  var tipoPerson=document.getElementsByName('tipo_usuario');
    tecla=getkey(evento);
    vCedulaName.value=vCedulaName.value.toUpperCase();
    vCedulaValue=vCedulaName.value;
    valor=vCedulaValue.substring(2,12);
    var numeros='0123456789/';
    var digit;
    var noerror=true;
    if (shift && tam>1) {
    return false;
    }
    for (var s=0;s<valor.length;s++){
    digit=valor.substr(s,1);
    if (numeros.indexOf(digit)<0) {
    noerror=false;
    break;
    }
    }
    tam=vCedulaValue.length;
    if (escribo) {
    if ( tecla==8 || tecla==37) {
    if (tam>2)
    vCedulaName.value=vCedulaValue.substr(0,tam-1);
    else
    vCedulaName.value='';
    return false;
    }
    if (tam==0 && tecla==69) {
    vCedulaName.value='E-';
    return false;
    }
    if (tam==0 && tecla==69) {
    //vCedulaName.value='E-';
    return false;
    }


    if (tam==0 && tecla==86) {
    vCedulaName.value='V-';
    return false;
    }


        if (tam==0 && tecla==74) {
    vCedulaName.value='J-';
    return false;
    }
    if (tam==0 && tecla==71) {
    vCedulaName.value='G-';
    return false;
    }
    if (tam==0 && tecla==80) {
    vCedulaName.value='P-';
    return false;
    }

    else if ((tam==0 && ! (tecla<14 || tecla==69 || tecla==86 || tecla==46)))
    return false;
    else if ((tam>1) && !(tecla<14 || tecla==16 || tecla==46 || tecla==8 || (tecla >= 48 && tecla <= 57) || (tecla>=96 && tecla<=105)))
    return false;
    }

}
/****************************validacion cargo representante legal 02/08/2016*************************************************/
function CedulaFormatrep(vCedulaName,mensaje,postab,escribo,evento) {
    tecla=getkey(evento);
    vCedulaName.value=vCedulaName.value.toUpperCase();
    vCedulaValue=vCedulaName.value;
    valor=vCedulaValue.substring(2,12);
    var numeros='0123456789/';
    var digit;
    var noerror=true;
    if (shift && tam>1) {
    return false;
    }
    for (var s=0;s<valor.length;s++){
    digit=valor.substr(s,1);
    if (numeros.indexOf(digit)<0) {
    noerror=false;
    break;
    }
    }
    tam=vCedulaValue.length;
    if (escribo) {
    if ( tecla==8 || tecla==37) {
    if (tam>2)
    vCedulaName.value=vCedulaValue.substr(0,tam-1);
    else
    vCedulaName.value='';
    return false;
    }


    if (tam==0 && tecla==86) {
    vCedulaName.value='V-';
    return false;
    }

    if (tam==0 && tecla==80) {
    vCedulaName.value='P-';
    return false;
    }

    if (tam==0 && tecla==69) {
    vCedulaName.value='E-';
    return false;
    }
    else if ((tam==0 && ! (tecla<14  || tecla==86 || tecla==46 || tecla==69)))
    return false;
    else if ((tam>1) && !(tecla<14 || tecla==16 || tecla==46 || tecla==8 || (tecla >= 48 && tecla <= 57) || (tecla>=96 && tecla<=105)))
    return false;
    }
}


function getkey(e){
    if (window.event) {
    shift= event.shiftKey;
    ctrl= event.ctrlKey;
    alt=event.altKey;
    return window.event.keyCode;
    }
    else if (e) {
    var valor=e.which;
    if (valor>96 && valor<123) {
    valor=valor-32;
    }
    return valor;
    }
    else
    return null;
}

$('input:radio[name="tipoUsu"]').change( function(){

 if($(this).is(':checked')) {

    if($(this).val()==1) {

     $('#select_tipoPersona').html('<option value="V">V</option><option value="E">E</option><option value="P">P</option>');

  }else{

    $('#select_tipoPersona').html('<option value="J">J</option><option value="G">G</option>');


  }

}



});
function validarPasaporte(){

    let valSelect = $('#select_tipoPersona').val();

    if(valSelect == 'P'){

        $('#rif').removeAttr('onkeypress');
        $('#rif').removeAttr('maxlength');

    }else{
        
        $('#rif').attr('onkeypress', 'return soloNumeros(event)');
        $('#rif').attr('maxlength', '9');
    }
}