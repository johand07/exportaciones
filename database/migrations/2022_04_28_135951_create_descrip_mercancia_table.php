<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDescripMercanciaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('descrip_mercancia', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('cert_exporta_facil_id')->nullable();
            $table->foreign('cert_exporta_facil_id')->references('id')->on('cert_exporta_facil');
            $table->string('serie', 200)->nullable();
            $table->string('sub_partida_arancel', 200)->nullable();
            $table->text('descripcion', 200)->nullable();
            $table->string('valor_fob', 200)->nullable();
            $table->string('cantidad', 200)->nullable();
            $table->date('fecha')->nullable();
            $table->string('regimen', 200)->nullable();
            $table->boolean('bactivo')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('descrip_mercancia');
    }
}
