<div class="modal fade" id="modal" role="dialog" aria-hidden="true">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
            {{$header}}
            {{Form::hidden('tipo_arancel',null,['id'=>'id_arancel'])}}
            <br>
            <div class="form-group" align="right">
            {!! Form::label('Buscar')!!}
            {!! Form::text('buscador',null,['id'=>'buscador_arancel'])!!}
            </div>


        </div>
      
        <div class="modal-body" id="arancel" style="overflow-y:scroll;height:200px">


        {{$body}}

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-info" data-dismiss="modal" style="display:none">

         {{$footer}}

          </button>
        </div>
      </div>

    </div>
  </div>
