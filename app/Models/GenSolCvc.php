<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GenSolCvc extends Model
{
    protected $table = 'gen_sol_cvc';

    protected $guarded = [

    	'gen_usuario_id',
    	'num_sol_cvc',
    	'gen_status_id',
    	'fstatus',
    	'direccion_notif',
    	'clave_pais',
    	'gen_aduana_salida_id',
    	'num_serie',
    	'pais_destino',
    	'pais_productor',
    	'pais_transbordo',
    	'fexportacion',
    	'nombre_transporte',
    	'marca_ident',
    	'cat_cargados_id',
    	'peso_neto',
    	'gen_unidad_medida_id',
    	'cat_tipo_cafe_id',
    	'cat_metodo_elab_id',
    	'norma_calidad_cafe',
    	'caract_especiales',
    	'codio_sist_sa',
    	'valor_fob',
    	'gen_divisa_id',
    	'codigo_arancelario',
    	'info_adicional',
    	'bactivo',
    ];


    protected $date =['created_at','updated_at'];

	    public function rUsuario() {

	        return $this->hasOne('App\Models\GenUsuario','id','gen_usuario_id');

	    }

		public function DetUsuario() {

     	return $this->hasOne('App\Models\DetUsuario','gen_usuario_id','gen_usuario_id'); 
  }


  	public function estatus() {

    	return $this->hasOne('App\Models\GenStatus','id','gen_status_id');

	}
	

  	 public function rAnalistaCvc() {

        return $this->hasOne('App\Models\BandejaAnalistaCvc','gen_sol_cvc_id','id');

    }



         public function GenUnidadMedida() {

        return $this->hasOne('App\Models\GenUnidadMedida','id','gen_unidad_medida_id');

    }


           public function GenAduanaSalida() {

        return $this->hasOne('App\Models\GenAduanaSalida','id','gen_aduana_salida_id');

    }
    
    public function Pais() {

        return $this->hasOne('App\Models\Pais','id','pais_destino');

    }

     public function PaisTransbordo() {

        return $this->hasOne('App\Models\Pais','id','pais_transbordo');

    }
    



    
    public function divisa(){
        return $this->hasOne('App\Models\GenDivisa','id','gen_divisa_id');
    }


     public function unidadMedida() {

     return $this->hasOne('App\Models\GenUnidadMedida','id','gen_unidad_medida_id');
     
    }



     public function paisDestino() {

     return $this->hasOne('App\Models\Pais','id','pais_destino');

    }

    public function paisProductor() {

     return $this->hasOne('App\Models\Pais','id','pais_productor');

    }


     public function paisTrasbordo() {

     return $this->hasOne('App\Models\Pais','id','pais_transbordo');

    }


    public function rAduanaSalida() {

        return $this->hasOne('App\Models\GenAduanaSalida','id','gen_aduana_salida_id');

    }


}
