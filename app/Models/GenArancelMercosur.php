<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GenArancelMercosur extends Model
{
    protected $table = 'gen_arancel_mercosur';

    protected $fillable = [

           'codigo',
           'descripcion',
           'capitulo',
           'bactivo',
    ];

    public function rProductos() {

     return $this->hasOne('App\Models\Productos','codigo','codigo');
     
    }
}
