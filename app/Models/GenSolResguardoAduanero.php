<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GenSolResguardoAduanero extends Model
{
    protected $table = 'gen_sol_resguardo_aduanero';

    protected $fillable = [

           'gen_usuario_id',
           'status_resguardo',
           'fstatus_resguardo',
           'status_cna',
           'fstatus_cna',
           'num_sol_resguardo',
           'aprobada_resguardo',
           'aprobada_cna',
           'edo_observacion_resguardo',
           'observacion_resguardo',
           'edo_observacion_cna',
           'observacion_cna',
           'tlf_1',
           'tlf_2',
           'tlf_3',
           'ubicacion_georeferencial',
           'gen_aduana_salida_id',
           'bactivo',
           
    ];

    protected $date =['created_at','updated_at'];

    public function rUsuario() {

        return $this->hasOne('App\Models\GenUsuario','id','gen_usuario_id');

    }
    
    public function rEstatusResguardo() {

        return $this->hasOne('App\Models\GenStatus','id','status_resguardo');

    }

    public function rEstatusCna() {

        return $this->hasOne('App\Models\GenStatus','id','status_cna');

    }


    public function DetUsuario() {

     return $this->hasOne('App\Models\DetUsuario','gen_usuario_id','gen_usuario_id');

    
  }


    public function rAnalistaResguardo() {

        return $this->hasOne('App\Models\BandejaAnalistaResguardo','gen_sol_resguardo_aduanero_id','id');

    }

    public function rAnalistaCna() {

        return $this->hasOne('App\Models\BandejaAnalistaCna','gen_sol_resguardo_aduanero_id','id');

    }

    public function rAduanaSalida() {

        return $this->hasOne('App\Models\GenAduanaSalida','gen_aduana_salida_id','id');

    }


    
}
