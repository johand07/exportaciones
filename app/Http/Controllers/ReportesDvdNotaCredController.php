<?php

namespace App\Http\Controllers;

use App\Models\GenNotaCredito;
use App\Models\GenUsuario;
use App\Models\DetUsuario;
use Illuminate\Http\Request;
use Session;
use Auth;
use Laracasts\Flash\Flash;
use Alert;
use PDF;
use Illuminate\Routing\Route;
use Illuminate\Http\Response;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\DB;

class ReportesDvdNotaCredController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $titulo='Solicitud de Dvd Nota de Credito';
        $descripcion='Detalle de DVD Nota de Credito';
        $anio=date('Y');
        $desde=$request->desde;
        $hasta=$request->hasta;
        // $desde =  $anio."/01/01";
        // $hasta = date('Y/m/d');
        //dd($format);

       /****Consulta*****/
        $nota_credito=DB::table('det_usuario as det')//
        ->select('dvd.gen_solicitud_id as Num_Solicitud',//
                 'dua.fembarque as Fecha_Embarque',//
                 'sol.fsolicitud as Fecha_DVD',//
                 'tsol.solicitud as tipo_Solicitud',//
                 'fr.descrip_pago as Descripcion_de_Pago',//
                 'det.rif',//
                 'det.razon_social',//
                 'fa.numero_factura as Num_Factura',//
                 'fa.fecha_factura as Fecha_Factura',//
                 'dua.numero_dua as DUA',//
                 'fa.monto_fob as Monto_Fob_Factura',//
                 'dvd.mfob as Monto_Fob',//
                 'ntcr.monto_nota_credito as Monto_Nota_Credito',/**/
                 'ntcr.justificacion as Observacion_nota_Credito',/**/
                 'dvd.mpercibido as Monto_Percibido',//
                 'dvd.mvendido as Monto_Vendido',//
                 'dvd.mretencion as Monto_Retenido',//
                 'dvd.mpendiente as Monto_Pendiente',//
                 'dv.ddivisa as Divisa',//
                 'dvd.fdisponibilidad as Fecha_Disponibilidad_Divisas',//
                 'dvd.fventa_bcv as Fecha_Venta_BCV',//
                 'op.nombre_oca as Operador_Cambiario',//
                 'dvd.descripcion as Observaciones',//
                 'st.nombre_status as Estatus',//
                 'dvd.fstatus as Fecha_Estatus'
                )
        ->join('gen_solicitud as sol', 'det.gen_usuario_id', '=', 'sol.gen_usuario_id')//
        ->join('gen_dvd_solicitud as dvd', 'sol.id', '=', 'dvd.gen_solicitud_id')//
        ->join('gen_status as st', 'dvd.gen_status_id', '=', 'st.id')//
        ->join('gen_operador_cambiario as op', 'dvd.gen_operador_cambiario_id', '=', 'op.id')//
        ->join('gen_factura as fa', 'dvd.gen_factura_id', '=', 'fa.id')//
        ->join('det_prod_factura as pro', 'fa.id', '=', 'pro.gen_factura_id')//
        ->join('gen_unidad_medida as uni', 'pro.unidad_medida_id', '=', 'uni.id')//
        ->join('gen_divisa as dv', 'dv.id', '=', 'fa.gen_divisa_id')//
        ->join('tipo_solicitud as tsol', 'fa.tipo_solicitud_id', '=', 'tsol.id')//
        ->join('forma_pago as fr', 'fa.forma_pago_id', '=', 'fr.id')//
        ->join('gen_dua as dua', 'fa.gen_dua_id', '=', 'dua.id')//
        ->join('gen_consignatario as cons', 'dua.gen_consignatario_id', '=', 'cons.id')//
        ->join('cat_tipo_convenio as cconv', 'cons.cat_tipo_convenio_id', '=', 'cconv.id')//
        ->join('gen_nota_credito as ntcr', 'fa.id', '=', 'ntcr.gen_factura_id')//
        ->whereNotIn('det.gen_usuario_id',[1,2,3,4,5,11,12,2430,2268])
        //->whereMonth('dvd.created_at',$mes)
        ->whereYear('sol.fsolicitud',$anio)
        ->where('dvd.bactivo',1)
        ->where('dvd.realizo_venta',1)
        ->orderBy('sol.fsolicitud', 'desc')
        //->whereBetween('sol.fsolicitud',[$desde,$hasta])
        ->distinct()
        ->get();

        //dd($nota_credito);

        return view('ReportesDvdNotaCred.index',compact('titulo','descripcion','desde','hasta','nota_credito'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $titulo='Reportes de Dvd Nota de Credito';
        $descripcion='Detalle de Dvd de Nota de Credito';
        $fecha=date("Y-m-d H");
        $fecha_desde=$request->desde.' 00:00:00';
        //dd($fecha_desde);
        $fecha_hasta=$request->hasta.' 23:59:59';
        $desde=$request->desde;
        $hasta=$request->hasta;
        $originalDate = "2022-06-02";
        $timestamp = strtotime($originalDate); 
        $newDate = date("d-m-Y", $timestamp );
        //dd($newDate);
        $numero = 1002002.365;
        $format=number_format($numero, 2, ",", ".");
        //dd($format);
        $nota_credito=DB::table('det_usuario as det')
        ->select('dvd.gen_solicitud_id as Num_Solicitud',//
                 'dua.fembarque as Fecha_Embarque',//
                 'sol.fsolicitud as Fecha_DVD',//
                 'tsol.solicitud as tipo_Solicitud',//
                 'fr.descrip_pago as Descripcion_de_Pago',//
                 'det.rif',//
                 'det.razon_social',//
                 'fa.numero_factura as Num_Factura',//
                 'fa.fecha_factura as Fecha_Factura',//
                 'dua.numero_dua as DUA',//
                 'fa.monto_fob as Monto_Fob_Factura',//
                 'dvd.mfob as Monto_Fob',//
                 'ntcr.monto_nota_credito as Monto_Nota_Credito',/**/
                 'ntcr.justificacion as Observacion_nota_Credito',/**/
                 'dvd.mpercibido as Monto_Percibido',//
                 'dvd.mvendido as Monto_Vendido',//
                 'dvd.mretencion as Monto_Retenido',//
                 'dvd.mpendiente as Monto_Pendiente',//
                 'dv.ddivisa as Divisa',//
                 'dvd.fdisponibilidad as Fecha_Disponibilidad_Divisas',//
                 'dvd.fventa_bcv as Fecha_Venta_BCV',//
                 'op.nombre_oca as Operador_Cambiario',//
                 'dvd.descripcion as Observaciones',//
                 'st.nombre_status as Estatus',//
                 'dvd.fstatus as Fecha_Estatus'
                )
        ->join('gen_solicitud as sol', 'det.gen_usuario_id', '=', 'sol.gen_usuario_id')//
        ->join('gen_dvd_solicitud as dvd', 'sol.id', '=', 'dvd.gen_solicitud_id')//
        ->join('gen_status as st', 'dvd.gen_status_id', '=', 'st.id')//
        ->join('gen_operador_cambiario as op', 'dvd.gen_operador_cambiario_id', '=', 'op.id')//
        ->join('gen_factura as fa', 'dvd.gen_factura_id', '=', 'fa.id')//
        ->join('det_prod_factura as pro', 'fa.id', '=', 'pro.gen_factura_id')//
        ->join('gen_unidad_medida as uni', 'pro.unidad_medida_id', '=', 'uni.id')//
        ->join('gen_divisa as dv', 'dv.id', '=', 'fa.gen_divisa_id')//
        ->join('tipo_solicitud as tsol', 'fa.tipo_solicitud_id', '=', 'tsol.id')//
        ->join('forma_pago as fr', 'fa.forma_pago_id', '=', 'fr.id')//
        ->join('gen_dua as dua', 'fa.gen_dua_id', '=', 'dua.id')//
        ->join('gen_consignatario as cons', 'dua.gen_consignatario_id', '=', 'cons.id')//
        ->join('cat_tipo_convenio as cconv', 'cons.cat_tipo_convenio_id', '=', 'cconv.id')//
        ->join('gen_nota_credito as ntcr', 'fa.id', '=', 'ntcr.gen_factura_id')//
        ->whereNotIn('det.gen_usuario_id',[1,2,3,4,5,11,12,2430,2268])
        //->whereMonth('dvd.created_at',$mes)
        ->where('dvd.bactivo',1)
        ->whereBetween('sol.fsolicitud',[$desde,$hasta])
        ->orderBy('sol.fsolicitud', 'desc')
        ->distinct()
        ->get();

        //dd($nota_credito);
        return view('ReportesDvdNotaCred.index',compact('titulo','descripcion','desde','hasta','nota_credito','fecha_desde','fecha_hasta'));
    }

    /**

     * Display the specified resource.
     *
     * @param  \App\Models\GenNotaCredito  $genNotaCredito
     * @return \Illuminate\Http\Response
     */
    public function show(GenNotaCredito $genNotaCredito)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\GenNotaCredito  $genNotaCredito
     * @return \Illuminate\Http\Response
     */
    public function edit(GenNotaCredito $genNotaCredito)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\GenNotaCredito  $genNotaCredito
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, GenNotaCredito $genNotaCredito)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\GenNotaCredito  $genNotaCredito
     * @return \Illuminate\Http\Response
     */
    public function destroy(GenNotaCredito $genNotaCredito)
    {
        //
    }

    public function ExcelReportesDvdNotaCred(Request $request, DetUsuario $detUsuario)
    {
        $desde=$request->desde;
        $hasta=$request->hasta;
        //dd($request->all());
        //dd($request->all());
        $nota_credito=DB::table('det_usuario as det')//
        ->select('dvd.gen_solicitud_id as Num_Solicitud',//
                 'dua.fembarque as Fecha_Embarque',//
                 'sol.fsolicitud as Fecha_DVD',//
                 'tsol.solicitud as tipo_Solicitud',//
                 'fr.descrip_pago as Descripcion_de_Pago',//
                 'det.rif',//
                 'det.razon_social',//
                 'fa.numero_factura as Num_Factura',//
                 'fa.fecha_factura as Fecha_Factura',//
                 'dua.numero_dua as DUA',//
                 'fa.monto_fob as Monto_Fob_Factura',//
                 'dvd.mfob as Monto_Fob',//
                 'ntcr.monto_nota_credito as Monto_Nota_Credito',
                 'ntcr.justificacion as Observacion_nota_Credito',
                 'dvd.mpercibido as Monto_Percibido',//
                 'dvd.mvendido as Monto_Vendido',//
                 'dvd.mretencion as Monto_Retenido',//
                 'dvd.mpendiente as Monto_Pendiente',//
                 'dv.ddivisa as Divisa',//
                 'dvd.fdisponibilidad as Fecha_Disponibilidad_Divisas',//
                 'dvd.fventa_bcv as Fecha_Venta_BCV',//
                 'op.nombre_oca as Operador_Cambiario',//
                 'dvd.descripcion as Observaciones',//
                 'st.nombre_status as Estatus',//
                 'dvd.fstatus as Fecha_Estatus'
                )
        ->join('gen_solicitud as sol', 'det.gen_usuario_id', '=', 'sol.gen_usuario_id')//
        ->join('gen_dvd_solicitud as dvd', 'sol.id', '=', 'dvd.gen_solicitud_id')//
        ->join('gen_status as st', 'dvd.gen_status_id', '=', 'st.id')//
        ->join('gen_operador_cambiario as op', 'dvd.gen_operador_cambiario_id', '=', 'op.id')//
        ->join('gen_factura as fa', 'dvd.gen_factura_id', '=', 'fa.id')//
        ->join('det_prod_factura as pro', 'fa.id', '=', 'pro.gen_factura_id')//
        ->join('gen_unidad_medida as uni', 'pro.unidad_medida_id', '=', 'uni.id')//
        ->join('gen_divisa as dv', 'dv.id', '=', 'fa.gen_divisa_id')//
        ->join('tipo_solicitud as tsol', 'fa.tipo_solicitud_id', '=', 'tsol.id')//
        ->join('forma_pago as fr', 'fa.forma_pago_id', '=', 'fr.id')//
        ->join('gen_dua as dua', 'fa.gen_dua_id', '=', 'dua.id')//
        ->join('gen_consignatario as cons', 'dua.gen_consignatario_id', '=', 'cons.id')//
        ->join('cat_tipo_convenio as cconv', 'cons.cat_tipo_convenio_id', '=', 'cconv.id')//
        ->join('gen_nota_credito as ntcr', 'fa.id', '=', 'ntcr.gen_factura_id')//
        ->whereNotIn('det.gen_usuario_id',[1,2,3,4,5,11,12,2430,2268])
        //->whereMonth('dvd.created_at',$mes)
        ->where('dvd.bactivo',1)
        ->whereBetween('sol.fsolicitud',[$desde,$hasta])
        ->orderBy('sol.fsolicitud', 'desc')
        ->distinct()
        ->get();

        ///dd($nota_credito);

        $originalDate = "2022-06-02";
        $timestamp = strtotime($originalDate); 
        $newDate = date("d-m-Y", $timestamp );
        //dd($newDate);
        $numero = 1002002.365;
        $format=number_format($numero, 2, ",", ".");
        //dd($format);

        $name="Reportes de solicitud Dvd Nota de Credito";
        //dd($nota_credito);
        $myFile   = Excel::create('Reportes de Solicitud Dvd Nota de Credito',function($excel)use($nota_credito){ // Nombre princial del archivo
            $excel->sheet('Dvd de Nota de Credito',function($sheet)use ($nota_credito){ // se crea la hoja con el nombre de datos
               // dd($nota_credito);
                //header
                $sheet->setStyle(array(
                'font' => array(
                    'name' =>  'itálico',
                    'size' =>  12,
                    'bold' =>  true

                )
                ));

                $sheet->mergeCells('A1:I1'); // union de celdas desde A a F
                $sheet->row(1,['Reportes de Solicitud Dvd Nota de Credito']);
                $sheet->row(2,[]);
                //$sheet->row(3,[]);

                $sheet->row(3,['Número Solicitud','Fecha de Embarque','Fecha DVD','Tipo Solicitud','Descripcion de Pago','Rif','Razón Social','Número de Factura','Fecha de Factura','Dua','Monto Fob Factura','Monto Fob','Monto Nota Credito','Observacion nota Credito','Monto Percibido','Monto Vendido','Monto Retenido','Monto Pendiente','Divisa','Fecha Disponibilidad Divisas','Fecha Venta BCV','Operador Cambiario','Observaciones','Estatus','Fecha de Estatus']);

                //$sheet->fromArray($nota_credito, null, 'A4', null, false);
                // consulta

                
               //dd($nota_credito);
                //recorrido
                foreach ($nota_credito as $emp) {

                    $forma_embarque=strtotime($emp->Fecha_Embarque); 
                    $fechaembarque = date("d-m-Y", $forma_embarque); 

                    $forma_sol=strtotime($emp->Fecha_DVD); 
                    $fechasol = date("d-m-Y", $forma_sol);

                    $forma_factura=strtotime($emp->Fecha_Factura); 
                    $fechafactura = date("d-m-Y", $forma_factura);

                    $forma_disponibilidad=strtotime($emp->Fecha_Disponibilidad_Divisas); 
                    $fechadisponibilidad = date("d-m-Y", $forma_disponibilidad);

                    $forma_venta=strtotime($emp->Fecha_Venta_BCV); 
                    $fechaventa = date("d-m-Y", $forma_venta );

                    $forma_status=strtotime($emp->Fecha_Estatus); 
                    $fechastatus = date("d-m-Y", $forma_status);


                   $row=[]; // un arreglo vacio que se va a llenar con las posicion 
                   $row[0]=$emp->Num_Solicitud;
                   $row[1]=$fechaembarque;
                   $row[2]=$fechasol;
                   $row[3]=$emp->tipo_Solicitud;
                   $row[4]=$emp->Descripcion_de_Pago;
                   $row[5]=$emp->rif;
                   $row[6]=$emp->razon_social;
                   $row[7]=$emp->Num_Factura;
                   $row[8]=$fechafactura;///
                   $row[9]=$emp->DUA;
                   $row[10]=number_format($emp->Monto_Fob_Factura, 2, ",", ".");
                   $row[11]=number_format($emp->Monto_Fob, 2, ",", ".");
                   $row[12]=number_format($emp->Monto_Nota_Credito, 2, ",", ".");
                   $row[13]=$emp->Observacion_nota_Credito;
                   $row[14]=number_format($emp->Monto_Percibido, 2, ",", ".");
                   $row[15]=number_format($emp->Monto_Vendido, 2, ",", ".");
                   $row[16]=number_format($emp->Monto_Retenido, 2, ",", ".");
                   $row[17]=number_format($emp->Monto_Pendiente, 2, ",", ".");
                   $row[18]=$emp->Divisa;
                   $row[19]=$fechadisponibilidad;
                   $row[20]=$fechaventa;
                   $row[21]=$emp->Operador_Cambiario;
                   $row[22]=$emp->Observaciones;
                   $row[23]=$emp->Estatus;
                   $row[24]=$fechastatus;

                   $sheet->appendRow($row); // agrega una fila vacia al final del reporte

                }


            });
           // dd($nota_credito);
           
        });
        $myFile   = $myFile->string('xls'); 
        $response = array(
            'name' => $name, 
            'file' => "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64," . base64_encode($myFile), 
        );

        return response()->json($response);
        //->export('xlsx');
    }
}



/******query de nota de credito
SELECT distinct 
dvd.gen_solicitud_id as "N° Solicitud", DATE_FORMAT(dua.fembarque, "%d/%m/%Y") as "Fecha de Embarque",DATE_FORMAT(sol.fsolicitud, "%d/%m/%Y") as "Fecha DVD", tsol.solicitud as "tipo Solicitud",
fr.descrip_pago as "Descripción de Pago",
det.rif as "RIF",
det.razon_social as "Nombre de Empresa",
fa.numero_factura as "N° Factura",
DATE_FORMAT(fa.fecha_factura, "%d/%m/%Y") as "Fecha de Factura",
dua.numero_dua as "DUA",
FORMAT(fa.monto_fob, 2) as "Monto Fob Factura",FORMAT(dvd.mfob, 2) as "Monto Fob",
FORMAT(ntcr.monto_nota_credito, 2) as "Monto Nota de Credito",
ntcr.justificacion as "Observacion de nota de Credito",
/*FORMAT(notde.monto_nota_debito, 2) as "Monto Nota Debito",*/
/**FORMAT(dvd.mpercibido, 2) as "Monto Percibido",
FORMAT(dvd.mvendido, 2) as "Monto Vendido",
FORMAT(dvd.mretencion, 2) as "Monto Retenido",FORMAT(dvd.mpendiente, 2) as "Monto Pendiente",
dv.ddivisa as "Divisa",
DATE_FORMAT(dvd.fdisponibilidad, "%d/%m/%Y") as "Fecha Disponibilidad Divisas",
DATE_FORMAT(dvd.fventa_bcv, "%d/%m/%Y") as "Fecha Venta BCV",
op.nombre_oca as "Operador Cambiario", dvd.descripcion as "Observaciones",
st.nombre_status as "Estatus",DATE_FORMAT(dvd.fstatus, "%d/%m/%Y") as "Fecha de Estatus"
FROM det_usuario det 
join gen_solicitud sol on det.gen_usuario_id=sol.gen_usuario_id 
join gen_dvd_solicitud dvd  on sol.id=dvd.gen_solicitud_id
join gen_status st on dvd.gen_status_id = st.id
join gen_operador_cambiario op on dvd.gen_operador_cambiario_id=op.id
join gen_factura fa on dvd.gen_factura_id=fa.id 
/*join factura_solicitud fac on sol.id = fac.gen_solicitud_id ***/
/**join det_prod_factura pro on fa.id=pro.gen_factura_id
join gen_unidad_medida uni on pro.unidad_medida_id = uni.id 
join gen_divisa dv on dv.id = fa.gen_divisa_id
join tipo_solicitud tsol on fa.tipo_solicitud_id=tsol.id
join forma_pago fr on fa.forma_pago_id = fr.id
join gen_dua dua on fa.gen_dua_id=dua.id
join gen_consignatario cons on dua.gen_consignatario_id=cons.id
join cat_tipo_convenio cconv on cons.cat_tipo_convenio_id=cconv.id
join gen_nota_credito ntcr on fa.id=ntcr.gen_factura_id
/*join nota_debito notde on fa.id=notde.gen_factura_id****/
/*where det.id not in (1,2,3,4,5,11,12)
       and sol.fsolicitud BETWEEN '2018-07-01' AND '2019-02-25'*/
