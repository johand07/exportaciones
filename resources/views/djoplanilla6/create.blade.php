@extends('templates/layoutlte')
@section('content')
<div class="content">
<div class="panel-group" style="background-color: #fff;">
      <div class="panel-primary">
        <div class="panel-heading"><h3>(P-6) - 8-. Fotografía del producto a exportar</h3></div>
        <div class="panel-body">
                  <div class="row" style="background-color: #fff">
                      <div class="col-md-12">
                        
                  {!! Form::open(['route' =>'DJOPlanilla6.store' ,'method'=>'POST', 'id'=>'planilla6Form', 'enctype' => 'multipart/form-data']) !!}

                          <div class="row">
                              
                              <div class="col-md-12">
                                  <table border="0" class="col-md-12">
                                    <thead>
                                      <tr>
                                        <th class="text-center" width="50%" height="40px"><h3><b>Sección de Carga</b></h3></th>
                                        <th class="text-center"><h3><b>Vista Previa</b></h3></th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      <tr>
                                        <td class="text-center" valign="top" > 
                                          <span><b>1.)</b></span>
                                          <span id="class_foto_1" class="btn btn-default btn-file" style="margin: 3px;">
                                              Cargar fotografía 1 del producto <span class="glyphicon glyphicon-picture"></span>
                                              {!! Form::file('foto_1', array('class' => 'file-input','style'=>'margin: 10px;')) !!}
                                          </span>
                                        </td>
                                        <td class="text-center" height="40px">
                                          <img id="imgdocumento_foto_1" src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px; display: none;">
                                          <img id="vista_previa_foto_1" src="" alt="" width="300px" style="margin: 3px;">
                                          <div id="alert_foto_1"></div></td>
                                      </tr>

                                      <tr>
                                        <td class="text-center" valign="top" >
                                          <span><b>2.)</b></span> 
                                          <span id="class_foto_2"class="btn btn-default btn-file" style="margin: 3px;">
                                              Cargar fotografía 2 del producto <span class="glyphicon glyphicon-picture"></span>
                                              {!! Form::file('foto_2', array('class' => 'file-input','style'=>'margin: 10px;', 'noreq'=>'noreq')) !!}
                                          </span>
                                        </td>
                                        <td class="text-center" height="40px">
                                          <img id="imgdocumento_foto_2" src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px; display: none;">
                                          <img id="vista_previa_foto_2" src="" alt="" width="300px" style="margin: 3px;">
                                          <div id="alert_foto_2"></div></td>
                                      </tr>

                                      <tr>
                                        <td class="text-center" valign="top" >
                                          <span><b>3.)</b></span> 
                                          <span id="class_foto_3" class="btn btn-default btn-file" style="margin: 3px;">
                                              Cargar fotografía 3 del producto <span class="glyphicon glyphicon-picture"></span>
                                              {!! Form::file('foto_3', array('class' => 'file-input','style'=>'margin: 10px;', 'noreq'=>'noreq')) !!}
                                          </span>
                                        </td>
                                        <td class="text-center" height="40px"><img id="vista_previa_foto_3" src="" alt="" width="300px" style="margin: 3px;">
                                        <img id="imgdocumento_foto_3" src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px; display: none;">
                                        <div id="alert_foto_3"></div></td>
                                      </tr>

                                    </tbody>
                                  </table>
                              </div>
                          </div>
<hr>
                          <div class="row" style="background-color: #fff">

                                <div class="col-md-12" align="center">

                                     <input type="submit"  name="submit" id="submit" class="btn btn-primary"  value="Guardar" onclick="validacionForm()">
                                    <a class="btn btn-danger btn-md" href="{{ url('exportador/DeclaracionJO') }}">Cancelar</a>

                                </div>
                              
                            </div>


                  {{Form::close()}}

                      </div>
                  </div>

        </div>
      </div>
    </div>

  </div><!-- Fin content -->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script>

   function cambiarImagen(event) {
    var id= event.target.getAttribute('name');
    var reader = new FileReader();
    reader.onload = function(){
      var output = document.getElementById('vista_previa_'+id);
      output.src = reader.result;
    };

  
   // reader.readAsDataURL(event.target.files[0]);
    var nombre = event.target.getAttribute('name');
    var archivo = $("[name^="+nombre+"]").val();
 
    var extensiones = archivo.substring(archivo.lastIndexOf(".")).toLowerCase();
    if(extensiones==".jpeg" ||extensiones==".png" || extensiones ==".jpg" || extensiones ==".gif")
    {
 
          let file=$('#'+id).val();
          $('#class_'+id).removeClass('btn btn-danger btn-file').addClass('btn btn-success btn-file')
          $('#class_'+id).removeClass('btn btn-default btn-file').addClass('btn btn-success btn-file')
          $('#nombre_'+id).html('<strong>'+file+'</strong>');
          $('#imgdocumento_'+id).hide();
          $('#vista_previa_'+id).show();
          $('#alert_'+id).html('<p style="color:red;"></p>');
         reader.readAsDataURL(event.target.files[0]);
       
        }else{        
        
         $('#imgdocumento_'+id).show();
         
        //  let file=$('#'+id).val();
          //$('#nombre_'+id).html('<strong>'+file+'</strong>');
          $('#class_'+id).removeClass('btn btn-default btn-file').addClass('btn btn-danger btn-file')
          $('#vista_previa_'+id).hide(); 
          $('#alert_'+id).html('<p style="color:red;">Formato no permitido para la carga</p>');
          $('#submit').attr('disabled', 'disabled');            
        } 


  
  }

    $(document).on('change', '.file-input', function(evt) {
           cambiarImagen(evt);
    });


</script>

<script>
  //Validación de formulario, se puede llamar y pasar la sección que contenga los inputs a validar
  $(document).ready(function ()
  {
              var error = '<div class="msg_alert"><span style="color:red" class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span><strong id="cod_aran-error" class="added"> El campo es obligatorio.</strong></div>';
                var valido = '<div class="msg_alert"><span style="color:green" class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span></div>';
                var mens=['Estimado Usuario. Debe completar los campos solicitados',]
  
                validacionForm = function() {
  
                $('#planilla6Form').submit(function(event) {
                
                var campos = $('#planilla6Form').find('input:file');
                var n = campos.length;
                var err = 0;
  
                $("div").remove(".msg_alert");
                //bucle que recorre todos los elementos del formulario
                for (var i = 0; i < n; i++) {
                    var cod_input = $('#planilla6Form').find('input:file').eq(i);
                    if (!cod_input.attr('noreq')) {
                      if (cod_input.val() == '' || cod_input.val() == null)
                      {
                        err = 1;
                        cod_input.css('border', '1px solid red').after(error);
                      }
                      else{
                        if (err == 1) {err = 1;}else{err = 0;}
                        cod_input.css('border', '1px solid green').after(valido);
                      }
                      
                    }
                }
  
                //Si hay errores se detendrá el submit del formulario y devolverá una alerta
                if(err==1){
                    event.preventDefault();
                    swal("Por Favor!", mens[0], "warning")
                  }
  
                  });
                }
              });
  </script>

@endsection