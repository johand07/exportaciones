@extends('templates/layoutlte')

@section('content')
<style >
.swal-close {
  position: absolute;
  top: 0;
  right: 0;
  margin-top: 10px;
  margin-right: 10px;
  z-index: 10;
}
</style >
<div class="panels">
<div class="content" style="background-color: #fff">
    <div class="panel-group" style="background-color: #fff;">
      <div class="panel-primary">
        <div class="panel-heading"><h3>(P-1) - 1. Datos Generales de la Empresa</h3> </div>
        <div class="panel-body">
          <input type="hidden" name="ruta_doc" id="ruta_doc" value="{{ $usuario->ruta_doc }}">
          <input type="hidden" name="ruta_rif" id="ruta_rif"  value="{{ $usuario->ruta_rif }}">
          <input type="hidden" name="ruta_reg_merc" id="ruta_reg_merc"  value="{{ $usuario->ruta_reg_merc }}">
            <div class="row">
              
              <div class="col-md-12">
                <div class="row">
                  <div class="col-md-7 col-sm-7 col-xs-7">

                           <h4 class="text-info"> Mis Declaraciones Juradas de Origen:</h4>
                          
                  </div>                 
                  <div class="col-md-3 col-sm-3 col-xs-3">
                    <a class="btn btn-success form-control" href="{{route('ListaDeclaracionJO.create')}}">
                      <span class="glyphicon glyphicon-file" title="Agregar" aria-hidden="true"></span>
                       Crear Nueva Solicitud
                    </a>
                  </div>
                  <div class="col-md-2 col-sm-2 col-xs-2">
                   
                    <a class="btn btn-primary form-control" href="{{route('ListaDeclaracionJO.index')}}">
                      <span class="glyphicon glyphicon-refresh" title="Agregar" aria-hidden="true"></span>
                        Actualizar
                    </a>
                  </div>

                </div> <hr>
                <table id="listaUsuarios" class="table table-striped table-bordered" style="width:100%">
                        <thead>
                            <tr>
                            	  <th class="text-center">Nº</th>
                                <th class="text-center">Nº Solicitud</th>                     
                                <th class="text-center">Razon Social</th>
                                <th >Rif</th>
                                <!--th class="text-center">Nº Productos</!--th-->
                                <th class="text-center">Paises Dest</th>                     
                                <th class="text-center" >F. de Solicitud</th> 
                                <th class="text-center">Estatus</th>                                 
                                <th class="text-center">Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                          @foreach($declaraciones as $key => $declaracion)
                            @if($declaracion->bactivo == 1)
                            <tr class="text-center" >           
                            	  <td width="4%">{{ $key+1 }}</td>          
                                <td width="15%"><b class="text-primary">{{ $declaracion->num_solicitud }}</b></td>
                                <td >{{ $declaracion->razon_social }}</td> 
                                <td >{{ $declaracion->rif }}</td>
                                <!--td class="text-center">{{ $declaracion->num_productos }}</!--td--> 
                                <td class="text-center">{{ $declaracion->num_paises }}</td>
                                <td class="col-md-2 text-center">{{ $declaracion->fstatus }}</td>
                                <td class="col-md-1" style="font-size: 11px;">
                                  @if($declaracion->gen_status_id==11)
                                    @if(@$declaracion->observacion_corregida==1)
                                        ¡Corrección Enviada!
                                    @else
                                        ¡Tiene Observaciones!
                                    @endif                                  
                                  @elseif($declaracion->gen_status_id==10)
                                  ¡Atendido por un Analista!
                                  @else
                                     ¡{{ $declaracion->nombre_status }}!
                                  @endif
                                </td>
                                <td class="col-md-1" id="estatus">
                                  <input id="token" type="hidden" name="_token" value="{{ csrf_token() }}">
                                  @if ($declaracion->gen_status_id==7)
                                        
                                      <a href="{{ route('DeclaracionJO.index',['djo'=>encrypt($declaracion->id)])}}" class="text-center btn btn-sm btn-danger list-inline "><i class="glyphicon glyphicon-exclamation-sign"></i> <b>Completar</b></a>
                                  
                                  @elseif($declaracion->gen_status_id==8)
                                  
                                      <a class="text-center btn btn-sm btn-danger list-inline"><i class="glyphicon glyphicon-remove "></i> {{ $declaracion->nombre_status }}</a>

                                  @elseif($declaracion->gen_status_id==9)
                                  
                                      <a class="text-center btn btn-sm btn-default list-inline"><i class="glyphicon glyphicon-refresh"></i> {{ $declaracion->nombre_status }}</a>

                                  @elseif($declaracion->gen_status_id==10)
                                      
                                      <a class="text-center btn btn-sm btn-primary list-inline"><i class="glyphicon glyphicon-user"></i> <b>Atendido</b> </a>

                                  @elseif($declaracion->gen_status_id==11)
                                        @if(@$declaracion->observacion_corregida==1)
                                            <a class="text-center btn btn-sm btn-info list-inline"><i class="glyphicon glyphicon-time"></i> <b>En Observacion </b></a>
                                        @else
                                            <a href="{{ route('DeclaracionJO.index',['djo'=>encrypt($declaracion->id)])}}" class="text-center btn btn-sm btn-warning list-inline"><i class="glyphicon glyphicon-pencil text-black"></i> <b>Corregir</b></a>
                                        @endif           
                                  @elseif($declaracion->gen_status_id==16)
                                      <button onclick="confirmView({{$declaracion->id}})" class="text-center btn btn-sm btn-success list-inline"><i class="glyphicon glyphicon-download"></i> <b>Descargar</b></button>
                                  
                                 
                                  
                                      @endif
                                </td>
                                @endif
                                </tr>              
            				      @endforeach
                        </tbody>
                    </table>
              </div>
              
            </div>
        </div>
      </div>
    </div>
</div><!-- fin content-->
<!-- Button trigger modal -->
<button type="button" class="btn btn-primary btn-lg hide" data-toggle="modal" data-target="#myModaltwo" id="btn_modal">
  Launch demo modal
</button>

<!-- Modal -->
<div class="modal fade" id="myModaltwo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" style="font-size:35px;">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><b style="color:#4C8BF5; font-size:20px;">Soporte de Documentos de Usuario</b></h4><br>
      </div>
      <div class="modal-body" >
        <p><h3><ul id="content_docs"></ul></h3></p>
      </div>
      <div class="modal-footer">
        <a href="{{route('DocumentosSoportes.create',Auth::user()->id)}}" type="button" class="btn btn-primary"><b style="font-size:14px;">Ir a Perfil del Exportador</b></a>
      </div>
    </div>
  </div>
</div>
</div>
<!-- Fin Modal Doc Soportes //////////////////////////////////////////////////////////-->
@stop
<!-- js calendario bootstrap -->
<script src="{{asset('js/jquery.min.js')}}"></script>

<script type="text/javascript">
  $(document).ready(function() {
   
    let ci=$('#ruta_doc').val();//se inicializan las variables ruta_doc
    let rif=$('#ruta_rif').val();//se incializa la variable ruta_rif
    let merc=$('#ruta_reg_merc').val();
    $('#content_docs').html('');
    if(!ci || ci === '') {// si viene vacio
      //console.log('ci', ci);
      $('#content_docs').append('<li>Por favor adjuntar el  documento de Identidad Cédula en su perfil del Exportador</li>')
      
    } 
    if(!rif || rif === ''){
      //console.log('rif', rif);
      $('#content_docs').append('<li>Por favor adjuntar el documento Rif en su perfil del Exportador</li>')
      // $('#btn_modal').click();
    } 

    if(!merc || merc === ''){
      //console.log('merc', merc);
      $('#content_docs').append('<li>Por favor adjuntar el documento Registro Mercantil en su perfil del Exportador</li>')
      // $('#btn_modal').click();
    }
    if ( (!ci || ci === '') || (!rif || rif === '') || (!merc || merc === '')) {
       $('#btn_modal').click();//boton que dispara la modal
    }
   
    
  });
</script>

<script>
	$(document).ready(function() {
	
	  confirmView = function(DOid) {
      $(document).on('click', '.SwalBtn1', function() {
        //Some code 1
        window.location.href = "DeclaracionJO/pdf?djo="+DOid;
        
    });
   
  //  '<a href="{{ route("DeclaracionJO.pdf",["djo"=>'+DOid+'])}}" target="_blank" class="SwalBtn2 text-center btn btn-sm btn-success list-inline">' + 'Descargar' + '</a>',
var closeButton = document.createElement('button');
closeButton.innerHTML = 'X';
closeButton.classList.add('swal-close');
closeButton.addEventListener('click', function() {
  swal.close();
});	
  swal({
		  title: "¿Qué deseas realizar con esta solicitud?",
		  html: true,
		  text: '<a href="DeclaracionJO/'+DOid+'/edit" class="text-center btn btn-sm btn-danger list-inline" style="margin-right:5px">' + 'Solicitar apelación' + '</a> ' +
            '<button type="button" role="button" class="SwalBtn1 text-center btn btn-sm btn-success list-inline">' + 'Descargar' + '</button>',
            type: "warning",
		  showConfirmButton: false,
		  showCancelButton: false,
      customClass: 'swal-with-close-button'
		});
    document.querySelector('.swal-with-close-button').appendChild(closeButton);
	  }
    

    confirmViewDec = function(DOid) {
      var DOid=DOid;
      $DOid = json_decode(DOid);

      swal({
        title: "Sólo descarga de planilla",
        html: true,
        text: '<a href="{{ route("DeclaracionJO.pdf",["djo"=>encrypt('+DOid+')])}}" target="_blank" class="SwalBtn2 text-center btn btn-sm btn-success list-inline">' + 'Descargar' + '</a>',
        type: "warning",
        showConfirmButton: false,
        showCancelButton: false
      });
      }
	
	});
	
</script>