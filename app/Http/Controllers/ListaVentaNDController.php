<?php

namespace App\Http\Controllers;

use App\Models\GenDvdNd;
use App\Models\NotaDebito;
use App\Models\DocumentosDvdNdBdv;
use Session;
use Auth;
use Laracasts\Flash\Flash;
use Alert;
use Illuminate\Http\Request;

class ListaVentaNDController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $titulo="Declaración de Ventas de Divisas por Notas de Débito";
        $descripcion="Notas de Debito Asociadas a una Factura";
        $NDebito=NotaDebito::where('gen_usuario_id',Auth::user()->id)->where('bactivo',1)->get();
        return view('DvdSolicitudND.index',compact('titulo','descripcion','NDebito'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\GenDvdNd  $GenDvdNd
     * @return \Illuminate\Http\Response
     */
    public function show(GenDvdNd $GenDvdNd)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\GenDvdNd  $GenDvdNd
     * @return \Illuminate\Http\Response
     */
    public function edit(GenDvdNd $GenDvdNd, $id)
    {
       $titulo="Declaración de Ventas de Divisas";
        $descripcion="Notas de Debito";
        $listaventa=GenDvdNd::where('nota_debito_id',$id)->where('tipo_solicitud_id',4)->get();
        //$Doc_Dvd_Nd=DocumentosDvdNdBdv::where('nota_debito_id',$id)->first();
        $idsolicitud=$id;
        //dd($listaventa);
        if (!empty($listaventa[0])) {
            return view('ListaVentaND.edit',compact('listaventa','titulo','descripcion','idsolicitud'));
        } else {
            //dd($listaventa);
             Alert::warning('Debe registrar por lo menos una', 'No hay venta de divisa Registrada!')->persistent("Ok");
            return view('ListaVentaND.edit',compact('listaventa','titulo','descripcion','idsolicitud'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\GenDvdNd  $GenDvdNd
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, GenDvdNd $GenDvdNd)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\GenDvdNd  $GenDvdNd
     * @return \Illuminate\Http\Response
     */
    public function destroy(GenDvdNd $GenDvdNd)
    {
        //
    }
}
