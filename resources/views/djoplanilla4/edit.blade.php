@extends('templates/layoutlte')
@section('content')

<div class="content">
  @if(!empty($planilla4->estado_observacion))
  <script>

  document.onreadystatechange = function () {
    var state = document.readyState;
    if (state == 'complete') {
      swal("Estimado usuario debera corregír las siguientes observaciones", "{{$planilla4->descrip_observacion}}", "warning");
    }
  }
  </script>
<div id="space_alert1" class="alert alert-danger" role="alert" style="display: block;">
  <strong>
    <span class="glyphicon glyphicon-circle-arrow-right"></span> Observación Indicada por el Analista: {{$planilla4->descrip_observacion}}
  </strong>
</div>
@endif
<div class="panel-group" style="background-color: #fff;">
      <div class="panel-primary">
        <div class="panel-heading"><h3>(P-4) - 6. Proceso productivo literal del producto a exportar</h3></div>
        <div class="panel-body">

<div class="row" style="background-color: #fff">
    <div class="col-md-12">
 

{{ Form::model($planilla4, array('route' => array('DJOPlanilla4.update', $planilla4->id), 'method' => 'PUT','id'=>'planilla4UpdateForm')) }}

        <div class="row">
            <div class="col-md-12"><h4><b>Producto:</b><h4></div>
            <div class="col-md-12">
                <h4>{{$producto->descrip_comercial}} (COD.ARANCELARIO: {{$producto->codigo}})</h4>
            </div>
            <div class="col-md-12" >
                <div class="form-group" style="border:1px solid; border-radius:5px;">
                    {!! Form::textarea('proceso',$planilla4->proceso_produccion,['class'=>'form-control editor-texto','placeholder'=>'Escribe aqui el proceso productivo de tu producto a Exportar..'])!!}
                </div>
            </div>
        </div>

        <div class="row" style="background-color: #fff">

              <div class="col-md-12" align="center">

                  <!--input type="submit"  name="submit" id="submit" class="btn btn-info"  value="Siguiente"/-->
                  <button  type="submit"  name="submit" id="submit"  class="btn btn-primary" onclick="validacionForm()"><b>Corregir</b></button>

              </div>
            
          </div>


{{Form::close()}}

    </div>
</div>

        </div>
      </div>
    </div>

  </div><!-- Fin content -->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
    <script>
    CKEDITOR.config.browserContextMenuOnCtrl = false;
        CKEDITOR.replace( 'proceso',{

              language: 'es',
              uiColor: '#FFC3DF28',
              toolbar: [
                         { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-'] },
                        { name: 'insert', items: [ 'HorizontalRule','PageBreak'] },
                        { name: 'tools', items: [ 'Maximize', 'ShowBlocks' ] },
                        { name: 'editing', groups: [ 'find', 'selection', 'spellchecker' ], items: [ 'Find', 'Replace', '-', 'SelectAll', '-', 'Scayt' ] },
                       

                ],
              

        });
        

    </script>

<script>
  //Validación de formulario, se puede llamar y pasar la sección que contenga los inputs a validar
  $(document).ready(function ()
  {
              var error = '<div class="msg_alert"><span style="color:red" class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span><strong id="cod_aran-error" class="added"> El campo es obligatorio.</strong></div>';
                var valido = '<div class="msg_alert"><span style="color:green" class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span></div>';
                var mens=['Estimado Usuario. Debe completar los campos solicitados',]
  
                validacionForm = function() {
  
                $('#planilla4UpdateForm').submit(function(event) {
                
                var campos = $('#planilla4UpdateForm').find('textarea');
                var n = campos.length;
                var err = 0;
  
                $("div").remove(".msg_alert");
                //bucle que recorre todos los elementos del formulario
                for (var i = 0; i < n; i++) {
                    var cod_input = $('#planilla4UpdateForm').find('textarea').eq(i);
                    if (!cod_input.attr('noreq')) {
                      if (cod_input.val() == '' || cod_input.val() == null)
                      {
                        err = 1;
                        cod_input.css('border', '1px solid red').after(error);
                      }
                      else{
                        if (err == 1) {err = 1;}else{err = 0;}
                        cod_input.css('border', '1px solid green').after(valido);
                      }
                      
                    }
                }
  
                //Si hay errores se detendrá el submit del formulario y devolverá una alerta
                if(err==1){
                    event.preventDefault();
                    swal("Por Favor!", mens[0], "warning")
                  }
  
                  });
                }
              });
  </script>


@endsection