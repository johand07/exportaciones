-- phpMyAdmin SQL Dump
-- version 4.2.12deb2+deb8u2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 18, 2019 at 12:54 PM
-- Server version: 10.0.32-MariaDB-0+deb8u1
-- PHP Version: 7.1.26-1+0~20190113101856.12+jessie~1.gbp7077bb

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `exportaciones`
--

-- --------------------------------------------------------

--
-- Table structure for table `gen_sol_inversionista`
--

CREATE TABLE IF NOT EXISTS `gen_sol_inversionista` (
`id` int(10) unsigned NOT NULL,
  `num_sol_inversionista` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gen_usuario_id` int(10) unsigned DEFAULT NULL,
  `gen_status_id` int(10) unsigned DEFAULT NULL,
  `pais_id` int(10) unsigned DEFAULT NULL,
  `cat_tipo_visa_id` int(10) unsigned DEFAULT NULL,
  `fstatus` date DEFAULT NULL,
  `estado_observacion` tinyint(1) DEFAULT '0',
  `observacion_inversionista` varchar(1500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `observacion_anulacion` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `observacion_doc_incomp` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bactivo` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `gen_sol_inversionista`
--


--
-- Indexes for dumped tables
--

--
-- Indexes for table `gen_sol_inversionista`
--
ALTER TABLE `gen_sol_inversionista`
 ADD PRIMARY KEY (`id`), ADD KEY `gen_sol_inversionista_gen_usuario_id_index` (`gen_usuario_id`), ADD KEY `gen_sol_inversionista_gen_status_id_index` (`gen_status_id`), ADD KEY `gen_sol_inversionista_pais_id_index` (`pais_id`), ADD KEY `gen_sol_inversionista_cat_tipo_visa_id_index` (`cat_tipo_visa_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `gen_sol_inversionista`
--
ALTER TABLE `gen_sol_inversionista`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `gen_sol_inversionista`
--
ALTER TABLE `gen_sol_inversionista`
ADD CONSTRAINT `gen_sol_inversionista_cat_tipo_visa_id_foreign` FOREIGN KEY (`cat_tipo_visa_id`) REFERENCES `cat_tipo_visa` (`id`),
ADD CONSTRAINT `gen_sol_inversionista_gen_status_id_foreign` FOREIGN KEY (`gen_status_id`) REFERENCES `gen_status` (`id`),
ADD CONSTRAINT `gen_sol_inversionista_gen_usuario_id_foreign` FOREIGN KEY (`gen_usuario_id`) REFERENCES `gen_usuario` (`id`),
ADD CONSTRAINT `gen_sol_inversionista_pais_id_foreign` FOREIGN KEY (`pais_id`) REFERENCES `pais` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
