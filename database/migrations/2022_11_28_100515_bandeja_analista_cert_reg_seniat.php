<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BandejaAnalistaCertRegSeniat extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bandeja_analista_cert_reg_seniat', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('gen_cert_reg_seniat_id')->nullable();
            $table->foreign('gen_cert_reg_seniat_id')->references('id')->on('gen_cert_reg_seniat');
            $table->unsignedInteger('gen_usuario_id')->nullable();
            $table->foreign('gen_usuario_id')->references('id')->on('gen_usuario');
            $table->unsignedInteger('gen_status_id')->nullable();
            $table->foreign('gen_status_id')->references('id')->on('gen_status');
            $table->date('fstatus',150)->nullable();
            $table->boolean('bactivo')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bandeja_analista_cert_reg_seniat');
    }
}
