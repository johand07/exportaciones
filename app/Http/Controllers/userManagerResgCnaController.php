<?php

namespace App\Http\Controllers;

use App\Models\GenUsuario;
use App\Models\DetUsuario;
use App\Models\GenSolResguardoAduanero;
use App\Models\GenDocResguardo;
use App\Models\CatPreguntaSeg;
use App\Models\GenTipoUsuario;
use App\Models\GenStatus;
use App\Models\GenAduanaSalida;
use App\Models\DetUsersResguardo;
use Illuminate\Support\Facades\DB;
use Session;
use Auth;
use Alert;
use File;
use Illuminate\Http\Request;

class userManagerResgCnaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // 
        $detUser= DetUsersResguardo::where('gen_usuario_id',Auth::user()->id )->first();
        if(!empty($detUser)){
        $users = GenUsuario::where('bactivo', 1)->whereIn('gen_tipo_usuario_id', [11,12,13])->whereNotIn('id',[2, 4, 1040, 1041, 1325, 1326, 1327, 1328, 1329, 1330, 1798, 1799, 1800, 2059])->get();
        } else {
            Alert::warning('Este usuario no tiene aduana asociada')->persistent("OK");
            
            return redirect()->action('AdministradorResguardoCnaController@index'); 
    
        }
        return view('userManagerResgCna.index', compact('users','detUser'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $titulo= 'Creación de Usuarios';
        $descripcion= 'Usuarios del sistema';
        
        $preguntas=CatPreguntaSeg::all()->pluck('descripcion_preg','id');
        $tipousu=GenTipoUsuario::whereIn('id',[11,12,13])->pluck('nombre_tipo_usu','id');
        $statususu=GenStatus::whereIn('id', [1, 2])->pluck('nombre_status','id');
        
        $AduanaSalida=GenAduanaSalida::where('bactivo',1)->whereIn('id', [1,4,7])->pluck('daduana','id');
        
       // dd($oca);
        //dd($statususu);
        return view('userManagerResgCna.create',compact('preguntas','tipousu','statususu', 'titulo', 'descripcion','AduanaSalida'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (isset($request->cat_preguntas_seg_id)) {
        if (isset($request->pregunta_alter)) {

           $pregunta=$request['pregunta_alter'];
        }else{

           $pregunta='N/A';
        }
      }
      $usu= new GenUsuario;
      $usu->email=$request->email;
      $usu->email_seg=$request->email_seg;
      $usu->gen_tipo_usuario_id=$request->gen_tipo_usuario_id;
      $usu->gen_status_id=$request->gen_status_id;
      $usu->password=bcrypt($request->password);
      $usu->cat_preguntas_seg_id=$request->cat_preguntas_seg_id;
      $usu->pregunta_alter=$pregunta;
      $usu->respuesta_seg=$request->respuesta_seg;
      $usu->remember_token=str_random(10);
      $usu->save();

      $Det_usu= new DetUsersResguardo;
      $Det_usu->gen_usuario_id=$usu->id;
      $Det_usu->nombre_funcionario=$request->nombre_funcionario;
      $Det_usu->apellido_funcionario=$request->apellido_funcionario;
      $Det_usu->cedula_funcionario=$request->cedula_funcionario;
      $Det_usu->rango_funcionario=$request->rango_funcionario;
      $Det_usu->unidad=$request->unidad;
      $Det_usu->gen_aduana_salida_id=$request->gen_aduana_salida_id;
      //$Det_usu->ubicacion=$request->ubicacion;
      $Det_usu->tlf_funcionario=$request->tlf_funcionario;
      
      
      $Det_usu->save();

      Alert::success('Registro Exitoso!', 'Usuario agregado.')->persistent("Ok");
      return redirect()->action('userManagerResgCnaController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\GenUsuario  $genUsuario
     * @return \Illuminate\Http\Response
     */
    public function show(GenUsuario $genUsuario)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\GenUsuario  $genUsuario
     * @return \Illuminate\Http\Response
     */
    public function edit(GenUsuario $genUsuario, $id)
    {
        $titulo= 'Editar Usuarios Exportadores';
        $descripcion= 'Editar Usuario';
        $usu=GenUsuario::find($id);
        $usu_edit = DB::table('gen_usuario')
              
            ->join('det_users_resguardo', 'gen_usuario.id','=','det_users_resguardo.gen_usuario_id')
              
            ->select(
                    'gen_usuario.id',
                    'gen_usuario.email',
                    'gen_usuario.email_seg',
                    'gen_usuario.gen_tipo_usuario_id',
                    'gen_usuario.gen_status_id',
                    'gen_usuario.cat_preguntas_seg_id',
                    'gen_usuario.respuesta_seg',
                    'det_users_resguardo.nombre_funcionario',
                    'det_users_resguardo.apellido_funcionario',
                    'det_users_resguardo.cedula_funcionario',
                    'det_users_resguardo.rango_funcionario',
                    'det_users_resguardo.unidad',
                    'det_users_resguardo.gen_aduana_salida_id',
                    
                    'det_users_resguardo.tlf_funcionario'
                    
                      )
            ->where('gen_usuario.id','=',$id)
            ->first();
        
        if(empty($usu_edit)) {
            $usu_edit = $usu;
        }
        
        // dd($usu_edit);
        $tipousu=GenTipoUsuario::whereIn('id',[11,12,13])->pluck('nombre_tipo_usu','id');
       
        $statususu=GenStatus::whereIn('id', [1, 2])->pluck('nombre_status','id');
        $preguntas=CatPreguntaSeg::all()->pluck('descripcion_preg','id');

        $AduanaSalida=GenAduanaSalida::where('bactivo',1)->whereIn('id', [1,4,7])->pluck('daduana','id');
        
        
        return view('userManagerResgCna.edit', compact('usu_edit','preguntas','titulo','descripcion','tipousu','statususu','AduanaSalida'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\GenUsuario  $genUsuario
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, GenUsuario $genUsuario, $id)
    {
        if (isset($request->cat_preguntas_seg_id)) {
            if (isset($request->pregunta_alter)) {

               $pregunta=$request['pregunta_alter'];
            }else{

               $pregunta='N/A';
            }
        }
      
        //dd($pregunta);
        $usu= GenUsuario::find($id);
        $usu->email=$request->email;
        $usu->email_seg=$request->email_seg;
        $usu->gen_tipo_usuario_id=$request->gen_tipo_usuario_id;
        $usu->gen_status_id=$request->gen_status_id;
        $usu->password=bcrypt($request->password);
        $usu->cat_preguntas_seg_id=$request->cat_preguntas_seg_id;
        $usu->pregunta_alter=$pregunta;
        $usu->respuesta_seg=$request->respuesta_seg;
        $usu->updated_at=date("Y-m-d H:i:s");
        $usu->save();
      
        DetUsersResguardo::updateOrCreate(
            ['gen_usuario_id'=>$id],
            [
                'gen_usuario_id' => $id,
                'nombre_funcionario' => $request->nombre_funcionario,
                'apellido_funcionario' => $request->apellido_funcionario,
                'cedula_funcionario' => $request->cedula_funcionario,
                'rango_funcionario' => $request->rango_funcionario,
                'unidad' => $request->unidad,
                'gen_aduana_salida_id' => $request->gen_aduana_salida_id,
                //'ubicacion' => $request->ubicacion,
                'tlf_funcionario' => $request->tlf_funcionario,
                
            ]

        );

        Alert::success('Registro Actualizado!', 'Usuario Actualizado.')->persistent("Ok");
        return redirect()->action('userManagerResgCnaController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\GenUsuario  $genUsuario
     * @return \Illuminate\Http\Response
     */
    public function destroy(GenUsuario $genUsuario)
    {
        //
    }
}
