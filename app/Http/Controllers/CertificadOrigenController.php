<?php

namespace App\Http\Controllers;
use Illuminate\Filesystem\Filesystem;
use App\Models\GenCertificado;
use App\Models\GenTipoCertificado;
use App\Models\DetUsuario;
use App\Models\DocumentosCertificado;
use App\Models\ImportadorCertificado;
use App\Models\DetProductosCertificado;
use App\Models\DetDeclaracionCertificado;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Auth;
use File;
use Session;

use App\Models\BandejaAnalisisCertificado;

use Laracasts\Flash\Flash;
use Alert;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;

class CertificadOrigenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()

    {
        $titulo= 'Certificado de Origen';
        $descripcion= 'Perfil del Exportador';

      /**Definiendo variable para consulta de certificado de origen,traerme todas**/

      $CertificadoOrigen=GenCertificado::where('gen_usuario_id',Auth::user()->id)->orderBy('id','DESC')->get();

      //dd($CertificadoOrigen);


       return view('CertificadoOrigen.index',compact('titulo','descripcion','CertificadoOrigen'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $titulo= 'Certificado de Origen';
        $descripcion= 'Perfil del Exportador';/* nsantos*/

        
        $tipo_certificado=GenTipoCertificado::all()->pluck('nombre_certificado','id');


         return view('CertificadoOrigen.create',compact('titulo','descripcion','tipo_certificado'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());
         $this->validate($request, [


                'file_1'                    => 'required|mimes:pdf,doc,docx,pptx,ppt|max:2000',
                'file_2'                    => 'required|mimes:pdf,doc,docx,pptx,ppt|max:2000',
                'file_3'                    => 'required|mimes:pdf,doc,docx,pptx,ppt|max:2000',
                'file_4'                    => 'required|mimes:pdf,doc,docx,pptx,ppt|max:2000',
                'file_5'                    => 'required|mimes:pdf,doc,docx,pptx,ppt|max:2000',
                'file_6'                    => '|mimes:pdf,doc,docx,pptx,ppt|max:2000',

                'gen_tipo_certificado_id'   => 'required',
                'pais_exportador'           => 'required',
                'descripcion_comercial'     => 'required_if:gen_tipo_certificado_id,9',
                'pais_importador'           => 'required_if:gen_tipo_certificado_id,1,2,3,5,6,7,8',
                'cantidad_segun_factura'    => 'required_if:gen_tipo_certificado_id,1,9',

                'numero_factura'            => 'required_if:gen_tipo_certificado_id,1,2,3,4,8,9',
                'f_fac_certificado'         => 'required_if:gen_tipo_certificado_id,1,2,3,8,9',
                'razon_social_productor'    => 'required_if:gen_tipo_certificado_id,3',
                'direccion_productor'       => 'required_if:gen_tipo_certificado_id,3',
                'rif_productor'             => 'required_if:gen_tipo_certificado_id,3',
                'rif_importador'            => 'required_if:gen_tipo_certificado_id,3',
                'fecha_emision_exportador'  => 'required_if:gen_tipo_certificado_id,1,2',
                'razon_social_importador'   => 'required_if:gen_tipo_certificado_id,1,2,3,8,9',
                'direccion_importador'      => 'required_if:gen_tipo_certificado_id,1,2,3,8,9',
                'pais_ciudad_importador'    => 'required_if:gen_tipo_certificado_id,1,9',
                'email_importador'          => 'required_if:gen_tipo_certificado_id,1',
                'telefono'                  => 'required_if:gen_tipo_certificado_id,1',
                'observaciones'             => 'required_if:gen_tipo_certificado_id,1',
                
        ]);
         
                   
        if(empty($request->gen_tipo_certificado_id)){

            //dd(1);
            Alert::warning('No es posible realizar su solicitud. Intente nuevamente.','Acción Denegada!')->persistent("OK");

            return redirect()->back()->withInput();
        }


         //Registrar en gen_certificado
        $certificado                           = new GenCertificado;
        $certificado->gen_tipo_certificado_id  = $request->gen_tipo_certificado_id;
        $certificado->num_certificado          = $this->generarRamdom();
        $certificado->gen_usuario_id           = Auth::user()->id;
        $certificado->pais_exportador          = $request->pais_exportador;
        $certificado->pais_importador          = $request->pais_importador;
        $certificado->pais_cuidad_exportador   = $request->pais_cuidad_exportador;
        $certificado->fecha_emision_exportador = $request->fecha_emision_exportador;
        $certificado->rif_productor            = (isset($request->rif_productor))?$request->rif_productor:'';
        $certificado->razon_social_productor   = (isset($request->razon_social_productor))?$request->razon_social_productor:'';
        $certificado->direccion_productor      = (isset($request->direccion_productor))?$request->direccion_productor:'';
        $certificado->gen_status_id            = 9;
        $certificado->observaciones            = $request->observaciones;
        $certificado->save();

        //importador    
        $imporCertific                              = new ImportadorCertificado;
        $imporCertific->gen_certificado_id          = $certificado->id;
        $imporCertific->razon_social_importador     = $request->razon_social_importador; 
        $imporCertific->direccion_importador        = $request->direccion_importador;
        $imporCertific->pais_ciudad_importador      = $request->pais_ciudad_importador;
        $imporCertific->email_importador            = $request->email_importador;
        $imporCertific->telefono                    = $request->telefono;
        $imporCertific->rif_importador              = (isset($request->rif_importador))?$request->rif_importador:'';
        $imporCertific->medio_transporte_importador = $request->medio_transporte_importador;
        $imporCertific->lugar_embarque_importador   = $request->lugar_embarque_importador;
        $imporCertific->save();


        if (!empty($request->num_orden)) {

              $cantidad = count($request->num_orden);
        }else{
             $cantidad = count($request->codigo_arancel);
             

        }

        //prod certificados
       // $cantidad = count($request->num_orden);
    
        
        for($i=0; $i <= $cantidad-1; $i++){
           
            if(isset($request->num_orden[$i]) OR isset($request->codigo_arancel[$i])){

            $prodCert                               =   new DetProductosCertificado;
            $prodCert->gen_certificado_id           = $certificado->id;
            $prodCert->num_orden                  = (isset($request->num_orden[$i]))?$request->num_orden[$i]:$i;
            $prodCert->codigo_arancel               = $request->codigo_arancel[$i];

           if(isset($request->descripcion_comercial[$i])){
                $prodCert->descripcion_comercial        = $request->descripcion_comercial[$i];
            }
            $prodCert->denominacion_mercancia       = $request->denominacion_mercancia[$i];
            $prodCert->unidad_fisica                = (isset($request->unidad_fisica[$i]))?$request->unidad_fisica[$i]:null;

            $prodCert->valor_fob                    = (isset($request->valor_fob[$i]))?$request->valor_fob[$i]:null;
            $prodCert->cantidad_segun_factura       = (isset($request->cantidad_segun_factura[$i]))?$request->cantidad_segun_factura[$i]:'';
            $prodCert->save();
            }
        }


/////////////////////////////////////////////////////////////////////////////////
       //dd($request->$cat_documentos_id);
        if ($request->gen_tipo_certificado_id != 8) {
            
             $cat_documentos_id = count($request->num_orden_declaracion);
        
                for($i=0; $i <= $cat_documentos_id-1; $i++){

                    $detDeclaracion                             = new DetDeclaracionCertificado;
                    $detDeclaracion->gen_certificado_id         = $certificado->id;
                    $detDeclaracion->num_orden_declaracion      = $request->num_orden_declaracion[$i];
                 
                    $detDeclaracion->normas_criterio            = $request->normas_criterio[$i];
                    $detDeclaracion->numero_factura             = $request->numero_factura;
                    $detDeclaracion->nombre_norma_origen        = $request->nombre_norma_origen;
                    $detDeclaracion->f_fac_certificado          = $request->f_fac_certificado;
                    $detDeclaracion->save();
                }
         }else{
                $detDeclaracion                             = new DetDeclaracionCertificado;
                $detDeclaracion->gen_certificado_id         = $certificado->id;
                $detDeclaracion->numero_factura             = $request->numero_factura;
                $detDeclaracion->f_fac_certificado          = $request->f_fac_certificado;
                $detDeclaracion->save();
           

        }


       
       

        //Guardar las imagenes
        $destino = 'img/certificadoOrigen/'.Auth::user()->detUsuario->cod_empresa;
        $destinoPrivado = public_path($destino);
        
        if (!file_exists($destinoPrivado)) {
            $filesystem = new Filesystem();
            $filesystem->makeDirectory($destinoPrivado, $mode = 0777, true, true); 
        }


        for( $i=1; $i <=6 ; $i++ ){

            $imagen = $request->file('file_'.$i);
            
            if (!empty($imagen)) {
                
               $nombre_imagen = $this->nameFile($imagen, $i);//$imagen->getClientOriginalName();
            
                $imagen->move($destinoPrivado, $nombre_imagen);
                $auxStr='file_'.$i;
                $$auxStr = $nombre_imagen;
               
            }
            

        }
        
            $doCertif                       = new DocumentosCertificado;
            $doCertif->gen_certificado_id   = $certificado->id;
            $doCertif->file_1               = $destino.'/'.$file_1;

            if (!empty($file_2)) {
                $doCertif->file_2 = $destino.'/'.$file_2;
            }
            if (!empty($file_3)) {
                $doCertif->file_3 = $destino.'/'.$file_3;
            }
            if (!empty($file_4)) {
                $doCertif->file_4 = $destino.'/'.$file_4;
            }
            if (!empty($file_5)) {
                $doCertif->file_5 = $destino.'/'.$file_5;
            }
            if (!empty($file_6)) {
                $doCertif->file_6 = $destino.'/'.$file_6;
            }
            
            $doCertif->bactivo=1;

            $doCertif->save();
                
            Alert::success('Se han guardado los documentos correspondientes!','Registrado Correctamente!')->persistent("Ok");

            return redirect()->action('CertificadOrigenController@index');

    }

    private function generarRamdom()
    {
        $user = Auth::user();
        $random = Str::random(4);
        $id  = $user->id;
        $rif = substr($user->detUsuario->rif, -2);
        $fecha = date('Ymd');
        $UltimatSolicitud=GenCertificado::where('gen_usuario_id',Auth::user()->id)->get()->last();
        $idUltimatSolicitud = !empty($UltimatSolicitud) ? $UltimatSolicitud->id : 0;
        return $id.'-'.$random.$fecha.$rif.$idUltimatSolicitud;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\GenCertificado  $genCertificado
     * @return \Illuminate\Http\Response
     */
    public function show(GenCertificado $genCertificado)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\GenCertificado  $genCertificado
     * @return \Illuminate\Http\Response
     */
    public function edit(GenCertificado $genCertificado, $id)
    {
        $titulo= 'Certificado de Origen';
        $descripcion= 'Perfil del Exportador';

        $nombre_norma_origen =[
         'Aladi'=>'Aladi',
         'Aladi-TP'=>'Aladi-TP',
         'Comunidad Andina de Naciones'=>'Comunidad Andina de Naciones'
        ];



        
        $certificado        =   GenCertificado::find($id);
        $import_cert        =   ImportadorCertificado::where('gen_certificado_id', $id)->first();
        $det_prod_cer       =   DetProductosCertificado::where('gen_certificado_id', $id)->get();
        $det_decla_cert     =   DetDeclaracionCertificado::where('gen_certificado_id', $id)->get();
        $tipo_certificado   =   GenTipoCertificado::all()->pluck('nombre_certificado','id');
        $doc_cert           =   DocumentosCertificado::where('gen_certificado_id', $id)->first();
        $det_usuario        =   DetUsuario::where('gen_usuario_id',Auth::user()->id)->where('bactivo',1)->first();
        //dd($det_decla_cert[0]->nombre_norma_origen);

        if($certificado->gen_tipo_certificado_id == 9){
            $view = 'CertificadoOrigen.edit_union_europea';////

        }elseif($certificado->gen_tipo_certificado_id == 8){
            $view = 'CertificadoOrigen.edit_turquia';

        }elseif($certificado->gen_tipo_certificado_id == 7){
            $view = 'CertificadoOrigen.edit_cuba';

        }elseif($certificado->gen_tipo_certificado_id == 6){
        $view = 'CertificadoOrigen.edit_peru';

        }elseif($certificado->gen_tipo_certificado_id == 5){
        $view = 'CertificadoOrigen.edit_mercosur';

        }elseif($certificado->gen_tipo_certificado_id == 4){
            $view = 'CertificadoOrigen.edit_aladi';
        }elseif($certificado->gen_tipo_certificado_id == 3){
            $view = 'CertificadoOrigen.edit_chile';
        }elseif($certificado->gen_tipo_certificado_id == 2){
            $view = 'CertificadoOrigen.edit_bolivia';
        }elseif($certificado->gen_tipo_certificado_id == 1){
            $view = 'CertificadoOrigen.edit_colombia';
        }

        return view($view, compact('certificado', 'import_cert', 'det_usuario', 'det_prod_cer', 'det_decla_cert', 'tipo_certificado', 'doc_cert','nombre_norma_origen'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\GenCertificado  $genCertificado
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, GenCertificado $genCertificado, $id)
    {   

        //dd($request->all());
        //Validaciones
        $this->validate($request, [

            'gen_tipo_certificado_id'   => 'required',
            'pais_exportador'           => 'required',
            'descripcion_comercial'     => 'required_if:gen_tipo_certificado_id,9',
            'pais_importador'           => 'required_if:gen_tipo_certificado_id,1,3,4,5,6,7,8',
            'cantidad_segun_factura'    => 'required_if:gen_tipo_certificado_id,1,9',
            'numero_factura'            => 'required_if:gen_tipo_certificado_id,1,2,3,4,8,9',
            'f_fac_certificado'         => 'required_if:gen_tipo_certificado_id,1,2,3,8,9',
            'razon_social_productor'    => 'required_if:gen_tipo_certificado_id,3',
            'direccion_productor'       => 'required_if:gen_tipo_certificado_id,3',
            'rif_productor'             => 'required_if:gen_tipo_certificado_id,3',
            'rif_importador'            => 'required_if:gen_tipo_certificado_id,3',
            'fecha_emision_exportador'  => 'required_if:gen_tipo_certificado_id,1,2',
            'razon_social_importador'   => 'required_if:gen_tipo_certificado_id,1,2,3,8,9',
            'direccion_importador'      => 'required_if:gen_tipo_certificado_id,1,2,3,8,9',
            'pais_ciudad_importador'    => 'required_if:gen_tipo_certificado_id,1,9',
            'email_importador'          => 'required_if:gen_tipo_certificado_id,1',
            'telefono'                  => 'required_if:gen_tipo_certificado_id,1',
            
    ]);
        
        

        $certificado                           =  GenCertificado::find($id);
        $certificado->pais_exportador          = $request->pais_exportador;
        $certificado->pais_importador          = $request->pais_importador;
        $certificado->gen_tipo_certificado_id  = $request->gen_tipo_certificado_id;
        $certificado->pais_cuidad_exportador   = $request->pais_cuidad_exportador;
        $certificado->fecha_emision_exportador = $request->fecha_emision_exportador;
        $certificado->rif_productor            = $request->rif_productor;
        $certificado->razon_social_productor   = $request->razon_social_productor;
        $certificado->direccion_productor      = $request->direccion_productor;
        $certificado->gen_status_id            = 18;
        $certificado->observacion_analista     = null;
        $certificado->observacion_coordinador  = null;
        $certificado->observaciones            = $request->observaciones;
        $certificado->save();
        
        //importador    
        $imporCertific                              = ImportadorCertificado::where('gen_certificado_id', $id)->first();
        $imporCertific->razon_social_importador     = $request->razon_social_importador; 
        $imporCertific->direccion_importador        = $request->direccion_importador;
        $imporCertific->pais_ciudad_importador      = $request->pais_ciudad_importador;
        $imporCertific->email_importador            = $request->email_importador;
        $imporCertific->telefono                    = $request->telefono;
        $imporCertific->rif_importador              = $request->rif_importador;
        $imporCertific->medio_transporte_importador = $request->medio_transporte_importador;
        $imporCertific->lugar_embarque_importador   = $request->lugar_embarque_importador;
        $imporCertific->save();


        if (!empty($request->num_orden)) {

              $cantidad = count($request->num_orden);
        }else{
             $cantidad = count($request->codigo_arancel);
             

        }


        //prod certificados
        //$cantidad = count($request->num_orden);
        //dd($request->num_orden);

       //cont_prod=count($request->num_orden);
        for($i=0;$i<=($cantidad-1);$i++){
            if(isset($request->num_orden[$i]) OR isset($request->codigo_arancel[$i])){
            
                DetProductosCertificado::updateOrCreate(
                ['id'=>(isset($request->id_productos[$i])) ? $request->id_productos[$i] : ""],
                ['num_orden'=>(isset($request->num_orden[$i])) ? $request->num_orden[$i] : '',
                 
                 'codigo_arancel'=>(isset($request->codigo_arancel[$i])) ? $request->codigo_arancel[$i] : '',
                 'descripcion_comercial'=>(isset($request->descripcion_comercial[$i])) ? $request->descripcion_comercial[$i] : '',
                 'denominacion_mercancia'=>(isset($request->denominacion_mercancia[$i])) ? $request->denominacion_mercancia[$i] : '',
                 'unidad_fisica'=>(isset($request->unidad_fisica[$i])) ? $request->unidad_fisica[$i] : '',
                 'valor_fob'=>(isset($request->valor_fob[$i])) ? $request->valor_fob[$i] : '',
                 'cantidad_segun_factura'=>(isset($request->cantidad_segun_factura[$i])) ? $request->cantidad_segun_factura[$i] : '',
                 'gen_certificado_id' => $certificado->id,
                 
                 ]

                );
            }

        }
        
        if ($request->gen_tipo_certificado_id != 8) {

            //Declaracion Certificado
            $cant_numer_ord = count($request->num_orden_declaracion);
            
                for($i=0; $i <= $cant_numer_ord-1; $i++){

                    DetDeclaracionCertificado::updateOrCreate(
                        ['id'=>(isset($request->id_criterios[$i])) ? $request->id_criterios[$i] : ""],
                        ['num_orden_declaracion'=>(isset($request->num_orden_declaracion[$i])) ? $request->num_orden_declaracion[$i] : '',
                         'normas_criterio'=>(isset($request->normas_criterio[$i])) ? $request->normas_criterio[$i] : '',
                         'numero_factura'=>(isset($request->numero_factura)) ? $request->numero_factura : '',
                         'nombre_norma_origen'=>(isset($request->nombre_norma_origen)) ? $request->nombre_norma_origen : '',
                         'f_fac_certificado'=>(isset($request->f_fac_certificado)) ? $request->f_fac_certificado : '',
                         'gen_certificado_id'=> $certificado->id,
                         
                         
                        ]
            
                        );
                    }
            }else{
                  DetDeclaracionCertificado::updateOrCreate(
                        [
                      
                         'numero_factura'=>(isset($request->numero_factura)) ? $request->numero_factura : '',
                        
                         'f_fac_certificado'=>(isset($request->f_fac_certificado)) ? $request->f_fac_certificado : '',
                         'gen_certificado_id'=> $certificado->id,
                         
                        ]
            
                        );
                    }

            
        //return redirect()->back();
        //Guardar las imagenes
        $destino = 'img/certificadoOrigen/'.Auth::user()->detUsuario->cod_empresa;
        $destinoPrivado = public_path($destino);
        
        if (!file_exists($destinoPrivado)) {
            $filesystem = new Filesystem();
            $filesystem->makeDirectory($destinoPrivado, $mode = 0777, true, true); 
        }


        for( $i=1; $i <=6 ; $i++ ){

            $imagen = $request->file('file_'.$i);
            
            if (!empty($imagen)) {
                
                $nombre_imagen = $this->nameFile($imagen, $i);
                
                $imagen->move($destinoPrivado, $nombre_imagen);
                $auxStr='file_'.$i;
                $$auxStr = $nombre_imagen;
               
            }
            

        }
        //dd($request->all());
        
            $doCertif=DocumentosCertificado::find($request->file_id);
          //  dd($file_1);
            if(!empty($file_1)){
                $auxFile1= $doCertif->file_1;
                $doCertif->file_1 = $destino.'/'.$file_1;
            }
            if(!empty($file_2)){
                $auxFile2= $doCertif->file_2;
                $doCertif->file_2 = $destino.'/'.$file_2;
            }
            if(!empty($file_3)){
                $auxFile3= $doCertif->file_3;
                $doCertif->file_3 = $destino.'/'.$file_3;
            }
            if(!empty($file_4)){
                $auxFile4= $doCertif->file_4;
                $doCertif->file_4 = $destino.'/'.$file_4;
            }
            if(!empty($file_5)){
                $auxFile5= $doCertif->file_5;
                $doCertif->file_5 = $destino.'/'.$file_5;
            }
            if(!empty($file_6)){
                $auxFile6= $doCertif->file_6;
                $doCertif->file_6 = $destino.'/'.$file_6;
            }
            
            if($doCertif->touch()){
                if(!empty($auxFile1)){
                    if(is_file('.'.$auxFile1))
                    unlink('.'.$auxFile1); //elimino el fichero
                }
                if(!empty($auxFile2)){
                    if(is_file('.'.$auxFile2))
                    unlink('.'.$auxFile2); //elimino el fichero
                }
                if(!empty($auxFile3)){
                    if(is_file('.'.$auxFile3))
                    unlink('.'.$auxFile3); //elimino el fichero
                }
                if(!empty($auxFile4)){
                    if(is_file('.'.$auxFile4))
                    unlink('.'.$auxFile4); //elimino el fichero
                }
                if(!empty($auxFile5)){
                    if(is_file('.'.$auxFile5))
                    unlink('.'.$auxFile5); //elimino el fichero
                }
                if(!empty($auxFile6)){
                    if(is_file('.'.$auxFile6))
                    unlink('.'.$auxFile6); //elimino el fichero
                }
        
            }else{
                if (!empty($file_1)) {
                    if(is_file('.'.$destino.'/'.$file_1))
                    unlink('.'.$destino.'/'.$file_1);
                }
                
                if (!empty($file_2)) {
                    if(is_file('.'.$destino.'/'.$file_2))
                    unlink('.'.$destino.'/'.$file_2);
                }
                
                if (!empty($file_3)) {
                    if(is_file('.'.$destino.'/'.$file_3))
                    unlink('.'.$destino.'/'.$file_3);
                }
        
                if (!empty($file_4)) {
                    if(is_file('.'.$destino.'/'.$file_4))
                    unlink('.'.$destino.'/'.$file_4);
                }
        
                if (!empty($file_5)) {
                    if(is_file('.'.$destino.'/'.$file_5))
                    unlink('.'.$destino.'/'.$file_5);
                }
        
                if (!empty($file_6)) {
                    if(is_file('.'.$destino.'/'.$file_6))
                    unlink('.'.$destino.'/'.$file_6);
                }
                
        
                Alert::warning('No es posible realizar su solicitud. Intente nuevamente.','Acción Denegada!')->persistent("OK");
                return redirect()->back()->withInput();
            }
                
            Alert::success('Se ha actualizado el registro correctamente!','Actualizado Correctamente!')->persistent("Ok");

            return redirect()->action('CertificadOrigenController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\GenCertificado  $genCertificado
     * @return \Illuminate\Http\Response
     */
    public function destroy(GenCertificado $genCertificado)
    {
        //
    }

     public function RedireccionarForm(Request $request)
    {
        //dd($request->all());
        
        $titulo= 'Certificado de Origen';
        $descripcion= 'Peril del Exportador';/* nsantos*/

         $nombre_norma_origen =[
         'Aladi'=>'Aladi',
         'Aladi-TP'=>'Aladi-TP',
         'Comunidad Andina de Naciones'=>'Comunidad Andina de Naciones'
        ];


        $det_usuario=DetUsuario::where('gen_usuario_id',Auth::user()->id)->where('bactivo',1)->first();

        if (!empty($request->gen_tipo_certificado_id)) {
          
            Session::put('gen_tipo_certificado_id',$request->gen_tipo_certificado_id);
        }

        
        $gen_tipo_certificado_id=$request->gen_tipo_certificado_id ? $request->gen_tipo_certificado_id : Session::get('gen_tipo_certificado_id') ;



        if ($gen_tipo_certificado_id == 1) {

            return view('CertificadoOrigen.create_colombia',compact('titulo','descripcion','gen_tipo_certificado_id','det_usuario'));
            
        }elseif ($gen_tipo_certificado_id == 2) {

            return view('CertificadoOrigen.create_bolivia',compact('titulo','descripcion','gen_tipo_certificado_id','det_usuario'));
            
        }elseif($gen_tipo_certificado_id == 3){
             
             return view('CertificadoOrigen.create_chile',compact('titulo','descripcion','gen_tipo_certificado_id','det_usuario'));

        }elseif($gen_tipo_certificado_id == 4){
             
             return view('CertificadoOrigen.create_aladi',compact('titulo','descripcion','gen_tipo_certificado_id','det_usuario','nombre_norma_origen'));

         }elseif($gen_tipo_certificado_id == 5){
             
             return view('CertificadoOrigen.create_mercosur',compact('titulo','descripcion','gen_tipo_certificado_id','det_usuario'));

         }elseif($gen_tipo_certificado_id == 6){
             
             return view('CertificadoOrigen.create_peru',compact('titulo','descripcion','gen_tipo_certificado_id','det_usuario'));


        }elseif($gen_tipo_certificado_id == 7){
             
            return view('CertificadoOrigen.create_cuba',compact('titulo','descripcion','gen_tipo_certificado_id','det_usuario','nombre_norma_origen'));

        }elseif($gen_tipo_certificado_id == 8){
             
           return view('CertificadoOrigen.create_turquia',compact('titulo','descripcion','gen_tipo_certificado_id','det_usuario'));

        }else {
             
             return view('CertificadoOrigen.create_union_europea',compact('titulo','descripcion','gen_tipo_certificado_id','det_usuario'));



        }
    
     
    }

    public function deleteProducto(Request $request)
    {
        

       $delete=DetProductosCertificado::find($_GET['id'])->delete();

      
       if($delete)
       {
       
          return "Producto Eliminado";
       }else
       {
         return "Producto No existe";
       }
    }

    public function deleteCriterio(Request $request)
    {
        $delete=DetDeclaracionCertificado::find($_GET['id'])->delete();

      
       if($delete)
       {
       
          return "Producto Eliminado";
       }else
       {
         return "Producto No existe";
       }
    }

    public function nameFile($imagen, $numero){

        $texto = preg_replace('([^A-Za-z0-9])', '', $imagen->getClientOriginalName());
        //dd($texto);
        $extension = pathinfo($imagen->getClientOriginalName(), PATHINFO_EXTENSION);
        return $texto.'_'.$numero.'.'.$extension;
    }



}

