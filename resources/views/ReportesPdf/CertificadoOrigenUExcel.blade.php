<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>UNION EUROPEA</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/style_Certificado.css">
</head>
<body>
    <div class="centrar espacio-superior">
        <table border="1" class="ancho-full">
            <tr>  
                <td class="ancho-medio top borde" colspan="10" style="width: 70%; height:100px;">
                    <div class="text-normal">1.Goods consigned from (exporter's business name, address, country)
                        <u> <b>{{$certificado->GenUsuario->DetUsuario->razon_social}}</b></u><br>
                        <u>{{$certificado->GenUsuario->DetUsuario->direccion}} {{$certificado->pais_exportador ? $certificado->pais_exportador : ''}}</u>
                    </div>
                </td>
                <td class="ancho-medio top borde" colspan="10" style="width: 70%; height:100px;">
                    <div class="text-normal">Referencia N°
                        <div class="justify text-9">GENERALIZED SYSTEM OF PREFERENCES <br>CERTIFICATE OF ORIGIN <br>(Combined declaration and certificate) <br><b>FORMA A</b></div>
                        <div class="text-9"><p>Issued in <b>República Bolivariana de Venezuela</b><br></div>
                        </p>
                    </div>
                </td>
                <br>
            </tr>
        </table>
    </div>
    <div class="centrar espacio-superior">
        <table border="1" class="ancho-full">
            <tr>
                <td class="ancho-medio top borde" colspan="10" style="width: 70%; height:100px;">
                    <div class="text-normal">2.Goods consigned to (consignee's name, address, country)
                        <u> {{$importadorCertificado->razon_social_importador ? $importadorCertificado->razon_social_importador : ''}}</u><br>
                        <u>{{$importadorCertificado->direccion_importador ? $importadorCertificado->direccion_importador : ''}} </u><br>
                        <u>{{$importadorCertificado->pais_ciudad_importador ? $importadorCertificado->pais_ciudad_importador : ''}}</u><br>
                    </div>
                </td>
                <td class="ancho-medio top borde" colspan="10" style="width: 70%; height:100px;"></td>
            </tr>
        </table>
    </div>
    <div class="centrar espacio-superior">
        <table border="1" class="ancho-full">
            <tr>
                <td class="ancho-medio top borde" colspan="10" style="width: 70%; height:100px;">
                    <div class="text-normal">3.Means of transport and route (as far as known) <br>
                        <u>{{$certificado->direccion_productor ? $certificado->direccion_productor : ''}}  </u>
                        <u>{{$importadorCertificado->lugar_embarque_importador ? $importadorCertificado->lugar_embarque_importador : ''}}</u>
                    </div>
                </td>
                <td class="ancho-medio top borde" colspan="10" style="width: 70%; height:100px;">
                    <div class="text-normal">4.For official use 
                        <h4 class="justify">ISSUED RETROSPECTIVELY</h4><br>
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <div class="centrar espacio-superior">
        <table class="ancho-full borde table table2">
            <tr class="text-small">
                <td class="top borde" colspan="1" style="width: 25%; height:100px;">5.Item Num <br>ber</td>
                <td class="top borde" colspan="1" style="width: 25%; height:100px;">6.Marks and number of <br>packages</td>
                <td class="top borde" colspan="3" style="width: 25%; height:100px;">7.Numbers and kind of packages descripcion of goods</td>
                <td class="top borde" colspan="5" style="width: 25%; height:100px;">8.Origin criterion <br>(see notes overlaf)</td>
                <td class="top borde" colspan="5" style="width: 25%; height:100px;">9.Gross weight or ther quantity</td>
                <td class="top borde" colspan="5" style="width: 25%; height:100px;">10.Number and <br>date of invoices</td>
            </tr>

            @foreach ($detalleProduct as $key => $detalleP)
            <tr class="text-small">
                <td class="top borde" colspan="1" style="width: 25%; height:100px;">
                    {{$detalleP->num_orden ? $detalleP->num_orden : ''}} <br><br><br>
                </td><!--num orden-->
                <td class="top borde" colspan="1" style="width: 25%; height:100px;">
                    {{$detalleP->cantidad_segun_factura ? $detalleP->cantidad_segun_factura : ''}} <br><br><br>
                </td><!--cantidad segun factura-->
                <td class="top borde" colspan="3" style="width: 25%; height:100px;">
                    {{$detalleP->descripcion_comercial ? $detalleP->descripcion_comercial : ''}} <br><br><br>
                </td><!--Descripcion comercial-->
                @endforeach

                <td class="top borde" colspan="1" style="width: 25%; height:100px;">
                    <table><!-- normas -->
                        @foreach ($detdeclaracionCert as $key => $detalleCert)
                        <tr>
                            <td colspan="5" style="width: 25%; height:100px;" rowspan="1">{{$detalleCert->normas_criterio ? $detalleCert->normas_criterio : ''}} <br> </td><!--normas criterios-->
                            <td class="top" colspan="5" style="width: 25%; height:100px;"></td>
                        </tr>
                        @endforeach
                    </table>  
                </td>

                <td class="top borde" colspan="5">
                    <table>
                        @foreach ($detalleProduct as $key => $detalleP2)
                        <tr class="text-center">
                            <td class="text-normal text-justify" style="width: 70%; height:100px;" colspan="5">{{$detalleP2->unidad_fisica ? $detalleP2->unidad_fisica : ''}} <br> <br>
                            </td>
                            <td class="top" colspan="5" style="width: 70%; height:100px;"></td>
                        </tr>
                        @endforeach
                    </table>
                </td><!--Unidad fisica-->

                <td class="top borde text-normal" colspan="5" style="width: 70%; height:100px;">
                    {{$detdeclaracionCert[0]->numero_factura ? $detdeclaracionCert[0]->numero_factura : ''}} {{$detdeclaracionCert[0]->f_fac_certificado ? $detdeclaracionCert[0]->f_fac_certificado : ''}}<!--Numero y fecha de factura-->                        
                </td>
            </tr>
        </table>
    </div>
    <div class="centrar espacio-superior">
        <table border="1" class="ancho-full borde">
            <tr>
                <td class="ancho-medio centrar top borde" colspan="10" style="width: 50%; height:400px;">
                    <div class="text-normal">11.Certification</div>
                    <div class="text-9 justify"><p>It is hereby certified, on the basis of control carried out, that the declaration by the exporter is correct</p><br><br><br><br><br><br><br><br><br><br><br><br>
                        .......................................................................................................................................<br><br><br><br>
                    Place and date, signature and stamp of certifying authority</div>
                </td>
                <td class="ancho-medio centrar top borde" colspan="10" style="width: 50%; height:400px;">
                    <div class="text-normal">12.Declaration by the exporter </div>
                    <div class="text-9"><br><p>The undersigned hereby declares that the above details and statements are correct; that all the goods were</p><br>
                        <p>produced in   República Bolivariana de Venezuela</p>
                        ........................................................................................................
                        <div class="centrar text-9">(country)</div><br>
                        <div class="justify text-9">and that they comply with the origin requirements specified for those goods in the generalized system of preferences for goods exported to</div><br>
                        .........................................................................................................
                        <div class="centrar text-9">(importing country)</div><br>
                        ..................................................................................................<br>
                        Place and date, signature and stamp of certifying authority
                    </td>
                </tr>
            </table>
        </div>
    </body>
    </html>