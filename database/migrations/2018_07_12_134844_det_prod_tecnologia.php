<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DetProdTecnologia extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('det_prod_tecnologia',function (Blueprint $table){

          $table->engine = 'InnoDB';
          $table->increments('id');
          $table->unsignedInteger('gen_factura_id')->index()->nullable();
          $table->foreign('gen_factura_id')->references('id')->on('gen_factura');
          $table->string('codigo_arancel')->default('0000.00.00')->nullable();
          $table->string('descripcion')->nullable();
          $table->double('cantidad')->nullable();
          $table->unsignedInteger('unidad_medida_id')->index()->nullable();
          $table->double('precio')->nullable();
          $table->double('valor_fob')->nullable();
          $table->boolean('bactivo')->default(1);
          $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('det_prod_tecnologia');
    }
}
