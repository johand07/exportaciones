@extends('templates/layoutlte_analista_cvc')
@section('content')
<div class="content" style="background-color: #fff">
    <div class="panel-group" style="background-color: #fff;">
      <div class="panel-primary">
        <div class="panel-heading"><h3>SOLICITUDES CVC</h3> </div>
        <div class="panel-body">
            <div class="row">
              <div class="col-md-12">
                <div class="row">
                  <div class="col-md-8">
                     <h4 class="text-info">Bandeja de Solicitudes CVC:</h4><hr>   
                  </div>
                  <div class="col-md-2">
                  </div>
                  <div class="col-md-2">
                    <a class="btn btn-primary" href="{{route('ListaAnalistCvc.index')}}">
                      <span class="glyphicon glyphicon-refresh" title="Agregar" aria-hidden="true"></span>
                        Actualizar
                    </a>
                  </div>
                </div>
                <table id="listaCvc" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                      <tr>
                        <th class="text-center">#</th>
                        <th class="text-center">Nro de Solicitud</th>
                        <th class="text-center">Rif</th>
                        <th class="text-center">Razón Social</th>
                        <th class="text-center">Estatus</th>
                        <th class="text-center">Fecha Solicitud</th>
                        <th class="text-center">Acciones</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($solicitud_cvc as $key=> $solicitud)
                        <tr>
                          <td class="text-primary"><b>{{$key+1}}</b></td>
                          <td class="text-center">{{$solicitud->num_sol_cvc}}</td>
                          <td class="text-center">{{$solicitud->DetUsuario->rif}}</td>
                          <td class="text-center">{{$solicitud->DetUsuario->razon_social}}</td>
                          <td class="text-center">{{$solicitud->estatus->nombre_status}}</td>
                          <td class="text-center">{{$solicitud->created_at}}</td>

                          <td>
                            <!--Atender solicitud-->
                          @if($solicitud->gen_status_id==9)
                            <a href="{{ route('ListaAnalistCvc.edit',$solicitud->id)}}" class="btn btn-sm btn-danger" id="atender"><i class="glyphicon glyphicon-user"></i><b> Atender</b></a>
                          @endif

                          @if($solicitud->gen_status_id==10)
                            <a href="{{ route('ListaAnalistCvc.edit',$solicitud->id)}}" class="btn btn-sm btn-warning" id="atender"><i class="glyphicon glyphicon-user"></i><b> Continuar</b></a>
                          @endif

                          <!--Corregida por el Usuario-->
                          @if($solicitud->gen_status_id==18)
                            <a href="{{ route('ListaAnalistCvc.edit',$solicitud->id)}}" class="btn btn-sm btn-primary" id="atender"><i class="glyphicon glyphicon-user"></i><b>Corregida</b></a>
                          @endif

                          @if($solicitud->gen_status_id==12)
                            <a href="{{action('PdfController@CertificadoDemandaInterna',array('id'=>$solicitud->id))}}" target="_blank" class="glyphicon glyphicon-file btn btn-success btn-sm" title="Certificado Demanda Interna" aria-hidden="true"><b>Certificado</b></a>

                            <a href="{{action('PdfController@CertificadoDemandaInternaSol',array('id'=>$solicitud->id))}}" target="_blank" class="text-center btn btn-sm btn-success list-inline"><i class="glyphicon glyphicon-download"></i><b>Descargar PDF</b></a>
                          @endif
                          </td>
                      </tr>
                    @endforeach
                    </tbody> 
                </table>
              </div>
            </div>
        </div>
      </div>
    </div>
</div>
@stop