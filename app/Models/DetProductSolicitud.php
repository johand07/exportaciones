<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DetProductSolicitud extends Model
{
    protected $table='det_producto_solicitud';

    protected $fillable =[

     'id',
     'gen_solicitud_id',
     'det_prod_factura_id',
     'cantidad',
     'disponibles',
     'monto',
     'bactivo',
    ];

   public function solicitud(){
     
     return $this->hasOne('App\Models\GenSolicitud','id','gen_solicitud_id');
   }


   public function detproductfac(){
     
     return $this->hasOne('App\Models\DetProdFactura','id','det_prod_factura_id');
   }


}
