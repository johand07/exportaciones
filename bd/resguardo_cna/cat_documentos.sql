INSERT INTO `cat_documentos` (`id`, `gen_ente_id`, `nombre_documento`, `bactivo`, `created_at`, `updated_at`) VALUES (NULL, '3', 'Carta de inscripción ante la Aduana Marítima de la Guaira (SENIAT) - Solo para Agentes Aduanales.', '1', '2022-02-18 00:00:00', '2022-02-18 00:00:00');
INSERT INTO `cat_documentos` (`id`, `gen_ente_id`, `nombre_documento`, `bactivo`, `created_at`, `updated_at`) VALUES (NULL, '3', 'Registro Mercantil: Primera y última asamblea y/o asambleas extraordinarias donde se haya modificado el objeto de la empresa, su domicilio, accionistas o representes legales', '1', '2022-02-18 00:00:00', '2022-02-18 00:00:00');
INSERT INTO `cat_documentos` (`id`, `gen_ente_id`, `nombre_documento`, `bactivo`, `created_at`, `updated_at`) VALUES (NULL, '3', 'Registro de información Fiscal (Rif) de la empresa y del representante legal.', '1', '2022-02-18 00:00:00', '2022-02-18 00:00:00');

INSERT INTO `cat_documentos` (`id`, `gen_ente_id`, `nombre_documento`, `bactivo`, `created_at`, `updated_at`) VALUES (NULL, '3', 'Cedula de identidad del representante legal de la empresa.', '1', '2022-02-18 00:00:00', '2022-02-18 00:00:00');

INSERT INTO `cat_documentos` (`id`, `gen_ente_id`, `nombre_documento`, `bactivo`, `created_at`, `updated_at`) VALUES (NULL, '3', 'Fotografía tamaño postal del representante legal de la empresa (Debe estar impresa en papel fotográfico).', '1', '2022-02-18 00:00:00', '2022-02-18 00:00:00');

INSERT INTO `cat_documentos` (`id`, `gen_ente_id`, `nombre_documento`, `bactivo`, `created_at`, `updated_at`) VALUES (NULL, '3', 'Constancia de residencia Original del representante legal de la empresa (CNE, Consejo Comunal O Junta de Condominio).', '1', '2022-02-18 00:00:00', '2022-02-18 00:00:00');

INSERT INTO `cat_documentos` (`id`, `gen_ente_id`, `nombre_documento`, `bactivo`, `created_at`, `updated_at`) VALUES (NULL, '3', 'Fotografías de la fachada de la empresa, desde un plano general hasta un plano especifico (Mínimo 06 fotografías y deben estar impresas en papel fotográfico).', '1', '2022-02-18 00:00:00', '2022-02-18 00:00:00');


INSERT INTO `cat_documentos` (`id`, `gen_ente_id`, `nombre_documento`, `bactivo`, `created_at`, `updated_at`) VALUES (NULL, '3', 'Original del recibo de un servicio público de la empresa (Agua, Electricidad, Teléfono, otros)', '1', '2022-02-18 00:00:00', '2022-02-18 00:00:00');

INSERT INTO `cat_documentos` (`id`, `gen_ente_id`, `nombre_documento`, `bactivo`, `created_at`, `updated_at`) VALUES (NULL, '3', 'Contrato de arrendamiento del local o Documento de Propiedad.', '1', '2022-02-18 00:00:00', '2022-02-18 00:00:00');


INSERT INTO `cat_documentos` (`id`, `gen_ente_id`, `nombre_documento`, `bactivo`, `created_at`, `updated_at`) VALUES (NULL, '3', 'Ultima declaración del ISLR.', '1', '2022-02-18 00:00:00', '2022-02-18 00:00:00');

INSERT INTO `cat_documentos` (`id`, `gen_ente_id`, `nombre_documento`, `bactivo`, `created_at`, `updated_at`) VALUES (NULL, '3', 'Patente Municipal.', '1', '2022-02-18 00:00:00', '2022-02-18 00:00:00');

INSERT INTO `cat_documentos` (`id`, `gen_ente_id`, `nombre_documento`, `bactivo`, `created_at`, `updated_at`) VALUES (NULL, '3', 'Permiso de Bomberos vigente.', '1', '2022-02-18 00:00:00', '2022-02-18 00:00:00');

INSERT INTO `cat_documentos` (`id`, `gen_ente_id`, `nombre_documento`, `bactivo`, `created_at`, `updated_at`) VALUES (NULL, '3', ' Relación completa de la nómina de Trabajadores, indicando nombres, apellidos, cedula de identidad y cargo desempeñado (Debe incluir empleados directos, obreros y contratados).', '1', '2022-02-18 00:00:00', '2022-02-18 00:00:00');

INSERT INTO `cat_documentos` (`id`, `gen_ente_id`, `nombre_documento`, `bactivo`, `created_at`, `updated_at`) VALUES (NULL, '3', 'Tres (03) referencias personales del representante legal (Debe incluir, copia de la cedula de identidad, número telefónico, dirección domiciliaria y estampa de las huellas dactilares (ambos pulgares) de quien refiere). Deben ser originales.', '1', '2022-02-18 00:00:00', '2022-02-18 00:00:00');

INSERT INTO `cat_documentos` (`id`, `gen_ente_id`, `nombre_documento`, `bactivo`, `created_at`, `updated_at`) VALUES (NULL, '3', 'Oficio de remisión de todos los requisitos (dos originales).', '1', '2022-02-18 00:00:00', '2022-02-18 00:00:00');

INSERT INTO `cat_documentos` (`id`, `gen_ente_id`, `nombre_documento`, `bactivo`, `created_at`, `updated_at`) VALUES (NULL, '3', 'Oficio de declaración de autenticidad de todos los requisitos consignados (Firmado, sellado y con huelas dactilares). Original.', '1', '2022-02-18 00:00:00', '2022-02-18 00:00:00');

INSERT INTO `cat_documentos` (`id`, `gen_ente_id`, `nombre_documento`, `bactivo`, `created_at`, `updated_at`) VALUES (NULL, '3', 'Reseña fotográfica de la mercancía a Exportador (NOTA: En caso que las agencias navieras, agencias aduanales o empresas exportadoras, interrumpan las operaciones de exportación que constantemente realizan, este registro tendrá una vigencia de Seis meses a partir de la última exportación realizada)', '1', '2022-02-18 00:00:00', '2022-02-18 00:00:00');

