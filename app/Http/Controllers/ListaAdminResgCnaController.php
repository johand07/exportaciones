<?php

namespace App\Http\Controllers;

use App\Models\GenUsuario;
use App\Models\DetUsuario;
use App\Models\GenSolResguardoAduanero;
use App\Models\GenDocResguardo;
use App\Models\CatPreguntaSeg;
use App\Models\GenTipoUsuario;
use App\Models\GenStatus;
use App\Models\DetUsersResguardo;
use App\Models\BandejaAnalistaResguardo;
use App\Models\BandejaAnalistaCna;
use Illuminate\Support\Facades\DB;
use Session;
use Auth;
use Alert;
use File;
use Illuminate\Http\Request;

class ListaAdminResgCnaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $detUser= DetUsersResguardo::where('gen_usuario_id',Auth::user()->id )->first();
        if(!empty($detUser)){
            $solicitudes=GenSolResguardoAduanero::where('bactivo',1)->where('gen_aduana_salida_id', $detUser->gen_aduana_salida_id)->with('rAnalistaCna')->get();
        } else {
            Alert::warning('Este usuario no tiene aduana asociada')->persistent("OK");
            
            return redirect()->action('AdministradorResguardoCnaController@index'); 
    
        }
        // $solicitudes = GenSolResguardoAduanero::where('bactivo', 1)->get();
        return view('ListaAdminResgCna.index', compact('solicitudes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\GenSolResguardoAduanero  $genSolResguardoAduanero
     * @return \Illuminate\Http\Response
     */
    public function show(GenSolResguardoAduanero $genSolResguardoAduanero)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\GenSolResguardoAduanero  $genSolResguardoAduanero
     * @return \Illuminate\Http\Response
     */
    public function edit(GenSolResguardoAduanero $genSolResguardoAduanero, $id)
    {
        $titulo= 'Verificación de solicitud';
        $descripcion= 'Perfil del Administrador';


        $solicitud_cna=GenSolResguardoAduanero::find($id);
        /*$solicitud_cna->status_cna=21;
        $solicitud_cna->fstatus_cna=date('Y-m-d');
        $solicitud_cna->save();

        $bandejaGet= BandejaAnalistaCna::where('gen_sol_resguardo_aduanero_id',$id)->first();
        if(empty($bandejaGet)){
            $bandejaCna= new BandejaAnalistaCna;
            $bandejaCna->gen_sol_resguardo_aduanero_id=$id;
            $bandejaCna->gen_usuario_id=Auth::user()->id;
            $bandejaCna->gen_status_id=21;
            $bandejaCna->fstatus=date('Y-m-d');
            $bandejaCna->save();
        }*/
        


        $estado_observacion=GenStatus::whereIn('id', [11,15,23,25])->pluck('nombre_status','id');

        $det_usuario=DetUsuario::where('gen_usuario_id',$solicitud_cna->gen_usuario_id)->first();

        $docResguardoCna=GenDocResguardo::where('gen_sol_resguardo_aduanero_id',$solicitud_cna->id)->first();

        //dd($docResguardoCna);

        //extension de los docuemntos//

         if(!empty($docResguardoCna['file_1'])){
             $exten_file_1=explode(".", $docResguardoCna['file_1']);
            $extencion_file_1 = $exten_file_1[1];
        } else {
            $extencion_file_1 = '';
        }

        if (!empty($docResguardoCna['file_2'])) {
            $exten_file_2=explode(".", $docResguardoCna['file_2']);
            $extencion_file_2 = $exten_file_2[1];
        }else {
            $extencion_file_2 = '';
        }

        if (!empty($docResguardoCna['file_3'])) {
            $exten_file_3=explode(".", $docResguardoCna['file_3']);
            $extencion_file_3 = $exten_file_3[1];
        }else {
            $extencion_file_3 = '';
        }

        if (!empty($docResguardoCna['file_4'])) {
            $exten_file_4=explode(".", $docResguardoCna['file_4']);
            $extencion_file_4 = $exten_file_4[1];
        }else {
            $extencion_file_4 = '';
        }


        if (!empty($docResguardoCna['file_5'])) {
            $exten_file_5=explode(".", $docResguardoCna['file_5']);
            $extencion_file_5 = $exten_file_5[1];
        }else {
            $extencion_file_5 = '';
        }


        if (!empty($docResguardoCna['file_6'])) {
            $exten_file_6=explode(".", $docResguardoCna['file_6']);
            $extencion_file_6 = $exten_file_5[1];
        }else {
            $extencion_file_6 = '';
        }

        if (!empty($docResguardoCna['file_7'])) {
            $exten_file_7=explode(".", $docResguardoCna['file_7']);
            $extencion_file_7 = $exten_file_7[1];
        }else {
            $extencion_file_7 = '';
        }

        if (!empty($docResguardoCna['file_8'])) {
            $exten_file_8=explode(".", $docResguardoCna['file_8']);
            $extencion_file_8 = $exten_file_8[1];
        }else {
            $extencion_file_8 = '';
        }

        if (!empty($docResguardoCna['file_9'])) {
            $exten_file_9=explode(".", $docResguardoCna['file_9']);
            $extencion_file_9 = $exten_file_9[1];
        }else {
            $extencion_file_9 = '';
        }

        if (!empty($docResguardoCna['file_10'])) {
            $exten_file_10=explode(".", $docResguardoCna['file_10']);
            $extencion_file_10 = $exten_file_10[1];
        }else {
            $extencion_file_10 = '';
        }

        if (!empty($docResguardoCna['file_11'])) {
            $exten_file_11=explode(".", $docResguardoCna['file_11']);
            $extencion_file_11 = $exten_file_11[1];
        }else {
            $extencion_file_11 = '';
        }

        if (!empty($docResguardoCna['file_12'])) {
            $exten_file_12=explode(".", $docResguardoCna['file_12']);
            $extencion_file_12 = $exten_file_12[1];
        }else {
            $extencion_file_12 = '';
        }

         if (!empty($docResguardoCna['file_13'])) {
            $exten_file_13=explode(".", $docResguardoCna['file_13']);
            $extencion_file_13 = $exten_file_13[1];
        }else {
            $extencion_file_13 = '';
        }

        if (!empty($docResguardoCna['file_14'])) {
            $exten_file_14=explode(".", $docResguardoCna['file_14']);
            $extencion_file_14 = $exten_file_14[1];
        }else {
            $extencion_file_14 = '';
        }

         if (!empty($docResguardoCna['file_15'])) {
            $exten_file_15=explode(".", $docResguardoCna['file_15']);
            $extencion_file_15 = $exten_file_15[1];
        }else {
            $extencion_file_15 = '';
        }

        if (!empty($docResguardoCna['file_16'])) {
            $exten_file_16=explode(".", $docResguardoCna['file_16']);
            $extencion_file_16 = $exten_file_16[1];
        }else {
            $extencion_file_16 = '';
        }

        if (!empty($docResguardoCna['file_17'])) {
            $exten_file_17=explode(".", $docResguardoCna['file_17']);
            $extencion_file_17 = $exten_file_17[1];
        }else {
            $extencion_file_17 = '';
        }

         if (!empty($docResguardoCna['file_18'])) {
            $exten_file_18=explode(".", $docResguardoCna['file_18']);
            $extencion_file_18 = $exten_file_18[1];
        }else {
            $extencion_file_18 = '';
        }

         if (!empty($docResguardoCna['file_19'])) {
            $exten_file_19=explode(".", $docResguardoCna['file_19']);
            $extencion_file_19 = $exten_file_19[1];
        }else {
            $extencion_file_19 = '';
        }

        if (!empty($docResguardoCna['file_20'])) {
            $exten_file_20=explode(".", $docResguardoCna['file_20']);
            $extencion_file_20 = $exten_file_20[1];
        }else {
            $extencion_file_20 = '';
        }

         if (!empty($docResguardoCna['file_21'])) {
            $exten_file_21=explode(".", $docResguardoCna['file_21']);
            $extencion_file_21 = $exten_file_21[1];
        }else {
            $extencion_file_21 = '';
        }

         if (!empty($docResguardoCna['file_22'])) {
            $exten_file_22=explode(".", $docResguardoCna['file_22']);
            $extencion_file_22 = $exten_file_22[1];
        }else {
            $extencion_file_22 = '';
        }

        if (!empty($docResguardoCna['file_23'])) {
            $exten_file_23=explode(".", $docResguardoCna['file_23']);
            $extencion_file_23 = $exten_file_23[1];
        }else {
            $extencion_file_23 = '';
        }

        if (!empty($docResguardoCna['file_24'])) {
            $exten_file_24=explode(".", $docResguardoCna['file_24']);
            $extencion_file_24 = $exten_file_24[1];
        }else {
            $extencion_file_24 = '';
        }


       return view('ListaAdminResgCna.edit',compact('solicitud_cna','titulo','descripcion','det_usuario','estado_observacion','docResguardoCna','extencion_file_1','extencion_file_2','extencion_file_3','extencion_file_4','extencion_file_5','extencion_file_6','extencion_file_7','extencion_file_8','extencion_file_9','extencion_file_10','extencion_file_11','extencion_file_12','extencion_file_13','extencion_file_14','extencion_file_15','extencion_file_16','extencion_file_17','extencion_file_18','extencion_file_19','extencion_file_20','extencion_file_21','extencion_file_22','extencion_file_23','extencion_file_24'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\GenSolResguardoAduanero  $genSolResguardoAduanero
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, GenSolResguardoAduanero $genSolResguardoAduanero)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\GenSolResguardoAduanero  $genSolResguardoAduanero
     * @return \Illuminate\Http\Response
     */
    public function destroy(GenSolResguardoAduanero $genSolResguardoAduanero)
    {
        //
    }
}
