<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddGenAduanaSalidaIdToResguardo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('gen_sol_resguardo_aduanero', function (Blueprint $table) {
            
            $table->unsignedInteger('gen_aduana_salida_id')->index()->after('ubicacion_georeferencial')->nullable();
            $table->foreign('gen_aduana_salida_id')->references('id')->on('gen_aduana_salida');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('gen_sol_resguardo_aduanero', function (Blueprint $table) {
            
            $table->dropColumn('gen_aduana_salida_id');
           
        
       });
    }
}
