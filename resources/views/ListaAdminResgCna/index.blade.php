@extends('templates/layoutlte_admin_resgu_cna')
@section('content')
<div class="content" style="background-color: #fff">
    <div class="panel-group" style="background-color: #fff;">
      <div class="panel-primary">
        <div class="panel-heading"><h3>Solicitud CNA</h3> </div>
        <div class="panel-body">
            <div class="row">
              <div class="col-md-12">
                <div class="row">
                  <div class="col-md-8">
                     <h4 class="text-info">Bandeja de Solicitudes:</h4><hr>   
                  </div>
                  <div class="col-md-2">
                  </div>
                  <div class="col-md-2">
                    <a class="btn btn-primary" href="{{route('ListaAnalistCna.index')}}">
                      <span class="glyphicon glyphicon-refresh" title="Agregar" aria-hidden="true"></span>
                        Actualizar
                    </a>
                  </div>
                </div>
                <table id="listaCna" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                      <tr>
                        <th class="text-center">N° Solicitud</th>
                        <th class="text-center">Código Empresa</th>
                        <th class="text-center">Razón Social</th>
                        <th class="text-center">Rif Empresa</th>
                        <th class="text-center">Estatus</th>
                        <th class="text-center">Analista de Resguardo</th>
                        <th class="text-center">Analista CNA</th>
                        <th class="text-center">Acciones</th>
                      </tr>
                    </thead>
                    <tbody>
                    @foreach($solicitudes as $key=> $sol_cna)
                    <tr>
                      <td class="text-primary"><b>{{$sol_cna->num_sol_resguardo}}</b></td>
                      <td class="text-center">{{$sol_cna->DetUsuario->cod_empresa}}</td>
                      <td class="text-center">{{$sol_cna->DetUsuario->razon_social}}</td>
                      <td class="text-center">{{$sol_cna->DetUsuario->rif}}</td>
                      <td class="text-center">
                      @if($sol_cna->status_resguardo==11 ||$sol_cna->status_cna==11)
                        Con Observación!
                      @elseif($sol_cna->status_resguardo==15 ||$sol_cna->status_cna==15)
                        Doc Imcompletos
                      @elseif($sol_cna->status_resguardo==22 ||$sol_cna->status_cna==23)
                        Aprobada
                      @elseif($sol_cna->status_resguardo==24 ||$sol_cna->status_cna==25)
                        Negada
                      @endif
                      </td>
                      <td class="text-center">{{$sol_cna->rAnalistaResguardo ? $sol_cna->rAnalistaResguardo->rGenUsuario->email : 'Sin atender'}}</td>
                      <td class="text-center">{{$sol_cna->rAnalistaCna ? $sol_cna->rAnalistaCna->rGenUsuario->email : 'Sin atender'}}</td>
                      <td class="text-center">
                      
                      @if(($sol_cna->status_cna==23 && $sol_cna->aprobada_cna==1 )||($sol_cna->sol_cna==22 && $sol_cna->aprobada_resguardo==1 ))
                        <a href="{{action('PdfController@CertificadoVuceCna',array('id'=>$sol_cna->id))}}" class="glyphicon glyphicon-file btn btn-warning btn-sm" target="_blank"  aria-hidden="true" title="Certificado CNA"></a>
                        <a href="{{ route('CertificadoReguardoAduanero.pdf',['cerId'=>$sol_cna->id])}}" class="glyphicon glyphicon-file btn btn-success btn-sm" title="Certificado Resguardo" aria-hidden="true"></a>
                        <a href="{{ route('ListaAdminResgCna.edit',$sol_cna->id)}}" class="btn btn-sm btn-primary" title="Ver" id="negada"><i class="glyphicon glyphicon-eye-open"></i><b>Ver</b></a>
                      @else
                        <a href="{{ route('ListaAdminResgCna.edit',$sol_cna->id)}}" class="btn btn-sm btn-warning" title="Ver" id="negada"><i class="glyphicon glyphicon-eye-open"></i><b>Ver</b></a>
                      @endif
                        

                    
                      
                  
                  <!--a href="{{ route('ListaAnalistCna.edit',$sol_cna->id)}}" class="btn btn-sm btn-danger" id="negada"><i class="glyphicon glyphicon-user"></i><b>Documentos Incompletos</b></a-->

                  </td>
                </tr>
                @endforeach 
                    </tbody> 
                </table>
              </div>
            </div>
        </div>
      </div>
    </div>
</div>
@stop