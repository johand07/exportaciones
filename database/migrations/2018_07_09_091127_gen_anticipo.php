<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GenAnticipo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gen_anticipo', function (Blueprint $table) {
            $table->engine = 'InnoDB';//Crear Relaciones
            $table->increments('id');//Lo crea por defecto

            $table->unsignedInteger('gen_usuario_id')->index()->nullable();
            $table->foreign('gen_usuario_id')->references('id')->on('gen_usuario');

            $table->unsignedInteger('gen_consignatario_id')->index()->nullable();
            $table->foreign('gen_consignatario_id')->references('id')->on('gen_consignatario');

            $table->unsignedInteger('gen_divisa_id')->index()->nullable();
            $table->foreign('gen_divisa_id')->references('id')->on('gen_divisa');

            $table->string('nro_anticipo',50)->nullable();
            $table->date('fecha_anticipo')->nullable();
            $table->string('nro_doc_comercial',50)->nullable();
            $table->double('monto_anticipo')->nullable();
            $table->double('monto_total_ant')->nullable();
            $table->string('observaciones',150)->nullable();
            
            $table->boolean('bactivo')->default(1);
            $table->timestamps();//Lo crea por defecto
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gen_anticipo');
    }
}
