@extends('templates/layoutlte_analist_djo')
@section('content')
<div class="panels">
<div id="myTabContent" class="tab-content">
    <table id="listaer" class="table table-striped table-bordered" style="width:100%">
      <thead>
          <tr>
              <th>Número de Solicitud</th>
              <th>Tipo de Solicitud</th>
              <th>Estatus Solicitud</th>
              <th>Fecha Solictud</th>
              <th>Monto Solictud</th>
          </tr>
      </thead>
      <tbody>
        @foreach($solicitudes as $solicitud)
          <tr>
              <td>{{$solicitud->id}}</td>
              <td>{{$solicitud->tiposolictud->solicitud}}</td>
              <td>@if(!is_null($solicitud->gen_status_id)){{$solicitud->status->nombre_status}}@endif</td>
              <td>{{$solicitud->fsolicitud}}</td>
              <td>{{$solicitud->monto_solicitud}}</td>
          </tr>  
         @endforeach
      </tbody>
    </table>
</div>
</div>
@stop