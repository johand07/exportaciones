<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Permiso extends Model
{
    protected $table = 'permiso';

    protected $fillable = [

    	'gen_usuario_id',
    	'tipo_permiso_id',
    	'numero_permiso',
    	'fecha_permiso',
    	'fecha_aprobacion',
    	'gen_aduana_salida_id',
    	'pais_id',
    	'bactivo'

    ];

     protected $date =['created_at','updated_at'];

     public function usuario(){

     	return $this->hasOne('App\Models\GenUsuario','id','gen_usuario_id');

     }

     public function tipermiso(){

     	return $this->hasOne('App\Models\TipoPermiso','id','tipo_permiso_id');
     }

      public function pais() {

      return $this->hasOne('App\Models\Pais','id','pais_id');
    	
    }

    public function aduanassal() {

     return $this->hasOne('App\Models\GenAduanaSalida','id','gen_aduana_salida_id');


    }
}
