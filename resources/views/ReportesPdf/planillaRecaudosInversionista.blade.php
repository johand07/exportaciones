<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Er</title>
    <style>

        body{
            text-align:center;
           font-family: Arial;
                 color: #58585A; 
        }
        th{
            font-size:16px;
            background-color:#818286;
            color: #fff;
        }
        td{
            font-size:14px;
            border: 1px solid #818286;
        }
        table{
            border: 1px solid #818286;
        }

        .table2{
            border: 0px;
        }
        .tabla2 > table > thead > tr > th:nth-child(0){

         width: 10%;
        }
        .td2{
            font-size:10px;
            border: 0px solid #818286;
            width: 125%;
            text-align:center;
            font-size:14px;
        }
        .td3{

            border: 0px solid #818286;
            width: 15%;
            text-align:center;

        }
        .td4{

            border: 0px solid #818286;


        }

         .centrar{
            text-align: center;
        }

         .borde{
            border: 1px solid #fff;
        }

    </style>
</head>
    <body>

      <div class="">
        <table style="width:100%;" class="table2">
          <tbody>
            <tr>
              <td class="td3">
                <img class="img-responsive" style="width:200px; height: 40px" src='img/MIPPCOEXIN.png'>
               
                 
              </td>
              <td class="borde"></td>
              <td class="borde"></td>
               <td>
                  <img class="img-responsive borde" style="width: 300px; height: 40px" src='img/cintillo.png'>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
      <div>
        <h5>REQUISITOS PARA EL REGISTRO DE POTENCIALES INVERSIONISTAS (Forma RPI01)</h5>
      </div>
      <div class="tabla">
        <h4>Recaudos Personas naturales.</h4>
        <table  style="width:100%;">
          <thead>
            <tr>
              <th>N°</th>
              <th class="centrar">RECAUDO</th>
              <th>CONSIGNO</th>
            
            </tr>
          </thead>
          <tbody>
        @foreach($documentosNat as $key=> $documento)
            <tr>
              
              <td >{{$key+1}}</td>
              <td >{{$documento->nombre_documento}}</td>
              <td align="center"> <center><input type="checkbox" name=""></center></td>
                 
            </tr>
         @endforeach

          </tbody>
        </table>
      </div>
      <div class="tabla">
        <h4>Recaudos Personas jurídicas.</h4>
        <table  style="width:100%;">
          <thead>
            <tr>
              <th>N°</th>
              <th class="centrar">RECAUDO</th>
              <th>CONSIGNO</th>
            
            </tr>
          </thead>
          <tbody>
        @foreach($documentosJuri as $key=> $documento)
            <tr>
              
              <td >{{$key+1}}</td>
              <td >{{$documento->nombre_documento}}</td>
              <td align="center"> <center><input type="checkbox" name=""></center></td>
                 
            </tr>
         @endforeach

          </tbody>
        </table>
      </div>
      <div class="tabla">
        <h4><u><b>Soportes comunes para optar al registro de potencial inversionista.</b></u></h4>
        <table  style="width:100%;">
          <thead>
            <tr>
              <th>N°</th>
              <th class="centrar">RECAUDO</th>
              <th>CONSIGNO</th>
            
            </tr>
          </thead>
          <tbody>
        @foreach($documentosSport as $key=> $documento)
            <tr>
              
              <td >{{$key+1}}</td>
              <td >{{$documento->nombre_documento}}</td>
              <td align="center"> <center><input type="checkbox" name=""></center></td>
                 
            </tr>
         @endforeach

          </tbody>
        </table>
      </div>
    </body>
</html>
