<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AnalisisCalificacion extends Model
{
    protected $table = 'analisis_calificacion';
    protected $fillable = [

           'gen_declaracion_jo_id',
           'num_solicitud',
           'gen_usuario_id',
           'gen_status_id',
           'bactivo',
          
    ];
   
   	public function declaracionProduc() {

   		return $this->hasOne('App\Models\DetDeclaracionProduc','id','det_declaracion_produc_id');
    }

    public function rDeclaracion() {

      return $this->hasOne('App\Models\GenDeclaracionJO','id','gen_declaracion_jo_id');

    }

    public function rCriterioAnalisis() {

     return $this->hasMany('App\Models\CriterioAnalisis','analisis_calificacion_id','id');

    }

    public function GenUsuario() {

      return $this->hasOne('App\Models\GenUsuario','id','gen_usuario_id');

    }





}
