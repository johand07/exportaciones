<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GenDocumentosInversiones extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gen_documentos_inversiones', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('gen_sol_inversionista_id')->default(1)->index()->nullable();
            $table->foreign('gen_sol_inversionista_id')->references('id')->on('gen_sol_inversionista');
            $table->string('file_1',300)->nullable();
            $table->string('file_2',300)->nullable();
            $table->string('file_3',300)->nullable();
            $table->string('file_4',300)->nullable();
            $table->string('file_5',300)->nullable();
            $table->string('file_6',300)->nullable();
            $table->string('file_7',300)->nullable();
            $table->string('file_8',300)->nullable();
            $table->string('file_9',300)->nullable();
            $table->string('file_10',300)->nullable();
            $table->string('file_11',300)->nullable();
            $table->string('file_12',300)->nullable();
            $table->string('file_13',300)->nullable();
            $table->string('file_14',300)->nullable();
            $table->string('file_15',300)->nullable();
            $table->string('file_16',300)->nullable();
            $table->string('file_17',300)->nullable();
            $table->string('file_18',300)->nullable();
            $table->string('file_19',300)->nullable();
            $table->string('file_20',300)->nullable();
            $table->string('file_21',300)->nullable();
            $table->boolean('bactivo')->default(1);
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::dropIfExists('gen_documentos_inversiones');
    }
}
