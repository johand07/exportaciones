<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableEstadoObservacionGenSolCvc extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

   public function up()
    {   
        Schema::table('gen_sol_cvc', function (Blueprint $table) {
            $table->boolean('edo_observacion')->default(0)->nullable()->after('gen_status_id');
            $table->text('observacion')->nullable()->after('edo_observacion');
        });
    }   

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('gen_sol_cvc', function (Blueprint $table) {
            //
            $table->dropColumn('edo_observacion');
            $table->dropColumn('observacion');
        });
    }


}
