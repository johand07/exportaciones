<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CasaMatriz extends Model
{
    protected $table = 'casa_matriz';

	protected $fillable = [

           'gen_usuario_id',
           'casa_matriz',
           'pais_id',
           'bactivo',
    ];

    protected $date =['created_at','updated_at'];

    public function usuarios() {

     return $this->hasOne('App\Models\GenUsuario','id','gen_usuario_id');

    }
     

}
