<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GenActividadEco extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gen_actividad_eco', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('cod_actividad', 100)->nullable();
            $table->string('dactividad', 100)->nullable();
            $table->unsignedInteger('gen_sector_id')->index()->nullable();
            $table->foreign('gen_sector_id')->references('id')->on('gen_sector');
            $table->boolean('bactivo')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::dropIfExists('gen_actividad_eco');
    }
}
