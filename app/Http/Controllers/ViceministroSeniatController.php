<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\GenArancelMercosur;
use App\Models\CatCertRegSeniat;
use App\Models\GenCertRegSeniat;
use App\Models\DocCertRegSeniat;
use App\Models\AspectoCumplidoCertRegSeniat;
use App\Models\BandejaAnalistaCertRegSeniat;
use App\Models\BandejaViceministroCertRegSeniat;
use Auth;
use File;
use Alert;

class ViceministroSeniatController extends Controller
{
    public function index()
    {
        return view('ViceministroSeniat.index');
    }

    public function lista(Request $request)
    {
        //dd(2222);
        if(isset($request->status)){
            
            if($request->status == 12){
                $certificados = GenCertRegSeniat::with('GenStatus')->where('bactivo', 1)
                                                ->where('gen_status_id', $request->status)
                                                ->orWhere('gen_status_id', 29)
                                                ->get();
            }else{
                $certificados = GenCertRegSeniat::with('GenStatus')->where('bactivo', 1)
                                                ->where('gen_status_id', $request->status)
                                                ->get();


            }
        }else{
            $certificados = GenCertRegSeniat::with('GenStatus')->where('bactivo', 1)->get();
        }
       
       //dd($certificados);
       return view('ViceministroSeniat.lista',compact('certificados'));
    }

    public function show($id)
    {
       //dd($id);
    
    	$certificado = GenCertRegSeniat::find($id);
        //dd($certificado);
        $bandejanalista=BandejaAnalistaCertRegSeniat::where('gen_cert_reg_seniat_id',$id)->first();
        $bandejavice=BandejaViceministroCertRegSeniat::where('gen_cert_reg_seniat_id',$id)->first();
        //dd($bandejanalista);
        
        $catCert = CatCertRegSeniat::where('bactivo', 1)->get();

        $aspectoscumplidos=AspectoCumplidoCertRegSeniat::where('gen_cert_reg_seniat_id',$id)->get();


        $id_aspctos = [];

        foreach($aspectoscumplidos as $row){
            $id_aspctos[$row->cat_cert_reg_seniat_id] = $row->cat_cert_reg_seniat_id;
        }

        //dd($id_aspctos);

        $file = DocCertRegSeniat::where('gen_cert_reg_seniat_id', $id)->first();

        return view('ViceministroSeniat.show', compact('certificado', 'file', 'catCert','aspectoscumplidos','bandejanalista','id_aspctos','bandejavice'));
      }


    public function store(Request $request)
    {
       //dd($request->all());

        $certificado = GenCertRegSeniat::find($request->id);

        $mensaje1   = '';
        $mensaje2   = '';

        if($request->desaprobar){

            $this->validate($request, [
                'observacion_analista'          => 'required'
                
            ]);

            $certificado->observacion_analista  =  $request->observacion_analista;
            $certificado->gen_status_id         =   28;
            $certificado->save();

            $mensaje1 = 'El certificado se ha Desaprobado exitosamente';
            $mensaje2 = 'Desaprobado con exito';

        }elseif($request->aprobar){

            $this->validate($request, [
                "gen_cert_reg_seniat_id"    => "required|array|min:1",
                "gen_cert_reg_seniat_id.*"  => "required|distinct|min:1",
            ]);
           
            if(isset($request->gen_cert_reg_seniat_id)){

                $contador = count($request->gen_cert_reg_seniat_id);

                for($i=0; $i <= $contador-1; $i++){

                    $aspectoCumplido = new AspectoCumplidoCertRegSeniat;
                    $aspectoCumplido->gen_cert_reg_seniat_id    =   $request->id;
                    $aspectoCumplido->cat_cert_reg_seniat_id    =   $request->gen_cert_reg_seniat_id[$i];
                    $aspectoCumplido->save();
    
                }
    
                if($contador == 13){
                    $certificado->gen_status_id         =   29;
                    $certificado->save();   
    
                    $mensaje1 = 'El certificado ha sido Aprobado de manera exitosa';
                    $mensaje2 = 'Aprobado con exito';
    
                }else{
                    $certificado->gen_status_id         =   28;
                    $certificado->save();  
    
                    $mensaje1 = 'El certificado no cumple con los aspectos requeridos';
                    $mensaje2 = 'Desaprobado';
                }
            }else{

                $certificado->gen_status_id         =   28;
                $certificado->save();  

                $mensaje1 = 'El certificado no cumple con los aspectos requeridos';
                $mensaje2 = 'Desaprobado';
            }
            
        }elseif($request->solo_aprobar){

            $certificado->gen_status_id         =   29;
            $certificado->save();   

            $mensaje1 = 'El certificado ha sido Aprobado de manera exitosa';
            $mensaje2 = 'Aprobado con exito';

        }

            

        BandejaViceministroCertRegSeniat::create([
            'gen_cert_reg_seniat_id'    =>      $certificado->id,
            'gen_usuario_id'            =>      Auth::user()->id,
            'gen_status_id'             =>      $certificado->gen_status_id,
            'fstatus'                   =>      date('Y-m-d'),
        ]);

        Alert::success($mensaje1.'!',$mensaje2.'!')->persistent("Ok");

        return redirect()->route('ViceministroSeniat');
        
    }
}
