<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NotaDebito extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nota_debito', function (Blueprint $table) {
           $table->engine = 'InnoDB';
           $table->increments('id');
           $table->unsignedInteger('gen_usuario_id')->index()->nullable();
           $table->foreign('gen_usuario_id')->references('id')->on('gen_usuario');
           $table->unsignedInteger('gen_factura_id')->index()->nullable();
           $table->foreign('gen_factura_id')->references('id')->on('gen_factura');
           $table->unsignedInteger('gen_consignatario_id')->index()->nullable();
           $table->foreign('gen_consignatario_id')->references('id')->on('gen_consignatario');
           $table->double('monto_nota_debito')->nullable();
           $table->string('num_nota_debito', 100)->nullable();
           $table->date('fecha_emision')->nullable();
           $table->string('concepto_nota_debito', 100)->nullable();
           $table->string('er', 200)->nullable();
           $table->string('justificacion', 200)->nullable();
           $table->boolean('bactivo')->default(1);
           $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
          Schema::dropIfExists('nota_debito');
    }
}
