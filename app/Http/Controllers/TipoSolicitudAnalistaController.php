<?php

namespace App\Http\Controllers;
use App\Models\GenSolicitud;
use App\Models\TipoSolicitud;
use App\Http\Requests\GenSolicitudRequest;
use App\Models\GenConsignatario;
use App\Models\ConsigTecno;
use App\Models\DetUsuario;
use App\Models\GenFactura;
use Illuminate\Support\Facades\Session;
use Auth;
use Laracasts\Flash\Flash;
use Alert;
use Illuminate\Http\Request;

class TipoSolicitudAnalistaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        
        $titulo="Solicitudes de Exportación Realizada";
        $descripcion="Solicitudes Registradas";
        $solicitudes=GenSolicitud::whereYear('created_at', date("Y"))
        ->whereNotNull('monto_solicitud')
        ->where('bactivo',1)
        ->get();

               
        return view('SolicitudERAnalista.index',compact('solicitudes','titulo','descripcion'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {



    }





    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(GenSolicitudRequest $request)
    {


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\GenSolicitud  $genSolicitud
     * @return \Illuminate\Http\Response
     */
    public function show(GenSolicitud $genSolicitud)
    {

    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\GenSolicitud  $genSolicitud
     * @return \Illuminate\Http\Response
     */
    public function edit(GenSolicitud $genSolicitud, $id)
    {
     
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\GenSolicitud  $genSolicitud
     * @return \Illuminate\Http\Response
     */
    public function update(GenSolicitudRequest $request, GenSolicitud $genSolicitud, $id)
    {
      

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\GenSolicitud  $genSolicitud
     * @return \Illuminate\Http\Response
     */
    public function destroy(GenSolicitud $genSolicitud)
    {
        //
    }

    public function verificarFac(Request $request){

       if($request->ajax()){

         $arx=[];
         $data=GenFactura::where('gen_usuario_id',Auth::user()->id)->get();
        foreach ($data as $key => $value) {

             if($value->bactivo!==0){
               $arx[$key]=0;
               break;
             }
         }

        if($arx==null){
            return 1;
        }else{
            return 0;
        }

       }
    }
}
