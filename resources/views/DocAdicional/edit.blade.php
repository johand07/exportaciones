@extends('templates/layoutlte')
@section('content')
@php
function namefile($name){
  $explode=explode("/",$name);
  return $explode[4];

}

function extension($url){
  $path = $url;
  $extension = pathinfo($path, PATHINFO_EXTENSION);
  return $extension;  

}
@endphp
<div class="content">
  @if(!empty($detDocAdicionalDeclaracion->estado_observacion) && $detDocAdicionalDeclaracion->estado_observacion == 1 )
  <script>

    document.onreadystatechange = function () {
      var state = document.readyState;
      if (state == 'complete') {
        swal("Estimado usuario debera corregír las siguientes observaciones", "{{$detDocAdicionalDeclaracion->descrip_observacion}}", "warning");
      }
    }
  </script>
  <br>

  <div class="alert alert-danger" role="alert">
    <strong>
      <span class="glyphicon glyphicon-circle-arrow-right"></span> Observación Indicada por el Analista: {{$detDocAdicionalDeclaracion->descrip_observacion}}
    </strong>
  </div>

@endif
<div class="panel-group" style="background-color: #fff;">
      <div class="panel-primary">
        <div class="panel-heading"><h3>Carga de Documentos Adicionales</h3></div>
        <div class="panel-body">
        <h5 style="color: red">Formatos permitidos .doc, .docx, .pdf, .pptx, .ppt, .xls, .xlsx, .zip, .png, .jpg, ,jpeg</h5>

                  <div class="row" style="background-color: #fff">
                      <div class="col-md-12">
                  {{ Form::model($detDocAdicionalDeclaracion, array('route' => array('DocAdicional.update', $detDocAdicionalDeclaracion->id), 'method' => 'PUT','enctype' => 'multipart/form-data')) }}   

                          <div class="row">
                              <div class="col-md-12">
                                  <table border="0" class="col-md-12">
                                    <thead>
                                      <tr>
                                        <th class="text-center" width="50%" height="40px"><h3><b>Sección de Carga</b></h3></th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                  <input type="hidden" name="gen_declaracion_jo_id" value="{{$gen_declaracion_jo_id}}" placeholder="" id="gen_declaracion_jo_id">
                                      <tr>
                                        <td class="text-center" valign="top" > 
                                          <span><b>1.)</b></span>
                                          <span class="btn btn-default btn-file" style="margin: 3px;">
                                              Cargar Archivo <span class="glyphicon glyphicon-picture"></span>
                                              {!! Form::file('file_1', array('class' => 'file-input','style'=>'margin: 10px','id'=>'file_1')) !!}
                                          </span>
                                        </td>
                                        <td class="text-center" height="40px"><img id="vista_previa_file_1" src="" alt="" width="300px" style="margin: 3px;"><img id="imgdocumento_file_1" src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px; display: none;"><div id="nombre_file_1"></div>
                                          
                                          @if(isset($detDocAdicionalDeclaracion->file_1))
                                          <div id="imgdocumentoEdit_file_1" class="file_1">
                                            @if(extension($detDocAdicionalDeclaracion->file_1) == 'jpeg' || extension($detDocAdicionalDeclaracion->file_1) == 'png')
                                              <img src="{{ asset($detDocAdicionalDeclaracion->file_1) }}" alt="" width="100px" style="margin: 3px;" >
                                            @else
                                              <img src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px;" >
                                            @endif
                                            
                                            {{namefile($detDocAdicionalDeclaracion->file_1)}}
                                          </div>
                                          @endif
                                        </td>
                                      </tr>

                                      <tr>
                                        <td class="text-center" valign="top" >
                                          <span><b>2.)</b></span> 
                                          <span class="btn btn-default btn-file" style="margin: 3px;">
                                              Cargar Archivo <span class="glyphicon glyphicon-picture"></span>
                                              {!! Form::file('file_2', array('class' => 'file-input','style'=>'margin: 10px','id'=>'file_2')) !!}
                                          </span>
                                        </td>
                                        <td class="text-center" height="40px"><img id="vista_previa_file_2" src="" alt="" width="300px" style="margin: 3px;"><img id="imgdocumento_file_2" src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px; display: none;"><div id="nombre_file_2"></div>
                                          @if(isset($detDocAdicionalDeclaracion->file_2))
                                            <div id="imgdocumentoEdit_file_2" class="file_2">
                                              @if(extension($detDocAdicionalDeclaracion->file_2) == 'jpeg' || extension($detDocAdicionalDeclaracion->file_2) == 'png')
                                                <img src="{{ asset($detDocAdicionalDeclaracion->file_2) }}" alt="" width="100px" style="margin: 3px;" >
                                              @else
                                                <img src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px;" >
                                              @endif
                                              {{namefile($detDocAdicionalDeclaracion->file_2)}}
                                            </div>
                                          @endif
                                        </td>
                                      </tr>

                                      <tr>
                                        <td class="text-center" valign="top" >
                                          <span><b>3.)</b></span> 
                                          <span class="btn btn-default btn-file" style="margin: 3px;">
                                              Cargar Archivo <span class="glyphicon glyphicon-picture"></span>
                                              {!! Form::file('file_3', array('class' => 'file-input','style'=>'margin: 10px','id'=>'file_3')) !!}
                                          </span>
                                        </td>
                                        <td class="text-center" height="40px"><img id="vista_previa_file_3" src="" alt="" width="300px" style="margin: 3px;"><img id="imgdocumento_file_3" src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px; display: none;"><div id="nombre_file_3"></div>
                                          @if(isset($detDocAdicionalDeclaracion->file_3))
                                            <div id="imgdocumentoEdit_file_3" class="file_3">
                                              @if(extension($detDocAdicionalDeclaracion->file_3) == 'jpeg' || extension($detDocAdicionalDeclaracion->file_3) == 'png')
                                                <img src="{{ asset($detDocAdicionalDeclaracion->file_3) }}" alt="" width="100px" style="margin: 3px;" >
                                              @else
                                                <img src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px;" >
                                              @endif
                                              {{namefile($detDocAdicionalDeclaracion->file_3)}}
                                            </div>
                                          @endif
                                        </td>
                                      </tr>

                                      <tr>
                                        <td class="text-center" valign="top" >
                                          <span><b>4.)</b></span> 
                                          <span class="btn btn-default btn-file" style="margin: 3px;">
                                              Cargar Archivo<span class="glyphicon glyphicon-picture"></span>
                                              {!! Form::file('file_4', array('class' => 'file-input','style'=>'margin: 10px','id'=>'file_4')) !!}
                                          </span>
                                        </td>
                                        <td class="text-center" height="40px"><img id="vista_previa_file_4" src="" alt="" width="300px" style="margin: 3px;"><img id="imgdocumento_file_4" src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px; display: none;"><div id="nombre_file_4"></div>
                                          @if(isset($detDocAdicionalDeclaracion->file_4))
                                            <div id="imgdocumentoEdit_file_4" class="file_4">
                                              @if(extension($detDocAdicionalDeclaracion->file_4) == 'jpeg' || extension($detDocAdicionalDeclaracion->file_4) == 'png')
                                                <img src="{{ asset($detDocAdicionalDeclaracion->file_4) }}" alt="" width="100px" style="margin: 3px;" >
                                              @else
                                                <img src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px;" >
                                              @endif
                                              {{namefile($detDocAdicionalDeclaracion->file_4)}}
                                            </div>
                                          @endif
                                        </td>
                                      </tr>

                                      <tr>
                                        <td class="text-center" valign="top" >
                                          <span><b>5.)</b></span> 
                                          <span class="btn btn-default btn-file" style="margin: 3px;">
                                              Cargar Archivo <span class="glyphicon glyphicon-picture"></span>
                                              {!! Form::file('file_5', array('class' => 'file-input','style'=>'margin: 10px','id'=>'file_5')) !!}
                                          </span>
                                        </td>
                                        <td class="text-center" height="40px"><img id="vista_previa_file_5" src="" alt="" width="300px" style="margin: 3px;"><img id="imgdocumento_file_5" src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px; display: none;"><div id="nombre_file_5"></div>
                                          @if(isset($detDocAdicionalDeclaracion->file_5))
                                            <div id="imgdocumentoEdit_file_5" class="file_5">
                                              @if(extension($detDocAdicionalDeclaracion->file_5) == 'jpeg' || extension($detDocAdicionalDeclaracion->file_5) == 'png')
                                                <img src="{{ asset($detDocAdicionalDeclaracion->file_5) }}" alt="" width="100px" style="margin: 3px;" >
                                              @else
                                                <img src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px;" >
                                              @endif
                                              {{namefile($detDocAdicionalDeclaracion->file_5)}}
                                            </div>
                                          @endif
                                        </td>
                                      </tr>

                                      <tr>
                                        <td class="text-center" valign="top" >
                                          <span><b>6.)</b></span> 
                                          <span class="btn btn-default btn-file" style="margin: 3px;">
                                              Cargar Archivo <span class="glyphicon glyphicon-picture"></span>
                                              {!! Form::file('file_6', array('class' => 'file-input','style'=>'margin: 10px','id'=>'file_6')) !!}
                                          </span>
                                        </td>
                                        <td class="text-center" height="40px"><img id="vista_previa_file_6" src="" alt="" width="300px" style="margin: 3px;"><img id="imgdocumento_file_6" src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px; display: none;"><div id="nombre_file_6"></div>
                                          @if(isset($detDocAdicionalDeclaracion->file_6))
                                            <div id="imgdocumentoEdit_file_6" class="file_6">
                                              @if(extension($detDocAdicionalDeclaracion->file_6) == 'jpeg' || extension($detDocAdicionalDeclaracion->file_6) == 'png')
                                                <img src="{{ asset($detDocAdicionalDeclaracion->file_6) }}" alt="" width="100px" style="margin: 3px;" >
                                              @else
                                                <img src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px;" >
                                              @endif
                                              {{namefile($detDocAdicionalDeclaracion->file_6)}}
                                            </div>
                                          @endif
                                        </td>
                                      </tr>

                                    </tbody>
                                  </table>
                              </div>
                          </div>
<hr>
                          <div class="row" style="background-color: #fff">

                                <div class="col-md-12" align="center">

                                     <input type="submit"  name="submit" id="submit" class="btn btn-primary"  value="Guardar" >
                                    <a class="btn btn-danger btn-md" href="{{ url('exportador/DeclaracionJO/create') }}">Cancelar</a>

                                </div>
                              
                            </div>


                  {{Form::close()}}

                      </div>
                  </div>

        </div>
      </div>
    </div>

  </div><!-- Fin content -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script>

   function cambiarImagen(event) {
    var id= event.target.getAttribute('name');
    //alert(id);
    var reader = new FileReader();
    reader.onload = function(){
      var output = document.getElementById('vista_previa_'+id);
      output.src = reader.result;
    };
   // var archivo = $(".file-input").val();

   var nombre = event.target.getAttribute('name');
    var archivo = $("[name^="+nombre+"]").val();
    var extensiones = archivo.substring(archivo.lastIndexOf(".")).toLowerCase();
    //alert(archivo);
    //var imgfile="/exportaciones/public/img/file.png";

//alert(imgfile);
    if(extensiones != ".jpeg" && extensiones != ".jpg" && extensiones != ".png"){
        $('#imgdocumento_'+id).show();
        let file=$('#'+id).val();
     // alert(file);
     //Y pintame ruta y nombre del archivo acargado con nombre_
      $('#nombre_'+id).html('<strong>'+file+'</strong>');
      //$('#imgdocumentoEdit_'+id).hide();
      $('.'+id).empty();
    }else{
      $('#vista_previa_'+id).show();
       let file=$('#'+id).val();
     // alert(file);
     //Y pintame ruta y nombre del archivo acargado con nombre_
      $('#nombre_'+id).html('<strong>'+file+'</strong>');
      $('.'+id).empty();
      
      $('#imgdocumento_'+id).hide();
      reader.readAsDataURL(event.target.files[0]);
    }
  }

  function cambiarImagenDoc(event) {

    
    //console.log(event);

    var id= event.target.getAttribute('id');
    var archivo = $("[name^="+nombre+"]").val();
    var extensiones = archivo.substring(archivo.lastIndexOf(".")).toLowerCase();
    
    //var imgfile="/exportaciones/public/img/file.png";

    
    if(extensiones != ".doc" && extensiones != ".docx" && extensiones != ".pdf"){
      swal("Formato invalido", "Formato de archivo invalido debe ser .doc, .docx o pdf", "warning"); 
        $('#'+id).val('');
        
    }else{
      $('#imgdocumento_'+id).show();
      let file=$('#'+id).val();
     // alert(file);
      $('#nombre_'+id).html('<strong>'+file+'</strong>');
     
      reader.readAsDataURL(event.target.files[0]);

    }

  }


    $(document).on('change', '.file-input', function(evt) {
      
      let size=this.files[0].size;
      if (size>8500000) {

         swal("Tamaño de archivo invalido", "El tamaño del archivo No puede ser mayor a 8.5 megabytes","warning"); 
          this.value='';
         //this.files[0].value='';
      }else{
        cambiarImagen(evt);
      }
      
           
    });








    $(document).on('change', '.file-input', function(evt) {
           cambiarImagen(evt);
    });


</script>

@endsection