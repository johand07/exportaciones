<?php

namespace App\Http\Controllers;
use App\Models\GenSolicitud;
//use App\Models\DetUsuario;
use Illuminate\Http\Request;
use Session;
use Auth;
use Alert;
use PDF;
use Illuminate\Routing\Route;
use Illuminate\Http\Response;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\DB;

class ReporteSolicitudesIntranetController extends Controller
{
    public function index(Request $request)
    {
        $fecha=date("Y-m-d");
        $desde=$request->desde;
        $hasta=$request->hasta;
        $mes=date('m');
        $anio=date('Y');


        $titulo = 'Reporte de Recepción de Solicitudes';
        $descripcion = 'Demostración de Venta de Divisas';
        return view ('ReporteSolicitudesIntranet.index', compact('titulo','descripcion','desde','hasta'));

    }

    public function store(Request $request)
    {
        //dd($request->desde);
        $fecha=date("Y-m-d H");
        $fecha_desde=$request->desde;
        //dd($fecha_desde);
        $fecha_hasta=$request->hasta;
        $desde=$request->desde;
        $hasta=$request->hasta;


   /**************************Consulta para de solicitudes de bienes me traigo las que tengo resgitrada*/

   $gen_Solicitud=count(GenSolicitud::whereIn('tipo_solicitud_id',[1,2])->whereIn('gen_status_id',[31,32,33,35])->whereBetween('fstatus',[$fecha_desde,$fecha_hasta])->get());

  //dd($gen_Solicitud);

   $solicitudesSuspendidos = DB::table('bandeja_asignaciones_intranet as ba')
   ->join('gen_solicitud as gs','ba.gen_solicitud_id','=','gs.id')
   
   ->join('tipo_solicitud as ts','gs.tipo_solicitud_id','=','ts.id')
   ->join('det_usuario as du','gs.gen_usuario_id','=','du.gen_usuario_id')
   ->join('gen_operador_cambiario as  op','gs.coperador_cambiario','=','op.id')
   ->join('gen_usuario as  gu','ba.analizado_por','=','gu.id')
   ->join('gen_usuario as  guas','ba.asignado_por','=','guas.id')
   ->select(
           'du.rif',
           'du.razon_social',
           'gs.id as gen_solicitud_id',
           'ts.solicitud',
           'op.nombre_oca',
           'gu.email as analizado_por',
           'guas.email as asignado_por',
           'ba.gen_status_id',
           'gs.observacion_analista_intra',
           'gs.bactivo'
          )
   ->whereIn('gs.gen_status_id',[31])
   ->where('gs.bactivo',1)
   ->whereBetween('gs.fstatus',[$fecha_desde,$fecha_hasta])
   ->get();

   //dd($solicitudesSuspendidos);

   $solicitudesCierreConformes = DB::table('bandeja_asignaciones_intranet as ba')
   ->join('gen_solicitud as gs','ba.gen_solicitud_id','=','gs.id')
   
   ->join('tipo_solicitud as ts','gs.tipo_solicitud_id','=','ts.id')
   ->join('det_usuario as du','gs.gen_usuario_id','=','du.gen_usuario_id')
   ->join('gen_operador_cambiario as  op','gs.coperador_cambiario','=','op.id')
   ->join('gen_usuario as  gu','ba.analizado_por','=','gu.id')
   ->join('gen_usuario as  guas','ba.asignado_por','=','guas.id')
   ->select(
           'du.rif',
           'du.razon_social',
           'gs.id as gen_solicitud_id',
           'ts.solicitud',
           'op.nombre_oca',
           'gu.email as analizado_por',
           'guas.email as asignado_por',
           'ba.gen_status_id',
           'gs.observacion_analista_intra',
           'gs.bactivo'
          )
   ->whereIn('gs.gen_status_id',[32])
   ->where('gs.bactivo',1)
   ->whereBetween('gs.fstatus',[$fecha_desde,$fecha_hasta])
   ->get();

   $solicitudesCierreParciales = DB::table('bandeja_asignaciones_intranet as ba')
            ->join('gen_solicitud as gs','ba.gen_solicitud_id','=','gs.id')
            
            ->join('tipo_solicitud as ts','gs.tipo_solicitud_id','=','ts.id')
            ->join('det_usuario as du','gs.gen_usuario_id','=','du.gen_usuario_id')
            ->join('gen_operador_cambiario as  op','gs.coperador_cambiario','=','op.id')
            ->join('gen_usuario as  gu','ba.analizado_por','=','gu.id')
            ->join('gen_usuario as  guas','ba.asignado_por','=','guas.id')
            ->select(
                    'du.rif',
                    'du.razon_social',
                    'gs.id as gen_solicitud_id',
                    'ts.solicitud',
                    'op.nombre_oca',
                    'gu.email as analizado_por',
                    'guas.email as asignado_por',
                    'ba.gen_status_id',
                    'gs.observacion_analista_intra',
                    'gs.bactivo'
                   )
            ->whereIn('gs.gen_status_id',[33])
            ->where('gs.bactivo',1)
            ->whereBetween('gs.fstatus',[$fecha_desde,$fecha_hasta])
            ->get();

            $solicitudesEviadasArchivos = DB::table('bandeja_asignaciones_intranet as ba')
            ->join('gen_solicitud as gs','ba.gen_solicitud_id','=','gs.id')
            
            ->join('tipo_solicitud as ts','gs.tipo_solicitud_id','=','ts.id')
            ->join('det_usuario as du','gs.gen_usuario_id','=','du.gen_usuario_id')
            ->join('gen_operador_cambiario as  op','gs.coperador_cambiario','=','op.id')
            ->join('gen_usuario as  gu','ba.analizado_por','=','gu.id')
            ->join('gen_usuario as  guas','ba.finalizado_por','=','guas.id')
            ->select(
                    'du.rif',
                    'du.razon_social',
                    'gs.id as gen_solicitud_id',
                    'ts.solicitud',
                    'op.nombre_oca',
                    'gu.email as analizado_por',
                    'guas.email as finalizado_por',
                    'ba.gen_status_id',
                    'gs.observacion_analista_intra',
                    'gs.bactivo'
                   )
            ->whereIn('gs.gen_status_id',[35])
            ->where('gs.bactivo',1)
            ->whereBetween('gs.fstatus',[$fecha_desde,$fecha_hasta])
            ->get();

        $titulo = 'Reporte de Recepción de Solicitudes';
        $descripcion = 'Demostración de Venta de Divisas';
        return view ('ReporteSolicitudesIntranet.index', compact('titulo','descripcion','solicitudesSuspendidos','solicitudesCierreConformes','solicitudesCierreParciales','solicitudesEviadasArchivos','desde','hasta'));

    }



/*Reporte Export de Excel de Recepcion de Solicitudes*/

    public function ExcelReportesRecepcionSolicitudes(Request $request, GenSolicitud $genSolicitud)
    {
        //dd(1);
        $desde=$request->desde;
        $hasta=$request->hasta;
        //$mes=date('m');

        //dd($request->all());
         $sol_er=DB::table('gen_solicitud as gs')
         ->join('det_usuario as det', 'gs.gen_usuario_id', '=', 'det.gen_usuario_id')
         ->join('tipo_solicitud as tsol', 'gs.tipo_solicitud_id', '=', 'tsol.id')
         ->join('gen_status as st', 'gs.gen_status_id', '=', 'st.id')
         ->select(
                        'gs.id',
                        'gs.fsolicitud',
                        'det.rif',
                        'det.razon_social',
                        'tsol.solicitud',
                        'st.nombre_status',
                        'gs.fstatus'
                       )
         ->whereIn('gs.gen_status_id',[31, 32, 33, 35])
         ->where('gs.bactivo',1)
         ->whereBetween('gs.fstatus',[$desde,$hasta])
         ->get();

        //dd($sol_er);

        $originalDate = "2022-06-02";
        $timestamp = strtotime($originalDate); 
        $newDate = date("d-m-Y", $timestamp );
        //dd($newDate);
        $numero = 1002002.365;
        $format=number_format($numero, 2, ",", ".");
        //dd($format);
        
        /**Estructura de excel***/
        $name="Reportes Recepción de Solicitudes";
        //dd($sol_er);
        $myFile   = Excel::create('Reportes de Recepción de Solicitudes',function($excel)use($sol_er){ // Nombre princial del archivo
            $excel->sheet('Recepción de Solicitudes',function($sheet)use ($sol_er){ // se crea la hoja con el nombre de datos
               // dd($sol_er);
                //header
                $sheet->setStyle(array(
                'font' => array(
                    'name' =>  'itálico',
                    'size' =>  12,
                    'bold' =>  true
                )
                ));

                $sheet->mergeCells('A1:H1'); // union de celdas desde A a F
                $sheet->row(1,['Reportes de Recepción de Solicitudes Demostración de Venta de Divisa']);
                $sheet->row(2,[]);
                $sheet->row(3,['Numero Solicitud','Fecha de Solicitud','Rif','Razón Social','Estatus','Tipo de Exportación','Fecha de Estatus']);

                //$sheet->fromArray($sol_er, null, 'A4', null, false);
                // consulta


                
               //dd($sol_er);
                //recorrido
                foreach ($sol_er as $emp) {

                $forma_sol=strtotime($emp->fsolicitud); 
                $fechasol = date("d-m-Y", $forma_sol);

                $forma_status=strtotime($emp->fstatus); 
                $fechastatus = date("d-m-Y", $forma_status );


                   $row=[]; // un arreglo vacio que se va a llenar con las posicion 
                   $row[0]=$emp->id; 
                   $row[1]=$fechasol;
                   $row[2]=$emp->rif; 
                   $row[3]=$emp->razon_social;
                   $row[4]=$emp->nombre_status; 
                   $row[5]=$emp->solicitud;
                   $row[6]=$fechastatus; 
                   
                   $sheet->appendRow($row); // agrega una fila vacia al final del reporte

                }

            });
           // dd($empresas);
           
        });
        $myFile   = $myFile->string('xls'); 
        $response = array(
            'name' => $name, 
            'file' => "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64," . base64_encode($myFile), 
        );

        return response()->json($response);
        //->export('xlsx');
    }

    
}
