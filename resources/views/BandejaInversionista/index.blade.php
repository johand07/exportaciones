@extends('templates/layoutlte')
@section('content')
<div class="panels">
  <div class="row">
    <div class="col-md-3">
      <span><b><p style="text-align: left;">Para ver los Recaudos de Documentos haga click aquí</p></b>
      <a class="btn btn-success" href="{{action('PdfController@planillaRecaudosInversionista')}}" >
        <span class="glyphicon glyphicon-file" title="Recaudos" aria-hidden="true"></span>
        Recaudo de Documentos
      </a>
    </div>
    <div class="col-md-3">
      <span><b><p style="text-align: left;">Regístrate como Potencial Inversionista aquí</p></b>
      <a class="btn btn-primary" href="{{url('/exportador/BandejaInversionista/create')}}">
        <span class="glyphicon glyphicon-file" title="Agregar" aria-hidden="true"></span>
        Solicitud de Potencial Inversionista
      </a>  
    </div>
    <div class="col-md-6"></div>
    <!-- <div class="col-md-3"></div> -->
  </div>
  <hr>
  <div class="content">
    <div class="row">
      <div class="col-md-12">
        <p style="color: red;"><b>Debe consignar el físico de los Documentos</b></p>
        <br>
        <table id="listadua" class="table table-striped table-bordered" style="width:100%">
            <thead>
                <tr>
                    <th>N° de solicitud</th>
                    <th>Fecha</th>
                    <th>Observación</th>
                    <th>Estatus</th>
                    <th>Acciones</th>  
                </tr>      
            </thead>
                <tbody>
                  @foreach($solinversion as $inversionista)
                    <tr>                                                    
                        <td>{{$inversionista->num_sol_inversionista}}</td>
                        <td>{{$inversionista->created_at}}</td>
                        
                          <td>
                            @if($inversionista->estado_observacion == 1 && ($inversionista->gen_status_id == 11 || $inversionista->gen_status_id == 19))

                              {{$inversionista->observacion_inversionista}}

                            @elseif($inversionista->estado_observacion == 1 && $inversionista->gen_status_id == 15)

                              {{$inversionista->observacion_doc_incomp}}

                            @elseif($inversionista->estado_observacion == 1 && $inversionista->gen_status_id == 13)

                              {{$inversionista->observacion_anulacion}}

                            @elseif($inversionista->estado_observacion == 1 && $inversionista->gen_status_id == 17)

                              {{$inversionista->observacion_anulacion}}

                            @endif
                          </td>
                        
                        <td>
                          
                          @if($inversionista->gen_status_id == 16) 
                            <p style="color:#4C8BF5;"><b>Debe consignar el físico de los Documentos Cargados</b></p>
                          @else
                            {{$inversionista->estatus->nombre_status}}
                          @endif
                        </td>
                    
                        <td>
                        @if($inversionista->gen_status_id == 9 || $inversionista->gen_status_id == 10 || $inversionista->gen_status_id == 11 || $inversionista->gen_status_id == 12 || $inversionista->gen_status_id == 16 || $inversionista->gen_status_id == 13 || $inversionista->gen_status_id == 14 || $inversionista->gen_status_id == 15 || $inversionista->gen_status_id == 17 || $inversionista->gen_status_id == 19 || $inversionista->gen_status_id == 26)
                          <!--a href="{{action('PdfController@planillaSolInversionista',array('id'=>$inversionista->id))}}" class="glyphicon glyphicon-file btn btn-default btn-sm" title="PLANILLA RPI-01" aria-hidden="true"></a-->
                        @endif
                      <!--CREAR VARIOS IF DEPENDE DE LOS ESTATUS (9)EN PROCESO, (10)ATENDIDA-->
                        @if($inversionista->gen_status_id == 11 || $inversionista->gen_status_id == 15)
                        <a href="{{route('BandejaInversionista.edit',$inversionista->id)}}"  class="glyphicon glyphicon-edit btn btn-warning btn-sm" title="Modificar" aria-hidden="true">Corregir</a>
                        @endif

                        @if($inversionista->gen_status_id == 19)
                          <a href="{{route('DocAdicionalInvercionista.edit',$inversionista->id)}}"  class="glyphicon glyphicon-file btn btn-warning btn-sm" title="Cargar documentos" aria-hidden="true">Doc. Adicionales</a>
                        @endif
                        @if($inversionista->gen_status_id == 26)
                          <a href="{{action('PdfController@planillaSolInversionista',array('id'=>$inversionista->id))}}" class="glyphicon glyphicon-file btn btn-default btn-sm" title="PLANILLA RPI-01" aria-hidden="true"></a>
                          <a href="{{action('PdfController@CertificadoInversionista',array('id'=>$inversionista->id))}}" class="glyphicon glyphicon-file btn btn-success btn-sm" target="_blank" title="Certificado Inversionista" aria-hidden="true">Certificado</a>
                        @endif
                      
                      
                  
                        </td>
                    </tr>
                  @endforeach          
                </tbody>
            </table>
      </div>
    </div>
  </div>
</div>
@stop
