
<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Rutas de sesion
Route::group(['prefix' => '/', 'middleware' => 'Sesion'], function() {

    Route::get('/', function () {
        return view('welcome');
    });
    Route::get('/loguin', [
        //muestra el formulario de login de usuarios
        'as'=>'login','uses' => 'SesionController@index'

    ]);
    Route::get('/reestablecer-password', function () {
        return view('sesion.reesPassword');
    })->name('me.tutorials');

    Route::post('/resetPassword', [
        'uses' => 'SesionController@resetPassword'

    ]);
    Route::post('/storePassword', [
        'uses' => 'SesionController@storePassword'

    ]);
    Route::get('/registro', [
        //muestra el formulario de login de usuarios
        'uses' => 'SesionController@create'

    ]);
    Route::get('/passwordReset', [
        //muestra el formulario de login de usuarios
        'uses' => 'SesionController@passwordReset'

    ]);
    Route::post('/consultUser', [
        //valida la informacion ingresada en formulario de login
        'uses' => 'SesionController@consultUser'
    ]);

    Route::post('/storeLogin', [
        //valida la informacion ingresada en formulario de login
        'uses' => 'SesionController@storeLogin'
    ]);
    Route::get('/validarRif', function () {
        Session::forget('bandera');
        Session::forget('bandera_rif');
        Session::forget('bandera_tipo_persona');
        Session::forget('bandera_persona');
        return view('sesion.ValidarRif');
    });

    Route::get('/refresh_captcha', [
        'uses' => 'SesionController@refreshCaptcha'
    ]);
   /*Route::get('/RifEmp/{arancel}/{token}', [
        //postRegister es la que me inserta los datos en las dos tablas
        'uses' => 'SesionController@RifValido'
    ]);*/
    Route::get('/RifEmp', [
        //postRegister es la que me inserta los datos en las dos tablas
        'uses' => 'SesionController@RifValido'
    ]);
    Route::post('/RifEmp', [
        //postRegister es la que me inserta los datos en las dos tablas
        'uses' => 'SesionController@RifValido'
    ]);
    Route::get('/Arancel', [
        'uses' => 'SesionController@Arancel'
    ]);
    Route::get('/registroUsr', [
        //postRegister es la que me inserta los datos en las dos tablas
        'uses' => 'SesionController@RifValido'
    ]);
    Route::post('/registroUsr', [
        //postRegister es la que me inserta los datos en las dos tablas
        'uses' => 'SesionController@registroUsr'
    ]);
    /*********************************************************************/
    Route::post('/ProcesarNatural', [
        //postRegister es la que me inserta los datos en las dos tablas
        'uses' => 'SesionController@storeNatural'
    ]);
    /**********************************************************************/

    Route::get('/home', [
        'uses' => 'ExportadorController@index'
    ]);
    Route::get('/logout', ['as'=>'logout',

        'uses' => 'SesionController@logout'
    ]);
    Route::get('/ActividadEco/{id}', [
        'uses' => 'SesionController@ActividadEcoEmp'
    ]);

    Route::get('/minicipios/{id}', [
        'uses' => 'SesionController@getMunicipios'
    ]);
    Route::get('/parroquias/{id}', [
        'uses' => 'SesionController@getParroquias'
    ]);
    Route::resource('estado','EstadoController');
    Route::get('ActividadEcoInversionista/{id}', [
        'uses' => 'InversionistaExtController@ActividadEcoEmpnIvert'
    ]);
});

Route::group(['prefix' => 'admin', 'middleware' => ['auth','admin']], function() {
    Route::get('/', [
        'uses' => 'AdminController@index',
    ]);
    /********GESTION USUARIOS**************/
    Route::resource('AdminUsuario','GenUsuarioController');
    Route::resource('Administradores','AdministradoresController');
    Route::resource('AdministradoresSeniat','AdministradoresSeniatController');
   // Route::resource('UsuarioSeniat','UsuarioSeniatController');
    Route::resource('AdminUsuarioDetalle','GenUsuarioController');
    Route::resource('DetalleEmpresas','ReporteEmpresasController');
    /*******rutas de reportes para ER y DVD nota credito********/
    Route::resource('ReportesEmpresa','ReportEmpresaController');
    Route::resource('ReportesEr','ReportesErController');
    Route::resource('ReportesDVD','ReportesDVDController');
    Route::resource('ReportesDvdNotaCred','ReportesDvdNotaCredController');
    /*****rutas de reportes en excel******/
    Route::get('/ExcelEmpresasReportes', [
        'uses' => 'ReportEmpresaController@ExcelEmpresasReportes',
    ]);
     Route::get('/ExcelReportesEr', [
        'uses' => 'ReportesErController@ExcelReportesEr',
    ]);
     Route::get('/ExcelReportesDVD',[
        'uses' => 'ReportesDVDController@ExcelReportesDVD',
    ]);
      Route::get('/ExcelReportesDvdNotaCred',[
        'uses' => 'ReportesDvdNotaCredController@ExcelReportesDvdNotaCred',
    ]);
    Route::get('/ExcelDetalleEmpresas', [
        'uses' => 'ReporteEmpresasController@ExcelDetalleEmpresas',
    ]);
    Route::resource('DetalleProductos','ReporteProductosController');
    Route::get('/DetalleProductosExcel', [
        'uses' => 'ReporteProductosController@DetalleProductosExcel',
        'as'=>'DetalleProductosExcel',
    ]);
    Route::resource('ReporteSolicitudes','ReporteSolicitudesController');
    Route::get('/reportePdf', [
        'uses' => 'PdfController@CrearPdf',
    ]);
    Route::get('/Graficos', [
        'uses' => 'PdfController@Creargrafic',
    ]);
    Route::post('/Graficanual', [

        'uses' => 'PdfController@Creargrafic'
    ]);
    Route::get('/Graficanual', [

        'uses' => 'PdfController@Creargrafic'
    ]);
    Route::get('/reporteArancel', [
        'uses' => 'ReporteProductosController@Arancel',
    ]);
    Route::get('/denegadaExportadores', ['as' => 'denegadaExportadores', function () {
        return view('rutasDenegadas.admin1');
    }]);
    Route::get('NotificacionesEmail', [
        'uses' => 'NotificacionesController@create'
    ]);

    Route::post('NotificacionesEmail', [
        'uses' => 'NotificacionesController@NotificacionesEmail',
        'as' => 'enviarNotificacionesEmail'
    ]);

});

Route::group(['prefix' => 'exportador', 'middleware' => ['auth','exportadores']], function() {
    /********DATOS DE LA EMPRESA**************/
    Route::resource('DatosEmpresa','DatosEmpresaController');
    Route::get('/ActividadEco/{id}', [
        'uses' => 'SesionController@ActividadEcoEmp'
    ]);
    Route::get('/minicipios/{id}', [
        'uses' => 'SesionController@getMunicipios'
    ]);
    Route::get('/parroquias/{id}', [
        'uses' => 'SesionController@getParroquias'
    ]);
     Route::resource('DatosJuridicos','DatosJuridicosController');
     Route::resource('DatosAccionista','AccionistaController');
     Route::resource('UltimaExportacion','UltimaExportacionController');
     Route::resource('CasaMatriz','CasaMatrizController');
     Route::resource('ProductosJuridicos','ProductosJuridicosController');
     /***********ruta resource para los productos juridicos*************/
     Route::get('DatosAccionista/{id}/eliminarAccionista','AccionistaController@eliminarAccionista');
     Route::resource('RegisMercantil','RegisMercantilController');
     Route::resource('DatosAcceso','DatosAccesoController');
    Route::resource('DocumentosSoportes','DocumentosSoportesController');
     Route::resource('detSolicitud','DetProdSolicitudController');
     Route::get('/planillaEr', [
         'uses' => 'PdfController@planillaEr',
     ]);
     Route::get('/planillaTecno', [
         'uses' => 'PdfController@planillaTecno',
       ]);
     Route::get('/PlanillaDvd', [
         'uses' => 'PdfController@planillaDvd',

     ]);
      Route::get('/PlanillaDvdDesestimiento', [
         'uses' => 'PdfController@planillaDvdDes',

     ]);
    Route::get('/PlanillaDvdNotaDebito', [
         'uses' => 'PdfController@planillaNotaD',

     ]);
    Route::get('/PlanillaDvdNotaDebitoDesestimiento', [
         'uses' => 'PdfController@planillaDvdDesND',

     ]);
    Route::get('/PlanillaConsigDoc', [
         'uses' => 'PdfController@PlanillaConsigDoc',

     ]);

    /***********Ruta para obtener los paises ajax *************/
    Route::get('ajax/paises/all',['uses' => 'AjaxGenPaises@listarTodo']);
    Route::get('ajax/sectoreconomico',['uses' => 'AjaxGenSectorEconomico@listar']);
    Route::get('ajax/destinoinversion',['uses' => 'AjaxGenDestinoInversion@listar']);
    /***********Ruta resource para declaracion jurada de origen *************/
    Route::POST('ListaDeclaracionJO/storeComercializador',[
        'uses'=>'ListaDeclaracionJOController@storeComercializador',
        'as'=> 'ListaDeclaracionJO.storeComercializador'
    ]);
    Route::resource('ListaDeclaracionJO','ListaDeclaracionJOController');
    Route::get('DeclaracionJO/pdf',[
        'uses'=>'DeclaracionJOController@pdf',
        'as' => "DeclaracionJO.pdf"
        ]);

    Route::resource('DeclaracionJO','DeclaracionJOController');
    /** Ruta resource para la construccion de las planillas **/
    Route::resource('DJOPlanilla2','DJOPlanilla2Controller');
    Route::resource('DJOPlanilla3','DJOPlanilla3Controller');
    Route::resource('DJOPlanilla4','DJOPlanilla4Controller');
    Route::resource('DJOPlanilla5','DJOPlanilla5Controller');
    Route::resource('DJOPlanilla6','DJOPlanilla6Controller');
    /***********ruta resource para el certificado de origen *************/
    Route::resource('CertificadoOrigen','CertificadoController');
    Route::get('CertificadoOrigen/{id}/eliminarRegistroProd','CertificadOrigenController@deleteProducto');
    Route::get('CertificadoOrigen/{id}/eliminarRegistroCriterio','CertificadOrigenController@deleteCriterio');
    /********CAMBIO DE CONTRASEÑA**************/
    Route::resource('CambioContrasena','CambioContrasenaController');
    /********CAMBIO PREGUNTA DE SEGURIDAD**************/
    /*Route::get('/cambioPreguntaSeguridad', function(){

        return view('usuario.forms.cambioPreguntaSeguridad')->with('titulo','Cambio de Pregunta de Seguridad')->with('descripcion','Datos de Pregunta de Seguridad');

    });*/

    /*Route::post('/actualizarempresa', [
        'uses' => 'HomeController@accionform1'
    ]);
    */
    /********REGISTRO DE CONSIGNATARIO**************/
    Route::resource('consignatario','RegistroConsignatarioController');
    /********REGISTRO DE AGENTE ADUANAL**************/
    Route::resource('AgenteAduanal','AgenteAduanalController');
    /********REGISTRO DE DUA**************/
    Route::resource('Dua', 'DuaController');
    /********REGISTRO DE FACTURA**************/
    /********CAMBIO PREGUNTA DE SEGURIDAD*******/
     Route::resource('CambioPregunta','CambioPreSegController');
   /****************Registro de Factura -Productos***************/
    Route::resource('factura','GenFacturaController');
    Route::resource('productos','DetProdFacturaController');
    Route::get('productos/{id}/eliminar','DetProdFacturaController@eliminar');
    /******************** Routes Facturas -Tecnologia ****************************/
    Route::get('productos_tecno/{id}/eliminar','DetProdTecnologiaController@eliminar');
    Route::get('verificarFac','TipoSolicitudController@verificarFac');
    Route::resource('productos_tecno','DetProdTecnologiaController');
    /********REGISTRO DE PERMISO**************/
    Route::resource('Permisos','PermisoController');
    Route::resource('ProdutPermisos','ProdutPermisosController');
    Route::get('ProdutPermisos/{id}/eliminar','ProdutPermisosController@eliminar');
    /*****************************************/
     /********REGISTRO DE Nota de Credito**************/

    Route::resource('NCredito','NCreditoController');

     Route::get('/facturaNC', [
         'uses' => 'NCreditoController@facturaNC',

     ]);

    /************************************************/
    /********REGISTRO DE Nota de Debito**************/

    Route::resource('NDebito','NdebitoController');

     Route::get('/facturaND', [
         'uses' => 'NdebitoController@facturaND',

     ]);
    /********ANTICIPO DE EXPORTACION**************/
    /*Route::get('/datosempresa', function(){



    /********ANTICIPO DE EXPORTACION**************/
    Route::resource('Anticipo','GenAnticipoController');


    /********REGISTRO DE EXPORTACION**************/
    Route::resource('SolicitudER','TipoSolicitudController');
    Route::resource('FacturaSolicitud','FacturaSolicitudController');
    Route::get('FacturaSolicitud/{id}/eliminar','FacturaSolicitudController@eliminar');
    Route::resource('','FacturaSolicitudController');
    Route::get('factura-solicitud/{id}/delete-er', 'FacturaSolicitudController@deleteEr')->name('factura-solicitud.delete-er');
   


    /********REGISTRO DE EXPORTACION Financiamiento Solicitud**************/
    Route::resource('FinanciamientoSolicitud','FinancSolicitudController');
    Route::get('FinanciamientoSolicitud/{id}/edit','FinancSolicitudController@edit');

    Route::resource('productos_export','ProductosController');
    Route::get('productos_export/{id}/eliminar','ProductosController@eliminar');
    Route::get('productos_export/{id}/editar','ProductosController@editar');
    Route::POST('actualizar',[
        'uses'=>'ProductosController@actualizar',
        'as'=> 'productos_export_edit.actualizar'
    ]);


    /******************************************************************************/

    /************************Registro de DVD**************************************/

    Route::resource('DvdSolicitud','GenDVDSolicitudController');
    Route::resource('ListaVenta','ListaVentaController');
    Route::resource('VentaSolicitud','VentaSolicitudController');





    /**************************Documentos de DVD*************************************/

    Route::resource('DocumentosDVD','DocumentosDVDController');
    Route::get('documentos-dvd/{id}/delete-dvd', 'DocumentosDVDController@deleteDvd')->name('documentos-dvd.delete-dvd');


    //Ruta para  Eliminar Lista dinamica Doc 2 y 4
    Route::get('DocumentosDVD/{id}/eliminarDocDvd','DocumentosDVDController@eliminarDocDvd');



    Route::resource('ListaVentaND','ListaVentaNDController');

    Route::resource('VentanND','VentaNDController');


    Route::resource('DvdSolicitudND','ListaVentaNDController');



    /**************************Documentos de DVD*************************************/

    Route::resource('DocumentosDvdND','DocumentosDvdNDController');


    //Ruta para  Eliminar Lista dinamica Doc 2 y 4
    Route::get('DocumentosDvdND/{id}/eliminarDocND','DocumentosDvdNDController@eliminarDocND');






    /*************************Reportar dudas o Problemas***************************/
    Route::resource('Dudas','DudasController');
    
    Route::post('Dudas/delete/{id}',[
        'uses' => 'DudasController@destroy'
    ]);

    Route::put('Dudas/update/{id}',[
        'uses' => 'DudasController@update'
    ]);

    Route::get('Dudas/{gen_asistencia_usuario_id}',[
        'uses' => 'DudasController@edit'
    ]);

    /*inversionista extranjera*/
    Route::resource('BandejaInversionista','InversionistaExtController');//

     Route::POST('FormInversionista',[
        'uses'=>'InversionistaExtController@RedireccionarForm',
        'as'=> 'BandejaInversionista.FormInversionista'
    ]);
     //Para cuando falla el Vali
      Route::get('FormInversionista',[
        'uses'=>'InversionistaExtController@RedireccionarForm',
        'as'=> 'BandejaInversionista.FormInversionista'
    ]);



    Route::get('BandejaInversionista/ActividadEcoInversionista/{id}', [
        'uses' => 'InversionistaExtController@ActividadEcoEmpnIvert'
    ]);





    //Ruta para  Eliminar Lista dinamica Doc 2 y 4
    Route::get('BandejaInversionista/{id}/eliminarSoporte2','InversionistaExtController@eliminarSoporte');

    /////////////////////////////////////////////////////////////

      //Ruta para  Eliminar Lista dinamica empresareceptora 3 y 4
    Route::get('BandejaInversionista/{id}/eliminarSoporteDinamico','Planilla1DecInversionController@eliminarSoporte');



    Route::get('BandejaInversionista/{id}/eliminarAccionista','InversionistaExtController@eliminarAccionista');

    Route::get('BandejaInversionista/eliminarAccionista/{id}','InversionistaExtController@eliminarAccionista');


    /*Ruta PDF de la Solicitud Inversionista*/
    Route::get('/planillaSolInversionista', [
         'uses' => 'PdfController@planillaSolInversionista',
       ]);

      /*Ruta PDF de Recaudos*/
    Route::get('/planillaRecaudosInversionista', [
         'uses' => 'PdfController@planillaRecaudosInversionista',
       ]);

     /*Ruta PDF Certificado Inversionista*/
    Route::get('/CertificadoInversionista', [
         'uses' => 'PdfController@CertificadoInversionista',
       ]);
    /*Certificado de Declaracion de Inversiones*/
    Route::get('/CertificadoInversionRealizada',[
    'uses'=>'PdfController@CertificadoInversionRealizada',
    ]);

    /*CertificadoRegistroVuce*/
    Route::get('/CertificadoRegistroVuce',[
        'uses'=>'PdfController@CertificadoRegistroVuce',
    ]);



/*Certificado de Declaracion de Inversiones*/
    Route::get('/CertificadoInversionRealizadaDJIR',[
            'uses'=>'PdfController@CertificadoInversionRealizada',
     ]);


    /*Declaracion de Inversion Planilla 1*/
    Route::get('/DeclaracionJuradaInversionRealizadaP01',[
        'uses'=>'PdfController@DeclaracionJuradaInversionRealizadaP01',
    ]);

    /*Fin de la ruta de declaracion planilla 1*/



        /*Declaracion de Inversion Planilla 2*/
    Route::get('/DeclaracionJuradaInversionRealizadaP02',[
        'uses'=>'PdfController@DeclaracionJuradaInversionRealizadaP02',
    ]);

    /*Fin de la ruta de declaracion planilla 2*/




     /*Declaracion de Inversion Planilla 3*/
    Route::get('/DeclaracionJuradaInversionRealizadaP03',[
        'uses'=>'PdfController@DeclaracionJuradaInversionRealizadaP03',
    ]);







    /*inversion extranjera*/
    Route::resource('BandejaInversion','BandejaInversionController');

     ///////////Ruta para Declaración de Inversión//////////
       Route::POST('FormDeclaracionInversion',[
        'uses'=>'BandejaInversionController@RedirecFormDeclaracionInversion',
        'as'=> 'BandejaInversion.FormDeclaracionInversion'
    ]);
     //Para cuando falla el Validation
    Route::get('FormDeclaracionInversion',[
        'uses'=>'BandejaInversionController@RedirecFormDeclaracionInversion',
        'as'=> 'BandejaInversion.FormDeclaracionInversion'
    ]);

     //Para cuando esta incompleta
    Route::get('FormDeclaracionInversion/{gen_declaracion_inversion_id}',[
        'uses'=>'BandejaInversionController@RedirecFormDeclaracionInversionComplet',
        'as'=> 'BandejaInversion.FormDeclaracionInversionComplet'
    ]);

    Route::get('ActualizarDeclaracion/{gen_declaracion_inversion_id}',[
        'uses'=>'BandejaInversionController@ActualizarDeclaracion',
        'as'=> 'BandejaInversion.ActualizarDeclaracion'
    ]);


      ////////////////////////////////////////////////////////////
      ///////Ruta para 1era Tarjeta Planilla 1 Inversion//////////
    /*   Route::POST('storePlanilla1Inversion',[
        'uses'=>'BandejaInversionController@storePlanilla1Inversion',
        'as'=> 'BandejaInversion.storePlanilla1Inversion'
    ]); */
     //Para cuando falla el Validation
    Route::get('Planilla1Inversion/{gen_declaracion_inversion_id}',[
        'uses'=>'BandejaInversionController@Planilla1Inversion',
        'as'=> 'BandejaInversion.Planilla1Inversion'
    ]);

    Route::resource('savePlanilla1Inversion','Planilla1DecInversionController');

    //Actualizar Planilla 1
    Route::resource('actualizarPlanilla1Inversion','ActualizarPlanilla1DecInversionController');
    Route::get('actualizarPlanilla1Inversion/{gen_declaracion_inversion_id}/{declaracion_id}','ActualizarPlanilla1DecInversionController@Actualizar');

    //Actualizar Planilla 2
    Route::resource('actualizarPlanilla2Inversion','ActualizarPlanilla2DecInversionController');
    Route::get('actualizarPlanilla2Inversion/{gen_declaracion_inversion_id}/{declaracion_id}','ActualizarPlanilla2DecInversionController@Actualizar');

    //Actualizar Planilla 3
    Route::resource('actualizarPlanilla3Inversion','ActualizarPlanilla3DecInversionController');
    Route::get('actualizarPlanilla3Inversion/{gen_declaracion_inversion_id}/{declaracion_id}','ActualizarPlanilla3DecInversionController@Actualizar');

    Route::get('savePlanilla1Inversion/{id}/eliminarAccionistaInversion','Planilla1DecInversionController@eliminarAccionista');
    Route::get('savePlanilla1Inversion/{id}/eliminarArchivo','Planilla1DecInversionController@eliminarArchivo');
       ////////////////////////////////////////////////////////////
      ///////Ruta para 2da Tarjeta Planilla 2 Inversion//////////
    /*    Route::POST('storePlanilla2Inversion',[
        'uses'=>'BandejaInversionController@storePlanilla2Inversion',
        'as'=> 'BandejaInversion.storePlanilla2Inversion'
    ]); */
     //Para cuando falla el Validation
    Route::get('Planilla2Inversion/{gen_declaracion_inversion_id}',[
        'uses'=>'BandejaInversionController@Planilla2Inversion',
        'as'=> 'BandejaInversion.Planilla2Inversion'
    ]);

    Route::resource('savePlanilla2Inversion','Planilla2DecInversionController');
    /*Route::post('savePlanilla2Inversion',[
        'uses'=>'Planilla2DecInversionController@storePlanilla2',
        'as'=> 'savePlanilla2Inversion.storePlanilla2'
    ]);*/
      ///////////////////////////////////////////////////////////
      ///////Ruta para 3da Tarjeta Planilla 3 Inversion//////////
    /*   Route::POST('storePlanilla3Inversion',[
        'uses'=>'BandejaInversionController@storePlanilla3Inversion',
        'as'=> 'BandejaInversion.storePlanilla3Inversion'
    ]); */
     //Para cuando falla el Validation
    Route::get('Planilla3Inversion/{gen_declaracion_inversion_id}',[
        'uses'=>'BandejaInversionController@Planilla3Inversion',
        'as'=> 'BandejaInversion.Planilla3Inversion'
    ]);

    Route::resource('savePlanilla3Inversion','Planilla3DecInversionController');
      //////////////////////////////////////////////////////



    /*transferencia tecnologicainversionista extranjera*/
    Route::resource('BandejaTranfTecnologica','TransferenciaTecController');





    Route::get('denegadaAdmin',['as' => 'denegadaAdmin', function () {
        return view('rutasDenegadas.admin1');
    }]);

    /***Para Consultar por AJAX GenArancelNandina y GenArancelMercosur***/
    Route::get('ajax/arancelNandina/all',[
        'uses' => 'AjaxGenArancelNandina@listarTodo'
    ]);

    Route::get('ajax/arancelNandina/user',[
        'uses' => 'AjaxGenArancelNandina@listarAsociado'
    ]);

    Route::get('ajax/arancelMercosur/all',[
        'uses' => 'AjaxGenArancelMercosur@listarTodo'
    ]);

    Route::get('ajax/arancelMercosur/user',[
        'uses' => 'AjaxGenArancelMercosur@listarAsociado'
    ]);
    // ruta doc adicional djo
    Route::resource('DocAdicional','DocAdicionalDjoController');

    Route::get('DocDjo/{gen_declaracion_jo_id}',[
                'uses' => 'DocAdicionalDjoController@create'
    ]);

    Route::get('DocDjoEdit/{gen_declaracion_jo_id}',[
                'uses' => 'DocAdicionalDjoController@edit'
    ]);

    //Ruta Certicado de Origen

    Route::resource('CertificadoOrigen','CertificadOrigenController');

     Route::POST('FormCerticado',[
        'uses'=>'CertificadOrigenController@RedireccionarForm',
        'as'=> 'CertificadoOrigen.FormCerticado'
    ]);

      Route::get('FormCerticado',[
        'uses'=>'CertificadOrigenController@RedireccionarForm',
        'as'=> 'CertificadoOrigen.FormCerticado'
    ]);

    Route::resource('DocAdicionalInvercionista','DocAdicionalInvercionistaController');

    //reporte de pago
    Route::get('reportePagos/{tipo_pago}/{solicitud_id}',[
                'uses' => 'ReportePagoController@create'
    ]);

    Route::resource('Pagos','ReportePagoController');

    /*permiso especial bovino en pie*/
    Route::resource('permisoEspecialBovino','permisoEspecialBovinoController');
    /* pdf permiso especial bovino*/
    Route::get('/AutorizacionEspecialBovino', [
        'uses' => 'PdfController@AutorizacionEspecialBovino',
    ]);
    /*consulta tramites insai*/
    Route::resource('certificadosInsai','certificadosInsaiController');

    Route::get('/getDataInsai', [
        'uses' => 'certificadosInsaiController@getDataInsai',
    ]);

    Route::resource('certificadosZooEu','certificadosZooEuController');

    Route::get('certificadosZooEu/{id}/eliminarRegistro','certificadosZooEuController@deleteCertificado');

     Route::get('/certZooEu', [
        'uses' => 'PdfController@certZooEu',
    ]);









      /********************Certificado CNA***************************/


      /*CertificadoVuceCNA*/
    Route::get('/CertificadoVuceCna',[
        'uses'=>'PdfController@CertificadoVuceCna',
    ]);













    Route::resource('resguardoAduanero','ResguardoAduaneroController');






    /***********CVC*****************/
    Route::resource('corporacionCafe','CorporacionCafeController');



    Route::get('/CertificadoDemandaInterna',[
        'uses'=>'PdfController@CertificadoDemandaInterna',
        ]);

     Route::get('/CertificadoDemandaInternaSol',[
        'uses'=>'PdfController@CertificadoDemandaInternaSol',
        ]);

     /***********Exporta Facil*****************/

    Route::resource('CalcularEnvioIpostel','CalcularEnvioIpostelController');
    Route::resource('CalcularEnvioConviasa','CalcularEnvioConviasaController');
    Route::resource('CertExportaFacil','CertExportaFacilController');




       /*CertificadoVuceCVC1*/
        Route::get('/CertificadoExportaFacil',[
            'uses'=>'PdfController@CertificadoExportaFacil',
        ]);



});





Route::group(['prefix' => 'calificaciondjo', 'middleware' => ['auth']], function()
{

            Route::get('DJOPlanilla2/show',[
                'uses' => 'DJOPlanilla2Controller@show'
            ]);

            Route::get('DJOPlanilla3/show',[
                'uses' => 'DJOPlanilla3Controller@show'
            ]);

            Route::get('DJOPlanilla4/show',[
                'uses' => 'DJOPlanilla4Controller@show'
            ]);

            Route::get('DJOPlanilla5/show',[
                'uses' => 'DJOPlanilla5Controller@show'
            ]);

            Route::get('DJOPlanilla6/show',[
                'uses' => 'DJOPlanilla6Controller@show'
            ]);

            Route::get('DocAdicional/show',[
                'uses' => 'DocAdicionalDjoController@show'
            ]);

            Route::get('ajax/arancelMercosur/index',[
                'uses' => 'AjaxGenArancelMercosur@index'
            ]);

            Route::get('ajax/arancelNandina/index',[
                'uses' => 'AjaxGenArancelNandina@index'
            ]);

            // ruta para eliminar registros de pruebas
            Route::get('AnalisisDjo/eliminarRP{id?}',[
                'uses' => 'AnalisisCalificacionController@eliminarRP'
            ]);


            //boton ver observaciones
             Route::get('AnalisisDjo/observacion/{id}',[
                'uses' => 'AnalisisCalificacionController@verObservacion'
            ]);

            //Validar Si hay o No observaciones
            /* Route::get('AnalisisDjo/validarobser/{id}',[
                'uses' => 'AnalisisCalificacionController@validarObservacion'
            ]);*/

            Route::resource('AnalisisDjo','AnalisisCalificacionController');
            /********REGISTRO DE EXPORTACION**************/
            Route::resource('SolicitudERAnalista','TipoSolicitudAnalistaController');
            Route::resource('DvdSolicitudAnalista','GenDVDSolicitudAnalistaController');

            /** Obtener la observacion por ajax**/
            Route::get('DJOPlanilla2/observacion/{id?}', ['uses' => 'DJOPlanilla2Controller@obtenerObservacion']);
            Route::get('DJOPlanilla3/observacion/{id?}', ['uses' => 'DJOPlanilla3Controller@obtenerObservacion']);
            Route::get('DJOPlanilla4/observacion/{id?}', ['uses' => 'DJOPlanilla4Controller@obtenerObservacion']);
            Route::get('DJOPlanilla5/observacion/{id?}', ['uses' => 'DJOPlanilla5Controller@obtenerObservacion']);
            Route::get('DJOPlanilla6/observacion/{id?}', ['uses' => 'DJOPlanilla6Controller@obtenerObservacion']);
            Route::get('DocAdicional/observacion/{id?}', ['uses' => 'DocAdicionalDjoController@obtenerObservacion']);



            /** Guardar la observacion por ajax **/
            Route::post('DJOPlanilla2/observacion', ['uses' => 'DJOPlanilla2Controller@storeObservacion']);
            Route::post('DJOPlanilla3/observacion', ['uses' => 'DJOPlanilla3Controller@storeObservacion']);
            Route::post('DJOPlanilla4/observacion', ['uses' => 'DJOPlanilla4Controller@storeObservacion']);
            Route::post('DJOPlanilla5/observacion', ['uses' => 'DJOPlanilla5Controller@storeObservacion']);
            Route::post('DJOPlanilla6/observacion', ['uses' => 'DJOPlanilla6Controller@storeObservacion']);
            Route::post('DocAdicional/observacion', ['uses' => 'DocAdicionalDjoController@storeObservacion']);


            //Ruta Certicado de Origen

            Route::resource('AnalistaCertificadoOrigen','AnalistaCertificadOrigenController');

            //DudasAnalista
            Route::resource('DudasAnalistaSolicitud','GenDudasAnalistaController');
        
            Route::put('DudasAnalistaSolicitud/update/{id}',[
                'uses' => 'GenDudasAnalistaController@update'
            ]);

});

Route::group(['prefix' => 'AnalistaInversion', 'middleware' => ['auth']], function()
{

        Route::resource('BandejaAnalista','BandejaAnalistaController');

        Route::resource('AnalistInverExtjero','ListaAnalistaInverController');
        Route::resource('AnalistDeclaracionInversion','ListaInverExtranjeraController');
        Route::resource('AnalistTransfeTecno','ListaTransfeTecnoController');


});

Route::group(['prefix' => 'CoordinadorInversion', 'middleware' => ['auth']], function()
{

            Route::resource('BandejaCoordinador','BandejaCoordinadorController');

            Route::resource('ListaCoordExtranjero','ListaCoordExtranjeroController');
            Route::resource('ListaCoordInvExtjera','ListaCoordInvExtjeraController');
            Route::resource('ListaCoordTransfeTecno','ListaCoordTransfeTecnoController');

            Route::get('/planillaSolInversionista', [
            'uses' => 'PdfController@planillaSolInversionista',
            ]);

            Route::get('/CertificadoInversionista', [
            'uses' => 'PdfController@CertificadoInversionista',
            ]);


                 /*Certificado de Declaracion de Inversiones*/
            Route::get('/CertificadoInversionRealizada',[
            'uses'=>'PdfController@CertificadoInversionRealizada',
            ]);

             /*Declaracion de Inversion Planilla 1*/
            Route::get('/DeclaracionJuradaInversionRealizadaP01',[
                'uses'=>'PdfController@DeclaracionJuradaInversionRealizadaP01',
            ]);

            /*Fin de la ruta de declaracion planilla 1*/



                /*Declaracion de Inversion Planilla 2*/
            Route::get('/DeclaracionJuradaInversionRealizadaP02',[
                'uses'=>'PdfController@DeclaracionJuradaInversionRealizadaP02',
            ]);

            /*Fin de la ruta de declaracion planilla 2*/




             /*Declaracion de Inversion Planilla 3*/
            Route::get('/DeclaracionJuradaInversionRealizadaP03',[
                'uses'=>'PdfController@DeclaracionJuradaInversionRealizadaP03',
            ]);

            Route::resource('userManagerInversiones','usersCoordInvertController');

 });





Route::group(['prefix' => 'verificaciondjo', 'middleware' => ['auth']], function()
{

    Route::resource('coordinadorDjo','CoordinadorVerificacionController');

    Route::resource('AdministradorDjo','AdministradorDJOController');





    //Ruta Certicado de Origen

    Route::resource('CoordinadorCertificadoOrigen','CoordinadorCertificadOrigenController');

 Route::get('CertificadoOrigenColombia/pdf',[
        'uses'=>'PdfController@CertificadoOrigenColombiapdf',
        'as' => "CertificadoOrigen.pdf"
        ]);

     /**Ruta de Certificado de Origen Bolivia **/
    Route::get('CertificadoOrigenBolivia/pdf',[
        'uses'=>'PdfController@CertificadoOrigenColombiapdf',
        'as' => "CertificadoOrigen.pdf"
        ]);

    /**Fin de ruta de Certificado de Origen Bolivia **/

 /*Certificado de Origen Chile falta agregar el /pdf a la ruta*/
    Route::get('CertificadoOrigenChile/pdf',[
        'uses'=>'PdfController@CertificadoOrigenColombiapdf',
        'as' => "CertificadoOrigen.pdf"
    ]);





 /**********Reporte Excel**************/


    Route::get('excelColombia/excel',[
        'uses'=>'ReporteExcelController@excelColombia',
        'as' => "excelColombia.excel"
        ]);

      /*****Ruta de certificado de Origen Aladi ****/
        Route::get('CertificadoOrigenAladi/pdf',[
        'uses'=>'PdfController@CertificadoOrigenColombiapdf',
        'as' => "CertificadoOrigen.pdf"
    ]);


  /******************* RUTA DE CERTIFICADO DE MERCOSUR***************/

        Route::get('CertificadoOrigMercosur/pdf',[
        'uses'=>'PdfController@CertificadoOrigenColombiapdf',
        'as' => "CertificadoOrigen.pdf"
    ]);
/**************** Ruta de Certificado de Origen Peru************/
        Route::get('CertificadoOrigenPeru/pdf',[
        'uses'=>'PdfController@CertificadoOrigenColombiapdf',
        'as' => "CertificadoOrigen.pdf"
    ]);
/**************** Ruta de Certificado de Origen Cuba************/
        Route::get('CertificadoOrigenCuba/pdf',[
        'uses'=>'PdfController@CertificadoOrigenColombiapdf',
        'as' => "CertificadoOrigen.pdf"
    ]);

/***************Ruta de Certificado Turquia*****************/

                
Route::get('CertificadoOrigenTurquia/pdf',[
        'uses'=>'PdfController@CertificadoOrigenColombiapdf',
        'as' => "CertificadoOrigen.pdf"
    ]);

/***************Ruta de Certificado UE*****************/
  
Route::get('CertificadoOrigenUE/pdf',[
        'uses'=>'PdfController@CertificadoOrigenColombiapdf',
        'as' => "CertificadoOrigen.pdf"
    ]);

});

Route::group(['prefix' => 'AnalistaPagos', 'middleware' => ['auth']], function()
{
    Route::resource('bandejaRecepcionPagos', 'BandejaRecepcionPagosController');
    Route::resource('bandejaRecepcionPagosDvd', 'BandejaRecepcionPagosDvdController');
    Route::post('validarPagosDvd', ['uses' => 'BandejaRecepcionPagosDvdController@store']);
});


Route::group(['prefix' => 'AnalistaResguado', 'middleware' => ['auth']], function()
{
        Route::resource('BandejaAnalistaResguardo','AnalistaResguardoController');
        Route::resource('ListaAnalistResguardo','ListaAnalistResguardoController');
        Route::get('CertificadoReguardoAduanero/pdf',[
        'uses'=>'PdfController@CertificadoReguardoAduanero',
        'as' => "CertificadoReguardoAduanero.pdf"
        ]);
});

Route::group(['prefix' => 'AnalistaCna', 'middleware' => ['auth']], function()
{
        Route::resource('BandejaAnalistaCna','AnalistaCnaController');
        Route::resource('ListaAnalistCna','ListaAnalistCnaController');
        Route::get('/CertificadoVuceCna',[
        'uses'=>'PdfController@CertificadoVuceCna',
        ]);

});

Route::group(['prefix' => 'AnalistaCvc', 'middleware' => ['auth']], function()
{
        Route::resource('BandejaAnalistaCvc','BandejaAnalistaCvcController');
        Route::resource('ListaAnalistCvc','ListaAnalistCvcController');
      /*CertificadoVuceCVC1*/
        Route::get('/CertificadoDemandaInternaSatisfecha',[
            'uses'=>'PdfController@CertificadoDemandaInterna',
            'as'=>"CertificadoDemandaInternaSatisfecha.pdf"
        ]);
         /*CertificadoVuceCVC2*/
        Route::get('/CertificadoDemandaInternaSolCvc',[
            'uses'=>'PdfController@CertificadoDemandaInternaSol',
        ]);

});

Route::group(['prefix' => 'Seniat', 'middleware' => ['auth']], function()
{
        Route::resource('UsuarioSeniat','UsuarioSeniatController');
        Route::get('UsuarioSeniat-lista','UsuarioSeniatController@lista');
        Route::resource('AnalistaSeniat','AnalistaSeniatController');
        Route::get('AnalistaSeniat-lista','AnalistaSeniatController@lista')->name('analista');
        Route::get('AnalistaSeniat-show/{id}','AnalistaSeniatController@mostar');
        Route::resource('ViceministroSeniat','ViceministroSeniatController');
        Route::get('ViceministroSeniat-lista','ViceministroSeniatController@lista')->name('ViceministroSeniat');
        Route::get('ViceministroSeniat-show/{id}','ViceministroSeniatController@show');
});
Route::group(['prefix' => 'AdministradorResguardoCna', 'middleware' => ['auth']], function()
{

        Route::resource('BandejaAdminResgCna','AdministradorResguardoCnaController');


        Route::resource('ListaAdminResgCna','ListaAdminResgCnaController');

        Route::resource('userManagerResgCna','userManagerResgCnaController');

        Route::get('/CertificadoVuceCna',[
            'uses'=>'PdfController@CertificadoVuceCna',
        ]);

        Route::get('CertificadoReguardoAduanero/pdf',[
            'uses'=>'PdfController@CertificadoReguardoAduanero',
            'as' => "CertificadoReguardoAduanero.pdf"
        ]);


});

Route::group(['prefix' => 'IntranetAnalista', 'middleware' => ['auth']], function()
{

    Route::resource('AnalistaIntranet','AnalistaIntranetController');
    Route::get('/planillaEr', [
        'uses' => 'PdfController@planillaEr',
    ]);
    Route::get('/planillaTecno', [
        'uses' => 'PdfController@planillaTecno',
    ]);
    Route::get('/PlanillaDvd', [
        'uses' => 'PdfController@planillaDvd',
    ]);
    Route::get('/PlanillaDvdDesestimiento', [
        'uses' => 'PdfController@planillaDvdDes',
    ]);
    Route::get('solEvaluadasAnalista','AnalistaIntranetController@solEvaluadasAnalista')->name('solEvaluadasAnalista');
    Route::get('/PlanillaDvdNotaDebito', [
        'uses' => 'PdfController@planillaNotaD',

    ]);
    Route::get('/PlanillaDvdNotaDebitoDesestimiento', [
        'uses' => 'PdfController@planillaDvdDesND',

    ]);
});
Route::group(['prefix' => 'IntranetCoordinador', 'middleware' => ['auth']], function()
{
    
    Route::resource('CoordinadorIntranet','CoordinadorIntranetController');
    Route::resource('RecepcionSolCoordIntra','RecepcionSolCoordIntraController');
    Route::patch('AsignacionAnalista','RecepcionSolCoordIntraController@AsignacionAnalista')->name('AsignacionAnalista');
    Route::get('AsignadasAnalistas','RecepcionSolCoordIntraController@AsignadasAnalistas')->name('AsignadasAnalistas');
    Route::get('SolEvaluadasAnalistas','RecepcionSolCoordIntraController@SolEvaluadasAnalistas')->name('SolEvaluadasAnalistas');
    Route::post('AnalisisCoordinador','RecepcionSolCoordIntraController@AnalisisCoordinador')->name('AnalisisCoordinador');
    Route::get('ReasignarSolicitud/{id}','RecepcionSolCoordIntraController@ReasignarSolicitud')->name('ReasignarSolicitud');
    Route::get('/planillaEr', [
        'uses' => 'PdfController@planillaEr',
    ]);
    Route::get('/planillaTecno', [
        'uses' => 'PdfController@planillaTecno',
    ]);
    Route::get('/PlanillaDvd', [
        'uses' => 'PdfController@planillaDvd',
    ]);
    Route::get('/PlanillaDvdDesestimiento', [
        'uses' => 'PdfController@planillaDvdDes',
    ]);
    /****************Rutas de modulos de reportes************/
    Route::resource('ReportesErIntra','ReportesErIntraController');
    Route::resource('ReportesDVDIntra','ReportesDVDIntraController');
    Route::resource('ReportesDvdNotaCredIntra','ReportesDvdNotaCredIntraController');

     Route::get('/ExcelReportesEr', [
        'uses' => 'ReportesErIntraController@ExcelReportesEr',
    ]);

    Route::get('/ExcelReportesDVD',[
        'uses' => 'ReportesDVDIntraController@ExcelReportesDVD',
    ]);


      Route::get('/ExcelReportesDvdNotaCred',[
        'uses' => 'ReportesDvdNotaCredIntraController@ExcelReportesDvdNotaCred',
    ]);
    /**************** FIN Rutas de modulos de reportes************/
    Route::get('/PlanillaDvdNotaDebito', [
        'uses' => 'PdfController@planillaNotaD',

    ]);
    Route::get('/PlanillaDvdNotaDebitoDesestimiento', [
        'uses' => 'PdfController@planillaDvdDesND',

    ]);
    Route::resource('ReporteSolicitudesIntranet','ReporteSolicitudesIntranetController');
    Route::get('/ExcelReportesRecepcionSolicitudes', [
        'uses' => 'ReporteSolicitudesIntranetController@ExcelReportesRecepcionSolicitudes',
    ]);
    
    Route::get('/EliminarRegistroFactura', [
        'uses' => 'GenFacturaController@EliminarRegistroFacturaUpdate',
    ]);
    
});

/*Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');*/
