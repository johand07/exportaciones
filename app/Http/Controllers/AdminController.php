<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\GenUsuario;
use App\Models\DetUsuario;
use App\Models\GenTipoUsuario;

class AdminController extends Controller
{
    protected function index(Request $request){

    $users=DetUsuario::where('bactivo',1)->get();
    return 	view('AdminUsuario.index',compact('users'));

    }

}
