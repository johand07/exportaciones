@extends('templates/layoutlte')
@section('content')

{{Form::open(['route'=>'detSolicitud.store','method'=>'POST','id'=>'formEr'])}}


<div class="row"><h3 align="center">Productos de las exportación asociada</h3></div>

<table class="table table-bordered table-condensed" id="fields_tec">

   <tr>
   <th>#</th>
   <th>Número de Factura </th>
   <th>Código Arancelario</th>
   <th>Descripción Arancelaria</th>
   @if(Session::get('tipo_solicitud_id')==1)
   <th>Descripción Comercial</th>
   @endif
   <th>Cantidad </th>
   <th>Precio Unitario </th>
   <th>Monto Total</th>

 </tr>
 @php $data=array(); @endphp

 {{Session::put('id_detFactura',$data)}}

  @foreach($productos as $key=>$valor)



  <tr>

  <td>{{$key+1}}</td>

   @if(Session::get('tipo_solicitud_id')==1)

  <td>{{$valor->factura->numero_factura}}</td>

  <td>{{$valor->codigo_arancel}}</td>

 <td>{{$valor->descripcion_arancelaria}}</td>

  <td>{{$valor->descripcion_comercial}}</td>

  <td>{{$valor->cantidad_producto}}</td>

  <td>{{$valor->precio_producto}}</td>

  <td>{{$valor->monto_total}}</td>

  @else



  <td>{{$valor->factura->numero_factura}}</td>

  <td>{{$valor->codigo_arancel}}</td>

 <td>{{$valor->descripcion}}</td>

  <td>{{$valor->cantidad}}</td>

  <td>{{$valor->precio}}</td>

  <td>{{$valor->valor_fob}}</td>

  @endif


</tr><br><br>

  @endforeach



</table>

{{form::close()}}

<div class="col-md-12" align="center">
 <input type="submit" onclick="validarER()" id="submit" class="btn btn-success" value="Enviar">
</div>


@endsection


<script type="text/javascript">


 function validarER(){
  
    $('#formEr').submit();
 }


</script>
