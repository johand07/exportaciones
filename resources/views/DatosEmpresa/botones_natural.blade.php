@extends('templates/layoutlte')
@section('content')

@php  use App\Models\DetUsuario; 
        
    $data=DetUsuario::where('gen_usuario_id',Auth::user()->id)->first();

@endphp
 <div class="row">
            <div class="col-md-12 text-center">
                 <h3 class="section-title h3">LISTA DEL EXPORTADOR</h3>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 text-right"><a href="{{action('PdfController@CertificadoRegistroVuce')}}" class="btn btn-danger">Descargar Certificado Registro VUCE</a>
            </div>
        </div><br>
<section id="team" class="pb-3">
    <div class="container">
       
       
        
        <div class="row">

        @if($data->produc_tecno==2)
        <div class="col-xs-12 col-sm-6 col-md-3">
                <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                    <div class="mainflip">
                        <div class="frontside">
                            <div class="card">
                                <div class="card-body text-center">
                                     <br><br><br>
                                    <p><img class=" img-fluid" src="/img/productos.png" style="width: 80px; height: 65px;"></p>
                                    <h4 class="card-title">Actualizar Productos</h4>                             
                                    <a href="#" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="backside">
                            <div class="card">
                                <div class="card-body text-center mt-4">
                                    <br><br><br>
                                   <h4 class="card-title">Actualizar Productos</h4>
                                    <p class="card-text">Podrá actualizar los datos de los productos de la exportación.</p>
                                    <a href="{{url('exportador/productos_export')}}" class="btn btn-primary" style="color:#FFFFFF;">ACTUALIZAR <br>PRODUCTOS</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
            <!-- Team member -->
            <div class="col-xs-12 col-md-6 col-md-3">
                <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                    <div class="mainflip">
                        <div class="frontside">
                            <div class="card">
                                <div class="card-body text-center">
                                     <br><br><br>
                                    <p><img class=" img-fluid" src="/img/datos_basic.png" style="width: 80px; height: 65px;"></p>
                                    <h4 class="card-title">Datos Básicos</h4>
                                    <!--<p class="card-text">Gestión de Médicos</p>-->
                                    <a href="#" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="backside">
                            <div class="card">
                                <div class="card-body text-center mt-4">
                                    <br><br><br>
                                    <h4 class="card-title">Datos Básicos</h4>
                                    <p class="card-text">Podrá actualizar los datos basicos de su registro.</p>
                                    <a href="{{route('DatosEmpresa.edit',Auth::user()->id)}}" class="btn btn-primary" style="color:#FFFFFF;">DATOS BÁSICOS</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ./Team member -->
            <div class="col-xs-12 col-sm-6 col-md-3">
                <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                    <div class="mainflip">
                        <div class="frontside">
                            <div class="card">
                                <div class="card-body text-center">
                                     <br><br><br>
                                    <p><img class=" img-fluid" src="/img/ult_exportacion.png" style="width: 80px; height: 65px;"></p>
                                    <h4 class="card-title">Última Exportación</h4>                             
                                    <a href="#" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="backside">
                            <div class="card">
                                <div class="card-body text-center mt-4">
                                    <br><br><br>
                                    <h4 class="card-title">Última Exportación</h4>
                                    <p class="card-text">Podrá actualizar los datos de la ultima exportación.</p>
                                    <a href="{{route('UltimaExportacion.edit',Auth::user()->id)}}" class="btn btn-primary" style="color:#FFFFFF;">DATOS  DE LA<br> ÚLTIMA EXPORTACIÓN</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ./Team member -->
            <!-- Team member -->
        </div>
    </div>
</section>

@stop



















