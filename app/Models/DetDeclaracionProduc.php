<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DetDeclaracionProduc extends Model
{
    //
    protected $table= 'det_declaracion_produc';

    protected $fillable = [

           'gen_declaracion_jo_id',
           'productos_id',
           'estado_p2',
           'estado_p3',
           'estado_p4',
           'estado_p5',
           'estado_p6',
                   
    ];

    
    public function declaracionJO() {

     return $this->hasOne('App\Models\GenDeclaracionJO','id','gen_declaracion_jo_id');

    }
    public function rProducto() {

     return $this->hasOne('App\Models\Productos','id','productos_id');

    }
    public function rPlanilla2() {

     return $this->hasOne('App\Models\Planilla2','det_declaracion_produc_id','id');

    }
    public function rPlanilla3() {

     return $this->hasOne('App\Models\Planilla3','det_declaracion_produc_id','id');

    }
    public function rPlanilla4() {

     return $this->hasOne('App\Models\Planilla4','det_declaracion_produc_id','id');

    }
    public function rPlanilla5() {

     return $this->hasOne('App\Models\Planilla5','det_declaracion_produc_id','id');

    }
    public function rPlanilla6() {

     return $this->hasOne('App\Models\Planilla6','det_declaracion_produc_id','id');

    }
    public function rDeclaracionPaises() {

     return $this->hasMany('App\Models\DetDeclaracionProducPais','det_declaracion_produc_id','id');

    }
    
}
