<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DocumentosDvdNdBdv extends Model
{
    protected $table = 'documentos_dvd_nd_bdv';

    	protected $fillable = [

           
           'gen_dvd_nd_id',
           'cat_documentos_id',
           'ruta_doc_dvd',
           'bactivo',
    ];

    protected $date =['created_at','updated_at'];

 

    public function dvdnd() {

     return $this->hasOne('App\Models\GenDvdNd','id','gen_dvd_nd_id');

    }

    public function documentos() {

     return $this->hasOne('App\Models\CatDocumentos','id','cat_documentos_id');

    }
}
