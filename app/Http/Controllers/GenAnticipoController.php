<?php

namespace App\Http\Controllers;

use App\Models\GenAnticipo;
use App\Models\GenConsignatario;
use App\Models\GenDivisa;
use App\Http\Requests\GenAnticipoRequest;

use Illuminate\Http\Request;
use Session;
use Auth;
use Laracasts\Flash\Flash;
use Alert;

class GenAnticipoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $titulo="Anticipo";
        $descripcion="Anticipos Registrados";

        $anticipo=GenAnticipo::where('gen_usuario_id',Auth::user()->id)->where('bactivo',1)->get();
        //dd($anticipo);
        return view('Anticipo.index',compact('anticipo','titulo','descripcion'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\sResponse
     */
    public function create()
    {
        $titulo="Anticipo";
        $descripcion="Registro de Anticipo";
        
        $consig=GenConsignatario::all()->pluck('nombre_consignatario','id');

        $divisa=GenDivisa::all()->pluck('ddivisa_abr','id');
          return view('Anticipo.create',compact('consig','titulo', 'descripcion','divisa'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response//
     */
    public function store(GenAnticipoRequest $request)

    {
        $fecha_actual = date("d-m-Y");
        if ((strtotime($request->fecha_anticipo)) > (strtotime($fecha_actual))) {
           Alert::warning('No puede ser superior a la Fecha Actual', 'Error en la Fecha de Anticipo !')->persistent("OK");
        return redirect()->action('GenAnticipoController@create');
        } else {

        GenAnticipo::create($request->all());


        Alert::success('REGISTRO EXITOSO','Anticipo agregado')->persistent("Ok");
        return redirect()->action('GenAnticipoController@index');
        }
    }
    

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\GenAnticipo  $genAnticipo
     * @return \Illuminate\Http\Response
     */
    public function show(GenAnticipo $genAnticipo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\GenAnticipo  $genAnticipo
     * @return \Illuminate\Http\Response
     */
    public function edit(GenAnticipo $genAnticipo, $id)
    {
        $titulo="Anticipo";
        $descripcion="Modificar Datos";
        $edit_anticipo=GenAnticipo::find($id);//find es para buscar en la tabla el Anticpo que tenga este id

        $consig=GenConsignatario::all()->pluck('nombre_consignatario','id');
        $divisa=GenDivisa::all()->pluck('ddivisa_abr','id');
        return view('Anticipo.edit',compact('edit_anticipo','consig','titulo','descripcion', 'divisa'));//Retorna Carpeta y Edit formulario

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\GenAnticipo  $genAnticipo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, GenAnticipo $genAnticipo,$id)
    {
        $fecha_actual = date("d-m-Y");
        if ((strtotime($request->fecha_anticipo)) > (strtotime($fecha_actual))) {
           Alert::warning('No puede ser superior a la Fecha Actual', 'Error en la Fecha de Anticipo !')->persistent("OK");
           $titulo="Anticipo";
        $descripcion="Modificar Datos";
        $edit_anticipo=GenAnticipo::find($id);//find es para buscar en la tabla el Anticpo que tenga este id

        $consig=GenConsignatario::all()->pluck('nombre_consignatario','id');
        $divisa=GenDivisa::all()->pluck('ddivisa_abr','id');
        return view('Anticipo.edit',compact('edit_anticipo','consig','titulo','descripcion','divisa'));
        } else {
            GenAnticipo::find($id)->update($request->all());
           //dd($request->all());
           Alert::success('Actualización Exitosa!', 'Actulización Exitosa.')->persistent("OK");
           return redirect()->action('GenAnticipoController@index')->with($id);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\GenAnticipo  $genAnticipo
     * @return \Illuminate\Http\Response
     */
    public function destroy(GenAnticipo $genAnticipo,$id)
    {

       $eliminar_anticipo=GenAnticipo::find($id)->update(['bactivo' => 0, 'updated_at' => date("Y-m-d H:i:s") ]);
            if ($eliminar_anticipo) {
               return 1;
            }else{
                return 2;
            }     
    }
}
