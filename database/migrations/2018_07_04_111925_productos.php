<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Productos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()  
    {
        Schema::create('productos', function (Blueprint $table) {
           $table->engine = 'InnoDB';
           $table->increments('id');
           $table->unsignedInteger('gen_usuario_id')->index()->nullable();
           $table->foreign('gen_usuario_id')->references('id')->on('gen_usuario');
           $table->unsignedInteger('gen_unidad_medida_id')->index()->nullable();
           $table->foreign('gen_unidad_medida_id')->references('id')->on('gen_unidad_medida');
           $table->string('codigo', 100)->nullable();
           $table->string('descripcion', 200)->nullable();
           $table->string('descrip_comercial', 200)->nullable();
           $table->string('cap_insta_anual', 200)->nullable();
           $table->string('cap_opera_anual', 200)->nullable();
           $table->string('cap_alamcenamiento', 200)->nullable();
           $table->string('cap_exportacion', 200)->nullable();
           $table->boolean('bactivo')->default(1);
           $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('productos');
    }
}
