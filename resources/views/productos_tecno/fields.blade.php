

<!-- Pestañas -->
<div class="warpper">
  <input class="radio" id="one" name="group" type="radio">
  <input class="radio" id="two" name="group" type="radio" checked>
  <input class="radio" id="three" name="group" type="radio">
  <div class="tabs">
    <a href="{{route('factura.edit',@$idfact)}}" aria-hidden="true"> 
      <label class="tab" >Factura 
      </label>
  </a>
    <label class="tab" id="two-tab" for="two">Productos</label>
  </div>
  <div class="panels">
    <div class="panel" id="two-panel">
    <div class="panel-title" style="color:#1e3165">        
        @if(isset($nfact))
        <span class="glyphicon glyphicon-pencil"></span>
            Factura N°: {{$nfact}}  -  Monto FOB registrado: <b style="color:red">{{$monto}}</b>
        @else
        <span class="glyphicon glyphicon-asterisk"></span>
           Factura Nueva - Monto FOB registrado: <b style="color:red">{{$monto}}</b>
          
        @endif
        </div>
      
  <table class="table table-bordered"  id="fields_tec">



	  <tr>
	    <th>Cód Arancelario </th>
	    <th>Descrip del Producto</th>
	    <th>Cantidad </th>
	   
	    <th>Precio Unitario </th>
	    <th>Valor FOB</th>
      <th>Unidad de Medida</th>
        @if(isset($productos))
          <th><button  name="add" type="button" id="add" value="add more" class="btn btn-success" >+</button></th>
         @else
          <th><button  name="add" type="button" id="add" value="add more" class="btn btn-success">+</button></th>
        @endif

        @if(isset($productos_tecno))
   <td>
    </tr>
   @endif      </tr>



@if(!isset($productos_tecno))


  <tr id="row0">

	  <td>{!!Form::text('cod[]','0000.00.0',['class'=>'form-control','readonly' => 'readonly'])!!}</td>
	  <td>{!!Form::text('descripcion[]',null,['class'=>'form-control','id'=>'descripcion','onkeypress'=>'return soloLetras(event)'])!!}</td>
	  <td>{!!Form::text('cantidad[]',null,['class'=>'form-control format','id'=>'cantidadFob','onInput'=>"calcularMonto('row0','cantidadFob','precioFob','montoFob')"])!!}</td>
	  <td>{!!Form::text('precio[]',null,['class' => 'form-control format text-right','id'=>'precioFob','onInput'=>"calcularMonto('row0','cantidadFob','precioFob','montoFob')"])!!}</td>

	  <td>{!!Form::text('valor_fob[]',null,['class' => 'form-control format text-right','readOnly'=>'readOnly','id'=>'montoFob'])!!}</td>
	  <td>{!!Form::select('unidad_medida_id[]',$medidas,null,['class'=>'form-control','placeholder'=>'Seleccione','id'=>'unidad_medida_id'])!!}</td>

	  <td></td>
  </tr>



@else

   @foreach($productos_tecno as $key =>$valor)

  @php $fila=$key+1; @endphp


  <tr id="row{{$key+1}}">

	   <td>{!!Form::text('cod[]','0000.00.0',['class'=>'form-control','readonly' => 'readonly'])!!}</td>

       <td>
	   {!!Form::hidden('gen_factura_id',$valor->gen_factura_id)!!}
	   {!!Form::text('descripcion[]',$valor->descripcion,['class'=>'form-control','onkeypress'=>'return soloLetras(event)'])!!}</td>

	  <td>{!!Form::text('cantidad[]',$valor->cantidad,['class'=>'form-control format','id'=>'cantidadFob','onInput'=>"calcularMonto('row$fila','cantidadFob','precioFob','x')"])!!}</td>

	
	  <td>{!!Form::text('precio[]',$valor->precio,['class' => 'form-control format text-right','id'=>'precioFob','onInput'=>"calcularMonto('row$fila','cantidadFob','precioFob','x')"])!!}</td>

	  <td>{!!Form::text('valor_fob[]',$valor->valor_fob,['class' => 'form-control format text-right','readOnly'=>'true','id'=>'montoFob'])!!}</td>
    <td>{!!Form::select('unidad_medida_id[]',$medidas,$valor->unidad_medida_id,['class'=>'form-control','placeholder'=>'Seleccione','id'=>'unidad_medida_id'])!!}</td>

	 <td>
	   @php $x[$key]=$valor->id; @endphp
	   {{Session::put('facturas_id',$x)}}
	   {!!Form::hidden('id[]',$valor->id)!!}
	   <button  name="remove" type="button" id="{{$key+1}}" value="" class="btn btn-danger btn-remove">x</button></td>

</tr><br>

  @endforeach




@endif


</table><br>
<span><b style="color: red;">Nota:</b> Los campos cantidad y precio usan como separador decimal el punto
                (.)
                <b style="color: red;">Ej: 3985.54 </b></span>
              <br><br>

@if(!isset($productos_tecno))

 <div class="col-md-12" align="center">
   <input type="submit" name="submit" class="btn btn-info"  id="id_boton" value="Enviar"/>
</div><br><br>


@else

 <div class="row">

    <div class="col-md-12">

      <div class="col-md-4"></div>
 
    <div class="col-md-3" align="center">


    <input type="submit" name="submit" class="btn btn-success" id="id_boton" value="Enviar"/>

    </div>

 </div>
</div><br>


@endif




<div id="monto_data" data-monto={{$monto}}></div>
<br><br>
          </div>
{{Form::close()}}




@push('productos_tecno')

  $(document).on('click','.btn-remove',function(){

        var id_boton= $(this).attr("id");
               var id=$(this).prev().val();

               if(id > 1){

                 swal({
                     title: "¿Eliminar?",
                     text: "¿Está seguro de eliminar este Registro?",
                     type: "warning",
                     showCancelButton: true,
                     confirmButtonColor: "#DD6B55",
                     confirmButtonText: "Si!, Borrarlo!",
                     cancelButtonText: "No, Cancelar!",
                     closeOnConfirm: false,
                     closeOnCancel: false,
                     showLoaderOnConfirm: true
                     },

    function(isConfirm){
         if (isConfirm) {

              $.ajax({

               'type':'get',
               'url':'eliminar',
               'data':{ 'id':id},
               success: function(data){

                $('#row'+id_boton).remove();
                if(data==1){

                   swal("Eliminación exitosa","Producto Eliminado ", "success");

                  }

                 }

             });


         }else{

              swal("Cancelado", "No se ha procesado la eliminación", "warning");

            }

        });


        }else{

            $('#row'+id_boton).remove();
         }
 });


 calcularMonto('row1','cantidadFob','precioFob','x');

@endpush