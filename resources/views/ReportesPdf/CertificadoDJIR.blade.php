<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>CERTIFICADO DE DECLARACION JURADA DE INVERSIÓN REALIZADA</title>
    <style>
      header {
                position: fixed;
                top: -30px;
                left: 0px;
                right: 0px;
                height: 50px;

                /** Extra personal styles **/
                /*background-color: #03a9f4;
                color: white;
                text-align: center;
                line-height: 35px;*/
            }
        body{
            text-align:center;
           font-family: Arial;
                 color: #000;
        }
        th{
            font-size:10px;
            background-color:#818286;
            color: #fff;
        }
        td{
            font-size:10px;
            border: 1px solid #818286;
        }
        .td2{
            font-size:10px;
            border: 0px solid #818286;
            width: 55%;
            text-align:center;
            font-size:14px;
        }
        .td3{

            border: 0px solid #818286;
            width: 15%;
            text-align:center;

        }
        .td4{

            border: 0px solid #818286;


        }
        table{
            border: 1px solid #818286;
        }
        .table2{
            border: 0px solid #818286;
        }

       table > tr > td {

          text-align: center;
        }
      .tabla > h4,h6{

        text-align: left;
        margin-bottom: 0;
        font-size:10px;

      }
      .tabla > table > thead > tr > th:nth-child(1){

         width: 30%;
      }
      .tabla > table > .tipo > tr:nth-child(1){

        border-top: 0;
      }

      .tabla2 > table > thead > tr > th:nth-child(0){

         width: 10%;
      }
       .centrar{
            text-align: center;
        }

        .titulo{
            
            font-weight: bold;
        }
         .derecha{
            text-align: left;

        }

         .borde{
            border: 1px solid #fff;
        }
        

    </style>
</head>
    <body>
  <div class="centrar espacio-superior">
        <table style="width:100%;" class="table2">
          <tbody>
            <tr>
              <td class="td3">
                <img class="img-responsive" style="width:200px; height: 40px;" src='img/MIPPCOEXIN.png'>
              </td>
           <td class="nro-correlativo borde" style="width: 30%; font-size:80%; text-align: left;">
            <td class="nro-correlativo borde" style="width: 30%; font-size:80%; text-align: left;">
               <td class="borde">
                  <img class="img-responsive borde" style="width: 300px; height: 40px; text-align: right;" src='img/cintillo.png'>
              </td>
            </tr>
          </tbody>
        </table>
    </div><br>
      <table style="width:100%;" class="table2">
          <tr align="text-right">
             <td class="td3">
                  <b><img src="data:image/png;base64,{!! base64_encode(QrCode::format('png')->size(350)->generate(URL::to('/api/VerificacionDJIR/'.$solicitud_de_inversionista->id))) !!}" width="160px" height="145px"></b>
            </td>
            
           
            <td class="borde"></td>
            <td class="borde">
             <table>
               <tr>
                <td class="titulo right" align="left"><b style="color: black; font-size: 10;">Nº MPPEFyCE-VCEyPI-DJIR: {{$solicitud_de_inversionista->num_declaracion ? $solicitud_de_inversionista->num_declaracion :''}}</b></td>
               </tr>
             </table>
         </td>
          </tr> 
      </table>
    </div><br><br>
    <div class="centrar titulo text-normal espacio-superior"><b style="font-family: Arial background:color:#000">COMPROBANTE DE RECEPCIÓN DE <br> DECLARACIÓN JURADA DE INVERSIÓN REALIZADA</b></div>
<p align="justify" style="font-size:16px;">El Ministerio del Poder Popular de Economía, Finanzas y Comercio Exterior, a través del Despacho de Viceministro para el Comercio Exterior y Promoción de Inversiones, de conformidad con lo dispuesto en la Ley Constitucional de Inversión Extranjera Productiva, publicada en la Gaceta Oficial de la República Bolivariana de Venezuela N° 41.310, del 29 de diciembre de 2017, certifica que la PERSONA NATURAL O JURÍDICA seguidamente identificada ha consignado los recaudos exigidos para que su <u><strong>Declaración Jurada de Inversión Realizada</strong></u> en la República Bolivariana de Venezuela sea incorporada bajo control posterior en el <u><strong>Sistema Único de Registro de Inversión (SURI)</strong></u>, conforme al siguiente detalle:</p>
<div class="tabla">
  <table  style="width:100%">
    <thead>
      <tr>
        <th style="width:50%">NOMBRE o RAZÓN SOCIAL del INVERSIONISTA</th>
        <th style="width:25%">RIF o PASAPORTE </th>
        <th style="width:25%">PAÍS de ORIGEN </th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>{{ucwords($users->razon_social ? $users->razon_social:'')}} <br><br> </td>
        <td>{{$users->rif ? $users->rif:''}} <br><br> </td>
        <td>{{$users->estado ? $users->estado->estado :''}} <br><br> </td>
      </tr>
    </tbody>
  </table>
   <table  style="width:100%">
    <thead>
      <tr>
        <th style="width:50%">RAZÓN SOCIAL DE LA EMPRESA RECEPTORA</th>
        <th style="width:25%">N° de RIF </th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>{{$solicitud_de_inversionista->rPlanillaDjir01 ? $solicitud_de_inversionista->rPlanillaDjir01->razon_social_solicitante : ''}} <br><br>
         <td>{{$solicitud_de_inversionista->rPlanillaDjir01 ? $solicitud_de_inversionista->rPlanillaDjir01->rif_solicitante : ''}}<br><br> </td> </td>
      </tr>
    </tbody>
  </table>
  <table style="width:100%">
    <thead>
      <tr>
         <th style="width:25%">SECTOR o RUBRO </th>
      </tr>
    </thead>
    <tbody>
      <tr>
        
          <td> 
            @foreach($sectorproducteconomico as $sector)
              {{$sector->sectorProdEco ? $sector->sectorProdEco->nombre_sector: ''}} ,      
            
            @endforeach
          </td>
      </tr>
    </tbody>
  </table>
  <table  style="width:100%">
    <thead>
      <tr>
        <th style="width:50%">REPRESENTANTE LEGAL EN VENEZUELA</th>
        <th style="width:50%">C.I. o PASAPORTE</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>{{$solicitud_de_inversionista->rPlanillaDjir01 ? $solicitud_de_inversionista->rPlanillaDjir01->nombre_repre :''}}{{$solicitud_de_inversionista->rPlanillaDjir01 ? $solicitud_de_inversionista->rPlanillaDjir01->apellido_repre: ''}}<br><br></td>
        <td>{{$solicitud_de_inversionista->rPlanillaDjir01 ? $solicitud_de_inversionista->rPlanillaDjir01->numero_doc_identidad:''}}<br><br></td>
      </tr>
    </tbody>
  </table>
<br>
  <div class="justify text-normal" style="font-size:16px;" align="text-justify">La validez del presente certificado es por un (01) año periodo por el cual deberá ser actualizado con los avances, modificaciones y descripción situacional de la inversión realizada  . <br><br>

  En la cuidad de Caracas, {{$Fecha}}.
  </div><br><br>


  <div class="justify centrar">
    <b>{{$datosfirmante->nombre_firmante ? $datosfirmante->nombre_firmante: ''}} {{$datosfirmante->apellido_firmante ? $datosfirmante->apellido_firmante:''}}</b><br>
    <b>{{$datosfirmante->cargo_firmante ? $datosfirmante->cargo_firmante:''}}</b><br>
    <b>{{$datosfirmante->gaceta_firmante ? $datosfirmante->gaceta_firmante:''}}</b><br>
  </div><br><br>

   <div class="text-center" style="font-size:10px;">La validez de este comprobante puede verificarse atrevas de <a href="https://www.vuce.gob.ve" target="_blank">www.vuce.gob.ve</a> accediendo al modulo de Inversionista. <br> La información y recaudo suministrados en esta Declaración de su solicitante son meramente declarativos.
  </div>

    </div>
</div>

    


      
     

      



