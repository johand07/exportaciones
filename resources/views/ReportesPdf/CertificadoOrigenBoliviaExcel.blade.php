<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>BOLIVIA-VENEZUELA</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/style_bolivia.css">
</head>
<body>
    
    <!--div class="derecha titulo text-medio"><b style="color:red">N°</b> del Certificado <span class="">{{ $certificado->num_certificado}}</span></div-->
<!--Certificado-->
    <div class="centrar titulo text-normal espacio-superior">
        <table border="0" class="ancho-full">
            <tr>
                <td></td>
                <td class="ancho-medio centrar" colspan="3">
                    <div class="text-normal">CERTIFICADO DE ORIGEN</div>
                </td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td class="ancho-medio centrar" colspan="2">
                    <div class="text-normal" align="center">ACUERDO DE COMERCIO PLURINACIONAL DE BOLIVIA
                    </div>
                </td>
                <td></td>
            </tr>
        </table>
       </div>
        <div class="centrar espacio-superior">
        <table border="0" class="ancho-full">
            <tr>
                <td class="ancho-medio centrar" colspan="2">
                    <div class="text-normal titulo">(1) PAÍS EXPORTADOR: <u>{{$certificado->pais_exportador}}</u></div>
                </td>
                 <td class="ancho-medio centrar" colspan="2">
                    <div class="text-normal titulo">(2) PAÍS IMPORTADOR: <u>{{$certificado->pais_importador}}</u></div>
                </td>
                <td></td>
            </tr>
        </table>
    </div>
    <!--Productos-->
     <div class="centrar espacio-superior">
        <table class="ancho-full borde table">
            <tr class="text-11">
               <td class="centrar top titulo borde" style="width: 17%;">(3) No. de Orden</td>
                <td class="centrar top titulo borde" style="width: 3%;">(4) Código Arancelario</td>
                <td class="centrar top titulo borde" style="width: 30%;" colspan="1">(5) Denominación de las Mercancías</td>
                <td class="centrar top titulo borde" style="width: 15%;" colspan="1">(6) Peso o Cantidad</td>
                <td class="centrar top titulo borde" style="width: 15%;" colspan="1">(7) Valor FOB en (U$S)</td>
            </tr>
           
            @foreach($detalleProduct as $key => $value)
            
                <tr class="text-medio">
                    <td class="centrar top borde">{{$value->num_orden}}</td>
                    <td class="centrar top borde">{{$value->codigo_arancel}}</td>
                    <td class="centrar top borde" colspan="1">{{$value->denominacion_mercancia}}</td>
                    <td class="centrar top borde" colspan="1">{{$value->unidad_fisica}}</td>
                    <td class="centrar top borde" colspan="1">{{$value->valor_fob}}</td>
                </tr>
            @endforeach
            @if(count($detalleProduct)<=1)
                @for($auxConut = 0; $auxConut<=3-count($detalleProduct); $auxConut++)
                 <tr class="text-medio">
                     <td class="centrar top">&nbsp;</td>
                    <td class="centrar top">&nbsp;</td>
                    <td class="centrar top">&nbsp;</td>
                    <td class="centrar top">&nbsp;</td>
                    <td class="centrar top">&nbsp;</td>
                    <td class="centrar top">&nbsp;</td>
                </tr>
                @endfor
            @endif
        </table>
    </div>
    <table class="ancho-full borde table">
        <tr class="text-medio">
            <td class="centrar top titulo" style="width: 50%;" colspan="5">
                <b align="center">(8) DECLARACION DE ORIGEN</b>
            </td>
        </tr>
        <tr class="text-medio">
            <td class="centrar top titulo" style="width: 50%;" colspan="5">
                DECLARAMOS que las mercancías indicadas en el presente formulario, correspondientes a la(s) Factura(s) Comercial(es) No. <u>{{ $detdeclaracionCert[0]->numero_factura }}de fecha <u>{{ $detdeclaracionCert[0]->f_fac_certificado }}</u> cumplen con lo establecido en las Normas de Origen del presente Acuerdo, de conformidad con el siguiente desglose.</u>
            </td>
        </tr>
    </table>
    <!--Productos-->
  <!--Detalle declaracion-->
    <div class="centrar espacio-superior">
        <table class="ancho-full borde table">
            <tr class="text-medio">
                <td class="centrar top titulo borde" style="width: 20%;">(9) No. de Orden</td>
                <td class="centrar top titulo borde" style="width: 80%;" colspan="4"><b align="center">(10) Normas</b></td>
            </tr>
            @foreach($detdeclaracionCert as $key =>$value1)
            
                 <tr class="text-center">
                    <td class="centrar top text-medio">{{$value1->num_orden_declaracion}}</td>
                    <td class="centrar top text-medio" colspan="4">{{$value1->normas_criterio}}</td>
                </tr>

            @endforeach

            @if(count($detdeclaracionCert)<=1)
                @for($auxConut = 0; $auxConut<=3-count($detdeclaracionCert); $auxConut++)
                <tr class="text-medio">
                    <td class="centrar top">&nbsp;</td>
                    <td class="centrar top">&nbsp;</td>
                </tr>
                @endfor
            @endif
        
        </table>
    </div>

    <div class="centrar espacio-superior">
        <table class="ancho-full table borde">
            <tr class="text-medio">
                <td class="top titulo borde text-medio" colspan="3">(11) EXPORTADOR O PRODUCTOR</td>
                 <td class="top titulo borde text-medio" colspan="2" style="width:20%;">(12) Sello y firma del Productor o Exportador</td>
            </tr>
           <tr class="text-medio">
                <td class="top borde" colspan="3">11.1 Nombre o Razón Social: {{ $certificado->GenUsuario->DetUsuario->razon_social }}</td>
                <td></td>
            </tr>
           <tr class="text-medio">
                <td class="top borde" colspan="3">11.2 Dirección: {{ $certificado->GenUsuario->DetUsuario->direccion }}</td>
                <td></td>
            </tr>
            <tr class="text-medio">
                <td class="top borde" colspan="3">11.3 Fecha: {{ $certificado->fecha_emision_exportador}}</td>
                <td></td>
            </tr>
            <tr class="text-medio">
                <td class="top titulo borde" colspan="3">(13) IMPORTADOR</td>
                <td></td>
            </tr>
            <tr class="text-medio">
                <td class="top borde" colspan="3">13.1 Nombre o Razón Social: {{ strtoupper($importadorCertificado->razon_social_importador) }}</td>
                <td></td>
            </tr>
            <tr class="text-medio">
                <td class="top borde" colspan="3">13.2 Dirección: {{ strtoupper($importadorCertificado->direccion_importador) }}</td>
                <td></td>
            </tr>
            <tr class="text-medio">
                <td class="top borde" colspan="3">(14) Medio de Transporte: {{ ($importadorCertificado->medio_transporte_importador) }}</td>
                <td></td>
            </tr>

            <tr class="text-medio">
                <td class="top borde" colspan="3">(15) Puerto o Lugar de Embarque: {{ ($importadorCertificado->lugar_embarque_importador) }}</td>
                <td></td>
            </tr>
        </table>
        <table class="ancho-full borde table">
            <tr class="text-medio">
                <td class="centrar top titulo borde" style="width: 15%;" colspan="5"><b>(16)<b>OBSERVACIONES</b>
                </td>
            </tr>
            <tr>
                <td class="text-medio borde" colspan="5" style="height: 30%;">{{ $certificado->observaciones}}</td>
            </tr>
        </table>
    </div>
     <div class="centrar espacio-superior">
        <table class="ancho-full borde table">
            <tr class="text-medio">
                <td class="top titulo borde table" colspan="2">(17) CERTIFICACIÓN DEL ORIGEN</td>
                <td class="top titulo borde table" colspan="3">(18) Sello y firma de funcionario habilitado y sello de la Autoridad <br>Competente:</td>
            </tr>
            <tr class="text-medio">
                <td class="top text-small borde" colspan="2"><div>Certifico la veracidad de la presente Declaracion en la ciudad de:</td>
            </tr>
            <tr>
                <td colspan="2" class="borde text-medio" style="height:50%;">
                    <span class="under-dotted espacio-superior">________________________________________</span><br>
                </td>
            </tr>
            <tr>
                <td colspan="2" class="borde text-medio" style="height:50%;">
                  A los:    <u>{{date('d')}}/</u><u>{{date('m')}}/</u><u>{{date('Y')}}</u>
                </td>
            </tr>
            <tr class="text-medio" style="height: 50%;">
                <td class="top text-small borde table" colspan="2"><div class="">(19) Nombre de la Autoridad Competente:<br><br><span class=""></span>Ministerio del Poder Popular Para el Comercio Exterior <br> e Inversion Internacional</div></td>
            </tr>
        </table>
    </div>
    <div class="centrar espacio-superior">
     <table class="ancho-full borde table">
        <tr class="text-medio">
            <td class="centrar top titulo" style="width: 15%;" colspan="5"> Referencias (1) Esta columna indica el orden en que se individualizan las mercancias comprendidas en el presente certificado. En caso de ser insuficiente, se continuara la individualizacion de las mercancias en ejemplares suplementarios de este certificado,numerados correlativamente.
            </td>
        </tr>
        <tr class="text-medio">
            <td class="centrar top titulo" style="width: 15%;" colspan="5">
               (2) En esta columna se identifica la Norma de Origen que cumple cada mercancia individualizada por su numero de orden
            </td>
        </tr>
        <tr class="text-medio">
            <td class="centrar top titulo" style="width: 15%;" colspan="5">
              Notas:(a) El formulario no podra presentar raspadura, tachaduras o enmiendas. (b) El formulario solo sera valido si todos sus campos, excepto el de "Observaciones", estuvieran debidamente llenos.  
            </td>
        </tr>
    </table>
    </div>
    <br>
</body>
</html>
