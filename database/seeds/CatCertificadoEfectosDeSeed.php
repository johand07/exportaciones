<?php

use Illuminate\Database\Seeder;
use App\Models\CatCertificadoEfectosDe;

class CatCertificadoEfectosDeSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $catCertifEfectosDe = [
            [
                'id'         => 1,
                'nombre'       => 'Productos Destinados al Consumo Humano',
                'created_at' => '2021-12-22 13:59:08',
                'updated_at' => '2021-12-22 13:59:08',
            ],
            [
                'id'         => 2,
                'nombre'       => 'Animales Acuaticos Vivos Destinados al Consumo Humano',
                'created_at' => '2021-12-22 13:59:08',
                'updated_at' => '2021-12-22 13:59:08',
            ],
            [
                'id'         => 3,
                'nombre'       => 'Industria Conservera',
                'created_at' => '2021-12-22 13:59:08',
                'updated_at' => '2021-12-22 13:59:08',
            ],

            [
                'id'         => 4,
                'nombre'       => 'Transformación Ulterior',
                'created_at' => '2021-12-22 13:59:08',
                'updated_at' => '2021-12-22 13:59:08',
            ],
        ];

        CatCertificadoEfectosDe::insert($catCertifEfectosDe);
    }
}
