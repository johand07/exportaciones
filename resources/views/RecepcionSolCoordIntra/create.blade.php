@extends('templates/layoutlte_coordinador_intranet')
@section('content')
{{Form::open(['route'=>'RecepcionSolCoordIntra.store','method'=>'POST','id'=>'form1']) }}
<br>
<div class="container">
    <div class="row">
        <div class="col-md-12">
        <div class="col-md-2"></div>
        <div class="col-md-8"><br><br>
            <div class="input-daterange" id="datepicker">
                <span><b>Desde:</b> </span>
                <input type="text" class="form-control" name="desde" id="fecha">
                <br><span><b>Hasta:</b> </span>
                <input type="text" class="form-control" name="hasta" id="fecha">
            </div><br>
            <div class="form-group">
                {!! Form::label('', 'Tipo de Solicitud') !!}
                {!!Form::select('tipo_solicitud',$tiposol,null,['placeholder'=>'Seleccione el tipo de Solicitud','class'=>'form-control','id'=>'tipo_solicitud']) !!}
           </div>
            <div class="form-group">
                {!! Form::label('', 'Tipo de Documento') !!}
                {!!Form::select('tipo_documento',$tipodoc,null,['placeholder'=>'Seleccione un tipo de documento','class'=>'form-control','id'=>'tipo_documento']) !!}
            </div>
        </div>
        <div class="col-md-2"></div>
        </div>
    </div><br>
    <div class="row">
        <div class="col-md-12">
        <div class="col-md-4"></div>
            <div class="col-md-4 text-center">
              <div class="form-group">
                <a href="#" class="btn btn-primary">Cancelar</a>
                <input class="btn btn-success" type="submit" name="" value="Enviar" onclick="generarforma1()">
            </div>
        </div>
        <div class="col-md-4"></div>
        </div>
    </div>
</div>

{{Form::close()}}
@stop
<script>
    /* Validación Generar forma1*/
function generarforma1() {

var mens=[
    'Estimado usuario. Debe ingresar el rango de fecha a consultar para generar  el reporte',
    'Estimado usuario. Debe seleccionar el Tipo de Solicitud para generar  el reporte',
    'Estimado usuario. Debe seleccionar el Tipo de Documento para generar  el reporte',
]

function forma1(input, mensaje,num) {
    if ($("#"+input).val()=="") {
        swal("Por Favor!", mensaje, "warning")
        $("#"+input).focus();
        key=1;
        $('#ok'+num).remove()
        $("#"+input).css('border', '1px solid red').after('<span id="ok'+num+'" style="color:red" class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span>');
        return 1;
    }else{
        $('#ok'+num).remove()
        $("#"+input).css('border', '1px solid green').after('<span id="ok'+num+'" style="color:green" class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span>');
        return 0;
    }
}

var key=0;
    $("#form1").submit(function() {
        key = forma1('fecha', mens[0],0)
        if(key == 1){return false}
        key = forma1('tipo_solicitud', mens[1],1)
        if(key == 1){return false}
        key = forma1('tipo_documento', mens[2],2)
        if(key == 1){return false}

        if (key==0) {
            return true;
        }
    });
}
</script>