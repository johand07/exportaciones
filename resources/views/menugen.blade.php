
<!-- Sidebar Menu -->
<?php  use App\Models\DetUsuario;
$dua=DetUsuario::where('gen_usuario_id',Auth::user()->id)->first();
?>

      <ul class="sidebar-menu" data-widget="tree">
        <li class="header"><a href="{{url('/home')}}"><span class="glyphicon glyphicon-home"></span>Menu Principal</a></li>
        <!-- Optionally, you can add icons to the links
        <li class="active"><a href="#"><i class="fa fa-link"></i> <span>Lingeneral</span></a></li>
        <li><a href="#">  <i class="fa fa-link"></i> <span>Another Link</span></a></li>-->

      <li class="treeview">
          <a href="#"><i class="glyphicon glyphicon-link"></i> <span>Trámites Institucionales</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
          </a>
          <ul class="treeview-menu">

            <!-- <li class="treeview">
              <a href="#"><i class="glyphicon glyphicon-link"></i> <span>Minec</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="http://www.minec.gob.ve/" target="_blank">Siger</a></li>
                {{--<li><a href="http://registrosigefor.minec.gob.ve/view/index.php" target="_blank">Sigefor</a></li>--}}
              </ul>
            </li> -->
            <li class="treeview">
              <a href="#"><i class="glyphicon glyphicon-link"></i> <span>Vitrina Venezuela </span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="https://vitrinavenezuela.com/registro-prueba/" target="_blank">Registro</a></li>
                {{--<li><a href="http://registrosigefor.minec.gob.ve/view/index.php" target="_blank">Sigefor</a></li>--}}
              </ul>
            </li>
            <!--<li class="treeview">
              <a href="#"><i class="glyphicon glyphicon-link"></i> <span>SAIME </span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="https://tramites.saime.gob.ve/index.php?r=site/login" target="_blank">Tramites</a></li>
                {{--<li><a href="http://registrosigefor.minec.gob.ve/view/index.php" target="_blank">Sigefor</a></li>--}}
              </ul>
            </li>
            <li class="treeview">
              <a href="#"><i class="glyphicon glyphicon-link"></i> <span>Saren </span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="https://citas.saren.gob.ve/" target="_blank">Citas Saren</a></li>
                {{--<li><a href="http://registrosigefor.minec.gob.ve/view/index.php" target="_blank">Sigefor</a></li>--}}
              </ul>
            </li> -->
            
            <li class="treeview">
              <a href="#"><i class="glyphicon glyphicon-link"></i> <span>Organismos internacionales </span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="https://pymesgrandesnegocios.org/" target="_blank">Pymes Latinas</a></li>
                {{--<li><a href="http://registrosigefor.minec.gob.ve/view/index.php" target="_blank">Sigefor</a></li>--}}
              </ul>
            </li>
            <li class="treeview">
              <a href="#"><i class="glyphicon glyphicon-link"></i> <span>Bolipuerto </span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="https://www.bolipuertos.gob.ve/?page_id=3270" target="_blank">Servicio Electónico</a></li>
                {{--<li><a href="http://registrosigefor.minec.gob.ve/view/index.php" target="_blank">Sigefor</a></li>--}}
              </ul>
            </li>

            <li class="treeview">
              <a href="#"><i class="glyphicon glyphicon-link"></i> <span>Insai</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="{{route('certificadosInsai.index')}}">Certificados Fitosanitario</a></li>
                {{--<li><a href="{{route('certificadosInsai.index')}}">Certificados Zoosanitario</a></li>--}}
                <li><a href="http://runsai.insai.gob.ve/" target="_blank">Runsai</a></li>
              </ul>
            </li>

            <li class="treeview">
              <a href="#"><i class="glyphicon glyphicon-link"></i> <span>Industria</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="https://sigesic.industrias.gob.ve/sistema/signin/?next=/sistema/" target="_blank">Permiso de Industrias</a></li>
                
              </ul>
            </li>

            <li class="treeview">
              <a href="#"><i class="glyphicon glyphicon-link"></i> <span>Min. de Alimentación</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="https://taquilla.minpal.gob.ve/new/login.asp?id=1" target="_blank">Permiso de Alimentación</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="#"><i class="glyphicon glyphicon-link"></i> <span>Contraloria Sanitaria</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="#" target="_blank">....</a></li>
              </ul>
            </li>

            {{--<li class="treeview">
              <a href="#"><i class="glyphicon glyphicon-link"></i> <span>Minería y Petróleo</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="https://sigdme.desarrollominero.gob.ve/" target="_blank">Permiso de Petróleo y Minería</a></li>
              </ul>
            </li> --}}

            <li class="treeview">
              <a href="#"><i class="glyphicon glyphicon-link"></i> <span>Pesca</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="{{route('certificadosZooEu.index')}}">Certificados ZooSanitario</a></li>
                {{--<li><a href="https://ripac.insopesca.gob.ve/login" target="_blank">Permiso de Pesca y Acuicultura</a></li>--}}
                <li><a href="http://minpesca.gob.ve/inicio-de-sesion/" target="_blank">Permiso de Pesca y Acuicultura</a></li>

              </ul>

            </li>
            <li class="treeview">
              <a href="#"><i class="glyphicon glyphicon-link"></i> <span>Seniat</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="{{route('resguardoAduanero.index')}}">Resguardo Aduanero y CNA</a></li>
                
              </ul>

            </li>
            <li class="treeview">
              <a href="#"><i class="glyphicon glyphicon-link"></i> <span>Corp. Venezolana del Café</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="{{route('corporacionCafe.index')}}">Certificado Café CVC</a></li>
              </ul>
            </li>


            <li class="treeview">
              <a href="#"><i class="glyphicon glyphicon-link"></i> <span>Exporta Facil</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="{{route('CalcularEnvioIpostel.create')}}">Ipostel</a></li>
                {{--<li><a href="{{route('certificadosInsai.index')}}">Certificados Zoosanitario</a></li>--}}
                <li><a href="{{route('CalcularEnvioConviasa.create')}}">Conviasa</a></li>

                <li><a href="{{route('CertExportaFacil.index')}}">Certificado</a></li>
              </ul>
             
            </li>

            <li class="treeview">
              <a href="#"><i class="glyphicon glyphicon-link"></i> <span>Sencamer</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="https://sigesca.sencamer.gob.ve/login" target="_blank">Sigesca</a></li>
                
                <li><a href="http://www.sencamer.gob.ve/consulta-certificado/" target="_blank">Constancia de Registro</a></li>

                <li><a href="http://www.sencamer.gob.ve/?q=content/certificados-firmados-0" target="_blank">Certificados Firmados</a></li>
              </ul>
             
            </li>
            <li class="treeview">
              <a href="#"><i class="glyphicon glyphicon-link"></i> <span>Sunagro</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="https://sistema.sunagro.gob.ve/" target="_blank">SICA</a></li>
              </ul>
            </li>


          </ul>
      </li>
      <li class="treeview">
          <a href="#"><i class="glyphicon glyphicon-file"></i> <span>Declaracion Jurada</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{url('exportador/ListaDeclaracionJO')}}">Declaración</a></li>
          </ul>
      </li>
       <li class="treeview">
          <a href="#"><i class="glyphicon glyphicon-list-alt"></i> <span>Certificado de Origen</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{url('exportador/CertificadoOrigen')}}">Certificado</a></li>
          </ul>
      </li>
 
@if(!empty($dua) && !empty($dua->invers==1))
      <li class="treeview">
          <a href="#"><i class="glyphicon glyphicon-paste"></i> <span>Inversiones Extranjeras</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">

            <li><a href="{{url('exportador/BandejaInversionista')}}">Solicitud de Potencial Inversionista</a></li>
             <li><a href="{{url('exportador/BandejaInversion')}}">Declaración Jurada de Inversión <br>
Realizada</a></li>
           {{--<li><a href="{{url('exportador/BandejaTranfTecnologica')}}">Transferencia Tecnológica</a></li>--}}

          </ul>
      </li>
@endif

@if(!empty($dua) && ($dua->export==1 || $dua->produc==1 || $dua->comerc==1))
      <li class="treeview">
          <a href="#"><i class="glyphicon glyphicon-tags"></i> <span>Datos de la Exportación</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
          </a>

          <ul class="treeview-menu">

            <li><a href="{{url('exportador/consignatario')}}">Registro Comprador / Proveedor</a></li>
            <li><a href="{{url('exportador/AgenteAduanal')}}">Registro de Agente de Aduana</a></li>
          {{-- @if(!empty($dua->produc_tecno) && $dua->produc_tecno!=1) --}}
            <li><a href="{{url('exportador/Dua')}}">Registro de DUA </a></li>
          {{-- @endif --}}
           {{-- <li><a href="{{url('exportador/Permisos')}}">Registro de Permiso</a></li>--}}
            <li><a href="{{url('exportador/factura')}}">Registro de Factura</a></li>
            <li><a href="{{url('exportador/NCredito')}}">Registro de Nota de Crédito</a></li>
            <li><a href="{{url('exportador/NDebito')}}">Registro de Nota de Débito</a></li>
          </ul>
      </li>

      <li class="treeview">
        <a href="#"><i class="glyphicon glyphicon-screenshot"></i> <span>Registro de Operación</span>
          <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
        </a>
        <ul class="treeview-menu">
          <!--<li><a href="{{url('exportador/Anticipo')}}">Anticipo de Exportación</a></li>-->
          <li><a href="{{url('exportador/SolicitudER')}}">Registro de Exportación</a></li>
        </ul>
      </li>

      <li class="treeview">
        <a href="#"><i class="glyphicon glyphicon-random"></i> <span>Registro de Venta de <br>Divisa</span>
          <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="{{url('exportador/DvdSolicitud')}}">Exportación Realizada</a></li>
          <li><a href="{{url('exportador/DvdSolicitudND')}}">Notas de Debito</a></li>
          {{--  <li><a href="#">Carga en Lote</a></li>
          <li><a href="#">Historial de Lotes</a></li>--}}
        </ul>
      </li>
@endif
      <li class="treeview">
        <a href="#"><i class="glyphicon glyphicon-transfer"></i> <span>Asistencia al Usuario</span>
          <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
        </a>
        <ul class="treeview-menu">
          <!--<li><a href="{{url('exportador/Dudas')}}">Reportar Duda o Problema </a></li>-->
          <li><a href="{{route('Dudas.index')}}">Reportar Duda o Problema </a></li>

        </ul>
      </li>
      @if(!empty($exportaGanado) && $exportaGanado == 1)
        <li class="treeview">
          <a href="#"><i class="glyphicon glyphicon-folder-open"></i> <span>Autorización Especial</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
          </a>
          <ul class="treeview-menu">

            <li><a href="{{route('permisoEspecialBovino.index')}}">Autorización especie bovina</a></li>
          </ul>
        </li>
      @endif


      <li class="treeview">
      <a href="{{route('DatosEmpresa.index')}}"><i class="glyphicon glyphicon-cog"></i> <span>Lista del Exportador</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
          </a>
          <ul class="treeview-menu">
          <li><a href="{{route('DatosJuridicos.edit',Auth::user()->id)}}">Datos Basicos</a></li>
          <li><a href="{{route('RegisMercantil.edit',Auth::user()->id)}}">Registro Mercantil</a></li>
          <li><a href="{{route('DatosAccionista.edit',Auth::user()->id)}}">Accionista</a></li>
          <li><a href="{{route('CasaMatriz.edit', Auth::user()->id)}}">Casa Matriz</a></li>
          <li><a href="{{route('UltimaExportacion.edit', Auth::user()->id)}}">Ultima Exportación</a></li>
          <li><a href="{{route('productos_export.edit',Auth::user()->id)}}">Productos</a></li>


            <li class="treeview">
              <a href="#"><span>Datos de acceso</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
              </a>
              <ul class="treeview-menu">
              <li><a href="{{route('CambioContrasena.edit',Auth::user()->id)}}">Cambio de Contraseña</a></li>
              <li><a href="{{route('CambioPregunta.edit',Auth::user()->id)}}">Pregunta de Seguridad</a></li>
              </ul>
            </li>
            <li><a href="{{route('DocumentosSoportes.create',Auth::user()->id)}}">Documento Soporte</a></li>
          </li>

      </ul>
      

      <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
