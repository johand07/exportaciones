<div>
  <!-- Pestañas -->

  <div class="warpper">
    <input class="radio" id="one" name="group" type="radio" checked>
    <input class="radio" id="two" name="group" type="radio">
    <input class="radio" id="three" name="group" type="radio">
    <div class="tabs">
      <label class="tab" id="one-tab" for="one">Factura</label>
      @if(isset($factura->numero_factura))
      <a href="{{route('productos.edit',@$factura->id)}}" aria-hidden="true"> 
      <label class="tab" >Productos</label>
      </a>
      @else
      <a href="{{route('productos.create',@$factura->id)}}" aria-hidden="true"> 
      <label class="tab" >Productos</label>
      </a>
      @endif
    </div>
    <div class="panels">
      <div class="panel" id="one-panel">

        @if(isset($factura->numero_factura))
        <div class="panel-title" style="color:#1e3165">
         <a href="#">
            <span class="glyphicon glyphicon-pencil"></span>
        </a>N°: {{@$factura->numero_factura}}</div>
        @else
        <div class="panel-title" style="color:#1e3165">
         <a href="#">
            <span class="glyphicon glyphicon-asterisk"></span>
        </a>Factura Nueva</div>
        @endif

        <div class="container">
          <div id="seccion1" style="display:block">
            <div class="row">
              <div class="col-md-4"></div>
              <div class="col-md-8">
              </div>
            </div>
            <div class="row">
              <div class="col-md-11">
                <div class="col-md-6">
                  <div class="form-group">
                  {!! Form::label('numero_factura', 'Número de factura') !!}
                  {!! Form::text('numero_factura', null, ['class' => 'form-control', 'data-toggle' => 'modal', 'data-target' => '#modal', 'id' => 'numero_factura']) !!}
                  {!! Form::hidden('gen_usuario_id', Auth::user()->id, ['class' => 'form-control']) !!}
               </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    {!! Form::label('', 'Fecha de Factura') !!}
                    <div class="input-group date">
                      {!! Form::text('fecha_factura', null, ['class' => 'form-control','id'=>'fecha_factura']) !!}
                      <span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-11">
                <div class="col-md-6">
                  <div class="form-group">
                    {!! Form::label('', ' Tipo de Solicitud a la cual asociará esta factura') !!}
                    <br>
                    @if($tipoExpor->produc_tecno==2)
                    {!!Form::radio('tipo_solicitud_id',1,null,['class'=>'radio-inline','id'=>"tipo_solicitud_id_1",'checked'=>'checked'])
                    !!}
                    {!! Form::label('Exportación realizada-Bienes','Exportación
                    realizada-Bienes',['class'=>'radio-inline']) !!}
                    @else
                    {!!Form::radio('tipo_solicitud_id',2,null,['class'=>'radio-inline','id'=>"tipo_solicitud_id_2",'checked'=>'checked'])
                    !!}
                    {!! Form::label('Exportación realizada-Servicios y/o Tecnología','Exportación realizada-Servicios
                    y/o Tecnología',['class'=>'radio-inline']) !!}
                    @endif
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    {!! Form::label('', 'Divisas') !!}
                    {!!
                    Form::select('gen_divisa_id',$divisas,null,['placeholder'=>'Seleccione','class'=>'form-control','id'=>'gen_divisa_id'])
                    !!}
                  </div>
                </div>

              </div>
            </div>
            <div class="row">
              <div class="col-md-11">
                <div class="col-md-6">
                  <div class="form-group">
                    {!! Form::label('', 'Forma de Pago') !!}
                    <br>
                    {!! Form::radio('forma_pago_id', '1', false, ['class' => 'radio-inline invalid-radio', 'id' => 'contado_no']) !!}
                    {!! Form::label('Contado', 'Contado', ['class' => 'radio-inline']) !!}

                    {!! Form::radio('forma_pago_id', '3', false, ['class' => 'radio-inline', 'pago_vista_no']) !!}
                    {!! Form::label('Pago a la vista', 'Pago a la vista', ['class' => 'radio-inline']) !!}

                    {!! Form::radio('forma_pago_id', '2', false, ['class' => 'radio-inline', 'credito_si']) !!}
                    {!! Form::label('Crédito', 'Crédito', ['class' => 'radio-inline']) !!}
                                      </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    {!! Form::label('', 'Tipo de Instrumento de Pago') !!}
                    <br>
                    @foreach($tipoInstru as $inst)
                    {!!Form::radio('tipo_instrumento_id',$inst->id,false,['class'=>'radio-inline .valid-radio ','id'=>'tipo_instrumento_id'])
                    !!}
                    {!!
                    Form::label($inst->descrip_tipo_instrumento,$inst->descrip_tipo_instrumento,['class'=>'radio-inline .valid-radio '])
                    !!}
                    @endforeach
                  </div>
                </div>
              </div>
            </div>
            
            
            <div class="row" >
 <div class="col-md-11" >
   <div class="col-md-6"  id="plazo" style="display:none">
     <div class="form-group">
     {!! Form::label('', 'N° Días de Plazo') !!}
      {!! Form::text('plazo_pago',null, ['class' => 'form-control format','id'=>'plazo_pago','onchange'=>'valPlazoPago()', 'max'=>'180',"noreq"=>"noreq"])!!}
    </div>
   </div>
   <div class="col-md-6"  id="forma_plazo" style="display:none">
     <div class="form-group">
     {!! Form::label('','Forma de Crédito') !!}
     <br>
     {!! Form::radio('tipo_plazo',"Total",false,['class'=>'radio-inline']) !!}
     {!! Form::label('', 'Total',['class'=>'radio-inline']) !!}
     {!! Form::radio('tipo_plazo',"Parcial",false,['class'=>'radio-inline']) !!}
     {!! Form::label('', 'Parcial',['class'=>'radio-inline']) !!}
   </div>
  </div>
</div>
</div>

            <div class="row">
              <div class="col-md-11">
                <div class="col-md-6" id="fecha_est_pago" style="display:none">
                  <div class="form-group">
                    {!! Form::label('', 'Fecha de pago estimada') !!}
                    <div class="input-group date">
                      {!! Form::text('fecha_estimada_pago', null, ['class' =>
                      'form-control','id'=>'fecha_estimada_pago']) !!}
                      <span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
                    </div>
                  </div>
                </div>
                <div class="col-md-6" id="cual_instrumento" style="display:none">
                  <div class="form-group">
                    {!! Form::label('', 'Especifique') !!}
                    {!! Form::text('especif_instrumento', null, ['class' => 'form-control','id'=>'especif_instrumento','align'=>'right',"noreq"=>"noreq"]) !!}
                  </div>
                </div>
              </div>
            </div>
          </div>
          @if(!isset($cuotas))
        
          <div id="include_cuotas" style="display:none">@include('factura.cuotas')</div>
          @else
          <div id="include_cuotas" style="display:none">@include('factura.cuotas_edit')</div>
          @endif
          <div class="row" id="botones" style="display:none">
            <div class="col-md-11">
              <div class="col-md-4"></div>
              <div class="col-md-4 text-center">
                <div class="form-group">
                  <a href="{{url('/exportador/factura')}}"><input class="btn btn-primary" type="button" name=""
                      value="Cancelar"></a>
                  <input class="btn btn-primary" type="button" name="siguiente" id="next" value="Siguiente">
                </div>
              </div>
              <div class="col-md-4"></div>
            </div>
          </div>
          <div id="seccion2" style="display:block">
            <div class="row">
              <div class="col-md-11">
                <div class="col-md-6">
                  <div class="form-group">
                    {!! Form::label('', ' Incoterm') !!}
                    {!!Form::select('incoterm_id',$incoterm,null,['placeholder'=>'Seleccione','class'=>'form-control','id'=>'incoterm_id'])
                    !!}
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    {!! Form::label('', 'Monto Fob') !!}
                    {!! Form::text('monto_fob',null, ['class' => 'form-control
                    format text-right','id'=>'monto_fob','onInput'=>"calcularMontoTotal()", 'onchange'=>" montoDecimal()"]) !!}
                    {!! Form::hidden('mont_fob_ant',@$factura->monto_fob, ['class' => 'form-control
                    format text-right','id'=>'mont_fob_ant','onInput'=>"calcularMontoTotal()", 'onchange'=>" montoDecimal()"]) !!}
                    <h5 style="color: red">Debe agregar los decimales con punto (.) </h5>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-11">
                <div class="col-md-6">
                  <div class="form-group">
                    {!! Form::label('', 'Monto Flete Internacional') !!}
                    {!! Form::text('monto_flete',null, ['class' => 'form-control
                    format text-right','id'=>'monto_flete','onInput'=>"calcularMontoTotal()"]) !!}
                    <h5 style="color: red">Debe agregar los decimales con punto (.) </h5>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    {!! Form::label('', 'Monto Seguro') !!}
                    {!! Form::text('monto_seguro',null, ['class' => 'form-control
                    format text-right','id'=>'monto_seguro','onInput'=>"calcularMontoTotal()"]) !!}
                    <h5 style="color: red">Debe agregar los decimales con punto (.) </h5>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-11">
                <div class="col-md-6">
                  <div class="form-group">
                    {!! Form::label('', 'Otros Gastos') !!}
                    {!! Form::text('otro_monto',null , ['class' => 'form-control
                    format text-right','id'=>'otro_monto','onInput'=>"calcularMontoTotal()","noreq"=>"noreq"]) !!}
                    <h5 style="color: red">Debe agregar los decimales con punto (.) </h5>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    {!! Form::label('', 'Monto Total') !!}
                    {!! Form::text('monto_total',null, ['class' => 'form-control format text-right','id'=>'monto_total']) !!}
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-11">
                <div class="col-md-6" id="cual_pago" style="display:none">
                  <div class="form-group">
                    {!! Form::label('', 'Especifique') !!}
                    {!! Form::text('especif_pago', null, ['class' => 'form-control','id'=>'especif_pago',"noreq"=>"noreq"]) !!}
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-11">
                <div class="col-md-6">
                 
                  <div class="form-group">
                    {!! Form::label('', 'Número de Dua') !!}
                    {!!
                    Form::select('gen_dua_id',$dua,null,['placeholder'=>'Seleccione','class'=>'form-control','id'=>'gen_dua_id'])
                    !!}
                  </div>
               
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-11">
                <div class="col-md-4">
                </div>
                <div class="col-md-4 text-center">
                  <div class="form-group">
                    <a href="#"><input class="btn btn-primary" id="prev" type="button" name="" value="Regresar"></a>
                    <input class="btn btn-primary" type="submit" name=""  value="Enviar">
                  </div>
                </div>
                <div class="col-md-4">
                </div>
              </div>
            </div>
          </div>
        </div>
        <script type="text/javascript">
          function valPlazoPago() {
            const max = 180; // Valor maximo Si pone uno Mayor que 180 Explota
            // Obtenemos el valor actual que es el que tipean por medio del "name"
            let value = $('#plazo_pago').val();
            console.log('value', value); //NO es necesario este es solo para ver valor por red
            if (value >
              max) { //Evaluamos la condicion Si el valor es Mayor que la variable declarada anteriormente "max" acciona Evento y manda Modal 
              console.log('El número Días de Plazo cuando es a credito no está dentro del rango permitido');
              swal("Valor superado",
                "El número Días de Plazo cuando es a credito no está dentro del rango permitido debe ser menor o igual a 180",
                "warning");
              $('#plazo_pago').val('');
            }
            // })
          }
          ////////////////////campos de factura con con decimales ///////////////////////////
          function montoDecimal() {
            let valFob = document.getElementById('monto_fob').value;
            console.log('valFob', valFob);
          }
        </script>
        @push('readyscripts')
        $(document).on('click','#prev',function(){
        var x=$('input[name="tipo_plazo"]');
        var metodo="{{Session::get('metodo')}}";
        if(metodo=="Registro")
        {
        if(x[1].checked==false)
        {
        location.href="../factura";
        }else{
        $('#seccion2').css('display','none');
        $('#seccion1').css('display','block');
        $('#include_cuotas').css('display','block');
        $('#botones').css('display','block');
        }
        }else{
        if(x[1].checked==false)
        {
        location.href="../";
        }else{
        $('#seccion2').css('display','none');
        $('#seccion1').css('display','block');
        $('#include_cuotas').css('display','block');
        $('#botones').css('display','block');
        }
        }
        });
      </div>



 
  @endpush