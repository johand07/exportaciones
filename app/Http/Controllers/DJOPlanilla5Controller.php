<?php

namespace App\Http\Controllers;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Http\Request;
use App\Models\DetDeclaracionProduc;
use App\Models\Planilla5;
use App\Models\Productos;
use App\Models\DetUsuario;
use Auth;
use Alert;
use Session;
use DB;
use File;
use View;
use Response;

class DJOPlanilla5Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return $this->create($request);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $productos_id= $request['producto_id']; // id del Producto
        $gen_declaracion_jo_id = Session::get('gen_declaracion_jo_id'); // id de Declaración Jurada de Origen
        
        // Obtengo aqui el Id de det_declaracion_produc al cual esta asociado el producto
        $det_declaracion_product=DetDeclaracionProduc::with('rProducto')->with('rPlanilla5')->where('gen_declaracion_jo_id',$gen_declaracion_jo_id)->where('id',$productos_id)->get()->last();        
         
        // Verificamos si existe datos en la planilla asociada al producto 
        if(!empty($det_declaracion_product->rPlanilla5->id)){

           return redirect()->action('DJOPlanilla5Controller@edit', ['id' => $det_declaracion_product->rPlanilla5->id]);
        }

        if(empty($det_declaracion_product)){

            Alert::warning('La solicitud que intenta realizar requiere que el poducto este asociado a la declaracion. Intente nuevamente.','Acción Denegada!')->persistent("OK");
            return redirect()->action('DeclaracionJOController@index');
        
        }else{

            Session::put('det_declaracion_product_id',$det_declaracion_product->id);
        }

        $titulo= 'Declaración Jurada de Origen';
        $descripcion= 'Planilla 5';
        $producto = $det_declaracion_product->rProducto;

        return view('djoplanilla5.create',compact('producto','titulo','descripcion'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

            $this->validate($request, [

                                        'proceso_grafico_imagen' => 'required|mimes:jpeg,png,jpg,gif,pdf|max:9500',
                                       
                                ]);

            $det_declaracion_product_id = Session::get('det_declaracion_product_id');
            $gen_declaracion_jo_id = Session::get('gen_declaracion_jo_id');

            if(empty($det_declaracion_product_id)){
                Alert::warning('No es posible realizar su solicitud. Intente nuevamente.','Acción Denegada!')->persistent("OK");

                return redirect()->back()->withInput();
            }

            $planilla5 = Planilla5::with('declaracionProduc')->where('det_declaracion_produc_id',$det_declaracion_product_id)->get()->last();

            if(!empty($planilla5)){

                 Alert::error('Usted ya registro datos para la planilla 5 asociada al producto " '.$planilla5->declaracionProduc->rProducto->descrip_comercial.' ". Intente nuevamente.','Acción Denegada!')->persistent("OK");

                return redirect()->action('DeclaracionJOController@index',['djo'=>encrypt($gen_declaracion_jo_id)]);
            }

            $imagen = $request->file('proceso_grafico_imagen');

            $nombre_imagen = time().'.'.$imagen->getClientOriginalExtension();
            $destino = '/img/declaracionjurada/'.Auth::user()->detUsuario->cod_empresa;
            $destinoPrivado = public_path($destino);
            
            if (!file_exists($destinoPrivado)) {
                $filesystem = new Filesystem();
                $filesystem->makeDirectory($destinoPrivado, $mode = 0777, true, true); 
            }

            $imagen->move($destinoPrivado, $nombre_imagen);

            $planilla5 = new Planilla5;
            $planilla5->det_declaracion_produc_id = $det_declaracion_product_id;
            $planilla5->diagrama_proceso_productivo = $destino.'/'.$nombre_imagen;
            $planilla5->bactivo=1;

            if($planilla5->save()){

                   /*****  Cambiando el estado de la planilla 5 en det declaracion product ******/
                $detDeclaracionProducModel = $planilla5->declaracionProduc;
                $detDeclaracionProducModel->estado_p5=1;
                $detDeclaracionProducModel->save();

                 Alert::success('Se han guardado los datos correspondientes a la Planilla 5!','Registrado Correctamente!')->persistent("Ok");

                return redirect()->action('DeclaracionJOController@index',['djo'=>encrypt($gen_declaracion_jo_id)]);
            }else{

            Alert::warning('No se logro guardar la información. Intente nuevamente.','Acción Denegada!')->persistent("OK");

            return redirect()->back()->withInput();
        }



    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        //dd($request->det_declaracion_id);
        $layout= $request->layout==="false"? false: true;

        if(!empty($request->det_declaracion_id))
        {
            
            $det_declaracion= DetDeclaracionProduc::with('rPlanilla5')->find($request->det_declaracion_id);
        
            if(empty($det_declaracion)){
                Alert::error('La planilla que intenta visualizar no esta registrada en el sistema. D1','Acción Denegada!')->persistent("OK");
                return $layout? redirect()->action('DeclaracionJOController@index') : '<div class="alert alert-danger">La planilla que intenta visualizar no esta registrada en el sistema. D1</div>';
            }

            $planilla5 = $det_declaracion->rPlanilla5;


        }else{
            $id= $request->id ;
            $planilla5 = Planilla5::with('declaracionProduc')->find($id);
        }

        /***** Validar que haya un registro de la planilla ****/
        if(empty($planilla5)){

             Alert::error('La planilla que intenta visualizar no esta registrada en el sistema.','Acción Denegada!')->persistent("OK");

            return $layout? redirect()->action('DeclaracionJOController@index') : '<div class="alert alert-danger">La planilla que intenta visualizar no esta registrada en el sistema.</div>';
        }

        /***** Validar que la planilla sea de un usuario en especifico ****/
        if($planilla5->declaracionProduc->declaracionJO->gen_usuario_id!=Auth::user()->id && Auth::user()->gen_tipo_usuario_id!=5 && Auth::user()->gen_tipo_usuario_id!=6 && Auth::user()->gen_tipo_usuario_id!=1 && Auth::user()->gen_tipo_usuario_id!=8){

            Alert::error('No tiene permisos para visualizar esta planilla.','Acción Denegada!')->persistent("OK");

           return  $layout? redirect()->action('DeclaracionJOController@index') : '<div class="alert alert-danger">No tiene permisos para visualizar esta planilla.</div>';
       }

       $producto = $planilla5->declaracionProduc->rProducto;
       //dd($planilla5->diagrama_proceso_productivo);
       $exten=explode(".", $planilla5->diagrama_proceso_productivo);
        $extencion = $exten[1];
        if($layout)
            return view('djoplanilla5.show',compact('layout','planilla5','producto','extencion'));
        else
            return View::make('djoplanilla5.show',compact('layout','planilla5','producto','extencion'))->renderSections()['content'];
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // get the planilla5
        $planilla5 = Planilla5::find($id);

        $producto = $planilla5->declaracionProduc->rProducto;
        $exten=explode(".", $planilla5->diagrama_proceso_productivo);
        $extencion = $exten[1];
        // show the edit form and pass the planilla4
        return View::make('djoplanilla5.edit',compact('planilla5','producto','extencion'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
          // validate
        // read more on validation at http://laravel.com/docs/validation
        $this->validate($request, [

            'proceso_grafico_imagen' => 'required|image|mimes:jpeg,png,jpg,gif|max:9500',

        ]);

        
        $imagen = $request->file('proceso_grafico_imagen');

            $nombre_imagen = time().'.'.$imagen->getClientOriginalExtension();
            $destino = '/img/declaracionjurada/'.Auth::user()->detUsuario->cod_empresa;
            $destinoPrivado = public_path($destino);
            
            $imagen->move($destinoPrivado, $nombre_imagen);

            $planilla5 = Planilla5::find($id);
            $auxImgAnterior = $planilla5->diagrama_proceso_productivo;
            $planilla5->diagrama_proceso_productivo = $destino.'/'.$nombre_imagen;
            $planilla5->bactivo=1;
            $planilla5->estado_observacion=0;
            
            if($planilla5->touch()){
                if(is_file('.'.$auxImgAnterior))
                unlink('.'.$auxImgAnterior); //elimino el fichero
            }else{
                if(is_file('.'.$destino.'/'.$nombre_imagen))
                unlink('.'.$destino.'/'.$nombre_imagen);

                Alert::warning('No es posible realizar su solicitud. Intente nuevamente.','Acción Denegada!')->persistent("OK");
                return redirect()->back()->withInput();
            }
            /*****  Cambiando el estado de la planilla 5 en det declaracion product ******/
            $detDeclaracionProducModel = $planilla5->declaracionProduc;
            $detDeclaracionProducModel->estado_p5=1;
            $detDeclaracionProducModel->touch();
        // redirect
        Alert::success('Se han actualizado los datos correspondientes a la Planilla 5!','Actualizado Correctamente!')->persistent("Ok");

        return redirect()->action('DeclaracionJOController@index');
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    

          /** Para consultar las observaciones mediante ajax **/
    /**
     * Se recibe como parametro el $id de la planilla
     * 
     */
    public function obtenerObservacion(Request $request,$id=null){

        if(!empty($request->det_declaracion_id)){
            $detDeclaracion= DetDeclaracionProduc::with('rPlanilla5')->find($request->det_declaracion_id);
            $planilla5= $detDeclaracion->rPlanilla5;
        }else{
        $planilla5 = Planilla5::find($id);
        }
        return Response::json(array('observacion'=>$planilla5->descrip_observacion));
        
    }

      /** Para guardar las observaciones mediante ajax **/
    /**
     * Se recibe como parametro el $id de la planilla
     * 
     */
    public function storeObservacion(Request $request,$id=null){

        
        if(!empty($request->det_declaracion_id)){
            $detDeclaracion= DetDeclaracionProduc::with('rPlanilla5')->find($request->det_declaracion_id);
            $planilla5= $detDeclaracion->rPlanilla5;
        }else{
            $planilla5 = Planilla5::find($id);
        }
        if(!empty($request->observacion)){
            $planilla5->descrip_observacion=$request->observacion;
            $planilla5->estado_observacion=1;
        }else{
            $planilla5->descrip_observacion=null;
            $planilla5->estado_observacion=0;
        }

        if($planilla5->touch())
            return Response::json(array('respuesta'=>'Solicitud procesada con exito','estado'=>1));
        else
            return Response::json(array('respuesta'=>'La solicitud no pudo ser procesada','estado'=>0));
        
    }
}
