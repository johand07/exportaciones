@extends('templates/layoutlte_analista_inversionista')

@section('content')

<div class="content" style="background-color: #fff">
    <div class="panel-group" style="background-color: #fff;">
      <div class="panel-primary">
        <div class="panel-heading"><h3>Solicitudes de Inversión Extranjera</h3> </div>
        <div class="panel-body">
            <div class="row">
              
              <div class="col-md-12">
                <div class="row">
                  <div class="col-md-8">

                           <h4 class="text-info">Bandeja de Solicitudes Inversionista:</h4><hr>
                          
                  </div>
                  <div class="col-md-2">
                   
                  </div>
                  <div class="col-md-2">
                    <a class="btn btn-primary" href="{{url('/AnalistaInversion/AnalistDeclaracionInversion')}}"><span class="glyphicon glyphicon-refresh" title="Agregar" aria-hidden="true"></span>
                        Actualizar</a>
                    
                  </div>
                </div>


        <table id="listaExtranjera" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                <tr>

                    <th>#</th>
                    <th>Nro. Solicitud</th>
                    <th>Rif</th>
                    <th>Razon Social</th>
                    <th>Estatus</th>
                    <th>Detalle estatus</th>
                    <th>Fecha etatus</th>
                    <th>Fecha Solicitud</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody>
              @if(isset($declaracionesInversion))
                @foreach($declaracionesInversion as $key =>  $decInversion)

                  <tr>
                    <td>
                      {{$key+1}}
                    </td>
                    <td>
                      {{$decInversion->num_declaracion}}
                    </td>
                    <td>
                      {{$decInversion->genUsuario->detUsuario->rif}}
                    </td>
                    <td>
                      {{$decInversion->genUsuario->detUsuario->razon_social}}
                    </td>
                    <td>
                      {{$decInversion->genStatus->nombre_status}}
                    </td>
                    <td>
                      @if((isset($decInversion->rPlanillaDjir01) && $decInversion->rPlanillaDjir01->gen_status_id == 9)&& (isset($decInversion->rPlanillaDjir02) && $decInversion->rPlanillaDjir02->gen_status_id == 9 )&& (isset($decInversion->rPlanillaDjir03) && $decInversion->rPlanillaDjir03->gen_status_id == 9))
                          Solicitud Completada y enviada, en espera de analisis!
                      @elseif(isset($decInversion->rPlanillaDjir01) && !isset($decInversion->rPlanillaDjir02) && !isset($decInversion->rPlanillaDjir03))
                        Declaracion Incompleta, Planilla Djir02 y Djir03 incompleta!
                      @elseif(isset($decInversion->rPlanillaDjir02) && isset($decInversion->rPlanillaDjir01) && !isset($decInversion->rPlanillaDjir03))
                        Declaracion Incompleta, Planilla Djir03 incompleta!
                      @elseif(!isset($decInversion->rPlanillaDjir01) && !isset($decInversion->rPlanillaDjir02) && !isset($decInversion->rPlanillaDjir03))
                        Declaracion totalmente Incompleta, Debe completar todas las planillas !
                      @elseif((isset($decInversion->rPlanillaDjir01) && $decInversion->rPlanillaDjir01->estado_observacion == 1 && ($decInversion->rPlanillaDjir01->gen_status_id == 11 || $decInversion->rPlanillaDjir01->gen_status_id == 15)) || (isset($decInversion->rPlanillaDjir02) && $decInversion->rPlanillaDjir02->estado_observacion == 1 && ($decInversion->rPlanillaDjir02->gen_status_id == 11 || $decInversion->rPlanillaDjir02->gen_status_id == 15) || (isset($decInversion->rPlanillaDjir03) && $decInversion->rPlanillaDjir03->estado_observacion == 1 && ($decInversion->rPlanillaDjir03->gen_status_id == 11 || $decInversion->rPlanillaDjir03->gen_status_id == 15))))
                         <b>PLanilla Djir01:</b> {{$decInversion->rPlanillaDjir01->descripcion_observacion ? 'Con observaciones' : 'Sin observaciones'}}
                         <br>
                         <b>PLanilla Djir02:</b> {{$decInversion->rPlanillaDjir02->descrip_observacion ? 'Con observaciones' : 'Sin observaciones'}}
                         <br>
                         <b>PLanilla Djir03:</b> {{$decInversion->rPlanillaDjir03->descrip_observacion ? 'Con observaciones' : 'Sin observaciones'}}
                      @endif
                    </td>
                    <td>
                      {{$decInversion->fstatus}}
                    </td>
                    <td>
                      {{$decInversion->created_at}}
                    </td>
                    <td>
                     @if(isset($decInversion->rPlanillaDjir01) && isset($decInversion->rPlanillaDjir02) && isset($decInversion->rPlanillaDjir03))
                        @if($decInversion->rPlanillaDjir01->gen_status_id == 9 && $decInversion->rPlanillaDjir02->gen_status_id == 9 && $decInversion->rPlanillaDjir03->gen_status_id == 9)
                           <a href="{{ route('AnalistDeclaracionInversion.edit',$decInversion->id)}}" class="btn btn-sm btn-danger" id="atender"><i class="glyphicon glyphicon-user"></i><b> Atender</b></a>
                        @endif
                      @endif


                      @if(isset($decInversion->rPlanillaDjir01) && isset($decInversion->rPlanillaDjir02) && isset($decInversion->rPlanillaDjir03))
                        @if($decInversion->rPlanillaDjir01->gen_status_id == 11 || $decInversion->rPlanillaDjir02->gen_status_id == 11 || $decInversion->rPlanillaDjir03->gen_status_id == 11)
                          <a href="{{ route('AnalistDeclaracionInversion.edit',$decInversion->id)}}" class="btn btn-sm btn-primary" id="atender"><i class="glyphicon glyphicon-user"></i><b> Continuar</b></a>
                        @endif
                      @endif

                       @if(isset($decInversion->rPlanillaDjir01) && isset($decInversion->rPlanillaDjir02) && isset($decInversion->rPlanillaDjir03))
                        @if($decInversion->rPlanillaDjir01->gen_status_id == 12 && $decInversion->rPlanillaDjir02->gen_status_id == 12 && $decInversion->rPlanillaDjir03->gen_status_id == 12)
                          Aprobado
                        @endif
                      @endif

                       @if(isset($decInversion->rPlanillaDjir01) && isset($decInversion->rPlanillaDjir02) && isset($decInversion->rPlanillaDjir03))
                        @if($decInversion->rPlanillaDjir01->gen_status_id == 13 || $decInversion->rPlanillaDjir02->gen_status_id == 13 || $decInversion->rPlanillaDjir03->gen_status_id == 13)
                          <a href="{{ route('AnalistDeclaracionInversion.edit',$decInversion->id)}}" class="btn btn-sm btn-primary" id="atender"><i class="glyphicon glyphicon-user"></i><b> Continuar</b></a>
                        @endif
                      @endif

                       @if(isset($decInversion->rPlanillaDjir01) && isset($decInversion->rPlanillaDjir02) && isset($decInversion->rPlanillaDjir03))
                        @if($decInversion->rPlanillaDjir01->gen_status_id == 15 || $decInversion->rPlanillaDjir02->gen_status_id == 15 || $decInversion->rPlanillaDjir03->gen_status_id == 15)
                          <a href="{{ route('AnalistDeclaracionInversion.edit',$decInversion->id)}}" class="btn btn-sm btn-primary" id="atender"><i class="glyphicon glyphicon-user"></i><b> Continuar</b></a>
                        @endif
                      @endif
                      @if(isset($decInversion->rPlanillaDjir01) && isset($decInversion->rPlanillaDjir02) && isset($decInversion->rPlanillaDjir03))
                        @if($decInversion->rPlanillaDjir01->gen_status_id == 16 && $decInversion->rPlanillaDjir02->gen_status_id == 16 && $decInversion->rPlanillaDjir03->gen_status_id == 16)
                          <a href="{{ route('AnalistDeclaracionInversion.edit',$decInversion->id)}}" class="btn btn-sm btn-default" id="atender"><i class="glyphicon glyphicon-user"></i><b> Re-Evaluar</b></a>
                        @endif
                      @endif

                      @if(isset($decInversion->rPlanillaDjir01) && isset($decInversion->rPlanillaDjir02) && isset($decInversion->rPlanillaDjir03))
                        @if($decInversion->rPlanillaDjir01->gen_status_id == 18 || $decInversion->rPlanillaDjir02->gen_status_id == 18 || $decInversion->rPlanillaDjir03->gen_status_id == 18)
                          <a href="{{ route('AnalistDeclaracionInversion.edit',$decInversion->id)}}" class="btn btn-sm btn-warning" id="atender"><i class="glyphicon glyphicon-user"></i><b> Reatender</b></a>
                        @endif
                      @endif

                      @if(isset($decInversion->rPlanillaDjir01) && !isset($decInversion->rPlanillaDjir02) && !isset($decInversion->rPlanillaDjir03))
                        Incompleta
                      @endif
                      @if(isset($decInversion->rPlanillaDjir02) && isset($decInversion->rPlanillaDjir01) && !isset($decInversion->rPlanillaDjir03))
                         Incompleta
                      @endif
                      @if(!isset($decInversion->rPlanillaDjir01) && !isset($decInversion->rPlanillaDjir02) && !isset($decInversion->rPlanillaDjir03))
                        Incompleta
                      @endif
                    </td>
                  </tr>
                @endforeach
              @endif
            
            </tbody>   
                </table>
              </div>
            </div><!-- Cierre 1° Row-->
        </div>
      </div>
    </div>
</div><!-- fin content-->
@stop