<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTablePlanilla2Djo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::table('planilla_2', function (Blueprint $table) {
            //
        
            $table->string('rif_productor')->nullable()->after('total_mate_nac');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('planilla_2', function (Blueprint $table) {
            //
            $table->dropColumn('rif_productor');
        });
    }
}
