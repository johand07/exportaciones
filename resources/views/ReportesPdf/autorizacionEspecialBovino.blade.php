<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Autorizacion</title>
    <style type="text/css">
        .centrar{
            text-align: center;
        }

        .titulo{
            
            font-weight: bold;
        }

        .nro-correlativo{
            color: red;
            font-size: 16px;
        }

        .derecha{
            text-align: left;

        }

        .text-normal{
            font-size: 14px;
        }

        .text-medio{
            font-size: 12px;
        }
        .text-11{
            font-size: 11px;
        }
        .text-small{
            font-size: 10px;
        }

        .text-9{
            font-size: 9px;
        }

        .text-8{
            font-size: 8px;
        }

        .espacio-superior{
            margin-top: 10px;
        }

        .espacio-superior-small{
            margin-top: 5px;
        }

        .ancho-medio{
            width: 50%;
        }

        .ancho-full{
            width: 100%;
        }

        .ancho-medio-alto{
            width: 75%;
        }

        .ancho-90{
            width: 90%;
        }

        .table{
            border-collapse: collapse;
        }
        
        .borde{
            border: 1px solid #000;
        }

        .sin-borde{
            border: none;
        }

        .top{
            vertical-align: top;
        }

        .under-dotted{    
            border-bottom: 1px dotted #000;
            text-decoration: none;
        }

        .content-1{
            width: 5%;
        }

        .content-9{
            width: 20%;
        }

        .sangria-1{
            margin-left: 28px;
        }

        .sangria-2{
            margin-left: 36px;
        }

        .sangria-3{
            margin-left: 54px;
        }

        .sangria-sub{
            margin-left: 14px;
            
        }

        
        .page-break { page-break-before: always; }

        .table-center{
            margin: 0 auto; 
        }

        .justify{
            text-align: justify;
        }

        td.justify {
            padding-left: 8px;
            padding-right: 8px; 
        }
        .lineal{
            
        }
        .lineal>div{
            max-width:91%;

        }
        .lineal>*{
           display: inline-block;
           vertical-align:top;
        }

        .sangria-lista{
            list-style-type: none;
            margin: 0;
            padding: 0;
            margin-bottom: 4px;
        }

        .sangria-lista>li{
            margin-top: 5px;
            padding-left: 10px;
            padding-right: 10px;
        }
    </style>
</head>
<body>          
	 <div class="centrar espacio-superior">
        <table class="ancho-full">
            <tr>
                <td class="top center" style="width: 70%; font-size:107%;">
                      <img class="img-responsive" style="width: 565px;" src="img/cintillo.png">
                </td>
            </tr>
        </table>
    </div>

    <div class="centrar espacio-superior">
        <table class="ancho-full">
            <tr>
                <td class="center" style="width: 70%; font-size:20px; text-align: center;">
                     <b>AUTORIZACIÓN ESPECIAL <br> DE EXPORTACIÓN</b>
                </td>
            </tr>
        </table>
    </div>
    <div class="centrar espacio-superior">
        <table class="ancho-full">
            <tr>
                <td class="nro-correlativo" style="width: 70%; font-size:90%; text-align: right;">
                     <b style="color: black">Solicitud: </b><b>CRE/N°</b><b style="color: black">{{ $autorizacion->num_autorizacion}}</b>
                </td>
            </tr>
        </table>
    </div>

    <div class="centrar espacio-superior">
	    <table class="ancho-full">
	        <tr>
	            <td class="top" style="text-align: left; font-size: 16px" colspan="50"><b>Sres.</b><br><br>{{ $det_usuario->razon_social}}</td>
	            <td class="top" style="text-align: center; font-size: 16px" colspan="50"><b>RIF</b><br><br>{{ $det_usuario->rif}}<br></td>
	        </tr>
	    </table>
	</div>

	<div>
	    <table>
	        <tr>
	            <td style="text-align: justify; font-size: 16px;" colspan="80">Presente. <br><br> En respuesta a su solicitud , el Despacho de Viceministro de Comercio Exterior y Promoción de Inversiones, representado en este acto por el viceministro, considera Otorgar la presente autorización, siendo evaluado todos los recaudos y previamente analizado por el Instituto Nacional de Salud Agrícola Integral (INSAI), el cual se emite a la (s) partidas arancelarias:</td>
	        </tr>
	    </table>
	</div>

	 <div class="centrar espacio-superior">
	    <table class="ancho-full borde" >
	        <tr>
	            <td class="top" style="text-align: left; font-size: 13px" colspan="18"><b>CÓDIGO <br>ARANCELARIO</b><br><br>{{ $autorizacion->codigo_arancel}}</td>
	            <td class="top" style="text-align: center; font-size: 13px" colspan="18"><b>DESCRIPCIÓN <br></b><br><br>{{ $autorizacion->descripcion_arancel}}<br><br></td>
	            <td class="top" style="text-align: left; font-size: 13px" colspan="16"><b>VOLUMEN <br></b><br><br>{{ $autorizacion->volumen}}</td>
	            <td class="top" style="text-align: center; font-size: 13px" colspan="16"><b>PRUERTO DE <br>SALIDA</b><br><br>{{ $autorizacion->puerto_salida}}<br><br></td>
	            <td class="top" style="text-align: left; font-size: 13px" colspan="16"><b>PUERTO DE <br> ENTRADA</b><br><br>{{ $autorizacion->puerto_entrada}}</td>
	            <td class="top" style="text-align: center; font-size: 13px" colspan="16"><b>N° DE <br> PROFORMA</b><br><br>{{ $autorizacion->num_proforma}}<br><br></td>

	        </tr>
	    </table>
	</div>

	<div>
	    <table>
	        <tr>
	            <td style="text-align: justify; font-size: 16px;" colspan="80">Sobre el particular, no dispensa los requisitos Zoosanitarios y demás requerimientos exigibles por el resto de las autoridades competentes en lo que respecta a los productos de Origen Animal. <br></td>
	        </tr>
	    </table>
	</div>

	<div>
	    <table>
	        <tr>
	            <td style="text-align: justify; font-size: 16px;">Sirva la presente, queda de usted,</td>
	        </tr>
	    </table>
	</div>
<br> <br>
	<div>
	    <table>
	        <tr>
	            <td style="text-align: center; font-size: 17px;" colspan="80"><b>{{ $datosfirmante->nombre_firmante}} {{ $datosfirmante->apellido_firmante}}</b><br><b>{{ $datosfirmante->cargo_firmante}}</b><br>{{ $datosfirmante->gaceta_firmante}}</td>
	        </tr>
	    </table>
	</div><br>

	<div>
	    <table>
	        <tr>
	            <td style="text-align: justify; font-size: 13px;" colspan="80">NOTA: <br> 
	            	<ul>
	            		- EL PRESENTE CERTIFICADO ES VALIDO PARA 1 SOLO EMBARQUE, DE CARÁCTER INTRANSFERIBLE E IMPRORROGABLE Y NO AMPARA MERCANCÍA DE SALIDA DEL PUERTO VENEZOLANO CON ANTERIORIDAD A SU EMISIÓN. <br>
	            	    - ESTE DOCUMENTO ES VALIDO HASTA EL {{ $autorizacion->fvalido}}
	            	</ul>
	             </td> <br>
	        </tr>
	    </table>
	</div>

<div class="centrar espacio-superior">
    <table class="ancho-full">
        <tr class="text-11">
            <td class="top" style="text-align: left" colspan="50"><b><img class="img-responsive" style="width: 250;" src="img/logovuce.png"></b><br><br></td>
            <td class="top" style="text-align: center" colspan="50"><b><img src="data:image/png;base64,{!! base64_encode(QrCode::format('png')->size(230)->generate(URL::to('/api/AutorizacionEspecialExportacion/'.$autorizacion->id))) !!}" width="160px" height="145px"></b></td>
        </tr>
    </table>

</div>

</body>
</html>