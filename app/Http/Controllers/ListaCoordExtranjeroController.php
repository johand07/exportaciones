<?php

namespace App\Http\Controllers;
use App\Models\BandejaSolInversionista;
use App\Models\GenSolInversionista;
use App\Models\CoordInversionista;
use App\Models\DetUsuario;
use App\Models\Accionistas;
use App\Models\Pais;
use App\Models\GenStatus;
use App\Models\CatDocumentos;
use App\Models\GenDocumentosInversiones;
use App\Models\Doc_Adic_Inversiones;
use App\Models\DocumentsVariosInversiones;
use App\Mail\NotificacionesInversiones;
use Illuminate\Http\Request;
use Mail;
use Session;
use Auth;
use Alert;


class ListaCoordExtranjeroController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    { 
        
        $coordinador_inversion=BandejaSolInversionista::where('bactivo',1)
        ->with('rcoordinversion')->get();

        //dd($coordinador_inversion);

        return view('ListaCoordExtranjero.index',compact('coordinador_inversion'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\GenSolicitud  $genSolicitud
     * @return \Illuminate\Http\Response
     */
    public function show(BandejaSolInversionista $bandejaSolInversionista, $id)
    {
        $titulo='Registro de Potencial Inversionista Extranjero';
        $descripcion='Perfil del Coordinador';

    /*****Consulto la solicitud para traerme el id de ese solicitud*****/
        $solicitud=GenSolInversionista::find($id);

         //$solicitud=GenSolInversionista::where('id',$id)->first();

     /** Consulta en mi bandeja si esta esa solicitud**/
        $bandejaanalista=BandejaSolInversionista::where('gen_sol_inversionista_id',$solicitud->id)->first();


    /************ Valido si la solicitud existe, Si existe no hago nada en caso contrario inserto la solicitud******************/

     /**Valido si esta**/
    $validaratencion=CoordInversionista::where('bandeja_sol_inversionista_id',$bandejaanalista->id)->where('gen_usuario_id',Auth::user()->id)->first();
     //dd($validaratencion);

      //dd($coordinversionista); 
     


/*************** Consulto en Detusuario para traerme el id*************************************/
      //Consulta el tipo de documentos de acuerdo el ente
        $documentos=CatDocumentos::where('gen_ente_id',2)->where('bactivo',1)->whereBetween('id',[10,17])->get(); 

           $estado_observacion=GenStatus::whereIn('id', [11,15,16,17,19])->pluck('nombre_status','id');
           //dd($estado_observacion); 

           $det_usuario=DetUsuario::where('gen_usuario_id',$solicitud->gen_usuario_id)->first();
           //dd($det_usuario);
        
           $pais=Pais::where('id',$solicitud->pais_id)->first();
           //dd($pais); 
         
         //Consulta los accionista de esa solicitud
           $accionistas=Accionistas::where('gen_usuario_id',$solicitud->gen_usuario_id)->where('bactivo',1)->get();

           //Documentos Inversionista//
           $docCertificados=GenDocumentosInversiones::where('gen_sol_inversionista_id',$solicitud->id)->first();

           $docAdicionales=Doc_Adic_Inversiones::where('gen_sol_inversionista_id',$solicitud->id)->first();

           //dd($docAdicionales);
           //
           $sopAdicionalesInv2=DocumentsVariosInversiones::where('gen_sol_inversionista_id',$solicitud->id)->where('cat_documentos_id',12)->get();

           $sopAdicionalesInv4=DocumentsVariosInversiones::where('gen_sol_inversionista_id',$solicitud->id)->where('cat_documentos_id',14)->get();


           //dd($docCertificados);
    
        //$extencion_file_1 = $exten_file_1[1];
     
        if(!empty($docCertificados['file_1'])){
             $exten_file_1=explode(".", $docCertificados['file_1']);
            for($i=0; $i <= count($exten_file_1)-1; $i++){
                if($exten_file_1[$i] == 'pdf' || $exten_file_1[$i] == 'doc' || $exten_file_1[$i] =='docx'){
                    //return $exten_file_1[$i];
                     $extencion_file_1 = $exten_file_1[$i];
                }
            }
//dd($extencion_file_1);
        } else {
            $extencion_file_1 = '';
        }
        if (!empty($docCertificados['file_2'])) {
            $exten_file_2=explode(".", $docCertificados['file_2']);
            for($i=0; $i <= count($exten_file_2)-1; $i++){
                if($exten_file_2[$i] == 'pdf' || $exten_file_2[$i] == 'doc' || $exten_file_1[$i] =='docx'){
                    //return $exten_file_1[$i];
                     $extencion_file_2 = $exten_file_2[$i];
                }
            }
        }else {
            $extencion_file_2 = '';
        }
        if (!empty($docCertificados['file_3'])) {
            $exten_file_3=explode(".", $docCertificados['file_3']);
             for($i=0; $i <= count($exten_file_3)-1; $i++){
                if($exten_file_3[$i] == 'pdf' || $exten_file_3[$i] == 'doc' || $exten_file_3[$i] =='docx'){
                    //return $exten_file_1[$i];
                     $extencion_file_3 = $exten_file_3[$i];
                }
            }
        }else {
            $extencion_file_3 = '';
        }
        if (!empty($docCertificados['file_4'])) {
            $exten_file_4=explode(".", $docCertificados['file_4']);
            for($i=0; $i <= count($exten_file_4)-1; $i++){
                if($exten_file_4[$i] == 'pdf' || $exten_file_4[$i] == 'doc' || $exten_file_4[$i] =='docx'){
                    //return $exten_file_1[$i];
                     $extencion_file_4 = $exten_file_4[$i];
                }
            }
        }else {
            $extencion_file_4 = '';
        }
        if (!empty($docCertificados['file_5'])) {
            $exten_file_5=explode(".", $docCertificados['file_5']);
            for($i=0; $i <= count($exten_file_5)-1; $i++){
                if($exten_file_5[$i] == 'pdf' || $exten_file_5[$i] == 'doc' || $exten_file_5[$i] =='docx'){
                    //return $exten_file_1[$i];
                     $extencion_file_5 = $exten_file_5[$i];
                }
            }
        }else {
            $extencion_file_5 = '';
        }
        if (!empty($docCertificados['file_6'])) {
            $exten_file_6=explode(".", $docCertificados['file_6']);
            for($i=0; $i <= count($exten_file_6)-1; $i++){
                if($exten_file_6[$i] == 'pdf' || $exten_file_6[$i] == 'doc' || $exten_file_6[$i] =='docx'){
                    //return $exten_file_1[$i];
                     $extencion_file_6 = $exten_file_6[$i];
                }
            }
        }else {
            $extencion_file_6 = '';
        }
        if (!empty($docCertificados['file_7'])) {
            $exten_file_7=explode(".", $docCertificados['file_7']);
            for($i=0; $i <= count($exten_file_7)-1; $i++){
                if($exten_file_7[$i] == 'pdf' || $exten_file_7[$i] == 'doc' || $exten_file_7[$i] =='docx'){
                    //return $exten_file_1[$i];
                     $extencion_file_7 = $exten_file_7[$i];
                }
            }
        }else {
            $extencion_file_7 = '';
        }
        if (!empty($docCertificados['file_8'])) {
            $exten_file_8=explode(".", $docCertificados['file_8']);
            for($i=0; $i <= count($exten_file_8)-1; $i++){
                if($exten_file_8[$i] == 'pdf' || $exten_file_8[$i] == 'doc' || $exten_file_8[$i] =='docx'){
                    //return $exten_file_1[$i];
                     $extencion_file_8 = $exten_file_8[$i];
                }
            }
        }else {
            $extencion_file_8 = '';
        }
        if (!empty($docCertificados['file_9'])) {
            $exten_file_9=explode(".", $docCertificados['file_9']);
            for($i=0; $i <= count($exten_file_9)-1; $i++){
                if($exten_file_9[$i] == 'pdf' || $exten_file_9[$i] == 'doc' || $exten_file_9[$i] =='docx'){
                    //return $exten_file_1[$i];
                     $extencion_file_9 = $exten_file_9[$i];
                }
            }
        }else {
            $extencion_file_9 = '';
        }
        if (!empty($docCertificados['file_10'])) {
            $exten_file_10=explode(".", $docCertificados['file_10']);
            for($i=0; $i <= count($exten_file_10)-1; $i++){
                if($exten_file_10[$i] == 'pdf' || $exten_file_10[$i] == 'doc' || $exten_file_10[$i] =='docx'){
                    //return $exten_file_1[$i];
                     $extencion_file_10 = $exten_file_10[$i];
                }
            }
        }else {
            $extencion_file_10 = '';
        }


         //Doc Adicionales
        if(!empty($docAdicionales['file_1'])){
            $exten_file_1=explode(".", $docAdicionales['file_1']);
            for($i=0; $i <= count($exten_file_1)-1; $i++){
                if($exten_file_1[$i] == 'pdf' || $exten_file_1[$i] == 'doc' || $exten_file_1[$i] =='docx'){
                    //return $exten_file_1[$i];
                     $extencion_adi_file_1 = $exten_file_1[$i];
                }
            }
        } else {
            $extencion_adi_file_1 = '';
        }
        if (!empty($docAdicionales['file_2'])) {
            $exten_file_2=explode(".", $docAdicionales['file_2']);
            for($i=0; $i <= count($exten_file_2)-1; $i++){
                if($exten_file_2[$i] == 'pdf' || $exten_file_2[$i] == 'doc' || $exten_file_2[$i] =='docx'){
                    //return $exten_file_1[$i];
                     $extencion_adi_file_2 = $exten_file_2[$i];
                }
            }
        }else {
            $extencion_adi_file_2 = '';
        }
        if (!empty($docAdicionales['file_3'])) {
            $exten_file_3=explode(".", $docAdicionales['file_3']);
            for($i=0; $i <= count($exten_file_3)-1; $i++){
                if($exten_file_3[$i] == 'pdf' || $exten_file_3[$i] == 'doc' || $exten_file_3[$i] =='docx'){
                    //return $exten_file_1[$i];
                     $extencion_adi_file_3 = $exten_file_3[$i];
                }
            }
        }else {
            $extencion_adi_file_3 = '';
        }
        if (!empty($docAdicionales['file_4'])) {
            $exten_file_4=explode(".", $docAdicionales['file_4']);
            for($i=0; $i <= count($exten_file_4)-1; $i++){
                if($exten_file_4[$i] == 'pdf' || $exten_file_4[$i] == 'doc' || $exten_file_4[$i] =='docx'){
                    //return $exten_file_1[$i];
                     $extencion_adi_file_4 = $exten_file_4[$i];
                }
            }
        }else {
            $extencion_adi_file_4 = '';
        }
        if (!empty($docAdicionales['file_5'])) {
            $exten_file_5=explode(".", $docAdicionales['file_5']);
            for($i=0; $i <= count($exten_file_5)-1; $i++){
                if($exten_file_5[$i] == 'pdf' || $exten_file_5[$i] == 'doc' || $exten_file_5[$i] =='docx'){
                    //return $exten_file_1[$i];
                     $extencion_adi_file_5 = $exten_file_5[$i];
                }
            }
        }else {
            $extencion_adi_file_5 = '';
        }
        if (!empty($docAdicionales['file_6'])) {
            $exten_file_6=explode(".", $docAdicionales['file_6']);
            for($i=0; $i <= count($exten_file_6)-1; $i++){
                if($exten_file_6[$i] == 'pdf' || $exten_file_6[$i] == 'doc' || $exten_file_6[$i] =='docx'){
                    //return $exten_file_1[$i];
                     $extencion_adi_file_6 = $exten_file_6[$i];
                }
            }
        }else {
            $extencion_adi_file_6 = '';
        }

        $documentosNat=CatDocumentos::where('gen_ente_id',2)->where('bactivo',1)->whereIn('id',[16,17,18])->get();
        $documentosJuri=CatDocumentos::where('gen_ente_id',2)->where('bactivo',1)->whereIn('id',[19,20,21,18,22])->get();

        $documentosSport=CatDocumentos::where('gen_ente_id',2)->where('bactivo',1)->whereIn('id',[12,14])->get();

        return view('ListaCoordExtranjero.show',compact('solicitud','det_usuario','accionistas','estado_observacion','pais','titulo','descripcion','docCertificados','documentos','extencion_file_1','extencion_file_2','extencion_file_3','extencion_file_4','extencion_file_5','extencion_file_6','extencion_file_7','extencion_file_8','extencion_file_9','extencion_file_10','extencion_adi_file_1','extencion_adi_file_2','extencion_adi_file_3','extencion_adi_file_4','extencion_adi_file_5','extencion_adi_file_6','documentosNat','documentosJuri','documentosSport','docAdicionales','sopAdicionalesInv2','sopAdicionalesInv4'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\GenSolicitud  $genSolicitud
     * @return \Illuminate\Http\Response
     */
    public function edit(BandejaSolInversionista $bandejaSolInversionista,$id)
    {
        $titulo='Registro de Potencial Inversionista Extranjero';
        $descripcion='Perfil del Coordinador';

    /*****Consulto la solicitud para traerme el id de ese solicitud*****/
        $solicitud=GenSolInversionista::find($id);

         //$solicitud=GenSolInversionista::where('id',$id)->first();

     /** Consulta en mi bandeja si esta esa solicitud**/
        $bandejaanalista=BandejaSolInversionista::where('gen_sol_inversionista_id',$solicitud->id)->first();


    /************ Valido si la solicitud existe, Si existe no hago nada en caso contrario inserto la solicitud******************/

     /**Valido si esta**/
    $validaratencion=CoordInversionista::where('bandeja_sol_inversionista_id',$bandejaanalista->id)->where('gen_usuario_id',Auth::user()->id)->first();
     //dd($validaratencion);

      //dd($coordinversionista); 
     if(empty($validaratencion)){
       
        $solicitud->gen_status_id=14;
        $solicitud->fstatus=date('Y-m-d');
        $solicitud->save();
      
    /***Insertamos en  en la bandeja del coordinadorinversiones me traigo el id de esa solicitud con el usuario   analista le actualizo el status a 10 que es en Observacion la actualizo a la fecha de hoy****/

            $coordinversionlist= new CoordInversionista;
            $coordinversionlist->bandeja_sol_inversionista_id=$bandejaanalista->id;
            $coordinversionlist->gen_usuario_id=Auth::user()->id;
            $coordinversionlist->gen_status_id=14;
            $coordinversionlist->fstatus=date('Y-m-d');
            $coordinversionlist->save();

        }


/*************** Consulto en Detusuario para traerme el id*************************************/
      //Consulta el tipo de documentos de acuerdo el ente
        $documentos=CatDocumentos::where('gen_ente_id',2)->where('bactivo',1)->whereBetween('id',[10,17])->get(); 

           $estado_observacion=GenStatus::whereIn('id', [11,15,16,17,19])->pluck('nombre_status','id');
           //dd($estado_observacion); 

           $det_usuario=DetUsuario::where('gen_usuario_id',$solicitud->gen_usuario_id)->first();
           //dd($det_usuario);
        
           $pais=Pais::where('id',$solicitud->pais_id)->first();
           //dd($pais); 
         
         //Consulta los accionista de esa solicitud
           $accionistas=Accionistas::where('gen_usuario_id',$solicitud->gen_usuario_id)->where('bactivo',1)->get();

           //Documentos Inversionista//
           $docCertificados=GenDocumentosInversiones::where('gen_sol_inversionista_id',$solicitud->id)->first();

           $docAdicionales=Doc_Adic_Inversiones::where('gen_sol_inversionista_id',$solicitud->id)->first();

           //dd($docAdicionales);
           //
           $sopAdicionalesInv2=DocumentsVariosInversiones::where('gen_sol_inversionista_id',$solicitud->id)->where('cat_documentos_id',12)->get();

           $sopAdicionalesInv4=DocumentsVariosInversiones::where('gen_sol_inversionista_id',$solicitud->id)->where('cat_documentos_id',14)->get();


           //dd($docCertificados);
    
        //$extencion_file_1 = $exten_file_1[1];
     
         if(!empty($docCertificados['file_1'])){
             $exten_file_1=explode(".", $docCertificados['file_1']);
            for($i=0; $i <= count($exten_file_1)-1; $i++){
                if($exten_file_1[$i] == 'pdf' || $exten_file_1[$i] == 'doc' || $exten_file_1[$i] =='docx'){
                    //return $exten_file_1[$i];
                     $extencion_file_1 = $exten_file_1[$i];
                }
            }
//dd($extencion_file_1);
        } else {
            $extencion_file_1 = '';
        }
        if (!empty($docCertificados['file_2'])) {
            $exten_file_2=explode(".", $docCertificados['file_2']);
            for($i=0; $i <= count($exten_file_2)-1; $i++){
                if($exten_file_2[$i] == 'pdf' || $exten_file_2[$i] == 'doc' || $exten_file_2[$i] =='docx'){
                    //return $exten_file_1[$i];
                     $extencion_file_2 = $exten_file_2[$i];
                }
            }
        }else {
            $extencion_file_2 = '';
        }
        if (!empty($docCertificados['file_3'])) {
            $exten_file_3=explode(".", $docCertificados['file_3']);
             for($i=0; $i <= count($exten_file_3)-1; $i++){
                if($exten_file_3[$i] == 'pdf' || $exten_file_3[$i] == 'doc' || $exten_file_3[$i] =='docx'){
                    //return $exten_file_1[$i];
                     $extencion_file_3 = $exten_file_3[$i];
                }
            }
        }else {
            $extencion_file_3 = '';
        }
        if (!empty($docCertificados['file_4'])) {
            $exten_file_4=explode(".", $docCertificados['file_4']);
            for($i=0; $i <= count($exten_file_4)-1; $i++){
                if($exten_file_4[$i] == 'pdf' || $exten_file_4[$i] == 'doc' || $exten_file_4[$i] =='docx'){
                    //return $exten_file_1[$i];
                     $extencion_file_4 = $exten_file_4[$i];
                }
            }
        }else {
            $extencion_file_4 = '';
        }
        if (!empty($docCertificados['file_5'])) {
            $exten_file_5=explode(".", $docCertificados['file_5']);
            for($i=0; $i <= count($exten_file_5)-1; $i++){
                if($exten_file_5[$i] == 'pdf' || $exten_file_5[$i] == 'doc' || $exten_file_5[$i] =='docx'){
                    //return $exten_file_1[$i];
                     $extencion_file_5 = $exten_file_5[$i];
                }
            }
        }else {
            $extencion_file_5 = '';
        }
        if (!empty($docCertificados['file_6'])) {
            $exten_file_6=explode(".", $docCertificados['file_6']);
            for($i=0; $i <= count($exten_file_6)-1; $i++){
                if($exten_file_6[$i] == 'pdf' || $exten_file_6[$i] == 'doc' || $exten_file_6[$i] =='docx'){
                    //return $exten_file_1[$i];
                     $extencion_file_6 = $exten_file_6[$i];
                }
            }
        }else {
            $extencion_file_6 = '';
        }
        if (!empty($docCertificados['file_7'])) {
            $exten_file_7=explode(".", $docCertificados['file_7']);
            for($i=0; $i <= count($exten_file_7)-1; $i++){
                if($exten_file_7[$i] == 'pdf' || $exten_file_7[$i] == 'doc' || $exten_file_7[$i] =='docx'){
                    //return $exten_file_1[$i];
                     $extencion_file_7 = $exten_file_7[$i];
                }
            }
        }else {
            $extencion_file_7 = '';
        }
        if (!empty($docCertificados['file_8'])) {
            $exten_file_8=explode(".", $docCertificados['file_8']);
            for($i=0; $i <= count($exten_file_8)-1; $i++){
                if($exten_file_8[$i] == 'pdf' || $exten_file_8[$i] == 'doc' || $exten_file_8[$i] =='docx'){
                    //return $exten_file_1[$i];
                     $extencion_file_8 = $exten_file_8[$i];
                }
            }
        }else {
            $extencion_file_8 = '';
        }
        if (!empty($docCertificados['file_9'])) {
            $exten_file_9=explode(".", $docCertificados['file_9']);
            for($i=0; $i <= count($exten_file_9)-1; $i++){
                if($exten_file_9[$i] == 'pdf' || $exten_file_9[$i] == 'doc' || $exten_file_9[$i] =='docx'){
                    //return $exten_file_1[$i];
                     $extencion_file_9 = $exten_file_9[$i];
                }
            }
        }else {
            $extencion_file_9 = '';
        }
        if (!empty($docCertificados['file_10'])) {
            $exten_file_10=explode(".", $docCertificados['file_10']);
            for($i=0; $i <= count($exten_file_10)-1; $i++){
                if($exten_file_10[$i] == 'pdf' || $exten_file_10[$i] == 'doc' || $exten_file_10[$i] =='docx'){
                    //return $exten_file_1[$i];
                     $extencion_file_10 = $exten_file_10[$i];
                }
            }
        }else {
            $extencion_file_10 = '';
        }


         //Doc Adicionales
        if(!empty($docAdicionales['file_1'])){
            $exten_file_1=explode(".", $docAdicionales['file_1']);
            for($i=0; $i <= count($exten_file_1)-1; $i++){
                if($exten_file_1[$i] == 'pdf' || $exten_file_1[$i] == 'doc' || $exten_file_1[$i] =='docx'){
                    //return $exten_file_1[$i];
                     $extencion_adi_file_1 = $exten_file_1[$i];
                }
            }
        } else {
            $extencion_adi_file_1 = '';
        }
        if (!empty($docAdicionales['file_2'])) {
            $exten_file_2=explode(".", $docAdicionales['file_2']);
            for($i=0; $i <= count($exten_file_2)-1; $i++){
                if($exten_file_2[$i] == 'pdf' || $exten_file_2[$i] == 'doc' || $exten_file_2[$i] =='docx'){
                    //return $exten_file_1[$i];
                     $extencion_adi_file_2 = $exten_file_2[$i];
                }
            }
        }else {
            $extencion_adi_file_2 = '';
        }
        if (!empty($docAdicionales['file_3'])) {
            $exten_file_3=explode(".", $docAdicionales['file_3']);
            for($i=0; $i <= count($exten_file_3)-1; $i++){
                if($exten_file_3[$i] == 'pdf' || $exten_file_3[$i] == 'doc' || $exten_file_3[$i] =='docx'){
                    //return $exten_file_1[$i];
                     $extencion_adi_file_3 = $exten_file_3[$i];
                }
            }
        }else {
            $extencion_adi_file_3 = '';
        }
        if (!empty($docAdicionales['file_4'])) {
            $exten_file_4=explode(".", $docAdicionales['file_4']);
            for($i=0; $i <= count($exten_file_4)-1; $i++){
                if($exten_file_4[$i] == 'pdf' || $exten_file_4[$i] == 'doc' || $exten_file_4[$i] =='docx'){
                    //return $exten_file_1[$i];
                     $extencion_adi_file_4 = $exten_file_4[$i];
                }
            }
        }else {
            $extencion_adi_file_4 = '';
        }
        if (!empty($docAdicionales['file_5'])) {
            $exten_file_5=explode(".", $docAdicionales['file_5']);
            for($i=0; $i <= count($exten_file_5)-1; $i++){
                if($exten_file_5[$i] == 'pdf' || $exten_file_5[$i] == 'doc' || $exten_file_5[$i] =='docx'){
                    //return $exten_file_1[$i];
                     $extencion_adi_file_5 = $exten_file_5[$i];
                }
            }
        }else {
            $extencion_adi_file_5 = '';
        }
        if (!empty($docAdicionales['file_6'])) {
            $exten_file_6=explode(".", $docAdicionales['file_6']);
            for($i=0; $i <= count($exten_file_6)-1; $i++){
                if($exten_file_6[$i] == 'pdf' || $exten_file_6[$i] == 'doc' || $exten_file_6[$i] =='docx'){
                    //return $exten_file_1[$i];
                     $extencion_adi_file_6 = $exten_file_6[$i];
                }
            }
        }else {
            $extencion_adi_file_6 = '';
        }


        $documentosNat=CatDocumentos::where('gen_ente_id',2)->where('bactivo',1)->whereIn('id',[16,17,18])->get();
        $documentosJuri=CatDocumentos::where('gen_ente_id',2)->where('bactivo',1)->whereIn('id',[19,20,21,18,22])->get();
        $documentosSport=CatDocumentos::where('gen_ente_id',2)->where('bactivo',1)->whereIn('id',[12,14])->get();

        return view('ListaCoordExtranjero.edit',compact('solicitud','det_usuario','accionistas','estado_observacion','pais','titulo','descripcion','docCertificados','documentos','extencion_file_1','extencion_file_2','extencion_file_3','extencion_file_4','extencion_file_5','extencion_file_6','extencion_file_7','extencion_file_8','extencion_file_9','extencion_file_10','extencion_adi_file_1','extencion_adi_file_2','extencion_adi_file_3','extencion_adi_file_4','extencion_adi_file_5','extencion_adi_file_6','documentosNat','documentosJuri','documentosSport','docAdicionales','sopAdicionalesInv2','sopAdicionalesInv4'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\GenSolicitud  $genSolicitud
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, GenSolInversionista $GenSolInversionista,$id)
    {
        //Validamos si llega lleno la obervacion que viene por request y esta en observacion

          //Valido si el estatus que estoy recibiendo por request es 11
           if(!empty($request->observacion_inversionista ) && $request->gen_status_id==11){
              
              $coordinversionista=GenSolInversionista::find($id)->update(['gen_status_id'=>11,'estado_observacion'=>1,'observacion_inversionista'=>$request->observacion_inversionista,'updated_at' => date("Y-m-d H:i:s") ]);

              $bandejacoordinador=CoordInversionista::where('bandeja_sol_inversionista_id',$id)->update(['gen_status_id'=>11,'updated_at' => date("Y-m-d H:i:s") ]);


            }elseif(!empty($request->observacion_inversionista ) && $request->gen_status_id == 15){
              
              $coordinversionista=GenSolInversionista::find($id)->update(['gen_status_id'=>15,'estado_observacion'=>1,'observacion_doc_incomp'=>$request->observacion_inversionista,'updated_at' => date("Y-m-d H:i:s") ]);

               $bandejacoordinador=CoordInversionista::where('bandeja_sol_inversionista_id',$id)->update(['gen_status_id'=>15,'updated_at' => date("Y-m-d H:i:s") ]);

            }elseif($request->gen_status_id == 16){
                $fecha_actual = date("Y-m-d");
                //sumo 6 mes
                $fecha_expiracion= date("Y-m-d",strtotime($fecha_actual."+ 6 month")); 
            
                $coordinversionista=GenSolInversionista::find($id)->update(['gen_status_id'=>16,'fecha_expiracion'=>$fecha_expiracion,'updated_at' => date("Y-m-d H:i:s") ]);


             $bandejacoordinador=CoordInversionista::where('bandeja_sol_inversionista_id',$id)->update(['gen_status_id'=>16,'updated_at' => date("Y-m-d H:i:s") ]);


            }elseif(!empty($request->observacion_inversionista ) && $request->gen_status_id == 17){
              
              $coordinversionista=GenSolInversionista::find($id)->update(['gen_status_id'=>17,'estado_observacion'=>1,'observacion_anulacion'=>$request->observacion_inversionista,'updated_at' => date("Y-m-d H:i:s") ]);

              $bandejacoordinador=CoordInversionista::where('bandeja_sol_inversionista_id',$id)->update(['gen_status_id'=>17,'updated_at' => date("Y-m-d H:i:s") ]);

            //Agregando estatus para documentos adicionales//
              }elseif(!empty($request->observacion_inversionista ) && $request->gen_status_id == 19){
              
              $coordinversionista=GenSolInversionista::find($id)->update(['gen_status_id'=>19,'estado_observacion'=>1,'observacion_inversionista'=>$request->observacion_inversionista,'updated_at' => date("Y-m-d H:i:s") ]);

              $bandejacoordinador=CoordInversionista::where('bandeja_sol_inversionista_id',$id)->update(['gen_status_id'=>19,'updated_at' => date("Y-m-d H:i:s") ]);

          }

          $inversiones    =  GenSolInversionista::find($id);
          Mail::to($inversiones->DetUsuario->correo)->send(new NotificacionesInversiones($inversiones));

              Alert::warning('Su Solicitud ha sido Atendida ')->persistent("OK");
              return redirect()->action('ListaCoordExtranjeroController@index'); 

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\GenSolicitud  $genSolicitud
     * @return \Illuminate\Http\Response
     */
    public function destroy(BandejaSolInversionista $bandejaSolInversionista)
    {
        //
    }
}
