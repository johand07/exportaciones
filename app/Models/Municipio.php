<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Municipio extends Model
{
    protected $table = 'municipio';

    public static function municipios ($id){
    	return Municipio::where('estado_id','=',$id)->get();
    }
}
