<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductosInversion extends Model
{
    protected $table = 'productos_inversion';

    protected $fillable = [
    	'gen_sol_inversion_id';
    	'productos_id';
    	'capacidad_utilizada';
    	'unidad_tiempo';
    	'porcentaje_util';
    	'capacidad_ociosa';
    	'bactivo';
    ];

    protected $date = ['created_at','updated_at'];

    public function inversion(){
    	return $this->hasOne('App\Models\GenSolInversion','id','gen_sol_inversion_id');
    }

    public function productos(){
    	return $this->hasOne('App\Models\Productos','id','productos_id');
    }
}
