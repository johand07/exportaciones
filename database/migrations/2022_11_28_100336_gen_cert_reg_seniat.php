<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GenCertRegSeniat extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gen_cert_reg_seniat', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('gen_usuario_id')->nullable();
            $table->foreign('gen_usuario_id')->references('id')->on('gen_usuario');
            $table->unsignedInteger('gen_status_id')->nullable();
            $table->foreign('gen_status_id')->references('id')->on('gen_status');
            $table->string('codigo_arancelario',150)->nullable();
            $table->date('fecha_certificado',150)->nullable();
            $table->string('observacion_analista', 100)->nullable();
            $table->string('observacion_viceministro', 100)->nullable();
            $table->boolean('bactivo')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gen_cert_reg_seniat');
    }
}
