<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HistSesionUser extends Model
{
    protected $table = 'hist_sesion_user';

    protected $fillable = [

    	'gen_usuario_id',
    	'ip_usuario',
    	'last_login',
    	'bactivo',
        'sesion',
    ];

    protected $date =['created_at','updated_at'];

     public function usuario() {
     return $this->hasOne('App\Models\GenUsuario','id','gen_usuario_id');

    }
}
