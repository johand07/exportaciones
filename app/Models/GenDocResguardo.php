<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GenDocResguardo extends Model
{
    protected $table = 'gen_doc_resguardo';

    protected $fillable = [

           'gen_sol_resguardo_aduanero_id',
           'file_1',
           'file_2',
           'file_3',
           'file_4',
           'file_5',
           'file_6',
           'file_7',
           'file_8',
           'file_9',
           'file_10',
           'file_11',
           'file_12',
           'file_13',
           'file_14',
           'file_15',
           'file_16',
           'file_17',
           'file_18',
           'file_19',
           'file_20',
           'file_21',
           'file_22',
           'file_23',
           'file_24',
           'file_25',
           'file_26',
           'file_27',
           'bactivo',
    ];

    protected $date =['created_at','updated_at'];

    
    public function rResguardoAduanero() {

        return $this->hasOne('App\Models\GenSolResguardoAduanero','id','gen_sol_resguardo_aduanero_id');

    }
}
