@extends('templates/layoutlte_coordinador_djo')

@section('content')
<div class="panels">
<div class="content" style="background-color:#fff">

    <div class="panel-group" style="background-color:#fff">
        <div class="panel-primary">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-8"><h3>DETALLE ANÁLISIS DE CERTIFICADO DE ORIGEN</h3></div>
                    <div class="col-md-2"><!-- Traerme la consulta con el numero de solicitud-->
                    
                        <h4><b>Nº Certificado:</b></h4>
                          {{ $certificados->num_certificado ? $certificados->num_certificado: '' }}
                    </div>
                    
                    <div class="col-md-2"><!-- Traerme la consulta con la fecha de solicitud-->
                        <h4><b>Fecha Solicitud:</b></h4>
                          {{ $certificados->created_at}}
                    </div>
                </div>
            </div><hr>
            
            <div class="panel-body">
       
                <div class="row">
                <div class="col-md-12">

                <!-- Agregando los documentos en el perfil del coordinador-->
                <button type="button" class="btn btn-warning btn-lg" data-toggle="modal" data-target="#myModal">Documentos Cargados</button>
                    <br><br>
                <!-- Modal -->
                <div id="myModal" class="modal fade" role="dialog">
                <div class="modal-dialog modal-lg">
                    <!-- Modal content-->
                    <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h3 class="modal-title" style="color:#64A4CA;"><b>Documentos de Certificado de Origen</b></h3>
                        <!--Mostrar los nombre de los documentos-->
                        <div id="mostrar_info">
                        <a href="#"><span class="glyphicon glyphicon-info-sign text-success"> Ver info.</span> </a>
                        </div>
                        <div id="ocultar_info" style="display: none;">
                        <a href="#"><span class="glyphicon glyphicon-resize-small text-warning"> Ocultar info.</span></a>
                        </div>
                        <div class="col-md-12" id="det_info" style="display: none;">
                        <div class="col-md-12">
                            <div class="text-muted text-justify">
                            <p></p>
                            <ul>
                                <li>1.)Cargar el formato de solicitud</li>
                                <li>2.)Declaración jurada de Origen</li>
                                <li>3.)Factura comercial de exportador</li>
                                <li>4.) Pago por concepto de solicitud de certificado de origen (0,25 petros)</li>
                                <li>5.) Pago de 0,02 U.T Gobierno del Distrito Capital</li>
                                <li>6.) Factura de productor (Si es comercializador)</li>
                            </ul>
                            </div>
                        </div>
                        </div>
                    </div>
                    <div class="modal-body">
                        <div class="modal-body">
                                <div role="tabpanel">
                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li role="presentation" class="active"><a href="#p2" class="cargador-planilla" aria-controls="p2" role="tab" data-toggle="tab" id="tab-inicial">Documento 1</a>
                                        </li>
                                        <li role="presentation"><a href="#p3" class="cargador-planilla" aria-controls="p3" role="tab" data-toggle="tab">Documento 2</a>
                                        </li>
                                        <li role="presentation"><a href="#p4" class="cargador-planilla" aria-controls="p4" role="tab" data-toggle="tab">Documento 3</a>
                                        </li>
                                        <li role="presentation"><a href="#p5" class="cargador-planilla" aria-controls="p5" role="tab" data-toggle="tab">Documento 4</a>
                                        </li>
                                        <li role="presentation"><a href="#p6" class="cargador-planilla" aria-controls="p6" role="tab" data-toggle="tab">Documento 5</a>
                                        </li>
                                        <li role="presentation"><a href="#p7" class="cargador-planilla" aria-controls="p7" role="tab" data-toggle="tab">Documento 6</a>
                                        </li>
                                    </ul>
                                    <!-- Tab panes -->
                                    </div>
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane active text-center" id="p2">
                                        @if(file_exists($docCertificados['file_1']))
                                        @if(isset($docCertificados['file_1']))
                                            @if(isset($extencion_file_1) && $extencion_file_1 == 'pdf')
                                                <span class="text-success"></span>
                                                <br>
                                                <embed id="fred" src="{{asset($docCertificados['file_1'])}}" width="800" height="600" type="application/pdf">

                                                @elseif(isset($extencion_file_1) && ($extencion_file_1 == 'docx' || $extencion_file_1 == 'doc' || $extencion_file_1 == 'pptx' || $extencion_file_1 == 'ppt'))
                                                <br><br>
                                                <a href="{{asset($docCertificados['file_1'])}}" class="btn btn-lg btn-primary"><span><i class="glyphicon glyphicon-cloud-download"></i> </span>Descargar documento 1</a>
                                                <br><br>
                                                @else
                                                .
                                            @endif
                                        @endif
                                        @else
                                        <br>
                                        <div class="alert alert-warning" style="color:#fff"><strong>Atención!</strong> No es posible ubicar este fichero, por favor comunicarse con el administrador de sistemas.</div>
                                        @endif
                                        </div>
                                        <div role="tabpanel" class="tab-pane text-center" id="p3">
                                        @if(file_exists($docCertificados['file_2']))
                                        @if(isset($docCertificados['file_2']))
                                            @if(isset($extencion_file_2) && $extencion_file_2 == 'pdf')
                                                <span class="text-success"></span>
                                                <br>
                                                <embed id="fred" src="{{asset($docCertificados['file_2'])}}" width="800" height="600" type="application/pdf">
                                                @elseif(isset($extencion_file_2) && ($extencion_file_2 == 'docx' || $extencion_file_2 == 'doc' || $extencion_file_2 == 'pptx' || $extencion_file_2 == 'ppt'))
                                                <br><br>
                                                <a href="{{asset($docCertificados['file_2'])}}" class="btn btn-lg btn-primary"><span><i class="glyphicon glyphicon-cloud-download"></i> </span>Descargar documento 2</a>
                                                <br><br>
                                                @else
                                                .
                                            @endif
                                        @endif
                                        @else
                                        <br>
                                        <div class="alert alert-warning" style="color:#fff"><strong>Atención!</strong> No es posible ubicar este fichero, por favor comunicarse con el administrador de sistemas.</div>
                                        @endif
                                        </div>
                                        <div role="tabpanel" class="tab-pane text-center" id="p4">
                                        @if(file_exists($docCertificados['file_3']))
                                        @if(isset($docCertificados['file_3']))
                                            @if(isset($extencion_file_3) && $extencion_file_3 == 'pdf')
                                                <span class="text-success"></span>
                                                <br>
                                                <embed id="fred" src="{{asset($docCertificados['file_3'])}}" width="800" height="600" type="application/pdf">
                                                @elseif(isset($extencion_file_3) && ($extencion_file_3 == 'docx' || $extencion_file_3 == 'doc' || $extencion_file_3 == 'pptx' || $extencion_file_3 == 'ppt'))
                                                <br><br>
                                                <a href="{{asset($docCertificados['file_3'])}}" class="btn btn-lg btn-primary"><span><i class="glyphicon glyphicon-cloud-download"></i> </span>Descargar documento 3</a>
                                                <br><br>
                                                @else
                                                .
                                            @endif
                                        @endif
                                        @else
                                        <br>
                                        <div class="alert alert-warning" style="color:#fff"><strong>Atención!</strong> No es posible ubicar este fichero, por favor comunicarse con el administrador de sistemas.</div>
                                        @endif
                                        </div>
                                        <div role="tabpanel" class="tab-pane text-center" id="p5">
                                        @if(file_exists($docCertificados['file_4']))
                                        @if(isset($docCertificados['file_4']))
                                            @if(isset($extencion_file_4) && $extencion_file_4 == 'pdf')
                                                <span class="text-success"></span>
                                                <br>
                                                <embed id="fred" src="{{asset($docCertificados['file_4'])}}" width="800" height="600" type="application/pdf">
                                                @elseif(isset($extencion_file_4) && ($extencion_file_4 == 'docx' || $extencion_file_4 == 'doc' || $extencion_file_4 == 'pptx' || $extencion_file_4 == 'ppt'))
                                                <br><br>
                                                <a href="{{asset($docCertificados['file_4'])}}" class="btn btn-lg btn-primary"><span><i class="glyphicon glyphicon-cloud-download"></i> </span>Descargar documento 4</a>
                                                <br><br>
                                                @else
                                                .
                                            @endif
                                        @endif
                                        @else
                                        <br>
                                        <div class="alert alert-warning" style="color:#fff"><strong>Atención!</strong> No es posible ubicar este fichero, por favor comunicarse con el administrador de sistemas.</div>
                                        @endif
                                        </div>
                                        <div role="tabpanel" class="tab-pane text-center" id="p6">
                                        @if(file_exists($docCertificados['file_5']))
                                        @if(isset($docCertificados['file_5']))
                                            @if(isset($extencion_file_5) && $extencion_file_5 == 'pdf')
                                                <span class="text-success"></span>
                                                <br>
                                                <embed id="fred" src="{{asset($docCertificados['file_5'])}}" width="800" height="600" type="application/pdf">
                                                @elseif(isset($extencion_file_5) && ($extencion_file_5 == 'docx' || $extencion_file_5 == 'doc' || $extencion_file_5 == 'pptx' || $extencion_file_5 == 'ppt'))
                                                <br><br>
                                                <a href="{{asset($docCertificados['file_5'])}}" class="btn btn-lg btn-primary"><span><i class="glyphicon glyphicon-cloud-download"></i> </span>Descargar documento 5</a>
                                                <br><br>
                                                @else
                                                .
                                            @endif
                                        @endif
                                        @else
                                        <br>
                                        <div class="alert alert-warning" style="color:#fff"><strong>Atención!</strong> No es posible ubicar este fichero, por favor comunicarse con el administrador de sistemas.</div>
                                        @endif
                                        </div>
                                        <div role="tabpanel" class="tab-pane text-center" id="p7">
                                        @if(file_exists($docCertificados['file_6']))
                                        @if(isset($docCertificados['file_6']))
                                            @if(isset($extencion_file_6) && $extencion_file_6 == 'pdf')
                                                <span class="text-success"></span>
                                                <br>
                                                <embed id="fred" src="{{asset($docCertificados['file_6'])}}" width="800" height="600" type="application/pdf">
                                                @elseif(isset($extencion_file_6) && ($extencion_file_6 == 'docx' || $extencion_file_6 == 'doc' || $extencion_file_6 == 'pptx' || $extencion_file_6 == 'ppt'))
                                                <br><br>
                                                <a href="{{asset($docCertificados['file_6'])}}" class="btn btn-lg btn-primary"><span><i class="glyphicon glyphicon-cloud-download"></i> </span>Descargar documento 6</a>
                                                <br><br>
                                                @else
                                                .
                                            @endif
                                        @endif
                                        @else
                                        <br>
                                        <div class="alert alert-warning" style="color:#fff"><strong>Atención!</strong> No es posible ubicar este fichero, por favor comunicarse con el administrador de sistemas.</div>
                                        @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>
                    <div class="modal-footer">
                    </div>
                    </div>
                </div>
                <!-- Fin de modal de los documentos-->
                <br>
                    @if($tipocertificado == 1)<!--Certificado de Colombia-->
                                        <div class="row">
                                    <h4 class="text-center"><b>CERTIFICADO DE ORIGEN<br>ACUERDO DE ALCANCE PARCIAL COLOMBIA - VENEZUELA</b></h4>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <h4><b>(1) País Exportador:</b></h4>
                                                    {{$certificados->pais_exportador ? $certificados->pais_exportador : ''}}
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <h4><b>(2) País Importador:</b></h4>
                                                    {{$certificados->pais_importador ? $certificados->pais_importador : ''}}
                                                </div>
                                            </div>
                                        </div><!--Primer row con datos pais del exportador-->
                                <!--Segundo Row-->
                                <div class="row">
                                        <table class="table table-bordered text-left" id="productos">
                                        <thead>
                                            <th class="col-md-1 text-center">(3) Nº. De Orden:</th>
                                            <th class=" col-md-2 text-center">(4) Código arancelario<br>
                                                        en la nomenclatura de <br>
                                                    la parte exportadora:</th>
                                            <th class="col-md-5 text-center">(5) Denominación de las 
                                                        <br>mercancías descripción<br>
                                                        arancelaria y comercial:</th>
                                            <th class="col-md-1 text-center">(6) Unidad física y cantidad<br>
                                                        según factura:</th>
                                            <th class="col-md-1 text-center">(7) Valor FOB de cada<br>
                                                        mercancía según<br>
                                                        factura en US$:</th>

                                            <th class="col-md-1 text-center">(8) Cantidad según factura<br>
                                                    para cupos capítulo 72:</th>
                                        </thead>
                                        <tbody>
                                            @foreach ($detalleProduct as $key => $detalleP)
                                            <tr class="text-center">
                                                <td>{{$detalleP->num_orden ? $detalleP->num_orden : ''}}</td>
                                                <td>{{$detalleP->codigo_arancel ? $detalleP->codigo_arancel : ''}}</td>
                                                <td>{{$detalleP->denominacion_mercancia ? $detalleP->denominacion_mercancia : ''}}</td>
                                                <td>{{$detalleP->unidad_fisica ? $detalleP->unidad_fisica : ''}}</td>
                                                <td>{{$detalleP->valor_fob ? $detalleP->valor_fob : ''}}</td>
                                                <td>{{$detalleP->cantidad_segun_factura ? $detalleP->cantidad_segun_factura : ''}}</td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                
                                <!--Fin del segundo row datos de productos-->
                                <!--Tercer row para factura-->
                                    <div class="row">
                                        <table class="table table-bordered text-left" id="productos">
                                        <center><h4><b>(9) DECLARACION DE ORIGEN</b></h4></center>
                                        <thead>
                                            <th class="col-md-6 text-center">Factura (s) Comercial (es) No:</th>
                                            <th class="col-md-6 text-center">Fecha:
                                            </th>
                                        </thead>
                                        <tbody><!--Itero el detalle de la declaracion numero factura y la fecha-->
                                        
                                            <tr class="text-center">
                                                <td>{{$detdeclaracionCert[0]->numero_factura ? $detdeclaracionCert[0]->numero_factura : ''}}</td>
                                                <td>{{$detdeclaracionCert[0]->f_fac_certificado ? $detdeclaracionCert[0]->f_fac_certificado : ''}}</td>
                                            </tr>
                                        
                                        </tbody>
                                        </table><br>
                                        <table class="table table-bordered text-left" id="productos">
                                        <thead>
                                            <th class="col-md-2 text-center">(10) Nº. De Orden :</th>
                                            <th class="col-md-10 text-center">(11) Criterio para la Calificación de Origen:
                                            </th>
                                            
                                        </thead>
                                        <tbody>
                                            @foreach ($detdeclaracionCert as $key => $detalleCert)
                                            <tr class="text-center">
                                                <td>{{$detalleCert->num_orden_declaracion ? $detalleCert->num_orden_declaracion : ''}}</td>
                                                <td>{{$detalleCert->normas_criterio ?$detalleCert->normas_criterio : ''}}</td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                        </table>
                                    </div><br>
                <!--Seccion de los criterio-->
                                    <!--Seccion del exportador-->
                                    <div class="row">
                                        <div class="col-md-6">
                                            <h4><b>(12)Productor o exportador</b></h4>
                                            <div class="form-group">
                                                <h4><b>12.1 Nombre o Razón Social:</b></h4>
                                                {{$certificados->GenUsuario ? $certificados->GenUsuario->DetUsuario->razon_social : ''}}
                                            </div>
                                            <div class="form-group">
                                            <h4><b>12.2 Número de identificación Fiscal:</b></h4>
                                                {{$certificados->GenUsuario ? $certificados->GenUsuario->DetUsuario->rif : ''}} 
                                            </div>
                                            <div class="form-group">
                                                <h4><b>12.3 Dirección:</b></h4>
                                                {{$certificados->GenUsuario ? $certificados->GenUsuario->DetUsuario->direccion : ''}}
                                            </div>
                                            <div class="form-group">
                                                <h4><b>12.4 País y Cuidad:</b></h4>
                                                {{$certificados->pais_cuidad_exportador ? $certificados->pais_cuidad_exportador : ''}}
                                            </div>
                                            <div class="form-group">
                                                <h4><b>12.5 E-mail:</b></h4>
                                                {{$certificados->GenUsuario ? $certificados->GenUsuario->DetUsuario->correo : ''}}
                                            </div>
                                            <div class="form-group">
                                                <h4><b>12.6 Teléfono:</b></h4>
                                                {{$certificados->GenUsuario ? $certificados->GenUsuario->DetUsuario->telefono_movil : ''}}
                                            </div>
                                            <div class="form-group">
                                                <h4><b>12.7 Fecha de Emision:</b></h4>
                                                {{$certificados->fecha_emision_exportador ? $certificados->fecha_emision_exportador : ''}}
                                            </div>
                                            
                                        </div>
                                        <div class="col-md-6">
                                            <h4><b>(14) Importador</b></h4>
                                            <div class="form-group">
                                                <h4><b>14.1 Nombre o Razón Social:</b></h4>
                                                {{$importadorCertificado->razon_social_importador ? $importadorCertificado->razon_social_importador : ''}}
                                            </div>
                                            <div class="form-group">
                                            <h4><b>14.2 Dirección:</b></h4>
                                                {{$importadorCertificado->direccion_importador ? $importadorCertificado->direccion_importador : ''}} 
                                            </div>
                                            <div class="form-group">
                                                <h4><b>14.3 País y Cuidad:</b></h4>
                                                {{$importadorCertificado->pais_ciudad_importador ? $importadorCertificado->pais_ciudad_importador : ''}}
                                            </div>
                                            <div class="form-group">
                                                <h4><b>14.4 E-mail:</b></h4>
                                                {{$importadorCertificado->email_importador ? $importadorCertificado->email_importador : ''}}
                                            </div>
                                            <div class="form-group">
                                                <h4><b>14.5 Teléfono:</b></h4>
                                                {{$importadorCertificado->telefono ? $importadorCertificado->telefono: ''}}
                                            </div>
                                            <div class="form-group">
                                                <h4><b>(15) Medio de Transporte (si es conocido):</b></h4>
                                                {{$importadorCertificado->medio_transporte_importador ? $importadorCertificado->medio_transporte_importador : ''}}
                                            </div>
                                            <div class="form-group">
                                                <h4><b>(16) Puerto o Lugar Embarque (si es conocido):</b></h4>
                                                {{$importadorCertificado->lugar_embarque_importador ? $importadorCertificado->lugar_embarque_importador : ''}}
                                            </div>
                                        </div>
                                        <div class="row"><!--Campo observaciones-->
                                            <div class="col-md-12 text-rigth">
                                                <h4><b>(17) OBSERVACIONES</b></h4>
                                                {{$certificados->observaciones ? $certificados->observaciones : ''}}
                                            </div>
                                        </div> 
                                    </div><br><br>
                            <!--Seccion del exportador e importador -->
                <!--Validaciones para formularios de Bolivia -->
                            <!--Valido si el tipo de certificado es tipo 2:Bolivia-->
                    @elseif($tipocertificado == 2)<!--Certificado de Bolivia-->
                        <div class="row">
                        <h4 class="text-center"><b>ACUERDO DE COMERCIO DE LOS PUEBLOS PARA LA COMPLEMENTARIEDAD ECONÓMICA, PRODUCTIVA Y EL INTERCAMBIO COMERCIAL <br>SOLIDARIO, ENTRE EL GOBIERNO DE LA REPUBLICA BOLIVARIANA DE VENEZUELA Y EL ESTADO PLURINACIONAL DE BOLIVIA</b></h4>
                                <div class="col-md-6 text-center">
                                    <div class="form-group">
                                        <h4><b>(1) País Exportador:</b></h4>
                                        {{$certificados->pais_exportador ? $certificados->pais_exportador : ''}}
                                    </div>
                                </div>
                                
                                <div class="col-md-6 text-center">
                                    <div class="form-group">
                                        <h4><b>(2) País Importador:</b></h4>
                                        {{$certificados->pais_importador ? $certificados->pais_importador : ''}}
                                    </div>
                                </div>
                        </div><!--Primer row con parte del certificado Bolivia-->

                        <div class="row">
                            <table class="table table-bordered text-left" id="productos">
                                <thead>
                                    <th class="col-md-1 text-center"><h4><b>(3) Nº. De Orden:</b></h4></th>
                                    <th class=" col-md-3 text-center"><h4><b>(4) Código arancelario:</b></h4></th>
                                    <th class="col-md-4 text-center"><h4><b>(5) Denominación de las Mercancías:</b></h4></th>
                                    <th class="col-md-2 text-center"><h4><b>(6) Peso o cantidad<br>
                                        según factura:</b></h4></th>
                                        <th class="col-md-2 text-center"><h4><b>(7) Valor FOB <br>en US$:</b></h4></th>

                                </thead>
                                <tbody>
                                    @foreach ($detalleProduct as $key => $detalleP)
                                    <tr class="text-center">
                                        <td>{{$detalleP->num_orden ? $detalleP->num_orden : ''}}</td>
                                        <td>{{$detalleP->codigo_arancel ? $detalleP->codigo_arancel : ''}}</td>
                                        <td>{{$detalleP->denominacion_mercancia ? $detalleP->denominacion_mercancia : ''}}</td>
                                        <td>{{$detalleP->unidad_fisica ? $detalleP->unidad_fisica : ''}}</td>
                                        <td>{{$detalleP->valor_fob ? $detalleP->valor_fob : ''}}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                <!--Fin de detalle producto-->
        <div class="row">
            <table class="table table-bordered text-left" id="productos">
            <center><h4><b>(8) DECLARACION DE ORIGEN</b></h4></center>
                <thead>
                    <th class="col-md-6 text-center"><h4><b>Factura Comercial No EX:</b></h4></th>
                    <th class="col-md-6 text-center"><h4><b>Fecha:</b></h4>
                    </th>
                </thead>
                <tbody><!--Itero el detalle de la declaracion numero factura y la fecha-->
                    <tr class="text-center">
                        <td>{{$detdeclaracionCert[0]->numero_factura ? $detdeclaracionCert[0]->numero_factura : '' }}</td>
                        <td>{{$detdeclaracionCert[0]->f_fac_certificado ? $detdeclaracionCert[0]->f_fac_certificado : '' }}</td>
                    </tr>
                </tbody>
            </table><br>
            <table class="table table-bordered text-left" id="productos">
                <thead>
                    <th class="col-md-2 text-center"><h4><b>(9) Nº. De Orden :</b></h4></th>
                    <th class="col-md-10 text-center"><h4><b>(10) Normas :</b></h4></th>
                </thead>
                <tbody>
                    @foreach ($detdeclaracionCert as $key => $detalleCert)
                    <tr>
                        <td class="text-center">{{$detalleCert->num_orden_declaracion ? $detalleCert->num_orden_declaracion : ''}}</td>
                        <td class="text-center">{{$detalleCert->normas_criterio ? $detalleCert->normas_criterio : ''}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div><br>

        <div class="row">
            <div class="col-md-6">
                <h4><b>(11) EXPORTADOR O PRODUCTOR</b></h4>
                <div class="form-group">
                    <h4><b>11.1 Nombre o Razón Social:</b></h4>
                    {{$certificados->GenUsuario ? $certificados->GenUsuario->DetUsuario->razon_social : ''}}
                </div>
                <div class="form-group">
                    <h4><b>11.2 Dirección:</b></h4>
                    {{$certificados->GenUsuario ? $certificados->GenUsuario->DetUsuario->direccion : ''}}
                </div>
                <div class="form-group">
                    <h4><b>11.3 Fecha</b></h4>
                    {{$certificados->fecha_emision_exportador ? $certificados->fecha_emision_exportador : ''}}
                </div>
            </div>

            <div class="col-md-6">
                <h4><b>(13) IMPORTADOR</b></h4>
                <div class="form-group">
                    <h4><b>13.1 Nombre o Razón Social:</b></h4>
                    {{$importadorCertificado->razon_social_importador ? $importadorCertificado->razon_social_importador : ''}}
                </div>
                <div class="form-group">
                    <h4><b>13.2 Dirección:</b></h4>
                    {{$importadorCertificado->direccion_importador ? $importadorCertificado->direccion_importador : ''}} 
                </div>
                <div class="form-group">
                    <h4><b>(14) Medio de Transporte</b></h4>
                    {{$importadorCertificado->medio_transporte_importador ? $importadorCertificado->medio_transporte_importador:''}}
                </div>
                <div class="form-group">
                    <h4><b>(15) Puerto o Lugar Embarque</b></h4>
                    {{$importadorCertificado->lugar_embarque_importador ? $importadorCertificado->lugar_embarque_importador : ''}}
                </div>
            </div>
        </div>

        <div class="row"><!--Campo observaciones-->
            <div class="col-md-12 text-rigth">
                <h4><b>(16) OBSERVACIONES</b></h4>
                {{$certificados->observaciones ? $certificados->observaciones : ''}}
            </div>
        </div>  
        <br><br><br>

<!--Validaciones planilla de chile -->
    @elseif($tipocertificado == 3)
                   
                    <div class="row">
                      <h4 class="text-center"><b>CERTIFICADO DE ORIGEN ASOCIACION LATINOAMERICANA DE INTEGRACION ACUERDO DE COMPLEMENTACION ECONOMICA No. 23</b></h4>
                   
                            <table class="table table-bordered text-left">
                                <thead>
                                    <th class="col-md-6 text-left">1. PAIS EXPORTADOR:</th>
                                    <th class=" col-md-16 text-left">2. PAIS IMPORTADOR:</th>
                                </thead>
                                <tbody>
                                    <tr class="text-left">
                                        <td>{{$certificados->pais_exportador ? $certificados->pais_exportador : ''}}</td>
                                        <td>{{$certificados->pais_importador ? $certificados->pais_importador : ''}}</td>
                                    </tr>
                                </tbody>
                            </table>
                    </div>

                    <div class="row">
                        <table class="table table-bordered text-left">
                        <thead>
                            <th class="col-md-4 text-left">3. DATOS DEL PRODUCTOR:</th>
                            <th class=" col-md-4 text-left">4. DATOS DEL EXPORTADOR:</th>
                            <th class="col-md-4 text-left">5. DATOS DEL IMPORTADOR:</th>
                        </thead>
                        <tbody>
                            <tr class="text-left">
                                <td>{{$certificados->razon_social_productor ? $certificados->razon_social_productor : ''}}<br>{{$certificados->direccion_productor ? $certificados->direccion_productor : ''}}</td>
                                <td>{{$det_usuario->razon_social ? $det_usuario->razon_social : ''}}<br>{{$det_usuario->direccion ? $det_usuario->direccion : ''}}</td>
                                <td>{{$importadorCertificado->razon_social_importador ? $importadorCertificado->razon_social_importador : ''}}<br>{{$importadorCertificado->direccion_importador ? $importadorCertificado->direccion_importador : ''}}</td>
                            </tr>
                        </tbody>
                        </table>
                    </div>

                    <div class="row">
                        <table class="table table-bordered text-left">
                        <thead>
                            <th class="col-md-4 text-left">6. RUT/RIF PRODUCTOR:</th>
                            <th class=" col-md-4 text-left">7. RUT/RIF EXPORTADOR:</th>
                            <th class="col-md-4 text-left">8. RUT/RIF IMPORTADOR:</th>
                        </thead>
                        <tbody>
                            <tr class="text-left">
                                <td>{{$certificados->rif_productor ? $certificados->rif_productor : ''}}</td>
                                <td>{{$det_usuario->rif ? $det_usuario->rif : ''}}</td>
                                <td>{{$importadorCertificado->rif_importador ? $importadorCertificado->rif_importador : ''}}</td>
                            </tr>
                        </tbody>
                        </table>
                    </div>

                    <div class="row">
                        <table class="table table-bordered text-left">
                        <thead>
                            <th class="col-md-4 text-left">9.PUERTO O LUGAR DE EMBARQUE:</th>
                            <th class=" col-md-4 text-left">10. FACTURA COMERCIAL No.:</th>
                            <th class="col-md-4 text-left">FECHA:</th>
                        </thead>
                        <tbody>
                            <tr class="text-left">
                                <td>{{$importadorCertificado->lugar_embarque_importador ? $importadorCertificado->lugar_embarque_importador : ''}}</td>
                                <td>{{$detdeclaracionCert[0]->numero_factura ? $detdeclaracionCert[0]->numero_factura : ''}}</td>
                                <td><span class="date-exported">{{$detdeclaracionCert[0]->f_fac_certificado ? $detdeclaracionCert[0]->f_fac_certificado : ''}}</span></td>
                            </tr>
                        </tbody>
                        </table>
                    </div>

                    <div class="row">
                            <table class="table table-bordered">
                                <thead>
                                    <th class="col-md-1 text-center">11. No. DE ORDEN:</th>
                                    <th class=" col-md-3 text-center">12. CODIGO <br> ARANCELARIO</th>
                                    <th class="col-md-4 text-center">13. DENOMINACION DE LAS MERCANCIAS</th>
                                    <th class="col-md-2 text-center">14. CANTIDAD Y <br>MEDIDA</th>
                                    <th class="col-md-2 text-center">15. VALORES FOB<br>EN US$</th>
                                </thead>
                                <tbody>
                                    @foreach ($detalleProduct as $key => $detalleP)
                                    <tr class="text-center">
                                        <td>{{$detalleP->num_orden ? $detalleP->num_orden : ''}}</td>
                                        <td>{{$detalleP->codigo_arancel ? $detalleP->codigo_arancel : ''}}</td>
                                        <td>{{$detalleP->denominacion_mercancia ? $detalleP->denominacion_mercancia : ''}}</td>
                                        <td>{{$detalleP->unidad_fisica ? $detalleP->unidad_fisica : ''}}</td>
                                        <td>{{$detalleP->valor_fob ? $detalleP->valor_fob : ''}}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                    </div>

                    <div class="row">
                            <table class="table table-bordered text-left">
                                <thead>
                                    <th class="col-md-1 text-center">16. No. DE <br> ORDEN:</th>
                                    <th class=" col-md-10 text-center">17. NORMAS DE ORIGEN</th>
                                </thead>
                                <tbody>  
                                @foreach ($detdeclaracionCert as $key => $detalleCert)                     
                                    <tr class="text-center">
                                        <td class="text-center">{{$detalleCert->num_orden_declaracion ? $detalleCert->num_orden_declaracion : ''}}</td>
                                        <td class="text-center">{{$detalleCert->normas_criterio ? $detalleCert->normas_criterio : ''}}</td>
                                    </tr>
                                @endforeach  
                                </tbody>
                            </table>
                    </div>
                    <div class="row">
                        <table class="table table-bordered text-left">
                            <thead>
                                <th class="col-md-12 text-left">19. OBSERVACIONES:</th>
                            </thead>
                            <tbody>
                                <tr class="text-left">
                                     <td>{{$certificados->observaciones ? $certificados->observaciones : ''}}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
<!--Vista de Certificado de Origen Aladi -->
    @elseif($tipocertificado == 4)
    <div class="row">
        <h4 class="text-center"><b>CERTIFICADO DE ORIGEN</b></h4>
        <div class="col-md-6">
            <div class="form-group">
                <h4><b>(1) País Exportador:</b></h4>
                {{$certificados->pais_exportador ? $certificados->pais_exportador : ''}}
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <h4><b>(2) País Importador:</b></h4>
                {{$certificados->pais_importador ? $certificados->pais_importador : ''}}
            </div>
        </div>
    </div><!--Primer row con datos pais del exportador-->
    <!--Segundo Row-->
    <div class="row">
        <table class="table table-bordered text-left" id="productos">
            <thead>
                <th class="col-md-1 text-center">Nº De Orden (3)*</th>
                <th class=" col-md-2 text-center">(4) Código arancelario</th>
                <th class="col-md-5 text-center">(5) DENOMINACION DE LAS MERCADERIAS</th>
            </thead>
            <tbody>
                @foreach ($detalleProduct as $key => $detalleP)
                <tr class="text-center">
                    <td>{{$detalleP->num_orden ? $detalleP->num_orden : ''}}</td>
                    <td>{{$detalleP->codigo_arancel ? $detalleP->codigo_arancel : ''}}</td>
                    <td>{{$detalleP->denominacion_mercancia ? $detalleP->denominacion_mercancia : ''}}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>

    <!--Fin del segundo row datos de productos-->
    <!--Tercer row para factura-->
    <div class="row">
        <table class="table table-bordered text-left" id="productos">
            <center><h4><b>DECLARACION DE ORIGEN</b></h4></center>
            <thead>
                <th class="col-md-6 text-center">Factura Comercial</th>
                <th class="col-md-6 text-center">Normas de Origen:
                </th>
            </thead>
            <tbody><!--Itero el detalle de la declaracion numero factura y la fecha-->
                <tr class="text-center">
                    <td>{{$detdeclaracionCert[0]->numero_factura ? $detdeclaracionCert[0]->numero_factura : ''}}</td>
                    <td>{{$detdeclaracionCert[0]->nombre_norma_origen ? $detdeclaracionCert[0]->nombre_norma_origen : ''}}</td>
                </tr>
            </tbody>
        </table><br>
        <table class="table table-bordered text-left" id="productos">
            <thead>
                <th class="col-md-2 text-center">Nº De Orden (6)*</th>
                <th class="col-md-10 text-center">(7) NORMAS **
                </th>
            </thead>
            <tbody>
                @foreach ($detdeclaracionCert as $key => $detalleCert)
                <tr class="text-center">
                    <td>{{$detalleCert->num_orden_declaracion ? $detalleCert->num_orden_declaracion : ''}}</td>
                    <td>{{$detalleCert->normas_criterio ? $detalleCert->normas_criterio : ''}}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div><br>
    <!--Seccion de los criterio-->
    <!--Seccion del exportador-->
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <h4><b>(8) Fecha </b></h4>
                {{$certificados->fecha_emision_exportador ? $certificados->fecha_emision_exportador : ''}}
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <h4><b>(9) Razón social del exportador o productor</b></h4>
                {{$certificados->GenUsuario ? $certificados->GenUsuario->DetUsuario->razon_social : ''}}
            </div>
        </div>
        <div class="row"><!--Campo observaciones-->
            <div class="col-md-12 text-rigth">
                <h4><b>(10) OBSERVACIONES</b></h4>
                {{$certificados->observaciones ? $certificados->observaciones : ''}}
            </div>
        </div> 
    </div><br><br>
<!--Vista de Certificado de Origen Aladi -->
<!--Validacion Mercosur-->
@elseif($tipocertificado == 5)

<div class="row">
     <h4 class="text-center"><b>CERTIFICADO DE ORIGEN ACUERDO MERCOSUR-COLOMBIA, ECUADOR Y VENEZUELA</b></h4><br><br>
    <table class="table table-bordered text-left">
        <thead>
            <th class="col-md-6 text-left">1. País Exportador:</th>
            <th class=" col-md-16 text-left">2. País Importador:</th>
        </thead>
        <tbody>
            <tr class="text-left">
                <td>{{$certificados->pais_exportador ? $certificados->pais_exportador : ''}}</td>
                <td>{{$certificados->pais_importador ? $certificados->pais_importador : ''}}</td>
            </tr>
        </tbody>
    </table>
</div>

<div class="row">
    <table class="table table-bordered">
        <thead>
            <th class="col-md-1 text-center">3. No. de Orden:</th>
            <th class=" col-md-3 text-center">4. NALADISA</th>
            <th class="col-md-4 text-center">5. DENOMINACION DE LA MERCANCIA</th>
            <th class="col-md-2 text-center">6. Peso o Cantidad</th>
            <th class="col-md-2 text-center">7. Valor FOB<br>en US$</th>
        </thead>
        <tbody>
            @foreach ($detalleProduct as $key => $detalleP)
            <tr class="text-center">
                <td>{{$detalleP->num_orden ? $detalleP->num_orden : ''}}</td>
                <td>{{$detalleP->codigo_arancel ? $detalleP->codigo_arancel : ''}}</td>
                <td>{{$detalleP->denominacion_mercancia ? $detalleP->denominacion_mercancia : ''}}</td>
                <td>{{$detalleP->unidad_fisica ? $detalleP->unidad_fisica : ''}}</td>
                <td>{{$detalleP->valor_fob ? $detalleP->valor_fob : ''}}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>

<div class="row">
    <table class="table table-bordered text-left" id="productos">
        <center><h4><b>8. DECLARACION DE ORIGEN</b></h4></center>
        <thead>
            <th class="col-md-6 text-center">Factura Comercial No:</th>
            <th class="col-md-6 text-center">Fecha:
            </th>
        </thead>
        <tbody>
            <tr class="text-center">
                <td>{{$detdeclaracionCert[0]->numero_factura ? $detdeclaracionCert[0]->numero_factura : ''}}</td>
                <td>{{$detdeclaracionCert[0]->f_fac_certificado ? $detdeclaracionCert[0]->f_fac_certificado : ''}}</td>
            </tr>
        </tbody>
    </table><br>
    <table class="table table-bordered text-left" id="productos">
        <thead>
            <th class="col-md-2 text-center">9. Nº. de Orden :</th>
            <th class="col-md-10 text-center">10. NORMAS
            </th>

        </thead>
        <tbody>
            @foreach ($detdeclaracionCert as $key => $detalleCert)
            <tr class="text-center">
                <td>{{$detalleCert->num_orden_declaracion ? $detalleCert->num_orden_declaracion : ''}}</td>
                <td>{{$detalleCert->normas_criterio ? $detalleCert->normas_criterio : ''}}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>

<div class="row">
    <div class="col-md-6">
        <h4><b>11. EXPORTADOR O PRODUCTOR:</b></h4>
        <div class="form-group">
            <h4><b>11.1 Razón Social:</b></h4>
            {{$certificados->GenUsuario ? $certificados->GenUsuario->DetUsuario->razon_social : ''}}
        </div>
        <div class="form-group">
            <h4><b>11.2 Dirección:</b></h4>
            {{$certificados->GenUsuario ? $certificados->GenUsuario->DetUsuario->direccion : ''}}
        </div>
        <div class="form-group">
            <h4><b>11.3 Fecha:</b></h4>
            {{$certificados->fecha_emision_exportador ? $certificados->fecha_emision_exportador : ''}}
        </div>

    </div>
    <div class="col-md-6">
        <h4><b>13. IMPORTADOR</b></h4>
        <div class="form-group">
            <h4><b>13.1 Razón Social:</b></h4>
            {{$importadorCertificado->razon_social_importador ? $importadorCertificado->razon_social_importador : ''}}
        </div>
        <div class="form-group">
            <h4><b>13.2 Dirección:</b></h4>
            {{$importadorCertificado->direccion_importador ? $importadorCertificado->direccion_importador : ''}} 
        </div>
        <div class="form-group">
            <h4><b>14. Medio de Transporte:</b></h4>
            {{$importadorCertificado->medio_transporte_importador ? $importadorCertificado->medio_transporte_importador : ''}}
        </div>
        <div class="form-group">
            <h4><b>15. Puerto o Lugar de Embarque:</b></h4>
            {{$importadorCertificado->lugar_embarque_importador ? $importadorCertificado->lugar_embarque_importador : ''}}
        </div>
    </div>  
</div>

<div class="row">
    <table class="table table-bordered text-left">
        <thead>
            <th class="col-md-12 text-left">16. OBSERVACIONES:</th>
        </thead>
        <tbody>
            <tr class="text-left">
                <td>{{$certificados->observaciones ? $certificados->observaciones : ''}}</td>
            </tr>
        </tbody>
    </table>
</div>


<!--Validacion de Peru-->

@elseif($tipocertificado == 6)
<div class="row">
     <h4 class="text-center"><b>CERTIFICADO DE ORIGEN ACUERDO DE ALCANCE PARCIAL DE NATURALEZA COMERCIAL ENTRE LA REPÚBLICA BOLIVARIANA DE<br> VENEZUELA Y LA REPÚBLICA DEL PERÚ</b></h4><br><br>
    <table class="table table-bordered text-left">
        <thead>
            <th class="col-md-6 text-left">(1) PAÍS EXPORTADOR:</th>
            <th class=" col-md-16 text-left">(2) PAÍS IMPORTADOR:</th>
        </thead>
        <tbody>
            <tr class="text-left">
                <td>{{$certificados->pais_exportador ? $certificados->pais_exportador : ''}}</td>
                <td>{{$certificados->pais_importador ? $certificados->pais_importador : ''}}</td>
            </tr>
        </tbody>
    </table>
</div>

<div class="row">
    <table class="table table-bordered">
        <thead>
            <th class="col-md-1 text-center">(3) No. de Orden</th>
            <th class=" col-md-3 text-center">(4) Código Arancelario (8 dígitos)</th>
            <th class="col-md-4 text-center">(5) Descripción de las Mercancías</th>
            <th class="col-md-2 text-center">(6)Unidad Fisica según Factura</th>
            <th class="col-md-2 text-center">(7) Valor de <br>cada Mercancía según Factura <br>en US$</th>
        </thead>
        <tbody>
            @foreach ($detalleProduct as $key => $detalleP)
            <tr class="text-center">
                <td>{{$detalleP->num_orden ? $detalleP->num_orden : ''}}</td>
                <td>{{$detalleP->codigo_arancel ? $detalleP->codigo_arancel : ''}}</td>
                <td>{{$detalleP->denominacion_mercancia ? $detalleP->denominacion_mercancia : ''}}</td>
                <td>{{$detalleP->unidad_fisica ? $detalleP->unidad_fisica : ''}}</td>
                <td>{{$detalleP->valor_fob ? $detalleP->valor_fob : ''}}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>

<div class="row">
    <table class="table table-bordered text-left" id="productos">
        <center><h4><b>(8) DECLARACION DE ORIGEN</b></h4></center>
        <thead>
            <th class="col-md-6 text-center">Factura(s) Comercial(es) No:</th>
            <th class="col-md-6 text-center">Fecha(s)
            </th>
        </thead>
        <tbody>
            <tr class="text-center">
                <td>{{$detdeclaracionCert[0]->numero_factura ? $detdeclaracionCert[0]->numero_factura : ''}}</td>
                <td>{{$detdeclaracionCert[0]->f_fac_certificado ? $detdeclaracionCert[0]->f_fac_certificado : ''}}</td>
            </tr>
        </tbody>
    </table><br>
    <table class="table table-bordered text-left" id="productos">
        <thead>
            <th class="col-md-2 text-center">(9) No. de Orden</th>
            <th class="col-md-10 text-center">(10) Criterios para la Calificación de Origen</th>
        </thead>
        <tbody>
            @foreach ($detdeclaracionCert as $key => $detalleCert)
            <tr class="text-center">
                <td>{{$detalleCert->num_orden_declaracion ? $detalleCert->num_orden_declaracion : ''}}</td>
                <td>{{$detalleCert->normas_criterio ? $detalleCert->normas_criterio : ''}}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>

<div class="row">
    <div class="col-md-6">
        <h4><b>(11) PRODUCTOR O EXPORTADOR</b></h4>
        <div class="form-group">
            <h4><b>11.1 Nombre o Razón Social:</b></h4>
            {{$certificados->GenUsuario ? $certificados->GenUsuario->DetUsuario->razon_social : ''}}
        </div>
         <div class="form-group">
            <h4><b>11.2 Número de Identificación Fiscal:</b></h4>
           {{ $certificados->GenUsuario ? $certificados->GenUsuario->DetUsuario->rif : '' }}
        </div>
        <div class="form-group">
            <h4><b>11.3 Dirección:</b></h4>
            {{$certificados->GenUsuario ? $certificados->GenUsuario->DetUsuario->direccion : ''}}
        </div>
         <div class="form-group">
            <h4><b>11.4 E-mail:</b></h4>
            {{ $certificados->GenUsuario ? $certificados->GenUsuario->DetUsuario->correo : '' }}
        </div> 
        <div class="form-group">
            <h4><b>11.5 Teléfono:</b></h4>
            {{$certificados->GenUsuario ? $certificados->GenUsuario->DetUsuario->telefono_movil : ''}}
        </div>

    </div>
    <div class="col-md-6">
        <h4><b>(13) IMPORTADOR</b></h4>
        <div class="form-group">
            <h4><b>13.1 Nombre o Razón Social:</b></h4>
            {{$importadorCertificado->razon_social_importador ? $importadorCertificado->razon_social_importador : ''}}
        </div>
        <div class="form-group">
            <h4><b>13.2 Dirección:</b></h4>
            {{$importadorCertificado->direccion_importador ? $importadorCertificado->direccion_importador : ''}} 
        </div>
         <div class="form-group">
            <h4><b>13.3 Teléfono:</b></h4>
            {{ $importadorCertificado->telefono ? $importadorCertificado->telefono : ''}} 
        </div>
         <div class="form-group">
            <h4><b>13.4 E-mail:</b></h4>
            {{ $importadorCertificado->email_importador ? $importadorCertificado->email_importador : ''}} 
        </div>
        <div class="form-group">
            <h4><b>(14) Medio de Transporte:</b></h4>
            {{$importadorCertificado->medio_transporte_importador ? $importadorCertificado->medio_transporte_importador : ''}}
        </div>
        <div class="form-group">
            <h4><b>(15) Puerto o Lugar de Embarque:</b></h4>
            {{$importadorCertificado->lugar_embarque_importador ? $importadorCertificado->lugar_embarque_importador : ''}}
        </div>
    </div>  
</div>
<div class="row">
    <table class="table table-bordered text-left">
        <thead>
            <th class="col-md-12 text-left">(16) OBSERVACIONES:</th>
        </thead>
        <tbody>
            <tr class="text-left">
                <td>{{$certificados->observaciones ? $certificados->observaciones : ''}}</td>
            </tr>
        </tbody>
    </table>
</div>

<!--vALIDACION PARA CERTIFICADO DE ORIGEN CUBA-->
  @elseif($tipocertificado == 7)
        <div class="row">
             <h4 class="text-center"><b>CERTIFICADO DE ORIGEN <br> ACUERDO DE COMPLEMENTACIÓN ECONOMICA ENTRE LA REPUBLICA DE CUBA Y LA REPUBLICA BOLIVARIANA DE VENEZUELA</b></h4><br><br>
            <table class="table table-bordered text-left">
                <thead>
                    <th class="col-md-6 text-left">(1) PAÍS EXPORTADOR:</th>
                    <th class=" col-md-16 text-left">(2) PAÍS IMPORTADOR:</th>
                </thead>
                <tbody>
                    <tr class="text-left">
                        <td>{{$certificados->pais_exportador ? $certificados->pais_exportador : ''}}</td>
                        <td>{{$certificados->pais_importador ? $certificados->pais_importador : ''}}</td>
                    </tr>
                </tbody>
            </table>
        </div>

        <div class="row">
            <table class="table table-bordered">
                <thead>
                    <th class="col-md-1 text-center">(3) No. de Orden</th>
                    <th class=" col-md-3 text-center">(4) Código Arancelario</th>
                    <th class="col-md-4 text-center">(5) Descripción de las Mercancías</th>
                    <th class="col-md-2 text-center">(6)Unidad Fisica según Factura</th>
                    <th class="col-md-2 text-center">(7) Valor de <br>cada Mercancía según Factura <br>en US$</th>
                </thead>
                <tbody>
                    @foreach ($detalleProduct as $key => $detalleP)
                    <tr class="text-center">
                        <td>{{$detalleP->num_orden ? $detalleP->num_orden : ''}}</td>
                        <td>{{$detalleP->codigo_arancel ? $detalleP->codigo_arancel : ''}}</td>
                        <td>{{$detalleP->denominacion_mercancia ? $detalleP->denominacion_mercancia : ''}}</td>
                        <td>{{$detalleP->unidad_fisica ? $detalleP->unidad_fisica : ''}}</td>
                        <td>{{$detalleP->valor_fob ? $detalleP->valor_fob : ''}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

        <div class="row">
            <table class="table table-bordered text-left" id="productos">
                <center><h4><b>(8) DECLARACION DE ORIGEN</b></h4></center>
                <thead>
                    <th class="col-md-6 text-center">Factura(s) Comercial(es) No:</th>
                    <th class="col-md-6 text-center">Fecha(s)
                    </th>
                </thead>
                <tbody>
                    <tr class="text-center">
                        <td>{{$detdeclaracionCert[0]->numero_factura ? $detdeclaracionCert[0]->numero_factura : ''}}</td>
                        <td>{{$detdeclaracionCert[0]->f_fac_certificado ? $detdeclaracionCert[0]->f_fac_certificado : ''}}</td>
                    </tr>
                </tbody>
            </table><br>
            <table class="table table-bordered text-left" id="productos">
                <thead>
                    <th class="col-md-2 text-center">(9) No. de Orden</th>
                    <th class="col-md-10 text-center">(10) Criterios para la Calificación de Origen</th>
                </thead>
                <tbody>
                    @foreach ($detdeclaracionCert as $key => $detalleCert)
                    <tr class="text-center">
                        <td>{{$detalleCert->num_orden_declaracion ? $detalleCert->num_orden_declaracion : ''}}</td>
                        <td>{{$detalleCert->normas_criterio ? $detalleCert->normas_criterio : ''}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

        <div class="row">
            <div class="col-md-6">
                <h4><b>(11) PRODUCTOR O EXPORTADOR</b></h4>
                <div class="form-group">
                    <h4><b>11.1 Nombre o Razón Social:</b></h4>
                    {{$certificados->GenUsuario ? $certificados->GenUsuario->DetUsuario->razon_social : ''}}
                </div>
                 <div class="form-group">
                    <h4><b>11.2 Número de Identificación Tributario/Registro de Información Fiscal:</b></h4>
                   {{ $certificados->GenUsuario ? $certificados->GenUsuario->DetUsuario->rif : ''}}
                </div>
                <div class="form-group">
                    <h4><b>11.3 Dirección o Domicilio:</b></h4>
                    {{$certificados->GenUsuario ? $certificados->GenUsuario->DetUsuario->direccion : ''}}
                </div>
                 <div class="form-group">
                    <h4><b>11.4 E-mail:</b></h4>
                    {{ $certificados->GenUsuario ? $certificados->GenUsuario->DetUsuario->correo : '' }}
                </div> 
                <div class="form-group">
                    <h4><b>11.5 Teléfono:</b></h4>
                    {{$certificados->GenUsuario ? $certificados->GenUsuario->DetUsuario->telefono_movil : ''}}
                </div>

            </div>
            <div class="col-md-6">
                <h4><b>(13) IMPORTADOR</b></h4>
                <div class="form-group">
                    <h4><b>13.1 Nombre o Razón Social:</b></h4>
                    {{$importadorCertificado->razon_social_importador ? $importadorCertificado->razon_social_importador : ''}}
                </div>
                <div class="form-group">
                    <h4><b>13.2 Dirección O Domicilio:</b></h4>
                    {{$importadorCertificado->direccion_importador ? $importadorCertificado->direccion_importador : ''}} 
                </div>
                 <div class="form-group">
                    <h4><b>13.3 Teléfono:</b></h4>
                    {{ $importadorCertificado->telefono ? $importadorCertificado->telefono : ''}} 
                </div>
                 <div class="form-group">
                    <h4><b>13.4 E-mail:</b></h4>
                    {{ $importadorCertificado->email_importador ? $importadorCertificado->email_importador : ''}} 
                </div>
                <div class="form-group">
                    <h4><b>(14) Medio de Transporte:</b></h4>
                    {{$importadorCertificado->medio_transporte_importador ? $importadorCertificado->medio_transporte_importador : ''}}
                </div>
                <div class="form-group">
                    <h4><b>(15) Puerto o Lugar de Embarque:</b></h4>
                    {{$importadorCertificado->lugar_embarque_importador ? $importadorCertificado->lugar_embarque_importador : ''}}
                </div>
            </div>  
        </div>
        <div class="row">
            <table class="table table-bordered text-left">
                <thead>
                    <th class="col-md-12 text-left">(16) OBSERVACIONES:</th>
                </thead>
                <tbody>
                    <tr class="text-left">
                        <td>{{$certificados->observaciones ? $certificados->observaciones : ''}}</td>
                    </tr>
                </tbody>
            </table>
        </div>
<!--Validacion de certificado de origen turquia-->
            @elseif($tipocertificado == 8)
                 <div class="row">
                   <h4 class="text-center"><b>MOVEMENT CERTIFICATE</b></h4><br><br>
                  <div class="col-md-6">
                    <div class="form-group">
                     <h4><b>1.Exporter</b> (Name,  full address, country )</h4>
                     {{$certificados->GenUsuario->DetUsuario->razon_social}} {{$certificados->GenUsuario->DetUsuario->direccion}} {{$certificados->pais_cuidad_exportador ? $certificados->pais_cuidad_exportador : ''}}  {{$certificados->GenUsuario->DetUsuario->rif}}
                    </div>
                  </div>
                   <div class="col-md-6">
                    <div class="form-group">
                     <h4><b>2. Certificate used in preferential trade between</b></h4>
                     <p class="text-left">{{$certificados->pais_exportador ? $certificados->pais_exportador : ''}}</p>
                    </div><br>
                    <div class="form-group">
                      <h4 class="text-center">and</h4><br>
                      <p>{{$certificados->pais_importador ? $certificados->pais_importador : ''}}</p>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <table class="table table-bordered">
                    <thead>
                      <th class="col-md-6"><h4><b>3. Consignee</b> (Name, full address, country) (Opcional)</h4></th>
                      <th class="col-md-3"><h4><b>4.Country, group of countries or territory in which the products are considered as origininathing</b></h4></th>
                      <th class="col-md-3"><h4><b>5.Country, group of countries or territory of destination </b></h4><br></th>
                    </thead>
                    <tbody>
                      <tr>
                        <td>{{$importadorCertificado->razon_social_importador ? $importadorCertificado->razon_social_importador : ''}} {{$importadorCertificado->direccion_importador ? $importadorCertificado->direccion_importador : ''}}</td><br>
                        <td>{{$certificados->pais_cuidad_exportador ? $certificados->pais_cuidad_exportador : ''}}</td><br>
                        <td>{{$importadorCertificado->pais_ciudad_importador ? $importadorCertificado->pais_ciudad_importador : ''}}</td><br>
                      </tr>
                    </tbody>
                  </table>
                </div>
                <!--puntos 6y 7-->
                <div class="row">
                  <table class="table table-bordered">
                    <thead>
                      <th class="col-md-6"><h4><b>6. Transport details </b>(Opcional)</h4></th>
                      <th class="col-md-6"><h4><b>7. Remarks</b></h4></th>
                    </thead>
                    <tbody>
                      <tr>
                        <td>{{$importadorCertificado->medio_transporte_importador ? $importadorCertificado->medio_transporte_importador : ''}} <br><br></td>
                        <td>{{$certificados->observaciones ? $certificados->observaciones : ''}} <br><br></td>
                      </tr>
                    </tbody>
                  </table>
                </div><br>
                <div class="row">
                    <table class="table table-bordered">
                      <thead>
                        <th class="col-md-5"><h4><b>8. Item number; Marks and numbers; Number and kind of packages; Description of goods(1)</b></h4></th>
                        <th class=" col-md-4"><h4><b>9.Gross mass (Kg) or other measure (litres, m3, etc.)</b></h4></th>
                       <th class="col-md-3"><h4><b>10.Invoices(2) </b>
                       </h4><br><br><br>
                       <div>{{$detdeclaracionCert[0]->numero_factura ? $detdeclaracionCert[0]->numero_factura : ''}} {{$detdeclaracionCert[0]->f_fac_certificado ? $detdeclaracionCert[0]->f_fac_certificado : ''}} <br></div>
                     </th>
                      </thead>
                      <tbody>
                        @foreach ($detalleProduct as $key => $detalleP)
                        <tr>
                          <td class="">{{$detalleP->codigo_arancel ? $detalleP->codigo_arancel : ''}} {{$detalleP->denominacion_mercancia ? $detalleP->denominacion_mercancia : ''}} <br><br></td><br>
                          <td class="col-md-4">{{$detalleP->unidad_fisica ? $detalleP->unidad_fisica : ''}} <br><br></td>
                          
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                  </div>
      <!--Validacion de certificado UE---->
    @else($tipocertificado == 9)
              <div class="row">
                <h4 class="text-center"><b>GENERALIZED SYSTEMA OF PREFERENCES CERTIFICATE OF ORIGIN <br>(Combined declaration and certificate FORM A)</b></h4><br><br>
                <table class="table table-bordered text-left">
                    <thead>
                      <th class="col-md-4">Goods consigned from (exporter's business name,address,Country)</th>
                      <th class=" col-md-4 text-justify">Goods consigned to(consignee's name,address,country)</th>
                      <th class="col-md-4 text-justify">Means of transport route (as far as known)</th>
                    </thead>
                    <tbody>
                      <tr class="text-left">
                        <td>{{$certificados->GenUsuario->DetUsuario->razon_social}} {{$certificados->GenUsuario->DetUsuario->direccion}} {{$certificados->pais_exportador ? $certificados->pais_exportador : ''}}</td>

                        <td>{{$importadorCertificado->razon_social_importador ? $importadorCertificado->razon_social_importador : ''}}  {{$importadorCertificado->direccion_importador ? $importadorCertificado->direccion_importador : ''}} {{$importadorCertificado->pais_ciudad_importador ? $importadorCertificado->pais_ciudad_importador : ''}}</td>
                        <td>{{$certificados->direccion_productor ? $certificados->direccion_productor : ''}}   {{$importadorCertificado->lugar_embarque_importador ? $importadorCertificado->lugar_embarque_importador : ''}}</td>
                      </tr>
                    </tbody>
                </table>
                <div>
                    <table class="table table-bordered">
                      <thead>
                        <th class="col-md-1 text-center">Item Num-<br>ber</th>
                        <th class=" col-md-1 text-center">Marks and number of packages</th>
                        <th class="col-md-4 text-center">Numbers and kind of packages; description of goods</th>
                        <th class="col-md-2 text-center">Gross weight or ther quantity</th>
                        <th class="col-md-2 text-center">Code tariff</th>
                        <th class="col-md-2 text-center">Name of the Goods</th>
                      </thead>
                      <tbody>

                        @foreach ($detalleProduct as $key => $detalleP)
                          <tr>
                              <td>{{$detalleP->num_orden ? $detalleP->num_orden : ''}}</td>
                              <td>{{$detalleP->cantidad_segun_factura ? $detalleP->cantidad_segun_factura : ''}}</td>
                              <td class="text-center">{{$detalleP->descripcion_comercial ? $detalleP->descripcion_comercial : ''}}</td>
                              <td>{{$detalleP->unidad_fisica ? $detalleP->unidad_fisica : ''}}</td>
                              <td>{{$detalleP->codigo_arancel ? $detalleP->codigo_arancel: '' }}</td>
                              <td>{{$detalleP->denominacion_mercancia ? $detalleP->denominacion_mercancia : ''}}</td>
                             @endforeach
                          </tr>
                      </table>
                  </div>
                   <table class="table table-bordered text-left" id="productos">
                        <thead>
                            <th class="col-md-6">Item Number</th>
                            <th class="col-md-6">Origin criterion (see notes overleaf)
                            </th>
                        </thead>
                        <tbody>
                            @foreach ($detdeclaracionCert as $key => $detalleCert)
                            <tr class="text-justify">
                                <td>{{$detalleCert->num_orden_declaracion}}</td>
                                <td>{{$detalleCert->normas_criterio}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                  <table class="table table-bordered text-left" id="productos">
                        <thead>
                            <th class="col-md-6">Number invoices</th>
                            <th class="col-md-6">Date invoices
                            </th>
                        </thead>
                        <tbody>
                            <tr class="text-justify">
                                <td>{{$detdeclaracionCert[0]->numero_factura}}</td>
                                <td>{{$detdeclaracionCert[0]->f_fac_certificado}}</td>
                            </tr>
                        </tbody>
                    </table>
                   <div>
                    <table class="table table-bordered text-left">
                        <thead>
                            <th class="col-md-12 text-left">Remarks</th>
                        </thead>
                        <tbody>
                            <tr class="text-left">
                                <td>{{$certificados->observaciones ? $certificados->observaciones : ''}}</td>
                            </tr>
                        </tbody>
                    </table>
               </div><br><br>
              @endif
<!--Secci`on para el input de observaciones -->
<!--botones de estatus-->
                    <div class="row">
                        <br>
                            <br>
                        <div class="row text-center"><br><br>
                           <a class="btn btn-primary" href="{{route('CoordinadorCertificadoOrigen.index')}}">Regresar</a>
                       </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="{{asset('js/ajax_v2.1.1.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.2/moment.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.2/locale/es.js"></script>

    <script>
      $(document).ready(function() {
      $('#mostrar_info').click(function(event) {
              $('#mostrar_info').hide(1000).animate({height: "hide"}, 1000,"linear");
              $('#det_info, #ocultar_info').show(1000).animate({width: "show"}, 1000,"linear");  

      });
      $('#ocultar_info').click(function(event) {
              $('#det_info,#ocultar_info').hide(1000).animate({height: "hide"}, 1000,"linear");
              $('#mostrar_info').show(1000).animate({width: "show"}, 1000,"linear");
      });
      
    }); 
      $('.date-exported').each(function(i, span) {
                span = $(span);
                var dateTime = span.text();
                moment.updateLocale('es', {
                    months : ['enero', 'febrero', 'marzo', 'abril', 'mayo', 'junio', 'julio', 'agosto', 'septiembre', 'octubre', 'noviembre', 'diciembre']
                });
                dateTime = moment.utc(dateTime).toDate();
                var fecha = moment(dateTime).local('es').format('LLL').substring(0, moment(dateTime).local('es').format('LLL').length - 5);
                span.text(fecha);
            });
    </script>

    

@stop

 




