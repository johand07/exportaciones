@extends('templates/layoutlte')

@section('content')



<div class="jumbotron">
<div class="row w-100">
        <div class="col-md-4">
          @if(isset($planillaDjir01) && (isset($planillaDjir01) && $planillaDjir01->gen_status_id == 16) && $planillaDjir01->actualizado == 0)
        
            <a href="{{url('exportador/actualizarPlanilla1Inversion',array('gen_declaracion_inversion_id'=>$gen_declaracion_inversion_id,'declaracion_id'=>$declaracion_id))}}" >  
              <div class="card border-info mx-sm-1 p-3">
                  <div class="card border-info shadow text-info p-3 my-card" ></div>
                  <div class="text-success text-center mt-3"><h3><b>PLANILLA (DJIR 01)</b></h3></div>
                  <div class="text-success text-center mt-2"><h4>PLANILLA DE DECLARACIÓN JURADA DE INVERSIÓN REALIZADA (01)<br>INFORMACIÓN GENERAL</h4></div>
              </div>
            </a>
          @elseif(!isset($planillaDjir01))
            <a href="{{url('exportador/Planilla1Inversion/')}}/{{$gen_declaracion_inversion_id}}">
              <div class="card border-info mx-sm-1 p-3">
                  <div class="card border-info shadow text-info p-3 my-card" ></div>
                  <div class="text-info text-center mt-3"><h3><b>PLANILLA (DJIR 01)</b></h3></div>
                  <div class="text-info text-center mt-2"><h4>PLANILLA DE DECLARACIÓN JURADA DE INVERSIÓN REALIZADA (01)<br>INFORMACIÓN GENERAL</h4></div>
              </div>
            </a>
          @elseif(isset($planillaDjir01) && ($planillaDjir01->gen_status_id == 11 || $planillaDjir01->gen_status_id == 15))
            <a href="{{route('savePlanilla1Inversion.edit',$gen_declaracion_inversion_id)}}">
              <div class="card border-info mx-sm-1 p-3">
                  <div class="card border-info shadow text-info p-3 my-card" ></div>
                  <div class="text-danger text-center mt-3"><h3><b>PLANILLA (DJIR 01)</b></h3></div>
                  <div class="text-danger text-center mt-2"><h4>PLANILLA DE DECLARACIÓN JURADA DE INVERSIÓN REALIZADA (01)<br>INFORMACIÓN GENERAL</h4></div>
              </div>
            </a>
          @endif
        </div>
        <div class="col-md-4">
        @if(isset($planillaDjir02) && (isset($planillaDjir02) && $planillaDjir02->gen_status_id == 16) && $planillaDjir02->actualizado == 0) 
        <a href="{{url('exportador/actualizarPlanilla2Inversion',array('gen_declaracion_inversion_id'=>$gen_declaracion_inversion_id,'declaracion_id'=>$declaracion_id))}}" >
          
            <div class="card border-info mx-sm-1 p-3">
                <div class="card border-info shadow text-info p-3 my-card" ></div>
                <div class="text-success text-center mt-3"><h3><b>PLANILLA (DJIR 02)</h3></b></div>
                <div class="text-success text-center mt-2"><h4>PLANILLA DE DECLARACIÓN JURADA DE INVERSIÓN REALIZADA (02) <br>INFORMACIÓN FINANCIERA</h4></div>
            </div>
          </a>
        @elseif(!isset($planillaDjir01) && !isset($planillaDjir02)&& !isset($planillaDjir03)) 
      
            <div class="card border-info mx-sm-1 p-3" id="empty_planillaDjir02">
                <div class="card border-info shadow text-info p-3 my-card" ></div>
                <div class="text-info text-center mt-3"><h3><b>PLANILLA (DJIR 02)</h3></b></div>
                <div class="text-info text-center mt-2"><h4>PLANILLA DE DECLARACIÓN JURADA DE INVERSIÓN REALIZADA (02) <br>INFORMACIÓN FINANCIERA</h4></div>
            </div>
        @elseif(isset($planillaDjir02) && (isset($planillaDjir02) && $planillaDjir02->gen_status_id == 11) || (isset($planillaDjir02) && $planillaDjir02->gen_status_id == 15))
          <a href="{{route('savePlanilla2Inversion.edit',$gen_declaracion_inversion_id)}}">
            <div class="card border-info mx-sm-1 p-3">
                <div class="card border-info shadow text-info p-3 my-card" ></div>
                <div class="text-danger text-center mt-3"><h3><b>PLANILLA (DJIR 02)</h3></b></div>
                <div class="text-danger text-center mt-2"><h4>PLANILLA DE DECLARACIÓN JURADA DE INVERSIÓN REALIZADA (02) <br>INFORMACIÓN FINANCIERA</h4></div>
            </div>
          </a>
        @elseif(isset($planillaDjir01) && !isset($planillaDjir02)&& !isset($planillaDjir03))
            <a href="{{url('exportador/Planilla2Inversion/')}}/{{$gen_declaracion_inversion_id}}">
              <div class="card border-info mx-sm-1 p-3">
                  <div class="card border-info shadow text-info p-3 my-card" ></div>
                  <div class="text-info text-center mt-3"><h3><b>PLANILLA (DJIR 02)</h3></b></div>
                  <div class="text-info text-center mt-2"><h4>PLANILLA DE DECLARACIÓN JURADA DE INVERSIÓN REALIZADA (02) <br>INFORMACIÓN FINANCIERA</h4></div>
              </div>
            </a>
        @endif
          
        </div>
        <div class="col-md-4">
          @if(isset($planillaDjir03) && (isset($planillaDjir03) && $planillaDjir03->gen_status_id == 16) && $planillaDjir03->actualizado == 0)
          <a href="{{url('exportador/actualizarPlanilla3Inversion',array('gen_declaracion_inversion_id'=>$gen_declaracion_inversion_id,'declaracion_id'=>$declaracion_id))}}" >
              <div class="card border-info mx-sm-1 p-3">
                  <div class="card border-info shadow text-info p-3 my-card" ></div>
                  <div class="text-success text-center mt-3"><h3><b>PLANILLA (DJIR 03)</b></h3></div>
                  <div class="text-success text-center mt-2"><h4>PLANILLA DE DECLARACIÓN JURADA DE INVERSIÓN REALIZADA (03) <br>INFORMACIÓN COMPLEMENTARIA</h4></div>
              </div>
            </a>
          @elseif(!isset($planillaDjir01) && !isset($planillaDjir02)&& !isset($planillaDjir03))
            
              <div class="card border-info mx-sm-1 p-3" id="empty_planillaDjir03">
                  <div class="card border-info shadow text-info p-3 my-card" ></div>
                  <div class="text-info text-center mt-3"><h3><b>PLANILLA (DJIR 03)</b></h3></div>
                  <div class="text-info text-center mt-2"><h4>PLANILLA DE DECLARACIÓN JURADA DE INVERSIÓN REALIZADA (03) <br>INFORMACIÓN COMPLEMENTARIA</h4></div>
              </div>
          @elseif(isset($planillaDjir01) && !isset($planillaDjir02)&& !isset($planillaDjir03))
            
              <div class="card border-info mx-sm-1 p-3" id="empty_planillaDjir03">
                  <div class="card border-info shadow text-info p-3 my-card" ></div>
                  <div class="text-info text-center mt-3"><h3><b>PLANILLA (DJIR 03)</b></h3></div>
                  <div class="text-info text-center mt-2"><h4>PLANILLA DE DECLARACIÓN JURADA DE INVERSIÓN REALIZADA (03) <br>INFORMACIÓN COMPLEMENTARIA</h4></div>
              </div>
          @elseif(isset($planillaDjir03) && (isset($planillaDjir03) && $planillaDjir03->gen_status_id == 11) || (isset($planillaDjir03) && $planillaDjir03->gen_status_id == 15))
            <a href="{{route('savePlanilla3Inversion.edit',$gen_declaracion_inversion_id)}}">
              <div class="card border-info mx-sm-1 p-3">
                  <div class="card border-info shadow text-info p-3 my-card" ></div>
                  <div class="text-danger text-center mt-3"><h3><b>PLANILLA (DJIR 03)</b></h3></div>
                  <div class="text-danger text-center mt-2"><h4>PLANILLA DE DECLARACIÓN JURADA DE INVERSIÓN REALIZADA (03) <br>INFORMACIÓN COMPLEMENTARIA</h4></div>
              </div>
            </a>
          @elseif(isset($planillaDjir01) && isset($planillaDjir02)&& !isset($planillaDjir03))
              <a href="{{url('exportador/Planilla3Inversion/')}}/{{$gen_declaracion_inversion_id}}">
                <div class="card border-info mx-sm-1 p-3">
                    <div class="card border-info shadow text-info p-3 my-card" ></div>
                    <div class="text-info text-center mt-3"><h3><b>PLANILLA (DJIR 03)</b></h3></div>
                    <div class="text-info text-center mt-2"><h4>PLANILLA DE DECLARACIÓN JURADA DE INVERSIÓN REALIZADA (03) <br>INFORMACIÓN COMPLEMENTARIA</h4></div>
                </div>
              </a>
          @endif
        </div>
        <br><br><br> <br><br><br> <br><br><br> <br><br><br>
        <div class="row text-center">
          <a href="{{ url('/exportador/BandejaInversion')}}" class="btn btn-primary">Cancelar</a>
           
        </div>
       
     </div>
</div>


@stop
<style type="text/css">

.card {
  box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
  transition: 0.3s;
  /*width: 40%;*/
  border-radius: 5px;
  padding: 2px 16px;
  
}

.card:hover {
  box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);

}


.container {
  padding: 2px 16px;

}

.my-card
{
    position:absolute;
    left:40%;
    top:-20px;
    border-radius:50%;
}

</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
   
  $(document).on('click','#empty_planillaDjir02',function(){


    swal("PLANILLA (DJIR 01) Incompleta", "Debe llenar la Planilla DJIR01 para completar esta planilla ", "warning");


  });

  $(document).on('click','#empty_planillaDjir03',function(){


    swal("PLANILLA (DJIR 02) Incompleta", "Debe llenar la Planilla DJIR02 para completar esta planilla ", "warning");


  });



});
</script>