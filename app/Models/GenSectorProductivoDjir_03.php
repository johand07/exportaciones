<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GenSectorProductivoDjir_03 extends Model
{
    protected $table = 'gen_sector_productivo_djir03';
    
    protected $fillable = [
        'gen_declaracion_inversion_id',
        'cat_sector_productivo_eco_id',
    ];

    public function genDecInversion()
	{
        return $this->hasOne('App\Models\GenDeclaracionInversion','id','gen_declaracion_inversion_id');
    }

    public function sectorProdEco()
	{
        return $this->hasOne('App\Models\CatSectorProductivoEco','id','cat_sector_productivo_eco_id');
    }
}
