<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CatSectorProductivoEco extends Model
{
    protected $table = 'cat_sector_productivo_eco';
    
    protected $fillable = [
        'nombre_sector',
    ];
}
