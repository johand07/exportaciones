<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FacturaSolicitud extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('factura_solicitud', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('gen_factura_id')->index()->nullable();
            $table->foreign('gen_factura_id')->references('id')->on('gen_factura');
            $table->unsignedInteger('gen_solicitud_id')->index()->nullable();
            $table->foreign('gen_solicitud_id')->references('id')->on('gen_solicitud');
            $table->string('numero_nota_credito', 20)->nullable();
            $table->boolean('bactivo')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('factura_solicitud');
    }
}
