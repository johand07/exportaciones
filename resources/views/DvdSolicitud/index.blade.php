@extends('templates/layoutlte')
@section('content')
<div class="panels
<div id="myTabContent" class="tab-content">
    <table id="listaer" class="table table-striped table-bordered" style="width:100%">
      <thead>
          <tr>
              <th>Número de Solicitud</th>
              <th>Tipo de Solicitud</th>
              <th>Estatus Solicitud</th>
              <th>Fecha Solictud</th>
              <th>Monto Solictud</th>
              <th>Acciones</th>
          </tr>
      </thead>
      <tbody>
        @foreach($solicitudes as $solicitud)
          <tr>
              <td>{{$solicitud->id}}</td>
              <td>{{$solicitud->tiposolictud->solicitud}}</td>
              <td>@if(!is_null($solicitud->gen_status_id)){{$solicitud->status->nombre_status}}@endif</td>
              <td>{{$solicitud->fsolicitud}}</td>
              <td>{{$solicitud->monto_solicitud}}</td>
              <td>
                <a href="{{route('ListaVenta.edit',$solicitud->id)}}" class="glyphicon glyphicon-pencil btn btn-success btn-sm" title="Generar Nueva Venta" aria-hidden="true"></a>

              </td>
          </tr>  
         @endforeach
      </tbody>
    </table>
</div>
</div>
@stop