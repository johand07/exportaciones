@extends('layouts.app')

@section('content')
@component('modal_dialogo',['arancel'=>$arancel])

@slot('header') <span>Código Arancelario</span> @endslot

@slot('body')  <div id="arancel"></div>

@endslot

@slot('footer')@endslot

@endcomponent
<div class="container">


 {{Form::open(['method'=>'POST','id'=>'formNatural','enctype' => 'multipart/form-data'])}}


<div class="panel panel-primary">
  <div class="panel-heading">DATOS BÁSICOS</div>
  <div class="panel-body">
    <div class="row">
      <div class="col-md-3">
        <div class="form-group">
          {!!Form::label('','Nombre o Razón Social')!!}
          {!!Form::text('razon_social',(!is_null($datos))?$datos->DNOMBRE:old('razon_social'),['class'=>'form-control','id'=>'razon_social','onkeypress="return soloNumerosyLetras(event)"'])!!}
        </div>
      </div>
      <div class="col-md-3">
        <div class="form-group">
          <label>Siglas</label><br>
          {!!Form::text('siglas',(!is_null($datos))?$datos->DSIGLAS:old('siglas'),['class'=>'form-control','id'=>'siglas','onkeypress="return soloLetrasyPunto(event)"','maxlength'=>'50'])!!}
        </div>
      </div>
      <div class="col-md-3">
        <div class="form-group">
          <label>Rif</label><br>
         @if(!is_null($datos))
        <input type="text" name="rif" value="{{$datos->CRIF}}" readonly id="rif" class="form-control" />
        @else
          <input type="text" name="rif" value="{{$rif}}" readonly id="rif" class="form-control" />
        @endif
        </div>
      </div>
        <div class="col-md-3">
          <div class="form-group">
            <label>Página Web</label><br>
            {!! Form::text('pagina_web',null, ['class'=>'form-control','id'=>'pagina_web','onkeypress'=>'return soloNumerosyLetras(event)','maxlength'=>'60']) !!}
          </div>
        </div> 
      </div>
      <div class="panel panel-primary">
        <div class="panel-heading">TIPO DE ACTIVIDAD ECONÓMICA</div>
      </div>
      <div class="row">
        <div class="col-md-3">

           <label>Exportador</label>
           {!!Form::checkbox('export','export',null,['class'=>'checkbox-inline','id'=>'export'])!!}

        </div>
        <div class="col-md-3">
           <label>Inversionista</label>
           {!!Form::checkbox('invers','Inversionista',null,['class'=>'checkbox-inline','id'=>'invers'])!!}
        </div>
        <div class="col-md-3">
            <label>Productor</label>
            {!!Form::checkbox('produc','Productor',null,['class'=>'checkbox-inline','id'=>'produc'])!!}
        </div>
        <div class="col-md-3">
            <label>Comercializadora</label>
            {!!Form::checkbox('comerc','Comercializadora',null,['class'=>'checkbox-inline','id'=>'comerc'])!!}
        </div>
      </div>
      <br>
    <div class="row">
      <div class="col-md-6">
        <div class="form-group">
          <label>Sector</label><br>
           {!!Form::select('id_gen_sector',$sectores,null,
              ['class'=>'form-control','id'=>'id_gen_sector','placeholder'=>'--Seleccione una Opción--'])!!}
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group">
          <label>Actividad Económica</label><br>
          {!!Form::select('id_gen_actividad_eco',$actividad_eco,null,['class'=>'form-control','id'=>'id_gen_actividad_eco','placeholder'=>'--Seleccione una Opción--'])!!}
        </div>
      </div>
    </div>
      <div class="panel panel-primary">
        <div class="panel-heading">DOMICILIO FISCAL</div>
      </div>
      <div class="row">
        <div class="col-md-4">
          <div class="form-group">
            <label>Estado</label><br>
             {!!Form::select('id_estado',$estados,null,
              ['class'=>'form-control','id'=>'id_estado','placeholder'=>'--Seleccione una Opción--'])!!}
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label>Municipio</label><br>
            {!!Form::select('id_municipio',$municipios,null,['class'=>'form-control','id'=>'id_municipio'])!!}
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label>Parroquia</label><br>

             {!!Form::select('id_parroquia',$parroquias,null,['class'=>'form-control','id'=>'id_parroquia'])!!}
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <label>Dirección</label><br>

            {!!Form::text('direccion',(!is_null($datos))?$datos->DDIRECCION:old('direccion'),['class'=>'form-control','id'=>'direccion','onkeypress="return soloNumerosyLetras(event)"','maxlength'=>'200'])!!}

          </div>
        </div>
        <div class="col-md-3">
          <div class="form-group">
            <label>Teléfono Local</label><br>

            {!!Form::text('telefono_local',(!is_null($datos))?$datos->CTELEFONO:old('telefono_local'),['class'=>'form-control','id'=>'telefono_local','onkeypress="return soloNumeros(event)"','maxlength'=>'11'])!!}
            <small>Ejemplo:02125554555</small>
          </div>
        </div>
        <div class="col-md-3">
          <div class="form-group">
            <label>Teléfono Celular</label><br>
             {!!Form::text('telefono_movil',(!is_null($datos))?$datos->CCELULAR:old('telefono_movil'),['class'=>'form-control','id'=>'telefono_movil','onkeypress="return soloNumeros(event)"','maxlength'=>'11'])!!}
            <small>Ejemplo:04165554555</small>
          </div>
        </div>
      </div>

      <div class="panel panel-primary">
        <div class="panel-heading">DATOS DE LOS PRODUCTOS QUE PRODUCE Y/O COMERCIALIZA</div>
      </div>
      <div class="row">
         <div class="col-md-1"></div>
         <div class="col-md-10 text-center">
            {!! Form::label('','¿Exporta Productos de Servicios y/o Tecnología?',['class'=>'form-control'])!!}

            {!! Form::label('','Si',['class'=>'radio-inline'])!!}
            {!! Form::radio('produc_tecno',1,false,['class'=>'radio-inline','id'=>'prod_tecnologia_si'])!!}
            {!! Form::label('','No',['class'=>'radio-inline'])!!}
            {!! Form::radio('produc_tecno',2,false,['class'=>'radio-inline','id'=>'prod_tecnologia_no'])!!}



          </div>
        <div class="col-md-1"></div>
      </div><br>
      <div class="row" id="tecno_especifique" style="display: none;">
        <div class="col-md-1"></div>
        <div class="col-md-10">
          {!! Form::label('','Especifique')!!}
          {!!Form::text('especifique_tecno',null,['class'=>'form-control','id'=>'especifique_tecno','onkeypress'=>'return soloLetras(event)','maxlength'=>'100'])!!}
        </div>
        <div class="col-md-1"></div>
      </div>

     <div class="row" id="codigo_arancelarios" style="display: none;">
         <div class="col-md-3"></div>
          <div class="col-md-6 text-center">
            {{--{!!Form::radio('tipo_arancel','1',false,['class'=>'radio-inline','id'=>'cod_andina'])!!}
            {!! Form::label('','Nandina',['class'=>'radio-inline'])!!}--}}
            {!! Form::radio('tipo_arancel','2',false,['class'=>'radio-inline','id'=>'cod_mercosur'])!!}
            {!! Form::label('','Mercosur (NCM)',['class'=>'radio-inline'])!!}
          </div>
        <div class="col-md-3"></div>
      </div><br>
      <table class="table table-bordered text-center" id="fields2" style="display: none;">

        <tr>
          <th>Código Arancelario </th>
          <th>Descripción Arancelaria</th>
          <th>Descripción Comercial</th>
          <th>Unidad de Medida</th>
          <th>Capacidad Operativa Anual</th>
          <th>Capacidad Instalada Anual</th>
          <th>Capacidad de Almacenamiento Anual</th>
          <th>Capacidad de Exportación Anual</th>
          <th></th>

        </tr>
        <tr id="fila">

          <td>{!!Form::text('codigo[]',null,['class'=>'form-control','data-toggle'=>"modal",'data-target'=>"#modal",'id'=>'codigo','readonly'=>'true'])!!}
          </td>
          <td>{!!Form::text('descripcion[]',null,['class'=>'form-control','id'=>'descripcion','onkeypress'=>'return soloLetras(event)'])!!}</td>
          <td>{!!Form::text('descrip_comercial[]',null,['class'=>'form-control','id'=>'descrip_comercial','onkeypress'=>'return soloLetras(event)'])!!}</td>
          <td>{!!Form::select('gen_unidad_medida_id[]',$unidad_medida,null,['placeholder'=>'--Seleccione una Unidad de Medida--', 'class'=>'form-control','id'=>'gen_unidad_medida_id']) !!}</td>
          <td>{!!Form::text('cap_opera_anual[]',null,['class'=>'form-control','id'=>'cap_opera_anual','onkeypress'=>'return soloNumeros(event)'])!!}</td>
          <td>{!!Form::text('cap_insta_anual[]',null,['class'=>'form-control','id'=>'cap_insta_anual','onkeypress'=>'return soloNumerosletrasyPorcentaje(event)'])!!}</td>
          <td>{!!Form::text('cap_alamcenamiento[]',null,['class'=>'form-control','id'=>'cap_alamcenamiento','onkeypress'=>'return soloNumeros(event)'])!!}</td>
          <td>{!!Form::text('cap_exportacion[]',null,['class'=>'form-control','id'=>'cap_exportacion','onkeypress'=>'return soloNumeros(event)'])!!}</td>

          <td><button  name="add2" type="button" id="add2" value="add more" class="btn btn-success">Agregar</button></td>
        </tr>

      </table>
      <div class="panel panel-primary">
      <div class="panel-heading">ÚLTIMA EXPORTACIÓN</div>
    </div>
    <div class="row">
      <div class="col-md-6">
        <div class="form-group">
          {!! Form::label('','País')!!}<!--Pais de la ultima exportacion-->
          {!!Form::select('pais_id',$paises,null,['class'=>'form-control','placeholder'=>'Seleccione un País de la Última Exportacion','id'=>'pais_id']) !!}
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group">
          {!! Form::label('','Fecha de Embarque de Última Exportación')!!}
          <div class="input-group date">
            {!! Form::text('fecha_ult_export',null, ['class'=>'form-control','id'=>'fecha_ult_export','onchange'=>'return validarfechaRM(this.value,"fecha_ult_export")','maxlength'=>'','readonly'=>'readonly']) !!}
            <span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
          </div> 
        </div>
      </div>
    </div>
    <br><br>
    <div class="panel panel-primary">
      <div class="panel-heading">SOPORTE</div>
    </div>
    <div class="row">
      <div class="col-md-4">
         <div class="form-group">
         {!! Form::label('','CI')!!}
          {!!Form::file('ruta_doc',null,['class'=>'form-control','id'=>'ruta_doc']) !!}
        </div>
      </div>
      <div class="col-md-4">
         <div class="form-group">
         {!! Form::label('','RIF')!!}
          {!!Form::file('ruta_rif',null,['class'=>'form-control','id'=>'ruta_rif']) !!}
        </div>
      </div>
      <div class="col-md-">
        {!! Form::label('','REGISTRO MERCANTIL')!!}
          {!!Form::file('ruta_reg_merc',null,['class'=>'form-control','id'=>'ruta_reg_merc']) !!}
      </div>
    </div>
    <br><br>
      <div class="panel panel-primary">
        <div class="panel-heading">DATOS DE ACCESO</div>
      </div>

      <div class="row">
        <div class="col-md-3">
          <div class="form-group">
            <label>Correo Electrónico</label><br>
           {!!Form::text('email',(!is_null($datos))?$datos->CMAIL_PRIM:old('email'),['class'=>'form-control','id'=>'email'])!!}
          </div>
        </div>
        <div class="col-md-3">
          <div class="form-group">
            <label>Correo Alternativo</label><br>

             {!!Form::text('email_seg',(!is_null($datos))?$datos->CMAIL_SEC:old('email_seg'),['class'=>'form-control','id'=>'email_seg'])!!}

          </div>
        </div>
        <div class="col-md-3">
          <div class="form-group">
            <label>Contraseña</label><br>
            {!!Form::password('password',['class'=>'form-control','id'=>'password','maxlength'=>'100'])!!}
          </div>
        </div>
        <div class="col-md-3">
          <div class="form-group">
            <label>Confirme Contraseña</label><br>
            {!!Form::password('password_confirm',['class'=>'form-control','id'=>'password_confirm','maxlength'=>'100'])!!}
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6">
          <div class="form-group" id="lista_pregunta">
            <label>Pregunta de Seguridad</label>
              {!!Form::select('id_cat_preguntas_seg',$preguntas_seg,null,
              ['class'=>'form-control','id'=>'id_cat_preguntas_seg','onchange="preguntaSeg ();"','placeholder'=>'--Seleccione una Opción--'])!!}

          </div>
          <div class="form-group" id="preg_alternativa" style="display: none;">
            <label>Pregunta Alternativa</label><br>
            {!!Form::text('pregunta_alter',null,['class'=>'form-control','id'=>'pregunta_alter','maxlength'=>'200','onkeypress="return soloNumerosyLetras(event)"'])!!}
            <span style="color:#3097D1;cursor:pointer;" id="lista_preg">lista de preguntas</span>
          </div>


        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label>Respuesta de Seguridad</label><br>
            {!!Form::text('respuesta_seg',null,['class'=>'form-control','id'=>'respuesta_seg','maxlength'=>'100','onkeypress="return soloNumerosyLetras(event)"'])!!}
          </div>
        </div>
      </div>




<div class="row">
  <div class="col-md-12">
    <div class="col-md-4"></div>
    <div class="col-md-4 text-center">
      <div class="form-group">


        <a class="btn btn-primary" href="#" id="guardar_personal" onclick="enviarNatural()">Completar inscripción</a>




      </div>
    </div>

    <div class="col-md-4"></div>
  </div>
</div>
<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
<div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title">Declaración Jurada del Exportador</h4>
</div>
<div class="modal-body">
<div class="col-md-12">
<p class="text-justify">Usted en su carácter de (Presidente, representante legal, apoderado), de la empresa, declara conocer las condiciones y términos establecidos en el basamento legal aplicable para esta solicitud, así como las consecuencias que de ellas se deslinden; a tales efectos, manifiesta que tanto la información como la documentación suministrada, son ciertas, fidedignas e irrefutables, ya que reflejan con total precisión, los elementos de comprobación necesarios para su validez y eficacia; en este sentido autorizo al Ministerio del Poder Popular de Economía, Finanzas y Comercio Exterior, o a quien este designe, a encauzar todos los medios de confirmación que considere necesarios, sin más limitaciones que las establecidas en la Constitución y las Leyes de la República, sin perjuicio de las acciones civiles, penales, administrativas que como resultado de dicha comprobación tuvieren lugar en razón de una omisión o suministro de información falsa a la Administración Pública.
 
De igual modo declara que, la recepción de toda notificación, mensaje de datos o incidencias relacionadas con esta solicitud, será efectiva desde el momento que sea emitida por este Sistema, de conformidad a lo estipulado en los artículos  9 y 10 el Decreto con Rango, Valor y Fuerza de Ley Nº 1204, de fecha 10 de febrero de 2001, que regula la materia de Mensaje de Datos y Firmas Electrónicas, publicado en la Gaceta Oficial de la República Bolivariana de Venezuela bajo el número 37.148, de fecha 28 de febrero de 2001.
</p>
<div class="row">
  <div class="colo-md-1"></div>
  <div class="col-md-10">

     <input type="checkbox" name="condicion" id="condicion" value="1" class="checkbox-inline"><label>Declaro haber leído y aceptado las condiciones antes expuestas</label>
  </div>
  <div class="colo-md-1"></div>
</div>
</div>

</div>

<div class="modal-footer">
<div class="row">
  <div class="col-md-12">
    <div class="col-md-4"></div>
    <div class="col-md-4 text-center">
      <div class="form-group">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <a class="btn btn-primary" style="cursor: pointer;" onclick="enviarNatural()" id="guardar" >Guardar</a>
      </div>
    </div>
    <div class="col-md-4"></div>
  </div>
</div>
</div>
</div>

</div>
</div>
<!-- fin Modal -->
{{Form::close()}}
</div>
</div>


</div>


<script src="{{asset('js/ajax_v2.1.1.js')}}"></script>



<script type="text/javascript">




  var exporta=document.getElementsByName('produc_tecno');

  if(exporta[0].checked){

  $('#tecno_especifique').show(1000).animate({width: "show"}, 1000,"linear");

  }



  var j = 0;
      $('#add2').click(function(){
       j++;

/*****************************************Campos dinamicos de codigos arancelarios********************************************************************/

    $('#fields2 tr:last').after('<tr id="fila'+j+'" display="block" class="show_div"><td>{!!Form::text('codigo[]',null,['class'=>'form-control ejem','data-toggle'=>"modal",'data-target'=>"#modal",'id'=>"codigo",'readonly'=>'true'])!!}</td><td>{!!Form::text('descripcion[]',null,['class'=>'form-control','onkeypress'=>'return soloLetras(event)'])!!}</td><td>{!!Form::text('descrip_comercial[]',null,['class'=>'form-control','onkeypress'=>'return soloLetras(event)'])!!}</td><td>{!!Form::select('gen_unidad_medida_id[]',$unidad_medida,null,['placeholder'=>'--Seleccione una Unidad de Medida--', 'class'=>'form-control']) !!}</td><td>{!!Form::text('cap_opera_anual[]',null,['class'=>'form-control','onkeypress'=>'return soloNumeros(event)'])!!}</td><td>{!!Form::text('cap_insta_anual[]',null,['class'=>'form-control','onkeypress'=>'return soloNumerosletrasyPorcentaje(event)'])!!}</td><td>{!!Form::text('cap_alamcenamiento[]',null,['class'=>'form-control','onkeypress'=>'return soloNumeros(event)'])!!}</td> <td>{!!Form::text('cap_exportacion[]',null,['class'=>'form-control','id'=>'cap_exportacion','onkeypress'=>'return soloNumeros(event)'])!!}</td><td><button  name="remove" type="button" id="'+j+'" value="" class="btn btn-danger btn-remove2">x</button></td></tr>');


       });



     $(document).on('click','.btn-remove2',function(){
        var id_boton= $(this).attr("id");
        $("#fila"+id_boton+"").remove();

     });


      /****************************Validacion de radio tecnologia********************************************/

      $('#prod_tecnologia_si').click(function(event) {
        $('#fields2,#codigo_arancelarios').hide(1000).animate({height: "hide"}, 1000,"linear");
     $('#tecno_especifique').show(1000).animate({width: "show"}, 1000,"linear");

      });

       $('#prod_tecnologia_no').click(function(event) {
        $('#tecno_especifique').hide(1000).animate({height: "hide"}, 1000,"linear");
        $('#fields2,#codigo_arancelarios').show(1000).animate({width: "show"}, 1000,"linear");
      });



  $('input:radio[name="tipo_arancel"]').change( function(){




     if($(this).is(':checked')) {

       var parametros = {
               'id': $(this).val()

       };
       var token= $('input[name="_token"]').val();
       console.log(token);
       $('#id_arancel').val(parametros.id);
      $('#codigo').prop('disabled',false);
       $.ajax({
           type: 'GET',
           url: '/Arancel',
           data: {'arancel': $(this).val(),'_token': token},
           success: function(data){

              $table='<table class="table table-bordered table-hover" id="tabla"><tr><th>Código Arancelario</th><th>Descripción</th></tr>';

              $('#arancel').html($table);
              $.each(data,function(index,value){

             $('#tabla tr:last').after('<tr class="data"><td>'+value.codigo+'</td><td>'+value.descripcion+'</td></tr></table>');

              });

           }

       });

    }
});

$('#codigo').show('modal');

/**------------------------------*/
 var global_id=localStorage.setItem('arancel'," ");


 $(document).on('click','#codigo',function(){

   var id=$(this).parents("tr").attr('id');
   localStorage.setItem('arancel',id);

});

 $(document).on('click','tr.data',function(){

  var id=localStorage.getItem('arancel');
  var cod=$(this).find("td").eq(0).text();
  var ara=$(this).find("td").eq(1).text();

  $("tr#"+id).find("td").eq(0).children("input").val(cod);
  $("tr#"+id).find("td").eq(1).children("input").val(ara);

  $('#modal button').click();
  $('#buscador_arancel').val("");



  });

 $(document).on('keyup','#buscador_arancel',function(){

 var tipo=$('input[name="tipo_arancel"]').val();

   $.ajax({

      type:'get',
      url: '/Arancel',
      data: {'valor': $(this).val(), 'tipoAran':tipo},
      success: function(data){

        $table='<table class="table table-bordered table-hover" id="tabla"><tr><th>Código Arancelario</th><th>Descripción</th></tr>';

        $('#arancel').html($table);
        if(data.length==0){

         $('#tabla tr:last').after('<tr><td colspan="2" style="color:red;text-align:center" >Códigos no existen</td></tr>');

       }else{
        $.each(data,function(index,value){

       $('#tabla tr:last').after('<tr class="data"><td>'+value.codigo+'</td><td>'+value.descripcion+'</td><tr></table');

        });

      }
      }

   });

});




</script>
@endsection
