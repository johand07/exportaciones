<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CertExportaFacil extends Model
{
    protected $table= 'cert_exporta_facil';

    protected $fillable =[

        'gen_usuario_id',
        'num_sol_cert',
        'tipo_meracncia',
        'meracncia',
        'codigo_arancelario',
        'pais_id',
        'peso_estimado',
        'alto',
        'ancho',
        'largo',
        'correo',
        'cantidad',
        'tratamiento_especial',
        'direccion_emisor',
        'direccion_receptor',
        'codigo',
        'regimen',
        'fecha',
        'nombre_repre_export',
        'telefono_exportador',
        'rud',
        'pais_origen',
        'cod_postal',
        'ciudad',
        'fecha_embarque',
        'cod_postal_destino',
        'ciudad_destino',
        'total_fob',
        'total_bultos',
        'tipo_servicio',
        'valor_total_flete_sinIva',
        'valor_total_flete_conIva',
        'total_seguro',
        'telefono_destinario',
        'destinario',
        'bactivo',


    ];

    public function usuario() {

        return $this->hasOne('App\Models\GenUsuario','id','gen_usuario_id');

    }

    public function pais() {

        return $this->hasOne('App\Models\Pais','id','pais_id');

    }

    public function paisOrigen() {

        return $this->hasOne('App\Models\Pais','id','pais_origen');

    }
}
