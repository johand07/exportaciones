@extends('templates/layoutlte')
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="row">
      <div class="col-md-10"></div>
      <div class="col-md-2">
        <a class="btn btn-primary" href="{{url('/exportador/corporacionCafe/create')}}">
          <span class="glyphicon glyphicon-file" title="Agregar" aria-hidden="true"></span>
           Registrar solicitud
        </a>
      </div><br><br>
    </div>
    <table id="listaResguardo" class="table table-striped table-bordered" style="width:100%">
            <thead>
                <tr>
                    <th>Nro. solicitud</th>
                    <th>Fecha de Emisión</th>
                    <th>Estatus</th>
                    <th>Razon social</th>
                    <th>Rif</th>
                    <th>Pais Destino</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody>
                @foreach($solicitudes as  $solicitud)
                <tr>                   
                    <td>{{$solicitud->num_sol_cvc ? $solicitud->num_sol_cvc : ''}}</td>
                    <td>{{$solicitud->created_at ? $solicitud->created_at->format('d-m-Y') : ''}}</td>
                    <td>
                      @if($solicitud->gen_status_id==11)
                        Con Observación!
                      @elseif($solicitud->gen_status_id==12)
                        Aprobada
                      @elseif($solicitud->gen_status_id==18)
                        Corregida
                      @elseif($solicitud->gen_status_id==9)
                        En Proceso
                      @endif
                      </td>
                    <td>{{$solicitud->DetUsuario ? $solicitud->DetUsuario->razon_social : ''}}</td>
                    <td>{{$solicitud->DetUsuario ? $solicitud->DetUsuario->rif : ''}}</td>
                    <td>
                        {{$solicitud->paisDestino ? $solicitud->paisDestino->dpais : ''}}
                    </td>
                    <td>
                      @if($solicitud->gen_status_id==11)
                        <a href="{{route('corporacionCafe.edit',$solicitud->id)}}" class="btn btn-warning btn-sm" title="Corregir" aria-hidden="true">Corregir</a>
                      @elseif($solicitud->gen_status_id==18)
                        Corregida
                      @elseif($solicitud->gen_status_id==12)
                        <a href="{{action('PdfController@CertificadoDemandaInterna',array('id'=>$solicitud->id))}}" class="glyphicon glyphicon-file btn btn-default btn-sm" target="_blank"  aria-hidden="true" title="Certificado Demanda Interna"></a>
                        <a href="{{action('PdfController@CertificadoDemandaInternaSol',array('id'=>$solicitud->id))}}" class="glyphicon glyphicon-file btn btn-default btn-sm" target="_blank"  aria-hidden="true" title="Certificado Demanda Interna Sol"></a>
                      @elseif($solicitud->gen_status_id==9)
                        En Proceso
                      @endif
                    </td>
                </tr>
              @endforeach
            </tbody>
        </table>
  </div>
</div>
@stop