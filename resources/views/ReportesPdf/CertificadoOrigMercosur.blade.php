<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>CERTIFICADO DE ORIGEN ACUERDO MERCOSUR-COLOMBIA, ECUADOR Y VENEZUELA</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style type="text/css">
        .centrar{
            text-align: center;
        }

        .titulo{
            
            font-weight: bold;
        }

        .nro-correlativo{
            color: red;
            font-size: 16px;
        }

        .derecha{
            text-align: right;

        }

        .text-normal{
            font-size: 14px;
        }

        .text-medio{
            font-size: 12px;
        }
        .text-11{
            font-size: 11px;
        }
        .text-small{
            font-size: 10px;
        }

        .text-9{
            font-size: 8px;
        }

        .text-8{
            font-size: 8px;
        }

        .espacio-superior{
            margin-top: 10px;
        }

        .espacio-superior-small{
            margin-top: 5px;
        }

        .ancho-medio{
            width: 50%;
        }

        .ancho-full{
            width: 100%;
        }

        .ancho-medio-alto{
            width: 75%;
        }

        .ancho-90{
            width: 90%;
        }

        .table{
            border-collapse: collapse;
        }
        
        .borde{
            border: 1px solid #000;
        }

        .sin-borde{
            border: none;
        }

        .top{
            vertical-align: top;
        }

        .under-dotted{    
            border-bottom: 1px dotted #000;
            text-decoration: none;
        }

        .content-1{
            width: 5%;
        }

        .content-9{
            width: 20%;
        }

        .sangria-1{
            margin-left: 28px;
        }

        .sangria-2{
            margin-left: 36px;
        }

        .sangria-3{
            margin-left: 54px;
        }

        .sangria-sub{
            margin-left: 14px;
            
        }

        
        .page-break { page-break-before: always; }

        .table-center{
            margin: 0 auto; 
        }

        .justify{
            text-align: justify;
        }

        td.justify {
            padding-left: 8px;
            padding-right: 8px; 
        }
        .lineal{
            
        }
        .lineal>div{
            max-width:91%;

        }
        .lineal>*{
           display: inline-block;
           vertical-align:top;
        }

        .sangria-lista{
            list-style-type: none;
            margin: 0;
            padding: 0;
            margin-bottom: 4px;
        }

        .sangria-lista>li{
            margin-top: 5px;
            padding-left: 10px;
            padding-right: 10px;
        }
    </style>
</head>
<body>
    
    <!--div class="derecha titulo text-medio"><b style="color:red">N°</b> del Certificado <span class="">{{ $certificado->num_certificado}}</span></div-->
    <div class="centrar titulo text-normal espacio-superior">CERTIFICADO DE ORIGEN<br>ACUERDO MERCOSUR-COLOMBIA, ECUADOR Y VENEZUELA</div>
    <div class="centrar espacio-superior">
        <table border="0" class="ancho-full">
        <tr>
            <td class="ancho-medio centrar">
            <div class="text-normal">1. PAÍS EXPORTADOR: <u>{{$certificado->pais_exportador}}</u></div>
            </td>
            <td class="ancho-medio centrar">
            <div class="text-normal">2. PAÍS IMPORTADOR: <u>{{$certificado->pais_importador}}</u></div>
            </td>
        </tr>
        </table>
    </div>

    <div class="centrar espacio-superior">
        <table class="ancho-full borde table">
            <tr class="text-11 ">
                <td class="centrar top titulo borde" style="width: 13%;">3. No. de Orden</td>
                <td class="centrar top titulo borde" style="width: 19%;">4. NALADISA</td>
                <td class="centrar top titulo borde" style="width: 29%;">5. DENOMINACIÓN DE LA MERCANCIA</td>
                <td class="centrar top titulo borde" style="width: 10%;">6. Peso o Cantidad</td>
                <td class="centrar top titulo borde" style="width: 17%;">7. Valor FOB <br>en US$</td>
            </tr>
           
            @foreach($detalleProduct as $value)
            
            <tr class="text-medio">
                <td class="centrar top">{{$value->num_orden}}</td>
                <td class="centrar top">{{$value->codigo_arancel}}</td>
                <td class="centrar top">{{$value->denominacion_mercancia}}</td>
                <td class="centrar top">{{$value->unidad_fisica}}</td>
                <td class="centrar top">{{$value->valor_fob}}</td>
            </tr>
            
            
            @endforeach

            @if(count($detalleProduct)<=1)
                @for($auxConut = 0; $auxConut<=3-count($detalleProduct); $auxConut++)
                <tr class="text-medio">
                    <td class="centrar top">&nbsp;</td>
                    <td class="centrar top">&nbsp;</td>
                    <td class="centrar top">&nbsp;</td>
                    <td class="centrar top">&nbsp;</td>
                    <td class="centrar top">&nbsp;</td>
                </tr>
                @endfor
            @endif
        
        </table>
    </div>
    <div class="centrar titulo text-medio espacio-superior">8 DECLARACION DE ORIGEN</div>
    <div class="text-medio espacio-superior">DECLARAMOS que las mercancías indicadas en el presente formulario, correspondientes a la (8.1) Factura Comercial No. <u>{{ $detdeclaracionCert[0]->numero_factura }}</u>
    de <br>(8.2) fecha <u>{{ $detdeclaracionCert[0]->f_fac_certificado }}</u> cumplen con lo establecido en las Normas de Origen del presente Acuerdo <b>ACE 59</b> de conformidad con el siguiente desglose.</div>

    <div class="centrar espacio-superior">
        <table class="ancho-full borde table">
            <tr class="text-medio">
                <td class="centrar top titulo borde" style="width: 15%;">9. No. de Orden</td>
                <td class="centrar top titulo borde" style="width: 85%;">10. NORMAS</td>
            </tr>
            @foreach($detdeclaracionCert as $value1)
            
                <tr class="text-center">
                    <td class="centrar top">{{$value1->num_orden_declaracion}}</td>
                    <td class="centrar top">{{$value1->normas_criterio}}</td>
                </tr>

            @endforeach

            @if(count($detdeclaracionCert)<=1)
                @for($auxConut = 0; $auxConut<=3-count($detdeclaracionCert); $auxConut++)
                <tr class="text-medio">
                    <td class="centrar top">&nbsp;</td>
                    <td class="centrar top">&nbsp;</td>
                </tr>
                @endfor
            @endif
        
        </table>
    </div>
    <div class="centrar espacio-superior">
        <table class="ancho-full table borde">
            <tr class="text-medio">
                <td class="top titulo borde" style="width: 65%;">11 EXPORTADOR O PRODUCTOR</td>
                 <td class="top titulo borde" colspan="2">(12) Sello Y firma del Exportador o Productor</td>
            </tr>
            <tr class="text-medio">
                <td class="top borde">11. Razón Social: {{ $certificado->GenUsuario->DetUsuario->razon_social }}</td>
                <td colspan="2"></td>
            </tr>
            <tr class="text-medio">
                <td class="top borde">11.2 Dirección: {{ $certificado->GenUsuario->DetUsuario->direccion }}</td>
                <td colspan="2"></td>
            </tr>
            <tr class="text-medio">
                <td class="top borde">11.3 Fecha: {{ $certificado->fecha_emision_exportador}}</td>
                <td colspan="2"></td>
            </tr>

            <tr class="text-medio">
                <td class="top titulo borde">13. IMPORTADOR</td>
                <td colspan="2"></td>
            </tr>
            <tr class="text-medio">
                <td class="top borde">13.1 Razón Social: {{ strtoupper($importadorCertificado->razon_social_importador) }}</td>
                <td colspan="2"></td>
            </tr>
            <tr class="text-medio">
                <td class="top borde">13.1 Dirección: {{ strtoupper($importadorCertificado->direccion_importador) }}</td>
                <td colspan="2"></td>
            </tr>
            <tr class="text-medio">
                <td class="top borde">14. Medio de Transporte: {{ ($importadorCertificado->medio_transporte_importador) }}</td>
                <td colspan="2"></td>
            </tr>
            <tr class="text-medio">
                <td class="top borde">15. Puerto o Lugar de Embarque: {{ ($importadorCertificado->lugar_embarque_importador) }}</td>
                <td colspan="2"></td>
            </tr>
        </table>
        <div class="text-normal borde table justify"><b>16. OBSERVACIONES</b><br><br>{{$certificado->observaciones}}<br><br></div>
        <div>
           <table class="ancho-full table borde">
            <tr class="text-medio">
                <td class="top titulo borde" style="width: 65%;"><b><center>17. CERTIFICACIÓN DE ORIGEN</b></center><br><br><br><br>17.1 Certifico la veracidad de la presente declaracion, en la ciudad de:_____________ <br> a los:____/____/____ <br><br><br><br>
                17.2 Nombre de la Entidad Certificados:_________________________________ <br><br>
                </td>
                 <td class="top titulo borde" colspan="2">18. Sello Y firma de La Entidad Certificadora</td>
            </tr>
        </table>
         <table class="ancho-full">
        <tr class="text-medio izquierda">
            <td class="justify text-11"><b class="text-11">ORIGINAL: ADUANA DE DESTINO DUPLICADO: EXPORTADOR</b></td>
            <td class="centrar"><b class="text-11">TRIPLICADO: MINISTERIO CUADRUPLICADO:MINISTERIO</b></td>
            <td class="centrar"><b class="text-11">EL FORMULARIO NO PODRA PRESENTAR RASPADURAS, TACHADURAS O ENMIENDAS.</b></td>
        </tr>
    </table>
        </div>
    </div>
</body>
</html>