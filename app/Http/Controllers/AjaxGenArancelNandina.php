<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Productos;
use App\Models\GenArancelNandina;
use Auth;

class AjaxGenArancelNandina extends Controller
{

	/*Para retornar los aranceles NO ASOCIADOS a usuarios. Se limita a 50 el resultado por busqueda para no sobre cargar a la pagina que espera la respuesta*/
    public function listarTodo(Request $request){

    	if($request['valor']=="")
            {

              
             $arancel=GenArancelNandina::whereRaw("LENGTH(REPLACE(codigo, '.', '')) = 10")->where('bactivo',1)->take(50)->get();

            }else{
                 
                 $arancel=GenArancelNandina::whereRaw("LENGTH(REPLACE(codigo, '.', '')) = 10")->where('bactivo',1)
                                    ->where(function($q) use ($request){

                                                $q->where('codigo','like','%'.$request['valor'].'%')
                                                ->orWhere('descripcion','like','%'.$request['valor'].'%');
                                            })
                                    ->take(50)
                                    ->get();
            }

            return response()->json($arancel);
    }

/*Para retornar los aranceles ASOCIADOS AL USUARIO. Se limita a 50 el resultado por busqueda para no sobre cargar a la pagina que espera la respuesta*/
     public function listarAsociado(Request $request){

    	if($request['valor']=="")
            {

              
                $arancel=GenArancelNandina::whereRaw("LENGTH(REPLACE(codigo, '.', '')) = 10")->where('gen_arancel_nandina.bactivo',1)->join('productos','gen_arancel_nandina.codigo','=','productos.codigo')->where('productos.gen_usuario_id',Auth::user()->id)->groupBy('gen_arancel_nandina.codigo')->take(50)->get();

            }else{
                 
                 $arancel=GenArancelNandina::whereRaw("LENGTH(REPLACE(codigo, '.', '')) = 10")->where('gen_arancel_nandina.bactivo',1)->join('productos','gen_arancel_nandina.codigo','=','producto.codigo')->where('producto.gen_usuario_id',Auth::user()->id)->where(function($q) use ($request){

                                                $q->where('gen_arancel_nandina.codigo','like','%'.$request['valor'].'%')
                                                ->orWhere('gen_arancel_nandina.descripcion','like','%'.$request['valor'].'%');
                                            })
                 					->groupBy('gen_arancel_nandina.codigo')
                                    ->take(50)
                                    ->get();

                 $arancel;
            }

           return  response()->json($arancel);
    }

    public function index(Request $request){
        $arancel=GenArancelNandina::whereRaw("LENGTH(REPLACE(codigo, '.', '')) = 10")->where('bactivo',1)->get();

       // if(empty($request->layout))
        return view('arancel.lista_nandina',compact('arancel'));
    }
}
