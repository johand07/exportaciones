<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CatCertificadoEfectosDe extends Model
{
    protected $table = 'cat_certificado_efectos_de';



	protected $fillable = [

        'nombre',
        'bactivo',
    ];

    protected $date =['created_at','updated_at'];

}
