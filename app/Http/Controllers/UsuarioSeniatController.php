<?php

namespace App\Http\Controllers;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Http\Request;
use App\Models\GenArancelMercosur;
use App\Models\GenCertRegSeniat;
use App\Models\DocCertRegSeniat;
use Auth;
use File;
use Alert;

class UsuarioSeniatController extends Controller
{
    public function index()
    {
        return view('UsuarioSeniat.index');
    }

    public function lista()
    {
        $certificados = GenCertRegSeniat::with('GenStatus')->where('bactivo', 1)->get();
        return view('UsuarioSeniat.lista', compact('certificados'));
    }

    public function create()
    {
        $codigos = GenArancelMercosur::where('bactivo', 1)->get();

        $arancel = [];
        foreach($codigos as $row){
            if(strlen($row->codigo) >= 13){
                array_push($arancel, 
                [
                    "codigo" => $row->codigo. '-'. $row->descripcion,
                ]);
            }
        }

        //dd($arancel);
        return view('UsuarioSeniat.create', compact('arancel'));
    }

    public function show($id)
    {
        $certificado = GenCertRegSeniat::find($id);

        $file = DocCertRegSeniat::where('gen_cert_reg_seniat_id', $id)->first();

        return view('UsuarioSeniat.show', compact('certificado', 'file'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'file'                    => 'required|mimes:pdf|max:2000',
            'codigo_arancelario'      => 'required'
            
        ]);

        $certificado = GenCertRegSeniat::create([
                                        'gen_usuario_id'        => Auth::user()->id,
                                        'gen_status_id'          => 27,
                                        'codigo_arancelario'     => $request->codigo_arancelario,
                                        'fecha_certificado'      => date('Y-m-d'),
                        ]);

        //Guardar las imagenes
        $destino = 'img/certificadoSeniat/';
        $destinoPrivado = public_path($destino);
        
        if (!file_exists($destinoPrivado)) {
            $filesystem = new Filesystem();
            $filesystem->makeDirectory($destinoPrivado, $mode = 0777, true, true); 
        }


        

            $imagen = $request->file('file');
            
            if (!empty($imagen)) {
                
               $nombre_imagen = $imagen->getClientOriginalName();
            
                $imagen->move($destinoPrivado, $nombre_imagen);

                $doCertif                           = new DocCertRegSeniat;
                $doCertif->gen_cert_reg_seniat_id   = $certificado->id;
                $doCertif->file                     = $destino.'/'.$nombre_imagen;
                $doCertif->save();
               
            }
            

        
        Alert::success('Se han guardado los documentos correspondientes!','Registrado Correctamente!')->persistent("Ok");

        return redirect()->action('UsuarioSeniatController@lista');
        
        //dd($request->all());
    }
}
