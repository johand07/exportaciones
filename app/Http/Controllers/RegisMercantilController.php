<?php

namespace App\Http\Controllers;
use App\Models\CircunsJudicial;
use App\Models\DetUsuario;
use Illuminate\Http\Request;
use Alert;

class RegisMercantilController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\DetUsuario  $detUsuario
     * @return \Illuminate\Http\Response
     */
    public function show(DetUsuario $detUsuario)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\DetUsuario  $detUsuario
     * @return \Illuminate\Http\Response
     */
    public function edit(DetUsuario $detUsuario, $id)
    {

        $titulo='Registro Mercantil';
        $descripcion='Actualización de Datos';
        $RegMercantil=DetUsuario::where('gen_usuario_id',$id)->first();
        $circuns_judiciales=CircunsJudicial::all()->pluck('registro','id');
        //dd($RegMercantil);

        return view('DatosJuridicos.edit_regist_mer', compact('RegMercantil','circuns_judiciales','titulo','descripcion'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\DetUsuario  $detUsuario
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DetUsuario $detUsuario, $id)
    {



        if($request->produc_tecno==2){

            $esp="No exporta tecnología";
        }else{

            $esp=$request->especifique_tecno;
        }
        $RegMercantil=DetUsuario::find($id);
        $RegMercantil->circuns_judicial_id=$request->circuns_judicial_id;
        $RegMercantil->num_registro=$request->num_registro;
        $RegMercantil->tomo=$request->tomo;
        $RegMercantil->folio=$request->folio;
        $RegMercantil->Oficina=$request->Oficina;
        $RegMercantil->f_registro_mer=$request->f_registro_mer;
        //$RegMercantil->nombre_presid=$request->nombre_presid;
        //$RegMercantil->apellido_presid=$request->apellido_presid;
        //$RegMercantil->ci_presid=$request->ci_presid;
        //$RegMercantil->cargo_presid=$request->cargo_presid;
        $RegMercantil->nombre_repre=$request->nombre_repre;
        $RegMercantil->apellido_repre=$request->apellido_repre;
        $RegMercantil->ci_repre=$request->ci_repre;
        $RegMercantil->cargo_repre=$request->cargo_repre;
        $RegMercantil->correo_repre=$request->correo_repre;
        $RegMercantil->especifique_tecno=$esp;
        $RegMercantil->produc_tecno=$request->produc_tecno;

        $RegMercantil->save();

        Alert::success('Registro Mercantil Actualizado!', 'ACTUALIZACIÓN EXITOSA.')->persistent("Ok");
        return view('DatosJuridicos.botones_juridicos');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\DetUsuario  $detUsuario
     * @return \Illuminate\Http\Response
     */
    public function destroy(DetUsuario $detUsuario)
    {
        //
    }
}
