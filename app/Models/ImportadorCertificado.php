<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ImportadorCertificado extends Model
{
    protected $table = 'importador_certificado';
    protected $fillable = [

           'id',
           'gen_certificado_id',
           'razon_social_importador',
           'direccion_importador',
           'pais_ciudad_importador',
           'email_importador',
           'telefono',
           'medio_transporte_importador',
           'lugar_embarque_importador',
           'bactivo',
    ];

    protected $date =['created_at','updated_at'];

    public function GenCertificado() {

     return $this->hasOne('App\Models\GenCertificado','id','gen_certificado_id');

    }
}
