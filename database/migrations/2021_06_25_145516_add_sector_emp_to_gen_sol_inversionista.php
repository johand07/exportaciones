<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSectorEmpToGenSolInversionista extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('gen_sol_inversionista', function (Blueprint $table) {
            $table->string('cat_sector_inversionista_id')->nullable()->after('descrip_proyec_inver');
         
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('gen_sol_inversionista', function (Blueprint $table) {
            $table->dropColumn('cat_sector_inversionista_id');
           
        });
    }
}
