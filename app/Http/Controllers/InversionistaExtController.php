<?php

namespace App\Http\Controllers;
use Illuminate\Filesystem\Filesystem;
use App\Models\GenSolicitud;
use App\Models\Accionistas; 
use App\Models\DetUsuario;
use App\Models\CatTipoUsuario;
use App\Models\Pais;
use App\Models\GenSolInversionista;
use App\Models\GenAccionistaInversionista;
use App\Models\CatTipoVisa;
use App\Models\GenDocumentosInversiones;
use App\Models\CatSectorInversionista;
use App\Models\CatActividadEcoInversionista;
use App\Models\DocumentsVariosInversiones;
use App\Models\GenDivisa;
use App\Http\Requests\InversionistaExtRequest;
use App\Http\Requests\UpdateInversionistaExtRequest;
use Illuminate\Http\Request;
use Session;
use Auth;
use Alert;
use File;


class InversionistaExtController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $solinversion=GenSolInversionista::where('gen_usuario_id',Auth::user()->id)->orderBy('id','ASC')->get();
       
        return view('BandejaInversionista.index', compact('solinversion'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        $tipousuario=CatTipoUsuario::where('bactivo',1)->pluck('nombre','id');

    
        return view('BandejaInversionista.create', compact('tipousuario') );
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     *///InversionistaExtRequest
    public function store(InversionistaExtRequest $request)
    {//dd($request->all());

        //dd(1);
        //generando numero de solicitud
            //CONTAMOS LA CANTIDAD DE MIS SOLICITUDES DE INVERSION
            $numSolicitudesInversion=GenSolInversionista::where('gen_usuario_id',Auth::user()->id)->get()->count();
            //LE CONCATENAMOS UN RANDOM AL ID DE USUARIO
            $idUsuario=rand(10,99).Auth::user()->id;
            //ME TRAIGO la fecha del servidor sin guiones
            $fecha=date('Ymd');
            //consultamos la ultima solicitud mis de inversiones
            $idUltimatSolicitud=GenSolInversionista::where('gen_usuario_id',Auth::user()->id)->get()->last();
            //valido si tengo o no una ultima solicitud
            if ($idUltimatSolicitud==null){
                $idUltimatSolicitud=0;
            }else{
                $idUltimatSolicitud=$idUltimatSolicitud->id;
            }
            //creo mi numero de solicitud
            $num_solicitud=($numSolicitudesInversion+1)."-I".$idUsuario.$fecha.$idUltimatSolicitud;
            //dd($num_solicitud);
            $fechaestatus=date('Y-m-d');
        $detUsu=DetUsuario::where('gen_usuario_id',Auth::user()->id)->where('bactivo',1)->first();
        //actualizamos dadtos de det_usuario
        $datos=DetUsuario::where('gen_usuario_id',Auth::user()->id)->where('bactivo',1)
        ->update([  'razon_social'      => $request->razon_social,
                    'ci_repre'          => (isset($request->ci_repre))?$request->ci_repre:$detUsu->ci_repre,
                    'direccion'         => $request->direccion,
                    'telefono_movil'    => $request->telefono_movil,
                    'correo'            => $request->correo,
                    'pagina_web'        => (isset($request->pagina_web))?$request->pagina_web:'',
                    'nombre_repre'      => (isset($request->nombre_repre))?$request->nombre_repre:'',
                    'apellido_repre'    => (isset($request->apellido_repre))?$request->apellido_repre:'',
                    'updated_at'        =>  date("Y-m-d H:i:s") 
        ]);


        if ($request->cat_tipo_usuario_id ==2) { 
            
            //insertamos en la tabla de registro de inversionista
            $SolicitudInversion= new GenSolInversionista;
            $SolicitudInversion->num_sol_inversionista          =   $num_solicitud;
            $SolicitudInversion->n_doc_identidad                =   $request->n_doc_identidad;
            $SolicitudInversion->gen_usuario_id                 =   Auth::user()->id;
            $SolicitudInversion->gen_status_id                  =   9;
            $SolicitudInversion->pais_id                        =   $request->pais_id;
            $SolicitudInversion->cat_tipo_visa_id               =   (isset($request->cat_tipo_visa_id))?$request->cat_tipo_visa_id:4;
            $SolicitudInversion->gen_divisa_id                  =   $request->gen_divisa_id;
            $SolicitudInversion->fstatus                        =   $fechaestatus;
            $SolicitudInversion->tipo_inversion                 =   $request->tipo_inversion;
            $SolicitudInversion->descrip_proyec_inver           =   $request->descrip_proyec_inver;
            $SolicitudInversion->cat_sector_inversionista_id           =   $request->cat_sector_inversionista_id;
            $SolicitudInversion->monto_inversion                =   $request->monto_inversion;
            $SolicitudInversion->actividad_eco_emp              =   $request->actividad_eco_emp;
            $SolicitudInversion->correo_emp_recep               =   $request->correo_emp_recep;
            $SolicitudInversion->pag_web_emp_recep              =   (isset($request->pag_web_emp_recep))?$request->pag_web_emp_recep:'';
            $SolicitudInversion->nombre_repre_legal             =   (isset($request->nombre_repre))?$request->nombre_repre:'';
            $SolicitudInversion->apellido_repre_legal           =   (isset($request->apellido_repre))?$request->apellido_repre:'';
            $SolicitudInversion->especif_otro_sector            =   (isset($request->especif_otro_sector))?$request->especif_otro_sector:'';
            $SolicitudInversion->especif_otro_actividad         =   (isset($request->especif_otro_actividad))?$request->especif_otro_actividad:'';
            $SolicitudInversion->cat_tipo_usuario_id            =   $request->cat_tipo_usuario_id;
            $SolicitudInversion->save();
            //consulto la ultima solictud mia
            $idUltimatSolicitudInsertada=GenSolInversionista::where('gen_usuario_id',Auth::user()->id)->get()->last();

            
           
            //preguntamos quienes se actualizaron op crearon
            $accionistas_actualizados=Accionistas::where('gen_usuario_id',Auth::user()->id)->where('bactivo',1)->orderby('updated_at','DESC')->get();

            //insertamos asociacion de solicitud con accionistas
            foreach ($accionistas_actualizados as $key => $value) {
                $Accionista                                 =   new GenAccionistaInversionista;
                $Accionista->gen_sol_inversionista_id       =   $idUltimatSolicitudInsertada->id;
                $Accionista->accionistas_id                 =   $value->id;
                $Accionista->save();
            }
              
        } else {
           
            $SolicitudInversion= new GenSolInversionista;
            $SolicitudInversion->num_sol_inversionista      =   $num_solicitud;
            $SolicitudInversion->n_doc_identidad                =   $request->n_doc_identidad;
            $SolicitudInversion->gen_usuario_id             =   Auth::user()->id;
            $SolicitudInversion->gen_status_id              =   9;
            $SolicitudInversion->pais_id                    =   $request->pais_id;
            $SolicitudInversion->gen_divisa_id              =   $request->gen_divisa_id;
            $SolicitudInversion->cat_tipo_visa_id           =   (isset($request->cat_tipo_visa_id))?$request->cat_tipo_visa_id:4;
            $SolicitudInversion->fstatus                    =   $fechaestatus;
            $SolicitudInversion->tipo_inversion             =   $request->tipo_inversion;
            $SolicitudInversion->descrip_proyec_inver       =   $request->descrip_proyec_inver;
            $SolicitudInversion->cat_sector_inversionista_id       =   $request->cat_sector_inversionista_id;

            $SolicitudInversion->monto_inversion            =   $request->monto_inversion;
            $SolicitudInversion->actividad_eco_emp          =   $request->actividad_eco_emp;
            $SolicitudInversion->correo_emp_recep           =   $request->correo_emp_recep;
            $SolicitudInversion->pag_web_emp_recep          =   (isset($request->pag_web_emp_recep))?$request->pag_web_emp_recep:'';
            $SolicitudInversion->nombre_repre_legal         =   (isset($request->nombre_repre))?$request->nombre_repre:'';
            $SolicitudInversion->apellido_repre_legal       =   (isset($request->apellido_repre))?$request->apellido_repre:'';
            $SolicitudInversion->especif_otro_sector        =   (isset($request->especif_otro_sector))?$request->especif_otro_sector:'';
            $SolicitudInversion->especif_otro_actividad     =   (isset($request->especif_otro_actividad))?$request->especif_otro_actividad:'';
            $SolicitudInversion->cat_tipo_usuario_id        =   $request->cat_tipo_usuario_id;
            $SolicitudInversion->save();
        }


        
        //Guardar las imagenes
        $destino = 'img/inversionistaExt/'.Auth::user()->detUsuario->cod_empresa;
        $destinoPrivado = public_path($destino);
        
        if (!file_exists($destinoPrivado)) {
            $filesystem = new Filesystem();
            $filesystem->makeDirectory($destinoPrivado, $mode = 0777, true, true); 
        }

        
        for( $i=1; $i <= 10 ; $i++ ){

            if(isset($request->file_1)){
                $imagen = $request->file('file_'.$i);
            
                if (!empty($imagen)) {
                    
                    $nombre_imagen = $imagen->getClientOriginalName();
                   
                    $imagen->move($destinoPrivado, $nombre_imagen);
                    $auxStr='file_'.$i;
                    $$auxStr = $nombre_imagen;
                
                }
            }      
        } 
       // dd($request->all());
            $inv_ext                                = new GenDocumentosInversiones;
            $inv_ext->gen_sol_inversionista_id      =     $SolicitudInversion->id;
            $inv_ext->file_1                        = $destino.'/'.$file_1;

            if (!empty($file_2)) {
                $inv_ext->file_2 = $destino.'/'.$file_2;
            }
            if (!empty($file_3)) {
                $inv_ext->file_3 = $destino.'/'.$file_3;
            }
            if (!empty($file_4)) {
                $inv_ext->file_4 = $destino.'/'.$file_4;
            }
            if (!empty($file_5)) {
                $inv_ext->file_5 = $destino.'/'.$file_5;
            }
            if (!empty($file_6)) {
                $inv_ext->file_6 = $destino.'/'.$file_6;
            }
            if (!empty($file_7)) {
                $inv_ext->file_7 = $destino.'/'.$file_7;
                
            }
            if (!empty($file_8)) {
                $inv_ext->file_8 = $destino.'/'.$file_8;
            }
            if (!empty($file_9)) {
                $inv_ext->file_9 = $destino.'/'.$file_9;
            }
            if (!empty($file_10)) {
                $inv_ext->file_10 = $destino.'/'.$file_10;
            }
            
            $inv_ext->bactivo=1;

            $inv_ext->save();
            /* */

            for($i=0; $i <= count($request->file)-1; $i++){

                $imagen = $request->file[$i];
                $doc = new DocumentsVariosInversiones;
                $doc->cat_documentos_id = $request->cat_documentos_id[$i];
                $doc->gen_sol_inversionista_id=$SolicitudInversion->id;
                    if (!empty($imagen)) {
                            
                        $nombre_imagen = $imagen->getClientOriginalName();
                    
                        $imagen->move($destinoPrivado, $nombre_imagen);
                        //$auxStr='file_'.$i;
                        //$$auxStr = $nombre_imagen;
                    
                    }
                $doc->file =$destino.'/'.$nombre_imagen;
                $doc->save();
            }

            Alert::success('Registro Exitoso!', 'Solicitud agregada.')->persistent("OK");
            return redirect()->action('InversionistaExtController@index');
    }

  

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\GenSolicitud  $genSolicitud
     * @return \Illuminate\Http\Response
     */
    public function show(GenSolicitud $genSolicitud)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\GenSolicitud  $genSolicitud
     * @return \Illuminate\Http\Response
     */
    public function edit(GenSolInversionista $GenSolInversionista, $id)
    {


        $inver=GenSolInversionista::find($id);//find es para buscar y (encontrar) en la tabla el que tenga este id
            
        $documentos=GenDocumentosInversiones::where('gen_sol_inversionista_id',$id)->first();// Uno el que pido el primero que consiga
        
        $datos=DetUsuario::where('gen_usuario_id',Auth::user()->id)->where('bactivo',1)->first();// le estoy pidiendo el primero que es el de este usuario
            //dd($datos);
        $tipousuario=CatTipoUsuario::where('bactivo',1)->pluck('nombre','id');
        
        $pais=Pais::where('bactivo',1)->pluck('dpais','id');

        $divisas=GenDivisa::orderBy('ddivisa_abr','asc')->pluck('ddivisa_abr','id');

        //Consulta para pintar el select los sectores de la Actividad////////// Económica//////////////////////////////////////////////////////////////
        $sectorActEco=CatSectorInversionista::where('bactivo',1)->pluck('nombre','id');
        ////////////////////////////////////////////////////////////////////////
        $docListDinamica1=DocumentsVariosInversiones::where('gen_sol_inversionista_id',$id)->where('cat_documentos_id',12)->get();

        $docListDinamica2=DocumentsVariosInversiones::where('gen_sol_inversionista_id',$id)->where('cat_documentos_id',14)->get();


        $TipoInversion=['Extranjera Directa'=>'Extranjera Directa',
        'Extranjera De Cartera'=>'Extranjera De Cartera',
        'Extranjera De Cartera Inversión financiera en divisas'=>'Extranjera De Cartera Inversión financiera en divisas',
        'Extranjera de Cartera Bienes de capital físicos o tangibles'=>'Extranjera de Cartera Bienes de capital físicos o tangibles',
        'Extranjera de Cartera Bienes inmateriales o intangibles'=>'Extranjera de Cartera Bienes inmateriales o intangibles',
        'Extranjera de Cartera Reinversiones'=>'Extranjera de Cartera Reinversiones'];


        
        $cat_tipo_usuario_id = $inver->cat_tipo_usuario_id;

        if ($cat_tipo_usuario_id == 1){
            $view = 'BandejaInversionista.edit_natural';
        }else{
            $view = 'BandejaInversionista.edit_juridico';
        }

        /*if ($inver->cat_tipo_usuario_id == 1){
            $view = 'BandejaInversionista.edit_natural';
        }else{
            $view = 'BandejaInversionista.edit_juridico';
        }*/
      return view($view, compact('inver','id','datos','tipousuario','pais','documentos','cat_tipo_usuario_id','sectorActEco','docListDinamica1','docListDinamica2','TipoInversion','divisas'));//Retorna Carpeta y Edit formulario
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\GenSolicitud  $genSolicitud
     * @return \Illuminate\Http\Response
     *///UpdateInversionistaExtRequest
    public function update(UpdateInversionistaExtRequest $request,GenSolInversionista $GenSolInversionista, $id)
    {
        //dd($request->all());
        $fechaestatus=date('Y-m-d');
        $detUsu=DetUsuario::where('gen_usuario_id',Auth::user()->id)->where('bactivo',1)->first();
        $datos=DetUsuario::where('gen_usuario_id',Auth::user()->id)->where('bactivo',1)
        ->update([  'razon_social'      => $request->razon_social,
                    'ci_repre'          => (isset($request->ci_repre))?$request->ci_repre:$detUsu->ci_repre,
                    'direccion'         => $request->direccion,
                    'telefono_movil'    => $request->telefono_movil,
                    'correo'            => $request->correo,
                    'pagina_web'        => (isset($request->pagina_web))?$request->pagina_web:'',
                    'nombre_repre'      => (isset($request->nombre_repre))?$request->nombre_repre:'',
                    'apellido_repre'    => (isset($request->apellido_repre))?$request->apellido_repre:'',
                    'updated_at'        =>  date("Y-m-d H:i:s") 
        ]);

        if (empty($request->cat_tipo_visa_id)){
            $tipovisa=4;

        }else{
            $tipovisa=$request->cat_tipo_visa_id; 
        }

//dd($request->gen_divisa_id);
        if (Auth::user()->cat_tipo_usuario_id ==2) { //Validando si el Usuario es Juridico
            
            //insertamos en la tabla de registro de inversionista
            $SolicitudInversion= GenSolInversionista::find($id);
           // $SolicitudInversion->num_sol_inversionista          =   $num_solicitud;
            $SolicitudInversion->n_doc_identidad                =   $request->n_doc_identidad;
            $SolicitudInversion->gen_usuario_id                 =   Auth::user()->id;
            $SolicitudInversion->gen_status_id                  =   18;
            $SolicitudInversion->pais_id                        =   $request->pais_id;
            $SolicitudInversion->gen_divisa_id                  =   $request->gen_divisa_id;
            $SolicitudInversion->cat_tipo_visa_id               =   (isset($request->cat_tipo_visa_id))?$request->cat_tipo_visa_id:4;
            $SolicitudInversion->fstatus                        =   $fechaestatus;
            $SolicitudInversion->tipo_inversion                 =   $request->tipo_inversion;
            $SolicitudInversion->descrip_proyec_inver           =   $request->descrip_proyec_inver;
            $SolicitudInversion->monto_inversion                =   $request->monto_inversion;
            $SolicitudInversion->actividad_eco_emp              =   $request->actividad_eco_emp;
            $SolicitudInversion->correo_emp_recep               =   $request->correo_emp_recep;
            $SolicitudInversion->pag_web_emp_recep              =   (isset($request->pag_web_emp_recep))?$request->pag_web_emp_recep:'';
            $SolicitudInversion->nombre_repre_legal             =   (isset($request->nombre_repre))?$request->nombre_repre:'';
            $SolicitudInversion->apellido_repre_legal           =   (isset($request->apellido_repre))?$request->apellido_repre:'';
            $SolicitudInversion->especif_otro_sector            =   (isset($request->especif_otro_sector))?$request->especif_otro_sector:'';
            $SolicitudInversion->especif_otro_actividad         =   (isset($request->especif_otro_actividad))?$request->especif_otro_actividad:'';
            $SolicitudInversion->cat_tipo_usuario_id            =   $request->cat_tipo_usuario_id;
            $SolicitudInversion->cat_sector_inversionista_id    =   $request->cat_sector_inversionista_id;
            $SolicitudInversion->estado_observacion             =   0;
            $SolicitudInversion->observacion_inversionista      =   '';
            $SolicitudInversion->observacion_anulacion          =   '';
            $SolicitudInversion->observacion_doc_incomp         =   '';
            $SolicitudInversion->updated_at                     =   date("Y-m-d H:i:s");
            $SolicitudInversion->save();
                      
        } else {//sino es por es natural
            //insertamos en la tabla de registro de inversionista
            $SolicitudInversion= GenSolInversionista::find($id);
            //$SolicitudInversion->num_sol_inversionista      =   $num_solicitud;
            $SolicitudInversion->gen_usuario_id             =   Auth::user()->id;
            $SolicitudInversion->gen_status_id              =   18;
            $SolicitudInversion->pais_id                    =   $request->pais_id;
            $SolicitudInversion->gen_divisa_id                  =$request->gen_divisa_id;
            $SolicitudInversion->cat_tipo_visa_id           =   (isset($request->cat_tipo_visa_id))?$request->cat_tipo_visa_id:4;
            $SolicitudInversion->fstatus                    =   $fechaestatus;
            $SolicitudInversion->tipo_inversion             =   $request->tipo_inversion;
            $SolicitudInversion->descrip_proyec_inver       =   $request->descrip_proyec_inver;
            $SolicitudInversion->monto_inversion            =   $request->monto_inversion;
            $SolicitudInversion->actividad_eco_emp          =   $request->actividad_eco_emp;
            $SolicitudInversion->correo_emp_recep           =   $request->correo_emp_recep;
            $SolicitudInversion->cat_sector_inversionista_id       =   $request->cat_sector_inversionista_id;
            $SolicitudInversion->pag_web_emp_recep          =   (isset($request->pag_web_emp_recep))?$request->pag_web_emp_recep:'';
            $SolicitudInversion->nombre_repre_legal         =   (isset($request->nombre_repre))?$request->nombre_repre:'';
            $SolicitudInversion->apellido_repre_legal       =   (isset($request->apellido_repre))?$request->apellido_repre:'';
            $SolicitudInversion->especif_otro_sector       =   (isset($request->especif_otro_sector))?$request->especif_otro_sector:'';
            $SolicitudInversion->especif_otro_actividad       =   (isset($request->especif_otro_actividad))?$request->especif_otro_actividad:'';
            $SolicitudInversion->cat_tipo_usuario_id        =   $request->cat_tipo_usuario_id;
            $SolicitudInversion->estado_observacion         =   0;
            $SolicitudInversion->observacion_inversionista  =   '';
            $SolicitudInversion->observacion_anulacion      =   '';
            $SolicitudInversion->observacion_doc_incomp     =   '';
            $SolicitudInversion->updated_at                 =   date("Y-m-d H:i:s");
            $SolicitudInversion->save();
            
        }


         //Guardar las imagenes
         $destino = 'img/inversionistaExt/'.Auth::user()->detUsuario->cod_empresa;
         $destinoPrivado = public_path($destino);
         
         if (!file_exists($destinoPrivado)) {
             $filesystem = new Filesystem();
             $filesystem->makeDirectory($destinoPrivado, $mode = 0777, true, true); 
         }
 
         
         for( $i=1; $i <=10 ; $i++ ){
 
             $imagen = $request->file('file_'.$i);
             
             if (!empty($imagen)) {
                 
                 $nombre_imagen = $imagen->getClientOriginalName();
                 
                 $imagen->move($destinoPrivado, $nombre_imagen);
                 $auxStr='file_'.$i;
                 $$auxStr = $nombre_imagen;
                
             }
             
 
         }
           
         
             $doCertif                       =  GenDocumentosInversiones::find($request->file_id);
            
             if(!empty($file_1)){
                 $auxFile1= $doCertif->file_1;
                 $doCertif->file_1 = $destino.'/'.$file_1;
             }
             if(!empty($file_2)){
                 $auxFile2= $doCertif->file_2;
                 $doCertif->file_2 = $destino.'/'.$file_2;
             }
             if(!empty($file_3)){
                 $auxFile3= $doCertif->file_3;
                 $doCertif->file_3 = $destino.'/'.$file_3;
             }
             if(!empty($file_4)){
                 $auxFile4= $doCertif->file_4;
                 $doCertif->file_4 = $destino.'/'.$file_4;
             }
             if(!empty($file_5)){
                 $auxFile5= $doCertif->file_5;
                 $doCertif->file_5 = $destino.'/'.$file_5;
             }
             if(!empty($file_6)){
                 $auxFile6= $doCertif->file_6;
                 $doCertif->file_6 = $destino.'/'.$file_6;
             }
             if (!empty($file_7)) {
                $auxFile7= $doCertif->file_7;
                 $doCertif->file_7 = $destino.'/'.$file_7;
            }
            if (!empty($file_8)) {
                $auxFile8= $doCertif->file_8;
                 $doCertif->file_8 = $destino.'/'.$file_8;
            }
            if (!empty($file_9)) {
                $auxFile9= $doCertif->file_9;
                 $doCertif->file_9 = $destino.'/'.$file_9;
            }
            if (!empty($file_10)) {
                $auxFile10= $doCertif->file_10;
                 $doCertif->file_10 = $destino.'/'.$file_10;
            }
           
             
             if($doCertif->touch()){
                 if(!empty($auxFile1)){
                     if(is_file('.'.$auxFile1))
                     unlink('.'.$auxFile1); //elimino el fichero
                 }
                 if(!empty($auxFile2)){
                     if(is_file('.'.$auxFile2))
                     unlink('.'.$auxFile2); //elimino el fichero
                 }
                 if(!empty($auxFile3)){
                     if(is_file('.'.$auxFile3))
                     unlink('.'.$auxFile3); //elimino el fichero
                 }
                 if(!empty($auxFile4)){
                     if(is_file('.'.$auxFile4))
                     unlink('.'.$auxFile4); //elimino el fichero
                 }
                 if(!empty($auxFile5)){
                     if(is_file('.'.$auxFile5))
                     unlink('.'.$auxFile5); //elimino el fichero
                 }
                 if(!empty($auxFile6)){
                     if(is_file('.'.$auxFile6))
                     unlink('.'.$auxFile6); //elimino el fichero
                 }
                 if(!empty($auxFile7)){
                    if(is_file('.'.$auxFile7))
                    unlink('.'.$auxFile7); //elimino el fichero
                }
                if(!empty($auxFile8)){
                    if(is_file('.'.$auxFile8))
                    unlink('.'.$auxFile8); //elimino el fichero
                }
                if(!empty($auxFile9)){
                    if(is_file('.'.$auxFile9))
                    unlink('.'.$auxFile9); //elimino el fichero
                }
                if(!empty($auxFile10)){
                    if(is_file('.'.$auxFile10))
                    unlink('.'.$auxFile10); //elimino el fichero
                }
         
             }else{
                 if (!empty($file_1)) {
                     if(is_file('.'.$destino.'/'.$file_1))
                     unlink('.'.$destino.'/'.$file_1);
                 }
                 
                 if (!empty($file_2)) {
                     if(is_file('.'.$destino.'/'.$file_2))
                     unlink('.'.$destino.'/'.$file_2);
                 }
                 
                 if (!empty($file_3)) {
                     if(is_file('.'.$destino.'/'.$file_3))
                     unlink('.'.$destino.'/'.$file_3);
                 }
         
                 if (!empty($file_4)) {
                     if(is_file('.'.$destino.'/'.$file_4))
                     unlink('.'.$destino.'/'.$file_4);
                 }
         
                 if (!empty($file_5)) {
                     if(is_file('.'.$destino.'/'.$file_5))
                     unlink('.'.$destino.'/'.$file_5);
                 }
         
                 if (!empty($file_6)) {
                     if(is_file('.'.$destino.'/'.$file_6))
                     unlink('.'.$destino.'/'.$file_6);
                 }
                 if (!empty($file_7)) {
                    if(is_file('.'.$destino.'/'.$file_7))
                    unlink('.'.$destino.'/'.$file_7);
                }
                if (!empty($file_8)) {
                    if(is_file('.'.$destino.'/'.$file_8))
                    unlink('.'.$destino.'/'.$file_8);
                }
                if (!empty($file_9)) {
                    if(is_file('.'.$destino.'/'.$file_9))
                    unlink('.'.$destino.'/'.$file_9);
                }
                if (!empty($file_10)) {
                    if(is_file('.'.$destino.'/'.$file_10))
                    unlink('.'.$destino.'/'.$file_10);
                }
                   
         
                 Alert::warning('No es posible realizar su solicitud. Intente nuevamente.','Acción Denegada!')->persistent("OK");
                 return redirect()->back()->withInput();
             }
             //dd($request->all());
            if(isset($request->file)){
                 for($i=0; $i <= count($request->file)-1; $i++){

                    $imagen = $request->file[$i];
                   
                    if (!empty($imagen)) {
                            
                        $nombre_imagen = $imagen->getClientOriginalName();
                    
                        $imagen->move($destinoPrivado, $nombre_imagen);
                        $doc = new DocumentsVariosInversiones;
                        $doc->cat_documentos_id = $request->cat_documentos_id[$i];
                        $doc->gen_sol_inversionista_id=$id;
                        $doc->file =$destino.'/'.$nombre_imagen;
                        $doc->save();
                    }
                    
                }
            }
                       

        Alert::success('Actualización Exitosa!', 'Solicitud Actualizada.')->persistent("OK");
        return redirect()->action('InversionistaExtController@index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\GenSolicitud  $genSolicitud
     * @return \Illuminate\Http\Response
     */
    public function destroy(GenSolicitud $genSolicitud)
    {
        
    }

    public function eliminarAccionista(Request $request,$id)
    {
        //dd($_GET['id']);
       $eli_accionista=GenAccionistaInversionista::where('accionistas_id',$_GET['id'])->delete();

       $delete=Accionistas::find($_GET['id'])->delete();

       if($eli_accionista)
       {
        //dd(1);
          return "Accionista Eliminado";
       }else
       {
         return "Accionista No existe";
       }

       if($delete)
       {
        //dd(1);
          return "Accionista Eliminado";
       }else
       {
         return "Accionista No existe";
       }



    }

    //Metodo para elegir el tipo de usuario Inversionista
     public function RedireccionarForm(Request $request)
    {
        //dd($request->all());
        
        $titulo= 'Certificado de Origen';
        $descripcion= 'Perfil del Exportador';/* nsantos*/

       
         //dd($accionistas);
        //$nacionalidad=['V'=>'V','E'=>'E'];

        $datos=DetUsuario::where('gen_usuario_id',Auth::user()->id)->where('bactivo',1)->first();
        //dd($datos);
        $tipousuario=CatTipoUsuario::where('bactivo',1)->pluck('nombre','id');

        $pais=Pais::where('bactivo',1)->pluck('dpais','id');

       // $tipovisa=CatTipoVisa::where('bactivo',1)->whereIn('id',[1,2,3])->pluck('nombre_visa','id');

        $tipodoc=CatTipoVisa::where('bactivo',1)->whereIn('id',[1,2,3])->pluck('nombre_visa','id');
        
        //Consulta para pintar el select los sectores de la Actividad////////// Económica//////////////////////////////////////////////////////////////
        $sectorActEco=CatSectorInversionista::where('bactivo',1)->pluck('nombre','id');
        ///////////////////////////////////////////////////////////////////////////////

        $divisas=GenDivisa::orderBy('ddivisa_abr','asc')->pluck('ddivisa_abr','id');

        if (!empty($request->cat_tipo_usuario_id)) {
          
            Session::put('cat_tipo_usuario_id',$request->cat_tipo_usuario_id);
        }

        
        $cat_tipo_usuario_id=$request->cat_tipo_usuario_id ? $request->cat_tipo_usuario_id : Session::get('cat_tipo_usuario_id') ;
       // dd($cat_tipo_usuario_id);
        

        if ($cat_tipo_usuario_id == 1 ) {
           return view('BandejaInversionista.create_natural',compact('datos','tipousuario','pais','cat_tipo_usuario_id','sectorActEco','divisas'));
        } else {
            return view('BandejaInversionista.create_juridico',compact('datos','tipousuario','pais','cat_tipo_usuario_id','sectorActEco','divisas'));
        }
       
       /* if ($cat_tipo_usuario_id == 1) {
           $vista='BandejaInversionista.create_natural';
        }else{
           $vista='BandejaInversionista.create_juridico';
        }19

     return view($vista, compact('datos','tipousuario','pais','cat_tipo_usuario_id'));*/

    }

    //Metodo para eliminar Lista Dinamica
     public function eliminarSoporte(Request $request)
    {

         //dd($_GET['id']);
       $delete=DocumentsVariosInversiones::find($_GET['id'])->delete();
       if($delete)
       {
        //dd(1);
          return "Documento Eliminado";
       }else
       {
         return "Documento No existe";
       }


    }


    //Metodo pra select Sector actividad economica Dinamico de Inversiones
    protected function ActividadEcoEmpnIvert (Request $request, $id){
        if ($request->ajax()) {
            $actividadeco=CatActividadEcoInversionista::actividadecoInvert($id);
            return response()->json($actividadeco);
        }
    }

}
