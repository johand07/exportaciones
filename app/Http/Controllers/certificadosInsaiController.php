<?php

namespace App\Http\Controllers;

use App\Models\DetUsuario;
use GuzzleHttp\Client;
// use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\ClientException;
use Session;
use Auth;
use Laracasts\Flash\Flash;
use Alert;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;

class certificadosInsaiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $titulo="INSAI";
            $descripcion="Certificados Fitosanitarios Vegetal.";
            $token = '090042cad6357ab5dd0a720c18b53b8e53d4c275';
            $user = DetUsuario::where('gen_usuario_id', Auth::user()->id)->where('bactivo',1)->select('rif')->first();
            if (!empty($user)) {
                
                $rif = $user->rif;
                if (strpos($rif, '-') !== false) {
                    $rifGuion = explode("-", $rif);
                    $rif = $rifGuion[0].$rifGuion[1];
                }
            }
            // dd($rif);
            $headers = [
                'Authorization' => 'Bearer ' . $token,        
                'Accept'        => 'application/json',
            ];
            $body = [
                'rif' => $rif
            ];

            $client = new Client();
            $response = $client->request('POST', 'http://certificados.insai.gob.ve/apiv1/vegetal/', [
                'headers' => $headers,'body'=> json_encode($body)
            ]);
            $datos = json_decode($response->getBody()->getContents(), true);
            // dd($datos['result']);
            $getDataInsai = [];
            if($datos['result'] == "") {
                $datosApi = collect($getDataInsai);
            } else {
                $datosApi = collect($datos['result']);
            }

            return view('certificadosInsai.fitosanitario.index',compact('titulo','descripcion','datosApi'));
        } catch (ClientException $err) {
            Log::error($err);
            Alert::success('Servicio de Insai No responde!','Intente mas tarde!')->persistent("Ok");

            return redirect()->action('ExportadorController@index');
        }
    }

    
    public function getDataInsai()
    {
        try {
            $titulo="INSAI";
            $descripcion="Certificados_Fitosanitarios.";
            $token = '090042cad6357ab5dd0a720c18b53b8e53d4c275';
            $user = DetUsuario::where('gen_usuario_id', Auth::user()->id)->where('bactivo',1)->select('rif')->first();
            if (!empty($user)) {
                
                $rif = $user->rif;
                if (strpos($rif, '-') !== false) {
                    $rifGuion = explode("-", $rif);
                    $rif = $rifGuion[0].$rifGuion[1];
                }
            }
            // dd($rif);
            $headers = [
                'Authorization' => 'Bearer ' . $token,        
                'Accept'        => 'application/json',
            ];
            $body = [
                'rif' => $rif
            ];

            $client = new Client();
            $response = $client->request('POST', 'http://certificados.insai.gob.ve/apiv1/vegetal/', [
                'headers' => $headers,'body'=> json_encode($body)
            ]);
            $data = json_decode($response->getBody()->getContents(), true);

            // dd(json_decode($data, true));
            // return $response->json();
            return response()->json($data);
            
            // $dataJson = response()->json($datos);
            // $data = $dataJson->data->result;
            // dd($data);
            // return view('certificadosInsai.index',compact('datos','titulo','descripcion'));
        } catch (\Expection $err) {
            Log::error($err);
            return response()->json([
                'success' => false,
                'message' => 'Error',
                'data' => $err,
            ]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\DetUsuario  $detUsuario
     * @return \Illuminate\Http\Response
     */
    public function show(DetUsuario $detUsuario)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\DetUsuario  $detUsuario
     * @return \Illuminate\Http\Response
     */
    public function edit(DetUsuario $detUsuario)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\DetUsuario  $detUsuario
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DetUsuario $detUsuario)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\DetUsuario  $detUsuario
     * @return \Illuminate\Http\Response
     */
    public function destroy(DetUsuario $detUsuario)
    {
        //
    }
}
