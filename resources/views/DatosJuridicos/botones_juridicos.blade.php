@extends('templates/layoutlte')
@section('content')

@php  use App\Models\DetUsuario; 
        
    $data=DetUsuario::where('gen_usuario_id',Auth::user()->id)->first();

@endphp
 <div class="row">
            <div class="col-md-12 text-center">
                 <h3 class="section-title h3">LISTA DEL EXPORTADOR</h3>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 text-right"><a href="{{action('PdfController@CertificadoRegistroVuce')}}" class="btn btn-danger">Descargar Certificado Registro VUCE</a>
            </div>
        </div><br>
<section id="team" class="pb-5">
    <div class="content">
        <div class="row">
            <!-- Team member -->
            <div class="col-xs-12 col-sm-6 col-md-3">
                <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                    <div class="mainflip">
                        <div class="frontside">
                            <div class="card">
                                <div class="card-body text-center">
                                    <br><br><br>
                                    <p><img class=" img-fluid" src="/img/datos_basic.png" style="width: 80px; height: 65px;"></p>
                                    <h4 class="card-title">Datos Básicos</h4>
                                    <!--<p class="card-text">Gestión de Médicos</p>-->
                                    <a href="#" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="backside">
                            <div class="card">
                                <div class="card-body text-center mt-3">
                                      <br><br><br>
                                    <h4 class="card-title">Datos Básicos</h4>
                                    <p class="card-text">Podrá actualizar los datos basicos de su registro.</p>
                                    <a href="{{route('DatosJuridicos.edit',Auth::user()->id)}}" class="btn btn-primary" style="color:#FFFFFF;">DATOS BÁSICOS</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ./Team member -->
            <!-- Team member -->
            <div class="col-xs-12 col-sm-6 col-md-3">
                <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                    <div class="mainflip">
                        <div class="frontside">
                            <div class="card">
                                <div class="card-body text-center">
                                    <br><br><br>
                                    <p><img class=" img-fluid" src="/img/registro_mer.png" style="width: 90px; height: 65px;"></p>
                                    <h4 class="card-title">Registro Mercantil</h4>
                                    <a href="#" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="backside">
                            <div class="card">
                                <div class="card-body text-center mt-4">
                                     <br><br><br>
                                    <h4 class="card-title">Registro Mercantil</h4>
                                    <p class="card-text">Podrá actualizar los datos del registro mercantil.</p>
                                    <a href="{{route('RegisMercantil.edit',Auth::user()->id)}}" class="btn btn-primary" style="color:#FFFFFF;">DATOS DEL REGISTRO <br>MERCANTIL</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ./Team member -->
            <!-- Team member -->
            <div class="col-xs-12 col-sm-6 col-md-3">
                <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                    <div class="mainflip">
                        <div class="frontside">
                            <div class="card">
                                <div class="card-body text-center">
                                    <br><br><br>
                                    <p><img class=" img-fluid" src="/img/accionistas.png" style="width: 80px; height: 65px;"></p>
                                    <h4 class="card-title">Accionistas</h4>
                                    <a href="#" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="backside">
                            <div class="card">
                                <div class="card-body text-center mt-4">
                                    <br><br><br>
                                    <h4 class="card-title">Accionistas</h4>
                                    <p class="card-text">Podrá actualizar los datos de los accionistas.</p>
                                    <a href="{{route('DatosAccionista.edit',Auth::user()->id)}}" class="btn btn-primary" style="color:#FFFFFF;">DATOS DE LOS <br>ACCIONISTAS</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ./Team member -->
            <!-- Team member -->
            <div class="col-xs-12 col-sm-6 col-md-3">
                <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                    <div class="mainflip">
                        <div class="frontside">
                            <div class="card">
                                <div class="card-body text-center">
                                    <br><br><br>
                                    <p><img class=" img-fluid" src="/img/registro_mer.png" style="width: 90px; height: 65px;"></p>
                                    <h4 class="card-title">Casa Matriz</h4>
                                    <a href="#" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="backside">
                            <div class="card">
                                <div class="card-body text-center mt-4">
                                     <br><br><br>
                                    <h4 class="card-title">Casa Matriz</h4>
                                    <p class="card-text">Podrá actualizar los datos de la casa matriz de la exportación.</p>
                                    <a href="{{route('CasaMatriz.edit',Auth::user()->id)}}" class="btn btn-primary" style="color:#FFFFFF;">DATOS DE LA <br>CASA MATRIZ</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ./Team member -->
            <!-- Team member -->
            <div class="col-xs-12 col-sm-6 col-md-3">
                <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                    <div class="mainflip">
                        <div class="frontside">
                            <div class="card">
                                <div class="card-body text-center">
                                     <br><br><br>
                                    <p><img class=" img-fluid" src="/img/ult_exportacion.png" style="width: 80px; height: 65px;"></p>
                                    <h4 class="card-title">Última Exportación</h4>                             
                                    <a href="#" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="backside">
                            <div class="card">
                                <div class="card-body text-center mt-4">
                                     <br><br><br>
                                    <h4 class="card-title">Última Exportación </h4>
                                    <p class="card-text">Podrá actualizar los datos de la ultima exportación.</p>
                                    <a href="{{route('UltimaExportacion.edit',Auth::user()->id)}}" class="btn btn-primary" style="color:#FFFFFF;">DATOS  DE LA ÚLTIMA <br> EXPORTACIÓN</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ./Team member -->
            <!-- Team member -->

 

       @if($data->produc_tecno==2)

            <div class="col-xs-12 col-sm-6 col-md-3">
                <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                    <div class="mainflip">
                        <div class="frontside">
                            <div class="card">
                                <div class="card-body text-center">
                                     <br><br><br>
                                    <p><img class=" img-fluid" src="/img/productos.png" style="width: 80px; height: 65px;"></p>
                                    <h4 class="card-title">Actualizar Productos </h4>                             
                                    <a href="#" class="btn btn-primary "><i class="fa fa-pencil"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="backside">
                            <div class="card">
                                <div class="card-body text-center mt-4">
                                    <br><br><br>
                                    <h4 class="card-title">Actualizar Productos </h4>
                                    <p class="card-text">Podrá actualizar los productos de la exportación.</p>
                                    <a href="{{route('productos_export.edit',Auth::user()->id)}}" class="btn btn-primary" style="color:#FFFFFF;">ACTUALIZAR <br> PRODUCTOS</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
  @endif
    
            <div class="col-xs-12 col-sm-6 col-md-3">
                <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                    <div class="mainflip">
                        <div class="frontside">
                            <div class="card">
                                <div class="card-body text-center">
                                     <br><br><br>
                                    <p><img class=" img-fluid" src="/img/datos_acceso.png" style="width: 80px; height: 65px;"></p>
                                    <h4 class="card-title">Datos de Acceso</h4>
                                    <a href="#" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="backside">
                            <div class="card">
                                <div class="card-body text-center mt-3">
                                      <br><br><br>
                                    <h4 class="card-title">Datos de Acceso</h4>
                                    <p class="card-text">Podrá actualizar los datos de acceso para ingresar al sistema.</p>
                                    <a href="{{route('DatosAcceso.edit',Auth::user()->id)}}" class="btn btn-primary" style="color:#FFFFFF;">DATOS DE <br>ACESSO</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ./Team member -->

                <div class="col-xs-12 col-sm-6 col-md-3">
                <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                    <div class="mainflip">
                        <div class="frontside">
                            <div class="card">
                                <div class="card-body text-center">
                                     <br><br><br>
                                    <p><img class=" img-fluid" src="/img/DocFile.jpeg" style="width: 80px; height: 65px;"></p>
                                    <h4 class="card-title">Documentos Soporte </h4>
                                    <a href="#" class="btn btn-primary"><i class="fa fa-file"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="backside">
                            <div class="card">
                                <div class="card-body text-center mt-3">
                                      <br><br><br>
                                    <h4 class="card-title">Documentos Soporte </h4>
                                    <p class="card-text">Podrá cargar y actualizar sus Documentos (CI,Rif,Registro Mercantil).</p>
                                    <a href="{{route('DocumentosSoportes.create',Auth::user()->id)}}" class="btn btn-primary" style="color:#FFFFFF;">Documentos</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>
</section>


@stop