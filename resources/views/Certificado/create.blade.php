@extends('templates/layoutlte')
@section('content')

<div class="container">
  {{Form::open(['route' =>'productos.store' ,'method'=>'POST','id'=>'FormProd','name'=>'add_product','onSubmit="validarProductosFactura(); return false"'])}}

  {{Form::hidden('metodo','Crear')}}
  <div class="row">

    <h3 align="center">{{$descripcion}}</h3>

    <table class="table table-bordered" id="fields">

      <tr>
      <th>Código Arancelario </th>
      <th>Descripcion Arancelaria</th>
      <th>Descripcion Comercial</th>
      <th>Cantidad </th>
      <th>Precio </th>
      <th>Unidad de Medida</th>
      {{--<th>Permiso</th>--}}

      </tr>



      <tr id="row">

        <td>{!!Form::text('codigo_arancel[]',null,['class'=>'form-control','data-toggle'=>"modal",'data-target'=>"#modal",'id'=>'cod_arancel','onkeypress'=>'return soloNumerosDouble(event)'])!!}
        </td>
        <td>{!!Form::text('descripcion_arancelaria[]',null,['class'=>'form-control','id'=>'arancel_descrip','onkeypress'=>'return soloLetras(event)'])!!}</td>
        <td>{!!Form::text('descripcion_comercial[]',null,['class'=>'form-control','onkeypress'=>'return soloLetras(event)'])!!}</td>
        <td>{!!Form::text('cantidad_producto[]',null,['class'=>'form-control','id'=>'cantidad_producto','onkeypress'=>'return soloNumeros(event)'])!!}</td>
        <td>{!!Form::text('precio_producto[]',null,['class'=>'form-control','precio_producto','onkeypress'=>'return soloNumerosDouble(event)'])!!}</td>
        <td>{!!Form::select('unidad_medida_id[]',$medidas,null,['class'=>'form-control','id'=>'unidad_medida_id','placeholder'=>'Seleccione'])!!}</td>

        {{--<td>{!!Form::select('permiso_id[]',$permisos,null,['class'=>'form-control','placeholder'=>'Seleccione','id'=>'permiso'])!!}</td>--}}

        <td><button  name="add" type="button" id="add" value="add more" class="btn btn-success">Agregar</button></td>
      </tr>

    </table>
    <div class="col-md-12" align="center">
      <input type="submit" name="submit" id="submit" class="btn btn-info"  value="Enviar"/>
    </div>
  </div>
</div>
{{Form::close()}}


@component('modal_dialogo',['arancel'=>$arancel])

    @slot('header') <span>Código Arancelario</span> @endslot

    @slot('body')  <div id="arancel"></div> @endslot

    @slot('footer')@endslot

@endcomponent




 <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>



<script type="text/javascript">

$(document).ready(function (){

var i = 0;
      $('#add').click(function(){
       i++;

$('#fields tr:last').after('<tr id="row'+i+'" display="block" class="show_div"><td>{!!Form::text('codigo_arancel[]',null,['class'=>'form-control','data-toggle'=>"modal",'data-target'=>"#modal",'id'=>"cod_arancel",'onkeypress'=>'return soloNumerosDouble(event)'])!!}</td><td>{!!Form::text('descripcion_arancelaria[]',null,['class'=>'form-control','id'=>'arancel_descrip'])!!}</td><td>{!!Form::text('descripcion_comercial[]',null,['class'=>'form-control','onkeypress'=>'return soloLetras(event)'])!!}</td><td>{!!Form::text('cantidad_producto[]',null,['class'=>'form-control','onkeypress'=>'return soloNumeros(event)'])!!}</td><td>{!!Form::text('precio_producto[]',null,['class'=>'form-control'])!!}</td><td>{!!Form::select('unidad_medida_id[]',$medidas,null,['class'=>'form-control','placeholder'=>'Seleccione','id'=>'unidad_medida_id'])!!}</td><td><button  name="remove" type="button" id="'+i+'" value="" class="btn btn-danger btn-remove">x</button></td></tr>');




    });

      $(document).on('click','.btn-remove',function(){
        var id_boton= $(this).attr("id");
        $("#row"+id_boton+"").remove();

      });




 });


</script>



@stop
