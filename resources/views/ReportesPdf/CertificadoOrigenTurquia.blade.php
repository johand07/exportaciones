<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>CERTIFICADO DE ORIGEN TURQUIA</title>
    <style type="text/css">
        .centrar{
            text-align: center;
        }

        .titulo{
            
            font-weight: bold;
        }

        .nro-correlativo{
            color: red;
            font-size: 16px;
        }

        .derecha{
            text-align: right;

        }

        .text-normal{
            font-size: 14px;
        }

        .text-medio{
            font-size: 12px;
        }
        .text-11{
            font-size: 11px;
        }
        .text-small{
            font-size: 10px;
        }

        .text-9{
            font-size: 9px;
        }

        .text-8{
            font-size: 8px;
        }

        .espacio-superior{
            margin-top: 10px;
        }

        .espacio-superior-small{
            margin-top: 5px;
        }

        .ancho-medio{
            width: 50%;
        }

        .ancho-full{
            width: 100%;
        }

        .ancho-medio-alto{
            width: 75%;
        }

        .ancho-90{
            width: 90%;
        }

        .table{
            border-collapse: collapse;
        }
        
        .borde{
            border: 1px solid #000;
        }

        .sin-borde{
            border: none;
        }

        .top{
            vertical-align: top;
        }

        .under-dotted{    
            border-bottom: 1px dotted #000;
            text-decoration: none;
        }

        .content-1{
            width: 5%;
        }

        .content-9{
            width: 20%;
        }

        .sangria-1{
            margin-left: 28px;
        }

        .sangria-2{
            margin-left: 36px;
        }

        .sangria-3{
            margin-left: 54px;
        }

        .sangria-sub{
            margin-left: 14px;
            
        }

        
        .page-break { page-break-before: always; }

        .table-center{
            margin: 0 auto; 
        }

        .justify{
            text-align: justify;
        }

        td.justify {
            padding-left: 8px;
            padding-right: 8px; 
        }
        .lineal{
            
        }
        .lineal>div{
            max-width:91%;

        }
        .lineal>*{
           display: inline-block;
           vertical-align:top;
        }

        .sangria-lista{
            list-style-type: none;
            margin: 0;
            padding: 0;
            margin-bottom: 4px;
        }

        .sangria-lista>li{
            margin-top: 5px;
            padding-left: 10px;
            padding-right: 10px;
        }
        .table2{
            border: 0px solid #E2E6E9;
        }

        {--texto-vertical-2 {
            float: left; 
             position: relative; 
             -moz-transform: rotate (270deg);/* FF3.5 + */
             -o-transform: rotate (270deg);/* Opera 10.5 */
             -webkit-transform: rotate (270deg);/* Saf3.1 +, Chrome */
             filter: progid: DXImageTransform.Microsoft.BasicImage (rotación = 3);/* IE6, IE7 */
             -ms-filter: progid: DXImageTransform.Microsoft.BasicImage (rotación = 3);/* IE8 */
       }--}

        .izq{
          margin: 2px;
          text-align: left;
        }

        .der{
          margin: auto;
          text-align: right;
        }
        .borde_blaco{
            color:#ffffff;
        }
    </style>
</head>
<body>          

</head>
<body>          
<div class="centrar titulo text-14 espacio-superior">MOVEMENT CERTIFICATE</div>
<!--- Primera parte-->
<div class="centrar espacio-superior-small">
    <table class="ancho-full borde table">
         <tr class="text-9">
            <td class="top titulo borde" colspan="50">1.<b>Exporter</b> (Name, full address, country)<br>{{ $certificado->GenUsuario->DetUsuario->razon_social }} {{ $certificado->GenUsuario->DetUsuario->direccion }} {{$certificado->pais_cuidad_exportador}} {{ $certificado->GenUsuario->DetUsuario->rif }}</td><br>
            <td class="borde" colspan="50"><div class="centrar"><b>EUR.1 No A</b><b style="color:red" class="left">&nbsp;&nbsp;00452</b>
            </div><div class="titulo borde centrar" colspan="50">See notes overleaf before completing this form<br><br></div> 2.<b>Certificate used in preferential trade between :</b>
            <p class="centrar">{{ $certificado->pais_exportador }} </p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;............................................................................
            &nbsp;&nbsp;&nbsp;&nbsp;<br>
            <p class="centrar  text-normal"><b>and</b></p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;...........................................................................<br>
            <p class="centrar">{{ $certificado->pais_importador }}</p>
            <div class="text-9 centrar"><b>(Insert appropriate countries, groups of countries or territories)</b></div>
            </td>

        </tr>
        <!--Parte 3 y 4,5-->
        <tr class="text-9">
            <td class="top titulo borde justify" colspan="50">3.Consignee(Name,full address,country)(Optional)<br><br><br><br>{{ ($importadorCertificado->razon_social_importador) }} {{ ($importadorCertificado->direccion_importador) }} <br><br></td>
            <td class="top titulo borde justify" colspan="25">4.Country, group of countries or territory in which the products are considered as origininathing<br><br>{{ $certificado->pais_cuidad_exportador }}
            </td>
            <td class="top titulo borde justify" colspan="25">5.Country, group of countries or territory in which the products are considered as origininathing  <br><br>{{ ($importadorCertificado->pais_ciudad_importador) }}</td>
        </tr>
        <tr class="text-9">
            <td class="top titulo borde" colspan="50">6. Transport details (Optional) <br>{{ ($importadorCertificado->medio_transporte_importador) }}<br></td>
            <td class="top titulo borde" colspan="50">7.Remarks <br>{{$certificado->observaciones}}<br><br></td>
        </tr>
      
    
        <tr class="text-9">
            <td class="top titulo borde justify" colspan="50">8.Item number;Marks and numbers; Number and kind of packages; Description of goods(1) <br></td>
            <td class="top titulo borde justify" colspan="25">9.Gross mass (kg) or other measure (litres,m3,etc.)<br></td>
            <td class="top titulo borde justify" colspan="25">10.Invoices(2)<br>
            </td>
        </tr>
        @foreach($detalleProduct as $key => $value)
        <tr class="text-11">
                <td class="top borde" align="center" colspan="50">{{$value->codigo_arancel}} {{$value->denominacion_mercancia}}<br><br></td>
                <td class=" borde" colspan="25">{{$value->unidad_fisica}}<br><br></td>
                @if($key ==0)
                <br>
                 <td class="top titulo justify" colspan="25">{{ $detdeclaracionCert[0]->numero_factura }} {{ $detdeclaracionCert[0]->f_fac_certificado }}</td>
                @else
                <td class="top titulo justify" colspan="25"></td>
                @endif
            </tr>
        @endforeach
        <!--parte 11 y 12-->
        <tr class="text-9">
            <td class="top borde" colspan="65"><b>11.COMPETENT AUTHORITY ENDORSEMENT</b><br>Declaration certified<br>Export Document (3)<br>
             <div>Form  <b>VENEZUELA</b>............No {{$certificado->num_certificado}}.........</div>
             <div>Of {{$certificado->fecha_emision_exportador}}..........................</div> 
             <div>Competent Authority {{$datosfirmante->ente_firmante }} ...........................</div>
             <div>Issuing country or territory  &nbsp;&nbsp;CARACAS....................</div>
             ..........................................................................&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="img/circulo.png" style="width:80px; height:50px;" class="text-left" style="padding-left: 40px:">
             <div>Place and date ....................{{$certificado->created_at}}</div><br>
             ..........................................................................
             <div>(Signature)</div>
            </td>
            <td class="top borde" colspan="35"><b>12.DECLARATION BY THE EXPORTER</b><br>1.the undersigned, declare that the goods described aove meet the conditions required for the issue of this certificate. <br><br>
            <div>Place and date</div><br>
            .........................................................................................
            <br><br><br><br>
            .........................................................................................
            <div>&nbsp;&nbsp;&nbsp;&nbsp;(Signature)</div>
            </td>
        </tr>
<!--Parte 13 y 14-->
        <tr class="text-9">
            <td class="top borde" colspan="42"><b>13.REQUEST FOR VERIFICATION, to</b><br><br><h3 style="font-size:14px;" class=""><b>Servicio Nacional Integrado de Administración Aduanera y Tributaria (SENIAT)</b></h3>
                __________________________________________________________________________________
                <br><div class="top">Verification of the authenticity and accuracy of this certificate is requested<br><br><br>.......................................................................................................
            <p class="centrar">(Place and date)   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="img/circulo.png" style="width:80px; height:60px;" class="text-left" style="padding-left: 40px:"></p>
            ........................................................
            <br><p>&nbsp;&nbsp;&nbsp;&nbsp;(Signature)</p>
          </div>
            </td>
            <td class="top borde" colspan="58"><b>14.RESULT OF VERIFICATION</b><br>
                Verification carried out shows that this certificate(1)<br><p></p>-was issued by the Competent Authority indicated and that the information contained therein is accurate. <br><p>-does not meet the requirements as to authenticity and accuracy (see remarks appended). <br><br><br>
                .............................................................................................................................
                <p class="centrar">(Place and date)</p><br>
                ..........................................................................<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="img/circulo.png" style="width:80px; height:60px;" class="text-left" style="padding-left: 40px:">
               
                <br>
                ...........................................................................
                <br>
                 &nbsp;&nbsp;(Signature)
                 <br>
                <p>(1)Insert X in the appropriate box.</p>

            </p></td>
        </tr>

 </table>
   <br><!--img src="img/circulo.png" style="width:130px; height:50px;" class="text-left"-->
</div>
<!--- Primera parte-->
<!--------------------Parte de las normas--->
<div class="page-break"></div>
________________________________________
<div class="text-9">(1) If goods are not packed, indicate number of articles or state « in bulk » as appropriate)</div>
<div class="text-9">(2) In the case of originating goods invoiced by a third party, it shall be indicated in the movement certificate EUR.1 that the goods will be invoiced by that third party and also the name and the address of the person/legal person in third party who will draw up the final invoice for the country of destination.</div>
<div class="text-9">(3) Complete only where the regulations of the exporting country or territory require.</div>
<div class="centrar"><h5>NOTES</h5></div>
<div class="justify text-normal" style="font-family:Times New Roman;">1)Certificate must not contain erasures or words written over one another. Any alterations must be made by deleting the incorrect particulars and adding any necessary corrections. Any such alteration must be initialled by the person who completed the certificate and endorsed by the Competent Authority of the issuing country or territory.</div><br>
<div class="justify text-normal" style="font-family:Times New Roman;">2)No spaces must be left between the items entered on the certificate and each item must be preceded by an item number. A horizontal line must be drawn immediately below the last item. Any unused space must be struck through in such a manner as to make any later additions impossible.</div>
<div class="centrar"><h5>SECTION IV: MOVEMENT CERTIFICATE EUR.1</h5></div>
<div class="justify text-normal" style="font-family:Times New Roman;"><b>Article 16: Procedure for issuing the Movement Certificate EUR.1</b>
For the purposes of paragraph 1, the exporter or his authorised representative shall complete the EUR.1 movement certificate in accordance with the provisions of the national law of the exporting Party, where appropriate. This form will be completed in English. In case it is completed by hand, it will be done with pen and in print. The description of the products shall be indicated in the box reserved for that, without leaving blank lines. When the box is not fully filled, a horizontal low line of the last description line must be drawn so that the empty space is crossed across the line.</div>
<div class="centrar"><h5>APPENDIX 3</h5><p><b>Specimens of Movement Certificate EUR.1</b></p><p><b>Printing Instructions</b></p></div>
<div class="justify text-normal" style="font-family:Times New Roman;">1. Each form shall measure 210 x 297 mm; a tolerance of up to minus 5 mm or plus 8 mm in the length may be allowed. The paper used must be white, sized for writing, not containing mechanical pulp and weighing not less than 25 g/m2. It shall have a printed green guilloche pattern background making any falsification by mechanical or chemical means apparent to the eye.</div><br>
<div class="justify text-normal" style="font-family:Times New Roman;">2. The competent authorities of the contracting parties may reserve the right to print the forms themselves or may have them printed by approved printers. In the latter case, each form must include a reference to such approval. Each form must bear the name and address of the printer or a mark by which the printer can be identified. It shall also bear a serial number, either printed or not, by which it can be identified.</div><br><br><br>
<div><u class="justify text-normal">Official Gazette Of the Bolivarian Republic of Venezuela No. 6,418 Extraordinary Caracas, Thursday 27 December 2018</u></div>

<div style="page-break-after:always;">
</div>
</body>
</html>