<?php

namespace App\Http\Controllers;
use App\Models\GenUsuario;
use App\Models\CasaMatriz;
use App\Models\CatExportProd;
use App\Models\Pais;
use Illuminate\Http\Request;
use Session;
use Auth;
use Laracasts\Flash\Flash;
use Alert;

class CasaMatrizController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('DatosJuridicos.botones_juridicos');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\CasaMatriz  $casaMatriz
     * @return \Illuminate\Http\Response
     */
    public function show(CasaMatriz $casaMatriz)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\CasaMatriz  $casaMatriz
     * @return \Illuminate\Http\Response
     */
    public function edit(CasaMatriz $casaMatriz,$id)
    {
        $titulo="Registro del Exportador";
        $descripcion="Actualización de Casa Matriz";
        $radio=CatExportProd::all();
        $matriz=CasaMatriz::where('gen_usuario_id',$id)->first();
        //dd($matriz);

        //consulto tabla de pais para mostrar Venezuela
            $paises=Pais::orderBy('dpais','asc')->pluck('dpais','id');


    /***************************************************************************/


        return view('DatosJuridicos.edit_casa_matriz',compact('titulo','descripcion','paises','radio','matriz'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\CasaMatriz  $casaMatriz
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CasaMatriz $casaMatriz,$id)
    {
        $cmatriz=CasaMatriz::find($id);
        $cmatriz->casa_matriz=$request->casa_matriz;
        $cmatriz->pais_id=$request->pais_id;
        $cmatriz->save();

        Alert::success('ACTUALIZACIÓN EXISTOSA','Casa Matriz Actualizada')->persistent("ok");
         return redirect()->action('DatosJuridicosController@index');
         
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\CasaMatriz  $casaMatriz
     * @return \Illuminate\Http\Response
     */
    public function destroy(CasaMatriz $casaMatriz)
    {
        //
    }
}
