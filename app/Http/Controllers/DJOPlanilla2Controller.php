<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\GenUnidadMedida;
use App\Models\Productos;
use App\Models\Pais;
use App\Models\DetDeclaracionProduc;
use App\Models\Planilla2;
use App\Models\MaterialesImportados;
use App\Models\MaterialesNacionales;
use App\Http\Requests\DJOPlanilla2Request;
use Auth;
use Alert;
use Session;
use DB;
use View;
use Response;

class DJOPlanilla2Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
     
        return $this->create($request);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

        if(strpos(session('_previous')['url'],"/exportador/DeclaracionJO")!==false)
        session(['_old_input' => null]);
        
        $pais = Pais::orderBy('dpais','asc')->pluck('dpais','id');

        $productos_id= $request['producto_id'];
        $gen_declaracion_jo_id = Session::get('gen_declaracion_jo_id');

        // validamos que exista la declaracion 
        if(empty($gen_declaracion_jo_id)){

          Alert::warning('La solicitud que intenta realizar requiere que este asociada a una Declaracion Jurada de Origen. Seleccione o creela.','Acción Denegada1!')->persistent("OK");

          return redirect()->action('ListaDeclaracionJOController@index'); 
        }


        // consultamos el producto asociado a la declaracion
        $det_declaracion_product=DetDeclaracionProduc::with('rProducto')->with('rPlanilla2')->where('gen_declaracion_jo_id',$gen_declaracion_jo_id)->where('id',$productos_id)->get()->last();

        // validamos que existe el producto asociado a la declaracion
        if(empty($det_declaracion_product->id)){

          Alert::warning('La solicitud que intenta realizar requiere que el producto este asociado a la Declaracion Jurada de Origen. Intente nuevamente.','Acción Denegada!')->persistent("OK");

          return redirect()->action('DeclaracionJOController@index');

        }else{
        
            Session::put('det_declaracion_product_id',$det_declaracion_product->id);
        
        }

        // Verificamos si existe datos en la planilla asociada al producto 
        if(!empty($det_declaracion_product->rPlanilla2->id)){

           return redirect()->action('DJOPlanilla2Controller@edit', ['id' => $det_declaracion_product->rPlanilla2->id]);
        }
        
        $producto = $det_declaracion_product->rProducto;
        $unidadMedida= $producto->unidadMedida;

        $titulo= 'Declaración Jurada de Origen';
        $descripcion= 'Planilla 2';

        return view('djoplanilla2.create',compact('pais','producto','unidadMedida','titulo','descripcion'));  
    }

 
    public function store(DJOPlanilla2Request $request)
    {
         
      DB::beginTransaction();

        $bandera_materiales_importados=false;
        $bandera_materiales_nacionales=false;
        $bandera_cantidad_datos_importados=false;
        $bandera_cantidad_datos_nacionales=false;
        $auxContador = 0;

        $det_declaracion_product_id = Session::get('det_declaracion_product_id');

        if(empty($det_declaracion_product_id)){

            Alert::warning('No es posible realizar su solicitud. Intente nuevamente.','Acción Denegada!')->persistent("OK");

            return redirect()->back()->withInput();
        }

        $planilla2 = Planilla2::with('declaracionProduc')->where('det_declaracion_produc_id',$det_declaracion_product_id)->get()->last();

        if(!empty($planilla2)){

             Alert::error('Usted ya registro datos para la planilla 2 asociada al producto " '.$planilla2->declaracionProduc->rProducto->descrip_comercial.' ". Intente nuevamente.','Acción Denegada!')->persistent("OK");

            return redirect()->action('DeclaracionJOController@index');
        }

        /*****************     Inicializar variables         ************/

        $total_mate_import = 0;
        $total_mate_nac = 0;


        /*****************             ************/

        $uso_aplicacion = $request['uso_aplicacion'];

        if(empty($uso_aplicacion)){
            Alert::warning('Debe indicar el uso y aplicacion del producto. Intente nuevamente.','Acción Denegada!')->persistent("OK");

            return redirect()->back()->withInput();
        }


        /*****************     Obtener datos de materiales importados del formulario.      ************/

        $descripcion_arancelaria_importado = $request->descripcion_arancelaria_importado;
        $codigo_arancel_importado = $request->codigo_arancel_importado;
        $pais_importado = $request->pais_importado;
        $monto_incidencia_importado = $request->monto_incidencia_importado;

        /*****************     Calculo del monto total de materiales importados         ************/
        foreach ($monto_incidencia_importado as $key => $value) {
            $total_mate_import+= ($value);
        }


        /*****************     Obtener datos de materiales nacionales del formulario.          ************/


        $descripcion_arancelaria_nacional= $request->descripcion_arancelaria_nacional;
        $codigo_arancel_nacional = $request->codigo_arancel_nacional;
        $nombre_producto_nacional = $request->nombre_producto_nacional;
        $nombre_proveedor_nacional = $request->nombre_proveedor_nacional;
        $monto_incidencia_nacional = $request->monto_incidencia_nacional;
        $rif_productor= $request->rif_productor;


        /*****************     Calculo del monto total de materiales nacionales         ************/

        foreach ($monto_incidencia_nacional as $key => $value) {
            $total_mate_nac+= ($value);
        }


        /*****************     Validar que los codigos de los materiales no se repitan  ************/

        /*if(count(array_unique($codigo_arancel_importado)) != count($codigo_arancel_importado) ){
            $bandera_materiales_importados=true;
        }

        if(count(array_unique($codigo_arancel_nacional)) != count($codigo_arancel_nacional) ){
            $bandera_materiales_nacionales=true;
        }*/

        if(!empty($bandera_materiales_importados)){

             Alert::warning('No se permite la repeticion de materiales internacionales. Intente nuevamente.','Acción Denegada!')->persistent("OK");

             return redirect()->back()->withInput();
        }

        if(!empty($bandera_materiales_nacionales)){
            dd($bandera_materiales_nacionales);

             Alert::warning('No se permite la repeticion de materiales nacionales. Intente nuevamente.','Acción Denegada!')->persistent("OK");

             return redirect()->back()->withInput();
        }


        /*****************    Validar que los datos esten completos         ************/


        if( count(array_filter($descripcion_arancelaria_importado))  != count(array_filter($codigo_arancel_importado)) || 
            count(array_filter($codigo_arancel_importado))  != count(array_filter($pais_importado) ) || 
            count(array_filter($pais_importado) ) != count(array_filter($monto_incidencia_importado)))
        {

            $bandera_cantidad_datos_importados= true;
        }


        if(  count(array_filter( $descripcion_arancelaria_nacional))  != count(array_filter($codigo_arancel_nacional)) || 
              count(array_filter($codigo_arancel_nacional))  != count(array_filter( $nombre_producto_nacional)) || 
              count(array_filter( $nombre_producto_nacional))  !=  count(array_filter($nombre_proveedor_nacional)) || 
              count(array_filter($nombre_proveedor_nacional))  !=  count(array_filter($monto_incidencia_nacional))
              || count(array_filter( $rif_productor))  !=  count(array_filter($rif_productor)) ){
        
            $bandera_cantidad_datos_nacionales= true;

        }


        if(!empty( $bandera_cantidad_datos_importados)){

            Alert::warning('Datos de materiales importados incompletos. Intente nuevamente.','Acción Denegada!')->persistent("OK");

            return redirect()->back()->withInput();
        }

        if(!empty($bandera_cantidad_datos_nacionales)){

             Alert::warning('Datos de materiales nacionales incompletos. Intente nuevamente.','Acción Denegada!')->persistent("OK");

            return redirect()->back()->withInput();
        }   

        /*****************     Validar que se hayan enviado almenos  un material importado OR un material nacional    ************/

        /*if( count(array_filter($monto_incidencia_importado))==0 && count(array_filter($monto_incidencia_nacional))==0){

            Alert::warning('La solicitud que intenta realizar requiere que indique los materiales utilizados para elaborar su producto. Intente nuevamente.','Acción Denegada!')->persistent("OK");

                    return redirect()->back()->withInput();
        }*/

        /*****************     Guardar datos de la tabla planilla 2        ************/

                $modelPlanilla2 = new Planilla2;
                $modelPlanilla2->det_declaracion_produc_id= $det_declaracion_product_id;
                $modelPlanilla2->uso_aplicacion= $uso_aplicacion;
                $modelPlanilla2->total_mate_import = $total_mate_import;
                $modelPlanilla2->total_mate_nac = $total_mate_nac;
                $modelPlanilla2->bactivo=1;

                if($modelPlanilla2->save()){


        /*****************     Guardar datos en tabla materiales importados        ************/

            foreach ($monto_incidencia_importado as $key => $value) {
                $modelMaterialesImportados = new MaterialesImportados;
                $modelMaterialesImportados->planilla_2_mi_id=$modelPlanilla2->id;
                $modelMaterialesImportados->codigo=$codigo_arancel_importado[$key];
                $modelMaterialesImportados->descripcion=$descripcion_arancelaria_importado[$key];
                $modelMaterialesImportados->pais_id=$pais_importado[$key];
                $modelMaterialesImportados->incidencia_costo=$value;
                //$modelMaterialesImportados->rif_productor=$request->rif_productor[$key];
                $modelMaterialesImportados->bactivo=1;
                $modelMaterialesImportados->save();
            }


        /*****************     Guardar datos en tabla materiales nacionales        ************/

            foreach ($monto_incidencia_nacional as $key => $value) {
                $modelMaterialesNacionales = new MaterialesNacionales;
                $modelMaterialesNacionales->planilla_2_id=$modelPlanilla2->id;
                $modelMaterialesNacionales->codigo=$codigo_arancel_nacional[$key];
                $modelMaterialesNacionales->descripcion=$descripcion_arancelaria_nacional[$key];
                $modelMaterialesNacionales->nombre_produc_nac=$nombre_producto_nacional[$key];
                $modelMaterialesNacionales->nombre_provedor =$nombre_proveedor_nacional[$key];
                $modelMaterialesNacionales->incidencia_costo=$value;
                $modelMaterialesNacionales->rif_productor=$request->rif_productor[$key];
                $modelMaterialesNacionales->bactivo=1;
                $modelMaterialesNacionales->save();
            }
            /*****  Cambiando el estado de la planilla 2 en det declaracion product ******/
            $detDeclaracionProducModel = $modelPlanilla2->declaracionProduc;
            $detDeclaracionProducModel->estado_p2=1;
            $detDeclaracionProducModel->save();
      
            DB::commit();
            
            Alert::success('Se han guardado los datos correspondientes a la Planilla 2!','Registrado Correctamente!')->persistent("Ok");
            
            return redirect()->action('DeclaracionJOController@index');
            
        }/// fin IF $modelPlanilla2 fue guardado
         
         
        DB::rollback();
        Alert::warning('No se pudo realizar su solicitud. Intente nuevamente.','Acción Denegada!')->persistent("OK");
        return redirect()->back()->withInput();
    }

    /**
     * Display the specified resource.
     * Se muestra los datos registrado para la planilla 2
     * Se puede mostrar la planilla de la siguiente manera
     * 1.- Enviar los parametros $id, $layout
     * 2.- Si es enviado el parametro $det_declaracion_id este tiene prioridad ante en id para la busqueda de la planilla
     * 
     * Observacion: Si el parametro layout es enviado como false, se mostrara solamente el contenido de la vista.
     * 
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        //dd($request);
        $layout= $request->layout==="false"? false: true;

        if(!empty($request->det_declaracion_id))
        {
            
            $det_declaracion= DetDeclaracionProduc::with('rPlanilla2')->find($request->det_declaracion_id);
        
            if(empty($det_declaracion)){
                Alert::error('La planilla que intenta visualizar no esta registrada en el sistema. D1','Acción Denegada!')->persistent("OK");
                return $layout? redirect()->action('ListaDeclaracionJOController@index') : '<div class="alert alert-danger">La planilla que intenta visualizar no esta registrada en el sistema. D1</div>';
            }

            $planilla2 = $det_declaracion->rPlanilla2;
            if($planilla2->bactivo!==1)
            $planilla2=null;

        }else{
            $id= $request->id ;
            $planilla2 = Planilla2::with('declaracionProduc')->with('materialesImportados')->with('materialesNacionales')->where('bactivo',1)->find($id);
        }

        /***** Validar que haya un registro de la planilla ****/
        if(empty($planilla2)){

             Alert::error('La planilla que intenta visualizar no esta registrada en el sistema.','Acción Denegada!')->persistent("OK");
            return  $layout? redirect()->action('ListaDeclaracionJOController@index') : '<div class="alert alert-danger">La planilla que intenta visualizar no esta registrada en el sistema.</div>';
        }

        /***** Validar que la planilla sea de un usuario en especifico ****/
        if($planilla2->declaracionProduc->declaracionJO->gen_usuario_id!=Auth::user()->id && Auth::user()->gen_tipo_usuario_id!=5 && Auth::user()->gen_tipo_usuario_id!=6 && Auth::user()->gen_tipo_usuario_id!=1 && Auth::user()->gen_tipo_usuario_id!=8){

            Alert::error('No tiene permisos para visualizar esta planilla.','Acción Denegada!')->persistent("OK");

           return  $layout? redirect()->action('DeclaracionJOController@index') : '<div class="alert alert-danger">No tiene permisos para visualizar esta planilla.</div>';
       }

       $producto = $planilla2->declaracionProduc->rProducto;
        $materiales_importados= $planilla2->materialesImportados->where('bactivo',1);
        $materiales_nacionales= $planilla2->materialesNacionales->where('bactivo',1);

        if($layout)
            return view('djoplanilla2.show',compact('layout','planilla2','producto','materiales_importados','materiales_nacionales'));
        else
            return View::make('djoplanilla2.show',compact('layout','planilla2','producto','materiales_importados','materiales_nacionales'))->renderSections()['content'];
    }

    /**
     * Show the form for editing the specified resource.
     * Mostrar el formulario para actualizar
     * Puede recibir el $id de la planilla para mostrarla 
     * Si es enviado det_declaracion_id este tiene mayor prioridad para cargar los datos de la planilla, es decir,
     * si se envian ambos (tanto $id como det_declaracion_id en el request, este ultimo tendra mayor prioridad )
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id,Request $request)
    {
      /** Validar que se este editando, si se esta editando, se libera la sesion planilla2_editando
       * y se preserva el old_input **/

        if(empty(Session::get('planilla2_editando')))
        session(['_old_input' => null]);
        else
        session(['planilla2_editando' => null]);

        if(!empty($request->det_declaracion_id))
        {
            
            $det_declaracion= DetDeclaracionProduc::with('rPlanilla2')->find($request->det_declaracion_id);
        
            if(empty($det_declaracion)){
                Alert::error('La planilla que intenta visualizar no esta registrada en el sistema. D1','Acción Denegada!')->persistent("OK");
                return redirect()->action('ListaDeclaracionJOController@index') ;
            }

            $planilla2 = $det_declaracion->rPlanilla2->where('bactivo',1);


        }else{
            $id= empty($id)? $request->id : $id ;
            $planilla2 = Planilla2::with('declaracionProduc')->with('materialesImportados')->with('materialesNacionales')->where('bactivo',1)->find($id);
        }
        /***** Validar que haya un registro de la planilla ****/
        if(empty($planilla2)){

             Alert::error('La planilla que intenta visualizar no esta registrada en el sistema.','Acción Denegada!')->persistent("OK");

            return  redirect()->action('ListaDeclaracionJOController@index');
        }
        
        /***** Validar que la planilla sea de un usuario en especifico ****/
        if($planilla2->declaracionProduc->declaracionJO->gen_usuario_id!=Auth::user()->id && Auth::user()->gen_tipo_usuario_id!=5 && Auth::user()->gen_tipo_usuario_id!=6 && Auth::user()->gen_tipo_usuario_id!=1 && Auth::user()->gen_tipo_usuario_id!=8){

            Alert::error('No tiene permisos para visualizar esta planilla.','Acción Denegada!')->persistent("OK");

           return   redirect()->action('ListaDeclaracionJOController@index');
       }

       /** La planilla solo puede editarse si el estado es 0 en la tabla declaracion produc **/
       if($planilla2->declaracionProduc->estado_p2!=0){
        Alert::error('Disculpe, no puede modificar esta planilla.','Acción Denegada!')->persistent("OK");

        return   redirect()->action('ListaDeclaracionJOController@index');
       }
        $producto = $planilla2->declaracionProduc->rProducto;
        $unidadMedida= $producto->unidadMedida;
        $materiales_importados= $planilla2->materialesImportados->where('bactivo',1);
        $materiales_nacionales= $planilla2->materialesNacionales->where('bactivo',1);
        $pais = Pais::orderBy('dpais','asc')->pluck('dpais','id');

        if(empty($request->old())){
     
        $arrMatId=array();
        $arrDescipcion=array();
        $arrCodigo=array();
        $arrPais=array();
        $arrMonto=array();
        $arrRifProductor=array();
        foreach($materiales_importados as $material){
                $arrMatId[] = $material->id;
                $arrDescipcion[] = $material->descripcion;
                $arrCodigo[] = $material->codigo;
                $arrPais[] = $material->pais_id;
                $arrMonto[] = $material->incidencia_costo;
                //$arrRifProductor[] = $material->rif_productor;
        }

        
        $arrMatId2=array();
        $arrDescipcion2=array();
        $arrCodigo2=array();
        $arrNombreProducto=array();
        $arrNombreProveedor=array();
        $arrMonto2=array();
        foreach($materiales_nacionales as $material){

            $arrMatId2[] = $material->id;
            $arrDescipcion2[]=$material->descripcion;
            $arrCodigo2[]=$material->codigo;
            $arrNombreProducto[]=$material->nombre_produc_nac;
            $arrNombreProveedor[]=$material->nombre_provedor;
            $arrMonto2[]=$material->incidencia_costo;
            $arrRifProductor[] = $material->rif_productor;

        }

        $old_precargar=[
            'descripcion_comercial' => $producto->descrip_comercial,
            'descripcion_arancel' => $producto->descripcion,
            'codigo_arancel' => $producto->codigo,
            'unidad_medida' => $unidadMedida->dunidad,
            'uso_aplicacion' => $planilla2->uso_aplicacion,
            'material_importado_id' => $arrMatId,
            'descripcion_arancelaria_importado' => $arrDescipcion,
            'codigo_arancel_importado' => $arrCodigo,
            'pais_importado' => $arrPais,
            'monto_incidencia_importado' => $arrMonto,
            'material_nacional_id' => $arrMatId2,
            'descripcion_arancelaria_nacional' => $arrDescipcion2,
            'codigo_arancel_nacional' => $arrCodigo2,
            'nombre_producto_nacional' =>  $arrNombreProducto,
            'nombre_proveedor_nacional'  =>  $arrNombreProveedor,
            'monto_incidencia_nacional' => $arrMonto2,
            'rif_productor' => $arrRifProductor
        ];
        Session::put('_old_input', $old_precargar);

        
        }

        //--Definicion de variable layout para mostrar contenido de vista--///
         $layout='Mostrar contenido de la vista';
  
        return view('djoplanilla2.edit',compact('layout','planilla2','producto','unidadMedida','materiales_importados','materiales_nacionales','pais'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(DJOPlanilla2Request $request, $id)
    {
       /** Indicar session de actualizacion **/
       Session::put('planilla2_editando', true);

            $id= empty($id)? $request->id : $id ;
            $planilla2 = Planilla2::with('declaracionProduc')->with('materialesImportados')->with('materialesNacionales')->find($id);
       
        
        /***** Validar que haya un registro de la planilla ****/
        if(empty($planilla2)){

             Alert::error('La planilla que intenta visualizar no esta registrada en el sistema.','Acción Denegada!')->persistent("OK");

            return  redirect()->action('DeclaracionJOController@index');
        }
        
        /***** Validar que la planilla sea de un usuario en especifico ****/
        if($planilla2->declaracionProduc->declaracionJO->gen_usuario_id!=Auth::user()->id && Auth::user()->gen_tipo_usuario_id!=5 && Auth::user()->gen_tipo_usuario_id!=6 && Auth::user()->gen_tipo_usuario_id!=1 && Auth::user()->gen_tipo_usuario_id!=8){

            Alert::error('No tiene permisos para visualizar esta planilla.','Acción Denegada!')->persistent("OK");

           return   redirect()->action('DeclaracionJOController@index');
       }

       /*****************     Inicializar variables         ************/

       $total_mate_import = 0;
       $total_mate_nac = 0;


/*****************             ************/

   $uso_aplicacion = $request['uso_aplicacion'];

   if(empty($uso_aplicacion)){
       Alert::warning('Debe indicar el uso y aplicacion del producto. Intente nuevamente.','Acción Denegada!')->persistent("OK");

       return redirect()->back()->withInput();
   }


/*****************     Obtener datos de materiales importados del formulario.      ************/
       $material_importado_id = $request->material_importado_id;
       $descripcion_arancelaria_importado = $request->descripcion_arancelaria_importado;
       $codigo_arancel_importado = $request->codigo_arancel_importado;
       $pais_importado = $request->pais_importado;
       $monto_incidencia_importado = $request->monto_incidencia_importado;
       //$rif_productor = $request->rif_productor;

/*****************     Calculo del monto total de materiales importados         ************/
       foreach ($monto_incidencia_importado as $key => $value) {
           $total_mate_import+= ($value);
       }


/*****************     Obtener datos de materiales nacionales del formulario.          ************/

       $material_nacional_id = $request->material_nacional_id;
       $descripcion_arancelaria_nacional= $request->descripcion_arancelaria_nacional;
       $codigo_arancel_nacional = $request->codigo_arancel_nacional;
       $nombre_producto_nacional = $request->nombre_producto_nacional;
       $nombre_proveedor_nacional = $request->nombre_proveedor_nacional;
       $monto_incidencia_nacional = $request->monto_incidencia_nacional;
       $rif_productor = $request->rif_productor;


/*****************     Calculo del monto total de materiales nacionales         ************/

       foreach ($monto_incidencia_nacional as $key => $value) {
           $total_mate_nac+= ($value);
       }


/*****************     Validar que los codigos de los materiales no se repitan  ************/

       /*if(count(array_unique($codigo_arancel_importado)) != count($codigo_arancel_importado) ){
           $bandera_materiales_importados=true;
       }

       if(count(array_unique($codigo_arancel_nacional)) != count($codigo_arancel_nacional) ){
           $bandera_materiales_nacionales=true;
       }*/

       if(!empty($bandera_materiales_importados)){

            Alert::warning('No se permite la repeticion de materiales internacionales. Intente nuevamente.','Acción Denegada!')->persistent("OK");

            return redirect()->back()->withInput();
       }

       if(!empty($bandera_materiales_nacionales)){

            Alert::warning('No se permite la repeticion de materiales nacionales. Intente nuevamente.','Acción Denegada!')->persistent("OK");

            return redirect()->back()->withInput();
       }


/*****************    Validar que los datos esten completos         ************/


if( count(array_filter($descripcion_arancelaria_importado))  != count(array_filter($codigo_arancel_importado)) || 
   count(array_filter($codigo_arancel_importado))  != count(array_filter($pais_importado) ) || 
   count(array_filter($pais_importado) ) != count(array_filter($monto_incidencia_importado)))
{

   $bandera_cantidad_datos_importados= true;
}


 if(  count(array_filter( $descripcion_arancelaria_nacional))  != count(array_filter($codigo_arancel_nacional)) || 
       count(array_filter($codigo_arancel_nacional))  != count(array_filter( $nombre_producto_nacional)) || 
       count(array_filter( $nombre_producto_nacional))  !=  count(array_filter($nombre_proveedor_nacional)) || 
       count(array_filter($nombre_proveedor_nacional))  !=  count(array_filter($monto_incidencia_nacional))
       || count(array_filter($rif_productor))  !=  count(array_filter($rif_productor)) ){
$bandera_cantidad_datos_nacionales= true;

 }

 /** Creando de forma manual old de regreso  **/
 $producto = $planilla2->declaracionProduc->rProducto;
        $unidadMedida= $producto->unidadMedida;
        $materiales_importados= $planilla2->materialesImportados;
        $materiales_nacionales= $planilla2->materialesNacionales;
        $pais = Pais::orderBy('dpais','asc')->pluck('dpais','id');
   
        $arrMatId=array();
        $arrDescipcion=array();
        $arrCodigo=array();
        $arrPais=array();
        $arrMonto=array();
        $arrRifProductor=array();
        foreach($materiales_importados as $material){
                $arrMatId[] = $material->id;
                $arrDescipcion[] = $material->descripcion;
                $arrCodigo[] = $material->codigo;
                $arrPais[] = $material->pais_id;
                $arrMonto[] = $material->incidencia_costo;
                //$arrRifProductor[] = $material->rif_productor;
        }

        
        $arrMatId2=array();
        $arrDescipcion2=array();
        $arrCodigo2=array();
        $arrNombreProducto=array();
        $arrNombreProveedor=array();
        $arrMonto2=array();
        foreach($materiales_nacionales as $material){

            $arrMatId2[] = $material->id;
            $arrDescipcion2[]=$material->descripcion;
            $arrCodigo2[]=$material->codigo;
            $arrNombreProducto[]=$material->nombre_produc_nac;
            $arrNombreProveedor[]=$material->nombre_provedor;
            $arrMonto2[]=$material->incidencia_costo;
            $arrRifProductor[] = $material->rif_productor;

        }

        $old_precargar=[
            'descripcion_comercial' => $producto->descrip_comercial,
            'descripcion_arancel' => $producto->descripcion,
            'codigo_arancel' => $producto->codigo,
            'unidad_medida' => $unidadMedida->dunidad,
            'uso_aplicacion' => $planilla2->uso_aplicacion,
            'material_importado_id' => $arrMatId,
            'descripcion_arancelaria_importado' => $arrDescipcion,
            'codigo_arancel_importado' => $arrCodigo,
            'pais_importado' => $arrPais,
            'monto_incidencia_importado' => $arrMonto,
            'material_nacional_id' => $arrMatId2,
            'descripcion_arancelaria_nacional' => $arrDescipcion2,
            'codigo_arancel_nacional' => $arrCodigo2,
            'nombre_producto_nacional' =>  $arrNombreProducto,
            'nombre_proveedor_nacional'  =>  $arrNombreProveedor,
            'monto_incidencia_nacional' => $arrMonto2,
            'rif_productor' => $arrRifProductor
        ];
        Session::put('_old_input', $old_precargar);

if(!empty( $bandera_cantidad_datos_importados)){

            Alert::warning('Datos de materiales importados incompletos. Intente nuevamente.','Acción Denegada!')->persistent("OK");

           return redirect()->back()->withInput();
       }

if(!empty($bandera_cantidad_datos_nacionales)){

            Alert::warning('Datos de materiales nacionales incompletos. Intente nuevamente.','Acción Denegada!')->persistent("OK");

           return redirect()->back()->withInput();
}

/*****************     Validar que se hayan enviado almenos  un material importado OR un material nacional    ************/

if( count(array_filter($monto_incidencia_importado))==0 && count(array_filter($monto_incidencia_nacional))==0){

   Alert::warning('La solicitud que intenta realizar requiere que indique los materiales utilizados para elaborar su producto. Intente nuevamente.','Acción Denegada!')->persistent("OK");

           return redirect()->back()->withInput();
}





       /** Actualizar datos de la tabla planilla 2 **/
       $planilla2->uso_aplicacion = $request->uso_aplicacion;
       $planilla2->total_mate_import = $total_mate_import;
       $planilla2->total_mate_nac = $total_mate_nac;
       $planilla2->bactivo=1;
       $planilla2->estado_observacion=0;
       if($planilla2->save()){

        /**------------- Proceso de actualizacion de los materiales importados -----------------------**/
        $matImporModels= $planilla2->materialesImportados->where('bactivo',1);

        /** $matImporActuales para almacenar la coleccion de modelo materiales importados
         * creados o actualizados provenientes del form edit para luego comparar con la
         * coleccion de modelos anteriores y obtener los modelos que se eliminaron del form
         * y asi proceder al borrado logico **/
        $matImporActuales= collect(new MaterialesImportados);

        /** Actualizar o Crear los materiales importados **/
         foreach($monto_incidencia_importado as $key => $value){
             if($material_importado_id[$key]!=null){ 
                 if($matImporModels->contains('id',$material_importado_id[$key])){
                     $modeloMatImport = $matImporModels->find($material_importado_id[$key]);
                     $modeloMatImport->codigo=$codigo_arancel_importado[$key];
                     $modeloMatImport->descripcion=$descripcion_arancelaria_importado[$key];
                     $modeloMatImport->pais_id=$pais_importado[$key];
                     $modeloMatImport->incidencia_costo=$value;
                     //$modeloMatImport->rif_productor=$rif_productor[$key];
                     $modeloMatImport->bactivo=1;
                     $modeloMatImport->touch();
                 }else{
                     $modeloMatImport = null;
                 }
             }else{
                $modeloMatImport = new MaterialesImportados;
                $modeloMatImport->planilla_2_mi_id= $planilla2->id;
                $modeloMatImport->codigo=$codigo_arancel_importado[$key];
                $modeloMatImport->descripcion=$descripcion_arancelaria_importado[$key];
                $modeloMatImport->pais_id=$pais_importado[$key];
                $modeloMatImport->incidencia_costo=$value;
                //$modeloMatImport->rif_productor=$rif_productor[$key];
                $modeloMatImport->bactivo=1;
                $modeloMatImport->save();
             }

             if(!empty($modeloMatImport)){
                 $matImporActuales->push($modeloMatImport);
             }

         }

         /** Validar que elementos se deben eliminar (Borrado Logico) ya que no regresaron del formulario **/
         $matImporDel= $matImporModels->diff($matImporActuales);
         foreach($matImporDel as $material)
         {
             $material->bactivo=0;
             $material->touch();
         }
         /**------------- FIN Proceso de actualizacion de los materiales importados -----------------------**/




         /**------------- Proceso de actualizacion de los materiales nacionales -----------------------**/
         $matNatModels= $planilla2->materialesNacionales->where('bactivo',1);

         /** $matNatActuales para almacenar la coleccion de modelo materiales nacionales
          * creados o actualizados provenientes del form edit para luego comparar con la
          * coleccion de modelos anteriores y obtener los modelos que se eliminaron del form
          * y asi proceder al borrado logico **/
         $matNatActuales= collect(new MaterialesNacionales);
         // dd($request->all());
 
         /** Actualizar o Crear los materiales nacionales **/
          foreach($monto_incidencia_nacional as $key => $value){
              if($material_nacional_id[$key]!=null){ 
                  if($matNatModels->contains('id',$material_nacional_id[$key])){
                      $modeloMatNacional = $matNatModels->find($material_nacional_id[$key]);
                      $modeloMatNacional->codigo=$codigo_arancel_nacional[$key];
                      $modeloMatNacional->descripcion=$descripcion_arancelaria_nacional[$key];
                      $modeloMatNacional->nombre_produc_nac =$nombre_producto_nacional[$key];
                      $modeloMatNacional->nombre_provedor =$nombre_proveedor_nacional[$key];
                      $modeloMatNacional->incidencia_costo=$value;
                      $modeloMatNacional->rif_productor=$rif_productor[$key];
                      $modeloMatNacional->bactivo=1;
                      $modeloMatNacional->touch();
                  }else{
                    $modeloMatNacional = null;
                  }
              }else{
                 $modeloMatNacional = new MaterialesNacionales;

                      $modeloMatNacional->planilla_2_id=$planilla2->id;
                      $modeloMatNacional->codigo=$codigo_arancel_nacional[$key];
                      $modeloMatNacional->descripcion=$descripcion_arancelaria_nacional[$key];
                      $modeloMatNacional->nombre_produc_nac =$nombre_producto_nacional[$key];
                      $modeloMatNacional->nombre_provedor =$nombre_proveedor_nacional[$key];
                      $modeloMatNacional->incidencia_costo=$value;
                      $modeloMatNacional->rif_productor=$rif_productor[$key];
                      $modeloMatNacional->bactivo=1;
                      $modeloMatNacional->save();
                 $modeloMatNacional->save();
              }
 
              if(!empty($modeloMatNacional)){
                  $matNatActuales->push($modeloMatNacional);
              }
 
          }
 
          /** Validar que elementos se deben eliminar (Borrado Logico) ya que no regresaron del formulario **/
          $matImporDel= $matImporModels->diff($matImporActuales);
          foreach($matImporDel as $material)
          {
              $material->bactivo=0;
              $material->touch();
          }

          /** Validar que elementos se deben eliminar (Borrado Logico) ya que no regresaron del formulario **/
          $matNatDel= $matNatModels->diff($matNatActuales);
          foreach($matNatDel as $material)
          {
              $material->bactivo=0;
              $material->touch();
          }
         

          /** Indicar session finalizar planilla2_editando **/
       Session::put('planilla2_editando', false);
        
       /** Olvidar olds **/
       session(['_old_input' => null]);
       // actualisar la tabla planilla 2   campo estado_observacion =0
       $planilla2->declaracionProduc->estado_p2 = 1;
       $planilla2->declaracionProduc->save();

       Alert::success('Se han actualizado los datos correspondientes a la Planilla 2!','Actualizacion Exitosa!')->persistent("Ok");
       return redirect()->action('DeclaracionJOController@index');

       }else{
            
            Alert::warning('Disculpe, no se pudieron actualizar los datos.','Acción Fallida!')->persistent("OK");
            return redirect()->back()->withInput();
       }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }



    /** Para consultar las observaciones mediante ajax **/
    /**
     * Se recibe como parametro el $id de la planilla
     * 
     */
    public function obtenerObservacion(Request $request,$id=null){

        if(!empty($request->det_declaracion_id)){
            $detDeclaracion= DetDeclaracionProduc::with('rPlanilla2')->find($request->det_declaracion_id);
            $planilla2= $detDeclaracion->rPlanilla2;
        }else{
        $planilla2 = Planilla2::find($id);
        }
        return Response::json(array('observacion'=>$planilla2->descrip_observacion));
        
    }

     /** Para guardar las observaciones mediante ajax **/
    /**
     * Se recibe como parametro el $id de la planilla
     * 
     */
    public function storeObservacion(Request $request,$id=null){

        
        if(!empty($request->det_declaracion_id)){
            $detDeclaracion= DetDeclaracionProduc::with('rPlanilla2')->find($request->det_declaracion_id);
            $planilla2= $detDeclaracion->rPlanilla2;
        }else{
            $planilla2 = Planilla2::find($id);
        }
        if(!empty($request->observacion)){
            $planilla2->descrip_observacion=$request->observacion;
            $planilla2->estado_observacion=1;
        }else{
            $planilla2->descrip_observacion=null;
            $planilla2->estado_observacion=0;
        }

        if($planilla2->touch())
            return Response::json(array('respuesta'=>'Solicitud procesada con exito','estado'=>1));
        else
            return Response::json(array('respuesta'=>'La solicitud no pudo ser procesada','estado'=>0));
        
    }
}
