<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DetDeclaracionProduc extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('det_declaracion_produc', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('gen_declaracion_jo_id')->index()->nullable();
            $table->foreign('gen_declaracion_jo_id')->references('id')->on('gen_declaracion_jo');
            $table->unsignedInteger('productos_id')->index()->nullable();
            $table->foreign('productos_id')->references('id')->on('productos');
            $table->string('estado_p2')->nullable();
            $table->string('estado_p3')->nullable();
            $table->string('estado_p4')->nullable();
            $table->string('estado_p5')->nullable();
            $table->string('estado_p6')->nullable();
            $table->unsignedInteger('estado_observacion')->nullable();
            $table->boolean('bactivo')->default(1);
            $table->timestamps();
          });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::dropIfExists('det_declaracion_produc');   
    }
}
