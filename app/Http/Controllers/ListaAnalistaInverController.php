<?php

namespace App\Http\Controllers;

use App\Models\GenSolInversionista;
use App\Models\DetUsuario;
use App\Models\Accionistas;
use App\Models\Pais;
use App\Models\CatDocumentos;
use App\Models\GenStatus;
use App\Models\BandejaSolInversionista;
use App\Models\GenDocumentosInversiones;
use App\Models\Doc_Adic_Inversiones;
use App\Models\DocumentsVariosInversiones;
use App\Mail\NotificacionesInversiones;
use Illuminate\Http\Request;
use Mail;
use Session;
use Auth;
use Alert;


class ListaAnalistaInverController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        //Consulta de la tabla gen_sol_inversionista para traer todas las solicitudes de inversion extranjero//

        $inv_extranjero=GenSolInversionista::where('bactivo',1)
        ->with('rbandejainversionista')->get();
        //dd($inv_extranjero);
       
         
         //--Solo me tarigo las que son mias--//
        /*$inv_extranjero=GenSolInversionista::where('gen_usuario_id',Auth::user()->id)
        ->where('bactivo',1)
        ->get();*/
        
        return view('AnalistInverExtjero.index',compact('inv_extranjero'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
      
       

     /*Consulta para */
         

           


     /*Consulta de los accionista*/
      $accionistas=Accionistas::where('gen_usuario_id',Auth::user()->id)->where('bactivo',1)->get();


        return view('AnalistInverExtjero.create',compact('accionistas'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\GenSolicitud  $genSolicitud
     * @return \Illuminate\Http\Response
     */
    public function show(GenSolInversionista $genSolInversionista)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\GenSolicitud  $genSolicitud
     * @return \Illuminate\Http\Response
     */
    public function edit(GenSolInversionista $genSolInversionista,$id)
    {

        $titulo= 'Registro de Potencial Inversionista Extranjero';
        $descripcion= 'Perfil del Analista';


     $inversionista=GenSolInversionista::find($id);

    /************ Valido si la solicitud existe, Si existe no hago nada en caso contrario inserto la solicitud******************/

       $validaratencion=BandejaSolInversionista::where('gen_sol_inversionista_id',$inversionista->id)->where('gen_usuario_id',Auth::user()->id)->first();
         //dd($validaratencion);


          if(empty($validaratencion)){
            /*Consulta las solicitudes mis solicitudes con el status id 10 atendido*/
            $inversionista=GenSolInversionista::find($id);
            $inversionista->gen_status_id=10;
            $inversionista->fstatus=date('Y-m-d');
            $inversionista->save();
             /***Inserto en la bandeja solinversionista me traigo el id de esa solicitud con el usuario   analista le actualizo el status a 10 que es en Observacion la actualizo a la fecha de hoy****/
            $inversionanalista= new BandejaSolInversionista;
            $inversionanalista->gen_sol_inversionista_id=$inversionista->id;
            $inversionanalista->gen_usuario_id=Auth::user()->id;
            $inversionanalista->gen_status_id=10;
            $inversionanalista->fstatus=date('Y-m-d');
            $inversionanalista->save();

          }
     

    /*************Consulta para traerme el estado de observacion de las solicitudes**************/
        //$estado_observacion=GenSolInversionista::all()->pluck('estado_observacion','id');
         //dd($estado_observacion);

        /*****Consulta para obtener el estado de la observacion de la solicitud 11 observacion, 12 Aprobada y 13 Anulada****/

        $documentos=CatDocumentos::where('gen_ente_id',2)->where('bactivo',1)->whereBetween('id',[10,17])->get();
        //dd($documentos);
        if($inversionista->gen_status_id == 12){
            $estado_observacion=GenStatus::whereIn('id', [13, 26])->pluck('nombre_status','id');
        }else{
            $estado_observacion=GenStatus::whereIn('id', [11, 12,13,15,19,26])->pluck('nombre_status','id');
        }
        

        //dd($select_observacion);
 
        /**/
        $det_usuario=DetUsuario::where('gen_usuario_id',$inversionista->gen_usuario_id)->first();
        

       /****************Consulta de la tabla pais para traerme el id del inversionista*****************/
        $pais=Pais::where('id',$inversionista->pais_id)->first();
        //dd($pais);
        //
        //dd($pais);
        
      
        $accionistas=Accionistas::where('gen_usuario_id',$inversionista->gen_usuario_id)->where('bactivo',1)->get();

         $docCertificados=GenDocumentosInversiones::where('gen_sol_inversionista_id',$inversionista->id)->first();


         $docAdicionales=Doc_Adic_Inversiones::where('gen_sol_inversionista_id',$inversionista->id)->first();

         $sopAdicionalesInv2=DocumentsVariosInversiones::where('gen_sol_inversionista_id',$inversionista->id)->where('cat_documentos_id',12)->get();

         $sopAdicionalesInv4=DocumentsVariosInversiones::where('gen_sol_inversionista_id',$inversionista->id)->where('cat_documentos_id',14)->get();

    
     //dd($sopAdicionalesInv2);
    // 
        //$extencion_file_1 = $exten_file_1[1];

         if(!empty($docCertificados['file_1'])){
             $exten_file_1=explode(".", $docCertificados['file_1']);
            for($i=0; $i <= count($exten_file_1)-1; $i++){
                if($exten_file_1[$i] == 'pdf' || $exten_file_1[$i] == 'doc' || $exten_file_1[$i] =='docx'){
                    //return $exten_file_1[$i];
                     $extencion_file_1 = $exten_file_1[$i];
                }
            }
//dd($extencion_file_1);
//Se modifica y valida explode; mas nombre de archivo para error offset
        } else {
            $extencion_file_1 = '';
        }
        if (!empty($docCertificados['file_2'])) {
            $exten_file_2=explode(".", $docCertificados['file_2']);
            for($i=0; $i <= count($exten_file_2)-1; $i++){
                if($exten_file_2[$i] == 'pdf' || $exten_file_2[$i] == 'doc' || $exten_file_2[$i] =='docx'){
                    //return $exten_file_1[$i];
                     $extencion_file_2 = $exten_file_2[$i];
                }
            }
        }else {
            $extencion_file_2 = '';
        }
        if (!empty($docCertificados['file_3'])) {
            $exten_file_3=explode(".", $docCertificados['file_3']);
             for($i=0; $i <= count($exten_file_3)-1; $i++){
                if($exten_file_3[$i] == 'pdf' || $exten_file_3[$i] == 'doc' || $exten_file_3[$i] =='docx'){
                    //return $exten_file_1[$i];
                     $extencion_file_3 = $exten_file_3[$i];
                }
            }
        }else {
            $extencion_file_3 = '';
        }
        if (!empty($docCertificados['file_4'])) {
            $exten_file_4=explode(".", $docCertificados['file_4']);
            for($i=0; $i <= count($exten_file_4)-1; $i++){
                if($exten_file_4[$i] == 'pdf' || $exten_file_4[$i] == 'doc' || $exten_file_4[$i] =='docx'){
                    //return $exten_file_1[$i];
                     $extencion_file_4 = $exten_file_4[$i];
                }
            }
        }else {
            $extencion_file_4 = '';
        }
        if (!empty($docCertificados['file_5'])) {
            $exten_file_5=explode(".", $docCertificados['file_5']);
            for($i=0; $i <= count($exten_file_5)-1; $i++){
                if($exten_file_5[$i] == 'pdf' || $exten_file_5[$i] == 'doc' || $exten_file_5[$i] =='docx'){
                    //return $exten_file_1[$i];
                     $extencion_file_5 = $exten_file_5[$i];
                }
            }
        }else {
            $extencion_file_5 = '';
        }
        if (!empty($docCertificados['file_6'])) {
            $exten_file_6=explode(".", $docCertificados['file_6']);
            for($i=0; $i <= count($exten_file_6)-1; $i++){
                if($exten_file_6[$i] == 'pdf' || $exten_file_6[$i] == 'doc' || $exten_file_6[$i] =='docx'){
                    //return $exten_file_1[$i];
                     $extencion_file_6 = $exten_file_6[$i];
                }
            }
        }else {
            $extencion_file_6 = '';
        }
        if (!empty($docCertificados['file_7'])) {
            $exten_file_7=explode(".", $docCertificados['file_7']);
            for($i=0; $i <= count($exten_file_7)-1; $i++){
                if($exten_file_7[$i] == 'pdf' || $exten_file_7[$i] == 'doc' || $exten_file_7[$i] =='docx'){
                    //return $exten_file_1[$i];
                     $extencion_file_7 = $exten_file_7[$i];
                }
            }
        }else {
            $extencion_file_7 = '';
        }
        if (!empty($docCertificados['file_8'])) {
            $exten_file_8=explode(".", $docCertificados['file_8']);
            for($i=0; $i <= count($exten_file_8)-1; $i++){
                if($exten_file_8[$i] == 'pdf' || $exten_file_8[$i] == 'doc' || $exten_file_8[$i] =='docx'){
                    //return $exten_file_1[$i];
                     $extencion_file_8 = $exten_file_8[$i];
                }
            }
        }else {
            $extencion_file_8 = '';
        }
        if (!empty($docCertificados['file_9'])) {
            $exten_file_9=explode(".", $docCertificados['file_9']);
            for($i=0; $i <= count($exten_file_9)-1; $i++){
                if($exten_file_9[$i] == 'pdf' || $exten_file_9[$i] == 'doc' || $exten_file_9[$i] =='docx'){
                    //return $exten_file_1[$i];
                     $extencion_file_9 = $exten_file_9[$i];
                }
            }
        }else {
            $extencion_file_9 = '';
        }
        if (!empty($docCertificados['file_10'])) {
            $exten_file_10=explode(".", $docCertificados['file_10']);
            for($i=0; $i <= count($exten_file_10)-1; $i++){
                if($exten_file_10[$i] == 'pdf' || $exten_file_10[$i] == 'doc' || $exten_file_10[$i] =='docx'){
                    //return $exten_file_1[$i];
                     $extencion_file_10 = $exten_file_10[$i];
                }
            }
        }else {
            $extencion_file_10 = '';
        }




        /**Doc Adicionales verificacion de los archivo*****/
         if(!empty($docAdicionales['file_1'])){
            $exten_file_1=explode(".", $docAdicionales['file_1']);
            for($i=0; $i <= count($exten_file_1)-1; $i++){
                if($exten_file_1[$i] == 'pdf' || $exten_file_1[$i] == 'doc' || $exten_file_1[$i] =='docx'){
                    //return $exten_file_1[$i];
                     $extencion_adi_file_1 = $exten_file_1[$i];
                }
            }
        } else {
            $extencion_adi_file_1 = '';
        }
        if (!empty($docAdicionales['file_2'])) {
            $exten_file_2=explode(".", $docAdicionales['file_2']);
            for($i=0; $i <= count($exten_file_2)-1; $i++){
                if($exten_file_2[$i] == 'pdf' || $exten_file_2[$i] == 'doc' || $exten_file_2[$i] =='docx'){
                    //return $exten_file_1[$i];
                     $extencion_adi_file_2 = $exten_file_2[$i];
                }
            }
        }else {
            $extencion_adi_file_2 = '';
        }
        if (!empty($docAdicionales['file_3'])) {
            $exten_file_3=explode(".", $docAdicionales['file_3']);
            for($i=0; $i <= count($exten_file_3)-1; $i++){
                if($exten_file_3[$i] == 'pdf' || $exten_file_3[$i] == 'doc' || $exten_file_3[$i] =='docx'){
                    //return $exten_file_1[$i];
                     $extencion_adi_file_3 = $exten_file_3[$i];
                }
            }
        }else {
            $extencion_adi_file_3 = '';
        }
        if (!empty($docAdicionales['file_4'])) {
            $exten_file_4=explode(".", $docAdicionales['file_4']);
            for($i=0; $i <= count($exten_file_4)-1; $i++){
                if($exten_file_4[$i] == 'pdf' || $exten_file_4[$i] == 'doc' || $exten_file_4[$i] =='docx'){
                    //return $exten_file_1[$i];
                     $extencion_adi_file_4 = $exten_file_4[$i];
                }
            }
        }else {
            $extencion_adi_file_4 = '';
        }
        if (!empty($docAdicionales['file_5'])) {
            $exten_file_5=explode(".", $docAdicionales['file_5']);
            for($i=0; $i <= count($exten_file_5)-1; $i++){
                if($exten_file_5[$i] == 'pdf' || $exten_file_5[$i] == 'doc' || $exten_file_5[$i] =='docx'){
                    //return $exten_file_1[$i];
                     $extencion_adi_file_5 = $exten_file_5[$i];
                }
            }
        }else {
            $extencion_adi_file_5 = '';
        }
        if (!empty($docAdicionales['file_6'])) {
            $exten_file_6=explode(".", $docAdicionales['file_6']);
            for($i=0; $i <= count($exten_file_6)-1; $i++){
                if($exten_file_6[$i] == 'pdf' || $exten_file_6[$i] == 'doc' || $exten_file_6[$i] =='docx'){
                    //return $exten_file_1[$i];
                     $extencion_adi_file_6 = $exten_file_6[$i];
                }
            }
        }else {
            $extencion_adi_file_6 = '';
        }


        $documentosNat=CatDocumentos::where('gen_ente_id',2)->where('bactivo',1)->whereIn('id',[16,17,18])->get();
        $documentosJuri=CatDocumentos::where('gen_ente_id',2)->where('bactivo',1)->whereIn('id',[19,20,21,18,22])->get();
        $documentosSport=CatDocumentos::where('gen_ente_id',2)->where('bactivo',1)->whereIn('id',[12,14])->get();


        return view('AnalistInverExtjero.edit',compact('inversionista','det_usuario','accionistas','estado_observacion','pais','estado_observacion','documentos','titulo','descripcion','extencion_file_1','extencion_file_2','extencion_file_3','extencion_file_4','extencion_file_5','extencion_file_6','extencion_file_7','extencion_file_8','extencion_file_9','extencion_file_10','docCertificados','docAdicionales','extencion_adi_file_1','extencion_adi_file_2','extencion_adi_file_3','extencion_adi_file_4','extencion_adi_file_5','extencion_adi_file_6','documentosNat','documentosJuri','documentosSport','sopAdicionalesInv2','sopAdicionalesInv4'));
           
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\GenSolicitud  $genSolicitud
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, GenSolInversionista $genSolInversionista,$id)
    { 

        /* Validamos si viene lleno la observacion y hago esta cosa que es actualizar el status de la solicitud si esta en Observacion.    */
        //dd($request->observacion_inversionista);

        
            if(!empty($request->observacion_inversionista ) && $request->gen_status_id==11){
         
                 $inversionista=GenSolInversionista::find($id)->update(['gen_status_id'=>11,'estado_observacion'=>1,'observacion_inversionista'=>$request->observacion_inversionista,'updated_at' => date("Y-m-d H:i:s") ]);

                 $bandejainversionista=BandejaSolInversionista::where('gen_sol_inversionista_id',$id)->update(['gen_status_id'=>11,'updated_at' => date("Y-m-d H:i:s") ]);

               
            }elseif(!empty($request->observacion_inversionista ) && $request->gen_status_id==13){
              
              $inversionista=GenSolInversionista::find($id)->update(['gen_status_id'=>13,'estado_observacion'=>1,'observacion_anulacion'=>$request->observacion_inversionista,'updated_at' => date("Y-m-d H:i:s") ]);

               $bandejainversionista=BandejaSolInversionista::where('gen_sol_inversionista_id',$id)->update(['gen_status_id'=>13,'updated_at' => date("Y-m-d H:i:s") ]);


            }elseif(!empty($request->observacion_inversionista ) && $request->gen_status_id == 15){
              
              $inversionista=GenSolInversionista::find($id)->update(['gen_status_id'=>15,'estado_observacion'=>1,'observacion_doc_incomp'=>$request->observacion_inversionista,'updated_at' => date("Y-m-d H:i:s") ]);

               $bandejainversionista=BandejaSolInversionista::where('gen_sol_inversionista_id',$id)->update(['gen_status_id'=>15,'updated_at' => date("Y-m-d H:i:s") ]);

            }elseif(empty($request->observacion_inversionista ) && $request->gen_status_id == 12){
              
              $inversionista=GenSolInversionista::find($id)->update(['gen_status_id'=>12,'estado_observacion'=>0,'updated_at' => date("Y-m-d H:i:s") ]);

              $bandejainversionista=BandejaSolInversionista::where('gen_sol_inversionista_id',$id)->update(['gen_status_id'=>12,'updated_at' => date("Y-m-d H:i:s") ]);

            }elseif(!empty($request->observacion_inversionista ) && $request->gen_status_id == 19){
              
              $analistainversionista=GenSolInversionista::find($id)->update(['gen_status_id'=>19,'estado_observacion'=>1,'observacion_inversionista'=>$request->observacion_inversionista,'updated_at' => date("Y-m-d H:i:s") ]);
              $bandejainversionista=BandejaSolInversionista::where('gen_sol_inversionista_id',$id)->update(['gen_status_id'=>19,'updated_at' => date("Y-m-d H:i:s") ]);

            }elseif(!empty($request->observacion_inversionista ) && $request->gen_status_id == 26){
                
                $analistainversionista=GenSolInversionista::find($id)->update(['gen_status_id'=>26,'estado_observacion'=>1,'observacion_inversionista'=>$request->observacion_inversionista,'updated_at' => date("Y-m-d H:i:s") ]);
                $bandejainversionista=BandejaSolInversionista::where('gen_sol_inversionista_id',$id)->update(['gen_status_id'=>26,'updated_at' => date("Y-m-d H:i:s") ]);
  
            }

            $inversiones    =  GenSolInversionista::find($id);
            Mail::to($inversiones->DetUsuario->correo)->send(new NotificacionesInversiones($inversiones));

            //dd(1);
            Alert::warning('Su Solicitud ha sido Atendida ')->persistent("OK");
            return redirect()->action('ListaAnalistaInverController@index'); 

         }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\GenSolicitud  $genSolicitud
     * @return \Illuminate\Http\Response
     */
    public function destroy(GenSolInversionista $genSolInversionista)
    {
        //
    }
}
