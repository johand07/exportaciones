<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TipoInstrumento extends Model
{
    protected $table = 'tipo_instrumento';
}
