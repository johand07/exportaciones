@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
    <div class="col-md-8 col-md-offset-2"><br><br>
            <div class="panel panel-primary">
                <div class="panel-heading"><span><i class="glyphicon glyphicon-user"></i></span> <bold>Login</bold></div>

                @if(!isset($preguntas_seg))
                    <div class="panel-body">

                            {{ Form::open(['action'=>'SesionController@consultUser','method'=>'POST'])}}
                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="email" class="col-md-4 control-label">Correo Electrónico</label>

                                <div class="col-md-6">
                                    <div class="input-group">
                                        {!!Form::email('email',null,['class'=>'form-control','id'=>'email','placeholder'=>'ejemplo@ejemplo.com','required'=>'required','autofocus'=>'true'])!!}
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
                                    </div>

                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <br><br><br>
                            <div class="form-group{{ $errors->has('rif') ? ' has-error' : '' }}">

                            <label for="rif" class="col-md-4 control-label">Ingrese el RIF </label>

                            <div class="col-md-8">

                                <table>
                                    <tr>
                                        <td>
                                            {!!Form::select('select_tipoPersona',array('V'=>'V', 'E' => 'E','J'=>'J','P'=>'P','G'=>'G'),null,['class'=>'form-control','id'=>'select_tipoPersona','placeholder'=>'', 'onchange="validarPasaporte()"'])!!}
                                        </td>

                                        <td>
                                            {!!Form::text('rif',null,['class'=>'form-control','placeholder'=>'J-00000001, V-00000001','onkeypress="return soloNumeros(event)"','maxlength'=>'9','id'=>'rif'])!!}
                                        </td>
                                    </tr>

                                </table>


                                    <span class="help-block">Ejemplo: V-00000001 , J-00000001, G-00000001</span>
                                    @if ($errors->has('rif'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('rif') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4"><br><br>
                                    <button type="submit" class="btn btn-primary btn-lg btn-block">
                                        Enviar
                                    </button>

                                    <!--<a class="btn btn-link" href="{{ url('/passwordReset') }}">
                                        Olvidaste tu contraseña?
                                    </a>-->
                                </div>
                            </div>
                            {{Form::close()}}
                    </div>
                @else 
                    <div class="panel-body">

                        {{ Form::open(['action'=>'SesionController@resetPassword','method'=>'POST'])}}
                            <br>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="email" class="col-md-2 control-label"></label>

                                    <div class="col-md-8">
                                       
                                            {{ $preguntas_seg->descripcion_preg }}
                                        
                                    </div>
                                </div>
                            </div><br><br>
                            <div class="col-md-12">
                                <div>
                                    <label for="respuesta_seg" class="col-md-2 control-label">Respuesta</label>

                                    <div class="col-md-8">
                                        

                                        
                                        {!!Form::text('respuesta_seg',null,['class'=>'form-control','id'=>'respuesta_seg','placeholder'=>'Introduzca su respuesta de Seguridad','required'=>'required'])!!}
                                        {!!Form::hidden('cat_preguntas_seg_id',$preguntas_seg->id)!!}
                                        {!!Form::hidden('gen_usuario_id',$gen_usuario_id)!!}
                                           
                                        
                                        @if ($errors->has('respuesta_seg'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('respuesta_seg') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4"><br><br>
                                    <button type="submit" class="btn btn-primary btn-lg btn-block">
                                        Enviar
                                    </button>

                                    <!--<a class="btn btn-link" href="{{ url('/passwordReset') }}">
                                        Olvidaste tu contraseña?
                                    </a>-->
                                </div>
                            </div>
                        {{Form::close()}}
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>

@include('templates/footer')
@endsection