@extends('templates/layoutlte_admin')

@section('content')
 
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="col-md-3"></div>
      <div class="col-md-6">
      {!! Form::open(['route'=>'AdminUsuario.store', 'method'=>'POST','id'=>'formadminusuario']) !!}
          
          <div class="form-group">
                {!! Form::label('', 'Email principal') !!}
                {!! Form::text('email', null, ['class' => 'form-control','id'=>'email']) !!}
          </div>
          <div class="form-group">
            {!! Form::label('', 'Email secundario') !!}
            {!! Form::text('email_seg', null, ['class' => 'form-control','id'=>'email_seg']) !!}
          </div>
          <div class="form-group">
            {!! Form::label('','Tipo de Usuario') !!}
            {!! Form::select('gen_tipo_usuario_id',$tipousu,null,['class'=>'form-control',
            'placeholder'=>'Seleccione el Tipo de Usuario','id'=>'gen_tipo_usuario_id','onchange'=>'operadorcambiario()']) !!}
          </div>
          <div class="form-group" id="oca" style="display: none;">
            {!! Form::label('','Operador Cambiario') !!}
            {!! Form::select('gen_operador_cambiario_id',$oca,null,['class'=>'form-control',
            'placeholder'=>'Seleccione el Tipo de Usuario','id'=>'gen_operador_cambiario_id']) !!}
          </div>
           <div class="form-group">
            {!! Form::label('','Estatus') !!}
            {!! Form::select('gen_status_id',$statususu,null,['class'=>'form-control',

            'placeholder'=>'Seleccione un estatus','id'=>'gen_status_id']) !!}

          <div class="form-group">
            {!! Form::label('', 'Contraseña') !!}<br>
            {!! Form::password('password',['class' => 'form-control','id'=>'password']) !!}
          </div>

          <div class="form-group">
            {!! Form::label('', 'Confirmar Contraseña') !!}<br>
            {!! Form::password('password_confirm',['class' => 'form-control','id'=>'password_confirm']) !!}
          </div>
          <div class="form-group" >

            <div id="lista_preg">
              {!! Form::label('', 'Pregunta de Seguridad') !!}
              {!! Form::select('cat_preguntas_seg_id',$preguntas, null, ['class' => 'form-control','placeholder'=>'Seleccione una pregunta','id'=>'preguntas_seg','onchange'=>'preguntaSeg ()']) !!}
            </div>

            <div class="form-group" id="preg_alternativa" style="display: none;">
              {!! Form::label('', 'Pregunta Alternativa') !!}
              {!! Form::text('pregunta_alter',null,['class'=>'form-control','id'=>'pregunta_alter']) !!}
              <span style="color:#3097D1;cursor:pointer;" id="boton">lista de preguntas</span>
            </div>
          </div>
          <div class="form-group">
            {!! Form::label('respuesta_p1', 'Respuesta') !!}
            {!! Form::text('respuesta_seg', null, ['class' => 'form-control','id'=>'respuesta_seg','onkeypress'=>'return soloNumerosyLetras(event)']) !!}
          </div>

          <div class="form-group text-center">
             <a class="btn btn-primary" href="{{url('admin/Administradores')}}">Cancelar</a>
               <input class="btn btn-success" type="submit" name="" value="Enviar" onclick="enviaradminusuario()">
          </div>
        {!! Form::close() !!}
        </div>
        <div class="col-md-3" ></div>
      </div>
    </div>
  </div>

@stop
