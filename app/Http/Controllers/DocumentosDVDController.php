<?php

namespace App\Http\Controllers;
use Illuminate\Filesystem\Filesystem;
use App\Models\GenDVDSolicitud;
use App\Models\DocumentosDvdBdv;
use App\Models\GenOperadorCamb;
use App\Models\GenFactura;
use App\Models\FacturaSolicitud;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Session;
use Auth;
use Alert;
use File;

class DocumentosDVDController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //dd($request->all());
        $titulo='Documentos Demostración de Ventas de Divisa';
        $descripcion='Perfil del Exportador';


        $solicitud_Dvd=GenDVDSolicitud::where('id',$request->id)->first();
        $Doc_Dvd=DocumentosDvdBdv::where('gen_dvd_solicitud_id',$request->id)->first();
        // dd($Doc_Dvd);


        return view('DocumentosDVD.documentosdvd',compact('titulo','descripcion','solicitud_Dvd','Doc_Dvd'));
    }
    public function deleteDvd(Request $request)
    {
        //$genDVDSolicitud=$request->genDVDSolicitud;
        $id=$request->id;
        $updateventa= GenDVDSolicitud::where('gen_solicitud_id',$id)->update(['bactivo' => 0]);
       // $deleteventa = GenDVDSolicitud::where('gen_solicitud_id', $id)->delete();
        $updatefactura= FacturaSolicitud::where('gen_solicitud_id',$id)->update(['bactivo' => 1]);
       
       $titulo="Declaración de Ventas de Divisas";
        $descripcion="Ventas de Divisas Registradas Solicitud Exportación Realizada (ER)";
        $listaventa=GenDVDSolicitud::where('gen_solicitud_id',$id)->where('bactivo',1)->get();
        //dd($id);
        // $Doc_Dvd=DocumentosDvdBdv::where('gen_dvd_solicitud_id',$listaventa->id)->first();
        $idsolicitud=$id;
        //dd($listaventa);
        if (!empty($listaventa[0])) {
           // return view('ListaVenta.edit',compact('listaventa','titulo','descripcion','idsolicitud'));
           return 0;
        } else {
            //dd($listaventa);
           // Alert::warning('Debe registrar por lo menos una', 'No hay venta de divisa Registrada!')->persistent("Ok");
          //  return view('ListaVenta.edit',compact('listaventa','titulo','descripcion','idsolicitud'));
          return 1;

        }
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $destino = '/img/docbdv/'.Auth::user()->detUsuario->cod_empresa;
        $destinoPrivado = public_path($destino);
        
        if (!file_exists($destinoPrivado)) {
            $filesystem = new Filesystem();
            $filesystem->makeDirectory($destinoPrivado, $mode = 0777, true, true); 
        }

      //dd(count($request->ruta_doc_dvd));

        if(isset($request->ruta_doc_dvd)){
         for( $i=0; $i <=count($request->ruta_doc_dvd)-1 ; $i++ ){

            $imagen = $request->ruta_doc_dvd[$i];
            if (!empty($imagen)) {
               
               $nombre_imagen = $imagen->getClientOriginalName();
              
                $imagen->move($destinoPrivado, $nombre_imagen);
                
               
                $file = new DocumentosDvdBdv;

                $file->gen_dvd_solicitud_id      = $request->gen_dvd_solicitud_id;
                $file->cat_documentos_id  = $request->cat_documentos_id[$i];
                $file->ruta_doc_dvd       = $destino.'/'.$nombre_imagen;
                $file->save();
                
            }
            

        }
      }
            Alert::success('Se han guardado los documentos correspondientes!','Cargados Correctamente!')->persistent("Ok");

         return redirect('exportador/DvdSolicitud');      

     }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\GenDVDSolicitud  $genDVDSolicitud
     * @return \Illuminate\Http\Response
     */
    public function show(GenDVDSolicitud $genDVDSolicitud)
    {

    /**
     * Show the form for editing the specified resource.
     *

     * @param  \App\Models\GenDVDSolicitud  $genDVDSolicitud
     * @return \Illuminate\Http\Response
     */

    }
           //

    public function edit(Request $request, $id)
    {
        $titulo='Documentos Demostración de Ventas de Divisa';
        $descripcion='Perfil del Exportador';

       
        /*********Consulta de general de dvd solicitud y documentos***********/
        $solicitud_Dvd=GenDVDSolicitud::where('id',$id)->first();
        

        $Doc_Dvd=DocumentosDvdBdv::where('gen_dvd_solicitud_id',$id)->first();

        $NotaDebitos=DocumentosDvdBdv::where('gen_dvd_solicitud_id',$id)->where('cat_documentos_id',9)->get();
        //dd($NotaDebitos);
        $NotaCreditos=DocumentosDvdBdv::where('gen_dvd_solicitud_id',$id)->where('cat_documentos_id',43)->get();

        $docEmbarques=DocumentosDvdBdv::where('gen_dvd_solicitud_id',$id)->where('cat_documentos_id',42)->get();

        $CopiaSwifts=DocumentosDvdBdv::where('gen_dvd_solicitud_id',$id)->where('cat_documentos_id',6)->get();

        $Facturas=DocumentosDvdBdv::where('gen_dvd_solicitud_id',$id)->where('cat_documentos_id',8)->first();
        //dd($Facturas);
        $Ivas=DocumentosDvdBdv::where('gen_dvd_solicitud_id',$id)->where('cat_documentos_id',3)->first();


        return view('DocumentosDVD.edit',compact('titulo','descripcion','solicitud_Dvd','Doc_Dvd','NotaDebitos','NotaCreditos','docEmbarques','CopiaSwifts','Facturas','Ivas'));
    
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\GenDVDSolicitud  $genDVDSolicitud
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, GenDVDSolicitud $genDVDSolicitud)
    {

        $destino = '/img/docbdv/'.Auth::user()->detUsuario->cod_empresa;
        $destinoPrivado = public_path($destino);
        
        if (!file_exists($destinoPrivado)) {
            $filesystem = new Filesystem();
            $filesystem->makeDirectory($destinoPrivado, $mode = 0777, true, true); 
        }

      //dd(count($request->ruta_doc_dvd));
      if(isset($request->ruta_doc_dvd)){
        for( $i=0; $i <=count($request->ruta_doc_dvd)-1 ; $i++ ){

            $imagen = $request->ruta_doc_dvd[$i];
            if (!empty($imagen)) {
               
               $nombre_imagen = $imagen->getClientOriginalName();
              
                $imagen->move($destinoPrivado, $nombre_imagen);
                
               
                $file = new DocumentosDvdBdv;

                $file->gen_dvd_solicitud_id  = $request->gen_dvd_solicitud_id;
                $file->cat_documentos_id  = $request->cat_documentos_id[$i];
                $file->ruta_doc_dvd       = $destino.'/'.$nombre_imagen;
                $file->save();
                
            }
            

        }
      }

        Alert::success('Sus Documentos han sido Actualizados!','Actualización Satisfactoria!')->persistent("Ok");

        return redirect('exportador/ListaVenta/'.$request->gen_solicitud_id.'/edit');   

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\GenDVDSolicitud  $genDVDSolicitud
     * @return \Illuminate\Http\Response
     */
    public function destroy(GenDVDSolicitud $genDVDSolicitud)
    {
        //
    }

    public function eliminarDocDvd(Request $request, $idSolicitud, $id, $documentoId)
    {
        $factura=GenFactura::where('id',$documentoId )->get();
        foreach ($factura as $key => $value) {
            $realizoVenta = GenDVDSolicitud::where('id', $id)
            ->where('gen_factura_id', $documentoId)
            ->update([
                'bactivo' => 0,
                'mpendiente'=>$value->monto_fob,
                'mpercibido'=>0
            ]);
/*
            $realizoVenta = GenDVDSolicitud::where('id', $id)
                ->where('gen_factura_id', $documentoId)
                ->delete();*/

            $facturasolic = facturasolicitud::where('gen_factura_id', $documentoId)->update([ 'bactivo' => 1]);
        }
    
     // $delete=GenDVDSolicitud::find($id)->delete();

      $titulo="Declaración de Ventas de Divisas";
      $descripcion="Ventas de Divisas Registradas Solicitud Exportación Realizada (ER)";
      $listaventa=GenDVDSolicitud::where('gen_solicitud_id',$idSolicitud)->where('bactivo',1)->get();
      //dd($id);
      // $Doc_Dvd=DocumentosDvdBdv::where('gen_dvd_solicitud_id',$listaventa->id)->first();
      $idsolicitud=$id;
      //dd($listaventa);
      if (!empty($listaventa)) {
          return view('ListaVenta.edit',compact('listaventa','titulo','descripcion','idsolicitud'));
      } else {
          //dd($listaventa);
           Alert::warning('Debe registrar por lo menos una', 'No hay venta de divisa Registrada!')->persistent("Ok");
          return view('ListaVenta.edit',compact('listaventa','titulo','descripcion','idsolicitud'));

      }


    }

}
