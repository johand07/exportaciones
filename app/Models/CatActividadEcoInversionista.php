<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CatActividadEcoInversionista extends Model
{
    protected $table='cat_act_eco_inversionista';

    protected $fillable =[
     'id',
     'cat_sector_inversionista_id',
     'nombre',
     'bactivo'
    ];

    public static function actividadecoInvert ($id){
    	return CatActividadEcoInversionista::where('cat_sector_inversionista_id','=',$id)->get();
    }

    public function actividadecosectorInvert() {

     return $this->hasOne('App\Models\CatSectorInversionista','id','cat_sector_inversionista_id');

    
    }
}
