<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GenSolInversionista extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('gen_sol_inversionista', function (Blueprint $table) {
          $table->engine = 'InnoDB';
          $table->increments('id');

          $table->string('num_sol_inversionista',100)->nullable();

          $table->unsignedInteger('gen_usuario_id')->index()->nullable();
          $table->foreign('gen_usuario_id')->references('id')->on('gen_usuario');

          $table->unsignedInteger('gen_status_id')->index()->nullable();
          $table->foreign('gen_status_id')->references('id')->on('gen_status');

          $table->unsignedInteger('pais_id')->index()->nullable();
          $table->foreign('pais_id')->references('id')->on('pais');

          $table->unsignedInteger('cat_tipo_visa_id')->index()->nullable();
          $table->foreign('cat_tipo_visa_id')->references('id')->on('cat_tipo_visa');

          $table->date('fstatus')->nullable();

          $table->boolean('estado_observacion')->default(0)->nullable();
          
          $table->string('observacion_inversionista', 1500)->nullable();
         
          $table->boolean('bactivo')->default(1);
          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::dropIfExists('gen_sol_inversionista');
    }
}
