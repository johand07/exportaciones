@extends('templates/layoutlte')
@section('content')

<!--Aqui Form::open-->
{{ Form::model($certificado, array('route' => array('CertificadoOrigen.update', $certificado->id), 'method' => 'PUT', 'id'=>'FormProd', 'enctype' => 'multipart/form-data')) }}

@php
$observacion = isset($certificado->observacion_analista)? $certificado->observacion_analista : $certificado->observacion_coordinador;


function namefile($name){
  $explode=explode("/",$name);
  return $explode[3];
  
}
@endphp

@if( $certificado->gen_status_id == 11 && (!empty($certificado->observacion_analista ) || !empty($certificado->observacion_coordinador ) ) )
@php $contador_prod=count($det_prod_cer); $contador_criterio=count($det_decla_cert); @endphp
{{Form::hidden('contador',$contador_prod)}}
{{Form::hidden('contador_criterio',$contador_criterio)}}
            <script>

              document.onreadystatechange = function () {
                var state = document.readyState;
                if (state == 'complete') {
                  swal("Estimado usuario debera corregír las siguientes observaciones", "{{$observacion}}", "warning");
                }
              }
            </script>
            <br>

			<div id="space_alert1" class="alert alert-danger" role="alert" style="display: block;">
				<strong><span class="glyphicon glyphicon-circle-arrow-right"></span> Observación Indicada por el Analista: {{$observacion}}</strong>
			  </div>

          @endif  
<input type="hidden" name="gen_tipo_certificado_id" id="gen_tipo_certificado_id" value="{{$certificado->gen_tipo_certificado_id}}" />
<div class="content">
	<div class="panel-group">

		<div class="panel-primary" style="background-color: #fff">
			<div class="panel-heading"><h3>Carga de Documentos </h3></div>
			<div class="panel-body">
				
            <div class="row"><br>
						<div class="col-md-12">
							<div class="col-md-6">
								<tr>
								
									<td class="text-center" valign="top" > 
									<span><b><p style="font-size:14px;">1.) Cargar el formato de Solicitud de Certificado de Origen</p></b></span>
								
									<span class="btn btn-default btn-file" style="margin: 3px;">
										Cargar Archivo <span class="glyphicon glyphicon-file"></span>
										{!! Form::file('file_1', array('class' => 'file-input','style'=>'margin: 10px;','id'=>'file_1','title'=>'formatos permitidos .doc, .docx, .ppt, .pptx, .pdf')) !!}
									</span>
									<input type="hidden" value="{{$doc_cert->id}}" name="file_id">
									</td>
									<td class="text-center" height="40px"><img id="" src="" alt="" width="300px" style="margin: 3px;"><img id="imgdocumento_file_1" src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px; display: none;">
											<div id="nombre_file_1"></div><!--Craga la 1era vez el nombre del Documento-->

                                          
                                          @if(isset($doc_cert->file_1))
                                          	<div id="imgdocumentoEdit_file_1">
                                           		 <img id="imgdocumentoEdit_file_1" src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px;">
                                           		  {{namefile($doc_cert->file_1)}}<!--Carga Nombre del documento en el edit con namefile que esta arriba dentro de php en una funcion-->
                                           	</div>
                                          @endif
                                    </td>
								</tr>
							</div>
							<div class="col-md-6">
								<tr>
									<td class="text-center" valign="top" > 
									<span><b><p style="font-size:14px;">4.) Pago por concepto de solicitud de certificado de origen (0,25 petros)</p></b></span>
									
									<span class="btn btn-default btn-file" style="margin: 3px;">
										Cargar Archivo <span class="glyphicon glyphicon-file"></span>
										{!! Form::file('file_4', array('class' => 'file-input','style'=>'margin: 10px;','id'=>'file_4','title'=>'formatos permitidos .doc, .docx, .ppt, .pptx, .pdf')) !!}
									</span>
									</td>
									<td class="text-center" height="40px"><img id="" src="" alt="" width="300px" style="margin: 3px;"><img id="imgdocumento_file_4" src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px; display: none;">
										<div id="nombre_file_4"></div><!--Craga la 1era vez el nombre del Documento-->


										@if(isset($doc_cert->file_4))
											<div id="imgdocumentoEdit_file_4">

												<img src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px;">
												{{namefile($doc_cert->file_4)}}
											</div>
										@endif
									</td>
								</tr>
							</div>
						</div>
						<br><br><br>
                        <div class="col-md-12"><br><br><br>
							<div class="col-md-6">
								<tr>
									<td class="text-center" valign="top" > 
									<span><b><p style="font-size:14px;">2.) Declaración jurada de Origen</p></b></span>
									
									<span class="btn btn-default btn-file" style="margin: 3px;">
										Cargar Archivo <span class="glyphicon glyphicon-file"></span>
										{!! Form::file('file_2', array('class' => 'file-input','style'=>'margin: 10px;','id'=>'file_2','title'=>'formatos permitidos .doc, .docx, .ppt, .pptx, .pdf')) !!}
									</span>
									</td>
									<td class="text-center" height="40px"><img id="" src="" alt="" width="300px" style="margin: 3px;"><img id="imgdocumento_file_2" src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px; display: none;">
										<div id="nombre_file_2"></div><!--Craga la 1era vez el nombre del Documento-->


										@if(isset($doc_cert->file_2))
											<div id="imgdocumentoEdit_file_2">
												<img src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px;">
												{{namefile($doc_cert->file_2)}}
											</div>
										@endif
									</td>
								</tr>
							</div>
							<div class="col-md-6">
								<tr>
									<td class="text-center" valign="top" > 
									<span><b><p style="font-size:14px;">5.) Pago de 0,02 U.T Gobierno del Distrito Capital</p></b></span>
									
									<span class="btn btn-default btn-file" style="margin: 3px;">
										Cargar Archivo <span class="glyphicon glyphicon-file"></span>
										{!! Form::file('file_5', array('class' => 'file-input','style'=>'margin: 10px;','id'=>'file_5','title'=>'formatos permitidos .doc, .docx, .ppt, .pptx, .pdf')) !!}
									</span>
									</td>
									<td class="text-center" height="40px"><img id="" src="" alt="" width="300px" style="margin: 3px;"><img id="imgdocumento_file_5" src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px; display: none;">
											<div id="nombre_file_5"></div><!--Craga la 1era vez el nombre del Documento-->

										@if(isset($doc_cert->file_5))
											<div id="imgdocumentoEdit_file_5">
												<img src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px;">
												{{namefile($doc_cert->file_5)}}
											</div>
										@endif
									</td>
								</tr>
							</div>
						</div><br><br><br>


						<div class="col-md-12"><br><br><br>
							<div class="col-md-6">
								<tr>
									<td class="text-center" valign="top" > 
									<span><b><p>3.) Factura comercial de exportador</p></b></span>
									
									<span class="btn btn-default btn-file" style="margin: 3px;">
										Cargar Archivo <span class="glyphicon glyphicon-file"></span>
										{!! Form::file('file_3', array('class' => 'file-input','style'=>'margin: 10px;','id'=>'file_3','title'=>'formatos permitidos .doc, .docx, .ppt, .pptx, .pdf')) !!}
									</span>
									</td>
									<td class="text-center" height="40px"><img id="vista_previa_file_3" src="" alt="" width="300px" style="margin: 3px;"><img id="imgdocumento_file_3" src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px; display: none;">
										<div id="nombre_file_3"></div><!--Craga la 1era vez el nombre del Documento-->

										@if(isset($doc_cert->file_3))
											<div id="imgdocumentoEdit_file_3">
												<img id="imgdocumentoEdit_file_3" src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px;
												">
												 {{namefile($doc_cert->file_3)}}<!--Carga Nombre del documento en el edit con namefile que esta arriba dentro de php en una funcion-->
											</div>
										@endif
									</td>
								</tr>
							</div>
							<div class="col-md-6">
								<tr>
									<td class="text-center" valign="top" > 
									<span><b><p style="font-size:14px;">6.) Factura de productor (Si es <b>Comercializadora</b>)</p></b></span>
									
									<span class="btn btn-default btn-file" style="margin: 3px;">
										Cargar Archivo <span class="glyphicon glyphicon-file"></span>
										{!! Form::file('file_6', array('class' => 'file-input','style'=>'margin: 10px;','id'=>'file_6','title'=>'formatos permitidos .doc, .docx, .ppt, .pptx, .pdf', 'noreq'=>'noreq')) !!}
									</span>
									</td>
									<td class="text-center" height="40px"><img id="" src="" alt="" width="300px" style="margin: 3px;"><img id="imgdocumento_file_6" src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px; display: none;">
											<div id="nombre_file_6"></div><!--Craga la 1era vez el nombre del Documento-->

										@if(isset($doc_cert->file_6))
											<div id="imgdocumentoEdit_file_6">
												<img  src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px;">
												 {{namefile($doc_cert->file_6)}}<!--Carga Nombre del documento en el edit con namefile que esta arriba dentro de php en una funcion-->
											</div>
										@endif
									</td>
								</tr>
							</div>
						</div>
                    </div><!--row-->
			</div>
		<br><br>
		<!---------------Item Certificado de Origen Aladi------------>
		<div class="panel-primary" style="background-color: #fff">
			<div class="panel-heading">
			<h3>Certificado de origen Aladi</h3>
			</div>
			<div class="panel-body">
				<div class="row"><br>
					<div class="col-md-6">
						<div class="form-group">
							{!! Form::label('','País Exportador') !!}
							{!! Form::text('pais_exportador',$certificado->pais_exportador,['class'=>'form-control','id'=>'pais_exportador']) !!}
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							{!! Form::label('','País Importador') !!}
							{!! Form::text('pais_importador',$certificado->pais_importador,['class'=>'form-control','id'=>'pais_importador', 'noreq'=>'noreq']) !!}
						</div>
					</div>
				</div><br_>
				<div class="row"><br><!--Agregar productos-->
					
					<table class="table table-bordered text-left" id="productos">
						<tr> <td><button  name="add" type="button" id="add-productos" value="add more" class="btn btn-success" >Agregar</button></td></tr>

						<tr>
							<th>Nº. De Orden </th>
							
							<th>Código Arancelario<br>
							</th>

							<th>Denominación de las 
								mercaderías
							</th>
							<th></th>
						
						</tr>
                        @foreach($det_prod_cer as $key => $det_prod)
                            <tr id="row{{$key+1}}"><!--row1 es para la lista dinamica de agregar productos nsantos-->
                                    <!--Lista dinamica Prodcutos-->
                            
                                <td>
                                {!!Form::text('num_orden[]',$det_prod->num_orden,['class'=>'form-control',''=>"modal",'data-target'=>"",'id'=>'num_orden',''=>'true', 'noreq'=>'noreq'])!!}
                                <input type="hidden" value="{{$det_prod->id}}" name="det_prod_certif_id[]">
                                </td>

                                <td>{!!Form::text('codigo_arancel[]',$det_prod->codigo_arancel,['class'=>'form-control','id'=>'codigo_arancel', 'noreq'=>'noreq'])!!}
                                </td>

                                <td>{!!Form::text('denominacion_mercancia[]',$det_prod->denominacion_mercancia,['class'=>'form-control','id'=>'denominacion_mercancia', 'noreq'=>'noreq'])!!}
                                </td>
                                <td>{!!Form::hidden('id_productos[]',$det_prod->id)!!}<button  name="remove" type="button" id="{{$key+1}}" value="" class="btn btn-danger btn-remove">x</button></td>
                            </tr>
                            
						@endforeach
					</table>
				</div><br>
				<div class="row">
					<center><h3>Declaración de origen</h3></center>
					<div class="col-md-6">
						<div class="form-group">
							{!! Form::label('','Factura Comercial No.') !!}
							{!! Form::text('numero_factura',$det_decla_cert[0]->numero_factura,['class'=>'form-control','id'=>'numero_factura']) !!}
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							{!! Form::label('','Normas de origen') !!}

							{!! Form::select('nombre_norma_origen',$nombre_norma_origen,$det_decla_cert[0]->nombre_norma_origen,['class'=>'form-control','id'=>'nombre_norma_origen','placeholder'=>'Seleccione la norma de origen','required'=>'true']) !!}
						</div>
					</div>
				</div><br>
				<div class="row"><br>
					<table class="table table-bordered text-left" id="criterios">
						<tr> <td><button  name="add" type="button" id="add-criterios" value="add more" class="btn btn-success" >Agregar</button></td></tr>

						<tr>
							<th>Nº. De Orden </th>
						
							<th>Normas
							</th>

						
						</tr>
                       
                        @foreach($det_decla_cert as $key => $det_declaracion)
                            <tr id="row_{{$key+1}}">

                                <td>
                                    {!!Form::text('num_orden_declaracion[]',$det_declaracion->num_orden_declaracion,['class'=>'form-control','id'=>'num_orden_declaracion', 'noreq'=>'noreq'])!!}
                                    <input type="hidden" value="{{$det_declaracion->id}}" name="det_decla_certif_id[]">
                                </td>

                                <td>{!!Form::text('normas_criterio[]',$det_declaracion->normas_criterio,['class'=>'form-control','id'=>'normas_criterio', 'noreq'=>'noreq'])!!}
                                </td>
								<td>{!!Form::hidden('id_criterios[]',$det_declaracion->id)!!}<button  name="remove" type="button" id="{{$key+1}}" value="" class="btn btn-danger btn-remove-criterio">x</button></td>
                            </tr>
                            
                        @endforeach
					</table>
				</div><br>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group"><br><br><br>
							{!! Form::label('','Fecha de Emision') !!}
							<div class="input-group date">		
								{!! Form::text('fecha_emision_exportador',null,['class'=>'form-control','readonly'=>'readonly','id'=>'fecha_emision_exportador', 'noreq'=>'noreq'])!!}<span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<h3>Exportador o productor</h3>
						<div class="form-group">					
							{!! Form::label('','Nombre o Razón Social') !!}
							{!! Form::text('razon_social',$det_usuario->razon_social,['class'=>'form-control','id'=>'razon_social', 'readonly'=>'readonly']) !!}
						</div>
					</div>
				
				</div><br>
				<div class="row">
					<div  class="col-md-12">
						<div class="form-group">
							{!! Form::label('','Observaciones') !!}
							{!! Form::textarea('observaciones',null,['class'=>'form-control','id'=>'observaciones', 'noreq'=>'noreq']) !!}
						</div>
					</div>
				</div>
				<div class="row text-center">
					<a href="{{ route('CertificadoOrigen.create')}}" class="btn btn-primary">Cancelar</a>
					<input type="submit" class="btn btn-success" value="Corregir" name="submit" id="submit" onclick="validacionForm()">
				</div><br><br>
		
			</div>
		</div>

	</div>
</div>
{{Form::close()}}
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="{{asset('js/ajax_v2.1.1.js')}}"></script>
<script src="{{asset('js/CertificadoOrigenJS/COEditAladi.js')}}"></script>

@stop