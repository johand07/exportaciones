<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocExtrasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('doc_extras', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('cert_exporta_facil_id')->nullable();
            $table->foreign('cert_exporta_facil_id')->references('id')->on('cert_exporta_facil');
            $table->unsignedInteger('cat_descrip_doc_id')->nullable();
            $table->foreign('cat_descrip_doc_id')->references('id')->on('cat_descrip_doc');
            $table->string('numero_documento', 200)->nullable();
            $table->string('entidad_emisora', 200)->nullable();
            $table->date('fecha')->nullable();
            $table->boolean('bactivo')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('doc_extras');
    }
}
