<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GenDVDSolicitud extends Model
{
    protected $table = 'gen_dvd_solicitud';

    protected $fillable = [

        'nota_debito_id',
        'gen_status_id',
        'tipo_solicitud_id',
        'gen_operador_cambiario_id',
        'cod_seguridad',
        'fdisponibilidad',
        'gen_factura_id',
        'mfob',
        'mpercibido',
        'mvendido',
        'mretencion',
        'mpendiente',
        'descripcion',
        'fstatus',
        'fventa_bcv',
        'realizo_venta',
        'bactivo',
    ];
      protected $date =['created_at','updated_at'];

    public function solicitud() {

     return $this->hasOne('App\Models\GenSolicitud','id','gen_solicitud_id');

    }

    public function estatus() {

     return $this->hasOne('App\Models\GenStatus','id','gen_status_id');

    }

    public function oca() {

     return $this->hasOne('App\Models\GenOperadorCamb','id','gen_operador_cambiario_id');

    }

    public function factura() {

     return $this->hasOne('App\Models\GenFactura','id','gen_factura_id');

    }

    public function eroca() {

     return $this->hasOne('App\Models\ErRecepcionOca','id','er_recepcion_oca_id');

    }

    public function docDvd() {

        return $this->hasOne('App\Models\DocumentosDvdBdv','gen_dvd_solicitud_id','id');

    }
}
