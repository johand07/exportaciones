->nullable()string<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GenJuridicoSeniat extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('gen_juridico_seniat', function (Blueprint $table) {
         $table->engine = 'InnoDB';
        $table->increments('ID');
         $table->string('DNOMBRE', 150)->nullable();
         $table->string('DSIGLAS', 150)->nullable();
         $table->string('CRIF', 110)->nullable();
         $table->string('CACTIVIDAD_ECO', 110)->nullable();
         $table->string('CTIPO_EMPRESA', 110)->nullable();
         $table->string('CPAIS', 110)->nullable();
         $table->string('DESTADO', 110)->nullable();
         $table->string('DCIUDAD', 110)->nullable();
         $table->string('DMUNICIPIO', 110)->nullable();
         $table->string('CPARROQUIA', 110)->nullable();
         $table->string('CTELEFONO', 110)->nullable();
         $table->string('CFAX', 110)->nullable();
         $table->string('CCELULAR', 110)->nullable();
         $table->string('CMAIL_PRIM', 100)->nullable();
         $table->string('CMAIL_SEC', 100)->nullable();
         $table->string('CREGISTRO_CIR', 20)->nullable();
         $table->string('NREGISTRO_MER', 110)->nullable();
         $table->string('CTOMO_MER', 110)->nullable();
         $table->string('CFOLIO_MER', 110)->nullable();
         $table->string('FREGISTRO_MER', 50)->nullable();
         $table->string('CCEDULA_REP', 100)->nullable();
         $table->string('DCARGO_REP', 100)->nullable();
         $table->string('DDIRECCION', 100)->nullable();
         $table->boolean('BACTIVO')->default(1);
         $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::dropIfExists('gen_juridico_seniat');
    }
}
