@extends('templates/layoutlte')
@section('content')
<div class="panels">
   <div class="row">
      <div class="col-md-1">
         <a class="btn btn-primary" href="{{url('/exportador/factura/create')}}">
            <span class="glyphicon glyphicon-file" title="Agregar" aria-hidden="true"></span>
            Nuevo
         </a>
      </div>
      <div class="col-md-1">
      </div>
      <div class="col-md-3"></div>
   </div>
   <hr>
   <div class="row">
      <div class="col-md-12">
         <table id="tablaFacturas" class="table table-striped table-bordered" style="width:100%">
            <!-- div class="col-md-1">
               Desde:
            </-div>
            <div class="col-md-2">
               <input type="date" name="fechadesde" class="form-control input-sm">
            </div>
            <div class="col-md-1">
               Hasta:
            </div>
            <div class="col-md-2">
               <input type="date" name="fechadesde" class="form-control input-sm">
            </div>
            <div class="col-md-1">
               Buscar:
            </div>
            <div class="col-md-3">
               <input type="text" name="fechadesde" class="form-control input-sm">
            </div -->
            <thead>
               <tr>
                  <th>#</th>
                  <th>Número de Factura</th>
                  @if( !is_null($id_sol) && $id_sol==1)
                  <th>Número de Dua </th>
                  @endif
                  <th>Fecha de la Factura</th>
                  <th>Monto FOB</th>
                  <th>Tipo de Solicitud</th>
                  @if($tipoExpor->productos_tecno==2)
                  <th>Número de la Dua</th>
                  @endif
                  <th>Acciones</th>
               </tr>
            </thead>
            <tbody>
               @foreach($facturas as $key=>$factura)
               <tr>
                  <td>{{$key+1}}</td>
                  <td>{{$factura->numero_factura}}</td>
                  @if( !is_null($id_sol) && $id_sol==1)
                  <td>{{$factura->dua->numero_dua}}</td>
                  @endif
                  <td>{{$factura->fecha_factura}}</td>
                  <td>{{$factura->monto_fob}}</td>
                  <td>@if(!is_null($factura->tipo_solicitud_id)){{$factura->tipoSolicitud->solicitud}}@endif</td>
                  @if($tipoExpor->productos_tecno==2)
                  <td>@if(!is_null($factura->gen_dua_id)){{ $factura->dua->numero_dua }} @endif</td>
                  @endif
                  <td>
                     <a href="{{route('factura.edit',$factura->id)}}"
                        class="glyphicon glyphicon-edit btn btn-primary btn-sm" title="Modificar"
                        aria-hidden="true"></a>
                     <input id="token" type="hidden" name="_token" value="{{ csrf_token() }}">
                     {{-- <a href="#" class="btn btn-danger btn-sm" title="Eliminar" onclick="return eliminar('facturasnatario/',{{$facturas->id}},'listaAgenteAduanal')">
                     <i class="glyphicon glyphicon-trash"></i> </a> --}}
                  </td>
               </tr>
               @endforeach
            </tbody>
         </table>
      </div>
   </div>
</div>
@stop