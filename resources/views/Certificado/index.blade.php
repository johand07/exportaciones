@extends('templates/layoutlte')

@section('content')
<!--link rel="stylesheet" href="https://rawgit.com/dbrekalo/attire/master/dist/css/build.min.css"-->
<script src="{{asset('plugins/fastselect/build.min.js')}}"></script>
<link rel="stylesheet" href="{{asset('plugins/fastselect/fastselect.min.css')}}">
<script src="{{asset('plugins/fastselect/fastselect.standalone.js')}}"></script>
<script src="{{asset('js/script_declaracionJO.js')}}"></script>
<link rel="stylesheet" href="{{asset('css/style.css')}}">


  <div class="content">
    {{Form::open(['route'=>'DeclaracionJO.store','method'=>'POST','onSubmit'=>'validacion();','id'=>'paisDJO'])}}
    <input type="text" class="hidden" name="idSolicitud" value="{{ $declaracion->id }}">
    <div class="panel-group" style="background-color: #fff;">
        <div class="panel-primary">
            <div class="panel-heading">
                  <div class="row">
                      <div class="col-md-8"><h3>(P1) - 2. Productos y Destinos de Exportación</h3></div>
                      <div class="col-md-2">
                       
                      </div>
                      <div class="col-md-2">
                         <h4>Nº Solicitud:</h4>
                        {{ $declaracion->num_solicitud }}
                        
                      </div>      
                  </div>
               
            </div>
            <div class="panel-body">
              <div class=" col-md-8 text-danger"><h4>Nota: Selecciona las Planillas desde P2 hasta P6 y Carga la Información Solicitada.</h4></div>
              <div class="col-md-1"><a class="btn btn-md btn-default" href="{{ url('exportador/ListaDeclaracionJO') }}"><span><i class="glyphicon glyphicon-list"></i></span> Volver </a></div>
              <div class="col-md-3 text-right"><button type="submit" class="text-center btn btn-md btn-success" onClick="verificarEnvioSolicitud()"><span><i class="glyphicon glyphicon-envelope"></i> </span> Enviar Declaración </button></div>
                <hr>
                <table border="1" class="col-md-12">
                  <thead>
                      <th class="text-center">Nº</th>
                      <th class="text-center">PRODUCTO</th>
                      <th class="text-center">PLANILLAS</th>
                      <th class="text-center">PAÍSES DESTINOS</th>
                  </thead>
                  <tbody>

                      <!-- productos -->
                      <!-- estatus de las planillas -->
                      <!-- 1) una planilla puede estar seleccionada como pendiente por llenar "select= active "-->
                      <!-- 2) una planilla puede estar lista y completada por el usuario "estado= true "-->
                      <!-- 3) una planilla puede estar con el enlace activo si esta seleccionada por defecto "$tab=tab" -->
                      @foreach ($declaracion->rDeclaracionProduc as $key => $producto)
                        
                        @php
                           
                          if ($producto->estado_p2==1)$estado2="true"; $tab2="tab";
                          if ($producto->estado_p3==1)$estado3="true"; $tab3="tab";
                          if ($producto->estado_p4==1)$estado4="true"; $tab4="tab";
                          if ($producto->estado_p5==1)$estado5="true"; $tab5="tab";
                          if ($producto->estado_p6==1)$estado6="true"; $tab6="tab";

                          if ($producto->estado_p2==0) {

                              #activa url 
                              $url2='href="'.route("DJOPlanilla2.index",["producto_id"=>$producto->id]).'"';
                              $tab2="";    #activa enlace 
                              $selection2="active"; #activa color de notificacion

                          }elseif ($producto->estado_p3==0 and $producto->estado_p2==1 ) {
                                        
                              $url3='href="'.route("DJOPlanilla3.index",["producto_id"=>$producto->id]).'"';
                              $tab3="";
                              $selection3="active";
                            
                               
                          }elseif ($producto->estado_p4==0 and $producto->estado_p3==1 ) {

                              $url4='href="'.route("DJOPlanilla4.index",["producto_id"=>$producto->id]).'"';
                              $tab4="";
                              $selection4="active";
                              

                          }elseif ($producto->estado_p5==0 and $producto->estado_p4==1 ) {

                              $url5='href="'.route("DJOPlanilla5.index",["producto_id"=>$producto->id]).'"';
                              $selection5="active";
                              $tab5="";
                             

                          }elseif ($producto->estado_p6==0 and $producto->estado_p5==1 ) {

                              $url6='href="'.route("DJOPlanilla6.index",["producto_id"=>$producto->id]).'"';
                              $selection6="active";
                              $tab6=""; 
                             
                          }elseif ($producto->estado_p6==1 and $producto->estado_p5==1 ) {            
                             
                              
                              $productoCompletado=$producto->estado_p2+$producto->estado_p3+$producto->estado_p4+$producto->estado_p4+$producto->estado_p5;
                              if ($productoCompletado==5)$validado="ok ok";
                          }

                        @endphp

                      <tr>
                          <td class="col-md-1 text-center">{{$key+1}}</td>
                          <td class="col-md-1 text-center">{{$producto->rProducto->descrip_comercial}}</td>

                          <td class="col-md-9">
                                <div class="col-md-12">
                                  <div class="wizard">
                                      <div class="wizard-inner">
                                          <div class="connecting-line"></div>
                                          <ul class="nav nav-tabs" role="tablist">

                                              <li role="presentation" class="{{ $selection2 or 'disabled' }}">
                                                  <a {!! $url2 or '' !!} data-toggle="{{$tab2 or 'tab'}}" aria-controls="step2" role="tab"  title="Planilla 2">
                                                      <span class="round-tab" id="round-tab-{{$estado2 or ''}}">
                                                        P2
                                                      </span>
                                                  </a>
                                              </li>
                                              <li role="presentation" class="{{ $selection3 or 'disabled' }}">
                                                  <a {!! $url3 or '' !!} data-toggle="{{$tab3 or 'tab'}}" aria-controls="step3" role="tab" title="Planilla 3">
                                                       <span class="round-tab" id="round-tab-{{$estado3 or ''}}">
                                                        P3
                                                      </span>
                                                  </a>
                                              </li>
        
                                              <li role="presentation" class="{{ $selection4 or 'disabled' }}">
                                                  <a {!! $url4 or '' !!} data-toggle="{{$tab4 or 'tab'}}" aria-controls="step4" role="tab" title="Planilla 4">
                                                      <span class="round-tab" id="round-tab-{{$estado4 or ''}}">
                                                        P4
                                                      </span>
                                                  </a>
                                              </li>
                                    
                                              <li role="presentation" class="{{ $selection5 or 'disabled' }}">
                                                  <a {!! $url5 or '' !!} data-toggle="{{$tab5 or 'tab'}}" aria-controls="step5" role="tab" title="Planilla 5">
                                                      <span class="round-tab" id="round-tab-{{$estado5 or ''}}">
                                                         P5
                                                      </span>
                                                  </a>
                                              </li>
                                              <li role="presentation" class="{{ $selection6 or 'disabled' }}">
                                                  <a {!! $url6 or '' !!} data-toggle="{{$tab6 or 'tab'}}" aria-controls="step6" role="tab" title="Planilla 6">
                                                      <span class="round-tab" id="round-tab-{{$estado6 or ''}}">
                                                          P6
                                                      </span>
                                                  </a>
                                              </li>
                                              <li role="presentation" class="{{ $selection or 'disabled' }}">
                                                  <a  title="Completado">
                                                      <span class="round-tab">
                                                          <i class="glyphicon glyphicon-{{ $validado or "lock locked" }}" id="valido_{{$key}}"></i>
                                                          
                                                      </span>
                                                  </a>
                                              </li>
                                          </ul>
                                      </div>    
                                  </div>
                                </div>
                          </td>
                          <td class=" col-md-1 text-center ">
                               
                                    <input
                                    type="text"
                                    multiple
                                    class="tagsInput"
                                    value=""
                                    placeholder="Seleccionar Destinos de Exportación"
                                    data-initial-value=''
                                    data-user-option-allowed="false"
                                    data-url="{{url('exportador/ajax/paises/all')}}"
                                    data-load-once="true"
                                    name="idPaises[{{$producto->id}}]=[]"
                                    id="idPaises_{{$key}}" />
                                    <script type="text/javascript">
                                        $('.tagsInput').fastselect();
                                    </script>
                                
                               
                          </td>
                      </tr>
                        @php
                                $url2="";$url3="";$url4="";$url5="";$url6="";
                                $selection2="disabled"; $selection3="disabled"; $selection4="disabled"; $selection5="disabled"; $selection6="disabled";
                                $estado2="";$estado3="";$estado4="";$estado5="";$estado6="";
                                $validado="lock locked";
                        @endphp
                      @endforeach
                      <!-- productos -->
                  </tbody>
                </table>
            </div>
        </div>

        <hr>

        <div class="panel-primary">
            <div class="panel-heading">
                <h3>(P1) - 3. Declaración Jurada</h3><!--span class="text-right"> Numero de Solicitud:</b></span><i class=""> {{ $declaracion->num_solicitud }}</i-->
            </div>
            <div class="panel-body">
                <div class="col-md-1 text-center">
                  <br>
                  
                  <input value="1" id="aceptaDeclaracion" type="checkbox" style="transform: scale(2.0);" class="custom-control-input" name="aceptaDeclaracion">
                </div>
                <div class="col-md-11">
                    <p class="text-justify">3.1.- DECLARO BAJO FE DE JURAMENTO QUE LA INFORMACIÓN CONTENIDA EN ESTE DOCUMENTO ES CIERTA Y EXACTA. EN VIRTUD DE ELLO, FACULTO A LAS AUTORIDADES COMPETENTES PARA COMPROBAR Y VERIFICAR LA AUTENTICIDAD DE LA MISMA, Y ASUMO LAS RESPONSABILIDADES LEGALES DERIVADAS DE LOS ERRORES U OMISIONES EN QUE HAYA INCURRIDO CON LA CONSIGNACIÓN DEL PRESENTE DOCUMENTO.</p>
                </div>
            </div>
        </div>
    </div><!-- Fin de Panel grup -->
    {{Form::close()}}
  </div><!-- Fin content -->


  <script>

    function verificarEnvioSolicitud()
    {
        var elementos = document.getElementsByClassName('tagsInput');
        
        if(!elementos==false)
        {

            var x= elementos.length;
            var mens=[
                        'Estimado Usuario. Debe ingresar el País Destino para completar el registro',
                        'Estimado Usuario. Debe llenar todas las planillas para completar el registro',
                        'Estimado Usuario. Debe aceptar que todos los datos ingresados son validos para completar el registro',
                      ];


            function validarpaisDJO(input, mensaje,num) 
            {

                if ($("#"+input).val()=="" ) 
                {
                    
                      swal("Por Favor!", mensaje, "warning");
                      
                      
                      $("#"+input).focus();
                      key=1;
                      $('#ok'+num).remove();
                      $("#"+input).css('border', '0px solid red').after('<span id="ok'+num+'" style="color:red" class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span>');
                    
                    return 1;

                }else{
                    
                      $('#ok'+num).remove();
                      $("#"+input).css('border', '0px solid green').after('<span id="ok'+num+'" style="color:green" class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span>');
                    
                    return 0;
                }


            }
            function validarradio(input, mensaje,num)
            {
                if ($("#"+input).is(':checked')) {
                    $('#ok'+num).remove()
                    $("#"+input).css('border', '1px solid green').after('<span id="ok'+num+'" style="color:green" class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span>');
                    return 0;

                }else{

                    swal("Por Favor!", mensaje, "warning")
                    $("#"+input).focus();
                    key=1;
                    $('#ok'+num).remove()
                    $("#"+input).css('border', '1px solid red').after('<span id="ok'+num+'" style="color:red" class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span>');
                    return 1;
                }
            }

            var key=0;
            

            $("#paisDJO").submit(function() 
            {
               
                for (var i=0; i<x; i++)
                {
                   
                    /* validar Planillas */
                    if ($("#valido_"+i).hasClass('locked'))
                    {
                      
                        key = validarpaisDJO('valido_'+i, mens[1],i);
                       
                        if(key == 1){return false;}

                    }
                    /* Validar Paises */
                    key = validarpaisDJO('idPaises_'+i, mens[0],i);
                    if(key == 1){return false;}

                } 
                if ($('#aceptaDeclaracion').prop('checked')==false) 
                {
                     
                     
                      key = validarradio('aceptaDeclaracion', mens[2],1);


                      if(key == 1){return false;}
                }
                    
                if (key==0) {return true;}
            });
        }

        return false;
    }

  </script>
@stop

