<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PlanillaDjir03 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('planilla_djir03', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('gen_declaracion_inversion_id');
            $table->foreign('gen_declaracion_inversion_id', 'g_d_i_fk6')->references('id')->on('gen_declaracion_inversion');
            $table->string('inversion_recursos_externos', 150)->nullable();
            $table->string('det_invert_recursos', 150)->nullable();
            $table->string('proyecto_nuevo', 150)->nullable();
            $table->string('nombre_proyecto', 150)->nullable();
            $table->string('ubicacion', 150)->nullable();
            $table->unsignedInteger('pais_id');
            $table->foreign('pais_id', 'pais_id_fk1')->references('id')->on('pais');
            $table->string('ampliacion_actual_emp', 150)->nullable();
            $table->string('ampliacion_accionaria_dir', 150)->nullable();
            $table->string('utilidad_reinvertida', 150)->nullable();
            $table->string('credito_casa_matriz', 150)->nullable();
            $table->string('creditos_terceros', 150)->nullable();
            $table->string('otra_dir', 150)->nullable();
            $table->string('especifique_directa', 150)->nullable();
            $table->string('participacion_accionaria_cart', 150)->nullable();
            $table->string('bonos_pagare')->nullable();
            $table->string('otra_cart', 150)->nullable();
            $table->string('periodo_inversion', 150)->nullable();
            $table->unsignedInteger('gen_status_id')->index();
            $table->foreign('gen_status_id')->references('id')->on('gen_status');
            $table->string('estado_observacion', 150)->nullable();
            $table->string('descrip_observacion', 150)->nullable();
            $table->boolean('bactivo')->default(1);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('planilla_djir03');
    }
}
