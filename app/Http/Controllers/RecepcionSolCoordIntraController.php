<?php

namespace App\Http\Controllers;

use App\Models\GenSolicitud;
use App\Models\GenDVDSolicitud;
use App\Models\TipoSolicitud;
use App\Models\TipoDocBanco;
use App\Models\GenUsuarioOca;
use App\Models\GenUsuario;
use App\Models\BandejaAsignacionesIntranet;
use App\Models\BandejaCoordinadorIntranet;
use App\Models\GenStatus;
use App\Models\FacturaSolicitud;
use App\Models\GenNotaCredito;
use App\Models\GenDvdNd;
use App\Models\NotaDebito;
use Illuminate\Support\Facades\DB;
use Session;
use Auth;
use Laracasts\Flash\Flash;
use Alert;
use Illuminate\Http\Request;

class RecepcionSolCoordIntraController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        return view('RecepcionSolCoordIntra.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $titulo ="Busqueda de Solicitudes";
        $descripcion ="recibida por el Banco";
        $analistas = GenUsuario::whereIn('gen_tipo_usuario_id',[18])->pluck('email','id');
        $tiposol=TipoSolicitud::whereIn('id',[3])->pluck('solicitud','id');
        // $tipodoc=TipoDocBanco::all()->pluck('descripcion_doc','id');
        $tipodoc=TipoDocBanco::whereIn('id',[2])->pluck('descripcion_doc','id');
        return view('RecepcionSolCoordIntra.create', compact('titulo','descripcion','tiposol','tipodoc','analistas'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $fecha = date("Y-m-d H");
        $analistas = GenUsuario::whereIn('gen_tipo_usuario_id',[18])->pluck('email','id');
        // dd($analistas);
        if (!empty($request->desde) && !empty($request->hasta)) {
            
            $fecha_desde = $request->desde.' 00:00:00';
            $fecha_hasta = $request->hasta.' 23:59:59';
            Session::put('fecha_desde',$fecha_desde);
            Session::put('analistas',$analistas);
            Session::put('fecha_hasta',$fecha_hasta);
        } else {
            $fecha_desde = Session::get('fecha_desde');
            $fecha_hasta = Session::get('fecha_hasta');
            $analistas = Session::get('analistas');
        }
        // dump(Session::get('analistas'));

        $titulo ="Busqueda de Solicitudes";
        $descripcion ="recibida por el Banco";
        $fecha_d = $request->desde;
        $fecha_h = $request->hasta;
        
        // if (!empty($request->desde) && !empty($request->hasta) && ($request->tipo_solicitud == 3) && ($request->tipo_documento == 2)) {
          
            $solicitudes = DB::table('gen_solicitud')
            ->leftjoin('factura_solicitud','gen_solicitud.id','=','factura_solicitud.gen_solicitud_id')

            ->join('gen_factura','factura_solicitud.gen_factura_id','=','gen_factura.id')
            ->join('tipo_solicitud','gen_solicitud.tipo_solicitud_id','=','tipo_solicitud.id')
            ->join('det_usuario','gen_solicitud.gen_usuario_id','=','det_usuario.gen_usuario_id')
            ->join('gen_operador_cambiario','gen_solicitud.coperador_cambiario','=','gen_operador_cambiario.id')
            ->select(
                    'det_usuario.rif',
                    'det_usuario.razon_social',
                    'gen_solicitud.id as gen_solicitud_id',
                    'tipo_solicitud.solicitud',
                    'factura_solicitud.numero_nota_credito',
                    'factura_solicitud.gen_factura_id',
                    'gen_factura.numero_factura',
                    'gen_operador_cambiario.nombre_oca',
                    'gen_solicitud.bactivo'
                   )
            ->where('gen_solicitud.gen_status_id',4)
            ->where('gen_solicitud.bactivo',1)
            ->whereBetween('gen_solicitud.updated_at',[$fecha_desde,$fecha_hasta])
            ->get();

           
            //consulta para pintar er recibidas por el banco
            // dd($solicitudes);
           
            

        // }
        /*Consulta forma 1 para el caso de las notas de debito

        if (!empty($request->desde) && !empty($request->hasta) && ($request->tipo_solicitud == 4) && ($request->tipo_documento == 2)) {

           $solicitudes = DB::table('gen_dvd_nd')
            ->join('nota_debito', 'gen_dvd_nd.nota_debito_id', '=', 'nota_debito.id')
            ->join('tipo_solicitud', 'gen_dvd_nd.tipo_solicitud_id', '=', 'tipo_solicitud.id')
            ->join('gen_status', 'gen_dvd_nd.gen_status_id', '=', 'gen_status.id')
            ->join('det_usuario','nota_debito.gen_usuario_id','=','det_usuario.gen_usuario_id')
            ->join('factura_solicitud','gen_dvd_nd.gen_factura_id','=','factura_solicitud.gen_factura_id')
            ->join('gen_factura','nota_debito.gen_factura_id','=','gen_factura.id') 
            ->join('gen_operador_cambiario','gen_dvd_nd.gen_operador_cambiario_id','=','gen_operador_cambiario.id')
            ->select(
                'det_usuario.rif',
                'det_usuario.razon_social',
                'gen_dvd_nd.nota_debito_id',
                'factura_solicitud.gen_solicitud_id',
                'tipo_solicitud.solicitud',
                'nota_debito.gen_factura_id',
                'gen_factura.numero_factura',
                'gen_operador_cambiario.nombre_oca'
            )
            ->where('gen_dvd_nd.gen_status_id',4)
            ->whereBetween('documentos_nd.created_at',[$fecha_desde,$fecha_hasta])
            ->get();
                dd($docdvd);
           
            
        } */
        // dd($solicitudes);
        return view('RecepcionSolCoordIntra.index', compact('solicitudes','analistas','fecha_d','fecha_h','titulo','descripcion'));
    }
    
    public function AsignacionAnalista(Request $request) {
        //dd($request->all());
        $cantidad = count($request->gen_solicitud_id);
        /* $cantidad = !empty($request->gen_solicitud_id) ? count($request->gen_solicitud_id) : 0;
        if ($cantidad == 0 || empty($request->gen_solicitud_id)) {
            Alert::warning('No se puede Enviar!','Debe seleccionar al menos una solicitud!')->persistent("Ok");
            return redirect()->back();
        } */ 
        for($i=0; $i <= $cantidad-1; $i++){
           
            if(isset($request->gen_solicitud_id[$i])){

                $consulBandeja = BandejaAsignacionesIntranet::where('gen_solicitud_id', $request->gen_solicitud_id[$i])->first();
                if(isset($consulBandeja)){

                    /* actualizamos la asignacion al usuario de las solicitudes seleccionadas */
                    $consulBandeja->gen_solicitud_id = $request->gen_solicitud_id[$i];
                    $consulBandeja->gen_usuario_id = $request->gen_usuario_id;
                    $consulBandeja->gen_status_id = 6;
                    $consulBandeja->fstatus= date('Y-m-d');
                    $consulBandeja->asignado_por = Auth::user()->id;
                    $consulBandeja->save();
                    /* Consulta la solicitudes para pasar al nuevo estatus una vez asignado */
                    $solicitud=GenSolicitud::find($request->gen_solicitud_id[$i]);
                    $solicitud->gen_status_id = 6;
                    $solicitud->fstatus=date('Y-m-d');
                    $solicitud->updated_at=date("Y-m-d H:i:s");
                    $solicitud->save();

                    $actualizar_solicitud=GenDVDSolicitud::where('gen_solicitud_id', $request->gen_solicitud_id[$i])->update(['gen_status_id' => 6, 'fstatus'=>date('Y-m-d'),'updated_at' => date("Y-m-d H:i:s") ]);
                
                }else{
                    /* insertamos la asignacion al usuario de las solicitudes seleccionadas */
                    $bandeja = new BandejaAsignacionesIntranet;
                    $bandeja->gen_solicitud_id = $request->gen_solicitud_id[$i];
                    $bandeja->gen_usuario_id = $request->gen_usuario_id;
                    $bandeja->gen_status_id = 6;
                    $bandeja->fstatus= date('Y-m-d');
                    $bandeja->asignado_por = Auth::user()->id;
                    $bandeja->save();
                    /* Consulta la solicitudes para pasar al nuevo estatus una vez asignado */
                    $solicitud=GenSolicitud::find($request->gen_solicitud_id[$i]);
                    $solicitud->gen_status_id = 6;
                    $solicitud->fstatus=date('Y-m-d');
                    $solicitud->updated_at=date("Y-m-d H:i:s");
                    $solicitud->save();

                    $actualizar_solicitud=GenDVDSolicitud::where('gen_solicitud_id', $request->gen_solicitud_id[$i])->update(['gen_status_id' => 6, 'fstatus'=>date('Y-m-d'),'updated_at' => date("Y-m-d H:i:s") ]);
                }
                  
            }
        }

        Alert::success('Se han asignado las solicitudes!','asociadas Correctamente!')->persistent("Ok");

        return redirect()->action('RecepcionSolCoordIntraController@create');
    }

    
    public function AsignadasAnalistas(Request $request) {
        $solicitudes = DB::table('bandeja_asignaciones_intranet as ba')
            ->join('gen_solicitud as gs','ba.gen_solicitud_id','=','gs.id')
            
            ->join('tipo_solicitud as ts','gs.tipo_solicitud_id','=','ts.id')
            ->join('det_usuario as du','gs.gen_usuario_id','=','du.gen_usuario_id')
            ->join('gen_operador_cambiario as  op','gs.coperador_cambiario','=','op.id')
            ->join('gen_usuario as  gu','ba.gen_usuario_id','=','gu.id')
            ->join('gen_usuario as  guas','ba.asignado_por','=','guas.id')
            ->join('gen_status as  gst','ba.gen_status_id','=','gst.id')
            ->select(
                    'du.rif',
                    'du.razon_social',
                    'gs.id as gen_solicitud_id',
                    'ts.solicitud',
                    'op.nombre_oca',
                    'gu.email as asignada_a',
                    'guas.email as asignado_por',
                    'gst.nombre_status',
                    'gs.bactivo'
                   )
            ->whereIn('gs.gen_status_id',[6,30,31,32,33,34,35])
            ->where('gs.bactivo',1)
            ->get();

            // dd($solicitudes);
            return view('RecepcionSolCoordIntra.asignadasAnalistas', compact('solicitudes'));
    }

    
    public function SolEvaluadasAnalistas(Request $request) { 
        $solicitudesSuspendidos = DB::table('bandeja_asignaciones_intranet as ba')
            ->join('gen_solicitud as gs','ba.gen_solicitud_id','=','gs.id')
            
            ->join('tipo_solicitud as ts','gs.tipo_solicitud_id','=','ts.id')
            ->join('det_usuario as du','gs.gen_usuario_id','=','du.gen_usuario_id')
            ->join('gen_operador_cambiario as  op','gs.coperador_cambiario','=','op.id')
            ->join('gen_usuario as  gu','ba.analizado_por','=','gu.id')
            ->join('gen_usuario as  guas','ba.asignado_por','=','guas.id')
            ->select(
                    'du.rif',
                    'du.razon_social',
                    'gs.id as gen_solicitud_id',
                    'ts.solicitud',
                    'op.nombre_oca',
                    'gu.email as analizado_por',
                    'guas.email as asignado_por',
                    'ba.gen_status_id',
                    'gs.observacion_analista_intra',
                    'gs.bactivo'
                   )
            ->whereIn('gs.gen_status_id',[31])
            ->where('gs.bactivo',1)
            ->get();
        
        $solicitudesCierreConformes = DB::table('bandeja_asignaciones_intranet as ba')
            ->join('gen_solicitud as gs','ba.gen_solicitud_id','=','gs.id')
            
            ->join('tipo_solicitud as ts','gs.tipo_solicitud_id','=','ts.id')
            ->join('det_usuario as du','gs.gen_usuario_id','=','du.gen_usuario_id')
            ->join('gen_operador_cambiario as  op','gs.coperador_cambiario','=','op.id')
            ->join('gen_usuario as  gu','ba.analizado_por','=','gu.id')
            ->join('gen_usuario as  guas','ba.asignado_por','=','guas.id')
            ->select(
                    'du.rif',
                    'du.razon_social',
                    'gs.id as gen_solicitud_id',
                    'ts.solicitud',
                    'op.nombre_oca',
                    'gu.email as analizado_por',
                    'guas.email as asignado_por',
                    'ba.gen_status_id',
                    'gs.observacion_analista_intra',
                    'gs.bactivo'
                   )
            ->whereIn('gs.gen_status_id',[32])
            ->where('gs.bactivo',1)
            ->get();

        $solicitudesCierreParciales = DB::table('bandeja_asignaciones_intranet as ba')
            ->join('gen_solicitud as gs','ba.gen_solicitud_id','=','gs.id')
            
            ->join('tipo_solicitud as ts','gs.tipo_solicitud_id','=','ts.id')
            ->join('det_usuario as du','gs.gen_usuario_id','=','du.gen_usuario_id')
            ->join('gen_operador_cambiario as  op','gs.coperador_cambiario','=','op.id')
            ->join('gen_usuario as  gu','ba.analizado_por','=','gu.id')
            ->join('gen_usuario as  guas','ba.asignado_por','=','guas.id')
            ->select(
                    'du.rif',
                    'du.razon_social',
                    'gs.id as gen_solicitud_id',
                    'ts.solicitud',
                    'op.nombre_oca',
                    'gu.email as analizado_por',
                    'guas.email as asignado_por',
                    'ba.gen_status_id',
                    'gs.observacion_analista_intra',
                    'gs.bactivo'
                   )
            ->whereIn('gs.gen_status_id',[33])
            ->where('gs.bactivo',1)
            ->get();

            $solicitudesEviadasArchivos = DB::table('bandeja_asignaciones_intranet as ba')
            ->join('gen_solicitud as gs','ba.gen_solicitud_id','=','gs.id')
            
            ->join('tipo_solicitud as ts','gs.tipo_solicitud_id','=','ts.id')
            ->join('det_usuario as du','gs.gen_usuario_id','=','du.gen_usuario_id')
            ->join('gen_operador_cambiario as  op','gs.coperador_cambiario','=','op.id')
            ->join('gen_usuario as  gu','ba.analizado_por','=','gu.id')
            ->join('gen_usuario as  guas','ba.finalizado_por','=','guas.id')
            ->select(
                    'du.rif',
                    'du.razon_social',
                    'gs.id as gen_solicitud_id',
                    'ts.solicitud',
                    'op.nombre_oca',
                    'gu.email as analizado_por',
                    'guas.email as finalizado_por',
                    'ba.gen_status_id',
                    'gs.observacion_analista_intra',
                    'gs.bactivo'
                   )
            ->whereIn('gs.gen_status_id',[35])
            ->where('gs.bactivo',1)
            ->get();

        return view('RecepcionSolCoordIntra.RecepcionAnalizadas', compact('solicitudesSuspendidos', 'solicitudesCierreConformes', 'solicitudesCierreParciales', 'solicitudesEviadasArchivos'));
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Models\GenSolicitud  $genSolicitud
     * @return \Illuminate\Http\Response
     */
    public function show(GenSolicitud $genSolicitud, $id)
    {
        $solicitudId = $id ? $id : '';

        // Validando si viene por get el id de la solicitud
        if (!empty($solicitudId)) {

            $solicitudId = decrypt($solicitudId) ? decrypt($solicitudId) : 'false';
            Session::put('gen_solicitud_id',$solicitudId);

        // o Validando si esta en la sesion
        } else {

            $solicitudId = Session::get('gen_solicitud_id');
        }

        // Validamos que  exista el id de la solicitud
        if(empty($solicitudId) or  $solicitudId=='false') {
              
              Alert::warning('No puede ser procesada la Solicitud.','Acción Denegada!')->persistent("OK");

              return redirect()->action('RecepcionSolCoordIntraController@SolEvaluadasAnalistas'); 

        }
        /*consultamos detalles para pintar vista create con detalles para el analisis */          
        $solicitud=GenSolicitud::where('id', $solicitudId)->where('bactivo',1)->first();
        $listaventa=GenDVDSolicitud::where('gen_solicitud_id',$solicitudId)->get();
        $facturasSolicitud=FacturaSolicitud::where('gen_solicitud_id',$solicitudId)->get();

        $facturasId = [];

        if (!empty($facturasSolicitud)) {
            foreach ($facturasSolicitud as $key => $value) {
                array_push($facturasId, $value->gen_factura_id);
            }
        }
        // dd($facturasId);
        $ncredito = GenNotaCredito::whereIn('gen_factura_id',$facturasId)->where('bactivo',1)->get();
        $NDebito=NotaDebito::whereIn('gen_factura_id',$facturasId)->where('bactivo',1)->get();
        $listaventaNd=GenDvdNd::whereIn('gen_factura_id',$facturasId)->where('tipo_solicitud_id',4)->get();
        return view('RecepcionSolCoordIntra.show', compact('solicitud', 'listaventa', 'solicitudId','ncredito','NDebito','listaventaNd'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\GenSolicitud  $genSolicitud
     * @return \Illuminate\Http\Response
     */
    public function edit(GenSolicitud $genSolicitud, $id)
    {
        $solicitudId = $id ? $id : '';

        // Validando si viene por get el id de la solicitud
        if (!empty($solicitudId)) {

            $solicitudId = decrypt($solicitudId) ? decrypt($solicitudId) : 'false';
            Session::put('gen_solicitud_id',$solicitudId);

        // o Validando si esta en la sesion
        } else {

            $solicitudId = Session::get('gen_solicitud_id');
        }

        // Validamos que  exista el id de la solicitud
        if(empty($solicitudId) or  $solicitudId=='false') {
              
              Alert::warning('No puede ser procesada la Solicitud.','Acción Denegada!')->persistent("OK");

              return redirect()->action('RecepcionSolCoordIntraController@SolEvaluadasAnalistas'); 

        }
        $validarAnalisis = BandejaCoordinadorIntranet::where('gen_solicitud_id', $solicitudId)->where('gen_usuario_id', Auth::user()->id)->first();
        if (empty($validarAnalisis)) {
            /* insertamos la atencion al usuario analista de las solicitud seleccionada */
            $bandeja = new BandejaCoordinadorIntranet;
            $bandeja->gen_solicitud_id = $solicitudId;
            $bandeja->gen_usuario_id = Auth::user()->id;
            $bandeja->gen_status_id = 34;
            $bandeja->fstatus= date('Y-m-d');
            $bandeja->save();
        }
        
        /* actualizamos la bandeja de asignaciones */
        $actualizarBandejaAsignaciones=BandejaAsignacionesIntranet::where('gen_solicitud_id', $solicitudId)->update(['gen_status_id' => 34,'fstatus'=>date('Y-m-d'),'updated_at' => date("Y-m-d H:i:s") ]);
        /* Consulta la solicitudes para pasar al nuevo estatus una vez atendido */
        $solicitud=GenSolicitud::find($solicitudId);
        $solicitud->gen_status_id = 34;
        $solicitud->fstatus=date('Y-m-d');
        $solicitud->updated_at=date("Y-m-d H:i:s");
        $solicitud->save();
        /* Actaulizamos dvds */
        $actualizar_dvds=GenDVDSolicitud::where('gen_solicitud_id', $solicitudId)->update(['gen_status_id' => 34, 'fstatus'=>date('Y-m-d'),'updated_at' => date("Y-m-d H:i:s") ]);
        
        /*consultamos detalles para pintar vista create con detalles para el analisis */          
        $solicitud=GenSolicitud::where('id', $solicitudId)->where('bactivo',1)->first();
        $listaventa=GenDVDSolicitud::where('gen_solicitud_id',$solicitudId)->get();
        $facturasSolicitud=FacturaSolicitud::where('gen_solicitud_id',$solicitudId)->get();

        $facturasId = [];

        if (!empty($facturasSolicitud)) {
            foreach ($facturasSolicitud as $key => $value) {
                array_push($facturasId, $value->gen_factura_id);
            }
        }
        // dd($facturasId);
        $ncredito = GenNotaCredito::whereIn('gen_factura_id',$facturasId)->where('bactivo',1)->get();
        $NDebito=NotaDebito::whereIn('gen_factura_id',$facturasId)->where('bactivo',1)->get();
        $listaventaNd=GenDvdNd::whereIn('gen_factura_id',$facturasId)->where('tipo_solicitud_id',4)->get();
        $status=GenStatus::whereIn('id',[35])->pluck('nombre_status','id');
        return view('RecepcionSolCoordIntra.createAnalisis', compact('solicitud', 'listaventa', 'status', 'solicitudId','ncredito','NDebito','listaventaNd'));
    }

    
    public function AnalisisCoordinador(GenSolicitud $genSolicitud, Request $request)
    {
        $solicitudId = $request->gen_solicitud_id;
        /* actualizamos la bandeja del analista con el status asignado por su evaluacion */
        $actualizarBandejaAnalista=BandejaCoordinadorIntranet::where('gen_solicitud_id', $solicitudId)->where('gen_usuario_id', Auth::user()->id)->update(['gen_status_id' => $request->gen_status_id,'fstatus'=>date('Y-m-d'),'updated_at' => date("Y-m-d H:i:s") ]);
        /* actualizamos la bandeja de asignaciones */
        $actualizarBandejaAsignaciones=BandejaAsignacionesIntranet::where('gen_solicitud_id', $solicitudId)->update(['gen_status_id' => $request->gen_status_id, 'finalizado_por'=> Auth::user()->id ,'fstatus'=>date('Y-m-d'),'updated_at' => date("Y-m-d H:i:s") ]);
        /* Consulta la solicitud para pasar al nuevo estatus una vez evaluado */
        $solicitud=GenSolicitud::find($solicitudId);
        $solicitud->gen_status_id = $request->gen_status_id;
        $solicitud->observacion_coordinador_intra = !empty($request->observacion_coordinador_intra) ? $request->observacion_coordinador_intra : null;
        $solicitud->fstatus=date('Y-m-d');
        $solicitud->updated_at=date("Y-m-d H:i:s");
        $solicitud->save();
        /* Actaulizamos dvds */
        $actualizar_dvds=GenDVDSolicitud::where('gen_solicitud_id', $solicitudId)->update(['gen_status_id' => $request->gen_status_id, 'fstatus'=>date('Y-m-d'),'updated_at' => date("Y-m-d H:i:s") ]);
        Alert::success('Se ha Evaluado la solicitud!','Evaluado Correctamente!')->persistent("Ok");

        return redirect()->action('RecepcionSolCoordIntraController@SolEvaluadasAnalistas');
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\GenSolicitud  $genSolicitud
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, GenSolicitud $genSolicitud)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\GenSolicitud  $genSolicitud
     * @return \Illuminate\Http\Response
     */
    public function destroy(GenSolicitud $genSolicitud)
    {
        //
    }

    public function ReasignarSolicitud($id)
    {
        $solicitudId = $id ? $id : '';

        // Validando si viene por get el id de la solicitud
        if (!empty($solicitudId)) {

            $solicitudId = decrypt($solicitudId) ? decrypt($solicitudId) : 'false';
          

        // o Validando si esta en la sesion
        } else {

            $solicitudId = Session::get('gen_solicitud_id');
        }

        // Validamos que  exista el id de la solicitud
        if(empty($solicitudId) or  $solicitudId=='false') {
              
              Alert::warning('No puede ser procesada la Solicitud.','Acción Denegada!')->persistent("OK");

              return redirect()->action('RecepcionSolCoordIntraController@SolEvaluadasAnalistas'); 

        }
        /*consultamos detalles para pintar vista create con detalles para el analisis */          
        
        $titulo ="Busqueda de Solicitudes";
        $descripcion ="recibida por el Banco";
      
        $analistas = GenUsuario::whereIn('gen_tipo_usuario_id',[18])->pluck('email','id');

        $solicitudes = DB::table('gen_solicitud')
            ->leftjoin('factura_solicitud','gen_solicitud.id','=','factura_solicitud.gen_solicitud_id')

            ->join('gen_factura','factura_solicitud.gen_factura_id','=','gen_factura.id')
            ->join('tipo_solicitud','gen_solicitud.tipo_solicitud_id','=','tipo_solicitud.id')
            ->join('det_usuario','gen_solicitud.gen_usuario_id','=','det_usuario.gen_usuario_id')
            ->join('gen_operador_cambiario','gen_solicitud.coperador_cambiario','=','gen_operador_cambiario.id')
            ->select(
                    'det_usuario.rif',
                    'det_usuario.razon_social',
                    'gen_solicitud.id as gen_solicitud_id',
                    'tipo_solicitud.solicitud',
                    'factura_solicitud.numero_nota_credito',
                    'factura_solicitud.gen_factura_id',
                    'gen_factura.numero_factura',
                    'gen_operador_cambiario.nombre_oca',
                    'gen_solicitud.bactivo'
                   )
            ->where('gen_solicitud.id', $solicitudId)
            ->where('gen_solicitud.bactivo',1)
            ->get();

            return view('RecepcionSolCoordIntra.index', compact('solicitudes','analistas','titulo','descripcion'));
    }
}
