<?php

namespace App\Http\Controllers;
use App\Models\GenDua;
use App\Models\GenAgenteAduanal;
use App\Models\GenTransporte;
use App\Models\GenAduanaSalida;
use App\Models\GenFactura;
use App\Models\GenConsignatario;
use App\Http\Requests\GenDuaRequest;
use App\Models\Pais;
use Session;
use Auth;
use Laracasts\Flash\Flash;
use Alert;
use Illuminate\Http\Request;

class DuaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $titulo="Declaración Única de Aduana";
        $descripcion="Duas Registradas";

        $duas=GenDua::where('gen_usuario_id',Auth::user()->id)->whereNotIn('numero_dua',['Sin Dua'])->orderBy('id','ASC')->get();

       // dd($duas);
        return view('Dua.index',compact('duas','titulo','descripcion'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $titulo="Declaración Única de Aduana";
        $descripcion="Registro de DUA";
        $agente=GenAgenteAduanal::where('gen_usuario_id',Auth::user()->id)->where('bactivo',1)->pluck('nombre_agente','id');
        $transport=GenTransporte::all()->pluck('descrip_transporte','id');
        $aduanasal=GenAduanaSalida::all()->pluck('daduana','id');
        $consignatario=GenConsignatario::where('gen_usuario_id',Auth::user()->id)->where('cat_tipo_cartera_id',1)->get()->pluck('nombre_consignatario','id');
        $pais=Pais::where('bactivo',1)->pluck('dpais','dpais');


        return view('Dua.create', compact('agente','transport','aduanasal','titulo','descripcion','consignatario','pais'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(GenDuaRequest $request)
    {
      if((strtotime($request->fregistro_dua_aduana)) > (strtotime($request->fembarque))){
        Alert::warning('No puede ser superior a la fecha de embarque', 'Error en la Fecha de Registro de DUA en Aduana!')->persistent("OK");
        return redirect()->action('DuaController@create');
      }elseif ((strtotime($request->fembarque)) > (strtotime($request->farribo))) {
          Alert::warning('No puede ser inferior a la fecha de arribo', 'Error en la Fecha de Embarque!')->persistent("OK");
          return redirect()->action('DuaController@create');
      }else{


    //consulto ususario que acabo de registrar
            $sin_dua=GenDua::where('numero_dua', '=', 'Sin Dua')->where('gen_usuario_id',Auth::user()->id)->first();

            if (empty($sin_dua)) {
                $dua_defecto=new GenDua;
                $dua_defecto->gen_usuario_id=Auth::user()->id;
                $dua_defecto->gen_agente_aduana_id  =1;
                $dua_defecto->gen_transporte_id=1;
                $dua_defecto->gen_aduana_salida_id=1;
                $dua_defecto->numero_dua='Sin Dua';
                $dua_defecto->save();

            }

        GenDua::create($request->all()); 
        Alert::success('Registro Exitoso!', 'Dua Agregada.')->persistent("OK");
        return redirect()->action('DuaController@index');
      }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\GenDua  $genDua
     * @return \Illuminate\Http\Response
     */
    public function show(GenDua $genDua)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\GenDua  $genDua
     * @return \Illuminate\Http\Response
     */
    public function edit(GenDua $genDua, $id)
    {
        $titulo="Editar Declaracion Única de Aduana";
        $descripcion="Editar Datos";
        $dua=GenDua::find($id);
        $agente=GenAgenteAduanal::all()->pluck('nombre_agente','id');
        $transport=GenTransporte::all()->pluck('descrip_transporte','id');
        $aduanasal=GenAduanaSalida::all()->pluck('daduana','id');
        $consignatario=GenConsignatario::where('gen_usuario_id',Auth::user()->id)->where('cat_tipo_cartera_id',1)->get()->pluck('nombre_consignatario','id');
        $pais=Pais::where('bactivo',1)->pluck('dpais','dpais');
        return view('Dua.edit', compact('dua','agente','transport','aduanasal','titulo','descripcion','consignatario','pais'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\GenDua  $genDua
     * @return \Illuminate\Http\Response
     */
    public function update(GenDuaRequest $request, GenDua $genDua, $id)
    {

        if((strtotime($request->fregistro_dua_aduana)) > (strtotime($request->fembarque))){
        Alert::warning('No puede ser superior a la fecha de embarque', 'Error en la Fecha de Registro de DUA en Aduana!')->persistent("OK");
        $titulo="Editar Declaracion Única de Aduana";
        $descripcion="Editar Datos";
        $dua=GenDua::find($id);
        $agente=GenAgenteAduanal::all()->pluck('nombre_agente','id');
        $transport=GenTransporte::all()->pluck('descrip_transporte','id');
        $aduanasal=GenAduanaSalida::all()->pluck('daduana','id');
        return view('Dua.edit', compact('dua','agente','transport','aduanasal','titulo','descripcion'));
      }elseif ((strtotime($request->fembarque)) > (strtotime($request->farribo))) {
          Alert::warning('No puede ser inferior a la fecha de arribo', 'Error en la Fecha de Embarque!')->persistent("OK");
          $titulo="Editar Declaracion Única de Aduana";
        $descripcion="Editar Datos";
        $dua=GenDua::find($id);
        $agente=GenAgenteAduanal::all()->pluck('nombre_agente','id');
        $transport=GenTransporte::all()->pluck('descrip_transporte','id');
        $aduanasal=GenAduanaSalida::all()->pluck('daduana','id');
        return view('Dua.edit', compact('dua','agente','transport','aduanasal','titulo','descripcion'));
      }else{
        GenDua::find($id)->update($request->all());
      Alert::success('Actualización Exitosa!', 'Actulización Exitosa.')->persistent("OK");
        return redirect()->action('DuaController@index');
      }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\GenDua  $genDua
     * @return \Illuminate\Http\Response
     */
    public function destroy(GenDua $genDua, $id)
    {
        $factura=GenFactura::where('gen_dua_id',$id)->first();
        if (!empty($factura)) {
            return 4;
        }else{
             $dua=GenDua::find($id)->update(['bactivo' => 0, 'updated_at' => date("Y-m-d H:i:s") ]);
        if ($dua) {
           return 1;
        }else{
            return 2;
        }
        }

    }
}
