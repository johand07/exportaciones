<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CostosInderectos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('costos_indirectos', function (Blueprint $table) {
          $table->engine = 'InnoDB';
          $table->increments('id');
          $table->unsignedInteger('planilla_3_id')->index()->nullable();
          $table->foreign('planilla_3_id')->references('id')->on('planilla_3');
          $table->double('gastos_admin',100)->nullable();
          $table->double('gastos_publicidad',100)->nullable();
          $table->double('gastos_ventas', 100)->nullable();
          $table->double('otros_gastos_indi', 100)->nullable();
          $table->double('total',100)->nullable(); 
          $table->boolean('bactivo')->default(1);
          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('costos_indirectos'); 
    }
}
