<?php

namespace App\Http\Controllers;

use App\Models\GenUsuario;
use App\Models\{DetUsuario, GenAsistenciaUsuario};
use Session;
use Auth;
use \App\Mail\Dudas as DudasEmail;
//use Illuminate\Mail\Message;
use \Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use Alert;

class DudasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     /** @return \Illuminate\Http\Response
     */
    public function index()
    {
        $solicitudes = [];
        $consulta_solicitudes = GenAsistenciaUsuario::select(
            'gen_asistencia_usuario.id as gen_asistencia_usuario_id', 'gen_asistencia_usuario.gen_usuario_id',
            'gen_asistencia_usuario.motivo_de_contacto','gen_asistencia_usuario.mensaje', 'gen_asistencia_usuario.respuesta',
            'gen_asistencia_usuario.gen_status_id as status', 'gen_asistencia_usuario.gen_usuario_atencion_id',
            'gen_asistencia_usuario.created_at', 'gen_status.nombre_status', 'gen_usuario.email as email'
        )
        ->where('gen_usuario_id', Auth::user()->id)
        ->join('gen_status', 'gen_asistencia_usuario.gen_status_id','=','gen_status.id')
        ->join('gen_usuario', 'gen_asistencia_usuario.gen_usuario_id','=','gen_usuario.id')
        
        ->get();
        
        foreach($consulta_solicitudes as $solicitud){
            $gen_usuario_atencion = '';
            if($solicitud->gen_usuario_atencion_id){
                $usuario_atencion = GenUsuario::where('id', $solicitud->gen_usuario_atencion_id)->first();
                $gen_usuario_atencion = $usuario_atencion->email;
            }
            
            $solicitudes[] = [
                'id' => $solicitud->gen_asistencia_usuario_id,
                'motivo_de_contacto' => $solicitud->motivo_de_contacto,
                'gen_usuario_id' => $solicitud->gen_usuario_id,
                'mensaje' => $solicitud->mensaje,
                'respuesta' => $solicitud->respuesta,
                'status' => $solicitud->status,
                'gen_usuario_atencion' => $gen_usuario_atencion,
                'nombre_status' => $solicitud->nombre_status,
                'email' => $solicitud->email,
                'created_at' => $solicitud->created_at
            ];
        }
        
        return view('Dudas.index', compact('solicitudes'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $titulo="Formulario Duda o problemas";
        $descripcion=" Registra tu duda";
        $datosempresa=DetUsuario::where('gen_usuario_id',Auth::user()->id)->first();
        //dd($datosempresa);
        
        return view('Dudas.create',compact('titulo','descripcion','datosempresa'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $email='incidenciavuce@gmail.com';
        $nombreape=$request->razon_social;
        $correo=$request->correo;
        $telefono=$request->telefono_movil;
        $tipo=$request->tipo;
        $texto=$request->mensaje;
        $gen_usuario_id = $request->gen_usuario_id;
        $motivo_de_contacto = $request->tipo;
        $mensaje = $request->mensaje;
        $agregar_duda = new GenAsistenciaUsuario();
        $agregar_duda->gen_usuario_id = $gen_usuario_id;
        $agregar_duda->motivo_de_contacto = $motivo_de_contacto;
        $agregar_duda->mensaje = $mensaje;
        $agregar_duda->gen_status_id = 9;
        $agregar_duda->save();

        $text = htmlentities($mensaje);

        //enviamos email con datos para acceso
        //Mail::to($email)->send(new DudasEmail($nombreape,$correo,$telefono,$tipo,$texto));
       // Mail::to($email)->send(new DudasEmail($nombreape,$correo,$telefono,$tipo,$text));
        Alert::success('Duda enviada!','Gracias por consultar en nuestro sistema.')->persistent("Ok");

        return redirect()->action('DudasController@index');
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Models\GenAsistenciaUsuario  $genAsistenciaUsuario
     * @return \Illuminate\Http\Response
     */
    public function show(GenAsistenciaUsuario $genAsistenciaUsuario, $id)
    {
        //
        //$solicitud = GenAsistenciaUsuario::find($id);
        $gen_usuario_atencion = '';
        $respuesta = '';
        $asistencia_usuario = [];
        $solicitud = GenAsistenciaUsuario::where('gen_asistencia_usuario.id', $id)
        ->select('gen_asistencia_usuario.id', 'gen_asistencia_usuario.gen_usuario_id','gen_asistencia_usuario.motivo_de_contacto',
            'gen_asistencia_usuario.mensaje','gen_asistencia_usuario.respuesta','gen_asistencia_usuario.gen_status_id as status',
            'gen_asistencia_usuario.gen_usuario_atencion_id as usuario_atencion_id', 'gen_asistencia_usuario.created_at',
            'gen_status.nombre_status', 'gen_usuario.email as email')
        ->join('gen_status', 'gen_asistencia_usuario.gen_status_id','=','gen_status.id')
        ->join('gen_usuario', 'gen_asistencia_usuario.gen_usuario_id','=','gen_usuario.id')
        ->first();
        if(!is_null($solicitud->usuario_atencion_id)){
            $usuario_atencion = DetUsuario::where('gen_usuario_id',$solicitud->usuario_atencion_id)->first();
        }
        
        $datosempresa = DetUsuario::where('gen_usuario_id',$solicitud->gen_usuario_id)->first();

        $asistencia_usuario = [
            'id' => $solicitud->gen_asistencia_usuario_id,
            'motivo_de_contacto' => $solicitud->motivo_de_contacto,
            'gen_usuario_id' => $solicitud->gen_usuario_id,
            'mensaje' => $solicitud->mensaje,
            'respuesta' => $solicitud->respuesta,
            'status' => $solicitud->status,
            'gen_usuario_atencion' => $gen_usuario_atencion,
            'nombre_status' => $solicitud->nombre_status,
            'email' => $solicitud->email,
            'created_at' => $solicitud->created_at
        ];
        
        return view('Dudas.show',compact('asistencia_usuario','datosempresa'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\GenAsistenciaUsuario  $gen_asistencia_usuario_id
     * @return \Illuminate\Http\Response
     */
    public function edit(GenAsistenciaUsuario $gen_asistencia_usuario_id, $id)
    {
        //
        //$solicitud = GenAsistenciaUsuario::find($id);
        $gen_usuario_atencion = '';
        $respuesta = '';
        $asistencia_usuario = [];
        $solicitud = GenAsistenciaUsuario::where('gen_asistencia_usuario.id', $id)
        ->select('gen_asistencia_usuario.id', 'gen_asistencia_usuario.gen_usuario_id','gen_asistencia_usuario.motivo_de_contacto',
            'gen_asistencia_usuario.mensaje','gen_asistencia_usuario.respuesta','gen_asistencia_usuario.gen_status_id as status',
            'gen_asistencia_usuario.gen_usuario_atencion_id as usuario_atencion_id', 'gen_asistencia_usuario.created_at',
            'gen_status.nombre_status', 'gen_usuario.email as email')
        ->join('gen_status', 'gen_asistencia_usuario.gen_status_id','=','gen_status.id')
        ->join('gen_usuario', 'gen_asistencia_usuario.gen_usuario_id','=','gen_usuario.id')
        ->first();
        if(!is_null($solicitud->usuario_atencion_id)){
            $usuario_atencion = DetUsuario::where('gen_usuario_id',$solicitud->usuario_atencion_id)->first();
        }
        
        $datosempresa = DetUsuario::where('gen_usuario_id',$solicitud->gen_usuario_id)->first();
        
        $asistencia_usuario = [
            'id' => $solicitud->id,
            'motivo_de_contacto' => $solicitud->motivo_de_contacto,
            'gen_usuario_id' => $solicitud->gen_usuario_id,
            'mensaje' => $solicitud->mensaje,
            'respuesta' => $solicitud->respuesta,
            'status' => $solicitud->status,
            'gen_usuario_atencion' => $gen_usuario_atencion,
            'nombre_status' => $solicitud->nombre_status,
            'email' => $solicitud->email,
            'created_at' => $solicitud->created_at
        ];
        
        return view('Dudas.edit',compact('asistencia_usuario','datosempresa'));
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\GenAsistenciaUsuario  $gen_asistencia_usuario_id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $modificar_mensaje = GenAsistenciaUsuario::find($id);
        $mensaje = $request->mensaje_registrado;
        $modificar_mensaje->mensaje = $mensaje;
        $modificar_mensaje->save();
        return response()->json(['success'=>'Duda actualizado exitosamente.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\GenAsistenciaUsuario $gen_asistencia_usuario_id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $duda = GenAsistenciaUsuario::find($id);
        $duda->delete();
        return response()->json(['success'=>'Duda eliminada exitosamente.']);
    }
}
