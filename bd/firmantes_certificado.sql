-- phpMyAdmin SQL Dump
-- version 4.2.12deb2+deb8u2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 18, 2019 at 11:59 AM
-- Server version: 10.0.32-MariaDB-0+deb8u1
-- PHP Version: 7.1.26-1+0~20190113101856.12+jessie~1.gbp7077bb

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `exportaciones`
--

-- --------------------------------------------------------

--
-- Table structure for table `firmantes_certificado`
--

CREATE TABLE IF NOT EXISTS `firmantes_certificado` (
`id` int(10) unsigned NOT NULL,
  `img_firma` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `img_sello` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nombre_firmante` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `apellido_firmante` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cargo_firmante` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ente_firmante` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gaceta_firmante` text COLLATE utf8mb4_unicode_ci,
  `tipo_certificado` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bactivo` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `firmantes_certificado`
--

INSERT INTO `firmantes_certificado` (`img_firma`, `img_sello`, `nombre_firmante`, `apellido_firmante`, `cargo_firmante`, `ente_firmante`, `gaceta_firmante`, `tipo_certificado`, `bactivo`, `created_at`, `updated_at`) VALUES
('/img/firma_vice_inversiones.png', '/img/sello_vice_inversiones.png', 'HÉCTOR JOSÉ', 'SILVA HERNÁNDEZ', 'Viceministro de Comercio Exterior y Promoción de Inversiones', 'Ministerio del Poder Popular para Economía, Finanzas y Comercio Exterior ', 'Decreto N° 4.372 de fecha 16/11/2020 publicado en la gaceta Oficial N°6.596 de la misma fecha.', 'Inversiones', 1, '2019-02-14 04:00:00', '2019-02-14 04:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `firmantes_certificado`
--
ALTER TABLE `firmantes_certificado`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `firmantes_certificado`
--
ALTER TABLE `firmantes_certificado`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
