<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DetProductosCertificado extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('det_productos_certificado', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('gen_certificado_id')->default(1)->index()->nullable();
            $table->foreign('gen_certificado_id')->references('id')->on('gen_certificado');
            $table->string('num_orden', 100)->nullable();
            $table->string('codigo_arancel', 100)->nullable();
            $table->string('denominacion_mercancia', 150)->nullable();
            $table->string('unidad_fisica', 100)->nullable();
            $table->double('valor_fob')->nullable();
            $table->double('cantidad_segun_factura')->nullable();
            $table->boolean('bactivo')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('det_productos_certificado');
    }
}
