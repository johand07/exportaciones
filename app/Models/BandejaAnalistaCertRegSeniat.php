<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BandejaAnalistaCertRegSeniat extends Model
{
    protected $table = 'bandeja_analista_cert_reg_seniat';
    protected $fillable = [

           'id',
           'gen_cert_reg_seniat_id',
           'gen_usuario_id',
           'gen_status_id',
           'fstatus',
           'bactivo',
    ];

    protected $date =['created_at','updated_at'];

    public function GenCertRegSeniat() {
   
        return $this->hasOne('App\Models\GenCertRegSeniat','id','gen_cert_reg_seniat_id');
   
    }
       
    public function GenUsuario() {
   
        return $this->hasOne('App\Models\GenUsuario','id','gen_usuario_id');
   
    }

    public function GenStatus() {

        return $this->hasOne('App\Models\GenStatus','id','gen_status_id');
   
    }
}
