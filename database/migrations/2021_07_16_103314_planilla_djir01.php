<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PlanillaDjir01 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('planilla_djir01', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('gen_declaracion_inversion_id');
            $table->foreign('gen_declaracion_inversion_id', 'g_d_i_fk4')->references('id')->on('gen_declaracion_inversion');
            $table->string('razon_social_solicitante', 150)->nullable();
            $table->string('rif_solicitante', 150)->nullable();
            $table->date('finicio_op')->nullable();
            $table->date('fingreso_inversion')->nullable();
            $table->boolean('emp_privada')->nullable();
            $table->boolean('emp_publica')->nullable();
            $table->boolean('emp_mixto')->nullable();
            $table->boolean('emp_otro')->nullable();
            $table->string('especifique_otro', 150)->nullable();
            $table->string('registro_publico', 150)->nullable();
            $table->string('estado_emp', 150)->nullable();
            $table->string('municipio_emp', 150)->nullable();
            $table->string('numero_r_mercantil', 150)->nullable();
            $table->string('folio', 150)->nullable();
            $table->string('tomo', 150)->nullable();
            $table->date('finspeccion')->nullable();
            $table->string('direccion_emp', 150)->nullable();
            $table->string('tlf_emp', 150)->nullable();
            $table->string('pagina_web_emp', 150)->nullable();
            $table->string('correo_emp', 150)->nullable();
            $table->string('rrss', 150)->nullable();
            $table->integer('emple_actual_directo')->nullable();
            $table->integer('emple_actual_indirecto')->nullable();
            $table->integer('emple_generar_directo')->nullable();
            $table->integer('emple_generar_indirecto')->nullable();
            $table->double('capital_suscrito')->nullable();
            $table->double('capital_apagado')->nullable();
            $table->integer('num_acciones_emitidas')->nullable();
            $table->double('valor_nominal_accion', 150)->nullable();
            $table->date('fmodificacion_accionaria');
            $table->date('finscripcion')->nullable();
            $table->string('nombre_repre', 150)->nullable();
            $table->string('apellido_repre', 150)->nullable();
            $table->string('numero_doc_identidad', 150)->nullable();
            $table->string('numero_pasaporte_repre', 150)->nullable();
            $table->string('cod_postal_repre', 150)->nullable();
            $table->string('rif_repre', 150)->nullable();
            $table->string('telefono_1', 150)->nullable();
            $table->string('telefono_2', 150)->nullable();
            $table->string('correo_repre', 150)->nullable();
            $table->string('direccion_repre', 150)->nullable();
            $table->unsignedInteger('gen_status_id')->index();
            $table->foreign('gen_status_id')->references('id')->on('gen_status');
            $table->integer('estado_observacion')->nullable();
            $table->boolean('bactivo')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('planilla_djir01');
    }
}
