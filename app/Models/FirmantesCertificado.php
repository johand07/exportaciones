<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FirmantesCertificado extends Model
{
    protected $table = 'firmantes_certificado';//Nombre de la Tabla(MIgracion)

    protected $fillable = [

    	   'img_firma',
           'img_sello',
           'nombre_firmante',
           'apellido_firmante',
           'cargo_firmante',
           'ente_firmante',
           'gaceta_firmante',
           'tipo_certificado',
        
           'bactivo',
    ];

    protected $date =['created_at','updated_at'];


}
