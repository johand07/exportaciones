<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DescripPartidaZooEu extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('descrip_partida_zoo_eu', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('gen_cert_zoo_eu_id');
            $table->foreign('gen_cert_zoo_eu_id')->references('id')->on('gen_cert_zoo_eu');
            $table->string('codigo_nc',150)->nullable();
            $table->string('especie',150)->nullable();
            $table->string('almacen_frigo',150)->nullable();
            $table->string('marc_ident',150)->nullable();
            $table->string('tipos_embalaje',150)->nullable();
            $table->string('nat_merc',150)->nullable();
            $table->string('consumidor_final',150)->nullable();
            $table->string('tipo_tratamiento',150)->nullable();
            $table->string('fecha_recogida',150)->nullable();
            $table->string('peso_neto',150)->nullable();
            $table->string('num_bultos',150)->nullable();
            $table->string('num_lotes',150)->nullable();
            $table->boolean('bactivo')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('descrip_partida_zoo_eu');
    }
}
