@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2"><br><br>
            <div class="panel panel-primary">
                <div class="panel-heading"><span><i class="glyphicon glyphicon-user"></i></span> <strong>Recuperación de clave</strong></div>

                <div class="panel-body">

                        {{ Form::open(['action'=>'SesionController@storePassword','method'=>'POST', 'id'=>'recpass'])}}
                        
                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Contraseña</label>

                            <div class="col-md-6">
                                <div class="input-group">

                                  
                                  {!!Form::password('password',['class'=>'form-control','id'=>'password','placeholder'=>'Contraseña'])!!}
                                  {!!Form::hidden('id',$genUsuario->id)!!}
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                                </div>
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <br><br>
                        <div class="form-group{{ $errors->has('password_confirm') ? ' has-error' : '' }}">
                            <label for="password_confirm" class="col-md-4 control-label">Confirmar Contraseña</label>

                            <div class="col-md-6">
                                <div class="input-group">
                                    {!!Form::password('password_confirm',['class'=>'form-control','id'=>'password_confirm','placeholder'=>'Confirmar Contraseña','autofocus'=>'true'])!!}
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                                </div>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirm') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <br><br>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary btn-lg btn-block" onclick="validacionForm()">
                                    Restablecer
                                </button>
                            </div>
                        </div>
                   {{Form::close()}}
                </div>
            </div>
        </div>
    </div>
</div>
@include('templates/footer')

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>

<script>
  //Validación de formulario, se puede llamar y pasar la sección que contenga los inputs a validar
  $(document).ready(function ()
  {
              var error = '<div class="msg_alert"><span style="color:red" class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span><strong id="cod_aran-error" class="added"> El campo es obligatorio.</strong></div>';
                var valido = '<div class="msg_alert"><span style="color:green" class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span></div>';
                var mens=['Estimado Usuario. Debe completar los campos solicitados', 'Estimado Usuario. la contraseña debe tener al menos 8 dígitos', 'Estimado Usuario. las contraseñas no coinciden', 'Estimado Usuario. la contraseña debe tener al menos una letra', 'Estimado Usuario. la contraseña debe tener al menos un número']
  
                validacionForm = function() {
  
                $('#recpass').submit(function(event) {
                
                var campos = $('#recpass').find('input:password');
                var n = campos.length;
                var err = 0;
  
                $("div").remove(".msg_alert");
                //bucle que recorre todos los elementos del formulario
                for (var i = 0; i < n; i++) {
                    var cod_input = $('#recpass').find('input:password').eq(i);
                    if (!cod_input.attr('noreq')) {
                      if (cod_input.val() == '' || cod_input.val() == null)
                      {
                        err = 1;
                        cod_input.css('border', '1px solid red').after(error);
                      }
                      else{
                        if (err == 1) {err = 1;}else{err = 0;}
                        cod_input.css('border', '1px solid green').after(valido);
                      }
                      
                    }
                }

                if(($('#password').val().length < 8) || ($('#password_confirm').val().length < 8)){
                    err = 2;
                }

                if($('#password').val() !== $('#password_confirm').val()){
                    err = 3;
                }

                if(!$('#password').val().match(/[A-z]/)){
                    err = 4;
                }

                if(!$('#password').val().match(/\d/)){
                    err = 5;
                }
  
                //Si hay errores se detendrá el submit del formulario y devolverá una alerta
                if(err==1){
                    event.preventDefault();
                    swal("Por Favor!", mens[0], "warning")
                  }
  

                if(err==2){
                    event.preventDefault();
                    swal("Por Favor!", mens[1], "warning")
                  }

                  if(err==3){
                    event.preventDefault();
                    swal("Por Favor!", mens[2], "warning")
                  }

                  if(err==4){
                    event.preventDefault();
                    swal("Por Favor!", mens[3], "warning")
                  }

                  if(err==5){
                    event.preventDefault();
                    swal("Por Favor!", mens[4], "warning")
                  }

                  });
                }
              });
  </script>

@endsection