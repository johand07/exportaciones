<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>PLANILLA RPI-01</title>
    <style>
        body{
            text-align:center;
           font-family: Arial;
                 color: #58585A;
        }
        th{
            font-size:10px;
            background-color:#818286;
            color: #fff;
        }
        td{
            font-size:10px;
            border: 1px solid #818286;
        }
         .derecha{
            text-align: right;

        }
        .td2{
            font-size:10px;
            border: 0px solid #818286;
            width: 55%;
            text-align:center;
            font-size:14px;
        }
        .td3{

            border: 0px solid #818286;
            width: 15%;
            text-align:center;

        }
        .td4{

            border: 0px solid #818286;


        }
        table{
            border: 1px solid #818286;
        }
        .table2{
            border: 0px solid #818286;
        }

       table > tr > td {

          text-align: center;
           word-wrap: break-word;
           max-width: 400px;
        }
      .tabla > h4,h6{

        text-align: left;
        margin-bottom: 0;
        font-size:10px;

      }
      .tabla > table > thead > tr > th:nth-child(1){

         width: 30%;
      }
      .tabla > table > .tipo > tr:nth-child(1){

        border-top: 0;
      }

      .tabla2 > table > thead > tr > th:nth-child(0){

         width: 10%;
      }
       .text-normal{
            font-size: 12px;
        }
        .borde{
            border: 1px solid #fff;
        }
         .sin-borde{
            border: none;
        }
          .nro-correlativo{
            color: red;
            font-size: 10px;
        }

         .centrar{
            text-align: left;
        }

        .espacio-superior{
            margin-top: 10px;
        }

         .ancho-full{
            width: 100%;
        }

         .text-11{
            font-size: 11px;
        }
.top{
            vertical-align: top;
        }


    </style>
</head>
    <body>
      <div class="centrar espacio-superior">
        <table style="width:100%;" class="table2">
          <tbody>
            <tr>
              <td class="td3">
                <img class="img-responsive" style="width:200px; height: 40px;" src='img/MIPPCOEXIN.png'>
              </td>
              <td class="borde"></td>
              <td class="borde"></td>
              <td class="borde"></td>
               <td>
                  <img class="borde img-responsive" style="width: 300px; height: 40px; text-align: right;" src='img/cintillo.png'>
              </td>
            </tr>
          </tbody>
        </table>
    </div><br>
      <br>
       <b style="font-size:14px;">PLANILLA RPI-01</b><br>
       <b style="
       font-size:14px;">PLANILLA DE REGISTRO DE POTENCIAL INVERSIONISTA EXTRANJERO</b>
       <br><br> 
        <div class="centrar espacio-superior">
            <b><img src="data:image/png;base64,{!! base64_encode(QrCode::format('png')->size(350)->generate(URL::to('/api/VerificacionRegistroInversionista/'.$solicitud_de_inversionista->num_sol_inversionista))) !!}" width="180px" height="165px"></b><br>
             <b>APROBADO</b>
        </div>
      <div class="">
        <table style="width:100%" class="table2">
          <tbody>    
              <td class="table2">
                <table style="width:80%">
                <tr>
                  <td rowspan="2" style="background-color:#818286; color:#fff;"><b>1) Tipo de Persona</b></td>
                  <td style="background-color:#818286; color:#fff;"><b>Natural </b></td>
                  <td>
                    @if($solicitud_de_inversionista->cat_tipo_usuario_id ==1)
                    x
                    @endif
                  </td>
                </tr>
                <tr>
                  <td style="background-color:#818286; color:#fff;"><b>Juridica</b></td>
                  <td>@if($solicitud_de_inversionista->cat_tipo_usuario_id ==2)
                    x
                    @endif</td>
                </tr>
                </table>
              </td>
              <td class="borde"></td>
              <td class="borde"></td>
              <td class="borde"></td>
               <td class="table2">
                <table  style="width:80%">
                  <thead>
                    <tr>
                    <th>N° RPI:</th>
                    <td><b class="nro-correlativo">{{$solicitud_de_inversionista->num_sol_inversionista}}</b></td>
                  </tr>
                  <tr>
                    <th>Lugar/fecha</th>
                    <td>{{$solicitud_de_inversionista->created_at}}</td>
                  </tr>
                  <tr>
                  <th>2) País de Origen</th>
                  <td>{{$solicitud_de_inversionista->pais->dpais}}</td>
                  </tr>
                  </thead>
                </table>
              </td>
          </tbody>
        </table>
        <br><br>
        <table style="width:100%" class="">
          <tr>
            <th>3) Nombre o razón social</th>
            <td colspan="100"><b>{{$solicitud_de_inversionista->DetUsuario->razon_social}}</td>
          </tr>
          <tr>
            <th>4) Nº de Documento de Identidad</th>
            <td colspan="45"><b>{{$solicitud_de_inversionista->n_doc_identidad}}</td>
            <th>5) Nº de Pasaporte</th>
            <td colspan="58"><b>{{$solicitud_de_inversionista->DetUsuario->ci_repre}}</td>
          </tr>
          <tr>
            <th>6) Domicilio en el exterior</th>
            <td colspan="100"><b>{{$solicitud_de_inversionista->DetUsuario->direccion}}</b></td>
          </tr>
          <tr>
             <td rowspan="2" style="background-color:#818286; color:#fff;" class="table2"><b>7) Numero telefónico</b></td>
                  <td style="background-color:#818286; color:#fff;"><b>Celular</b></td>
                  <td colspan="50">{{$solicitud_de_inversionista->DetUsuario->telefono_movil}}</td>
                   <td style="background-color:#818286; color:#fff;"><b>8) Correo electrónico solicitante</b></td>
                   <td colspan="48">{{$solicitud_de_inversionista->DetUsuario->correo}}</td>
          </tr>
           <tr>
              <td style="background-color:#818286; color:#fff;"><b>Fijo</b></td>
              <td colspan="50">{{$solicitud_de_inversionista->DetUsuario->telefono_local}}</td>
              <td style="background-color:#818286; color:#fff;" class="text-rigth"><b>9) Página Web solicitante</b></td>
              <td colspan="48">{{$solicitud_de_inversionista->DetUsuario->pagina_web}}</td>
          </tr>
          <tr>
            <th>10) Tipo de Inversión</th>
            <td colspan="100"><b>{{$solicitud_de_inversionista->tipo_inversion}}</b></td>
          </tr>
          <tr>
            <th>11) Monto de Inversión</th>
            <td colspan="100"><b>{{$solicitud_de_inversionista->monto_inversion}}</b></td>
          </tr>
        </table>
        <table style="width:100%" class="">
          <tr>
            <td style="background-color:#818286; color:#fff;" colspan="100"><b>12) Breve descripción del proyecto de inversión</b></td>
          </tr>
          <tr>
            <td colspan="100"><b>{{$solicitud_de_inversionista->descrip_proyec_inver}}</b></td>
          </tr>
        </table>
        <table style="width:100%" class="">
          <tr>
            <th>13) Actividad economica de la empresa receptora</th>
            <td colspan="100"><b>{{ $solicitud_de_inversionista->CatSectorInversionista ? $solicitud_de_inversionista->CatSectorInversionista->nombre : ''}} :
                 @if($solicitud_de_inversionista->cat_sector_inversionista_id==11)
                  {{$solicitud_de_inversionista->especif_otro_sector}}
                 @endif

                 <b style="color: black"> - </b>
                 {{ $solicitud_de_inversionista->CatActividadEcoInversionista ? $solicitud_de_inversionista->CatActividadEcoInversionista->nombre : ''}} :
                  @if($solicitud_de_inversionista->actividad_eco_emp==66 || $solicitud_de_inversionista->actividad_eco_emp==67 || $solicitud_de_inversionista->actividad_eco_emp==68 || $solicitud_de_inversionista->actividad_eco_emp==69 || $solicitud_de_inversionista->actividad_eco_emp==70 || $solicitud_de_inversionista->actividad_eco_emp==71 || $solicitud_de_inversionista->actividad_eco_emp==72 || $solicitud_de_inversionista->actividad_eco_emp==73 || $solicitud_de_inversionista->actividad_eco_emp==74 || $solicitud_de_inversionista->actividad_eco_emp==75 || $solicitud_de_inversionista->actividad_eco_emp==76)
                  {{$solicitud_de_inversionista->especif_otro_actividad}}
                 @endif</b></td>
          </tr>
          <tr>
            <th>14) Correo electrónico empresa pretendida</th>
            <td colspan="50"><b>{{$solicitud_de_inversionista->correo_emp_recep}}</b></td>
            <th colspan="25">15)  Página web empresa pretendida</th>
            <td colspan="50"><b>{{$solicitud_de_inversionista->pag_web_emp_recep}}</b></td>
          </tr>
        </table>
        <br>
        <table class="derecha" style="width:100%">
            <thead>
              <tr>
                <th style="width:50%">Firma y sello del representante legal o apoderado</th>
                <td><b>{{$solicitud_de_inversionista->nombre_repre_legal}} {{$solicitud_de_inversionista->apellido_repre_legal}}</b></td>
              </tr>
            </thead>
          </table>
         <!--table  style="width:60%">
            <thead>
              <tr>
                <th class="">3) Nombre o razón social</th>
                <td><b>{{$solicitud_de_inversionista->DetUsuario->razon_social}}</td>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table-->
           <!--table  style="width:60%">
            <thead>
              <tr>
                <th class="">4) Nº de Documento de Identidad</th>
                <td><b>{{$solicitud_de_inversionista->n_doc_identidad}}</td>
                <th>5) Nº de Pasaporte</th>
                <td><b>{{$solicitud_de_inversionista->DetUsuario->ci_repre}}</td>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table-->
          <!--table  style="width:60%">
            <thead>
              <tr>
                <th class="">6) Domicilio en el exterior</th>
                <td><b>{{$solicitud_de_inversionista->DetUsuario->direccion}}</b></td>
              </tr>
            </thead>
          </table-->
          <!--table  style="width:100%" class="table2" border="4px">
            
            <tbody>
                <tr>
                  <td rowspan="2" style="background-color:#818286; color:#fff;" class="table2"><b>7) Numero telefónico</b></td>
                  <td style="background-color:#818286; color:#fff;"><b>Celular</b></td>
                  <td>{{$solicitud_de_inversionista->DetUsuario->telefono_movil}}</td>
                   <td style="background-color:#818286; color:#fff;"><b>8) Correo electrónico</b></td>
                   <td>{{$solicitud_de_inversionista->DetUsuario->correo}}</td>
                  
                </tr>
                <tr>
                  <td style="background-color:#818286; color:#fff;"><b>Fijo</b></td>
                  <td>{{$solicitud_de_inversionista->DetUsuario->telefono_local}}</td>
                  <td style="background-color:#818286; color:#fff;" class="text-rigth"><b>9) Página Web</b></td>
                  <td>{{$solicitud_de_inversionista->DetUsuario->pagina_web}}</td>
                </tr>
                </tbody>
          
          </table-->
          <!--table  style="width:100%">
            <thead>
              <tr>
                <th>8)Correo electrónico</th>
                <th>9)Página Web</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td><b>{{$solicitud_de_inversionista->DetUsuario->correo}}</b></td>
                <td><b></b></td>
              </tr>
            </tbody>
          </table-->
           <!--table  style="width:60%">
            <thead>
              <tr>
                <th class="">10) Tipo de Inversión</th>
                <td><b>{{$solicitud_de_inversionista->tipo_inversion}}</b></td>
              </tr>
            </thead>
          </table-->
           <!--table  style="width:60%">
            <thead>
              <tr>
                <th class="">11) Monto de Inversión</th>
                <td><b>{{$solicitud_de_inversionista->monto_inversion}}</b></td>
              </tr>
            </thead>
          </table-->
           <!--table  style="width:60%">
            <thead>
              <tr>
                <th class="">12) Breve descripción del proyecto de inversión</th>
                <td><b>{{$solicitud_de_inversionista->descrip_proyec_inver}}</b></td>
              </tr>
            </thead>
          </table-->
          <!--table  style="width:60%">
            <thead>
              <tr>
                <th class="">13) Actividad economica de la empresa receptora</th>
                <td><b>{{$solicitud_de_inversionista->actividad_eco_emp}}</b></td>
              </tr>
            </thead>
          </table-->
          <!--table  style="width:100%">
            <thead>
              <tr>
                <th>14) Correo electrónico</th>
                <th>15) Página web</th>

              </tr>
            </thead>
            <tbody>
              <tr>
                <td><b>{{$solicitud_de_inversionista->correo_emp_recep}}</b></td>
                <td><b>{{$solicitud_de_inversionista->pag_web_emp_recep}}</b></td>
              </tr>
            </tbody>
          </table-->
          <br><br>
          <!--table  style="width:60%" class="table2" align="text-rigth">
            <thead>
              <tr>
                <th>Firma y sello del representante legal o apoderado</th>

              </tr>
            </thead>
            <tbody>
              <tr>
                <td><b>{{$solicitud_de_inversionista->nombre_repre_legal}} {{$solicitud_de_inversionista->apellido_repre_legal}}</b></td>
              </tr>
            </tbody>
          </table-->
      </div>
    </body>
</html>
