@extends('templates/layoutlte_seniat')

@section('content')
<div class="row"> 
    <div class="col-md-12">
        <div id="datos_persona1">
            <div class="panel panel-primary">
            <div class="panel-heading">CERTIFICADO DE ORIGEN</div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-6">
                            <label>Código Arancelario</label><br>
                            <strong>{{ $certificado->codigo_arancelario }}</strong><br><br><br>

                            <label>Archivo</label><br>
                            <a href="{{ asset($file->file) }}" download="Certificado" class="btn btn-primary btn-lg">Certificado de Origen</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            
                        </div>
                    </div>
                   
                        </div>
                    @if($certificado->gen_status_id == 27)
                        <div class="panel-footer">
                            <div class="row">
                                <div class="col-md-6 text-center">
                                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                                            Desaprobar
                                    </button>
                                </div>
                                <div class="col-md-6 text-left">
                                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal1">
                                            Aprobar
                                    </button>
                                </div>
                        </div>
                    @endif
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="exampleModalLabel">Aspectos a revisar en el Certificado de Origen del Acuerdo Comercial con Colombia</h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            {{Form::open(['route' =>'AnalistaSeniat.store' ,'method'=>'POST','id'=>''])}}
            <div class="modal-body">
                
                    <div class="form-group">
                        <label for="observaciones">Observaciones</label>
                        <textarea name="observacion_analista" class="form-control"></textarea>
                        <input type="hidden" name='id' value="{{ $certificado->id }}">
                    </div>
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <input type="submit" class="btn btn-primary" value="Desaprobar" name="desaprobar">
            </div>
            {{Form::close()}} 
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog  modal-lg" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="exampleModalLabel">Aspectos a revisar en el Certificado de Origen del Acuerdo Comercial con Colombia</h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            {{Form::open(['route' =>'AnalistaSeniat.store' ,'method'=>'POST','id'=>''])}}
            <div class="modal-body">
                
                @if(isset($catCert))    
                    @foreach($catCert as $key => $row)
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="gen_cert_reg_seniat_id[]" value="{{$row->id}}" id="{{$row->id}}" />
                                    <label class="form-check-label" for="{{$row->id}}">{{$key + 1}} . {{$row->pregunta}}</label>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endif
                <input type="hidden" name='id' value="{{ $certificado->id }}">
                    
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <input type="submit" class="btn btn-primary" value="Aprobar" name="aprobar">
            </div>
            {{Form::close()}} 
            </div>
        </div>
    </div>
</div>
@stop