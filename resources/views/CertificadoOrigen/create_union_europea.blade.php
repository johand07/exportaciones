@extends('templates/layoutlte')
@section('content')

<!--Aqui Form::open-->
{!! Form::open(['route' => 'CertificadoOrigen.store' ,'method'=>'POST', 'id'=>'FormProd', 'enctype' => 'multipart/form-data']) !!}
<input type="hidden" name="gen_tipo_certificado_id" id="gen_tipo_certificado_id" value="{{$gen_tipo_certificado_id ? $gen_tipo_certificado_id : 9}}" />

<div class="content">
	<div class="panel-group">
		<!--Panel primari para Documentos-->
		<div class="panel-primary" style="background-color: #fff">
			<div class="panel-heading"><h3>Carga de Documentos</h3></div><!--Este cierra aqui mismo porque es solo un Titulo-->
			<div class="panel-body">
				<h5 style="color: red">Los campos marcados con (*) son Obligatorios. (Formatos permitidos .doc, .docx, .ppt, .pptx, .pdf)</h5>
				<div class="row"><br>
					<div class="col-md-12">
						<div class="col-md-6">
							<tr>
								<td class="text-center" valign="top" > 
									<span><b><p style="font-size:14px;">1.) Cargar el formato de Solicitud de Certificado de Origen</p></b></span>
									 <strong style="color: red"> *</strong>
									<span class="btn btn-default btn-file" style="margin: 3px;">
										Cargar Archivo <span class="glyphicon glyphicon-file"></span>
										{!! Form::file('file_1', array('class' => 'file-input', 'style'=>'margin: 10px;','id'=>'file_1','title'=>'formatos permitidos .doc, .docx, .ppt, .pptx, .pdf',)) !!}
									</span>
								</td>
								<td class="text-center" height="40px"><img id="vista_previa_file_1" src="" alt="" width="300px" style="margin: 3px;"><img id="imgdocumento_file_1" src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px; display: none;"><div id="nombre_file_1"></div>
								</td>
							</tr>
						</div>
						<div class="col-md-6">
							<tr>
								<td class="text-center" valign="top" > 
									<span><b><p style="font-size:14px;">4.) Pago por concepto de solicitud de certificado de origen (0,25 petros)</p></b></span>
									 <strong style="color: red"> *</strong>
									<span class="btn btn-default btn-file" style="margin: 3px;">
										Cargar Archivo <span class="glyphicon glyphicon-file"></span>
										{!! Form::file('file_4', array('class' => 'file-input','style'=>'margin: 10px;','id'=>'file_4','title'=>'formatos permitidos .doc, .docx, .ppt, .pptx, .pdf')) !!}
									</span>
								</td>
								<td class="text-center" height="40px"><img id="" src="" alt="" width="300px" style="margin: 3px;"><img id="imgdocumento_file_4" src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px; display: none;"><div id="nombre_file_4"></div>
								</td>
							</tr>
						</div>
					</div>
					

					<div class="col-md-12"><br><br><br>
						<div class="col-md-6">
							<tr>
								<td class="text-center" valign="top" > 
									<span><b><p style="font-size:14px;">2.) Declaración jurada de Origen</p></b></span>
									 <strong style="color: red"> *</strong>
									<span class="btn btn-default btn-file" style="margin: 3px;">
										Cargar Archivo <span class="glyphicon glyphicon-file"></span>
										{!! Form::file('file_2', array('class' => 'file-input','style'=>'margin: 10px;','id'=>'file_2','title'=>'formatos permitidos .doc, .docx,.ppt, .pptx, .pdf')) !!}
									</span>
								</td>
								<td class="text-center" height="40px"><img id="" src="" alt="" width="300px" style="margin: 3px;"><img id="imgdocumento_file_2" src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px; display: none;"><div id="nombre_file_2"></div>
								</td>
							</tr>
						</div>
						<div class="col-md-6">
							<tr>
								<td class="text-center" valign="top" > 
									<span><b><p style="font-size:14px;">5.) Pago de 0,02 U.T Gobierno del Distrito Capital</p></b></span>
									 <strong style="color: red"> *</strong>
									<span class="btn btn-default btn-file" style="margin: 3px;">
										Cargar Archivo <span class="glyphicon glyphicon-file"></span>
										{!! Form::file('file_5', array('class' => 'file-input','style'=>'margin: 10px;','id'=>'file_5','title'=>'formatos permitidos .doc, .docx, .ppt, .pptx, .pdf')) !!}
									</span>
								</td>
								<td class="text-center" height="40px"><img id="" src="" alt="" width="300px" style="margin: 3px;"><img id="imgdocumento_file_5" src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px; display: none;"><div id="nombre_file_5"></div>
								</td>
							</tr>
						</div>
					</div>
					<div class="col-md-12"><br><br><br>
						<div class="col-md-6">
							<tr>
								<td class="text-center" valign="top" > 
									<span><b><p>3.) Factura comercial de exportador</p></b></span>
									 <strong style="color: red"> *</strong>
									<span class="btn btn-default btn-file" style="margin: 3px;">
										Cargar Archivo <span class="glyphicon glyphicon-file"></span>
										{!! Form::file('file_3', array('class' => 'file-input','style'=>'margin: 10px;','id'=>'file_3','title'=>'formatos permitidos .doc, .docx,.ppt, .pptx, .pdf')) !!}
									</span>
								</td>
								<td class="text-center" height="40px"><img id="" src="" alt="" width="300px" style="margin: 3px;"><img id="imgdocumento_file_3" src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px; display: none;"><div id="nombre_file_3"></div>
								</td>
							</tr>
						</div>
						<div class="col-md-6">
							<tr>
								<td class="text-center" valign="top" > 
									<span><b><p style="font-size:14px;">6.) Factura de productor (Si es <b>Comercializadora</b>)</p></b></span>
									 <!--strong style="color: red"> *</strong-->
									<span class="btn btn-default btn-file" style="margin: 3px;">
										Cargar Archivo <span class="glyphicon glyphicon-file"></span>
										{!! Form::file('file_6', array('class' => 'file-input','style'=>'margin: 10px;','id'=>'file_6','title'=>'formatos permitidos .doc, .docx, .ppt, .pptx, .pdf', "noreq"=>"noreq")) !!}
									</span>
								</td>
								<td class="text-center" height="40px"><img id="" src="" alt="" width="300px" style="margin: 3px;"><img id="imgdocumento_file_6" src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px; display: none;">
								<div id="nombre_file_6"></div>
								</td>
							</tr>
						</div>
					</div>
				</div>
			</div>
		<br><br><br>
		</div>
		<!--Panel primari para Formulario Certificado Origen UE-->
		<div class="panel-primary" style="background-color: #fff">
			<div class="panel-heading">
			<h3>Certificado de origen Unión Europea</h3>
			</div>
			<div class="panel-body">
				<div class="row">
					<h4>&nbsp;&nbsp;&nbsp;<b>Goods consigned from</b></h4>
					<div class="col-md-4">
						<div class="form-group">
							{!! Form::label('','Exporters business name') !!} <p class="text-muted">(Razón social de los exportadores)</p>
							{!! Form::text('razon_social',$det_usuario->razon_social,['class'=>'form-control','id'=>'razon_social', 'readonly'=>'readonly']) !!}
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							{!! Form::label('','Address') !!}
							<p class="text-muted">(Dirección completa y ciudad)</p>
							{!! Form::text('direccion',$det_usuario->direccion,['class'=>'form-control','id'=>'direccion', 'readonly'=>'readonly']) !!}
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							{!! Form::label('','Country') !!}<p class="text-muted">(País)</p>
							{!! Form::text('pais_exportador','VENEZUELA',['class'=>'form-control','readonly'=>'readonly','id'=>'pais_exportador']) !!}
						</div>
					</div>
				</div><br>


				<div class="row">
						<h4>&nbsp;&nbsp;&nbsp;<b>Goods consigned to</b></h4>
					<div class="col-md-4">
						<div class="form-group">
							{!! Form::label('','Consigneess name') !!}
							<p class="text-muted">Nombre del consignatario</p>
							{!! Form::text('razon_social_importador',null,['class'=>'form-control','id'=>'razon_social_importador']) !!}
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							{!! Form::label('','Address') !!}
							<p class="text-muted">(Dirección completa)</p>
							{!! Form::text('direccion_importador',null,['class'=>'form-control','id'=>'direccion_importador']) !!}
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							{!! Form::label('','Country') !!}
							<p class="text-muted">(País/Ciudad)</p>
							{!! Form::text('pais_ciudad_importador',null,['class'=>'form-control','id'=>'pais_ciudad_importador']) !!}
						</div>
					</div>
				</div><br><br>

				<div class="row">
						<h4>&nbsp;&nbsp;&nbsp;<b>Means of transport and route (as far as known)</b></h4>
						<p class="text-muted">Medio de transporte y ruta (hasta donde se conozca)</p>
					<div class="col-md-6">
						<div class="form-group">
							{!! Form::label('','Dirección Productor') !!}
							<p class="text-muted">Dirección Productor</p>
							{!! Form::text('direccion_productor',null,['class'=>'form-control','id'=>'direccion_productor', "noreq"=>"noreq"]) !!}
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							{!! Form::label('','Lugar embarque Importador') !!}
							<p class="text-muted">(Lugar embarque Importador)</p>
							{!! Form::text('lugar_embarque_importador',null,['class'=>'form-control','id'=>'lugar_embarque_importador', "noreq"=>"noreq"]) !!}
						</div>
					</div>
				
				</div><br><br>

				
				<!--Primera tabla para prodcutos completos en el caso Union Europea -->
				
				<div class="row"><br><!--Agregar productos-->
					
					<table class="table table-bordered text-left" id="productos">
						<tr> <td><button  name="add" type="button" id="add-productos" value="add more" class="btn btn-success" >Add products</button></td></tr>

						<tr>
							<th>Item number<br>
							<br><p class="text-muted">Número de artículo</p></th>

							<th>Marks and numbers of packages<br><br><p class="text-muted">Marcas y números de paquetes.</p></th>

							<th>Numbers and king of packages, description commercial<br><br>
								<p class="text-muted">Números de paquetes, descripción <br>comercial</p>
							</th>


							<th>Gross weight or other quantity<br><br>
								<p class="text-muted">Peso bruto u otra cantidad</p>
							</th>

							<th>Code tariff<br><br>
								<p class="text-muted">Código Arancel</p>
							</th>

							<th>Name of the Goods<br><br>
								<p class="text-muted">Denominación de la mercancía</p>
							</th>
						
						</tr>
						<tr id="row1"><!--row1 es para la lista dinamica de agregar productos nsantos-->
								<!--Lista dinamica Prodcutos-->

							<td>{!!Form::text('num_orden[]',null,['class'=>'form-control','id'=>'num_orden'])!!}
							</td>

							<td>{!!Form::text('cantidad_segun_factura[]',null,['class'=>'form-control','id'=>'cantidad_segun_factura','onkeypress'=>'return soloNumeros(event)'])!!}
							</td>

							<td>{!!Form::text('descripcion_comercial[]',null,['class'=>'form-control','id'=>'descripcion_comercial'])!!}
							</td>

							<td>{!!Form::text('unidad_fisica[]',null,['class'=>'form-control','id'=>'unidad_fisica'])!!}
							</td>

							<td>{!!Form::text('codigo_arancel[]',null,['class'=>'form-control','id'=>'codigo_arancel'])!!}
							</td>

							<td>{!!Form::text('denominacion_mercancia[]',null,['class'=>'form-control','id'=>'denominacion_mercancia'])!!}
							</td>
								
						</tr>
					</table>
				</div>


				<div class="row"><br><!--Agregar lista Dinamica para Criterios-->
					
					<table class="table table-bordered text-left" id="criterios">
						<tr> <td><button  name="add" type="button" id="add-criterios" value="add more" class="btn btn-success" >Add criterion</button></td></tr>

						<tr>
				
							<th>Order number<br>
							<br>
							<p class="text-muted">Número orden</p></th>

							<th>Origin criterion<br><br><p class="text-muted">Criterio de origen</p></th>

							
						
						</tr>
						<tr id="row2"><!--row1 es para la lista dinamica de agregar criterios nsantos-->
								<!--Lista dinamica Criterios-->

							<td>{!!Form::text('num_orden_declaracion[]',null,['class'=>'form-control','id'=>'num_orden_declaracion'])!!}
							</td>

							<td>{!!Form::text('normas_criterio[]',null,['class'=>'form-control','id'=>'normas_criterio'])!!}
							</td>

								
						</tr>
					</table>
				</div>


			
				<div class="row"><br>
					<div class="col-md-6">
						<div class="form-group">
							{!! Form::label('','Number Invoice') !!}
							<p class="text-muted">Factura</p>
							{!! Form::text('numero_factura',null,['class'=>'form-control','id'=>'numero_factura']) !!}
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							{!! Form::label('','Date Invoice') !!}
							<p class="text-muted">Fecha</p>
							<div class="input-group date">
								{!! Form::text('f_fac_certificado',null,['class'=>'form-control','readonly'=>'readonly','id'=>'f_fac_certificado']) !!}
								<span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
							</div>
						</div>
					</div>
				</div><br>
			
			<div id="observaciones"><br>
					<div class="row">
						<div  class="col-md-12">
							<div class="form-group">
								&nbsp;&nbsp;&nbsp;{!! Form::label('','Remarks') !!}
								&nbsp;&nbsp;&nbsp;<p class="text-muted">Observaciones</p>
								{!! Form::textarea('observaciones',null,['class'=>'form-control','id'=>'observaciones', "noreq"=>"noreq"]) !!}
							</div>
						</div>
					</div>
				</div>

				<div class="row text-center">
					<a href="{{ route('CertificadoOrigen.create')}}" class="btn btn-primary">Cancel</a>
					<input type="submit" class="btn btn-success" value="Enviar" name="submit" id="submit" onclick="validacionForm()"> 
				</div>

			</div>
		</div>

		
	</div><!--div Para Todo cierre panel-group general-->
</div><!--div Para Todo cierre container general-->



{{Form::close()}}

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="{{asset('js/ajax_v2.1.1.js')}}"></script>
<!--Script para Tooltips guia de campos con stylos bootstrap-->
<script src="https://unpkg.com/@popperjs/core@2"></script>
<script src="https://unpkg.com/tippy.js@6"></script>
<script src="{{asset('js/CertificadoOrigenJS/COCreateUnionEuropea.js')}}"></script>



@stop