<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BandejaEntradaDjo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gen_bandeja_entrada_djo', function (Blueprint $table) {
          $table->engine = 'InnoDB';
          $table->increments('id');
          $table->unsignedInteger('gen_declaracion_jo_id')->index()->nullable();
          $table->foreign('gen_declaracion_jo_id')->references('id')->on('gen_declaracion_jo');
          $table->unsignedInteger('gen_usuario_id')->index()->nullable();
          $table->foreign('gen_usuario_id')->references('id')->on('gen_usuario');
          $table->unsignedInteger('estado')->index()->nullable();
          $table->string('descripcion', 200)->nullable();
          $table->boolean('bactivo')->default(1);
          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gen_bandeja_entrada_djo');
    }
}
