@extends('templates/layoutlte_analista_inversionista')

@section('content')
<div class="row">
    <div class="col-md-12">
        <section class="content" id="gestionDeclaInvert" >
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="alert alert-dismissible alert-info">
                              <button type="button" class="close" data-dismiss="alert">&times;</button>
                              <h4 class="alert-heading">Información Importante.!</h4>
                              <p class="mb-0"><h4><span class="text-danger">Debe dar estatus a las tres planillas (Djir01, Djir02, Djir03).</span> Al final de cada planilla puede proceder seleccionar el que corresponda según su evaluación</h4></p>
                            </div>
                        </div>
                    </div>
                    <ul class="nav nav-tabs">
                        <li class="active" >
                            <a href="#detplanillaDjir01" data-toggle="tab">Planilla Djir01
                            </a>
                        </li> 
                        <li>
                            <a href="#detplanillaDjir02" data-toggle="tab">Planilla Djir02
                            </a>    
                        </li>
                        <li>
                            <a href="#detplanillaDjir03" data-toggle="tab">PlanillaDjir_03
                            </a>    
                        </li>
                    </ul>
                    <div class="nav-tabs-custom">
                        <div class="tab-content"> 
                            <div class="tab-pane active" id="detplanillaDjir01">
                                <section class="content-header">
                                  <h4>
                                   DETALLE PLANILLA DE DECLARACIÓN JURADA DE INVERSIÓN REALIZADA (01)
                                    <small>INFORMACIÓN GENERAL</small>
                                  </h4>
                                </section>
                                <div style="overflow-y: auto;">
                                    @include('AnalistInverExtranjera.detPLanilla1Iversion')
                                </div>
                            </div> 
                            <div class="tab-pane" id="detplanillaDjir02">
                                <section class="content-header">
                                  <h4>
                                     PLANILLA DE DECLARACIÓN JURADA DE INVERSIÓN REALIZADA (02)
                                    <small class="justify">(INFORMACIÓN FINANCIERA)</small>
                                  </h4>
                                </section>
                                <div style="overflow-y: auto;">
                                    @include('AnalistInverExtranjera.detPLanilla2Iversion')
                                </div>
                            </div> 
                            <div class="tab-pane" id="detplanillaDjir03">
                                <section class="content-header">
                                  <h4>
                                    DETALLE PLANILLA DE DECLARACIÓN JURADA DE INVERSIÓN REALIZADA (03) 
                                    <small>INFORMACIÓN COMPLEMENTARIA</small>
                                  </h4>
                                </section>
                                <div style="overflow-y: auto;">
                                    @include('AnalistInverExtranjera.detPLanilla3Iversion')
                                </div>  
                            </div>                    
                        </div>

                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
        

@stop