<?php

namespace App\Http\Controllers;

use App\Models\PermisoEspecialBovino;
use App\Models\Productos;
use App\Models\GenArancelMercosur;
use Illuminate\Support\Str;
use Session;
use Auth;
use Laracasts\Flash\Flash;
use Alert;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class permisoEspecialBovinoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   $titulo="Autorización Especial de Exportación";
        $descripcion="ANIMALES VIVOS DE LA ESPECIE BOVINA.";
        $permisosBovinos=PermisoEspecialBovino::where('gen_usuario_id',Auth::user()->id)->where('bactivo',1)->get();

        $exportaGanado = 0;
        $prod_users = Productos::where('gen_usuario_id', Auth::user()->id)->whereIn('codigo',['0102.29.90.90', '0102.29.11.00'])->get();
        $cont = count($prod_users);
        if (!empty($prod_users) && $cont > 0) {
            $exportaGanado = 1;
        }

        return view('permisoEspecialBovino.index',compact('permisosBovinos','titulo','descripcion','exportaGanado'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $titulo="Autorización Especial de Exportación";
        $descripcion="ANIMALES VIVOS DE LA ESPECIE BOVINA.";

        $exportaGanado = 0;
        $prod_users = Productos::where('gen_usuario_id', Auth::user()->id)->whereIn('codigo',['0102.29.90.90', '0102.29.11.00'])->get();
        $cont = count($prod_users);
        if (!empty($prod_users) && $cont > 0) {
            $exportaGanado = 1;
        }

        $aranceles = GenArancelMercosur::whereIn('codigo',['0102.29.90.90', '0102.29.11.00'])->get();
        $descripcionArancel = 'Preñadas o con cría al pie';
        // dd($aranceles);

        return view('permisoEspecialBovino.create',compact('aranceles','descripcionArancel','exportaGanado','titulo','descripcion'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            
            'codigo_arancel'        => 'required',
            'descripcion_arancel'   => 'required',
            'volumen'               => 'required',
            'puerto_salida'         => 'required',
            'puerto_entrada'        => 'required',
            'fembarque_bovino'      => 'required'
        
        ]);
        
        $permisoEspecial = new PermisoEspecialBovino;
        $permisoEspecial->gen_usuario_id        = Auth::user()->id;
        $permisoEspecial->num_autorizacion      = $this->generarRamdom();
        $permisoEspecial->codigo_arancel        = $request->codigo_arancel;
        $permisoEspecial->descripcion_arancel   = $request->descripcion_arancel;
        $permisoEspecial->volumen               = $request->volumen;
        $permisoEspecial->puerto_salida         = $request->puerto_salida;
        $permisoEspecial->puerto_entrada        = $request->puerto_entrada;
        $permisoEspecial->num_proforma          = $this->generarProForma();
        $permisoEspecial->fembarque_bovino      = $request->fembarque_bovino;
        $permisoEspecial->fvalido               = $request->fembarque_bovino;
        $permisoEspecial->save();
        
        
        Alert::success('Permiso Registrado con exito!','Registrado Correctamente!')->persistent("Ok");

            return redirect()->action('permisoEspecialBovinoController@index');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PermisoEspecialBovino  $permisoEspecialBovino
     * @return \Illuminate\Http\Response
     */
    public function show(PermisoEspecialBovino $permisoEspecialBovino)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\PermisoEspecialBovino  $permisoEspecialBovino
     * @return \Illuminate\Http\Response
     */
    public function edit(PermisoEspecialBovino $permisoEspecialBovino)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\PermisoEspecialBovino  $permisoEspecialBovino
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PermisoEspecialBovino $permisoEspecialBovino)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PermisoEspecialBovino  $permisoEspecialBovino
     * @return \Illuminate\Http\Response
     */
    public function destroy(PermisoEspecialBovino $permisoEspecialBovino)
    {
        //
    }

    private function generarRamdom()
    {
        $user = Auth::user();
        $random = Str::random(2);
        $id  = $user->id;
        $rif = substr($user->detUsuario->rif, -2);
        $fecha = date('Ymd');
        return $id.'-'.$fecha.$random.$rif;
    }

    private function generarProForma()
    {
        $user = Auth::user();
        $id  = $user->id;
        $fecha = date('Y-hi');
        return $id.'-'.$fecha;
    }
}
