<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddGenSolInversionistaSectorActividad extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::table('gen_sol_inversionista', function (Blueprint $table) {
            //
            $table->string('especif_otro_sector')->nullable()->after('cat_sector_inversionista_id'); 
            $table->string('especif_otro_actividad')->nullable()->after('actividad_eco_emp');
          
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('gen_sol_inversionista', function (Blueprint $table) {
            //
            $table->dropColumn('especif_otro_sector');
            $table->dropColumn('especif_otro_actividad');
        });
    }
}
