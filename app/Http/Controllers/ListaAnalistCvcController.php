<?php

namespace App\Http\Controllers;

use App\Models\GenSolCvc;
use App\Models\GenUsuario;
use App\Models\GenStatus;
use App\Models\DetUsuario;
use App\Models\Pais;
use App\Models\GenAduanaSalida;
use App\Models\GenUnidadMedida;
use App\Models\GenDivisa;
use App\Models\BandejaAnalistaCvc;
use App\Models\CatCargados;
use App\Models\CatTipoCafe;
use App\Models\CatMetodoElab;
use Auth;
use Session;
use Alert;
use Illuminate\Http\Request;

class ListaAnalistCvcController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $solicitud_cvc=GenSolCvc::where('bactivo',1)
        ->with('rAnalistaCvc')->get();

        //dd($solicitud_cvc);
         
        return view('ListaAnalistCvc.index',compact('solicitud_cvc')); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\GenSolCvc  $genSolCvc
     * @return \Illuminate\Http\Response
     */
    public function show(GenSolCvc $genSolCvc)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\GenSolCvc  $genSolCvc
     * @return \Illuminate\Http\Response
     */
    public function edit(GenSolCvc $genSolCvc, $id)
    {
          $titulo= 'Corporación Venezolana del Café';
          $descripcion= 'Perfil del Analista CVC';

          $solicitud_cvc=GenSolCvc::find($id);

        /**Validacion de la bandeja**/
          $bandejacvc= BandejaAnalistaCvc::where('gen_sol_cvc_id',$id)->first();

           //dd($bandejacvc);

          if(empty($bandejacvc)){

            $solicitud_cvc->gen_status_id=10;
            $solicitud_cvc->fstatus=date('Y-m-d');
            $solicitud_cvc->save();
            
            $solicitud_cvc= new BandejaAnalistaCvc;
            $solicitud_cvc->gen_sol_cvc_id=$id;
            $solicitud_cvc->gen_usuario_id=Auth::user()->id;
            $solicitud_cvc->gen_status_id=10;
            $solicitud_cvc->fstatus=date('Y-m-d');
            $solicitud_cvc->save();
        }

          /****Consulta de los estatus**/

          $edo_observacion=GenStatus::whereIn('id', [11,12])->pluck('nombre_status','id');

           /****Consulta tipo de moneda**/

          $unidadmedida=GenUnidadMedida::where('bactivo',1)->pluck('dunidad','id');
           
           //dd($unidadmedida);

          $divisas=GenDivisa::orderBy('ddivisa_abr','asc')->pluck('ddivisa_abr','id');
         //dd($divisas);


         $genaduana=GenAduanaSalida::where('bactivo',1)->pluck('daduana','id');
         //dd($genaduana);

          $pais=Pais::where('bactivo',1)->pluck('dpais','id');

          //dd($pais);

          $tiposcargados=CatCargados::where('bactivo',1)->pluck('nombre_cargados','id');

          //dd($tiposcargados);


          $tiposcafe=CatTipoCafe::where('bactivo',1)->pluck('nombre_cafe','id');

          $tiposmetodos=CatMetodoElab::where('bactivo',1)->pluck('nombre_metodo','id');


        return view('ListaAnalistCvc.edit',compact('titulo','descripcion','solicitud_cvc','edo_observacion','divisas','pais','tiposcargados','tiposcafe','tiposmetodos','unidadmedida','genaduana'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\GenSolCvc  $genSolCvc
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, GenSolCvc $genSolCvc, $id)
    {

         if(!empty($request->observacion) && $request->gen_status_id==11){
         
                 $solicitud_cvc=GenSolCvc::where('id',$id)->update(['gen_status_id'=>11,'edo_observacion'=>1,'observacion'=>$request->observacion,'updated_at' => date("Y-m-d H:i:s") ]);

                 $bandejacorporacion=BandejaAnalistaCvc::where('gen_sol_cvc_id',$id)->update(['gen_status_id'=>11,'updated_at' => date("Y-m-d H:i:s") ]);

               
            }elseif(empty($request->observacion ) && $request->gen_status_id == 12){
              
             $analistacvc=GenSolCvc::where('id',$id)->update(['gen_status_id'=>12,'edo_observacion'=>1,'observacion'=>$request->observacion,'updated_at' => date("Y-m-d H:i:s") ]);

              $bandejacorporacion=BandejaAnalistaCvc::where('gen_sol_cvc_id',$id)->update(['gen_status_id'=>12,'updated_at' => date("Y-m-d H:i:s") ]);

          }


        Alert::warning('Su Solicitud ha sido Atendida ')->persistent("OK");
            return redirect()->action('ListaAnalistCvcController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\GenSolCvc  $genSolCvc
     * @return \Illuminate\Http\Response
     */
    public function destroy(GenSolCvc $genSolCvc)
    {
        //
    }
}
