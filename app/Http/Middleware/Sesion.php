<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class Sesion
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        //dd(url());
 
        // Verificamos si hay sesión activa
        /*if (Auth::check())
        {*/
            //dd($request->path());
            // Si tenemos sesión activa mostrará la página de inicio
            /*if ("/".$request->path() =="/loguin")  {
                return redirect('/');
            }*/
            if ($request->path() == "/RifEmp")  {
                //dd($request->path());
                return redirect('RifEmp');
            }
            if ($request->path() =="/validarRif")  {
                //dd($request->path());
                return redirect('/RifEmp');
            }
            
        /*}*/
        // Si no hay sesión activa mostramos el formulario
        return $next($request);
    }
}
