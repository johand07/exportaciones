<?php

namespace App\Http\Controllers;

use App\Models\GenCertZooEu;
use App\Models\CatMediotransporte;
use App\Models\CatCondtransporte;
use App\Models\CatCertificadoEfectosDe;
use App\Models\Pais;
use App\Models\DescripPartidaZoo;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Auth;
use Laracasts\Flash\Flash;
use Alert;

class certificadosZooEuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $titulo = 'Certificado Zoosanitario';
        $descripcion =  'Certificado Zoosanitario EU';
        $certificadoZoo = GenCertZooEu::where('bactivo',1)->where('gen_usuario_id', Auth::user()->id)->get();
        return view('certificadosZooEu.index',compact('titulo','descripcion','certificadoZoo'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $titulo = 'Certificado Zoosanitario';
        $descripcion =  'Certificado Zoosanitario EU';
        $pais = Pais::where('bactivo', 1)->pluck('dpais', 'dpais');
        $catMediotrans = CatMediotransporte::where('bactivo', 1)->pluck('nombre', 'id');
        $catCondTrans  = CatCondtransporte::where('bactivo', 1)->pluck('nombre', 'id');
        $catCertEfect  = CatCertificadoEfectosDe::where('bactivo', 1)->pluck('nombre', 'id');
        
        
        return view('certificadosZooEu.create',compact('titulo','descripcion', 'pais', 'catMediotrans', 'catCondTrans', 'catCertEfect'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

     //dd($request->all());
        $genCertZooEu = [
            'gen_usuario_id'                => Auth::user()->id,
            //'num_ref_cert'                  => $request->num_ref_cert,

            'pais_export'                   => (isset($request->pais_export))?$request->pais_export:'',
            'cod_iso_pais_export'           => (isset($request->cod_iso_pais_export))?$request->cod_iso_pais_export:'',
            'nombre_import'                 => (isset($request->nombre_import))?$request->nombre_import:'',
            'direccion_import'              => (isset($request->direccion_import))?$request->direccion_import:'',
            'pais_import'                   => (isset($request->pais_import))?$request->pais_import:'',
            'cod_iso_pais_inport'           => (isset($request->cod_iso_pais_inport))?$request->cod_iso_pais_inport:'',
            'nombre_oper_respons'           => (isset($request->nombre_oper_respons))?$request->nombre_oper_respons:'',
            'direccion_oper_respons'        => (isset($request->direccion_oper_respons))?$request->direccion_oper_respons:'',
            'pais_oper_respons'             => (isset($request->pais_oper_respons))?$request->pais_oper_respons:'',
            'cod_iso_pais_oper_respons'     => (isset($request->cod_iso_pais_oper_respons))?$request->cod_iso_pais_oper_respons:'',
            'pais_origen'                   => (isset($request->pais_origen))?$request->pais_origen:'',
            'cod_iso_pais_origen'           => (isset($request->cod_iso_pais_origen))?$request->cod_iso_pais_origen:'',
            'pais_destino'                  => (isset($request->pais_destino))?$request->pais_destino:'',
            'cod_iso_pais_destino'          => (isset($request->cod_iso_pais_destino))?$request->cod_iso_pais_destino:'',
            'region_origen'                 => (isset($request->region_origen))?$request->region_origen:'',
            'cod_reg_origen'                => (isset($request->cod_reg_origen))?$request->cod_reg_origen:'',
            'region_destino'                => (isset($request->region_destino))?$request->region_destino:'',
            'cod_reg_dest'                  => (isset($request->cod_reg_dest))?$request->cod_reg_dest:'',
            'nombre_lug_exp'                => (isset($request->nombre_lug_exp))?$request->nombre_lug_exp:'',
            'num_reg_exp'                   => (isset($request->num_reg_exp))?$request->num_reg_exp:'',
            'dir_lug_exp'                   => (isset($request->dir_lug_exp))?$request->dir_lug_exp:'',
            'pais_lug_exp'                  => (isset($request->pais_lug_exp))?$request->pais_lug_exp:'',
            'cod_iso_lug_exp'               => (isset($request->cod_iso_lug_exp))?$request->cod_iso_lug_exp:'',
            'nombre_lug_dest'               => (isset($request->nombre_lug_dest))?$request->nombre_lug_dest:'',
            'num_reg_lug_dest'              => (isset($request->num_reg_lug_dest))?$request->num_reg_lug_dest:'',
            'dir_lug_dest'                  => (isset($request->dir_lug_dest))?$request->dir_lug_dest:'',
            'pais_lug_dest'                 => (isset($request->pais_lug_dest))?$request->pais_lug_dest:'',
            'cod_iso_lug_dest'              => (isset($request->cod_iso_lug_dest))?$request->cod_iso_lug_dest:'',
            'lug_carga'                     => (isset($request->lug_carga))?$request->lug_carga:'',
            'fecha_hora_salida'             => (isset($request->fecha_hora_salida))?$request->fecha_hora_salida:'',
            'cat_medio_transport_id'        => (isset($request->cat_medio_transport_id))?$request->cat_medio_transport_id:'',
            'puest_control_front'           => (isset($request->puest_control_front))?$request->puest_control_front:'',
            'tipo_doc_acomp'                => (isset($request->tipo_doc_acomp))?$request->tipo_doc_acomp:'',
            'cod_doc_acomp'                 => (isset($request->cod_doc_acomp))?$request->cod_doc_acomp:'',
            'pais_doc_acomp'                => (isset($request->pais_doc_acomp))?$request->pais_doc_acomp:'',
            'cod_iso_doc_acomp'             => (isset($request->cod_iso_doc_acomp))?$request->cod_iso_doc_acomp:'',
            'ref_doc_comer'                 => (isset($request->ref_doc_comer))?$request->ref_doc_comer:'',
            'ident_medio_transport'         => (isset($request->ident_medio_transport))?$request->ident_medio_transport:'',
            'cat_cond_transport_id'         => (isset($request->cat_cond_transport_id))?$request->cat_cond_transport_id:'',
            'num_recipiente'                => (isset($request->num_recipiente))?$request->num_recipiente:'',
            'num_precinto'                  => (isset($request->num_precinto))?$request->num_precinto:'',
            'cat_certificado_efectos_de_id' => (isset($request->cat_certificado_efectos_de_id))?$request->cat_certificado_efectos_de_id:'',
            'para_merc_interior'            => (isset($request->para_merc_interior))?$request->para_merc_interior:'',
            'num_total_bultos'              => (isset($request->num_total_bultos))?$request->num_total_bultos:'',
            'cant_total'                    => (isset($request->cant_total))?$request->cant_total:'',
            'peso_neto_pn'                     => (($request->peso_neto_pn))?$request->peso_neto_pn:'',
            'peso_bruto'                    => (($request->peso_bruto))?$request->peso_bruto:'',
        ];

        $insertGenCert= GenCertZooEu::insert($genCertZooEu);

        //$UltimoCertZooEu=GenCertZooEu::where('gen_usuario_id', Auth::user()->id)->last();

        $UltimoCertZooEu=GenCertZooEu::all()->last();
        $UltimoCertZooEu->num_ref_cert  = $this->generarRamdom();
        $UltimoCertZooEu->save();


        for($i=0;$i<count($request->codigo_nc);$i++)
        {
            DescripPartidaZoo::create([
                'gen_cert_zoo_eu_id' => $UltimoCertZooEu->id,
                'codigo_nc' => $request->codigo_nc[$i],
                'especie' => $request->especie[$i],
                'almacen_frigo' => $request->almacen_frigo[$i],
                'marc_ident' => $request->marc_ident[$i],
                'tipos_embalaje' => $request->tipos_embalaje[$i],
                'nat_merc' => $request->nat_merc[$i],
                'consumidor_final' => $request->consumidor_final[$i],
                'tipo_tratamiento' => $request->tipo_tratamiento[$i],
                'fecha_recogida' => $request->fecha_recogida[$i],
                'peso_neto' => $request->peso_neto[$i],
                'num_bultos' => $request->num_bultos[$i],
                'num_lotes' => $request->num_lotes[$i],
            ]);
        }

              Alert::success('Registro Exitoso!')->persistent("Ok");
              return redirect()->action('certificadosZooEuController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\GenCertZooEu  $genCertZooEu
     * @return \Illuminate\Http\Response
     */
    public function show(GenCertZooEu $genCertZooEu)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\GenCertZooEu  $genCertZooEu
     * @return \Illuminate\Http\Response
     */
    public function edit(GenCertZooEu $genCertZooEu, $id)
    {
        $titulo = 'Certificado Zoosanitario';
        $descripcion =  'Certificado Zoosanitario EU';

        $genCertZooEu=GenCertZooEu::find($id);
        

        $DescripPartidaZoo=DescripPartidaZoo::where('gen_cert_zoo_eu_id',$genCertZooEu->id)->get();

//dd($DescripPartidaZoo);

        $pais = Pais::where('bactivo', 1)->pluck('dpais', 'dpais');
        $catMediotrans = CatMediotransporte::where('bactivo', 1)->pluck('nombre', 'id');
        $catCondTrans  = CatCondtransporte::where('bactivo', 1)->pluck('nombre', 'id');
        $catCertEfect  = CatCertificadoEfectosDe::where('bactivo', 1)->pluck('nombre', 'id');
        return view('certificadosZooEu.edit',compact('titulo','descripcion', 'pais', 'catMediotrans', 'catCondTrans', 'catCertEfect','genCertZooEu','DescripPartidaZoo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\GenCertZooEu  $genCertZooEu
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, GenCertZooEu $genCertZooEu, $id)
    {

     // dd($id);
        $genCertZooEu = GenCertZooEu::find($id);

        //$genCertZooEu->num_ref_cert                  = (($request->num_ref_cert))?$request->:'';
        $genCertZooEu->pais_export                   = (($request->pais_export))?$request->pais_export:'';
        $genCertZooEu->cod_iso_pais_export           = (($request->cod_iso_pais_export))?$request->cod_iso_pais_export:'';
        $genCertZooEu->nombre_import                 = (($request->nombre_import))?$request->nombre_import:'';
        $genCertZooEu->direccion_import              = (($request->direccion_import))?$request->direccion_import:'';
        $genCertZooEu->pais_import                   = (($request->pais_import))?$request->pais_import:'';
        $genCertZooEu->cod_iso_pais_inport           = (($request->cod_iso_pais_inport))?$request->cod_iso_pais_inport:'';
        $genCertZooEu->nombre_oper_respons           = (($request->nombre_oper_respons))?$request->nombre_oper_respons:'';
        $genCertZooEu->direccion_oper_respons        = (($request->direccion_oper_respons))?$request->direccion_oper_respons:'';
        $genCertZooEu->pais_oper_respons             = (($request->pais_oper_respons))?$request->pais_oper_respons:'';
        $genCertZooEu->cod_iso_pais_oper_respons     = (($request->cod_iso_pais_oper_respons))?$request->cod_iso_pais_oper_respons:'';
        $genCertZooEu->pais_origen                   = (($request->pais_origen))?$request->pais_origen:'';
        $genCertZooEu->cod_iso_pais_origen           = (($request->cod_iso_pais_origen))?$request->cod_iso_pais_origen:'';
        $genCertZooEu->pais_destino                  = (($request->pais_destino))?$request->pais_destino:'';
        $genCertZooEu->cod_iso_pais_destino          = (($request->cod_iso_pais_destino))?$request->cod_iso_pais_destino:'';
        $genCertZooEu->region_origen                 = (($request->region_origen))?$request->region_origen:'';
        $genCertZooEu->cod_reg_origen                = (($request->cod_reg_origen))?$request->cod_reg_origen:'';
        $genCertZooEu->region_destino                = (($request->region_destino))?$request->region_destino:'';
        $genCertZooEu->cod_reg_dest                  = (($request->cod_reg_dest))?$request->cod_reg_dest:'';
        $genCertZooEu->nombre_lug_exp                = (($request->nombre_lug_exp))?$request->nombre_lug_exp:'';
        $genCertZooEu->num_reg_exp                   = (($request->num_reg_exp))?$request->num_reg_exp:'';
        $genCertZooEu->dir_lug_exp                   = (($request->dir_lug_exp))?$request->dir_lug_exp:'';
        $genCertZooEu->pais_lug_exp                  = (($request->pais_lug_exp))?$request->pais_lug_exp:'';
        $genCertZooEu->cod_iso_lug_exp               = (($request->cod_iso_lug_exp))?$request->cod_iso_lug_exp:'';
        $genCertZooEu->nombre_lug_dest               = (($request->nombre_lug_dest))?$request->nombre_lug_dest:'';
        $genCertZooEu->num_reg_lug_dest              = (($request->num_reg_lug_dest))?$request->num_reg_lug_dest:'';
        $genCertZooEu->dir_lug_dest                  = (($request->dir_lug_dest))?$request->dir_lug_dest:'';
        $genCertZooEu->pais_lug_dest                 = (($request->pais_lug_dest))?$request->pais_lug_dest:'';
        $genCertZooEu->cod_iso_lug_dest              = (($request->cod_iso_lug_dest))?$request->cod_iso_lug_dest:'';
        $genCertZooEu->lug_carga                     = (($request->lug_carga))?$request->lug_carga:'';
        $genCertZooEu->fecha_hora_salida             = (($request->fecha_hora_salida))?$request->fecha_hora_salida:'';
        $genCertZooEu->cat_medio_transport_id        = (($request->cat_medio_transport_id))?$request->cat_medio_transport_id:'';
        $genCertZooEu->puest_control_front           = (($request->puest_control_front))?$request->puest_control_front:'';
        $genCertZooEu->tipo_doc_acomp                = (($request->tipo_doc_acomp))?$request->tipo_doc_acomp:'';
        $genCertZooEu->cod_doc_acomp                 = (($request->cod_doc_acomp))?$request->cod_doc_acomp:'';
        $genCertZooEu->pais_doc_acomp                = (($request->pais_doc_acomp))?$request->pais_doc_acomp:'';
        $genCertZooEu->cod_iso_doc_acomp             = (($request->cod_iso_doc_acomp))?$request->cod_iso_doc_acomp:'';
        $genCertZooEu->ref_doc_comer                 = (($request->ref_doc_come))?$request->ref_doc_come:'';
        $genCertZooEu->ident_medio_transport         = (($request->ident_medio_transport))?$request->ident_medio_transport:'';
        $genCertZooEu->cat_cond_transport_id         = (($request->cat_cond_transport_id))?$request->cat_cond_transport_id:'';
        $genCertZooEu->num_recipiente                = (($request->num_recipiente))?$request->num_recipiente:'';
        $genCertZooEu->num_precinto                  = (($request->num_precinto))?$request->num_precinto:'';
        $genCertZooEu->cat_certificado_efectos_de_id = (($request->cat_certificado_efectos_de_id))?$request->cat_certificado_efectos_de_id:'';
        $genCertZooEu->para_merc_interior            = (($request->para_merc_interior))?$request->para_merc_interior:'';
        $genCertZooEu->num_total_bultos              = (($request->num_total_bultos))?$request->num_total_bultos:'';
        $genCertZooEu->cant_total                    = (($request->cant_total))?$request->cant_total:'';
        $genCertZooEu->peso_neto_pn                  = (($request->peso_neto_pn))?$request->peso_neto_pn:'';
        $genCertZooEu->peso_bruto                    = (($request->peso_bruto))?$request->peso_bruto:'';
        $genCertZooEu->save();


//        dd($request->id);

        for($i=0;$i<count($request->codigo_nc);$i++)
        {
            DescripPartidaZoo::updateOrCreate(
                ['id'=>$request->id[$i]],
                [
                    'gen_cert_zoo_eu_id' => $genCertZooEu->id,
                    'codigo_nc'                     => $request->codigo_nc[$i],
                    'especie'                       => $request->especie[$i],
                    'almacen_frigo'                 => $request->almacen_frigo[$i],
                    'marc_ident'                    => $request->marc_ident[$i],
                    'tipos_embalaje'                => $request->tipos_embalaje[$i],
                    'nat_merc'                      => $request->nat_merc[$i],
                    'consumidor_final'              => $request->consumidor_final[$i],
                    'tipo_tratamiento'              => $request->tipo_tratamiento[$i],
                    'fecha_recogida'                => $request->fecha_recogida[$i],
                    'peso_neto'                     => $request->peso_neto[$i],
                    'num_bultos'                    => $request->num_bultos[$i],
                    'num_lotes'                     => $request->num_lotes[$i],
                ]);
        }

              Alert::success('Su Actualización ha sido Exitosa!')->persistent("Ok");
              return redirect()->action('certificadosZooEuController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\GenCertZooEu  $genCertZooEu
     * @return \Illuminate\Http\Response
     */
    public function destroy(GenCertZooEu $genCertZooEu)
    {
        //
    }

    private function generarRamdom()
    {
        $user = Auth::user();
        $random = Str::random(4);
        $id  = $user->id;
        $rif = substr($user->detUsuario->rif, -2);
        $fecha = date('Ymd');
        $UltimatSolicitud=GenCertZooEu::where('gen_usuario_id',Auth::user()->id)->get()->last();
        $idUltimatSolicitud = !empty($UltimatSolicitud) ? $UltimatSolicitud->id : 0;
        return $id.'-'.$random.$fecha.$rif.$idUltimatSolicitud;
    }



 public function deleteCertificado(Request $request)
    {
        $delete=DescripPartidaZoo::find($_GET['id'])->delete();

      
       if($delete)
       {
       
          return "Producto Eliminado";
       }else
       {
         return "Producto No existe";
       }
    }
}


