<?php

namespace App\Http\Controllers;
use Illuminate\Filesystem\Filesystem;
use App\Models\GenDVDSolicitud;
use App\Models\FacturaSolicitud;
use App\Models\GenFactura;
use App\Models\GenDua;
use App\Models\GenParametro;
use App\Models\GenSolicitud;
use App\Models\GenOperadorCamb;
use App\Models\GenNotaCredito;
use App\Models\GenNotaDebito;
use App\Models\FinanciamientoSolicitud;
use App\Models\NotaCreditoSolicitud;
use App\Models\DocumentosDvdBdv;
use App\Http\Requests\GenDVDSolicitudRequest;
use File;
use Session;
use Auth;
use Laracasts\Flash\Flash;
use Alert;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class VentaSolicitudController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
  public function index(Request $request)
    {
      // Consulta el ultimo monto pendiente de la ultima venta de esa factura
      $dvdsol=GenDVDSolicitud::where('gen_factura_id',$request->gen_factura_id)->where('gen_status_id',3)->where('tipo_solicitud_id',3)->orderby('created_at','DESC')->take(1)->first();
//dd($dvdsol);
      // Si viene llego se toma como monto pendiente el ultimo monto pendiente vendido
      if (!empty($dvdsol)) {
        $monto_pendiente=$dvdsol->mpendiente;
 //dd($monto_pendiente);

          /************************************/
        $gen_factura=GenFactura::select('monto_fob')->where('id',$request->gen_factura_id)->first();

        $solicitud=GenSolicitud::find($request->gen_solicitud_id);
        if ($solicitud->tipo_solicitud_id == 1) {

           //$nota=FinanciamientoSolicitud::where('gen_solicitud_id',$request->gen_solicitud_id)->first();
          
           $nota=NotaCreditoSolicitud::where('gen_solicitud_id',$request->gen_solicitud_id)->get();
           //dd($nota);
            foreach($nota as $key=>$value) {
              if(!empty($value->gen_nota_credito_id)){
                $idnota[]=$value->gen_nota_credito_id;
              }
              //$idnota[]=$value->gen_nota_credito_id;

            }
         //dd($nota->nota_credito);
            if (!empty($idnota)) {
               $notacredito=GenNotaCredito::whereIn('id',$idnota)->where('gen_factura_id',$request->gen_factura_id)->sum('monto_nota_credito');

              if (empty($notacredito) || $notacredito == 0) {
                 $monto_nota_credito=0;
              } else {
                 
                 $monto_nota_credito=$notacredito;
              }

            }else {
               $monto_nota_credito=0;
            }
        }elseif ($solicitud->tipo_solicitud_id == 2) {
          $notacredito=GenNotaCredito::where('gen_factura_id',$request->gen_factura_id)->sum('monto_nota_credito');
          if (empty($notacredito) || $notacredito == 0) {
                 $monto_nota_credito=0;
          } else {
             $monto_nota_credito=$notacredito;
          }
        }else {

          $monto_nota_credito=0;
        }

        $monto_fob=$gen_factura->monto_fob-$monto_nota_credito;
//dd($monto_fob);

        $fecha= date('Y-m-d');
        $gen_factura2=GenFactura::select('gen_dua_id')->where('id',$request->gen_factura_id)->first();
        $gen_dua=GenDua::where('id',$gen_factura2->gen_dua_id)->first();

        if (empty($gen_dua->fembarque)) {
            $fembarque=$fecha;
        } else {
            $fembarque=$gen_dua->fembarque;
        }

        $parametro14=GenParametro::where('tipo_convenio','14')->first();
        $parametro27=GenParametro::where('tipo_convenio','27 - 34')->first();
        $parametro34=GenParametro::where('tipo_convenio','34')->first();

        if((strtotime($fembarque)) >= (strtotime($parametro14->fdesde)) && (strtotime($fembarque)) <= (strtotime($parametro14->fhasta)) ){
          $montoventa=$parametro14->porcentaje_venta;
          $montoretencion=$parametro14->porcentaje_retencion;

        }elseif ((strtotime($fembarque)) >= (strtotime($parametro27->fdesde)) && (strtotime($fembarque)) <= (strtotime($parametro27->fhasta)) ) {
          $montoventa=$parametro27->porcentaje_venta;
          $montoretencion=$parametro27->porcentaje_retencion;

        }elseif ((strtotime($fembarque)) >= (strtotime($parametro34->fdesde)) && (strtotime($fembarque)) <= (strtotime($fecha)) || (strtotime($fembarque)) >= (strtotime($parametro34->fdesde)) && (strtotime($fembarque)) >= (strtotime($fecha)) ) {
          $montoventa=$parametro34->porcentaje_venta;
          $montoretencion=$parametro34->porcentaje_retencion;

        }

          //$ventabcv=$montoventa*$monto_fob/100; // venta bcv dependiando del %
          //$retencionusr=$montoretencion*$monto_fob/100;
              $monto=array(
                  'monto_fob'  => $monto_fob,
                  'montoventa'  => $montoventa,
                  'montoretencion'  => $montoretencion,
                  'montopendiente'  => $monto_pendiente

                );

      }else{
      // si no ha hecho ninguna venta
        /************************************/
      $gen_factura=GenFactura::select('monto_fob')->where('id',$request->gen_factura_id)->first();
      $solicitud=GenSolicitud::find($request->gen_solicitud_id);
      if ($solicitud->tipo_solicitud_id == 1) {
      $nota=NotaCreditoSolicitud::where('gen_solicitud_id',$request->gen_solicitud_id)->get();
      foreach($nota as $key=>$value) {
        if(!empty($value->gen_nota_credito_id)){
           $idnota[]=$value->gen_nota_credito_id;
        }

      //$idnota[]=$value->gen_nota_credito_id;

      }
       //dd($idnota);
         if (!empty ($idnota)) {
           $notacredito=GenNotaCredito::whereIn('id',$idnota)->where('gen_factura_id',$request->gen_factura_id)->sum('monto_nota_credito');
           //dd($notacredito); 
           if (empty($notacredito) || $notacredito == 0) {
             $monto_nota_credito=0;
           } else {
             $monto_nota_credito=$notacredito;
           }


         }else {
           $monto_nota_credito=0;
         }
      }elseif ($solicitud->tipo_solicitud_id == 2) {
         /*$notacredito=GenNotaCredito::where('gen_factura_id',$request->gen_factura_id)->get();*/
        $notacredito=GenNotaCredito::where('gen_factura_id',$request->gen_factura_id)->sum('monto_nota_credito');
          if (empty($notacredito) || $notacredito == 0) {
                 $monto_nota_credito=0;
          } else {
             $monto_nota_credito=$notacredito;
          }
      }else{
        $monto_nota_credito=0;
      }

       $monto_fob=$gen_factura->monto_fob-$monto_nota_credito;
//dd($monto_fob);

        $fecha= date('Y-m-d');
        $gen_factura2=GenFactura::select('gen_dua_id')->where('id',$request->gen_factura_id)->first();
        $gen_dua=GenDua::where('id',$gen_factura2->gen_dua_id)->first();

        if (empty($gen_dua->fembarque)) {
            $fembarque=$fecha;
        } else {
            $fembarque=$gen_dua->fembarque;
        }

        $parametro14=GenParametro::where('tipo_convenio','14')->first();
        $parametro27=GenParametro::where('tipo_convenio','27 - 34')->first();
        $parametro34=GenParametro::where('tipo_convenio','34')->first();

        if((strtotime($fembarque)) >= (strtotime($parametro14->fdesde)) && (strtotime($fembarque)) <= (strtotime($parametro14->fhasta)) ){
          $montoventa=$parametro14->porcentaje_venta;
          $montoretencion=$parametro14->porcentaje_retencion;

        }elseif ((strtotime($fembarque)) >= (strtotime($parametro27->fdesde)) && (strtotime($fembarque)) <= (strtotime($parametro27->fhasta)) ) {
          $montoventa=$parametro27->porcentaje_venta;
          $montoretencion=$parametro27->porcentaje_retencion;

        }elseif ((strtotime($fembarque)) >= (strtotime($parametro34->fdesde)) && (strtotime($fembarque)) <= (strtotime($fecha)) || (strtotime($fembarque)) >= (strtotime($parametro34->fdesde)) && (strtotime($fembarque)) >= (strtotime($fecha)) ) {
          $montoventa=$parametro34->porcentaje_venta;
          $montoretencion=$parametro34->porcentaje_retencion;

        }

          //$ventabcv=$montoventa*$monto_fob/100; // venta bcv dependiando del %
          //$retencionusr=$montoretencion*$monto_fob/100;
              $monto=array(
                  'monto_fob'  => $monto_fob,
                  'montoventa'  => $montoventa,
                  'montoretencion'  => $montoretencion,
                  'montopendiente'  => $monto_fob
                );
      }

        return response()->json($monto);

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(GenDVDSolicitudRequest $request)
    {
      
      //dd($request->gen_operador_cambiario_id);
      //dd($request->all());
      if(!isset($request->gen_factura_id)){
        $desestimiento = GenDVDSolicitud::where('gen_solicitud_id', $request->gen_solicitud_id)->whereNull('gen_factura_id')->count();
        if($desestimiento > 0){
          Alert::warning('Alerta','Registro No Exitoso, Ya existe un Desestimiento para esta Solicitud')->persistent("Ok");

      return redirect('exportador/ListaVenta/'.$request->gen_solicitud_id.'/edit');
        }
      }

    /*ramdom que te permite generar el cifrado del codigo de seguridad*/
    function generarRandom($longitud) {
        $key = '';
        $pattern = '1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $max = strlen($pattern)-1;
       // for($i=0;$i < $longitud;$i++) $key .= $pattern{mt_rand(0,$max)};
        for($i=0;$i < $longitud;$i++) $key .= $pattern[mt_rand(0,$max)];
        return $key;
    }
       $user=Auth::user()->id;
       $estatus=3;
       $fecha=date('Y-m-d');
       $random=generarRandom(5);
       $firma=$fecha.$user.$random;
       $cod_seguridad=md5($firma);

       if ($request->realizo_venta == 2 && empty($request->descripcion)) {
          $descripcion=$request->descripciondes;
        }else{
          $descripcion=$request->descripcion;
        }

        if(isset($request->gen_factura_id)){
          $factura_id = $request->gen_factura_id;
        }else{
          $factura_id = $request->gen_factura_id_2;
        }


         $monto_pendiente=$request->pendienteventa-$request->mpercibido;

           $insertdvd = new GenDVDSolicitud;
           $insertdvd->gen_solicitud_id=$request->gen_solicitud_id;
           $insertdvd->tipo_solicitud_id=3;
           $insertdvd->gen_status_id=$estatus;
           $insertdvd->gen_operador_cambiario_id=$request->gen_operador_cambiario_id;
           $insertdvd->cod_seguridad=$cod_seguridad;
           $insertdvd->fdisponibilidad=$request->fdisponibilidad;
           $insertdvd->gen_factura_id=$factura_id;
           $insertdvd->mfob=$request->mfob;
           $insertdvd->mpercibido=$request->mpercibido;
           $insertdvd->mvendido=$request->mvendido;
           $insertdvd->mretencion=$request->mretencion;
           $insertdvd->mpendiente=$monto_pendiente;
           $insertdvd->descripcion=$descripcion;
           $insertdvd->fstatus=$fecha;
           $insertdvd->fventa_bcv=$fecha;
           $insertdvd->realizo_venta=$request->realizo_venta;
           $insertdvd->save();

            if ($monto_pendiente==0 && $request->realizo_venta == 1) {
        $dua=FacturaSolicitud::where('gen_factura_id',$request->gen_factura_id)->where('gen_solicitud_id',$request->gen_solicitud_id)->update(['bactivo' => 0, 'updated_at' => date("Y-m-d H:i:s") ]);
        //dd($dua);
      }
      if(isset($request->gen_factura_id)){
        $fac=GenFactura::where('id',$request->gen_factura_id)->first();
        $dua2=GenDua::where('id',$fac->gen_dua_id)->first();
        if ($dua2->numero_dua != 'Sin Dua') {
          $desdua=GenDua::where('id',$fac->gen_dua_id)->update(['bactivo' => 0, 'updated_at' => date("Y-m-d H:i:s") ]);
        }



            $dvdsol=GenDVDSolicitud::where('gen_factura_id',$request->gen_factura_id)->where('gen_status_id',3)->where('tipo_solicitud_id',3)->orderby('created_at','DESC')->take(1)->first();

              Session::put('solicitud_dvd',$dvdsol->id);


      }

/*******Congelando la validacion de los doc para bdv y tesoro mientras se termina*****/
/*if(isset($request->ruta_doc_dvd) && ($request->gen_operador_cambiario_id == 2 || $request->gen_operador_cambiario_id == 18)){

  
        $destino = '/img/docbdv/'.Auth::user()->detUsuario->cod_empresa;
        $destinoPrivado = public_path($destino);
        
        if (!file_exists($destinoPrivado)) {
            $filesystem = new Filesystem();
$filesystem->makeDirectory($destinoPrivado, $mode = 0777, true, true); 
        }

      // dd($insertdvd->id);
//dd(count($request->ruta_doc_dvd));
      
        for( $i=0; $i <=count($request->ruta_doc_dvd)+2; $i++ ){
          if (array_key_exists($i, $request->ruta_doc_dvd)) {
            $imagen = $request->ruta_doc_dvd[$i];
            if (!empty($imagen)) {
               
               $nombre_imagen = $imagen->getClientOriginalName();
              
                $imagen->move($destinoPrivado, $nombre_imagen);
                
               
                $file = new DocumentosDvdBdv;

                $file->gen_dvd_solicitud_id      = $insertdvd->id;
                $file->cat_documentos_id  = $request->cat_documentos_id[$i];
                $file->ruta_doc_dvd       = $destino.'/'.$nombre_imagen;
                $file->save();
                
            }

          }
            

        }
}*/





      Alert::success('REGISTRO EXITOSO','DVD Agregada Exito Ya Puede Descargar su planilla')->persistent("Ok");

      return redirect('exportador/ListaVenta/'.$request->gen_solicitud_id.'/edit');


           //view('exportador/ListaVenta/'.$request->gen_solicitud_id.'/edit');
           //redirect()->action('ListaVentaController@edit')->with('id',$request->gen_solicitud_id);



       }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\GenDVDSolicitud  $genDVDSolicitud
     * @return \Illuminate\Http\Response
     */
    public function show(GenDVDSolicitud $genDVDSolicitud)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\GenDVDSolicitud  $genDVDSolicitud
     * @return \Illuminate\Http\Response
     */
    public function edit(GenDVDSolicitud $genDVDSolicitud, $id)
    {

      //dd($id);


       $facturasolicitud=FacturaSolicitud::where('gen_solicitud_id',$id )->where('bactivo',1)->get();
      
       foreach ($facturasolicitud as $key => $value) {
        $realizoVenta = GenDVDSolicitud::where('gen_solicitud_id',$id)->where('gen_factura_id',$value->gen_factura_id)->where('realizo_venta',2)->first();
        
        if(empty($realizoVenta)){
          $factura_id[]=$value->gen_factura_id;
          
        }

       }
   
       if (empty($factura_id)) {
         Alert::success('Facturas Vendidas','Ya realizo la venta total de la Solicitud')->persistent("Ok");
           return redirect('exportador/DvdSolicitud');
       }


       $facturas=GenFactura::whereIn('id',$factura_id)->pluck('numero_factura','id');
       $listaventa=GenDVDSolicitud::where('gen_solicitud_id',$id)->get();
       //preguntamos por los usuarios banco de produccion para que les muestre sus oca de prueba sino para los usuarios normales muestra los oca reales
       if (Auth::user()->id == 4|| Auth::user()->id == 5 || Auth::user()->id == 1 || Auth::user()->id == 283 || Auth::user()->id == 476) {
         $oca=GenOperadorCamb::all()->pluck('nombre_oca','id');
       } else {
         $oca=GenOperadorCamb::whereNotIn('id',[31,32,33])->pluck('nombre_oca','id');
       }
       //dd($facturasolicitud);
       //$factura_divisa=GenFactura::whereIn('id',$factura_id)->get();
       $solicitud=GenSolicitud::find($id);
       $genDvd=GenDVDSolicitud::where('gen_solicitud_id',$id)->sum('mpercibido');

        if ($solicitud->tipo_solicitud_id == 1) {
            //$nota=FinanciamientoSolicitud::where('gen_solicitud_id',$id )->first();
            $nota=NotaCreditoSolicitud::where('gen_solicitud_id',$id)->get();
              foreach($nota as $key=>$value) {

              $idnota[]=$value->gen_nota_credito_id;

              }
         //dd($nota->nota_credito);
  //dd($genDvd);
          if (!empty($idnota)) {
            $notacredito=GenNotaCredito::whereIn('id',$idnota)->sum('monto_nota_credito');
            //dd($notacredito);
            $monto_nota_credito=$notacredito;
          } else {
            $monto_nota_credito=0;
          }

        }elseif ($solicitud->tipo_solicitud_id == 2) {
          /*$notacredito=GenNotaCredito::where('gen_factura_id',$factura_id)->first();
          if (empty($notacredito)) {
                 $monto_nota_credito=0;
          } else {
             $monto_nota_credito=$notacredito->monto_nota_credito;
          }
         }else {
         $monto_nota_credito=0;*/
         //$nota=FinanciamientoSolicitud::where('gen_solicitud_id',$id )->first();
            $nota=NotaCreditoSolicitud::where('gen_solicitud_id',$id)->get();
              foreach($nota as $key=>$value) {

              $idnota[]=$value->gen_nota_credito_id;

              }
         //dd($nota->nota_credito);
  //dd($genDvd);
          if (!empty($idnota)) {
            $notacredito=GenNotaCredito::whereIn('id',$idnota)->sum('monto_nota_credito');
            //dd($notacredito);
            $monto_nota_credito=$notacredito;
          } else {
            $monto_nota_credito=0;
          }
        }



       $mvendido=0;
       //dd($solicitud->monto_solicitud);
       //foreach ($genDvd as $key => $value) {
           $mvendido=$mvendido+$genDvd;

       //}
       $idsolicitud=$id;
       $pendiente=$solicitud->monto_solicitud-$mvendido-$monto_nota_credito;
       
       $titulo=" Venta de las Divisas";
       $descripcion="Complete la información acerca de la Venta de las Divisas";
       return view('VentaSolicitud.create',compact('titulo', 'descripcion','facturas','oca','listaventa','solicitud','mvendido','pendiente','idsolicitud','monto_nota_credito'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\GenDVDSolicitud  $genDVDSolicitud
     * @return \Illuminate\Http\Response
     */
    public function update(GenDVDSolicitudRequest $request, GenDVDSolicitud $genDVDSolicitud)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\GenDVDSolicitud  $genDVDSolicitud
     * @return \Illuminate\Http\Response
     */
    public function destroy(GenDVDSolicitud $genDVDSolicitud)
    {
        //
    }
}
