<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ErRecepcionOca extends Model
{
    protected $table = 'er_recepcion_oca';

    protected $fillable = [

           'gen_dvd_solicitud_id',
           'gen_usuario_id',
           'bactivo',
    ];
    protected $date =['created_at','updated_at'];

    public function usuarios() {

     return $this->hasOne('App\Models\GenUsuario','id','gen_usuario_id');

    }
}
