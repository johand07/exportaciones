<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterDetDeclaracion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('det_declaracion_produc', function (Blueprint $table) {
            //
            $table->string('id_gen_consignatario')->nullable()->after('estado_p6');
            

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('det_declaracion_produc', function (Blueprint $table) {
            //
            $table->dropColumn('id_gen_consignatario');
            
        });
    }
}
