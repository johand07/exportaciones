<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DetProductoSolicitud extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('det_producto_solicitud', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('gen_solicitud_id')->index()->nullable();
            $table->foreign('gen_solicitud_id')->references('id')->on('gen_solicitud');
            $table->unsignedInteger('det_prod_factura_id')->index()->nullable();
            $table->foreign('det_prod_factura_id')->references('id')->on('det_prod_factura');
            $table->integer('cantidad')->nullable();
            $table->integer('disponible')->nullable();
            $table->double('monto')->nullable();
            $table->boolean('bactivo')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::dropIfExists('det_producto_solicitud');
    }
}
