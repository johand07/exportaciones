@extends('templates/layoutlte_coordinador_intranet')
@section('content')
<div class="row  text-center">
    <h3>Asignar Solicitudes</h3>
</div>
@if(isset($fecha_d))
<div class="row text-center">
    <p>Desde: {{$fecha_d}} Hasta: {{$fecha_h}}</p>
</div>
@endif
{{Form::open(['route'=>'AsignacionAnalista','method'=>'PATCH','id'=>'formAsignacion']) }}
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('', 'Seleccione un Analista') !!}
            {!!Form::select('gen_usuario_id',$analistas,null,['placeholder'=>'Seleccione un usuario','class'=>'form-control','id'=>'gen_usuario_id', 'required'=>'true']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <br>
        <a href="{{route('RecepcionSolCoordIntra.create')}}" class="btn btn-warning">Cancelar</a>
        <input class="btn btn-success" type="submit" name="" value="Asignar">
    </div>
</div>
<br><br>
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-8"></div>
                
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
                        <input id="system_search" name="busqueda" type="text" class="form-control" placeholder="Buscar">
                    </div>
                </div>
            </div>
            <table id="recepcionIntra" class="table-list-search tbody table table-bordered table-hover" style="width:100%">
                <thead>
                    <tr class="text-center">
                        <th class="col-md-2 text-center">Rif</th>  
                        <th class="col-md-2 text-center">Razon Social</th>  
                        <th class="col-md-2 text-center">Nº Solicitud</th> 
                        <th class="col-md-2 text-center">Tipo</th>                    
                        <th class="col-md-3 text-center">Nº Factura</th> 
                        <th class="col-md-3 text-center">OCA</th>
                        <th class="col-md-2 text-center">Seleccionar</th>
                    
                        
                    </tr>
                </thead>
                <tbody>
                    @foreach($solicitudes as $key =>  $solicitud)
                        @if($solicitud->bactivo == 1)
                            <tr >           
                                <td class="text-center"> {{ $solicitud->rif }}</td>          
                                <td class="text-center"  >{{ $solicitud->razon_social }}</td>
                                <td class="text-center"  ><b class="text-primary">{{ $solicitud->gen_solicitud_id }}</b></td>
                                <td class="text-center"  >{{ $solicitud->solicitud }}</td>
                                <td class="text-center"  >{{ $solicitud->numero_factura }}</td>
                                <td class="text-center"  >{{ $solicitud->nombre_oca }}</td>
                                <td class="text-center"  >
                                    <div id="solicitudes">
                                        <input type="checkbox" name="gen_solicitud_id[]" id="gen_solicitud_id{{ $key }}"  value="{{ $solicitud->gen_solicitud_id }}">
                                    </div>
                                </td>
                                
                            </tr>  
                        @endif 
                    @endforeach
                </tbody>
            </table>
        </div>
        
    </div>
{{Form::close()}}
@stop
<script>
   function validar_checkbox() {
    // Obtener hijos dentro de etiqueta <div>
    let cont = document.getElementById('solicitudes').children;
    console.log('cont', cont);
    let i = 0;
    let al_menos_uno = false;
    //Recorrido de checkbox's
    while (i < cont.length) {
        // Verifica si el elemento es un checkbox
        if (cont[i].tagName == 'INPUT' && cont[i].type == 'checkbox') {
            // Verifica si esta checked
            if (cont[i].checked) {
                al_menos_uno = true;
            }
        }
        i++
    }
    $("#formAsignacion").submit(function(e) {
    
        //Valida si al menos un checkbox es checked
        if (!al_menos_uno) {
            // alert('Selecciona al menos un checkbox');
            swal("No se puede Enviar", "Debe seleccionar al menos una solicitud", "warning");
            if (e.preventDefault) {
                e.preventDefault();
            } else {
                e.returnValue = false;
            }
        }
    
    });
    
}
</script>