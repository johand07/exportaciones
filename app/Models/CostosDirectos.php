<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CostosDirectos extends Model
{
    protected $table = 'costos_directos';
    protected $fillable = [

           'planilla_3_id',
           'mate_importados',
           'mate_nacional',
           'mano_obra',
           'depreciacion',
           'otros_costos_dir',
           'total',
           'bactivo',
          
    ];
   
    public function planilla3Id() 
    {

      return $this->hasOne('App\Models\Planilla3','id','planilla_3_id');

    }

   


}
