<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GestionInvitacion extends Model
{
    protected $table = 'gestion_invitacion';

    protected $fillable = [
    	
    	'eventos_id',
    	'repre_legal',
    	'cedula',
    	'razon_social',
    	'rif',
    	'asistencia',
    	'invitacion_enviada',
    	'bactivo'
    ];

    protected $date =['created_at','updated_at'];

    public function eventos() {

     return $this->hasOne('App\Models\Eventos','id','eventos_id');

    }
}
