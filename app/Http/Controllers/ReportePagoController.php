<?php

namespace App\Http\Controllers;
use Illuminate\Filesystem\Filesystem;
use App\Models\ReportePago;
use Illuminate\Http\Request;
use App\Models\GenOperadorCamb;
use Alert;
use File;

class ReportePagoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $tipo_pago = $request->tipo_pago;

        $solicitud_id = $request->solicitud_id;

        $cat_banco_admin_id = array('1' => 'Venezuela', '2' => 'Mercantil', '3' => 'Tesoro', '4' => 'Banesco');

        $gen_operador_cambiario_id = GenOperadorCamb::pluck('nombre_oca','id');

        return view('reportePagos.create', compact('tipo_pago', 'cat_banco_admin_id', 'gen_operador_cambiario_id', 'solicitud_id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'img_capture'                   => 'mimes:pdf,png,jpg,jpeg|max:1000',
            'cat_banco_admin_id'            => 'required',
            'gen_operador_cambiario_id'     => 'required',
            //'num_referencia'                => 'required',
            'fecha_reporte'                 => 'required',
        ]);

        //Guardar Img
        $ruta_img = null;
        if(isset($request->img_capture)){
            $destino = 'img/captures';
            $destinoPrivado = public_path($destino);
            
            if (!file_exists($destinoPrivado)) {
                $filesystem = new Filesystem();
                $filesystem->makeDirectory($destinoPrivado, $mode = 0777, true, true); 
            }


            

                $imagen = $request->file('img_capture');
                
                if (!empty($imagen)) {
                    
                $nombre_imagen = $imagen->getClientOriginalName();
                
                    $imagen->move($destinoPrivado, $nombre_imagen);
                   
                
                }
                
                $ruta_img = $destino.'/'.$nombre_imagen;
            
        }
        

        $reporte_Pago = new ReportePago;
        $reporte_Pago->tipo_pago_id                 = $request->tipo_pago;
        $reporte_Pago->cat_banco_admin_id           = $request->cat_banco_admin_id;
        //$reporte_Pago->gen_operador_cambiario_id    = '';//$request->'';
        $reporte_Pago->gen_sol_inversionista_id     = $request->solicitud_id;
        //$reporte_Pago->gen_dvd_solicitud_id         = '';//$request->'';
        //$reporte_Pago->gen_declaracion_jo_id        = '';//$request->'';
        //$reporte_Pago->gen_certificado_id           = '';// $request->'';
        $reporte_Pago->num_referencia               = $request->numero_referencia;
        $reporte_Pago->fecha_reporte                = $request->fecha_reporte;
        $reporte_Pago->img_capture                  = $ruta_img;
        $reporte_Pago->fstatus                      = date('Y-m-d');
        $reporte_Pago->gen_status_id                = 19;
        

        if($reporte_Pago->save()){
            Alert::success('Ha Registrado el pago de forma exitosa.','Exitosa!')->persistent("OK");
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ReportePago  $reporte_Pago
     * @return \Illuminate\Http\Response
     */
    public function show(ReportePago $reporte_Pago)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ReportePago  $reporte_Pago
     * @return \Illuminate\Http\Response
     */
    public function edit(ReportePago $reporte_Pago)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ReportePago  $reporte_Pago
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ReportePago $reporte_Pago)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ReportePago  $reporte_Pago
     * @return \Illuminate\Http\Response
     */
    public function destroy(ReportePago $reporte_Pago)
    {
        //
    }
}
