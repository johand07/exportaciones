<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CatSectorInversionista extends Model
{
    protected $table='cat_sector_inversionista';

    protected $fillable =[

     'id',
     'nombre',
     'bactivo'
    ];
}
