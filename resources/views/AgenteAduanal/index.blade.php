@extends('templates/layoutlte')

@section('content')
<div class="panels">
<div class="row">
  <div class="col-md-1"></div>
  <div class="col-md-10">
    <div class="row">
    <div class="col-md-3">
        <a class="btn btn-primary" href="{{url('exportador/AgenteAduanal/create')}}">
          <span class="glyphicon glyphicon-file" title="Agregar" aria-hidden="true"></span>
           Registro Agente de Aduana
        </a>
    
      </div>
    </div>    <hr>
    <table id="listaAnticipo" class="table table-striped table-bordered" style="width:100%">
            <thead>
                <tr>
                    <th>Nombre agente</th>
                    <th>Rif</th>
                    <th>Número registro</th>
                    <th>Ciudad</th>
                    <th>Teléfono</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody>
            	@foreach($agentes as $agen)
                <tr>
                    <td>{{$agen->nombre_agente}}</td>
                    <td>{{$agen->letra_rif_agente}}{{$agen->rif_agente}}</td>
                    <td>{{$agen->numero_registro}}</td>
                    <td>{{$agen->ciudad_agente}}</td>
                    <td>{{$agen->telefono_agente}}</td>

                    <td>
                    <!--Ruta y metodo-->
                    <a href="{{route('AgenteAduanal.edit',$agen->id)}}" class="btn btn-primary btn-sm"><i class="glyphicon glyphicon-edit"></i></a>
                    <!--Boton eliminar-->
                    <input id="token" type="hidden" name="_token" value="{{ csrf_token() }}">
                       <a href="#" class="btn btn-danger btn-sm" title="Eliminar" onclick="return eliminar('AgenteAduanal/',{{$agen->id}},'listaAnticipo')"> <i class="glyphicon glyphicon-trash"></i> </a>


                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
  </div>
  <div class="col-md-1"></div>
</div>
</div>
@stop
