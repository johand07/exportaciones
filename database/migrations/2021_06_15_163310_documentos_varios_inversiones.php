<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DocumentosVariosInversiones extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('documentos_varios_inversiones', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('cat_documentos_id')->index()->nullable();
            $table->foreign('cat_documentos_id')->references('id')->on('cat_documentos');
            $table->unsignedInteger('gen_sol_inversionista_id')->index()->nullable();
            $table->foreign('gen_sol_inversionista_id')->references('id')->on('gen_sol_inversionista');
            $table->string('file', 100)->nullable();
            $table->boolean('bactivo')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('documentos_varios_inversiones');
    }
}
