<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GenAccionistasDeclaracionDjir_01 extends Model
{
    protected $table = 'gen_accionistas_declaracion_djir01';
    
    protected $fillable = [
        'gen_declaracion_inversion_id',
        'razon_social_accionista',
        'identificacion_accionista',
        'nacionalidad_accionista',
        'capital_social_suscrito',
        'capital_social_pagado',
        'porcentaje_participacion',
    ];

    public function genDecInversion()
	{
        return $this->hasOne('App\Models\GenDeclaracionInversion','id','gen_d_inversion_id');
    }
    
}
