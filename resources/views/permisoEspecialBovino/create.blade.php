@extends('templates/layoutlte')
@section('content')

{{Form::open(['route'=>'permisoEspecialBovino.store','method'=>'POST','id'=>'autorizacion_bovino','enctype' => 'multipart/form-data']) }}

<div class="panels">
<div class="panel panel-primary">
    <div class="panel-primary" style="#fff">
      	<div class="panel-heading"><h4>Permiso Especial Ganado Bovino</h4></div>
			<div class="panel-body">
			<div class="row"><br><br><br>
				<div class="col-md-12">
					<div class="col-md-4">
						<div class="form-group">
							<label><b>Aranceles</b></label>
							<select name="codigo_arancel" id="codigo_arancel" class="form-control text-select" onchange="valArancel()" required="true">

								<option value="">Seleccione un código arancelario</option>
								@foreach($aranceles as $arancel)

									<option value="{{$arancel->codigo}}">{{$arancel->codigo}}</option>
								
								@endforeach
							</select>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							{!! Form::label('','Descripción arancel')!!}
							{!! Form::text('descripcion_arancel',null,['class'=>'form-control','id'=>'descripcion_arancel','readonly'=>'true','onkeypress'=>'return soloNumerosyLetras(event)','required'=>'true'])!!}
						</div>  
					</div>
					<div class="col-md-4">
						<div class="form-group">
							{!! Form::label('','Volumen')!!}
							{!! Form::text('volumen',null,['class'=>'form-control','id'=>'volumen','onkeypress'=>'return soloNumerosyLetras(event)','required'=>'true'])!!}
						</div>
					</div>
				</div>
			</div>
			<div class="row"><br><br>
				<div class="col-md-12">
					<div class="col-md-4">
						<div class="form-group">
						{!! Form::label('','Puerto de salida')!!}
						{!! Form::text('puerto_salida',null,['class'=>'form-control','id'=>'puerto_salida','onkeypress'=>'return soloNumerosyLetras(event)','required'=>'true'])!!}
						</div> 
					</div>
					<div class="col-md-4">
						<div class="form-group">
						{!! Form::label('','Puerto de entrada')!!}
						{!! Form::text('puerto_entrada',null,['class'=>'form-control','id'=>'puerto_entrada','onkeypress'=>'return soloNumerosyLetras(event)','required'=>'true'])!!}
						</div>  
					</div>
					<div class="col-md-4">
						<div class="form-group">
						{!! Form::label('','Fecha de embarque')!!}
						<div class="input-group date">
							{!! Form::text('fembarque_bovino',null,['class'=>'form-control','readonly'=>'readonly','id'=>'fembarque_bovino','required'=>'true']) !!}
							<span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
						</div>
					</div>
				</div>
			</div>
		</div><br><br><br>
		<div class="row text-center">
			<a href="{{ url('/exportador/permisoEspecialBovino')}}" class="btn btn-primary">Cancelar</a>
			<!-- <input type="submit" class="btn btn-success" value="Enviar" name="Enviar">  -->
			<input type="button" class="btn btn-success" value="Enviar" name="Enviar" onclick="enviarBovino()"> 
		</div><br>
		</div>
	</div>
</div>
</div>

{{Form::close()}}
@stop

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="{{asset('js/ajax_v2.1.1.js')}}"></script>

<script src="https://unpkg.com/@popperjs/core@2"></script>
<script src="https://unpkg.com/tippy.js@6"></script>


<script type="text/javascript">
	function valArancel(){
		let arancel = $('#codigo_arancel').val();
		console.log('arancel' +arancel);
		if (arancel == '0102.29.90.90') {
			$('#descripcion_arancel').val('Los demás');
		} else {
			$('#descripcion_arancel').val('Preñadas o con cría al pie');
		}

	}

	function enviarBovino() {
		let input_basicos = $('#autorizacion_bovino').find('input, select, textarea').not(':button,:hidden, :file');
		
		let campos = [];
		let nombre_campos = [];
		for(let i=0; i < input_basicos.length; i++){
			console.log(input_basicos[i].id);
			campos.push(input_basicos[i].id);
			if(input_basicos[i].id == 'fembarque_bovino'){
				nombre_campos.push($('#'+input_basicos[i].id).parent().parent().find('label')[0].innerHTML);
			}
			else
			{
				nombre_campos.push($('#'+input_basicos[i].id).parent().find('label')[0].innerHTML);
			}
		}
		
		$("div").remove(".msg_alert");

		let err_duda = 0;
		let error_dudas = '<div class="msg_alert"><span style="color:red" class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span><strong id="cod_aran-error" class="added"> El campo es obligatorio.</strong></div>';
		let valido_dudas = '<div class="msg_alert"><span style="color:green" class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span></div>';
		
		let n = campos.length;
		let input_duda = '';

		for (var i = 0; i < n; i++) {
			input_duda = $('#'+campos[i]);
			
			if (input_duda.val() == '')
			{
				err_duda = 1;
				input_duda.css('border', '1px solid red').after(error_dudas);
				if(err_duda==1){
					event.preventDefault(); 
					swal("¡Por Favor!", 'Estimado Usuario. Debe Ingresar '+nombre_campos[i].toUpperCase()+' para Completar el Registro', "warning")
				}
			}
			else{
				if (err_duda == 1) {err_duda = 1;}else{err_duda = 0;}
				input_duda.css('border', '1px solid green').after(valido_dudas);
			}
		}

		if(err_duda == 0){
			$('#autorizacion_bovino').trigger('submit');
		}
		return err_duda;
	}
	
</script>