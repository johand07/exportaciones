<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NotaCreditoSolicitud extends Model
{
    protected $table = 'NotaCreditoSolicitud';

    protected $fillable = [
       'gen_nota_credito_id',
       'gen_solicitud_id',
       'bactivo',
    ];
    protected $date =['created_at','updated_at'];

    public function notaCredito() {

     return $this->hasOne('App\Models\gen_nota_credito','id','gen_nota_credito_id');

    }

    
}