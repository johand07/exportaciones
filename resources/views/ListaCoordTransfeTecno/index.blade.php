@extends('templates/layoutlte_coord_inversionista')

@section('content')

<div class="content" style="background-color: #fff">
    <div class="panel-group" style="background-color: #fff;">
      <div class="panel-primary">
        <div class="panel-heading"><h3>Solicitudes de Transferencia Tecnológica</h3> </div>
        <div class="panel-body">
            <div class="row">
              
              <div class="col-md-12">
                <div class="row">
                  <div class="col-md-8">

                           <h4 class="text-info">Bandeja de Solicitudes de Transferencia Tecnológica:</h4><hr>
                          
                  </div>
                  <div class="col-md-2">
                   
                  </div>
                  <div class="col-md-2">
                    <a class="btn btn-primary" href="#">
                      <span class="glyphicon glyphicon-refresh" title="Agregar" aria-hidden="true"></span>
                        Actualizar
                    </a>
                  </div>
                </div>
                <table id="listaCoordTransfTecno" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                      <tr class="text-center">
                        <th></th>
                        <th class="col-md-1 text-center">N°</th>
                        <th class="col-md-2 text-center">N° Solicitud</th>
                        <th class="col-md-1 text-center">N° Pasaporte</th>
                        <th class="col-md-1 text-center">Tipo de Visa</th>
                        <th class="col-md-2 text-center">País de Origen</th>
                        <th class="col-md-2 text-center">Fecha de Solicitud</th>
                        <th class="col-md-1 text-center">Estatus</th>
                        <th class="col-md-2 text-center">Acciones</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                         <td></td>
                         <td></td>
                         <td></td>
                         <td></td>
                         <td></td>
                         <td></td>
                         <td></td>
                         <td></td>
                         <td>
                           <!--<a href="#" class="glyphicon glyphicon-edit btn-primary btn-sm""></a>-->

                           
                            <a href="#" class="text-center btn btn-sm btn-success list-inline"><i class="glyphicon glyphicon-share"></i><b>Verficado</b></a>
                         </td>
                      </tr>
                    </tbody>   
                </table>
              </div>
            </div><!-- Cierre 1° Row-->
        </div>
      </div>
    </div>
</div><!-- fin content-->
@stop