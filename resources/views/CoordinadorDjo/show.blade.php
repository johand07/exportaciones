@extends('templates/layoutlte_coordinador_djo')
@section('content')

<!-- MENSAJES DE OBSERVACION CORREGIDA -->
	@if(!empty($planilla2->estado_observacion))
		<script>
			document.onreadystatechange = function () 
			{
			  var state = document.readyState;
			  if (state == 'complete') {
			    swal("Observaciones", "{{$planilla2->descrip_observacion}}", "warning");
			  }
			}
		</script>
		<div class="alert alert-warning" role="alert">
			Observación:
			<br>
			<ul>
				<li>{{$planilla2->descrip_observacion}}</li>
			</ul>
		</div>
	@endif
<!-- FIN MENSAJES -->

<div class="content" style="background-color: #fff">
{!! Form::model($producDeclaracion->rDeclaracion, ['method'=>'PATCH','route'=>['coordinadorDjo.show',$producDeclaracion->id]]) !!}

    <div class="panel-group" style="">
      	<div class="panel-primary">
	        <div class="panel-heading">
	        	<div class="row">
		        	<div class="col-md-6"><h3> ANÁLISIS DE DECLARACIÓN JURADA DE ORIGEN</h3></div>
		        	<div class="col-md-2">
		        		<h4>Tipo de Usuario</h4>
		        		{{ ($gen_Declaracion->tipoUsuario)?$gen_Declaracion->tipoUsuario:'' }}
		        	</div>
		        	<div class="col-md-2">
		        		<h4>Nº Solicitud:</h4>
		        		{{ $producDeclaracion->num_solicitud }}
		        		<input type="hidden" name="gen_declaracion_jo_id" value="{{$producDeclaracion->id}}">
		        		<input type="hidden" name="num_solicitud" value="{{$producDeclaracion->num_solicitud}}">
		        	</div>
		        	<div class="col-md-2">
		        		
		        		<h4>Fecha Solicitud:</h4>
		        		{{ $producDeclaracion->created_at }}
		        	</div>			
	        	</div>
	        </div>
		</div>
		<br>
<!-----Documentos para declaración de origen perfil analista--->
<div class="row">
	<div class="col-md-12">
		<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">SOPORTES</button><br>
	</div>
</div>
<!-----Documentos para declaración de origen perfil analista--->
        <div class="panel-body">

			<table class="col-md-12" border="2">
				<thead>
					<th class="col-md-2">RIF</th>
					<th class="col-md-2">EMPRESA</th>
					<th class="col-md-4" colspan="8">FECHA EMISIÓN</th>
					<th class="col-md-4" colspan="8">FECHA VENCIMIENTO</th>

				</thead>
				<tbody>
					<tr class="text-center">
						<td>	
							{{ $producDeclaracion->rDeclaracion->rDetUsuario->rif}}
						</td>
						<td> 
							{{ $producDeclaracion->rDeclaracion->rDetUsuario->razon_social}}
							{{-- dd($producDeclaracion) --}}
						</td>
						<td colspan="8" align="center">
							 <div class="input-group date">
								 {!! Form::text('fecha_emision', null, ['class' => 'form-control text-center','placeholder'=>'Ingresa fecha emisión','id'=>'fecha_emision', 'required']) !!}
								 <span style="background-color: #3c8dbc;color:#fffa" class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
							</div>
						</td>
						<td colspan="8" align="center">
						 	<div class="input-group date"> 
							 {!! Form::text('fecha_vencimiento', null, ['class' => 'form-control text-center','placeholder'=>'Ingresa fecha vencimiento','id'=>'fecha_vencimiento', 'required']) !!}
							  <span style="background-color: #3c8dbc;color:#fffa" class="input-group-addon "><i class="glyphicon glyphicon-th"></i></span>
							</div>
						</td>
					</tr>
					<tr>
						<th class="text-center">
							Código Arancelario
						</th>
						<th rowspan="2" class="text-center">
							Denominación Comercial del Producto
						</th>
						<th colspan="15" class="text-center">
							Criterios de Origen
						</th>
						
					</tr>
					<tr class="text-center">
						<td style="font-size: 12px;">
							<div class="col-md-12 text-left text-danger"><b>Nacional NCM</b></div>
							<div class="col-md-12 text-right text-success"><b>Naladisa 96</b></div>
						</td>
				
						<td><b>BOL</b></td>
						<td><b>COL</b></td>
						<td><b>ECU</b></td>
						<td><b>PER</b></td>
						<td><b>CUB</b></td>
						<td><b>ALD</b></td>
						<td><b>ARG</b></td>
						<td><b>BRA</b></td>
						<td><b>PAR</b></td>
						<td><b>URU</b></td>
						<td><b>USA</b></td>
						<td><b>UE</b></td>
						<td><b>CND</b></td>
						<td><b>TP</b></td>
						<td><b>TR</b></td>
												
					</tr>
					@foreach($producDeclaracion->rCriterioAnalisis as $key =>  $productos)

						<tr class="text-center">
							<td style="font-size: 12px;">
								@php 
						 
									$boton_cod_mer='<botton id="boton_mer_'.$productos->det_declaracion_produc_id.'" class="btn btn-xs cargar_arancel btn-danger" data-toggle="modal" data-target="#arancel" data-tipo-arancel="mer" data-indice="'.$productos->det_declaracion_produc_id.'"><i class="glyphicon glyphicon-plus"></i></botton>';
									$boton_cod_mer= @$productos->arancel_mer ? '' : $boton_cod_mer;
									$boton_cod_nan='<botton type="button" required id="boton_nan_'.$productos->det_declaracion_produc_id.'" class=" btn btn-xs cargar_arancel btn-success" data-toggle="modal" data-target="#arancel" data-tipo-arancel="nan" data-indice="'.$productos->det_declaracion_produc_id.'"><i class="glyphicon glyphicon-plus"></i></button>';						
									$boton_cod_nan= @$productos->arancel_nan ? '' : $boton_cod_nan;

								@endphp

								<div class="col-md-12 text-left text-danger" style="padding-left: 6px;">
									
									{!! $boton_cod_mer or '' !!}
									<input readonly="true" size="10" class="text-left" style="border:0px;" id="cod_mer_{{$productos->det_declaracion_produc_id}}" type="text" name="cod_mer[{!! $productos->det_declaracion_produc_id !!}]" value="{{ $productos->arancel_mer or '' }}" required>
								
								</div>
								<div class="col-md-12 text-right text-success" style="padding-right: 2px;">
									
									<input readonly="true" size="10" class="text-right" style="border:0px;" id="cod_nan_{{$productos->det_declaracion_produc_id}}" type="text" name="cod_nan[{!! $productos->det_declaracion_produc_id !!}]" value="{{ $productos->arancel_nan or '' }}" required>
									{!! $boton_cod_nan or '' !!}

									

								</div>
							</td>
							<td>
								<b class="text-primary">{{ $productos->declaracionProduc->rProducto->descrip_comercial }}</b>
								<br> 
									{{ $productos->declaracionProduc->rProducto->descripcion  }} 
								<br>
								<input class="form-control" type="hidden" readonly="true" name="productoId[]" value="{{$productos->det_declaracion_produc_id}}">
								<input class="form-control" type="text" placeholder="Descripción Adicional" style="font-size: 12px;" name="arancel_descrip_adicional[{{$productos->det_declaracion_produc_id}}]" value="{{$productos->arancel_descrip_adicional}}" required>
								<button type="button" class="btn btn-success btn-xs btn-view-planillas" data-toggle="modal" data-target="#modalPlanillas" data-declaracion-product="{{ $productos->det_declaracion_produc_id }}">{{ $key+1 }}.) Ver P(2-3-4-5-6)</button>
							</td>
							@for($i=1;$i<=15;$i++)
								<td align="center" style="width: 4%;">
									@php $idPaises=[]; @endphp
									@php $idConvenio=[]; @endphp
									@php $cat_ue=["W"=>"W","P"=>"P"]; $cat_usa=["P"=>"P"]; $cat_cnd=["F"=>"F","P"=>"P"]; @endphp

									@foreach ($productos->declaracionProduc->rDeclaracionPaises as $key => $value)
										@php $idPaises[]=$value->pais_id; @endphp
										@php $idConvenio[]=$value->rPais->cconvenio; @endphp
									@endforeach

									
									@if($i==1) {{-- Bolivia --}}
										@if (in_array("23", $idPaises)) 
									   		{!!Form::select('bol['.$productos->det_declaracion_produc_id.']',$cat_bolivia,$productos->bol,['style'=>'width:90%;padding:0px;!important;font-size:12px;','placeholder'=>'-','class'=>'form-control','id'=>'id_bol','required']) !!}
										@endif
									@endif
									@if($i==2) {{-- colombia --}}
										@if (in_array("37", $idPaises)) 
									   		{!!Form::select('col['.$productos->det_declaracion_produc_id.']',$cat_colombia,$productos->col,['style'=>'width:90%;padding:0px;!important;font-size:12px;','placeholder'=>'-','class'=>'form-control','id'=>'id_col','required']) !!}
										@endif
									@endif	
									@if($i==3) {{-- ecuador --}}
										@if (in_array("46", $idPaises)) 
									   		{!!Form::select('ecu['.$productos->det_declaracion_produc_id.']',$cat_aladi,$productos->ecu,['style'=>'width:90%;padding:0px;!important;font-size:12px;','placeholder'=>'-','class'=>'form-control','id'=>'id_ecu','required']) !!}
										@endif
									@endif	
									@if($i==4) {{-- peru --}}
										@if (in_array("128", $idPaises)) 
									   		{!!Form::select('per['.$productos->det_declaracion_produc_id.']',$cat_peru,$productos->per,['style'=>'width:90%;padding:0px;!important;font-size:12px;','placeholder'=>'-','class'=>'form-control','id'=>'id_per','required']) !!}
										@endif
									@endif	
									@if($i==5) {{-- cuba --}}
										@if (in_array("43", $idPaises)) 
									   		{!!Form::select('cub['.$productos->det_declaracion_produc_id.']',$cat_cuba,$productos->cub,['style'=>'width:90%;padding:0px;!important;font-size:12px;','placeholder'=>'-','class'=>'form-control','id'=>'id_cub','required']) !!}
										@endif
									@endif	
									@if($i==6) {{-- ald --}}
										@if (in_array("ald", $idConvenio))
									   		 {!!Form::select('ald['.$productos->det_declaracion_produc_id.']',$cat_aladi,$productos->ald,['style'=>'width:90%;padding:0px;!important;font-size:12px;','placeholder'=>'-','class'=>'form-control','id'=>'id_ald','required'])!!}
										@endif
									@endif	
									@if($i==7) {{-- argentina --}}
										@if (in_array("10", $idPaises)) 
									   		{!!Form::select('arg['.$productos->det_declaracion_produc_id.']',$cat_mercosur,$productos->arg,['style'=>'width:90%;padding:0px;!important;font-size:12px;','placeholder'=>'-','class'=>'form-control','id'=>'id_arg','required']) !!}
										@endif
									@endif	
									@if($i==8) {{-- brasil --}}
										@if (in_array("25", $idPaises)) 
									   		{!!Form::select('bra['.$productos->det_declaracion_produc_id.']',$cat_mercosur,$productos->bra,['style'=>'width:90%;padding:0px;!important;font-size:12px;','placeholder'=>'-','class'=>'form-control','id'=>'id_bra','required']) !!}
										@endif
									@endif	
									@if($i==9) {{-- paraguay --}}
										@if (in_array("127", $idPaises)) 
									   		{!!Form::select('par['.$productos->det_declaracion_produc_id.']',$cat_mercosur,$productos->par,['style'=>'width:90%;padding:0px;!important;font-size:12px;','placeholder'=>'-','class'=>'form-control','id'=>'id_par','required']) !!}
										@endif
									@endif	
									@if($i==10) {{-- uruguay --}}
										@if (in_array("164", $idPaises)) 
									   		{!!Form::select('uru['.$productos->det_declaracion_produc_id.']',$cat_mercosur,$productos->uru,['style'=>'width:90%;padding:0px;!important;font-size:12px;','placeholder'=>'-','class'=>'form-control','id'=>'id_uru','required']) !!}
										@endif
									@endif	
									@if($i==11) {{-- usa --}}
										@if (in_array("53", $idPaises)) 
									   		{!!Form::select('usa['.$productos->det_declaracion_produc_id.']',$cat_usa,$productos->usa,['style'=>'width:90%;padding:0px;!important;font-size:12px;','placeholder'=>'-','class'=>'form-control','id'=>'id_usa','required']) !!}
										@endif
									@endif	
									@if($i==12) {{-- union_europea --}}
										@if (in_array("ue", $idConvenio))
									   		 {!!Form::select('ue['.$productos->det_declaracion_produc_id.']',$cat_ue,$productos->ue,['style'=>'width:90%;padding:0px;!important;font-size:12px;','placeholder'=>'-','class'=>'form-control','id'=>'id_ue','required'])!!}
										@endif
									@endif	
									@if($i==13) {{-- canada --}}
										@if (in_array("32", $idPaises)) 
									   		{!!Form::select('cnd['.$productos->det_declaracion_produc_id.']',$cat_cnd,$productos->cnd,['style'=>'width:90%;padding:0px;!important;font-size:12px;','placeholder'=>'-','class'=>'form-control','id'=>'id_cnd','required']) !!}
										@endif
									@endif	
									@if($i==14) {{-- terceros paises --}}
										@if (in_array("tp", $idConvenio))
									   		 {!!Form::select('tp['.$productos->det_declaracion_produc_id.']',$cat_aladi,$productos->tp,['style'=>'width:90%;padding:0px;!important;font-size:12px;','placeholder'=>'-','class'=>'form-control','id'=>'id_tp','required'])!!}
										@endif
									@endif
									@if($i==15)  {{--turquia --}}	
									@if (in_array("omc", $idConvenio))
								   		
										{!!Form::select('tr['.$productos->det_declaracion_produc_id.']',$cat_turquia,$productos->tr,['style'=>'width:90%;padding:0px;!important;font-size:12px;','placeholder'=>'-','class'=>'form-control','id'=>'id_tp','required'])!!}
										
									@endif
								@endif																	
								</td>
							@endfor
						</tr>
					@endforeach
					<tr>
						<td colspan="17">
							<div class="col-md-12" style="margin-bottom: 10px;">

								<b><u>Observaciones:</u></b>
								<textarea rows="4"  class="form-control text-left" name="observacion_analisis" style="font-weight:bold; text-transform: uppercase;" >{{ $producDeclaracion->observacion_analisis or '' }}</textarea>
							</div>
						</td>
					</tr>	

					
				</tbody>
			</table>
        </div>
		
        <hr>

      <div class="panel-footer">
        	<div class="row">
	        	<div class="col-md-12 text-center">
				<a href="{{ route('coordinadorDjo.index')}}" class="btn btn-primary btn-lg">Cancelar</a>
	        	</div>
        	</div>-
        </div>
      </div>
    </div>
</div>
{{Form::close()}}
<!-- Modal -->
<div class="modal fade" id="modalPlanillas" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>

                </button>
                 <h4 class="modal-title" id="myModalLabel">Planillas Declaracion Jurada de Origen</h4>

            </div>
            <div class="modal-body">
                <div role="tabpanel">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#p2" class="cargador-planilla" aria-controls="p2" role="tab" data-toggle="tab" id="tab-inicial">Planilla 2</a>
                        </li>
                        <li role="presentation"><a href="#p3" class="cargador-planilla" aria-controls="p3" role="tab" data-toggle="tab">Planilla 3</a>
                        </li>
                        <li role="presentation"><a href="#p4" class="cargador-planilla" aria-controls="p4" role="tab" data-toggle="tab">Planilla 4</a>
                        </li>
                        <li role="presentation"><a href="#p5" class="cargador-planilla" aria-controls="p5" role="tab" data-toggle="tab">Planilla 5</a>
                        </li>
                        <li role="presentation"><a href="#p6" class="cargador-planilla" aria-controls="p6" role="tab" data-toggle="tab">Planilla 6</a>
                        </li>
                         @if(isset($doc_adicional))
                        	<li role="presentation"><a href="#p7" class="cargador-planilla" aria-controls="p7" role="tab" data-toggle="tab">Doc Adicionales</a>
                        	</li>
                        @endif
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="p2">p2</div>
                        <div role="tabpanel" class="tab-pane" id="p3">p3</div>
                        <div role="tabpanel" class="tab-pane" id="p4">p4</div>
                        <div role="tabpanel" class="tab-pane" id="p5">p5</div>
                        <div role="tabpanel" class="tab-pane" id="p6">p6</div>
                        <div role="tabpanel" class="tab-pane" id="p7">p7</div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
				<div class="row invisible">
					<div class="col-md-12">
						<div class="form-group text-left">
							<label for="observacion">Observaci&oacute;n</label>
							<textarea name="observacion" id="observacion" class="form-control"></textarea></div>
						</div>
					</div>
				<!--div class="row" style="margin-top:10px;">
					<div class="col-md-8"></div>
					<div class="col-md-2"><button type="button" class="btn btn-warning" id="botonObservacion" onclick="mostrarObservacion()">Agregar Observaci&oacute;n</button></div>
					<div class="col-md-2"><button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar ventana</button></div>
				</div-->
            </div>
        </div>
    </div>
</div>
<!-- fin Modal-->

<!-- Modal arancel //////////////////////////////////////////////////////////////-->
<div class="modal fade" id="arancel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
            	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                 <h3 class="modal-title" id="myModalLabel"></h3>
            </div>
            <div class="modal-body"></div>
            <div class="modal-footer"></div>
        </div>
    </div>
</div>	
<!-- Fin Modal Arancel //////////////////////////////////////////////////////////-->




<!--Modal de Documentos del Usuario-->

<div id="myModal" class="modal fade" role="dialog">
	<div class="modal-dialog modal-lg">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h3 class="modal-title"><b style="color:#337AB7">Soporte de Usuario</b></h3>
			</div>
			<div class="modal-body">
				<div class="modal-body">
					<div role="tabpanel">
						<!-- Nav tabs -->
						<ul class="nav nav-tabs" role="tablist">
							<li role="presentation" class="active"><a href="#p1cedula" class="cargador-planilla" aria-controls="p1cedula" role="tab" data-toggle="tab" id="tab-inicial">Cédula</a>
							</li>
							<li role="presentation"><a href="#p2Rif" class="cargador-planilla" aria-controls="p2Rif" role="tab" data-toggle="tab">Rif</a>
							</li>
							<li role="presentation"><a href="#p3RegMer" class="cargador-planilla" aria-controls="p3RegMer" role="tab" data-toggle="tab">Registro Mercantil</a>
							</li>
						</ul>
						<!-- Tab panes -->
					</div>
					<div class="tab-content">
						<div role="tabpanel" class="tab-pane active text-center" id="p1cedula">
						@if(file_exists(substr($rutacedula, 1)))
								@if(isset($extencion_file_1) && $extencion_file_1 == 'pdf')
							        <span class="text-success"></span>
							        <br>                                <embed id="fred" src="{{asset($rutacedula)}}" width="800" height="600" type="application/pdf">

							        @elseif(isset($extencion_file_1) && ($extencion_file_1 == 'docx' || $extencion_file_1 == 'doc'))
							          <br><br>
							          <a href="{{asset($rutacedula)}}" class="btn btn-lg btn-primary"><span><i class="glyphicon glyphicon-cloud-download"></i> </span>Descargar documento 1</a>
							          <br><br>
							        @else.
							    @endif
								@else
								<br>
								<div class="alert alert-warning" style="color:#fff"><strong>Atención!</strong> No es posible ubicar este fichero, por favor comunicarse con el administrador de sistemas.</div>
								@endif
							</div>
							<div role="tabpanel" class="tab-pane text-center" id="p2Rif">
							@if(file_exists(substr($rutarif, 1)))
								@if(isset($extencion_file_1) && $extencion_file_1 == 'pdf')
							        <span class="text-success"></span>
							        <br>                                <embed id="fred" src="{{asset($rutarif)}}" width="800" height="600" type="application/pdf">

							        @elseif(isset($extencion_file_1) && ($extencion_file_1 == 'docx' || $extencion_file_1 == 'doc'))
							          <br><br>
							          <a href="{{asset($rutarif)}}" class="btn btn-lg btn-primary"><span><i class="glyphicon glyphicon-cloud-download"></i> </span>Descargar documento 1</a>
							          <br><br>
							        @else.
							    @endif
								@else
								<br>
								<div class="alert alert-warning" style="color:#fff"><strong>Atención!</strong> No es posible ubicar este fichero, por favor comunicarse con el administrador de sistemas.</div>
								@endif
								</div>
								<div role="tabpanel" class="tab-pane text-center" id="p3RegMer">
								@if(file_exists(substr($rutaregistromer, 1)))
								@if(isset($extencion_file_1) && $extencion_file_1 == 'pdf')
							        <span class="text-success"></span>
							        <br>                                <embed id="fred" src="{{asset($rutaregistromer)}}" width="800" height="600" type="application/pdf">

							        @elseif(isset($extencion_file_1) && ($extencion_file_1 == 'docx' || $extencion_file_1 == 'doc'))
							          <br><br>
							          <a href="{{asset($rutaregistromer)}}" class="btn btn-lg btn-primary"><span><i class="glyphicon glyphicon-cloud-download"></i> </span>Descargar documento 1</a>
							          <br><br>
							        @else.
							   @endif
							   @else
							   <br>
							   <div class="alert alert-warning" style="color:#fff"><strong>Atención!</strong> No es posible ubicar este fichero, por favor comunicarse con el administrador de sistemas.</div>
							   @endif
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
					</div>
				</div>
			</div>
			<!--Fin modal de documentos de usuarios-->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script type="text/javascript">
var ocultarObservacion=null;
var mostrarObservacion = null;
var cargarObservacion= null;
var guardarObservacion= null;
$(document).ready(function()
{

    /** Escuchando evento click en los elementos que contengan la clase btn-view-planillas 
     *  donde se creara una variable local que contendra el id de det declaracion produc
     *  adicionalmente se induce el evento click en la pestaña inicial de los tabs**/
    $(document).on('click','.btn-view-planillas',function()
    {
        var det_declaracion_id = this.getAttribute('data-declaracion-product');
        localStorage.setItem("det_declaracion_id","");

        var gen_declaracion_jo_id=$('#gen_declaracion_jo_id').val();///
        
        if(!det_declaracion_id==false)
        {
            localStorage.setItem("det_declaracion_id",det_declaracion_id);
        }

        document.getElementById('tab-inicial').click();
    });

	/** Escuchando evento click en los elementos que contengan la clase cargador-planilla
	 *  donde se obtienen el nombre del ancla (id_href) del enlace y el nombre la planilla a mostrar
	 *  consulta via ajax de la visual de la planilla correspondiente y la carga de los datos recibidos en
	 * el contenedor especifico **/
    $(document).on('click','.cargador-planilla',function()
    {
        var id_href = this.getAttribute('href');
        var nombre = this.getAttribute('aria-controls');
        var det_declaracion_id= localStorage.getItem("det_declaracion_id");
        var gen_declaracion_jo_id=$('#gen_declaracion_jo_id').val();///
		localStorage.setItem("nombre_tag","");
		ocultarObservacion();

        if(!id_href==false && !nombre==false)
        {
            var base= document.location.origin;
            var url= '/calificaciondjo/';
			localStorage.setItem("nombre_tag",nombre);
            if(nombre=="p2"){///
                url+='DJOPlanilla2/show';
            }else if(nombre=="p3"){
                url+='DJOPlanilla3/show';
            }else if(nombre=="p4"){
                url+='DJOPlanilla4/show';
            }else if(nombre=="p5"){
                url+='DJOPlanilla5/show';
            }else if(nombre=="p6"){
                url+='DJOPlanilla6/show';
            }else if(nombre=="p7"){
                url+='DocAdicional/show';
            }else{
                url+='unknow';
            }

            var datos_enviados = {
                'det_declaracion_id' : det_declaracion_id,
                'layout' : false
                //'gen_declaracion_jo_id' : gen_declaracion_jo_id///
            }

            var request = $.ajax({
            url: base + url ,
            method: "GET",
            data: datos_enviados,
            dataType: "html"
            });


            request.done(function( data ) {
				$(id_href).html(data);
				cargarObservacion();
            });

            request.fail(function( jqXHR, textStatus ) {
            //alert( "Hubo un error: " + textStatus );
            });
        }

    }); /*fin cargador planilla*/

	//////////////////////////////////////////////////////////////////////////////////////
	//  CODIGO ARANCELARIO
    //////////////////////////////////////////////////////////////////////////////////////
    // Escuchando evento click para los botones de carga del arancel
    $(".cargar_arancel").on('click', function()
    { 
    	// Variables
	    var	idBoton = this.getAttribute('id'); //  boton arancel (boton_mer_0 o boton_nan_0)
	    var	indice = this.getAttribute('data-indice'); // numero de la fila	(0)	
	    var tipoArancel = this.getAttribute('data-tipo-arancel'); // tipo de boton arancel (nan o mer)
	    //
	    
	    // Variables de Validacion  
        var tituloArancelModal = (tipoArancel == "mer" ? "Arancel Mercosur" : "Arancel Nandina");
        var urlbase= document.location.origin; // url dominio
		var url= '/calificaciondjo/'; // url view arancel

		// Validamos la url de la busqueda para mercosur o nandina
		if(tipoArancel=='mer'){

			url+="ajax/arancelMercosur/index";

		}else{

			url+="ajax/arancelNandina/index";
		}
        // peticion ajax para traer la data
        var listaArancel = $.ajax({
            url: urlbase + url ,
            method: "GET",
            data: {'tipoArancel':tipoArancel},
            dataType: "html"
            });
        // fin ajax

        // Pintamos el Titulo del Modal
        	$('#arancel .modal-header #myModalLabel').html(tituloArancelModal);
	  	// fin Titulo
	  		
	  		// Si Obtenemos Respuesta
	  		listaArancel.done(function( data ) 
	  		{
					$('#arancel .modal-body').html(data);
					if(tipoArancel=='mer'){
								
						// Inicializando la tabla de codigos arancelarios Nandina				
						$('#listaMercosur').DataTable({
							"serverside":true,
							"ajax":"{{ url('api/arancelMercosur') }}",
							"columns":[
								
								{data:'id'},
								{data:'codigo'},
								{data:'descripcion'},
							]

						});
						//
							// Escuchando el evento click sobre la tabla de codigo arancelario 
							$('#listaMercosur tbody').on( 'click', 'tr', function () {
							    
							    var codigoObtenido=this.cells[1].innerHTML;

							    capturar(indice,tipoArancel,idBoton,codigoObtenido);
		    	
						    	delete indice;
						    	delete tipoArancel;
						    	delete idBoton;	

							} );
							//

					}else{

						// Inicializando la Tabla de codigos Arancelarios Nandina
						$('#listaNandina').DataTable({
							"serverside":true,
							"ajax":"{{ url('api/arancelNandina') }}",
							"asStripeClasses": [ 'codigo_nan_modal strip2'],
							"columns":[
								
								{data:'id'},
								{data:'codigo'},
								{data:'descripcion'},
							]

						});
						//
							// Escuchando el evento click sobre la tabla de codigo arancelario 
							$('#listaNandina tbody').on( 'click', 'tr', function () {
								var codigoObtenido=this.cells[1].innerHTML;
							    //alert( 'Clicked on: '+this.innerHTML );
							    //alert( 'Clicked on: '+this.cells[1].innerHTML);
							    capturar(indice,tipoArancel,idBoton,codigoObtenido);
		    	
						    	delete indice;
						    	delete tipoArancel;
						    	delete idBoton;							    
							} );
							//
					}		
	        });
	  		// Si no obtenemos una respuesta a la peticion de los aranceles
	        listaArancel.fail(function( jqXHR, textStatus ) 
	        {		
	        		data="<p class='text-danger'>Error procesando su peticion</p>"+ textStatus;
	            	 $('#arancel .modal-body').html(data);
	        });
	        //
	        
	     // Funcion para pintar el codigo en la celda correspondiente a la planilla
	    function capturar(indice,tipoArancel,idBoton,codigo="")
	    {
		
	    	// validamos donde colocar el codigo capturado	    	
	    	if(tipoArancel=="mer"){

	    		$('#'+idBoton+ ' i.glyphicon').addClass('glyphicon glyphicon-pencil');
	    		$('#cod_mer_'+indice).val(codigo);
	    		$('#cod_mer_'+indice).attr('readonly', 'true');
	    		
	    	}else{

	    		$('#'+idBoton+ ' i.glyphicon').addClass('glyphicon glyphicon-pencil');
	    		$('#cod_nan_'+indice).val(codigo);
	    		$('#cod_nan_'+indice).attr('readonly', 'true');
	    	}
	    	  // cerramos la ventana modal
	    	  $("#arancel .close").click();
		}

    });
    //////////////////////////////////////////////////////////////////////////////////////
	//  FIN CODIGO ARANCELARIO
    //////////////////////////////////////////////////////////////////////////////////////		

/** Cargar observacion correspondiente a una planilla **/
cargarObservacion=  function(){

	var elemento = $('#observacion');
	var det_declaracion_id= localStorage.getItem("det_declaracion_id");
	var nombre = localStorage.getItem("nombre_tag");
	var gen_declaracion_jo_id=$('#gen_declaracion_jo_id').val();//

	if(elemento.length && !nombre==false){

		var base= document.location.origin;
            var url= '/calificaciondjo/';
         
            if(nombre=="p2"){
                url+='DJOPlanilla2/observacion';
            }else if(nombre=="p3"){
                url+='DJOPlanilla3/observacion';
            }else if(nombre=="p4"){
                url+='DJOPlanilla4/observacion';
            }else if(nombre=="p5"){
                url+='DJOPlanilla5/observacion';
            }else if(nombre=="p6"){
                url+='DJOPlanilla6/observacion';
            }else if(nombre=="p7"){
                url+='DocAdicional/observacion';
            }else{
                url+='unknow';
            }

		var datos_enviados = {
					'det_declaracion_id' : det_declaracion_id,
					'gen_declaracion_jo_id': gen_declaracion_jo_id,///
				}

				var request = $.ajax({
				url: base + url ,
				method: "GET",
				data: datos_enviados,
				dataType: "json"
				});


				request.done(function( data ) {
					elemento[0].value=data.observacion;
					if(!data.observacion==false){
						mostrarObservacion();
					}
				});

				request.fail(function( jqXHR, textStatus ) {
				alert( "Hubo un error: " + textStatus );
				});
	}

}

/** Guardar observacion correspondiente a la planilla que esta siendo visualizada **/
guardarObservacion= function(){

	var elemento = $('#observacion');
	var det_declaracion_id= localStorage.getItem("det_declaracion_id");
	var nombre = localStorage.getItem("nombre_tag");
	var gen_declaracion_jo_id=$('#gen_declaracion_jo_id').val();//

	if(elemento.length && !nombre==false){

		var base= document.location.origin;
            var url= '/calificaciondjo/';
         
            if(nombre=="p2"){
                url+='DJOPlanilla2/observacion';
            }else if(nombre=="p3"){
                url+='DJOPlanilla3/observacion';
            }else if(nombre=="p4"){
                url+='DJOPlanilla4/observacion';
            }else if(nombre=="p5"){
                url+='DJOPlanilla5/observacion';
            }else if(nombre=="p6"){
                url+='DJOPlanilla6/observacion';
            }else if(nombre=="p7"){
                url+='DocAdicional/observacion';
            }else{
                url+='unknow';
            }

		var datos_enviados = {
					"_token": "{{ csrf_token() }}",
					'det_declaracion_id' : det_declaracion_id,
					'observacion': elemento[0].value,
					'gen_declaracion_jo_id': gen_declaracion_jo_id,
				}

				var request = $.ajax({
				url: base + url ,
				method: "POST",
				data: datos_enviados,
				dataType: "json"
				});


				request.done(function( data ) {
					swal("Observación Agregada", "La observación se ha agregado de forma satisfactoria.", "success");
				});

				request.fail(function( jqXHR, textStatus ) {
				alert( "Hubo un error: " + textStatus );
				});
	}

}

/** Mostrar el campo observacion **/
mostrarObservacion = function(){
		var elemento = $('#observacion');
		var boton= $('#botonObservacion');

		if(elemento.length && boton.length){
			
			elemento[0].parentNode.parentNode.parentNode.classList.remove("invisible");

			boton[0].innerHTML="Guardar Observaci&oacute;n";
			boton[0].classList.remove("btn-warning");
			boton[0].classList.add("btn-info");
			boton[0].onclick=guardarObservacion;
		}
	}

/** Ocultar el campo observacion **/
ocultarObservacion = function(){
		var elemento = $('#observacion');
		var boton= $('#botonObservacion');

		if(elemento.length && boton.length){
			
			elemento[0].value="";
			elemento[0].parentNode.parentNode.parentNode.classList.add("invisible");

			boton[0].innerHTML="Agregar Observaci&oacute;n";
			boton[0].classList.remove("btn-info");
			boton[0].classList.add("btn-warning");
			boton[0].onclick=mostrarObservacion;
		}
	}
});



</script>
@stop