@extends('templates/layoutlte_analista_inversionista')

@section('content')

<div class="content" style="background-color: #fff">
    <div class="panel-group" style="background-color: #fff;">
      <div class="panel-primary">
        <div class="panel-heading"><h3>Solicitudes de Potencial Inversionista Extranjero</h3> </div>
        <div class="panel-body">
            <div class="row">
              
              <div class="col-md-12">
                <div class="row">
                  <div class="col-md-8">

                           <h4 class="text-info">Bandeja de Solicitudes de Inversión Extranjero:</h4><hr>
                          
                  </div>
                  <div class="col-md-2">
                   
                  </div>
                  <div class="col-md-2">
                    <a class="btn btn-primary" href="{{route('AnalistInverExtjero.index')}}">
                      <span class="glyphicon glyphicon-refresh" title="Agregar" aria-hidden="true"></span>
                        Actualizar
                    </a>

                  </div>
                </div>
                <table id="listaAnalista" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                      <tr class="text-center">
                        <th class="col-md-1 text-center">N°</th>
                        <th class="col-md-3 text-center">N° Solicitud</th>
                        <th class="col-md-3 text-center">N° Pasaporte</th>
                        <th class="col-md-2 text-center">País de Origen</th>
                        <th class="col-md-2 text-center">Monto de Inversión</th>
                        <th class="col-md-2 text-center">Fecha de Solicitud</th>
                        <th class="col-md-2 text-center">Estatus</th>
                        <th class="col-md-1 text-center">Acciones</th>
                      </tr>
                    </thead>
                    <tbody>
                  @foreach($inv_extranjero as $key=> $extranjero)
                    <tr>
                      <td class="col-md-1 text-center text-warning">{{$key+1}}</td>
                      <td class="col-md-2 text-center text-primary"><b>{{$extranjero->num_sol_inversionista}}</b></td>
                      <td class="col-md-2 text-center">{{$extranjero->DetUsuario->rif}}</td>
                      <td class="col-md-2 text-center">{{$extranjero->pais->dpais}}</td>
                      <td class="col-md-2 text-center">
                      @if($extranjero->monto_inversion >= 10000 && $extranjero->monto_inversion < 40000)
                        <b>Baja</b>
                      @elseif($extranjero->monto_inversion >= 40000 && $extranjero->monto_inversion < 80000)
                        <b>Media</b>
                      @elseif($extranjero->monto_inversion >= 80000)
                        <b>Alta</b>
                      </td>
                      @endif


                      <td class="col-md-2 text-center">{{$extranjero->created_at}}</td>
                      <td class="col-md-2 text-center">{{$extranjero->estatus->nombre_status}}</td>
                      
                      <td>
                           <!--Atender Solicitud-->
                            @if($extranjero->gen_status_id==9 )
                              <a href="{{ route('AnalistInverExtjero.edit',$extranjero->id)}}" class="btn btn-sm btn-danger" id="atender"><i class="glyphicon glyphicon-user"></i><b> Atender</b></a>
                            @endif
                            @if($extranjero->gen_status_id==10 )
                              <a href="{{ route('AnalistInverExtjero.edit',$extranjero->id)}}" class="btn btn-sm btn-warning" id="atender"><i class="glyphicon glyphicon-user"></i><b> Continuar</b></a>
                            @endif
                            @if($extranjero->gen_status_id==16 )
                              <a href="{{ route('AnalistInverExtjero.edit',$extranjero->id)}}" class="btn btn-sm btn-success" id="atender"><i class="glyphicon glyphicon-user"></i><b> Revisar</b></a>
                            @endif
                           <!--Fin Atender Solicitud-->

                           <!--Corregida por el Usuario-->
                            @if($extranjero->gen_status_id==18)
                              <a href="{{ route('AnalistInverExtjero.edit',$extranjero->id)}}" class="btn btn-sm btn-primary" id="atender"><i class="glyphicon glyphicon-user"></i><b>Reatender</b></a>
                            @endif
                            <!--Fin-->

                            <!--Documentos Incompletos-->
                            @if($extranjero->gen_status_id==15)@endif
                            <!--Fin-->

                            <!--Anulada-->
                            @if($extranjero->gen_status_id==13 && $extranjero->estado_observacion == 1 && !empty($extranjero->observacion_anulacion))@endif
                             <!--Fin-->



                            @if($extranjero->gen_status_id==12)@endif


                            @if($extranjero->gen_status_id==19)@endif

                                



                      </td>
                    </tr>
                  @endforeach
                    </tbody>   
                </table>
              </div>
            </div><!-- Cierre 1° Row-->
        </div>
      </div>
    </div>
</div><!-- fin content-->
@stop