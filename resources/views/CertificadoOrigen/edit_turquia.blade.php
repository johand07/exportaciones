@extends('templates/layoutlte')
@section('content')

@php

$observacion = isset($certificado->observacion_analista)? $certificado->observacion_analista : $certificado->observacion_coordinador;

function namefile($name){
  $explode=explode("/",$name);
  return $explode[3];  
}

@endphp
 @if( $certificado->gen_status_id == 11 && (!empty($certificado->observacion_analista ) || !empty($certificado->observacion_coordinador ) ) )
@php $contador_prod=count($det_prod_cer); @endphp
{{Form::hidden('contador',$contador_prod)}}



{{ Form::model($certificado, array('route' => array('CertificadoOrigen.update', $certificado->id), 'method' => 'PUT', 'id'=>'FormProd', 'enctype' => 'multipart/form-data')) }} 


    <script>

      document.onreadystatechange = function () {
        var state = document.readyState;
        if (state == 'complete') {
          swal("Estimado usuario debera corregír las siguientes observaciones", "{{$observacion}}", "warning");
        }
      }
    </script>
    <br>

    <div id="space_alert1" class="alert alert-danger" role="alert" style="display: block;">
        <strong><span class="glyphicon glyphicon-circle-arrow-right"></span> Observación Indicada por el Analista: {{$observacion}}</strong>
      </div>

    @endif
<input type="hidden" name="gen_tipo_certificado_id" id="gen_tipo_certificado_id" value="{{$certificado->gen_tipo_certificado_id}}">

<div class="content">
    <div class="panel-group">
        <div class="panel-primary" style="background-color: #fff">
            <div class="panel-heading"><h3>Carga de Documentos</h3></div>
            <div class="panel-body">
                <h5 style="color: red">(Formatos permitidos .doc, .docx, .ppt, .pptx, .pdf)</h5>
                <div class="row"><br>
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <tr>
                                <td class="text-center" valign="top" > 
                                    <span><b><p style="font-size:14px;">1.) Cargar el formato de Solicitud de Certificado de Origen</p></b></span>
                                    
                                    <span class="btn btn-default btn-file" style="margin: 3px;">
                                        Cargar Archivo <span class="glyphicon glyphicon-file"></span>
                                        {!! Form::file('file_1', array('class' => 'file-input', 'style'=>'margin: 10px;','id'=>'file_1','title'=>'formatos permitidos .doc, .docx, .ppt, .pptx, .pdf',)) !!}
                                    </span>
                                <input type="hidden" value="{{$doc_cert->id}}" name="file_id"> 
                                </td>
                                <td class="text-center" height="40px"><img id="vista_previa_file_1" src="" alt="" width="300px" style="margin: 3px;"><img id="imgdocumento_file_1" src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px; display: none;">
                                
                                <div id="nombre_file_1"></div><!--Craga la 1era vez el nombre del Documento-->

                                     @if(isset($doc_cert->file_1))
                                     <div id="imgdocumentoEdit_file_1">
                                      <img src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px;">
                                       {{namefile($doc_cert->file_1)}}<!--Carga Nombre del documento en el edit con namefile que esta arriba dentro de php en una funcion-->
                                       </div>
                                    @endif

                                </td>
                            </tr>
                        </div>
                        <div class="col-md-6">
                            <tr>
                                <td class="text-center" valign="top" > 
                                    <span><b><p style="font-size:14px;">4.) Pago por concepto de solicitud de certificado de origen (0,25 petros)</p></b></span>
                                     
                                    <span class="btn btn-default btn-file" style="margin: 3px;">
                                        Cargar Archivo <span class="glyphicon glyphicon-file"></span>
                                        {!! Form::file('file_4', array('class' => 'file-input','style'=>'margin: 10px;','id'=>'file_4','title'=>'formatos permitidos .doc, .docx, .ppt, .pptx, .pdf')) !!}
                                    </span>
                                </td>
                                <td class="text-center" height="40px"><img id="" src="" alt="" width="300px" style="margin: 3px;"><img id="imgdocumento_file_4" src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px; display: none;"><div id="nombre_file_4"></div>
                                        @if(isset($doc_cert->file_4))
                                             <div id="imgdocumentoEdit_file_4">
                                                    <img id="imgdocumentoEdit_file_4" src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px;">
                                                    {{namefile($doc_cert->file_4)}}
                                            </div>
                                        @endif
                                </td>
                            </tr>
                        </div>
                    </div>
                    

                    <div class="col-md-12"><br><br><br>
                        <div class="col-md-6">
                            <tr>
                                <td class="text-center" valign="top" > 
                                    <span><b><p style="font-size:14px;">2.) Declaración jurada de Origen</p></b></span>
                                    
                                    <span class="btn btn-default btn-file" style="margin: 3px;">
                                        Cargar Archivo <span class="glyphicon glyphicon-file"></span>
                                        {!! Form::file('file_2', array('class' => 'file-input','style'=>'margin: 10px;','id'=>'file_2','title'=>'formatos permitidos .doc, .docx,.ppt, .pptx, .pdf')) !!}
                                    </span>
                                </td>
                                <td class="text-center" height="40px"><img id="" src="" alt="" width="300px" style="margin: 3px;"><img id="imgdocumento_file_2" src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px; display: none;"><div id="nombre_file_2"></div>
                                    @if(isset($doc_cert->file_2))
                                        <div id="imgdocumentoEdit_file_2">
                                            <img id="imgdocumentoEdit_file_2" src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px;">
                                            {{namefile($doc_cert->file_2)}}
                                        </div>
                                    @endif
                                </td>
                            </tr>
                        </div>
                        <div class="col-md-6">
                            <tr>
                                <td class="text-center" valign="top" > 
                                    <span><b><p style="font-size:14px;">5.) Pago de 0,02 U.T Gobierno del Distrito Capital</p></b></span>
                                   
                                    <span class="btn btn-default btn-file" style="margin: 3px;">
                                        Cargar Archivo <span class="glyphicon glyphicon-file"></span>
                                        {!! Form::file('file_5', array('class' => 'file-input','style'=>'margin: 10px;','id'=>'file_5','title'=>'formatos permitidos .doc, .docx, .ppt, .pptx, .pdf')) !!}
                                    </span>
                                </td>
                                <td class="text-center" height="40px"><img id="" src="" alt="" width="300px" style="margin: 3px;"><img id="imgdocumento_file_5" src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px; display: none;"><div id="nombre_file_5"></div>
                                    @if(isset($doc_cert->file_5))
                                        <div id="imgdocumentoEdit_file_5">
                                            <img src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px;">
                                            {{namefile($doc_cert->file_5)}}
                                        </div>
                                    @endif
                                </td>
                            </tr>
                        </div>
                    </div>
                    <div class="col-md-12"><br><br><br>
                        <div class="col-md-6">
                            <tr>
                                <td class="text-center" valign="top" > 
                                    <span><b><p>3.) Factura comercial de exportador</p></b>
                                    <span class="btn btn-default btn-file" style="margin: 3px;">
                                        Cargar Archivo <span class="glyphicon glyphicon-file"></span>
                                        {!! Form::file('file_3', array('class' => 'file-input','style'=>'margin: 10px;','id'=>'file_3','title'=>'formatos permitidos .doc, .docx,.ppt, .pptx, .pdf')) !!}
                                    </span>
                                </td>
                                <td class="text-center" height="40px"><img id="" src="" alt="" width="300px" style="margin: 3px;"><img id="imgdocumento_file_3" src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px; display: none;"><div id="nombre_file_3"></div>
                                    @if(isset($doc_cert->file_3))
                                            <div id="imgdocumentoEdit_file_3">
                                                <img id="imgdocumentoEdit_file_3" src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px;">
                                                 {{namefile($doc_cert->file_3)}}<!--Carga Nombre del documento en el edit con namefile que esta arriba dentro de php en una funcion-->
                                            </div>
                                    @endif
                                </td>
                            </tr>
                        </div>
                        <div class="col-md-6">
                            <tr>
                                <td class="text-center" valign="top" > 
                                    <span><b><p style="font-size:14px;">6.) Factura de productor (Si es <b>Comercializadora</b>)</p></b></span>
                                     <!--strong style="color: red"> *</strong-->
                                    <span class="btn btn-default btn-file" style="margin: 3px;">
                                        Cargar Archivo <span class="glyphicon glyphicon-file"></span>
                                        {!! Form::file('file_6', array('class' => 'file-input','style'=>'margin: 10px;','id'=>'file_6','title'=>'formatos permitidos .doc, .docx, .ppt, .pptx, .pdf', "noreq"=>"noreq")) !!}
                                    </span>
                                </td>
                                <td class="text-center" height="40px"><img id="" src="" alt="" width="300px" style="margin: 3px;"><img id="imgdocumento_file_6" src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px; display: none;">
                                <div id="nombre_file_6"></div>
                                    @if(isset($doc_cert->file_6))
                                    <div id="imgdocumentoEdit_file_6">
                                        <img src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px;">
                                             {{namefile($doc_cert->file_6)}}<!--Carga Nombre del documento en el edit con namefile que esta arriba dentro de php en una funcion-->
                                    @endif
                                </td>
                            </tr>
                        </div>
                    </div>
                </div>
            </div>
        <br><br><br>
        <!-----------------Item Certificado de Origen Colombia------------ -->
        <div class="panel-primary" style="background-color: #fff">
            <div class="panel-heading">
            <h3>Certificado de origen Turquia</h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <h4>&nbsp;&nbsp;&nbsp;<b>Exporter</b></h4>
                    <div class="col-md-4">
                        <div class="form-group">
                            {!! Form::label('','Name') !!} <p class="text-muted">(Nombre)</p>
                            {!! Form::text('razon_social',$det_usuario->razon_social,['class'=>'form-control','id'=>'razon_social', 'readonly'=>'readonly']) !!}
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            {!! Form::label('','Full address and country') !!}
                            <p class="text-muted">(Dirección completa y ciudad)</p>
                            {!! Form::text('direccion',$det_usuario->direccion,['class'=>'form-control','id'=>'direccion', 'readonly'=>'readonly']) !!}
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            {!! Form::label('','RIF') !!}<p class="text-muted">(Registro Único de Información Fiscal)</p>
                            {!! Form::text('rif',$det_usuario->rif,['class'=>'form-control','id'=>'rif', 'readonly'=>'readonly']) !!}
                        </div>
                    </div>
                </div><br>

                <div class="row"><br>
                    <center><h4><b>Certificate used in preferential trade between</b></h4></center>
                    <center><p class="text-muted">Certificado utilizado en el comercio preferencial entre</p></center>
                    <div class="col-md-5">
                        <!--Incrustar datos directamente en el código-->
                        <div class="form-group">
                            {!! Form::text('pais_exportador','VENEZUELA',['class'=>'form-control','readonly'=>'readonly','id'=>'pais_exportador']) !!}
                        </div>
                    </div>
                    <div class="col-md-2">
                            <center><h5><b>and</b></h5></center>
                        </div>
                    <div class="col-md-5">
                        <div class="form-group">
                            
                            {!! Form::text('pais_importador','TURKIYE',['class'=>'form-control','readonly'=>'readonly','id'=>'pais_importador']) !!}
                        </div>
                    </div>
                </div><br>

                <div class="row">
                        <h4>&nbsp;&nbsp;&nbsp;<b>Consignee</b></h4>
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('','Name') !!}
                            <p class="text-muted">Nombre</p>
                            {!! Form::text('razon_social_importador',$import_cert->razon_social_importador,['class'=>'form-control','id'=>'razon_social_importador']) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('','Full address and country') !!}
                            <p class="text-muted">(Dirección completa y ciudad)</p>
                            {!! Form::text('direccion_importador',$import_cert->direccion_importador,['class'=>'form-control','id'=>'direccion_importador']) !!}
                        </div>
                    </div>
                </div><br>

                <div class="row">       
                    <div class="col-md-4">
                        <div class="form-group">
                            {!! Form::label('','Country, group of countries or territory in which the products are considered as originating') !!}
                            <p class="text-muted">País o territorio en el que los productos se consideran originarios</p>
                            {!! Form::text('pais_cuidad_exportador','VENEZUELA',['class'=>'form-control','readonly'=>'readonly','id'=>'pais_cuidad_exportador']) !!}
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            {!! Form::label('','Country, group of countries or territory of destination')!!}
                                <p class="text-muted">País o territorio de destino</p><br><br>
                            {!! Form::text('pais_ciudad_importador','TURKIYE',['class'=>'form-control','readonly'=>'readonly','id'=>'pais_ciudad_importador']) !!}
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            {!! Form::label('','Transport details (Optional)') !!}
                            <p class="text-muted">Detalles de transporte (Opcional)</p><br>
                            <br><br>{!! Form::text('medio_transporte_importador',$import_cert->medio_transporte_importador,['class'=>'form-control','id'=>'medio_transporte_importador', "noreq"=>"noreq"]) !!}
                        </div>
                    </div>
                </div><br>

                <!--Primera tabla para prodcutos completos en el caso turquia -->
            
                <div id="productos_planilla">
                <div class="row"><br><!--Agregar productos-->
                    
                    <table class="table table-bordered text-left" id="productos">
                        <tr> <td><button  name="add" type="button" id="add-productos" value="add more" class="btn btn-success">Add products</button></td></tr>

                        <tr>
                
                            <th>Marks and numbers; Number and kind of packages<br>
                            <br>
                            <p class="text-muted">Número y tipo de paquetes.</p></th>

                            <th>Description of goods<br><br><br><p class="text-muted">Descripción de los bienes</p></th>

                            <th>Gross mass (kg) or orther measure (litres, m3, etc.)<br><br>
                                <p class="text-muted">Masa bruta (kg) u otra medida (litros, m3, etc.)</p>
                            </th>
                        
                        </tr>
                        @foreach($det_prod_cer as $key => $prod_certifi)
                            <tr id="row{{$key+1}}"><!--row1 es para la lista dinamica de agregar productos nsantos-->
                                                    <!--Lista dinamica Prodcutos-->
                            <td>{!!Form::text('codigo_arancel[]',$prod_certifi->codigo_arancel,['class'=>'form-control','id'=>'codigo_arancel'])!!}
                            </td>

                            <td>{!!Form::text('denominacion_mercancia[]',$prod_certifi->denominacion_mercancia,['class'=>'form-control','id'=>'denominacion_mercancia'])!!}
                            </td>

                            <td>{!!Form::text('unidad_fisica[]',$prod_certifi->unidad_fisica,['class'=>'form-control','id'=>'unidad_fisica'])!!}
                            </td>
                                <td>{!!Form::hidden('id_productos[]',$prod_certifi->id)!!}<button  name="remove" type="button" id="{{$key+1}}" value="" class="btn btn-danger btn-remove">x</button></td>
                                
                        </tr>
                    @endforeach
                    </table>
                </div>
                </div>
            
                <div class="row"><br>
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('','Invoice') !!}
                            <p class="text-muted">Factura</p>
                            {!! Form::text('numero_factura',$det_decla_cert[0]->numero_factura,['class'=>'form-control','id'=>'numero_factura']) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('','Date') !!}
                            <p class="text-muted">Fecha</p>
                            <div class="input-group date">
                                {!! Form::text('f_fac_certificado',$det_decla_cert[0]->f_fac_certificado,['class'=>'form-control','readonly'=>'readonly','id'=>'f_fac_certificado']) !!}
                                <span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
                            </div>
                        </div>
                    </div>
                </div><br>
                <!--<div class="row">       
                    <h4>&nbsp;&nbsp;&nbsp;<b>Competent authority endorsement</b></h4>
                    <div class="col-md-4">
                        <div class="form-group">
                            {!! Form::label('','From') !!}<br>
                            <b>{!! Form::label('','VENEZUELA') !!}</b>

                        
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            {!! Form::label('','N° of competent authority')!!}
                            {!! Form::text('num_certificado',null,['class'=>'form-control','id'=>'num_certificado']) !!}
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            {!! Form::label('','Place and date') !!}
                            <div class="input-group date">
                                {!! Form::text('fecha_emision_exportador',null,['class'=>'form-control','readonly'=>'readonly','id'=>'fecha_emision_exportador']) !!}
                                <span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
                            </div>
                        </div>
                    </div>
                </div><br><br>-->

                <!--<div class="row"><br>
                    <div class="col-md-6">
                        <div class="form-group">
                                <h4>&nbsp;<b>Declaration By The Exporter</b></h4>
                            {!! Form::label('','Place and date') !!}

                            <div class="input-group date">
                                {!! Form::text('created_at',null,['class'=>'form-control','readonly'=>'readonly','id'=>'created_at']) !!}
                                <span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <h4>&nbsp;<b>Request for verification, to'</b></h4>
                            <h5>{!! Form::label('','Servicio Nacional Integrado de Administración Aduanera y Tributaria (SENIAT)') !!}</h5>
                            
                        </div>
                    </div>
                </div><br>-->
                
                <div id="observaciones"><br>
                    <div class="row">
                        <div  class="col-md-12">
                            <div class="form-group">
                                &nbsp;&nbsp;&nbsp;{!! Form::label('','Remarks') !!}
                                &nbsp;&nbsp;&nbsp;<p class="text-muted">Observaciones</p>
                                {!! Form::textarea('observaciones',null,['class'=>'form-control','id'=>'observaciones', "noreq"=>"noreq"]) !!}
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row text-center">
                    <a href="{{ route('CertificadoOrigen.create')}}" class="btn btn-primary">Cancel</a>
                    <input type="submit" class="btn btn-success" value="Corregir" name="submit" id="submit" onclick="validacionForm()"> 
                </div>

            </div>
        </div>
        

    </div><!--div cierre panel-group general-->
</div><!--div cierre content general-->


{{Form::close()}}

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="{{asset('js/ajax_v2.1.1.js')}}"></script>
<script src="{{asset('js/CertificadoOrigenJS/COEditTurquia.js')}}"></script>


@stop







