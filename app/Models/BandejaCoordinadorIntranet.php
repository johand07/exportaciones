<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BandejaCoordinadorIntranet extends Model
{
    protected $table = 'bandeja_coordinador_intranet';
    
    protected $fillable = [
        'gen_solicitud_id',
        'gen_usuario_id',
        'gen_status_id',
        'fstatus',
        'bactivo',
    ];


     public function rGenSolCvc()
    {
        return $this->hasOne('App\Models\GenSolicitud','id','gen_solicitud_id');
    }

    public function rGenUsuario()
    {
        return $this->hasOne('App\Models\GenUsuario','id','gen_usuario_id');
    }

     public function rGenStatus()
    {
        return $this->hasOne('App\Models\GenStatus','id','gen_status_id');
    }
}
