<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UltimaExport extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */      
    public function up()
    {
         Schema::create('ultima_export', function (Blueprint $table) {
           $table->engine = 'InnoDB';
           $table->increments('id');
           $table->unsignedInteger('gen_usuario_id')->index()->nullable();
           $table->foreign('gen_usuario_id')->references('id')->on('gen_usuario');
           $table->unsignedInteger('pais_id')->index()->nullable();//Se agrego el pais de ult exportacion
           $table->foreign('pais_id')->references('id')->on('pais');
           $table->date('fecha_ult_export')->nullable();// Se agrego un nuevo campo//
           $table->boolean('bactivo')->default(1);
           $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::dropIfExists('ultima_export');
        
    }
}
