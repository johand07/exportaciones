<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AnalisisCalificacion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('analisis_calificacion', function (Blueprint $table) {
          $table->engine = 'InnoDB';
          $table->increments('id');
          $table->unsignedInteger('gen_declaracion_jo_id')->index()->nullable();
          $table->foreign('gen_declaracion_jo_id')->references('id')->on('gen_declaracion_jo');
          $table->unsignedInteger('gen_usuario_id')->index()->nullable();
          $table->foreign('gen_usuario_id')->references('id')->on('gen_usuario');
          $table->unsignedInteger('gen_status_id')->index()->nullable();
          $table->foreign('gen_status_id')->references('id')->on('gen_status');
         
          $table->boolean('bactivo')->default(1);
          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::dropIfExists('analisis_calificacion');  
    }
}
