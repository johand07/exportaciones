<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>TURQUIA</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/style_Certificado.css">
</head>
<body>
<div class="centrar titulo text-normal espacio-superior">
    <table border="0" class="ancho-full">
         <tr>
               <td></td><td></td>
                <td class="ancho-medio centrar" colspan="5" style="height:30px;">
                    <div class="text-normal">MOVEMENT CERTIFICATE</div>
                    <td></td>
                    <td></td>
                </td>
                
                <td></td>
                <td></td>
                <td></td>
                
            </tr>
    </table>
</div>
 <div class="centrar espacio-superior">
        <table border="1" class="ancho-full">
        <tr>  
            <td class="ancho-medio top borde" colspan="5" style="width: 70%; height:80px;">
            <div class="text-normal">1.<b>Exporter</b> (Name, full address, country):
                <u>{{ $certificado->GenUsuario->DetUsuario->razon_social}}</u><br>
                <u>{{ $certificado->GenUsuario->DetUsuario->direccion }}</u>
                <u>{{$certificado->pais_cuidad_exportador}}</u>
                <u>{{ $certificado->GenUsuario->DetUsuario->rif }}</u>
           </div>
            </td>

            <td class="ancho-medio top borde" colspan="10" style="width: 70%; height:80px;">
            <div class="text-normal"><h3 class="centrar">EUR.1 No A</h3><p class="centrar">See notes overleaf before completing this form</p>2. Certificate used in preferential trade between
            <u>{{ $certificado->pais_exportador }}</u><br>
            <u>{{ $certificado->pais_importador }}</u>
            </div>
            </td>
            <br>
        </tr>
        </table>
    </div>
    <div class="centrar espacio-superior">
        <table border="1" class="ancho-full">
            <tr>
                <td class="ancho-medio top borde" colspan="5" style="width: 70%; height:80px;">
                    <div class="text-normal">3.Consignee(Name,full address,country)(Optional) <br> <br>
                        <u>{{ ($importadorCertificado->razon_social_importador) }}</u><br>
                        <u>{{ ($importadorCertificado->direccion_importador) }}</u>
                    </div>
                </td>
                <td class="ancho-medio top borde" colspan="5" style="width: 100%; height:80px;">
                    <div class="text-normal">4.Country, group of countries <br> or territory in which the products <br> are considered as origininathing<br><br>
                        <u>{{ $certificado->pais_cuidad_exportador }}</u>
                    </div>
                </td>
                <td class="ancho-medio top borde" colspan="5" style="width: 50%; height:80px;">
                    <div class="text-normal">5.Country, group of countries <br> or territory in which the products are <br>considered as origininathing<br><br>
                    <u>{{ ($importadorCertificado->pais_ciudad_importador) }}</u>
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <div class="centrar espacio-superior">
        <table border="1" class="ancho-full">
            <tr>
                <td class="ancho-medio top borde" colspan="5" style="width: 70%; height:80px;">
                    <div class="text-normal">6. Transport details (Optional)<br> <br>
                    <u>{{ ($importadorCertificado->medio_transporte_importador) }}</u>
                    </div>
                </td>
                <td class="ancho-medio top borde" colspan="10" style="width: 100%; height:80px;">
                    <div class="text-normal">7.Remarks<br><br>
                    <u>{{$certificado->observaciones}}</u>
                    </div>
                </td>
            </tr>
        </table>
    </div>
     <div class="centrar espacio-superior">
        <table border="1" class="ancho-full">
            <tr>
                <td class="ancho-medio top borde" colspan="5" style="width: 70%; height:80px;">
                    <div class="text-normal">8.<b>Item number;Marks and numbers; Number and kind of packages; Description of goods(1)<br> <br>
                    <u></u>
                </div>
                </td>
                <td class="ancho-medio  top borde" colspan="5" style="width: 100%; height:80px;">
                    <div class="text-normal">9.<b>Gross mass (kg) or other measure (litres,m3,etc.)<br><br>
                    </div>
                </td>
                <td class="ancho-medio top borde" colspan="5" style="width: 50%; height:80px;">
                    <div class="text-normal">10.<b>Invoices(2)<br><br>
                    <u>{{ $detdeclaracionCert[0]->numero_factura }} {{ $detdeclaracionCert[0]->f_fac_certificado }}</u><br>
                    </div>
                </td>
            </tr>
             @foreach($detalleProduct as $key => $value)
            <tr>
                <td class="top borde" align="center" colspan="5">{{$value->codigo_arancel}} {{$value->denominacion_mercancia}}<br><br></td>
                <td class="borde" colspan="5">{{$value->unidad_fisica}}<br><br></td>

                @if($key ==0)
                <br>
                 <td class="top titulo justify" colspan="5">{{ $detdeclaracionCert[0]->numero_factura }} {{ $detdeclaracionCert[0]->f_fac_certificado }}</td>
                @else
                <td class="top titulo justify" colspan="5"></td>
                @endif
            </tr>
            @endforeach
        </table>
    </div>
    <div class="centrar espacio-superior">
        <table border="1" class="ancho-full">
            <tr>
                <td class="ancho-medio top borde" colspan="5" style="width: 70%; height:80px;">
                    <div class="text-normal">11.COMPETENT AUTHORITY ENDORSEMENT<br> <br>
                        <u>Declaration certified</u>
                        <u>Export Document (3)</u>
                        <u><div>Form  <b>VENEZUELA</b>............No {{$certificado->num_certificado}}.........</div></u>
                        <u><div>Of {{$certificado->fecha_emision_exportador}}..........................</div></u>
                        <u><div>Competent Authority {{$datosfirmante->ente_firmante }} ...........................</div></u>
                        <u><div>Issuing country or territory  &nbsp;&nbsp;CARACAS....................</div></u>
                        <u><div>Place and date ....................{{$certificado->created_at}}</div><br></u>
                         ..........................................................................
                        <u><div>(Signature)</div></u>
                    </div>
                </td>
                <td class="ancho-medio top borde" colspan="10" style="width: 100%; height:80px;">
                <div class="text-normal">12.DECLARATION BY THE EXPORTER <br>
                    <u>1.the undersigned, declare that the goods described aove meet the conditions required for the issue of this certificate.<br><br></u>
                    <div>Place and date</div><br>
                    .........................................................................................
                    <br><br><br><br>
                     .........................................................................................
                     <div>&nbsp;&nbsp;&nbsp;&nbsp;(Signature)</div>
                </div>
                </td>
                </tr>
            </table>
        </div>
        <div class="centrar espacio-superior">
        <table border="1" class="ancho-full">
        <tr>
            <td class="ancho-medio top borde" colspan="5" style="width: 70%; height:80px;">
                    <div class="text-normal">13.REQUEST FOR VERIFICATION, to
                    <br><br><br> 
                    SISTEMA NACIONAL INTEGRADO DE ADMINISTRACION<br>
                    ADUANERA Y TRIBUTARIA (SENIAT)
                     __________________________________________________________________________________
                    <br><div class="top">Verification of the authenticity and accuracy of this certificate is requested<br><br><br>.......................................................................................................
                    <p class="centrar">(Place and date)</p>
                    ........................................................
                    <br><p>&nbsp;&nbsp;&nbsp;&nbsp;(Signature)</p>
                    </div>
                </td>
            
                <td class="ancho-medio top borde" colspan="10" style="width: 100%; height:80px;">
                <div class="text-normal">14.RESULT OF VERIFICATION <br><br>
                    1. the undersigned, declare that the goods described above meet the conditions for the issue of this certificate.<br><br>
                </div>
                </td>
        
            </tr>  
            </table>
        </div>
</body>
</html>