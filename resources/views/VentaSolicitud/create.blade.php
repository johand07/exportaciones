@extends('templates/layoutlte')
@section('content')
<div class="panels">
<!--Vista-->
<div class="">


  {{Form::open(['route'=>'VentaSolicitud.store','method'=>'POST','id'=>'formventasolicitud', 'enctype' => 'multipart/form-data']) }}
  <div class="row ">
    <div class="col-md-6">
      <label>Usted ha realizo con éxito su exportación?</label>
    </div>
    <div class="col-md-6">
      <div class="form-group" id="realizoventa">
        <label class="radio-inline">
          <input type="radio" name="realizo_venta" value="1" id="accion_si" checked>SI
        </label>
        <label class="radio-inline">
          <input type="radio" name="realizo_venta" value="2" id="accion_no">No
        </label>
      </div>
    </div>
    

    <div class="row">
      <div class="col-md-12">
        <table id="ventasolicitud" border="1" class="table table-striped table-bordered" style="width:100%">
          <thead>
            <tr>
              <th>Monto Total de la Exportación</th>
              <th>Monto FOB</th>
              <th>Monto Vendido</th>
              <th>Monto Pendiente por Vender</th>
            </tr>
          </thead>
          <tbody>        
            <tr>
              <td>{{$solicitud->monto_cif-$monto_nota_credito}}</td>
              <td>{{$solicitud->monto_solicitud - $monto_nota_credito}}</td>
              <td>{{$solicitud->monto_solicitud- $monto_nota_credito}} / {{$mvendido}}</td>
              <td>{{$solicitud->monto_solicitud- $monto_nota_credito}} / {{$pendiente}}</td>
            </tr> 
          </tbody>
        </table>
      </div>
    </div>

    <div class="container" id="fomulariodvd">
      <div class="row">
        <div class="col-md-10">
          <div class="col-md-6">
            <div class="form-group">
              {!! Form::hidden('gen_solicitud_id',$idsolicitud,['class'=>'form-control', 'id'=>'gen_solicitud_id']) !!}
              {!! Form::label('','Operador Cambiario') !!}
              {!! Form::select('gen_operador_cambiario_id',$oca,null,['class'=>'form-control', 'id'=>'gen_operador_cambiario_id','placeholder'=>'Seleccione un Operador Cambiario']) !!}
            </div>
            <div class="form-group">
              {!! Form::label('','Fecha Disponibilidad de las Divisas') !!}
              <div class="input-group date">
                {!! Form::text('fdisponibilidad',null,['class'=>'form-control','readonly'=>'readonly','id'=>'fdisponibilidad']) !!}
                <span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
              </div>
            </div>
            <div class="form-group">
              {!! Form::label('','Número de Factura') !!}
              {!! Form::select('gen_factura_id',$facturas,null,['class'=>'form-control', 'id'=>'gen_factura_id','placeholder'=>'Seleccione un Número de Factura','onchange'=>'facturaSolicitud()']) !!}
            </div>
            <div class="form-group">
              {!! Form::hidden('pendienteventa',null,['class'=>'form-control', 'id'=>'pendienteventa','readonly'=>'true']) !!}
              {!! Form::hidden('mfob',null,['class'=>'form-control', 'id'=>'mfob','readonly'=>'true']) !!}
              {!! Form::label('mpercibido','Monto Percibido de la Venta') !!}
              {!! Form::text('mpercibido',null,['class'=>'form-control','id'=>'mpercibido','onchange'=>'calcular()']) !!}
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              {!! Form::label('','Monto Venta BCV') !!}
              {!! Form::text('mvendido',null,['class'=>'form-control','id'=>'mvendido','readonly'=>'true']) !!}
              {!! Form::hidden('porcentajebcv',null,['class'=>'form-control','id'=>'porcentajebcv','readonly'=>'true']) !!}
            </div>
            <div class="form-group">
              {!! Form::label('','Monto a retener por el usuario según el Monto percibido de la venta') !!}
              {!! Form::text('mretencion',null,['class'=>'form-control','id'=>'mretencion','readonly'=>'true']) !!}
              {!! Form::hidden('porcentajeretencion',null,['class'=>'form-control','id'=>'porcentajeretencion','readonly'=>'true']) !!}
            </div>
            <div class="form-group">
              {!! Form::label('descripcion','Observaciones')!!}
              {!! Form::text('descripcion',null,['class'=>'form-control','id'=>'descripcion','maxlength'=>'200','onkeypress'=>'return soloNumerosyLetras(event)'])!!}
            </div>
          </div>
        </div>
      </div>
      <!-------------------- Aqui Carga de Documentos BDV-------------------->
      <br><br>
        <!--Congelando la carga de los doc tesoro y bdv-->
      {{--<div class="row container-dvd" id="documentos_dvd" style="display:none;">
        <h4>&nbsp;&nbsp;&nbsp;<b>Carga de Documentos</b></h4><br><br>
        <div class="row">
          <div class="col-md-12">
            <div class="col-md-6">
              <table class="table table-bordered text-left" id="factura">
                <span><b>Factura Comercial</b></span><br><br> 

                <td>
                  <strong style="color: red"> *</strong>
                  <input type="hidden" name="cat_documentos_id[]" id="cat_documentos_id" value="8">
                  <input type="file" name='ruta_doc_dvd[]' id='fac_comercial_dvd' class="file_multiple"><!--Quitando required de los campos-->
                </td>

              </table>

            </div>
            <div class="col-md-6">
              <table class="table table-bordered text-left" id="notadebito">
                <span><b>Nota débito</b></span><br><br> 
                <tr> <td><button  name="add" type="button" id="add-notadebito" value="add more" class="btn btn-success" >Agregar</button></td></tr>

                <tr id="row1">

                  <td>
                    <!--<strong style="color: red"> *</strong>-->
                    <input type="hidden" name="cat_documentos_id[]" id="cat_documentos_id" value="9">
                    <input type="file" name='ruta_doc_dvd[]' id='nota_debito_dvd' class="file_multiple">
                  </td>

                </tr>
              </table>
            </div>
          </div>
        </div><br><br>

        <div class="row">
          <div class="col-md-12">
            <div class="col-md-6">
              <table class="table table-bordered text-left" id="notacredito">

                <span><b>Nota crédito</b></span><br><br>
                <tr> <td><button  name="add" type="button" id="add-notacredito" value="add more" class="btn btn-success" >Agregar</button></td></tr>

                <tr id="row2">

                  <td>
                    <!--<strong style="color: red"> *</strong>-->

                    <input type="hidden" name="cat_documentos_id[]" id="cat_documentos_id" value="43">

                    <input type="file" name='ruta_doc_dvd[]' id='nota_credito_dvd' class="file_multiple">
                  </td>

                </tr>
              </table>

            </div>
            <div class="col-md-6">
              <table class="table table-bordered text-left" id="docembarque">

                <span><b>Documento(s) de embarque (Bill Of lading,<br>guía aérea, CPIC, guía de entrega o despacho).</b></span><br><br>


                <tr> <td><button  name="add" type="button" id="add-docembarque" value="add more" class="btn btn-success" >Agregar</button></td></tr>

                <tr id="row3">

                  <td>
                    <strong style="color: red"> *</strong>

                    <input type="hidden" name="cat_documentos_id[]" id="cat_documentos_id" value="42">

                    <input type="file" name='ruta_doc_dvd[]' id='doc_embarq_dvd' class="file_multiple"><!--Quitando required de los campos-->

                  </td>

                </tr>
              </table>

            </div>

          </div>
        </div><br><br><br>

        <div class="row">
          <div class="col-md-12">
            <div class="col-md-6">
              <table class="table table-bordered text-left" id="swift">

                <span><b>Copia de mensaje(s) Swift correspondiente al ingreso <br>o pago de las divisas producto de la exportación realizada.</b></span><br><br>


                <tr> <td><button  name="add" type="button" id="add-swift" value="add more" class="btn btn-success" >Agregar</button></td></tr>

                <tr id="row4">

                  <td>
                    <strong style="color: red"> *</strong>

                    <input type="hidden" name="cat_documentos_id[]" id="cat_documentos_id" value="6">

                    <input type="file" name='ruta_doc_dvd[]' id='swift_dvd' class="file_multiple"><!--Quitando required de los campos-->

                  </td>

                </tr>
              </table>

            </div>
            <div class="col-md-6">
              <table class="table table-bordered text-left" id="declaracioniva">
                <span><b>Planilla de declaración y pago del IVA.</b></span><br><br> 

                <td>
                  <strong style="color: red"> *</strong>
                  <input type="hidden" name="cat_documentos_id[]" id="cat_documentos_id" value="3">
                  <input type="file" name='ruta_doc_dvd[]' id='iva_dvd' class="file_multiple">
                </td>

              </table>

            </div>

          </div>
        </div><br><br>
      </div>--}}


<!---Caso no de desestimiento-->
      <div class="row">
        <div class="col-md-12">
          <div class="col-md-4"></div>
          <div class="col-md-4 text-center">
            <div class="form-group">
              <a href="{{route('DvdSolicitud.index')}}" class="btn btn-primary">Cancelar</a>
              <input class="btn btn-success" type="submit" name="" value="Enviar" onclick="enviardvd()">
            </div>
          </div>
          <div class="col-md-4"></div>
        </div>
      </div>
    </div>
    <div class="container"  id="desestimiento" style="display: none;">
      <div class="row">
        <div class="col-md-12">
          <div class="col-md-4">
            <div class="form-group">
              {!! Form::label('','Número de Factura') !!}
              {!! Form::select('gen_factura_id_2',$facturas,null,['class'=>'form-control', 'id'=>'gen_factura_id','placeholder'=>'Seleccione un Numero de Factura','onchange'=>'facturaSolicitud()']) !!}
            </div>
          </div>
          <div class="col-md-5">
            <div class="form-group">
              {!! Form::label('descripcion','Indique brevemente el por qué no se concretó la exportación')!!}
              {!! Form::text('descripciondes',null,['class'=>'form-control','id'=>'descripciondes','maxlength'=>'200','onkeypress'=>'return soloNumerosyLetras(event)'])!!}
            </div>
          </div>
          <div class="col-md-3"></div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="col-md-4"></div>
          <div class="col-md-4 text-center">
            <div class="form-group">
              <a href="{{route('DvdSolicitud.index')}}" class="btn btn-primary">Cancelar</a>
              <input class="btn btn-success" type="submit" name="" value="Enviar" onclick="enviardvd()">
            </div>
          </div>
          <div class="col-md-4"></div>
        </div>
      </div>
    </div>
  </div>
  </div>
  {{Form::close()}}

  <style type="text/css">
    .container-dvd {
      width: 950px; 
    }
    @media (min-width: 1200px)
    .container-dvd {
      width: 900px; 
    }
    @media (min-width: 992px)
    .container-dvd {
      width: 970px;
    }
    @media (min-width: 768px)
    .container-dvd {
      width: 750px;
    }
  </style>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
  <script src="{{asset('js/ajax_v2.1.1.js')}}"></script>
  <!--Script para Tooltips guia de campos con stylos bootstrap-->
  <script src="https://unpkg.com/@popperjs/core@2"></script>
  <script src="https://unpkg.com/tippy.js@6"></script>

  <script type="text/javascript">



    $(document).ready(function() {

        
  /**Congelando funcion para ocultar los doc en el caso de bdv y tesoro**/
      /*$("#accion_no").click(function() {  
          if($("#accion_no").is(':checked')) {  
            $('#gen_operador_cambiario_id').val('');
         
            $('#documentos_dvd').hide(1000);//Si es cualquier otra Ocultar
            $("#fac_comercial_dvd").prop('required',false);
            $("#nota_debito_dvd").prop('required',false);
            $("#nota_credito_dvd").prop('required',false);
            $("#doc_embarq_dvd").prop('required',false);
            $("#swift_dvd").prop('required',false);
            $("#iva_dvd").prop('required',false); 
          } else {  
                 
          }  
      }); 
      */ 

      $("#mvendido").prop("readonly", true); 
      $("#mretencion").prop("readonly", true); 



//////////////////////Para Documentos BDV listas dinamicas////////////////////////

///////////////////////lista1 Nota debito
var i = 0;
$('#add-notadebito').click(function(){
  i++;


  $('#notadebito tr:last').after('<tr id="row1'+i+'"display="block" class="show_div"><td><strong style="color: red"> *</strong><input type="hidden" name="cat_documentos_id[]" id="cat_documentos_id" value="9"><input type="file" name="ruta_doc_dvd[]" id="ruta_doc_dvd'+i+'" class="file_multiple"></td><td><button  name="remove" type="button" id="'+i+'"value="" class="btn btn-danger btn-remove1">x</button></td></tr>');


});

$(document).on('click','.btn-remove1',function(){
  var id_boton= $(this).attr("id");
  $("#row1"+id_boton+"").remove();

});


////////////////////////lista2
var j = 0;
$('#add-notacredito').click(function(){
  j++;


  $('#notacredito tr:last').after('<tr id="row2'+j+'"display="block" class="show_div"><td><strong style="color: red"> *</strong><input type="hidden" name="cat_documentos_id[]" id="cat_documentos_id" value="43"><input type="file" name="ruta_doc_dvd[]" id="ruta_doc_dvd'+j+'" class="file_multiple"></td><td><button  name="remove" type="button" id="'+j+'" value="" class="btn btn-danger btn-remove2">x</button></td></tr>');


});

$(document).on('click','.btn-remove2',function(){
  var id_boton= $(this).attr("id");
  $("#row2"+id_boton+"").remove();

});

////////////////////////lista3
var k = 0;
$('#add-docembarque').click(function(){
  k++;

  $('#docembarque tr:last').after('<tr id="row3'+k+'"display="block" class="show_div"><td><strong style="color: red"> *</strong><input type="hidden" name="cat_documentos_id[]" id="cat_documentos_id" value="42"><input type="file" name="ruta_doc_dvd[]" id="ruta_doc_dvd'+k+'" class="file_multiple" required="true"></td><td><button  name="remove" type="button" id="'+k+'" value="" class="btn btn-danger btn-remove3">x</button></td></tr>');


});

$(document).on('click','.btn-remove3',function(){
  var id_boton= $(this).attr("id");
  $("#row3"+id_boton+"").remove();

});
////////////////////////lista4

var l = 0;
$('#add-swift').click(function(){
  j++;


  $('#swift tr:last').after('<tr id="row4'+l+'"display="block" class="show_div"><td><strong style="color: red"> *</strong><input type="hidden" name="cat_documentos_id[]" id="cat_documentos_id" value="6"><input type="file" name="ruta_doc_dvd[]" id="ruta_doc_dvd'+l+'" class="file_multiple" required="true"></td><td><button  name="remove" type="button" id="'+l+'" value="" class="btn btn-danger btn-remove4">x</button></td></tr>');


});

$(document).on('click','.btn-remove4',function(){
  var id_boton= $(this).attr("id");
  $("#row4"+id_boton+"").remove();

});


});


////Funcion para ocultar y mostrar solo mostrar Carga de Documentos cuando sea BDV y Tesoro

/**Congelando funcion para mostar u ocultar los doc en bdv y tesoro*****/
/*function oca (argument) {

  let oca=$("#gen_operador_cambiario_id").val();
  //let oca=$("#gen_operador_cambiario_id").val();
//lert(oca);
console.log('oca',oca);
if (oca==2 || oca==18) {
$("#documentos_dvd").show(1000)//Mostrar 

$("#fac_comercial_dvd").prop('required',false);
$("#nota_debito_dvd");
$("#nota_credito_dvd");
$("#doc_embarq_dvd").prop('required',false);
$("#swift_dvd").prop('required',false);
$("#iva_dvd").prop('required',false);


}else{
$('#documentos_dvd').hide(1000);//Si es cualquier otra Ocultar

$("#fac_comercial_dvd").prop('required',false);
$("#nota_debito_dvd").prop('required',false);
$("#nota_credito_dvd").prop('required',false);
$("#doc_embarq_dvd").prop('required',false);
$("#swift_dvd").prop('required',false);
$("#iva_dvd").prop('required',false);


}
}*/

/**Congelando funcion para mostar u ocultar los doc en bdv y tesoro*****/


/*** Funcion para calcular los montos de ventas, monto fob ***/
function facturaSolicitud(){
$.ajax({    //AgenteAduanal.destroy O exportador/AgenteAduanal/{AgenteAduanal}
  url: '/exportador/VentaSolicitud',
  type: 'GET',

  data: {
    '_token'  : $('#token').val(),

    'gen_factura_id':$('#gen_factura_id').val(),
    'gen_solicitud_id':$('#gen_solicitud_id').val(),
  },
})
.done(function(data) {
  var montototal     = data.monto_fob;
  var ventabcv     = data.montoventa;
  var retencionusr     = data.montoretencion;
  var mpendiente     = data.montopendiente;

//alert(ventabcv);
//alert(retencionusr);
//alert(montototal);
$('#mfob').val(montototal);
$('#mpercibido').val(mpendiente);
$('#porcentajebcv').val(ventabcv);
$('#porcentajeretencion').val(retencionusr);
$('#pendienteventa').val(mpendiente);

var mpercibido=$('#mpercibido').val();
var porcentajebcv=$('#porcentajebcv').val();
var porcentajeretencion=$('#porcentajeretencion').val();
var mpendienteventa=$('#pendienteventa').val();

calcular(mpercibido,porcentajebcv,porcentajeretencion,mpendienteventa);

})
.fail(function(jqXHR, textStatus, thrownError) {
  errorAjax(jqXHR,textStatus)
})
}

function calcular(mpercibido,porcentajebcv,porcentajeretencion,mpendienteventa){
  var mpercibido=parseFloat($('#mpercibido').val());
  var porcentajebcv=parseFloat($('#porcentajebcv').val());
  var porcentajeretencion=parseFloat($('#porcentajeretencion').val());
  var mpendienteventa=parseFloat($('#pendienteventa').val());

  var mfob=parseFloat($('#mfob').val());
  /*alert(mpercibido+'>'+mpendienteventa);*/
  var pendiente=mfob-mpercibido;


  if (mpercibido > mpendienteventa) {

    swal('Error!', ' El monto percibido de la venta no puede ser mayor al monto pendiente por vender', 'warning',{ button: "Ok!",});
    $('#mpercibido').val(mfob);
    $('#pendienteventa').val(mpendienteventa);
  }else{
    $('#pendienteventa').val(mpendienteventa);
  }

var ventabcv=porcentajebcv*mpercibido/100; // venta bcv dependiando del %
var retencionusr=porcentajeretencion*mpercibido/100;// retencion

$('#mvendido').val(ventabcv);
$('#mretencion').val(retencionusr);

}

$(document).ready(function () {
  $('#accion_no').click(function(){
    $('#fomulariodvd').hide(1000);
    $('#desestimiento').show(1000);
  });
  $('#accion_si').click(function(){
    $('#fomulariodvd').show(1000);
    $('#desestimiento').hide(1000);

  });
});



////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////

/********************Scrip para carga de archivos**************** */
function cambiarImagen(event) {
  console.log('event', event);
  var id= event.target.getAttribute('id');

  console.log('id', id);
  var reader = new FileReader();
/*reader.onload = function(){
var output = document.getElementById('vista_previa_'+id);
output.src = reader.result;
};*/
//let archivo = $(".file-input").val();
//if (!archivo) {

  let archivo=$('#'+id).val();
//console.log(reader);
//}
var extensiones = archivo.substring(archivo.lastIndexOf(".")).toLowerCase();

//var imgfile="/exportaciones/public/img/file.png";

//alert(imgfile);
if(extensiones != ".doc" && extensiones != ".docx" && extensiones != ".pdf"){
  swal("Formato invalido", "Formato de archivo invalido debe ser .doc, .docx o pdf", "warning"); 


}else{
//Pintame la vista previa el Documento azulito cuando se carga un archivo con imgdocumento_
$('#imgdocumento_'+id).show();
let file=$('#'+id).val();
// alert(file);
//Y pintame ruta y nombre del archivo acargado con nombre_
$('#nombre_'+id).html('<strong>'+file+'</strong>');

reader.readAsDataURL(event.target.files[0]);

}

}
///////////////Para validar documentos//////////////////

function cambiarImagenDoc(event) {


//console.log(event);

var id= event.target.getAttribute('id');
//alert(id);


archivo=$('#'+id).val();
//}
var extensiones = archivo.substring(archivo.lastIndexOf(".")).toLowerCase();

//var imgfile="/exportaciones/public/img/file.png";


if(extensiones != ".doc" && extensiones != ".docx" && extensiones != ".pdf"){
  swal("Formato invalido", "Formato de archivo invalido debe ser .doc, .docx o pdf", "warning"); 
  $('#'+id).val('');

}else{
  $('#imgdocumento_'+id).show();
  let file=$('#'+id).val();
// alert(file);
$('#nombre_'+id).html('<strong>'+file+'</strong>');

reader.readAsDataURL(event.target.files[0]);

}

}
/////////////////Validacion acondicionada para cuanto la carga de archivo NO puede sercargado un archivo que pee mas de 2.5 megabytes////////////////
$(document).on('change', '.file-input', function(evt) {

  let size=this.files[0].size;
  if (size>8500000) {

    swal("Tamaño de archivo invalido", "El tamaño del archivo No puede ser mayor a 8.5 megabytes","warning"); 
    this.value='';
//this.files[0].value='';
}else{
  cambiarImagen(evt);
}


});
///////////LO mismo para carga de archivo lista dinamica//////////
$(document).on('change', '.file_multiple', function(evt) {


  let size=this.files[0].size;
  if (size>8500000) {

    swal("Tamaño de archivo invalido", "El tamaño del archivo No puede ser mayor a 8.5 megabytes","warning"); 
    this.value='';
  }else{
    cambiarImagenDoc(evt);
  }

});

</script>

@stop