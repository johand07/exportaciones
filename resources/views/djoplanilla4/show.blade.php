@extends('templates/layoutlte')
@section('content')

<div class="content">
<div class="panel-group" style="background-color: #fff;">
      <div class="panel-primary">
        <div class="panel-heading"><h3>(P-4) - 6. Proceso productivo literal del producto a exportar</h3></div>
        <div class="panel-body">


<div class="row" style="background-color: #fff">
    <div class="col-md-12">
 

        <div class="row">
            <div class="col-md-12"><h4><b>Producto:</b><h4></div>
            <div class="col-md-12">
                <h4>{{$producto->descrip_comercial}} (COD.ARANCELARIO: {{$producto->codigo}})</h4>
            </div>
            <div class="col-md-12" style="border:1px solid; border-radius:5px;">
                <div>
                   {!!$planilla4->proceso_produccion!!}
                </div>
            </div>
        </div>

          </div>



    </div>
</div>

        </div>
      </div>
    </div>

  </div><!-- Fin content -->

<!--script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script-->

@endsection