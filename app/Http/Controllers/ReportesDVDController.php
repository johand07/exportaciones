<?php

namespace App\Http\Controllers;

use App\Models\GenDVDSolicitud;
use App\Models\GenSolicitud;
use App\Models\GenUsuario;
use App\Models\DetUsuario;
use App\Models\GenFactura;
use Illuminate\Http\Request;
use Session;
use Auth;
use Alert;
use PDF;
use Illuminate\Routing\Route;
use Illuminate\Http\Response;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\DB;

class ReportesDVDController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $titulo='Reportes de Demostración de Ventas de Divisa (DVD)';
        $descripcion='Detalle de DVD';
        
        $anio=date('Y');
        $desde=$request->desde;
        $hasta=$request->hasta;
        // $desde =  $anio."/01/01";
        // $hasta = date('Y/m/d');

        $sol_dvd=DB::table('det_usuario as det')
        ->select('dvd.gen_solicitud_id as Num_Solicitud',
                 'dua.fembarque as Fecha_Embarque',
                 'sol.fsolicitud as Fecha_DVD',
                 'tsol.solicitud as tipo_Solicitud',
                 'fr.descrip_pago as Descripcion_de_Pago',
                 'det.rif',
                 'det.razon_social',
                 'fa.numero_factura as Num_Factura',
                 'fa.fecha_factura as Fecha_Factura',
                 'dua.numero_dua as DUA',
                 'fa.monto_fob as Monto_Fob_Factura',
                 'dvd.mfob as Monto_Fob',
                 'dvd.mpercibido as Monto_Percibido',
                 'dvd.mvendido as Monto_Vendido',
                 'dvd.mretencion as Monto_Retenido',
                 'dvd.mpendiente as Monto_Pendiente',
                 'dv.ddivisa as Divisa',
                 'dvd.fdisponibilidad as Fecha_Disponibilidad_Divisas',
                 'dvd.fventa_bcv as Fecha_Venta_BCV',
                 'op.nombre_oca as Operador_Cambiario',
                 'dvd.descripcion as Observaciones',
                 'st.nombre_status as Estatus',
                 'dvd.fstatus as Fecha_Estatus'
                )
        ->join('gen_solicitud as sol', 'det.gen_usuario_id', '=', 'sol.gen_usuario_id')//
        ->join('gen_dvd_solicitud as dvd', 'sol.id', '=', 'dvd.gen_solicitud_id')//
        ->join('gen_status as st', 'dvd.gen_status_id','=', 'st.id')//
        ->join('gen_operador_cambiario as op', 'dvd.gen_operador_cambiario_id', '=', 'op.id')//
        ->join('gen_factura as fa', 'dvd.gen_factura_id', '=', 'fa.id')//
        ->join('det_prod_factura as pro', 'fa.id', '=', 'pro.gen_factura_id')//
        ->join('gen_unidad_medida as uni', 'pro.unidad_medida_id', '=', 'uni.id')//
        ->join('gen_divisa as dv', 'dv.id', '=', 'fa.gen_divisa_id')//
        ->join('tipo_solicitud as tsol', 'fa.tipo_solicitud_id', '=', 'tsol.id')//
        ->join('forma_pago as fr', 'fa.forma_pago_id', '=', 'fr.id')//
        ->join('gen_dua as dua', 'fa.gen_dua_id', '=', 'dua.id')//
        ->join('gen_consignatario as cons', 'dua.gen_consignatario_id', '=', 'cons.id')//
        ->join('cat_tipo_convenio as cconv', 'cons.cat_tipo_convenio_id', '=', 'cconv.id')//
        ->whereNotIn('det.gen_usuario_id',[1,2,3,4,5,11,12,2430,2268])
        //->whereMonth('dvd.created_at',$mes)
        ->whereYear('sol.fsolicitud',$anio)
        ->where('dvd.bactivo',1)
        ->where('dvd.realizo_venta',1)
        ->orderBy('sol.fsolicitud', 'desc')
        //->whereBetween('sol.fsolicitud',[$desde,$hasta])
        ->distinct()
        ->get();

        //dd($sol_dvd);

        return view('ReportesDVD.index',compact('titulo','descripcion','sol_dvd','desde','hasta'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());
        $titulo='Reportes de Demostración de Ventas de Divisa (DVD)';
        $descripcion='Detalle de DVD';
        $fecha=date("Y-m-d H");
        $fecha_desde=$request->desde.' 00:00:00';
        $fecha_hasta=$request->hasta.' 23:59:59';
        $desde=$request->desde;
        $hasta=$request->hasta;
        $mes=date('m');
       
        
        $sol_dvd=DB::table('det_usuario as det')
        ->select('dvd.gen_solicitud_id as Num_Solicitud',
                 'dua.fembarque as Fecha_Embarque',
                 'sol.fsolicitud as Fecha_DVD',
                 'tsol.solicitud as tipo_Solicitud',
                 'fr.descrip_pago as Descripcion_de_Pago',
                 'det.rif',
                 'det.razon_social',
                 'fa.numero_factura as Num_Factura',
                 'fa.fecha_factura as Fecha_Factura',
                 'dua.numero_dua as DUA',
                 'fa.monto_fob as Monto_Fob_Factura',
                 'dvd.mfob as Monto_Fob',
                 'dvd.mpercibido as Monto_Percibido',
                 'dvd.mvendido as Monto_Vendido',
                 'dvd.mretencion as Monto_Retenido',
                 'dvd.mpendiente as Monto_Pendiente',
                 'dv.ddivisa as Divisa',
                 'dvd.fdisponibilidad as Fecha_Disponibilidad_Divisas',
                 'dvd.fventa_bcv as Fecha_Venta_BCV',
                 'op.nombre_oca as Operador_Cambiario',
                 'dvd.descripcion as Observaciones',
                 'st.nombre_status as Estatus',
                 'dvd.fstatus as Fecha_Estatus'
                )
        ->join('gen_solicitud as sol', 'det.gen_usuario_id', '=', 'sol.gen_usuario_id')//
        ->join('gen_dvd_solicitud as dvd', 'sol.id', '=', 'dvd.gen_solicitud_id')//
        ->join('gen_status as st', 'dvd.gen_status_id','=', 'st.id')//
        ->join('gen_operador_cambiario as op', 'dvd.gen_operador_cambiario_id', '=', 'op.id')//
        ->join('gen_factura as fa', 'dvd.gen_factura_id', '=', 'fa.id')//
        ->join('det_prod_factura as pro', 'fa.id', '=', 'pro.gen_factura_id')//
        ->join('gen_unidad_medida as uni', 'pro.unidad_medida_id', '=', 'uni.id')//
        ->join('gen_divisa as dv', 'dv.id', '=', 'fa.gen_divisa_id')//
        ->join('tipo_solicitud as tsol', 'fa.tipo_solicitud_id', '=', 'tsol.id')//
        ->join('forma_pago as fr', 'fa.forma_pago_id', '=', 'fr.id')//
        ->join('gen_dua as dua', 'fa.gen_dua_id', '=', 'dua.id')//
        ->join('gen_consignatario as cons', 'dua.gen_consignatario_id', '=', 'cons.id')//
        ->join('cat_tipo_convenio as cconv', 'cons.cat_tipo_convenio_id', '=', 'cconv.id')//
        ->whereNotIn('det.gen_usuario_id',[1,2,3,4,5,11,12,2430,2268])
        //->whereMonth('dvd.created_at',$mes)
        ->where('dvd.bactivo',1)
        ->where('dvd.realizo_venta',1)
        ->whereBetween('sol.fsolicitud',[$desde,$hasta])
        ->orderBy('sol.fsolicitud', 'desc')
        ->distinct()
        ->get();

        //dd($sol_dvd);


        return view('ReportesDVD.index',compact('titulo','descripcion','desde','hasta','sol_dvd','fecha_desde','fecha_hasta'));
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\GenDVDSolicitud  $genDVDSolicitud
     * @return \Illuminate\Http\Response
     */
    public function show(GenDVDSolicitud $genDVDSolicitud)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\GenDVDSolicitud  $genDVDSolicitud
     * @return \Illuminate\Http\Response
     */
    public function edit(GenDVDSolicitud $genDVDSolicitud)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\GenDVDSolicitud  $genDVDSolicitud
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, GenDVDSolicitud $genDVDSolicitud)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\GenDVDSolicitud  $genDVDSolicitud
     * @return \Illuminate\Http\Response
     */
    public function destroy(GenDVDSolicitud $genDVDSolicitud)
    {
        //
    }

     public function ExcelReportesDVD(Request $request, DetUsuario $detUsuario)
    {
        $desde=$request->desde;
        $hasta=$request->hasta;
        //dd($request->all());
        //dd($request->all());

        $sol_dvd=DB::table('det_usuario as det')
        ->select('dvd.gen_solicitud_id as Num_Solicitud',
                 'dua.fembarque as Fecha_Embarque',
                 'sol.fsolicitud as Fecha_DVD',
                 'tsol.solicitud as tipo_Solicitud',
                 'fr.descrip_pago as Descripcion_de_Pago',
                 'det.rif',
                 'det.razon_social',
                 'fa.numero_factura as Num_Factura',
                 'fa.fecha_factura as Fecha_Factura',
                 'dua.numero_dua as DUA',
                 'fa.monto_fob as Monto_Fob_Factura',
                 'dvd.mfob as Monto_Fob',
                 'dvd.mpercibido as Monto_Percibido',
                 'dvd.mvendido as Monto_Vendido',
                 'dvd.mretencion as Monto_Retenido',
                 'dvd.mpendiente as Monto_Pendiente',
                 'dv.ddivisa as Divisa',
                 'dvd.fdisponibilidad as Fecha_Disponibilidad_Divisas',
                 'dvd.fventa_bcv as Fecha_Venta_BCV',
                 'op.nombre_oca as Operador_Cambiario',
                 'dvd.descripcion as Observaciones',
                 'st.nombre_status as Estatus',
                 'dvd.fstatus as Fecha_Estatus'
                )
        ->join('gen_solicitud as sol', 'det.gen_usuario_id', '=', 'sol.gen_usuario_id')//
        ->join('gen_dvd_solicitud as dvd', 'sol.id', '=', 'dvd.gen_solicitud_id')//
        ->join('gen_status as st', 'dvd.gen_status_id','=', 'st.id')//
        ->join('gen_operador_cambiario as op', 'dvd.gen_operador_cambiario_id', '=', 'op.id')//
        ->join('gen_factura as fa', 'dvd.gen_factura_id', '=', 'fa.id')//
        ->join('det_prod_factura as pro', 'fa.id', '=', 'pro.gen_factura_id')//
        ->join('gen_unidad_medida as uni', 'pro.unidad_medida_id', '=', 'uni.id')//
        ->join('gen_divisa as dv', 'dv.id', '=', 'fa.gen_divisa_id')//
        ->join('tipo_solicitud as tsol', 'fa.tipo_solicitud_id', '=', 'tsol.id')//
        ->join('forma_pago as fr', 'fa.forma_pago_id', '=', 'fr.id')//
        ->join('gen_dua as dua', 'fa.gen_dua_id', '=', 'dua.id')//
        ->join('gen_consignatario as cons', 'dua.gen_consignatario_id', '=', 'cons.id')//
        ->join('cat_tipo_convenio as cconv', 'cons.cat_tipo_convenio_id', '=', 'cconv.id')//
        ->whereNotIn('det.gen_usuario_id',[1,2,3,4,5,11,12,2430,2268])
        ->where('dvd.bactivo',1)
        ->where('dvd.realizo_venta',1)
        ->whereBetween('sol.fsolicitud',[$desde,$hasta])
        ->orderBy('sol.fsolicitud', 'desc')
        ->distinct()
        ->get();

        //dd($sol_dvd);

        $originalDate = "2022-06-02";
        $timestamp = strtotime($originalDate); 
        $newDate = date("d-m-Y", $timestamp );
        //dd($newDate);
        $numero = 1002002.365;
        $format=number_format($numero, 2, ",", ".");
        //dd($format);
        

        $name="Reportes de solicitud DVD";
        
        //dd($sol_dvd);

        $myFile   = Excel::create('Reportes de solicitud DVD',function($excel)use($sol_dvd){ // Nombre princial del archivo
            $excel->sheet('Solicitud DVD',function($sheet)use ($sol_dvd){ // se crea la hoja con el nombre de datos
               // dd($sol_dvd);
                //header
                $sheet->setStyle(array(
                'font' => array(
                    'name' =>  'itálico',
                    'size' =>  12,
                    'bold' =>  true
                )
                ));

                $sheet->mergeCells('A1:I1'); // union de celdas desde A a F
                $sheet->row(1,['Reportes de solicitud DVD']);
                $sheet->row(2,[]);
                $sheet->row(3,['Numero de Solicitud','Fecha Embarque','Fecha de DVD','Tipo Solicitud','Descripcion de Pago','Rif','Razon Social','Numero de Factura','Fecha Factura','DUA','Monto Fob Factura','Monto Fob','Monto Percibido','Monto Vendido','Monto Retenido','Monto Pendiente','Divisa','Fecha Disponibilidad Divisas','Fecha Venta BCV','Operador Cambiario','Observaciones','Estatus','Fecha de Estatus']);

                //$sheet->fromArray($sol_dvd, null, 'A4', null, false);
                // consulta

                
               //dd($sol_dvd);
                //recorrido
                foreach ($sol_dvd as $emp) {

                $forma_embarque=strtotime($emp->Fecha_Embarque); 
                $fechaembarque = date("d-m-Y", $forma_embarque);

                $forma_sol=strtotime($emp->Fecha_DVD); 
                $fechasol = date("d-m-Y", $forma_sol);

                $forma_factura=strtotime($emp->Fecha_Factura); 
                $fechafactura = date("d-m-Y", $forma_factura);

                $forma_disponibilidad=strtotime($emp->Fecha_Disponibilidad_Divisas); 
                $fechadisponibilidad = date("d-m-Y", $forma_disponibilidad);

                $forma_venta=strtotime($emp->Fecha_Venta_BCV); 
                $fechaventa = date("d-m-Y", $forma_venta);

                $forma_status=strtotime($emp->Fecha_Estatus); 
                $fechastatus = date("d-m-Y", $forma_status);


                   $row=[]; // un arreglo vacio que se va a llenar con las posicion 
                   $row[0]=$emp->Num_Solicitud;
                   $row[1]=$fechaembarque;
                   $row[2]=$fechasol;
                   $row[3]=$emp->tipo_Solicitud;
                   $row[4]=$emp->Descripcion_de_Pago;
                   $row[5]=$emp->rif;
                   $row[6]=$emp->razon_social;
                   $row[7]=$emp->Num_Factura;
                   $row[8]=$fechafactura;
                   $row[9]=$emp->DUA;
                   $row[10]=number_format($emp->Monto_Fob_Factura, 2, ",", ".");
                   $row[11]=number_format($emp->Monto_Fob, 2, ",", ".");
                   $row[12]=number_format($emp->Monto_Percibido, 2, ",", ".");
                   $row[13]=number_format($emp->Monto_Vendido, 2, ",", ".");
                   $row[14]=number_format($emp->Monto_Retenido, 2, ",", ".");
                   $row[15]=number_format($emp->Monto_Pendiente, 2, ",", ".");
                   $row[16]=$emp->Divisa;
                   $row[17]=$fechadisponibilidad;
                   $row[18]=$fechaventa;
                   $row[19]=$emp->Operador_Cambiario;
                   $row[20]=$emp->Observaciones;
                   $row[21]=$emp->Estatus;
                   $row[22]=$fechastatus;

                   $sheet->appendRow($row); // agrega una fila vacia al final del reporte

                }


            });
           // dd($sol_dvd);
           
        });
        $myFile   = $myFile->string('xls'); 
        $response = array(
            'name' => $name, 
            'file' => "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64," . base64_encode($myFile), 
        );

        return response()->json($response);
        //->export('xlsx');
    }
}
