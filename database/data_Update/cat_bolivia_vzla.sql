/*
-- Query: SELECT * FROM exportaciones.cat_bolivia_vzla
LIMIT 0, 500

-- Date: 2019-08-23 01:58
*/
INSERT INTO `cat_bolivia_vzla` (`id`,`codigo`,`descripcion`,`bactivo`,`created_at`,`updated_at`) VALUES (1,'3-a','Anexo II,Artículo 3,literal a)',1,NULL,NULL);
INSERT INTO `cat_bolivia_vzla` (`id`,`codigo`,`descripcion`,`bactivo`,`created_at`,`updated_at`) VALUES (2,'3-b','Anexo II,Artículo 3,literal b)',1,NULL,NULL);
INSERT INTO `cat_bolivia_vzla` (`id`,`codigo`,`descripcion`,`bactivo`,`created_at`,`updated_at`) VALUES (3,'3-c','Anexo II,Artículo 3,literal c)',1,NULL,NULL);
INSERT INTO `cat_bolivia_vzla` (`id`,`codigo`,`descripcion`,`bactivo`,`created_at`,`updated_at`) VALUES (4,'3-d','Anexo II,Artículo 3,literal d)',1,NULL,NULL);
INSERT INTO `cat_bolivia_vzla` (`id`,`codigo`,`descripcion`,`bactivo`,`created_at`,`updated_at`) VALUES (5,'3-e','Anexo II,Artículo 3,literal e)',1,NULL,NULL);
INSERT INTO `cat_bolivia_vzla` (`id`,`codigo`,`descripcion`,`bactivo`,`created_at`,`updated_at`) VALUES (6,'3-f','Anexo II,Artículo 3,literal f)',1,NULL,NULL);
INSERT INTO `cat_bolivia_vzla` (`id`,`codigo`,`descripcion`,`bactivo`,`created_at`,`updated_at`) VALUES (7,'10','Anexo II,Artículo 10°',1,NULL,NULL);
INSERT INTO `cat_bolivia_vzla` (`id`,`codigo`,`descripcion`,`bactivo`,`created_at`,`updated_at`) VALUES (8,'n','No califica Origen Venezolano para ese Acuerdo o País',1,NULL,NULL);
INSERT INTO `cat_bolivia_vzla` (`id`,`codigo`,`descripcion`,`bactivo`,`created_at`,`updated_at`) VALUES (9,'*','Información insuficiente para la determinación del Origen',1,NULL,NULL);
INSERT INTO `cat_bolivia_vzla` (`id`,`codigo`,`descripcion`,`bactivo`,`created_at`,`updated_at`) VALUES (10,'(1)','no',1,NULL,NULL);
INSERT INTO `cat_bolivia_vzla` (`id`,`codigo`,`descripcion`,`bactivo`,`created_at`,`updated_at`) VALUES (11,'(2)','no',1,NULL,NULL);
INSERT INTO `cat_bolivia_vzla` (`id`,`codigo`,`descripcion`,`bactivo`,`created_at`,`updated_at`) VALUES (12,'(3)','no',1,NULL,NULL);
