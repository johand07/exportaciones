<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Facades\Auth;

class Admin
{
    protected $auth;

    /**
     * Create a new middleware instance.
     *
     * @param  Guard
     * @return void
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::user()->gen_tipo_usuario_id == 7) {
            return $next($request);
        } else {
            if (Auth::user()->gen_tipo_usuario_id != 2 )  {
         // dd('No eres Admin');
                return redirect('exportador/denegadaAdmin');
            }
            return $next($request);
        }
        
        
    }
}
