<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GenSectorProductivoDjir03 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gen_sector_productivo_djir03', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('gen_declaracion_inversion_id');
            $table->foreign('gen_declaracion_inversion_id', 'g_d_i_fk2')->references('id')->on('gen_declaracion_inversion');
            $table->unsignedInteger('cat_sector_productivo_eco_id')->index();
            $table->foreign('cat_sector_productivo_eco_id', 'g_s_p_fk1')->references('id')->on('cat_sector_productivo_eco');
            $table->boolean('bactivo')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gen_sector_productivo_djir03');
    }
}
