@extends('templates/layoutlte')
@section('content')
<style>button { margin: 20px; }</style>
<div class="content">
<div class="panel-group" style="background-color: #fff;">
      <div class="panel-primary">
        <div class="panel-heading"><h3>Carga de Documentos Adicionales</h3></div>
        <div class="panel-body">
        <h5 style="color: red">Formatos permitidos .doc, .docx, .pdf, .pptx, .ppt, .xls, .xlsx, .zip, .png, .jpg, .jpeg</h5>

                  <div class="row" style="background-color: #fff">
                      <div class="col-md-12">
                        {!! Form::open(['route' =>'DocAdicional.index' ,'method'=>'POST', 'id'=>'docAdicional', 'enctype' => 'multipart/form-data']) !!}
                          <div class="row">
                              <div class="col-md-12">
                                  <table border="0" class="col-md-12">
                                    <thead>
                                      <tr>
                                        <th class="text-center" width="50%" height="40px"><h3><b>Sección de Carga</b></h3></th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                  <input type="hidden" name="gen_declaracion_jo_id" value="{{$gen_declaracion_jo_id}}" placeholder="" id="gen_declaracion_jo_id">
                                      <tr>
                                        <td class="text-center" valign="top" > 
                                          <span><b>1.)</b></span>
                                          <span id="class_file_1" class="btn btn-default btn-file" >
                                              Cargar Archivo <span class="glyphicon glyphicon-picture"></span>
                                              {!! Form::file('file_1', array('class' => 'file-input','style'=>'margin: 10px','id'=>'file_1')) !!}
                                             
                                            </span>
                                          
                                        </td>
                                       
                                        <td class="text-center" height="40px"><img id="vista_previa_file_1" src="" alt="" width="300px" style="margin: 3px;">
                                        <img id="imgdocumento_file_1" src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px; display: none;">
                                        <div id="nombre_file_1"></div> <div id="alert_file_1"></div></td>
                                       
                                      </tr>

                                      <tr>
                                        <td class="text-center" valign="top" >
                                          <span><b>2.)</b></span> 
                                          <span id="class_file_2" class="btn btn-default btn-file" style="margin: 3px;">
                                              Cargar Archivo <span class="glyphicon glyphicon-picture"></span>
                                              {!! Form::file('file_2', array('class' => 'file-input','style'=>'margin: 10px','id'=>'file_2', 'noreq'=>'noreq')) !!}
                                          </span>
                                          
                                        </td>
                                        <td class="text-center" height="40px"><img id="vista_previa_file_2" src="" alt="" width="300px" style="margin: 3px;"><img id="imgdocumento_file_2" src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px; display: none;"><div id="nombre_file_2"></div><div id="alert_file_2"></div></td>
                                      </tr>

                                      <tr>
                                        <td class="text-center" valign="top" >
                                          <span><b>3.)</b></span> 
                                          <span id="class_file_3" class="btn btn-default btn-file" style="margin: 3px;">
                                              Cargar Archivo <span class="glyphicon glyphicon-picture"></span>
                                              {!! Form::file('file_3', array('class' => 'file-input','style'=>'margin: 10px','id'=>'file_3', 'noreq'=>'noreq')) !!}
                                          </span>
                                         
                                        </td>
                                        <td class="text-center" height="40px"><img id="vista_previa_file_3" src="" alt="" width="300px" style="margin: 3px;"><img id="imgdocumento_file_3" src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px; display: none;"><div id="nombre_file_3"></div><div id="alert_file_3"></div></td>
                                      </tr>

                                      <tr>
                                        <td class="text-center" valign="top" >
                                          <span><b>4.)</b></span> 
                                          <span id="class_file_4" class="btn btn-default btn-file" style="margin: 3px;">
                                              Cargar Archivo<span class="glyphicon glyphicon-picture"></span>
                                              {!! Form::file('file_4', array('class' => 'file-input','style'=>'margin: 10px','id'=>'file_4', 'noreq'=>'noreq')) !!}
                                          </span>
                                          
                                        </td>
                                        <td class="text-center" height="40px"><img id="vista_previa_file_4" src="" alt="" width="300px" style="margin: 3px;"><img id="imgdocumento_file_4" src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px; display: none;"><div id="nombre_file_4"></div><div id="alert_file_4"></div></td>
                                      </tr>

                                      <tr>
                                        <td class="text-center" valign="top" >
                                          <span><b>5.)</b></span> 
                                          <span id="class_file_5" class="btn btn-default btn-file" style="margin: 3px;">
                                              Cargar Archivo <span class="glyphicon glyphicon-picture"></span>
                                              {!! Form::file('file_5', array('class' => 'file-input','style'=>'margin: 10px','id'=>'file_5', 'noreq'=>'noreq')) !!}
                                          </span>
                                          
                                        </td>
                                        <td class="text-center" height="40px"><img id="vista_previa_file_5" src="" alt="" width="300px" style="margin: 3px;"><img id="imgdocumento_file_5" src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px; display: none;"><div id="nombre_file_5"></div><div id="alert_file_5"></div></td>
                                      </tr>

                                      <tr>
                                        <td class="text-center" valign="top" >
                                          <span><b>6.)</b></span> 
                                          <span id="class_file_6" class="btn btn-default btn-file" style="margin: 3px;">
                                              Cargar Archivo <span class="glyphicon glyphicon-picture"></span>
                                              {!! Form::file('file_6', array('class' => 'file-input','style'=>'margin: 10px','id'=>'file_6', 'noreq'=>'noreq')) !!}
                                          </span>
                                         
                                        </td>
                                        <td class="text-center" height="40px"><img id="vista_previa_file_6" src="" alt="" width="300px" style="margin: 3px;"><img id="imgdocumento_file_6" src="{{ asset('img/file.png') }}" alt="" width="100px" style="margin: 3px; display: none;"><div id="nombre_file_6"></div> <div id="alert_file_6"></div></td>
                                      </tr>

                                    </tbody>
                                  </table>
                              </div>
                          </div>
<hr>
                          <div class="row" style="background-color: #fff">

                                <div class="col-md-12" align="center">

                                     <input type="submit"  name="submit" id="submit" class="btn btn-primary"  value="Guardar" onclick="validacionForm()">
                                    <a class="btn btn-danger btn-md" href="{{ url('exportador/DeclaracionJO/create') }}">Cancelar</a>

                                </div>
                           </div>
                      {{Form::close()}}
                      </div>
                  </div>
        </div>
      </div>
    </div>
  </div><!-- Fin content -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script>
   function cambiarImagen(event) {
    var id= event.target.getAttribute('name');
    const i = (Number(id.substring(5))+1);
      
    var reader = new FileReader();
    reader.onload = function(){
      var output = document.getElementById('vista_previa_'+id);
      output.src = reader.result;
    };
    var nombre = event.target.getAttribute('name');
    var archivo = $("[name^="+nombre+"]").val()
    var extensiones = archivo.substring(archivo.lastIndexOf(".")).toLowerCase();
    // Validacion de formulario 
    if(extensiones==".doc"|| extensiones==".docx"|| extensiones==".pdf"|| extensiones==".pptx"|| extensiones==".ppt"|| extensiones==".xls"|| extensiones==".xlsx"|| extensiones==".zip"||extensiones==".png"||extensiones==".jpg"||extensiones==".jpeg"){ 
        $('#class_'+id).removeClass('btn btn-default btn-file').addClass('btn btn-success btn-file')
        $('#class_'+id).removeClass('btn btn-danger btn-file').addClass('btn btn-success btn-file')
        $('#alert_'+id).html('');
        $('#submit').removeAttr("disabled");    
        for (let j = i; j < 7; j++){           
            $('#file_'+j).removeAttr("disabled"); 
            $('#class_file_'+j).removeAttr("disabled");     
        }  
        if(extensiones != ".jpeg" && extensiones != ".png" && extensiones != ".jpg" && extensiones != ".gif"){
          $('#imgdocumento_'+id).show();
          let file=$('#'+id).val();
          $('#nombre_'+id).html('<strong>'+file+'</strong>');
          $('#imgdocumentoEdit_'+id).hide();
          $('#vista_previa_'+id).hide();            
        }else{        
          let file=$('#'+id).val();
          $('#nombre_'+id).html('<strong>'+file+'</strong>');
          $('#imgdocumento_'+id).hide();
          $('#vista_previa_'+id).show();
          reader.readAsDataURL(event.target.files[0]);
        }       
      }else{
             // swal("Formato de Archivo", "No Permitido", "warning");                       
              $('#class_'+id).removeClass('btn btn-default btn-file').addClass('btn btn-danger btn-file')
              $('#alert_'+id).html('<p style="color:red;">Formato no permitido para la carga</p>');
              $('#nombre_'+id).html('');
              $('#vista_previa_'+id).hide();
              $('#imgdocumento_'+id).show();
              $('#submit').attr('disabled', 'disabled');             
              for (let j = i; j < 7; j++) {           
               $('#file_'+j).attr('disabled', 'disabled');
               $('#class_file_'+j).attr('disabled', 'disabled');
              } 
      }
  }
    $(document).on('change', '.file-input', function(evt) {
           cambiarImagen(evt);
    });
</script>

<script>
  //Validación de formulario, se puede llamar y pasar la sección que contenga los inputs a validar
  $(document).ready(function ()
  {
              var error = '<div class="msg_alert"><span style="color:red" class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span><strong id="cod_aran-error" class="added"> El campo es obligatorio.</strong></div>';
                var valido = '<div class="msg_alert"><span style="color:green" class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span></div>';
                var mens=['Estimado Usuario. Debe completar los campos solicitados',]
  
                validacionForm = function() {
  
                $('#docAdicional').submit(function(event) {
                
                var campos = $('#docAdicional').find('input:file');
                var n = campos.length;
                var err = 0;
  
                $("div").remove(".msg_alert");
                //bucle que recorre todos los elementos del formulario
                for (var i = 0; i < n; i++) {
                    var cod_input = $('#docAdicional').find('input:file').eq(i);
                    if (!cod_input.attr('noreq')) {
                      if (cod_input.val() == '' || cod_input.val() == null)
                      {
                        err = 1;
                        cod_input.css('border', '1px solid red').after(error);
                      }
                      else{
                        if (err == 1) {err = 1;}else{err = 0;}
                        cod_input.css('border', '1px solid green').after(valido);
                      }
                      
                    }
                }
  
                //Si hay errores se detendrá el submit del formulario y devolverá una alerta
                if(err==1){
                    event.preventDefault();
                    swal("Por Favor!", mens[0], "warning")
                  }
  
                  });
                }
              });
  </script>

@endsection