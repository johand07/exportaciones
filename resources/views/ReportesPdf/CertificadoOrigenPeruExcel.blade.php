<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>PERÚ-VENEZUELA</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/style_Certificado.css">
</head>
<body>
    
    <!--div class="derecha titulo text-medio"><b style="color:red">N°</b> del Certificado <span class="">{{ $certificado->num_certificado}}</span></div-->
<!--Certificado-->
    <div class="centrar titulo text-normal espacio-superior">
        <table border="0" class="ancho-full">
            <tr>
                <td></td>
                <td class="ancho-medio centrar" colspan="3">
                    <div class="text-normal">CERTIFICADO DE ORIGEN</div>
                </td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td class="ancho-medio centrar" colspan="2">
                    <div class="text-normal" align="center">ACUERDO DE ALCANCE PARCIAL DE NATURALEZA COMERCIAL ENTRE LA 
                    </div>
                </td>
                <td></td>
            </tr>
             <tr>
                <td></td>
                <td class="ancho-medio centrar" colspan="2">
                    <div class="text-normal" align="center">REPÚBLICA BOLIVARIANA DE VENEZUELA Y LA REPÚBLICA DEL PERÚ
                    </div>
                </td>
                <td></td>
            </tr>
        </table>
       </div>
        <div class="centrar espacio-superior">
        <table border="0" class="ancho-full">
            <tr>
                <td class="ancho-medio centrar" colspan="2">
                    <div class="text-normal titulo">(1) PAÍS EXPORTADOR: <u>{{$certificado->pais_exportador}}</u></div>
                </td>
                 <td class="ancho-medio centrar" colspan="2">
                    <div class="text-normal titulo">(2) PAÍS IMPORTADOR: <u>{{$certificado->pais_importador}}</u></div>
                </td>
                <td></td>
            </tr>
        </table>
    </div>
    <!--Productos-->
     <div class="centrar espacio-superior">
        <table class="ancho-full borde table">
            <tr class="text-11">
               <td class="centrar top titulo borde" style="width: 17%;">(3) No. de Orden</td>
                <td class="centrar top titulo borde" style="width: 3%;">(4) Código Arancelario</td>
                <td class="centrar top titulo borde" style="width: 30%;" colspan="1">(5) Descripción de las Mercancías</td>
                <td class="centrar top titulo borde" style="width: 15%;" colspan="1">(6) Unidad Fisica según Factura</td>
                <td class="centrar top titulo borde" style="width: 15%;" colspan="1">(7) Valor de <br>cada Mercancía según Factura</td>
            </tr>
           
            @foreach($detalleProduct as $key => $value)
            
                <tr class="text-medio">
                    <td class="centrar top borde" rowspan="5">{{$value->num_orden}}</td>
                    <td class="centrar top borde" rowspan="5">{{$value->codigo_arancel}}</td>
                    <td class="centrar top borde" colspan="1" rowspan="5">{{$value->denominacion_mercancia}}</td>
                    <td class="centrar top borde" colspan="1" rowspan="5">{{$value->unidad_fisica}}</td>
                    <td class="centrar top borde" colspan="1" rowspan="5">{{$value->valor_fob}}</td>
                </tr>
            @endforeach
            @if(count($detalleProduct)<=1)
                @for($auxConut = 0; $auxConut<=3-count($detalleProduct); $auxConut++)
                 <tr class="text-medio">
                    <td class="centrar top">&nbsp;</td>
                    <td class="centrar top">&nbsp;</td>
                    <td class="centrar top">&nbsp;</td>
                    <td class="centrar top">&nbsp;</td>
                    <td class="centrar top">&nbsp;</td>
                    <td class="centrar top">&nbsp;</td>
                </tr>
                @endfor
            @endif
        </table>
    </div>
    <table class="ancho-full borde table">
        <tr class="text-medio">
            <td></td>
            <td class="centrar top titulo" style="width: 50%;" colspan="4">
                <b align="center">(8) DECLARACION DE ORIGEN</b>
            </td>
        </tr>
        <tr class="text-medio">
            <td class="centrar top titulo" style="width: 50%;" colspan="5">
                DECLARAMOS que las mercancías indicadas en el presente formulario, correspondientes a la(s) Factura(s) Comercial(es) No. <u>{{ $detdeclaracionCert[0]->numero_factura }}de fecha <u>{{ $detdeclaracionCert[0]->f_fac_certificado }}</u> cumplen con lo establecido en las Normas de Origen del presente Acuerdo, de conformidad con el siguiente desglose.</u>
            </td>
        </tr>
    </table>
    <!--Productos-->
  <!--Detalle declaracion-->
    
    <div class="centrar espacio-superior">
        <table class="ancho-full borde table">
            <tr class="text-medio">
                <td class="centrar top titulo borde" style="width: 15%;">(9) No. de Orden</td>
                <td class="centrar top titulo borde" style="width: 85%;" colspan="4">(10) Criterios para la Calificación de Origen</td>
            </tr>
            @foreach($detdeclaracionCert as $value1)
            
                <tr class="text-center">
                    <td class="centrar top">{{$value1->num_orden_declaracion}}</td>
                    <td class="centrar top" colspan="4">{{$value1->normas_criterio}}</td>
                </tr>

            @endforeach

            @if(count($detdeclaracionCert)<=1)
                @for($auxConut = 0; $auxConut<=3-count($detdeclaracionCert); $auxConut++)
                <tr class="text-medio">
                    <td class="centrar top">&nbsp;</td>
                    <td class="centrar top">&nbsp;</td>
                </tr>
                @endfor
            @endif
        
        </table>
    </div>

    <div class="centrar espacio-superior">
        <table class="ancho-full table borde">
            <tr class="text-medio">
                <td class="top titulo borde text-medio" colspan="3">(11) PRODUCTOR O EXPORTADOR</td>
                 <td class="top titulo borde text-small" colspan="2" style="width:20%;">(12) Sello y firma del Productor o Exportador</td>
            </tr>
           <tr class="text-medio">
                <td class="top borde" colspan="3">11.1 Nombre o Razón Social: {{ $certificado->GenUsuario->DetUsuario->razon_social }}</td>
                <td></td>
            </tr>
           <tr class="text-medio">
                <td class="top borde" colspan="3">11.2 Número de Identificación Fiscal: {{ $certificado->GenUsuario->DetUsuario->rif }}</td>
                <td></td>
            </tr>
            <tr class="text-medio">
                <td class="top borde" colspan="3">11.3 Dirección: {{ $certificado->GenUsuario->DetUsuario->direccion}}</td>
                <td></td>
            </tr>
            <tr class="text-medio">
                <td class="top borde" colspan="3">11.4 E-mail:{{ $certificado->GenUsuario->DetUsuario->correo}}</td>
                <td></td>
            </tr>
            <tr class="text-medio">
                <td class="top borde" colspan="3">11.5Teléfono:{{$certificado->GenUsuario->DetUsuario->telefono_movil}}</td>
                <td></td>
            </tr>
            <tr class="text-medio">
                <td class="top titulo borde" colspan="3">(13 IMPORTADOR</td>
                <td></td>
            </tr>
            <tr class="text-medio">
                <td class="top borde" colspan="3">13.1 Nombre o Razón Social: {{ strtoupper($importadorCertificado->razon_social_importador) }}</td>
                <td></td>
            </tr>
            <tr class="text-medio">
                <td class="top borde" colspan="3">13.2 Dirección: {{ strtoupper($importadorCertificado->direccion_importador) }}</td>
                <td></td>
            </tr>
            <tr class="text-medio">
                <td class="top borde" colspan="3">13.3 Teléfono: {{ $importadorCertificado->telefono }}</td>
                <td></td>
            </tr>
            <tr class="text-medio">
                <td class="top borde" colspan="3">13.4 E-mail: {{ $importadorCertificado->email_importador }}</td>
                <td></td>
            </tr>
            <tr class="text-medio">
                <td class="top borde" colspan="3">(14) Medio de Transporte: {{ ($importadorCertificado->medio_transporte_importador) }}</td>
                <td></td>
            </tr>

            <tr class="text-medio">
                <td class="top borde" colspan="3">(15) Puerto o Lugar de Embarque: {{ ($importadorCertificado->lugar_embarque_importador) }}</td>
                <td></td>
            </tr>
        </table>
        <table class="ancho-full borde table">
            <tr class="text-medio">
                <td class="centrar top titulo borde" style="width: 15%;" colspan="5"><b>(16)<b>OBSERVACIONES</b>
                </td>
            </tr>
            <tr>
                <td class="text-medio borde" colspan="5" style="height: 30%;">{{ $certificado->observaciones}}</td>
            </tr>
        </table>
    </div>
     <div class="centrar espacio-superior">
        <table class="ancho-full borde table">
            <tr class="text-medio">
                <td class="top titulo borde table" colspan="2">(17) CERTIFICACIÓN DEL ORIGEN</td>
                <td class="top titulo borde table borde" colspan="3">(18)Nombre y firma del funcionario habilitado y sello de la Entidad Habilitada o Autoridad Competente</td>
            </tr>
            <tr class="text-medio">
                <td class="top text-small" colspan="3"><div class="">Certifico la veracidad de la presente Declaracion en la ciudad de:<br></div>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <span class="under-dotted espacio-superior">________________________________________</span><br>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <u>A los:____/____/____</u>
                </td>
            </tr>
        </table>
    </div>
    
    <br>
</body>
</html>
