<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Pais;
use Auth;

class AjaxGenPaises extends Controller
{

	
    public function listarTodo1(Request $request)
    {
    	             
            $paises=Pais::select('dpais','cpais')->where('bactivo',1)->get();

            return response()->json($paises);
    }

    public function listarTodo(){

         /*$paises=Pais::select('text','dpais','cpais')->where('bactivo',1)->get();*/

         

         $paises = Pais::select('dpais','id')->where('bactivo',1)->get();

         $listadepaises=[];

         foreach ($paises as $key => $value) {

             $listadepaises[]=['text'=>''.$value->dpais.'','value'=>''.$value->id.''];
         }

         return response()->json($listadepaises);

        /* formato de respuesta [{"text": "Venezuela", "value" : "idvenezuela"}]*/
    }


}
