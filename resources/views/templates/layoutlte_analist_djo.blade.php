<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>.:Exportaciones:.</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="{{asset('adminlte/bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{asset('adminlte/bower_components/font-awesome/css/font-awesome.min.css')}}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{asset('adminlte/bower_components/Ionicons/css/ionicons.min.css')}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset('adminlte/dist/css/AdminLTE.min.css')}}">
  <!-- css calendario bootstrap -->
  <link rel="stylesheet" href="{{asset('css/bootstrap-datepicker3.min.css')}}">
    <!-- css data table bootstrap -->
  <link rel="stylesheet" href="{{asset('css/dataTables.bootstrap.min.css')}}">
  <link rel="stylesheet" href="{{asset('css/styleTab.css')}}">
  <!-- css switalert -->
  <link rel="stylesheet" href="{{asset('css/sweetalert.css')}}">
  <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
        page. However, you can choose any other skin. Make sure you
        apply the skin class to the body tag so the changes take effect. -->
  <link rel="stylesheet" href="{{asset('adminlte/dist/css/skins/skin-blue-light.min.css')}}">
  <!--graficos js-->
<script src="{{asset('js/amcharts/amcharts.js')}}"></script>
<script src="{{asset('js/amcharts/pie.js')}}"></script>
<script src="{{asset('js/amcharts/funnel.js')}}"></script>
<script src="{{asset('js/amcharts/gantt.js')}}"></script>
<script src="{{asset('js/amcharts/gauge.js')}}"></script>
<script src="{{asset('js/amcharts/radar.js')}}"></script>
<script src="{{asset('js/amcharts/serial.js')}}"></script>
<script src="{{asset('js/amcharts/xy.js')}}"></script>

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to get the
desired effect
|---------------------------------------------------------|
| SKINS         | skin-blue                               |
|               | skin-black                              |
|               | skin-purple                             |
|               | skin-yellow                             |
|               | skin-red                                |
|               | skin-green                              |
|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="hold-transition skin-blue-light sidebar-mini">
<div class="wrapper">

  <!-- Main Header -->
  <header class="main-header">

    <!-- Logo -->
    <a href="index2.html" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>E</b>YD</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Exporta</b> y Declara</span>
    </a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
          <li class="dropdown messages-menu">
            <!-- Menu toggle button -->
            <!--INICIO DEL ICONO DE LA BANDEJA DEL MENSAJE
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-envelope-o"></i>
              <span class="label label-success">2</span>
            </a>

            FIN DEL ICONO DE LA BANDEJA DEL MENSAJE-->
            <a href="#" id="boton"  style="padding: 1.30rem 0.75rem; " >
              <i class="glyphicon glyphicon-info-sign" style="font-size: 24px;"></i>	
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have 4 messages</li>
              <li>
                <!-- inner menu: contains the messages -->
                <ul class="menu">
                  <li><!-- start message -->
                    <a href="#">
                      <div class="pull-left">
                        <!-- User Image -->

                       {{-- <img src="{{asset('adminlte/dist/img/avatar.jpg')}}" class="img-circle" alt="User Image"> --}}

                      </div>
                      <!-- Message title and timestamp -->
                      <h4>
                        Support Team
                        <small><i class="fa fa-clock-o"></i> 5 mins</small>
                      </h4>
                      <!-- The message -->
                      <p>Why not buy a new awesome theme?</p>
                    </a>
                  </li>
                  <!-- end message -->
                </ul>
                <!-- /.menu -->
              </li>
              <li class="footer"><a href="#">See All Messages</a></li>
            </ul>
          </li>
          <!-- /.messages-menu -->

          <!-- Notifications Menu -->
          <li class="dropdown notifications-menu">
            <!-- Menu toggle button -->
            <!--INICIO DEL ICONO DE LA CAMPANITA
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-bell-o"></i>
              <span class="label label-warning">4</span>
            </a>

            FIN DEL ICONO DE LA CAMPANITA-->
            <ul class="dropdown-menu">
              <li class="header">You have 10 notifications</li>
              <li>
                <!-- Inner Menu: contains the notifications -->
                <ul class="menu">
                  <li><!-- start notification -->
                    <a href="#">
                      <i class="fa fa-users text-aqua"></i> 5 new members joined today
                    </a>
                  </li>
                  <!-- end notification -->
                </ul>
              </li>
              <li class="footer"><a href="#">View all</a></li>
            </ul>
          </li>
          <!-- Tasks Menu -->
          <li class="dropdown tasks-menu">
            <!-- Menu Toggle Button -->
            <!--INICIO DEL ICONO DE LA BANDERITA
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-flag-o"></i>
              <span class="label label-danger">6</span>
            </a>

            FIN DEL ICONO DE LA BANDERITA -->
            <ul class="dropdown-menu">
              <li class="header">You have 9 tasks</li>
              <li>
                <!-- Inner menu: contains the tasks -->
                <ul class="menu">
                  <li><!-- Task item -->
                    <a href="#">
                      <!-- Task title and progress text -->
                      <h3>
                        Design some buttons
                        <small class="pull-right">20%</small>
                      </h3>
                      <!-- The progress bar -->
                      <div class="progress xs">
                        <!-- Change the css width attribute to simulate progress -->
                        <div class="progress-bar progress-bar-aqua" style="width: 20%" role="progressbar"
                             aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                          <span class="sr-only">20% Complete</span>
                        </div>
                      </div>
                    </a>
                  </li>
                  <!-- end task item -->
                </ul>
              </li>
              <li class="footer">
                <a href="#">View all tasks</a>
              </li>
            </ul>
          </li>
          <!-- User Account Menu -->
          <li class="dropdown user user-menu">
            <!-- Menu Toggle Button -->
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <!-- The user image in the navbar-->
              <img src="{{asset('adminlte/dist/img/avatar.png')}}" class="user-image" alt="User Image">
              <!-- hidden-xs hides the username on small devices so only the image appears. -->
              <span class="hidden-xs">{{ Auth::user()->email }}</span>
            </a>
            <ul class="dropdown-menu">
              <!-- The user image in the menu -->
              <li class="user-header">
                <img src="{{asset('adminlte/dist/img/avatar.png')}}" class="img-circle" alt="User Image">

                <p>
                  {{ Auth::user()->email }} - Usuario Administrador

                </p>
              </li>
              <!-- Menu Body
              <li class="user-body">
                <div class="row">
                  <div class="col-xs-4 text-center">
                    <a href="#">Followers</a>
                  </div>
                  <div class="col-xs-4 text-center">
                    <a href="#">Sales</a>
                  </div>
                  <div class="col-xs-4 text-center">
                    <a href="#">Friends</a>
                  </div>
                </div>
                <!-- /.row
              </li>-->
              <!-- Menu Footer-->
              <li class="user-footer">

                <!--div class="pull-left">
                  <a href="#" class="btn btn-default btn-flat">Perfil</a>
                </div-->
                <!--div class="pull-right">
                  <a href="{{url('/logout')}}" class="btn btn-default btn-flat">Salir</a>
                </div-->
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
            <li>
              <!--Incorporando el boton de salida-->
               <a href="{{url('/logout')}}" class="btn btn-primary" aria-label="" style="background-color:#1E3165; color:#FFFFFF;"><b>SALIR</b>&nbsp;&nbsp;<span class="glyphicon glyphicon-log-out"></span></a>
            </li>
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

      <!-- Sidebar user panel (optional) -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="{{asset('adminlte/dist/img/avatar.png')}}" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">

          <!-- Status -->
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>

        @section('menu')
          @include('menugen_analist_djo')
        @stop
      </div>
      <div class="row text-center sidebar-form">
        <p><b>{{ Auth::user()->email }}</b></p>
      </div>
      <!-- search form (Optional)
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
              <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
              </button>
            </span>
        </div>
      </form>
      <!-- /.search form -->
     @yield('menu')
    <!--------------------------
        | Your Page Menu Here |
        -------------------------->

   </section>
    <!-- /.sidebar -->

  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        @if (isset($titulo))
          {{$titulo}}
       @else
       Ventanilla Única de Comercio Exterior - Analista
       @endif

       @if (isset($descripcion)) 
       <div class="alert-container" >
          <div class="row">
          <div class="col-lg-12">   
          
          @if (@$error==0)
             <div id="space_alert1" class="alert alert-success" role="alert" style="display: block;">
             <i class="glyphicon glyphicon-warning-sign"></i>
          @else
             <div id="space_alert1" class="alert alert-danger" role="alert" style="display: block;">
            
             <i class="glyphicon glyphicon-warning-sign"></i>
          @endif
             {{$titulo}}  <br>
            </div>
            <div id="space_alert2" class="alert alert-light" role="alert" style="display: none;">
            <br>
            </div>
           </div>
       </div>
     </div>
        @else
           VUCE
        @endif
        <!--ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
          <li class="active">Here</li>
        </ol-->
        </h1>
      </section>

    <!-- Main content -->
    <section class="content container-fluid">
      @include('flash::message')
       @yield('content')


      <!--------------------------
        | Your Page Content Here |
        -------------------------->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
Departamento de Desarrollo
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2023 </strong> Gerencia de Tecnología de la Información
  </footer>

  <!-- Control Sidebar -->
  {{--<aside class="control-sidebar control-sidebar-dark">
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
      <li class="active"><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
      <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
      <!-- Home tab content -->
      <div class="tab-pane active" id="control-sidebar-home-tab">
        <h3 class="control-sidebar-heading">Recent Activity</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:;">
              <i class="menu-icon fa fa-birthday-cake bg-red"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>

                <p>Will be 23 on April 24th</p>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

        <h3 class="control-sidebar-heading">Tasks Progress</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:;">
              <h4 class="control-sidebar-subheading">
                Custom Template Design
                <span class="pull-right-container">
                    <span class="label label-danger pull-right">70%</span>
                  </span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

      </div>
      <!-- /.tab-pane -->
      <!-- Stats tab content -->
      <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
      <!-- /.tab-pane -->
      <!-- Settings tab content -->
      <div class="tab-pane" id="control-sidebar-settings-tab">
        <form method="post">
          <h3 class="control-sidebar-heading">General Settings</h3>

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Report panel usage
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Some information about this general settings option
            </p>
          </div>
          <!-- /.form-group -->
        </form>
      </div>
      <!-- /.tab-pane -->
    </div>
  </aside> --}}
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
  immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->


<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 3 -->
<script src="{{asset('adminlte/bower_components/jquery/dist/jquery.min.js')}}"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.2/moment.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.2/locale/es.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="{{asset('adminlte/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<!-- js calendario bootstrap -->
<script src="{{asset('js/jquery.min.js')}}"></script>
<script src="{{asset('js/bootstrap-datepicker.min.js')}}"></script>
<script src="{{asset('js/locales/bootstrap-datepicker.es.min.js')}}"></script>

<script src="{{asset('js/jquery.dataTables.min.js')}}"></script>
<!-- js sweetalert -->
<script src="{{asset('js/sweetalert.js')}}"></script>

<!-- script js y jquery del sistema -->
<script src="{{asset('js/script_exportaciones.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('adminlte/dist/js/adminlte.min.js')}}"></script>
<!-- js data table bootstrap -->
<script src="{{asset('js/dataTables.bootstrap.min.js')}}"></script>
<script type="text/javascript">
  //almacenando el div y el boton en unas variables
  var div = document.getElementById('space_alert1');
  var div2 = document.getElementById('space_alert2');
  var but = document.getElementById('boton');
  //la funcion que oculta y muestra
  function showHide(e){
      e.preventDefault();
      e.stopPropagation();
      if(div.style.display == "none"){
          div.style.display = "block"; 
          div2.style.display = "none"; 
              but.style.padding = "1.50rem  0.75rem";
          but.innerHTML = '<i class="glyphicon glyphicon-info-sign" style="font-size: 24px;"></i> ';
    
      } else if(div.style.display == "block"){
          div.style.display = "none";
          div2.style.display = "block";
              but.style.padding = "1.50rem   0.75rem";
          but.innerHTML = '<i class="glyphicon glyphicon-info-sign" style="font-size: 24px;"></i> ';
   
      }
  }
  //al hacer click en el boton
  but.addEventListener("click", showHide, false);

</script>


<script>
    $('#mensaje_show, #mensaje_analista').prop('disabled', true);


/********* Duda Analista*********** */
$('#actualizar_respuesta').click(function (e) {
 
  let validar = enviarDuda();
  let id = $(this).data("id");
  let route = '/calificaciondjo/DudasAnalistaSolicitud/update/'+id;
  e.preventDefault();
  $(this).html('Enviando..');
  
  if(validar == 0){
    $.ajax({
      data: $('#dudas').serialize(),
      url: route,
      type: "PUT",
      dataType: 'json',
      success: function (data) {
          console.log(data);
          swal("¡Actualizado!", "¡Duda actualizada exitosamente!", "success");
      },
      error: function (data) {
          console.log('Error:', data);
          $('#actualizar_mensaje').html('Actualizar');
          swal("Cancelado", "No se ha procesado la actualización", "warning");
      }  
    });
  }
  
       
});

/* Validación Registro DUDA*/
function enviarDuda() {

    var mens=[
        'Estimado usuario. Debe ingresar el Número de DUA para completar el registro',
        'Estimado usuario. Debe ingresar el Número de Referencia para completar el registro',
        'Estimado usuario. Debe seleccionar el Agente Aduanal para completar el registro',
        'Estimado usuario. Debe seleccionar la Modalidad de Transporte para completar el registro',
        'Estimado usuario. Debe seleccionar la Aduana Salida para completar el registro',
        'Estimado usuario. Debe ingresar el Lugar de Salida para completar el registro',
        'Estimado usuario. Debe ingresar la Aduana de LLegada para completar el registro',
        'Estimado usuario. Debe ingresar el Lugar de LLegada para completar el registro',
        'Estimado usuario. Debe ingresar la Fecha de Registro de DUA en Aduanas para completar el registro',
        'Estimado usuario. Debe ingresar la Fecha de Embarque para completar el registro',
        'Estimado usuario. Debe ingresar la Fecha de Estimada de Arribo para completar el registro',
        'Estimado usuario. Debe seleccionar el consignatario para completar el registro',
        'Estimado usuario. Debe ingresar el mensaje de duda para completar el registro',
        'Estimado usuario. Debe ingresar la respuesta a la duda para completar el registro'
    ];

    $("div").remove(".msg_alert");

    let err_duda = 0;
    let error_dudas = '<div class="msg_alert"><span style="color:red" class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span><strong id="cod_aran-error" class="added"> El campo es obligatorio.</strong></div>';
    let valido_dudas = '<div class="msg_alert"><span style="color:green" class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span></div>';
    let campos = ["correo", "razon_social", "telefono_movil", "mensaje", "mensaje_registrado", "repuesta", "respuesta_analista", "tipo"];
    let nombre_campos = ["correo", "razon social", "telefono móvil", "mensaje", "mensaje", "respuesta", "respuesta", "motivo de contacto"];
    let n = campos.length;
    let input_duda = '';

    for (var i = 0; i < n; i++) {
      input_duda = $('#'+campos[i]);
      
      if (input_duda.val() == '')
      {
        err_duda = 1;
        input_duda.css('border', '1px solid red').after(error_dudas);
        if(err_duda==1){
          event.preventDefault(); 
          swal("Por Favor!", 'Estimado Usuario. Debe Ingresar '+nombre_campos[i].toUpperCase()+' para Completar el Registro', "warning")
        }
      }
      else{
        if (err_duda == 1) {err_duda = 1;}else{err_duda = 0;}
        input_duda.css('border', '1px solid green').after(valido_dudas);
      }
    }

    return err_duda;
}
</script>
<!-- Optionally, you can add Slimscroll and FastClick plugins.
     Both of these plugins are recommended to enhance the
     user experience. -->
     @include('sweet::alert')
</body>
</html>
